-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2020 at 11:49 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `max_uses` int(200) NOT NULL,
  `max_uses_user` int(200) NOT NULL,
  `discount` int(200) DEFAULT NULL,
  `status` int(200) NOT NULL,
  `freedelivery` int(200) DEFAULT NULL,
  `exclude_category` longtext DEFAULT NULL,
  `exclude_product` longtext DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `startandexpire` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `type`, `max_uses`, `max_uses_user`, `discount`, `status`, `freedelivery`, `exclude_category`, `exclude_product`, `start_date`, `expire_date`, `startandexpire`, `created_at`, `updated_at`) VALUES
(1, '<N>A!iyLB-HPFcf&h^<$', 'fixed', 1, 1000, 1000, 1, 0, 'a:3:{i:0;s:2:\"42\";i:1;s:2:\"43\";i:2;s:2:\"49\";}', 'a:3:{i:0;s:2:\"40\";i:1;s:2:\"41\";i:2;s:2:\"42\";}', '2020-12-02', '2020-12-03', '02/12/2020 - 03/12/2020', '2020-02-23 03:52:50', '2020-02-23 05:42:08'),
(3, 'Birthday', 'fixed', 2, 1000, 100, 1, 1, 'a:2:{i:0;s:2:\"43\";i:1;s:2:\"49\";}', 'a:2:{i:0;s:2:\"38\";i:1;s:2:\"40\";}', '2020-10-06', '2020-10-07', NULL, '2020-02-23 04:46:24', '2020-02-23 04:46:24'),
(4, '8@55knqfg<MEDkPJ-c!m', 'fixed', 1, 1000, 21, 1, 1, 'a:1:{i:0;s:2:\"50\";}', 'a:1:{i:0;s:2:\"41\";}', '2020-06-02', '2020-07-03', '02/06/2020 - 03/07/2020', '2020-02-23 04:57:36', '2020-02-23 05:49:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
