-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 11, 2020 at 10:53 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_setting`
--

CREATE TABLE `category_setting` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `reason_titles` longtext,
  `reason_descriptions` longtext,
  `feature_titles` longtext,
  `feature_descriptions` longtext,
  `posters` longtext,
  `actual_material_pics` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_setting`
--

INSERT INTO `category_setting` (`id`, `status`, `reason_titles`, `reason_descriptions`, `feature_titles`, `feature_descriptions`, `posters`, `actual_material_pics`, `created_at`, `updated_at`) VALUES
(7, 'on', 'a:1:{i:0;s:8:\"Reason 1\";}', 'a:1:{i:0;s:56:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\";}', 'a:1:{i:0;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}', 'a:1:{i:0;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}', 'a:1:{i:0;s:83:\"upload/category_setting/posters-pics//LPJ92fy7vucGFwChPAsq3QZwYz7O1R09MNy3VzVn.jpeg\";}', 'a:1:{i:0;s:83:\"upload/category/actual-material-pics//uCeL3P23C90qzpVigEeuGDRbuzf2kFiNV9pq6vKN.jpeg\";}', '2020-04-11 15:51:39', '2020-04-11 15:52:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_setting`
--
ALTER TABLE `category_setting`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category_setting`
--
ALTER TABLE `category_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
