-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2020 at 06:56 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  `product_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_meta` longtext,
  `product_variation_id` int(11) DEFAULT NULL,
  `image` longtext,
  `user_meta` longtext,
  `price` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `material` varchar(200) DEFAULT NULL,
  `size` varchar(200) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `extra_detail` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_log`
--

INSERT INTO `order_log` (`id`, `user_id`, `product_id`, `product_name`, `product_meta`, `product_variation_id`, `image`, `user_meta`, `price`, `qty`, `material`, `size`, `extra`, `weight`, `extra_detail`, `created_at`, `updated_at`) VALUES
(49, '1', '155', 'post-img1', 'a:12:{s:2:\"id\";i:155;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"post-img1\";s:4:\"slug\";s:9:\"post-img1\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:78:\"upload/product/post-img1/feature/nEfHsDtaInZ4ArHS2FtOvPjJZCRhoMk3fYQHKjC0.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";i:63;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/post-img1/feature/nEfHsDtaInZ4ArHS2FtOvPjJZCRhoMk3fYQHKjC0.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":\"2020-06-04 15:22:05\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-06-04 15:22:05\"}', '126', 2, 'Heavy Material', '240cm', 'Yes', 5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"2\";s:6:\"weight\";d:5;}', '2020-06-08 12:55:43', '2020-06-08 12:55:43'),
(50, '1', '156', 'post-img2', 'a:12:{s:2:\"id\";i:156;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"post-img2\";s:4:\"slug\";s:9:\"post-img2\";s:11:\"description\";s:612:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>\";s:3:\"sku\";N;s:5:\"image\";s:78:\"upload/product/post-img2/feature/SJJ05fADEODOCttITm0UsVZXYQZBDut4PBBrxiFX.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";i:20;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/post-img2/feature/SJJ05fADEODOCttITm0UsVZXYQZBDut4PBBrxiFX.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":\"2020-06-04 15:22:05\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-06-04 15:22:05\"}', '126', 2, 'Heavy Material', '240cm', 'Yes', 12, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"2\";s:6:\"weight\";i:12;}', '2020-06-08 12:55:52', '2020-06-08 12:55:52'),
(51, '1', '157', 'cat-widget1', 'a:12:{s:2:\"id\";i:157;s:7:\"user_id\";i:1;s:5:\"title\";s:11:\"cat-widget1\";s:4:\"slug\";s:11:\"cat-widget1\";s:11:\"description\";s:604:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis. Fusce at pretium urna, quis pharetra elit. Curabitur sollicitudin pellentesque ex sit amet imperdiet. Aenean erat ante, accumsan venenatis tincidunt a, tincidunt sed leo. Morbi scelerisque tempus neque vel pretium. Vivamus ac erat cursus, commodo ligula et, eleifend nulla. Nulla sit amet volutpat turpis. Suspendisse pretium maximus sem, at fermentum velit viverra sodales.</span>\";s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/cat-widget1/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";i:63;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/cat-widget1/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":\"2020-06-04 15:22:05\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-06-04 15:22:05\"}', '126', 1, 'Heavy Material', '240cm', 'Yes', 3.6, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:3.6;}', '2020-06-08 12:55:56', '2020-06-08 12:55:56'),
(52, '1', '158', 'Testing Product Dumy', 'a:12:{s:2:\"id\";i:158;s:7:\"user_id\";i:1;s:5:\"title\";s:20:\"Testing Product Dumy\";s:4:\"slug\";s:20:\"testing-product-dumy\";s:11:\"description\";s:786:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at mi eget tortor egestas rhoncus. Mauris dictum, dui a luctus aliquet, quam magna laoreet massa, vel fringilla libero leo ac massa. Quisque quam odio, finibus quis tempor vitae, ultricies sagittis est. Donec pharetra lacinia velit ac tincidunt. Aenean et ligula pulvinar, tristique justo non, facilisis augue. Nunc a auctor ligula, sed facilisis sapien. Sed lectus ligula, semper eget ultricies non, sagittis eget nisl. Nam et leo semper, mollis tortor quis, accumsan metus. Morbi eget sem eu lectus aliquet congue. Vivamus interdum elit et est aliquet maximus. Sed convallis malesuada tristique.</span><br></p>\";s:3:\"sku\";N;s:5:\"image\";s:89:\"upload/product/testing-product-dumy/feature/eiCpdpOuGpXOXoQ1IYwrZJxHd4qJ8Oph5PDGABsR.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:55;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/testing-product-dumy/feature/eiCpdpOuGpXOXoQ1IYwrZJxHd4qJ8Oph5PDGABsR.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":\"2020-06-04 15:22:05\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-06-04 15:22:05\"}', '123', 1, 'Heavy Material', '234cm', 'Yes', 5.4, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:5.4;}', '2020-06-08 12:55:59', '2020-06-08 12:55:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
