-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2020 at 11:19 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` longtext,
  `answer` longtext,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(1, '<div>What Is Torben Goldmund’s Art ‘N’</div>', '<div style=\"text-align: justify;\"><font face=\"Open Sans, Arial, sans-serif\"><span style=\"font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris molestie ornare laoreet. Nam sed mattis ante. Nam ullamcorper molestie pellentesque. Pellentesque finibus metus a nulla volutpat, eget bibendum ligula aliquet. Duis consectetur ut justo sit amet dignissim. Sed laoreet mauris sed nulla finibus hendrerit. Proin semper vestibulum maximus. Curabitur maximus nunc metus, sed bibendum dui lacinia sit amet. Quisque porta, elit eget facilisis faucibus, nulla nulla ultrices odio, ut elementum metus nisl et massa. Donec pharetra, arcu dignissim varius bibendum, elit turpis porta odio, vitae dapibus augue nulla nec nibh. Praesent hendrerit lacus neque, sed pellentesque enim ullamcorper vitae. Interdum et malesuada fames ac ante ipsum primis in faucibus.</span></font></div>', 0, '2020-02-27 16:28:17', '2020-02-27 17:15:26'),
(2, 'weweweqwewqqwewqe', 'dsadfagfgdgdfgsdfhd', 0, '2020-02-27 17:04:35', '2020-02-27 17:16:00'),
(3, 'ncvbnbvnbdjjfjf', 'ghjffhjfhjghjghjhncbvnbvn', 1, '2020-02-27 17:04:58', '2020-02-27 17:16:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
