-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2020 at 01:13 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'on',
  `address` longtext,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `image` longtext,
  `timing` varchar(255) DEFAULT NULL,
  `details` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `status`, `address`, `state`, `city`, `country`, `image`, `timing`, `details`, `created_at`, `updated_at`) VALUES
(1, 'Art Center Manatee', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'West Virgina', 'Huntington', 'United States', 'upload/events/1/wIbxTZTiNDuWciRqZXJ4jdMq6Abr9P6jNfTTfa8j.jpeg', '05/30/2020 12:00 PM', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel felis dictum, efficitur nulla ac, luctus ipsum. Ut placerat sapien nec orci dictum, sit amet accumsan nisi tristique.', '2020-04-30 14:13:43', '2020-04-30 15:09:05'),
(2, 'Otto Hubbard', 'on', 'Excepteur porro ab r', 'Bavaria', 'Munich', 'Germany', 'upload/events/2/o5w96Iw6wXDBau7DoPbopPh0KE3FCLisgrFg90HO.jpeg', '05/25/2020 1:11 PM', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</span>', '2020-04-30 15:12:44', '2020-04-30 16:55:51'),
(3, 'Art Center Venice', 'on', 'Excepteur porro ab r', NULL, 'Venice', 'Italy', 'upload/events/3/EeE5Ha2pQsgCeIerWmInreFUzotG2ouRxwV10M6s.jpeg', '06/18/2020 1:16 PM', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</span><br></p>', '2020-04-30 15:16:40', '2020-04-30 16:55:53'),
(4, 'Exhibition Of Art Culture', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 'London', 'United Kingdom', 'upload/events/4/zPXg38Ps3ue9QyWlWJeohNsvsaYAsQuB2AoYgDh9.jpeg', '06/27/2020 1:17 PM', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel felis dictum, efficitur nulla ac.</span><br></p>', '2020-04-30 15:19:11', '2020-04-30 16:55:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
