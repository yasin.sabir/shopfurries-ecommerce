-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2020 at 10:59 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_meta`
--

CREATE TABLE `category_meta` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category_meta`
--

INSERT INTO `category_meta` (`id`, `category_id`, `meta_key`, `meta_value`) VALUES
(2, 58, 'price', '389'),
(3, 58, 'tags', 'a:1:{i:0;s:1:\"8\";}'),
(4, 58, 'poster_images', 'a:1:{i:0;s:9:\"comic.png\";}'),
(5, 58, 'actual_material_images', 'a:3:{i:0;s:50:\"aerial_view_of_toronto_city-wallpaper-1366x768.jpg\";i:1;s:36:\"airbus_a350_3-wallpaper-1366x768.jpg\";i:2;s:36:\"apple_desktop-wallpaper-1366x768.jpg\";}'),
(6, 58, 'category_prod_features', 'Odit nostrum nihil i.asdasdasdasdsaddasdasdasdasdasdas'),
(7, 59, 'price', NULL),
(8, 59, 'tags', NULL),
(9, 59, 'poster_images', NULL),
(10, 59, 'actual_material_images', NULL),
(11, 59, 'category_reason_title', NULL),
(12, 59, 'category_reason_detail', NULL),
(13, 59, 'category_prod_features', NULL),
(14, 60, 'price', '264'),
(15, 60, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"8\";i:2;s:1:\"9\";}'),
(16, 60, 'poster_images', 'a:1:{i:0;s:9:\"comic.png\";}'),
(17, 60, 'actual_material_images', 'a:3:{i:0;s:36:\"airbus_a350_3-wallpaper-1366x768.jpg\";i:1;s:36:\"apple_desktop-wallpaper-1366x768.jpg\";i:2;s:22:\"pexels-photo-80277.png\";}'),
(18, 60, 'category_reason_title', 'a:3:{i:0;s:8:\"Reason 1\";i:1;s:8:\"Reason 2\";i:2;s:8:\"Reason 3\";}'),
(19, 60, 'category_reason_detail', 'a:3:{i:0;a:1:{i:0;s:56:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\";}i:1;a:1:{i:0;s:56:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\";}i:2;a:1:{i:0;s:56:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\";}}'),
(20, 60, 'category_prod_features', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in consequat nibh. Cras ultricies pretium euismod. Sed euismod mauris nec nisl scelerisque, a interdum sapien finibus. Donec elementum sodales lacus, ut semper ipsum feugiat quis. Vestibulum rhoncus pulvinar massa et cursus. Cras non odio finibus, lacinia ante id, egestas lorem.</span>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_meta`
--
ALTER TABLE `category_meta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category_meta`
--
ALTER TABLE `category_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
