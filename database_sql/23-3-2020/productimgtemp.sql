-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2020 at 12:04 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `productimgtemp`
--

CREATE TABLE `productimgtemp` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` longtext,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `productimgtemp`
--

INSERT INTO `productimgtemp` (`id`, `name`, `image`, `category_id`, `created_at`, `updated_at`) VALUES
(298, 'battlefield_23-wallpaper-1366x768', 'upload/product_temperory_img/spc8Db9aouiqyfEO4aLZoKWqmeWWU9IJMxWeTGOI.jpeg', 65, '2020-03-22 11:44:08', '2020-03-22 11:44:08'),
(299, 'camera-drone-electronics-916015', 'upload/product_temperory_img/j9viwNZhFDYu1reHXeMp388n75cL1kYMaMl2o1rd.jpeg', 65, '2020-03-22 11:44:08', '2020-03-22 11:44:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=300;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
