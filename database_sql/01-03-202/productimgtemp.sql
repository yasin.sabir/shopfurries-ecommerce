-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2020 at 07:05 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `productimgtemp`
--

CREATE TABLE `productimgtemp` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `productimgtemp`
--

INSERT INTO `productimgtemp` (`id`, `name`, `created_at`, `updated_at`) VALUES
(161, 'upload/product_temperory_img//0xnrrGOQ7KBwqtf8U3GAmLOBvhNZ5DrhbidrEvHF.jpeg', '2020-03-01 10:01:47', '2020-03-01 10:01:47'),
(162, 'upload/product_temperory_img//TpXd3NZXFZ0rWBh57Y4GTzBTXvvHavnw1UtehQXO.jpeg', '2020-03-01 10:03:16', '2020-03-01 10:03:16'),
(163, 'upload/product_temperory_img//zS1yuHt14xo0cVAKQ1w7QoqE93vZNAKIMCmxmspj.jpeg', '2020-03-01 10:03:16', '2020-03-01 10:03:16'),
(164, 'upload/product_temperory_img//Kpj5cYvdQ8wWwaAATil3xnJt7SdthtxLY4m0kJd3.jpeg', '2020-03-01 10:04:53', '2020-03-01 10:04:53'),
(165, 'upload/product_temperory_img//AtCmPgYWXjQ1RTdRtixO9PqeiBLTR95ryF2L4MaV.jpeg', '2020-03-01 10:04:53', '2020-03-01 10:04:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
