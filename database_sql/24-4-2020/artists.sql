-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2020 at 10:21 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `image` longtext,
  `status` varchar(255) DEFAULT NULL,
  `additional_details` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`, `email`, `image`, `status`, `additional_details`, `created_at`, `updated_at`) VALUES
(6, 'qwerty Artist 123', 'qwerty_arddddtist@gmail.com', 'upload/artists/6//JB063lHTnecxkS79vzZxe7i1LvsGZQgQRZxkEuNq.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-04-14 16:13:49', '2020-04-18 15:09:27'),
(7, 'sdsadas', 'dsds@d.com', NULL, 'on', NULL, '2020-04-14 16:28:48', '2020-04-14 16:28:48'),
(9, 'Jackie Wilson', 'wilson@gmail.com', 'upload/artists/9/1bxVvBuVNJeVEpzRN3F2iClyNXdpnDCn1z41XNYI.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-04-15 15:39:33', '2020-04-16 15:23:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artists`
--
ALTER TABLE `artists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
