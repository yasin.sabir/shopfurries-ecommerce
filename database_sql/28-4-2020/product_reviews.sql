-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2020 at 07:29 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `flag` varchar(255) DEFAULT NULL,
  `description` longtext,
  `image` longtext,
  `video` longtext,
  `parent_id` int(11) DEFAULT NULL,
  `voted` varchar(255) DEFAULT NULL,
  `unvoted` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `user_id`, `product_id`, `status`, `flag`, `description`, `image`, `video`, `parent_id`, `voted`, `unvoted`, `created_at`, `updated_at`) VALUES
(5, 1, 126, 'on', 'average', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies tortor sit amet sagittis imperdiet.', 'a:2:{i:0;s:95:\"upload/product_review/product_126/user_1/review_5/Mr6p0B01tHarH79e8Poy9G2J3s0wbP758NEv2sZg.jpeg\";i:1;s:95:\"upload/product_review/product_126/user_1/review_5/r0urDvXmvaztMO1AwRId5ZAaeYLx8ax8p3EiyRmQ.jpeg\";}', NULL, NULL, NULL, NULL, '2020-04-26 15:27:34', '2020-04-26 15:27:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
