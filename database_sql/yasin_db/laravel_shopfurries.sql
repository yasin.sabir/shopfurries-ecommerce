-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2020 at 01:14 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `image` longtext,
  `status` varchar(255) DEFAULT NULL,
  `additional_details` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`, `email`, `image`, `status`, `additional_details`, `created_at`, `updated_at`) VALUES
(9, 'Jackie Wilson', 'wilson@gmail.com', 'upload/artists/9/1bxVvBuVNJeVEpzRN3F2iClyNXdpnDCn1z41XNYI.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-04-15 15:39:33', '2020-04-16 15:23:59');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Alias` varchar(255) NOT NULL,
  `Image` longtext,
  `Parent` varchar(255) DEFAULT NULL,
  `Top` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Additional_Req` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `Name`, `Alias`, `Image`, `Parent`, `Top`, `Status`, `Additional_Req`, `created_at`, `updated_at`) VALUES
(64, 'Mens', 'mens', 'upload/category/mens_64/thumbnail/uawAE6pguoTIylpcsomID6yLLjajz0Ub9JkUWGtp.jpeg', NULL, 1, 1, 0, '2020-03-21 12:50:20', '2020-03-31 15:59:31'),
(65, 'Shirts', 'shirts', 'upload/category/shirts_65/thumbnail/g8cUGSUwzFPdmqnfsLRt4PR95xX7GmhOUlmcHeey.jpeg', '64', 1, 1, 1, '2020-03-21 12:51:36', '2020-03-28 04:04:53'),
(66, 'Art', 'art', 'upload/category/art_66/thumbnail/8uOJzAHHvRuMYT3xXgHSRxgansfVFEULrUf4vCaW.jpeg', NULL, 1, 1, 1, '2020-03-28 04:16:47', '2020-03-28 08:44:03'),
(67, 'Abstract Art', 'abstract-art', 'upload/category/abstract-art_67/thumbnail/QwFtUcGXYQwHJ7t1cMFWNziWaJsycFFJnrVE7329.jpeg', NULL, 1, 1, 1, '2020-03-28 11:27:08', '2020-04-21 14:59:01'),
(68, 'Florida Shirts', 'florida-shirts', 'upload/category/florida-shirts_68/thumbnail/qRywPuYhFcq00gUG2wRLLjI9VXzRJxQMW7Xd00xR.png', NULL, 1, 1, 1, '2020-04-15 17:54:55', '2020-04-23 15:34:05'),
(70, 'Slim Shirts', 'slim-shirts', 'upload/category/slim-shirts_70/thumbnail/gfzIjvpQMZSiK6dkfemgP6c3h3TQKOnQfBpYRMCy.png', 'root', 1, 1, 0, '2020-04-21 15:27:24', '2020-04-21 15:27:24');

-- --------------------------------------------------------

--
-- Table structure for table `category_meta`
--

CREATE TABLE `category_meta` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category_meta`
--

INSERT INTO `category_meta` (`id`, `category_id`, `meta_key`, `meta_value`) VALUES
(42, 64, 'price', NULL),
(43, 64, 'tags', NULL),
(44, 64, 'poster_images', 'a:0:{}'),
(45, 64, 'actual_material_images', 'a:0:{}'),
(46, 64, 'category_reason_title', NULL),
(47, 64, 'category_reason_detail', NULL),
(48, 64, 'category_prod_features', NULL),
(49, 65, 'price', '68'),
(50, 65, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(51, 65, 'poster_images', 'a:2:{i:0;s:84:\"upload/category/shirts_65/posters-pics//SEYncNtoIVPXc6Icz5T0tr4Tn1NqCgCjZe5KPJl6.png\";i:1;s:84:\"upload/category/shirts_65/posters-pics//j2d9Vc7gbXp4tRaZHt2DwIToo43gv2xho8uuRDhU.png\";}'),
(52, 65, 'actual_material_images', 'a:3:{i:0;s:93:\"upload/category/shirts_65/actual-material-pics//Xa26lMVzr0dvkvo0YvEZUUPorns5l0ipTPcObeVM.jpeg\";i:1;s:93:\"upload/category/shirts_65/actual-material-pics//gVNjMKnFl5X8cqyOOlsSqs3IEgrCrUvYToAXJ6Kz.jpeg\";i:2;s:93:\"upload/category/shirts_65/actual-material-pics//CRd8ElNCy5zUnpF4NqXYRFQdvyklLBsosC6T7PmQ.jpeg\";}'),
(53, 65, 'category_reason_title', 'a:1:{i:0;s:8:\"Reason 1\";}'),
(54, 65, 'category_reason_detail', 'a:1:{i:0;s:11:\"dsdasdasdsd\";}'),
(55, 65, 'category_prod_features', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dapibus elit finibus, lacinia libero vel, mollis est. Phasellus vitae nulla nec quam pulvinar porttitor at quis ex. Vivamus dui lorem, elementum eget tristique et, auctor vitae lorem. Sed urna lacus, tempor a convallis ut, hendrerit at nisl. Curabitur aliquet consectetur cursus.'),
(56, 65, 'material', 'Heavy Material , Soft Quality'),
(57, 65, 'sizes', '240cm, 566c,'),
(58, 66, 'price', '56'),
(59, 66, 'tags', 'a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}'),
(60, 66, 'material', 'Heavy Material , Soft Quality'),
(61, 66, 'sizes', '240cm, 566cm'),
(62, 66, 'poster_images', 'a:2:{i:0;s:82:\"upload/category/art_66/posters-pics//Dg814bd92OZuxiqotzaDhZf65r1hEPzS4Gibeiaq.jpeg\";i:1;s:82:\"upload/category/art_66/posters-pics//APnAeuiKH20YpvwyTi4k98ZT4KcfyLjy1J0BRyUD.jpeg\";}'),
(63, 66, 'actual_material_images', 'a:3:{i:0;s:90:\"upload/category/art_66/actual-material-pics//rwRvDeYvwOzE0wD4mG2udBysdEQR9XkGWJCb35gM.jpeg\";i:1;s:90:\"upload/category/art_66/actual-material-pics//x6csmO81T9dYdlBvgY8kWQi343zYMjsmKXYzIaRQ.jpeg\";i:2;s:90:\"upload/category/art_66/actual-material-pics//ClyBnU6mVQKLfkA8lV25dZtTM53KgQb1n6WoEMIW.jpeg\";}'),
(64, 66, 'category_reason_title', 'a:1:{i:0;s:16:\"UNIQUE MATERIAL:\";}'),
(65, 66, 'category_reason_detail', 'a:1:{i:0;s:94:\"Unlike All Those Cheaply-Made Peach Skin Pillow Covers, You Can Finally Pamper Yourself With A\";}'),
(66, 66, 'category_prod_features', '<div><b>HELLO</b></div><ul><li>fdfdf</li><li>dfsdfsdf</li><li>fsdfsdfsd</li><li>dfsdfsdf</li></ul><p><b>Art Gallery</b></p><ul><li>fdfssfsdf</li><li>fsdfsdf</li><li>sdfdff</li><li>g343434</li></ul>'),
(67, 66, 'category_prod_feature_title', 'a:2:{i:0;s:51:\"HUG & CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";i:1;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(68, 66, 'category_prod_feature_detail', 'a:2:{i:0;s:220:\"Are You Ready To Meet Your Next Best Furry Friend? The Torben Goldmund\'s Art \'N\' Prints Anime Body Pillow Cases Are Here To Help You Sleep Better At Night Thanks To The Ultra-Soft Fabric And Eye-Catching Printed Designs.\";i:1;s:98:\"Are You Ready To Meet Your Next Best Furry Friend? The Torben Goldmund\'s Art \'N\' Prints Anime Body\";}'),
(69, 67, 'price', '59.0'),
(70, 67, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(71, 67, 'material', 'Heavy Material , Soft Quality'),
(72, 67, 'sizes', '240cm, 566cm'),
(73, 67, 'poster_images', 'a:1:{i:0;s:91:\"upload/category/abstract-art_67/posters-pics//nOgSD8dmGuttVIoemdeg5kYd4LzFDiU7whIp2d1D.jpeg\";}'),
(74, 67, 'actual_material_images', 'a:3:{i:0;s:99:\"upload/category/abstract-art_67/actual-material-pics//elr8jzjdXo2Zk2ecf3Nc3aLuVBTDnVEHPPeHTi1g.jpeg\";i:1;s:99:\"upload/category/abstract-art_67/actual-material-pics//OUyeH0U0DSaXC6FnGPPBDQS0rVXxiEJCn3PkYZvj.jpeg\";i:2;s:98:\"upload/category/abstract-art_67/actual-material-pics//Ff1TYqWXvH5fvtQan8yhSvY6kazxqfdhxBj3TaVi.png\";}'),
(75, 67, 'category_reason_title', 'a:3:{i:0;s:8:\"Reason 1\";i:1;s:8:\"Reason 2\";i:2;s:8:\"Reason 3\";}'),
(76, 67, 'category_reason_detail', 'a:3:{i:0;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";i:1;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";i:2;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";}'),
(77, 67, 'category_prod_feature_title', 'a:2:{i:0;s:51:\"HUG & CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";i:1;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(78, 67, 'category_prod_feature_detail', 'a:2:{i:0;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";i:1;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";}'),
(79, 64, 'material', NULL),
(80, 64, 'sizes', NULL),
(81, 64, 'category_prod_feature_title', NULL),
(82, 64, 'category_prod_feature_detail', NULL),
(83, 68, 'artist', '10'),
(84, 68, 'price', '68'),
(85, 68, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(86, 68, 'material', 'Heavy Material,Soft Quality'),
(87, 68, 'sizes', '240cm, 566c'),
(88, 68, 'poster_images', 'a:1:{i:0;s:93:\"upload/category/florida-shirts_68/posters-pics//NJmp4Bm27fWoXCKaALKrztM1df90YrEgjFlPfuxC.jpeg\";}'),
(89, 68, 'actual_material_images', 'a:1:{i:0;s:100:\"upload/category/florida-shirts_68/actual-material-pics//D14CAqm402Bf9RqhwlPzbFjPDHwdSjnrHPhp0hRO.png\";}'),
(90, 68, 'category_reason_title', 'a:1:{i:0;s:8:\"Reason 1\";}'),
(91, 68, 'category_reason_detail', 'a:1:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(92, 68, 'category_prod_feature_title', 'a:1:{i:0;s:11:\"SNUGGLIEST2\";}'),
(93, 68, 'category_prod_feature_detail', 'a:1:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(94, 67, 'artist', '9'),
(95, 69, 'artist', '6'),
(96, 69, 'price', '68'),
(97, 69, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"6\";}'),
(98, 69, 'material', 'Heavy-Material,Soft-Quality'),
(99, 69, 'sizes', '240cm,566c'),
(100, 69, 'poster_images', 'a:1:{i:0;s:88:\"upload/category/slim-card_69/posters-pics//MdkOrz7uqB2K0nVhZGdToWedMITcrmu7BaQnR1MF.jpeg\";}'),
(101, 69, 'actual_material_images', 'a:1:{i:0;s:96:\"upload/category/slim-card_69/actual-material-pics//9dat5yxfLFf2BticSOeE7Pf8IQ8dZrvpQuWFwNcM.jpeg\";}'),
(102, 69, 'category_reason_title', 'a:1:{i:0;s:16:\"UNIQUE MATERIAL:\";}'),
(103, 69, 'category_reason_detail', 'a:1:{i:0;s:17:\"dsasdasdasdasdasd\";}'),
(104, 69, 'category_prod_feature_title', 'a:1:{i:0;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(105, 69, 'category_prod_feature_detail', 'a:1:{i:0;s:13:\"dsdadsdasdasd\";}'),
(106, 70, 'artist', NULL),
(107, 70, 'price', NULL),
(108, 70, 'tags', NULL),
(109, 70, 'material', NULL),
(110, 70, 'sizes', NULL),
(111, 70, 'poster_images', 'a:0:{}'),
(112, 70, 'actual_material_images', 'a:0:{}'),
(113, 70, 'category_reason_title', NULL),
(114, 70, 'category_reason_detail', NULL),
(115, 70, 'category_prod_feature_title', NULL),
(116, 70, 'category_prod_feature_detail', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `product_id`, `category_id`) VALUES
(50, 56, 43),
(51, 56, 50),
(52, 56, 52),
(53, 56, 53),
(58, 58, 43),
(59, 58, 50),
(60, 58, 52),
(61, 59, 43),
(62, 59, 44),
(63, 59, 49),
(64, 59, 50),
(65, 59, 52),
(66, 59, 53),
(67, 60, 43),
(68, 60, 44),
(69, 60, 51),
(70, 60, 52),
(71, 61, 44),
(72, 61, 49),
(73, 62, 44),
(74, 62, 50),
(75, 62, 51),
(76, 62, 53),
(77, 63, 44),
(78, 63, 49),
(79, 63, 52),
(80, 64, 50),
(81, 65, 43),
(82, 65, 44),
(83, 66, 50),
(84, 66, 51),
(85, 66, 52),
(110, 74, 44),
(111, 74, 49),
(112, 74, 51),
(113, 74, 52),
(114, 74, 53),
(115, 74, 54),
(126, 77, 53),
(127, 77, 54),
(206, 102, 66),
(208, 104, 67),
(209, 105, 67),
(210, 106, 67),
(211, 107, 67),
(212, 108, 67),
(213, 109, 67),
(214, 110, 67),
(215, 111, 67),
(216, 112, 67),
(217, 113, 67),
(218, 114, 67),
(219, 115, 67),
(220, 116, 67),
(221, 117, 67),
(223, 119, 67),
(224, 120, 67),
(225, 121, 67),
(226, 122, 67),
(232, 57, 67),
(236, 53, 67),
(239, 118, 64),
(249, 123, 67),
(250, 124, 68),
(251, 125, 68),
(252, 126, 68),
(253, 127, 68);

-- --------------------------------------------------------

--
-- Table structure for table `category_setting`
--

CREATE TABLE `category_setting` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `reason_titles` longtext,
  `reason_descriptions` longtext,
  `feature_titles` longtext,
  `feature_descriptions` longtext,
  `posters` longtext,
  `actual_material_pics` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_setting`
--

INSERT INTO `category_setting` (`id`, `status`, `reason_titles`, `reason_descriptions`, `feature_titles`, `feature_descriptions`, `posters`, `actual_material_pics`, `created_at`, `updated_at`) VALUES
(7, 'on', 'a:1:{i:0;s:8:\"Reason 1\";}', 'a:1:{i:0;s:56:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\";}', 'a:1:{i:0;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}', 'a:1:{i:0;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}', 'a:1:{i:0;s:83:\"upload/category_setting/posters-pics//LPJ92fy7vucGFwChPAsq3QZwYz7O1R09MNy3VzVn.jpeg\";}', 'a:1:{i:0;s:91:\"upload/category_setting/actual-material-pics//WOy5l7DzODy7VcfYZudTNLt1PToJjYKIhWMupyC6.jpeg\";}', '2020-04-11 15:51:39', '2020-04-21 16:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'AL', 'Albania'),
(2, 'DZ', 'Algeria'),
(3, 'DS', 'American Samoa'),
(4, 'AD', 'Andorra'),
(5, 'AO', 'Angola'),
(6, 'AI', 'Anguilla'),
(7, 'AQ', 'Antarctica'),
(8, 'AG', 'Antigua and Barbuda'),
(9, 'AR', 'Argentina'),
(10, 'AM', 'Armenia'),
(11, 'AW', 'Aruba'),
(12, 'AU', 'Australia'),
(13, 'AT', 'Austria'),
(14, 'AZ', 'Azerbaijan'),
(15, 'BS', 'Bahamas'),
(16, 'BH', 'Bahrain'),
(17, 'BD', 'Bangladesh'),
(18, 'BB', 'Barbados'),
(19, 'BY', 'Belarus'),
(20, 'BE', 'Belgium'),
(21, 'BZ', 'Belize'),
(22, 'BJ', 'Benin'),
(23, 'BM', 'Bermuda'),
(24, 'BT', 'Bhutan'),
(25, 'BO', 'Bolivia'),
(26, 'BA', 'Bosnia and Herzegovina'),
(27, 'BW', 'Botswana'),
(28, 'BV', 'Bouvet Island'),
(29, 'BR', 'Brazil'),
(30, 'IO', 'British Indian Ocean Territory'),
(31, 'BN', 'Brunei Darussalam'),
(32, 'BG', 'Bulgaria'),
(33, 'BF', 'Burkina Faso'),
(34, 'BI', 'Burundi'),
(35, 'KH', 'Cambodia'),
(36, 'CM', 'Cameroon'),
(37, 'CA', 'Canada'),
(38, 'CV', 'Cape Verde'),
(39, 'KY', 'Cayman Islands'),
(40, 'CF', 'Central African Republic'),
(41, 'TD', 'Chad'),
(42, 'CL', 'Chile'),
(43, 'CN', 'China'),
(44, 'CX', 'Christmas Island'),
(45, 'CC', 'Cocos (Keeling) Islands'),
(46, 'CO', 'Colombia'),
(47, 'KM', 'Comoros'),
(48, 'CG', 'Congo'),
(49, 'CK', 'Cook Islands'),
(50, 'CR', 'Costa Rica'),
(51, 'HR', 'Croatia (Hrvatska)'),
(52, 'CU', 'Cuba'),
(53, 'CY', 'Cyprus'),
(54, 'CZ', 'Czech Republic'),
(55, 'DK', 'Denmark'),
(56, 'DJ', 'Djibouti'),
(57, 'DM', 'Dominica'),
(58, 'DO', 'Dominican Republic'),
(59, 'TP', 'East Timor'),
(60, 'EC', 'Ecuador'),
(61, 'EG', 'Egypt'),
(62, 'SV', 'El Salvador'),
(63, 'GQ', 'Equatorial Guinea'),
(64, 'ER', 'Eritrea'),
(65, 'EE', 'Estonia'),
(66, 'ET', 'Ethiopia'),
(67, 'FK', 'Falkland Islands (Malvinas)'),
(68, 'FO', 'Faroe Islands'),
(69, 'FJ', 'Fiji'),
(70, 'FI', 'Finland'),
(71, 'FR', 'France'),
(72, 'FX', 'France, Metropolitan'),
(73, 'GF', 'French Guiana'),
(74, 'PF', 'French Polynesia'),
(75, 'TF', 'French Southern Territories'),
(76, 'GA', 'Gabon'),
(77, 'GM', 'Gambia'),
(78, 'GE', 'Georgia'),
(79, 'DE', 'Germany'),
(80, 'GH', 'Ghana'),
(81, 'GI', 'Gibraltar'),
(82, 'GK', 'Guernsey'),
(83, 'GR', 'Greece'),
(84, 'GL', 'Greenland'),
(85, 'GD', 'Grenada'),
(86, 'GP', 'Guadeloupe'),
(87, 'GU', 'Guam'),
(88, 'GT', 'Guatemala'),
(89, 'GN', 'Guinea'),
(90, 'GW', 'Guinea-Bissau'),
(91, 'GY', 'Guyana'),
(92, 'HT', 'Haiti'),
(93, 'HM', 'Heard and Mc Donald Islands'),
(94, 'HN', 'Honduras'),
(95, 'HK', 'Hong Kong'),
(96, 'HU', 'Hungary'),
(97, 'IS', 'Iceland'),
(98, 'IN', 'India'),
(99, 'IM', 'Isle of Man'),
(100, 'ID', 'Indonesia'),
(101, 'IR', 'Iran (Islamic Republic of)'),
(102, 'IQ', 'Iraq'),
(103, 'IE', 'Ireland'),
(104, 'IL', 'Israel'),
(105, 'IT', 'Italy'),
(106, 'CI', 'Ivory Coast'),
(107, 'JE', 'Jersey'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'JO', 'Jordan'),
(111, 'KZ', 'Kazakhstan'),
(112, 'KE', 'Kenya'),
(113, 'KI', 'Kiribati'),
(114, 'KP', 'Korea,Democratic People\'s Republic of'),
(115, 'KR', 'Korea, Republic of'),
(116, 'XK', 'Kosovo'),
(117, 'KW', 'Kuwait'),
(118, 'KG', 'Kyrgyzstan'),
(119, 'LA', 'Lao People\'s Democratic Republic'),
(120, 'LV', 'Latvia'),
(121, 'LB', 'Lebanon'),
(122, 'LS', 'Lesotho'),
(123, 'LR', 'Liberia'),
(124, 'LY', 'Libyan Arab Jamahiriya'),
(125, 'LI', 'Liechtenstein'),
(126, 'LT', 'Lithuania'),
(127, 'LU', 'Luxembourg'),
(128, 'MO', 'Macau'),
(129, 'MK', 'Macedonia'),
(130, 'MG', 'Madagascar'),
(131, 'MW', 'Malawi'),
(132, 'MY', 'Malaysia'),
(133, 'MV', 'Maldives'),
(134, 'ML', 'Mali'),
(135, 'MT', 'Malta'),
(136, 'MH', 'Marshall Islands'),
(137, 'MQ', 'Martinique'),
(138, 'MR', 'Mauritania'),
(139, 'MU', 'Mauritius'),
(140, 'TY', 'Mayotte'),
(141, 'MX', 'Mexico'),
(142, 'FM', 'Micronesia, Federated States of'),
(143, 'MD', 'Moldova, Republic of'),
(144, 'MC', 'Monaco'),
(145, 'MN', 'Mongolia'),
(146, 'ME', 'Montenegro'),
(147, 'MS', 'Montserrat'),
(148, 'MA', 'Morocco'),
(149, 'MZ', 'Mozambique'),
(150, 'MM', 'Myanmar'),
(151, 'NA', 'Namibia'),
(152, 'NR', 'Nauru'),
(153, 'NP', 'Nepal'),
(154, 'NL', 'Netherlands'),
(155, 'AN', 'Netherlands Antilles'),
(156, 'NC', 'New Caledonia'),
(157, 'NZ', 'New Zealand'),
(158, 'NI', 'Nicaragua'),
(159, 'NE', 'Niger'),
(160, 'NG', 'Nigeria'),
(161, 'NU', 'Niue'),
(162, 'NF', 'Norfolk Island'),
(163, 'MP', 'Northern Mariana Islands'),
(164, 'NO', 'Norway'),
(165, 'OM', 'Oman'),
(166, 'PK', 'Pakistan'),
(167, 'PW', 'Palau'),
(168, 'PS', 'Palestine'),
(169, 'PA', 'Panama'),
(170, 'PG', 'Papua New Guinea'),
(171, 'PY', 'Paraguay'),
(172, 'PE', 'Peru'),
(173, 'PH', 'Philippines'),
(174, 'PN', 'Pitcairn'),
(175, 'PL', 'Poland'),
(176, 'PT', 'Portugal'),
(177, 'PR', 'Puerto Rico'),
(178, 'QA', 'Qatar'),
(179, 'RE', 'Reunion'),
(180, 'RO', 'Romania'),
(181, 'RU', 'Russian Federation'),
(182, 'RW', 'Rwanda'),
(183, 'KN', 'Saint Kitts and Nevis'),
(184, 'LC', 'Saint Lucia'),
(185, 'VC', 'Saint Vincent and the Grenadines'),
(186, 'WS', 'Samoa'),
(187, 'SM', 'San Marino'),
(188, 'ST', 'Sao Tome and Principe'),
(189, 'SA', 'Saudi Arabia'),
(190, 'SN', 'Senegal'),
(191, 'RS', 'Serbia'),
(192, 'SC', 'Seychelles'),
(193, 'SL', 'Sierra Leone'),
(194, 'SG', 'Singapore'),
(195, 'SK', 'Slovakia'),
(196, 'SI', 'Slovenia'),
(197, 'SB', 'Solomon Islands'),
(198, 'SO', 'Somalia'),
(199, 'ZA', 'South Africa'),
(200, 'GS', 'South Georgia South Sandwich Islands'),
(201, 'SS', 'South Sudan'),
(202, 'ES', 'Spain'),
(203, 'LK', 'Sri Lanka'),
(204, 'SH', 'St. Helena'),
(205, 'PM', 'St. Pierre and Miquelon'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen Islands'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TG', 'Togo'),
(218, 'TK', 'Tokelau'),
(219, 'TO', 'Tonga'),
(220, 'TT', 'Trinidad and Tobago'),
(221, 'TN', 'Tunisia'),
(222, 'TR', 'Turkey'),
(223, 'TM', 'Turkmenistan'),
(224, 'TC', 'Turks and Caicos Islands'),
(225, 'TV', 'Tuvalu'),
(226, 'UG', 'Uganda'),
(227, 'UA', 'Ukraine'),
(228, 'AE', 'United Arab Emirates'),
(229, 'GB', 'United Kingdom'),
(230, 'US', 'United States'),
(231, 'UM', 'United States minor outlying islands'),
(232, 'UY', 'Uruguay'),
(233, 'UZ', 'Uzbekistan'),
(234, 'VU', 'Vanuatu'),
(235, 'VA', 'Vatican City State'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Vietnam'),
(238, 'VG', 'Virgin Islands (British)'),
(239, 'VI', 'Virgin Islands (U.S.)'),
(240, 'WF', 'Wallis and Futuna Islands'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'ZR', 'Zaire'),
(244, 'ZM', 'Zambia'),
(245, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `max_uses` int(200) NOT NULL,
  `max_uses_user` int(200) NOT NULL,
  `discount` int(200) DEFAULT NULL,
  `status` int(200) NOT NULL,
  `freedelivery` int(200) DEFAULT NULL,
  `exclude_category` longtext,
  `exclude_product` longtext,
  `start_date` date DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `startandexpire` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_product`
--

CREATE TABLE `coupon_product` (
  `id` int(11) NOT NULL,
  `product_id` int(20) NOT NULL,
  `coupon_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_user`
--

CREATE TABLE `coupon_user` (
  `id` int(11) NOT NULL,
  `user_id` int(20) NOT NULL,
  `coupon_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `code` varchar(200) NOT NULL,
  `symbol` varchar(200) NOT NULL,
  `exchange_rate` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reviews`
--

CREATE TABLE `customer_reviews` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` longtext,
  `rating` varchar(255) DEFAULT NULL,
  `detail` longtext,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_reviews`
--

INSERT INTO `customer_reviews` (`id`, `name`, `image`, `rating`, `detail`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Jack', 'upload/customer-reviews/1/5KCaWKDoe7faxomPao2p4FkzQ5BQ9ivBYcPUf4mx.jpeg', NULL, '<p>I\'m going to do more business with them very soon. FANTASTCI SERVICE! AN<br></p>', 'on', '2020-04-30 18:01:03', '2020-04-30 18:12:14');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'on',
  `address` longtext,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `image` longtext,
  `timing` varchar(255) DEFAULT NULL,
  `details` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `status`, `address`, `state`, `city`, `country`, `image`, `timing`, `details`, `created_at`, `updated_at`) VALUES
(1, 'Art Center Manatee', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'West Virgina', 'Huntington', 'United States', 'upload/events/1/wIbxTZTiNDuWciRqZXJ4jdMq6Abr9P6jNfTTfa8j.jpeg', '05/30/2020 12:00 PM', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel felis dictum, efficitur nulla ac, luctus ipsum. Ut placerat sapien nec orci dictum, sit amet accumsan nisi tristique.', '2020-04-30 14:13:43', '2020-04-30 15:09:05'),
(2, 'Otto Hubbard', 'on', 'Excepteur porro ab r', 'Bavaria', 'Munich', 'Germany', 'upload/events/2/o5w96Iw6wXDBau7DoPbopPh0KE3FCLisgrFg90HO.jpeg', '05/25/2020 1:11 PM', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</span>', '2020-04-30 15:12:44', '2020-04-30 16:55:51'),
(3, 'Art Center Venice', 'on', 'Excepteur porro ab r', NULL, 'Venice', 'Italy', 'upload/events/3/EeE5Ha2pQsgCeIerWmInreFUzotG2ouRxwV10M6s.jpeg', '06/18/2020 1:16 PM', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</span><br></p>', '2020-04-30 15:16:40', '2020-04-30 16:55:53'),
(4, 'Exhibition Of Art Culture', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 'London', 'United Kingdom', 'upload/events/4/zPXg38Ps3ue9QyWlWJeohNsvsaYAsQuB2AoYgDh9.jpeg', '06/27/2020 1:17 PM', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel felis dictum, efficitur nulla ac.</span><br></p>', '2020-04-30 15:19:11', '2020-04-30 16:55:57');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` longtext,
  `answer` longtext,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(1, '<div>What Is Torben Goldmund’s Art ‘N’</div>', '<div style=\"text-align: justify;\"><font face=\"Open Sans, Arial, sans-serif\"><span style=\"font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris molestie ornare laoreet. Nam sed mattis ante. Nam ullamcorper molestie pellentesque. Pellentesque finibus metus a nulla volutpat, eget bibendum ligula aliquet. Duis consectetur ut justo sit amet dignissim. Sed laoreet mauris sed nulla finibus hendrerit. Proin semper vestibulum maximus. Curabitur maximus nunc metus, sed bibendum dui lacinia sit amet. Quisque porta, elit eget facilisis faucibus, nulla nulla ultrices odio, ut elementum metus nisl et massa. Donec pharetra, arcu dignissim varius bibendum, elit turpis porta odio, vitae dapibus augue nulla nec nibh. Praesent hendrerit lacus neque, sed pellentesque enim ullamcorper vitae. Interdum et malesuada fames ac ante ipsum primis in faucibus.</span></font></div>', 1, '2020-02-27 16:28:17', '2020-03-25 16:45:49'),
(13, 'dsadsdasasdas', 'dasdasdasdasdas', 1, '2020-04-20 14:38:33', '2020-04-20 14:38:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_02_05_102643_create_permission_tables', 1),
(4, '2020_02_09_203836_create_verify_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(3, 'App\\User', 2),
(3, 'App\\User', 3),
(3, 'App\\User', 4),
(3, 'App\\User', 5),
(3, 'App\\User', 6),
(3, 'App\\User', 7),
(3, 'App\\User', 8),
(3, 'App\\User', 9),
(3, 'App\\User', 10),
(3, 'App\\User', 11),
(3, 'App\\User', 12),
(3, 'App\\User', 13),
(3, 'App\\User', 14),
(3, 'App\\User', 15),
(3, 'App\\User', 16),
(3, 'App\\User', 17),
(3, 'App\\User', 18),
(3, 'App\\User', 19),
(3, 'App\\User', 20),
(3, 'App\\User', 21);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `order_details` longtext NOT NULL,
  `product_detail` longtext NOT NULL,
  `product_extra_detail` longtext,
  `user_meta` longtext NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subtotal` varchar(200) NOT NULL,
  `shipping` varchar(200) DEFAULT NULL,
  `discount` varchar(200) DEFAULT NULL,
  `tax` varchar(200) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `user_email`, `order_details`, `product_detail`, `product_extra_detail`, `user_meta`, `user_id`, `subtotal`, `shipping`, `discount`, `tax`, `status`, `created_at`, `updated_at`) VALUES
(18, 'XmljHy1TW7', 'ahsan.amin334@gmail.com', 'a:14:{s:6:\"_token\";s:40:\"UXGOw6blOdn15xx2ue5gyMTBsY2AADxMFJjG9o5B\";s:4:\"name\";N;s:5:\"email\";N;s:7:\"address\";N;s:6:\"mobile\";N;s:5:\"pcode\";N;s:4:\"city\";N;s:9:\"d-country\";s:14:\"Åland Islands\";s:13:\"other-address\";N;s:10:\"other-city\";N;s:11:\"other-pcode\";N;s:8:\"shipping\";N;s:7:\"payment\";s:6:\"paypal\";s:13:\"other-country\";s:14:\"Åland Islands\";}', 'a:2:{i:0;s:327:\"a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";i:1;s:300:\"a:11:{s:2:\"id\";i:87;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"XYZ\";s:4:\"slug\";s:3:\"xyz\";s:11:\"description\";s:3:\"XYZ\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/xyz/feature/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg\";s:5:\"video\";s:4:\"null\";s:5:\"price\";i:20;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";}', 'a:2:{i:0;s:99:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";i:1;s:98:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";}', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', 1, '30', '', '', '', 'Confirm', '2020-04-11 10:30:02', '2020-04-13 15:51:45'),
(19, 'RULso3uyK5', 'tahseen@gmail.com', 'a:14:{s:6:\"_token\";s:40:\"UXGOw6blOdn15xx2ue5gyMTBsY2AADxMFJjG9o5B\";s:4:\"name\";N;s:5:\"email\";N;s:7:\"address\";N;s:6:\"mobile\";N;s:5:\"pcode\";N;s:4:\"city\";N;s:9:\"d-country\";s:14:\"Åland Islands\";s:13:\"other-address\";N;s:10:\"other-city\";N;s:11:\"other-pcode\";N;s:8:\"shipping\";s:3:\"dhl\";s:7:\"payment\";s:6:\"paypal\";s:13:\"other-country\";s:14:\"Åland Islands\";}', 'a:3:{i:0;s:327:\"a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";i:1;s:327:\"a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";i:2;s:300:\"a:11:{s:2:\"id\";i:87;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"XYZ\";s:4:\"slug\";s:3:\"xyz\";s:11:\"description\";s:3:\"XYZ\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/xyz/feature/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg\";s:5:\"video\";s:4:\"null\";s:5:\"price\";i:20;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";}', 'a:3:{i:0;s:99:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";i:1;s:98:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:2:\"No\";s:8:\"quantity\";s:1:\"1\";}\";i:2;s:210:\"a:2:{i:0;a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}i:1;a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}}\";}', '{\"id\":2,\"name\":\"ahsan\",\"email\":\"ahsan.amin334@gmail.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 12:30:34\",\"updated_at\":\"2020-02-05 12:30:34\"}', 2, '60', '', '', '', 'Confirm', '2020-04-12 14:53:04', '2020-04-13 16:48:17');

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

CREATE TABLE `order_history` (
  `id` int(11) NOT NULL,
  `content` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  `product_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_meta` longtext,
  `image` longtext,
  `user_meta` longtext,
  `price` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `material` varchar(200) DEFAULT NULL,
  `size` varchar(200) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  `extra_detail` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_meta`
--

CREATE TABLE `order_meta` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `name`) VALUES
(1, 'processing'),
(2, 'hold');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('qwerty@gmail.com', '$2y$10$6B0tWaLL4uyCoVnzzcBSLuVuV1jvB.NM33OYyhFPue9L5eDPeCxpW', '2020-03-01 12:34:10');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productimgtemp`
--

CREATE TABLE `productimgtemp` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` longtext,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4,
  `sku` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `image` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `video` longtext COLLATE utf8_unicode_ci,
  `price` int(200) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `title`, `slug`, `description`, `sku`, `image`, `video`, `price`, `stock`, `status`, `created_at`, `updated_at`) VALUES
(53, 1, 'sdsada', 'sdsada', NULL, NULL, 'upload/product/sdsada/feature/7d5L1j9VVDz6UQxPOl49nbqia8KTv4DnIAP4VGa9.jpeg', 'www.youtube.com/embed/z15Akk6Z4OY', 123, 1, 1, '2020-02-23 06:30:13', '2020-04-18 10:51:40'),
(55, 1, 'testing product', 'testing-produt', 'dfdfsdfsdfsd', 'RJHE9-44', 'upload/product/testing-produt/feature/w0zg7gj3LcvoolnlKTaj26T4pQVoUDcw2nE68K7p.jpeg', 'null', 332, 1, 1, '2020-02-25 14:50:29', '2020-03-30 17:43:38'),
(56, 1, 'Brandon Jimenez', 'brandon-jimenez', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>', NULL, 'upload/product/brandon-jimenez/feature/0lvRIbb9m6CYjuNcgDnmjI30qUFCirz3EzGmS8cF.jpeg', 'null', 323, 1, 1, '2020-03-03 15:14:45', '2020-03-21 03:05:34'),
(57, 1, 'Beau Holloway', 'beau-holloway', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>', NULL, 'upload/product/beau-holloway/feature/mlb5da9t2eoQbsJIk0m4w8nBMu2YSPg8IoTIvRAI.jpeg', 'https://gitlab.com/yasin.sabir/erp/-/tree/master/app/Http/Controllers', 356, 1, 1, '2020-03-03 15:16:06', '2020-04-04 07:11:38'),
(58, 1, 'Nomlanga Cleveland', 'nomlanga-cleveland', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>', NULL, 'upload/product/nomlanga-cleveland/feature/flhitiexBq7XwqBcewRfhX768lJ3INfa7EpMZQmS.jpeg', NULL, 549, 1, 1, '2020-03-03 15:17:16', '2020-03-03 15:17:16'),
(59, 1, 'Ifeoma Davis', 'ifeoma-davis', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>', NULL, 'upload/product/ifeoma-davis/feature/gUYq7dG6GNwBmk2ZxVMXAYl9RVpVBcrTYzJcYEAH.jpeg', NULL, 252, 1, 1, '2020-03-03 15:34:43', '2020-03-03 15:34:43'),
(60, 1, 'Tobias Riley', 'tobias-riley', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>', NULL, 'upload/product/tobias-riley/feature/Da0r21JrtcdeJb59UCsYNIvtrSOAjEoyeJWdMvcP.jpeg', NULL, 943, 1, 1, '2020-03-03 15:35:22', '2020-03-03 15:35:22'),
(61, 1, 'Brandon Sheppard', 'brandon-sheppard', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>', NULL, 'upload/product/brandon-sheppard/feature/Dz8XYrQ47zqSPa7LuelmuzdGR1GYJ7MTeAgWIcQS.jpeg', NULL, 537, 1, 1, '2020-03-03 15:36:19', '2020-03-03 15:36:19'),
(62, 1, 'Bradley Head', 'bradley-head', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>', NULL, 'upload/product/bradley-head/feature/VrONxJKgzeq7aCREr3luvkQgkhvU7p2KD6SrVgjO.jpeg', NULL, 662, 1, 1, '2020-03-03 15:37:14', '2020-03-03 15:37:14'),
(63, 1, 'Nicholas Drake', 'nicholas-drake', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>', NULL, 'upload/product/nicholas-drake/feature/A4uRzpklXN3AWhm5lRB1GsHCIQmR9Wc8xk9U2Stq.jpeg', NULL, 168, 1, 1, '2020-03-03 15:38:33', '2020-03-03 15:38:33'),
(64, 1, 'dummy 0987', 'dummy-0987', 'asdadasdasdsdasdasdasd', 'sde-432333', 'upload/product/dummy-0987/feature/whxl7DnWQqdUHdf63zHjKAE1Jcv1746Nm18VLmeX.jpeg', NULL, 123, 1, 1, '2020-03-09 15:19:49', '2020-03-09 15:19:49'),
(65, 1, 'asd-001', 'asd-001', '<span style=\"font-family: roboto; font-size: 14px; background-color: rgb(255, 255, 255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas</span>', 'sde-4432', 'upload/product/asd-001/feature/vAvsiHHkcfyj2Sau3ssZidUFAGwBtSqf0q1xeYzq.png', NULL, 120, 10, 1, '2020-03-09 15:49:08', '2020-03-09 16:46:57'),
(66, 1, 'Felix Carter', 'felix-carter', '<div style=\"text-align: left;\"><span style=\"text-align: justify;\">L</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">orem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel diam convallis, suscipit arcu et, pretium mauris. Etiam a tortor porttitor, gravida nulla in, hendrerit turpis. Quisque non scelerisque nunc. Pellentesque dictum efficitur sagittis. Nulla eget pulvinar sapien. Morbi id placerat lectus. Praesent non ipsum eu erat efficitur fringilla vel ut risus. Praesent pellentesque purus a leo eleifend pulvinar. Fusce commodo ligula lorem</span><span style=\"font-size: 1rem;\">ue nostrum .</span></div>', NULL, 'upload/product/felix-carter/feature/UOqdBgW3w404QBQvGpK1IeBPgFap3E279LP31x6I.jpeg', NULL, 857, 1, 1, '2020-03-09 16:52:06', '2020-03-09 16:52:06'),
(74, 1, 'Jack Chambers', 'jack-chambers', NULL, 'Ratione eiusmod veli', 'upload/product/jack-chambers/feature/fzyPGwqgFnetPwuo1FPhwn3wBdTku0bYQXZCmVf6.jpeg', 'null', 625, 121, 0, '2020-03-15 14:11:31', '2020-03-15 14:11:31'),
(77, 1, 'Oren Head', 'oren-head', NULL, 'Qui dicta distinctio', 'upload/product/oren-head/feature/L9lLSnIoXVdEtVizU5OTLT7dBnjthDSn2HRZ9RBY.jpeg', 'null', 387, 323, 1, '2020-03-15 14:11:31', '2020-03-15 14:11:31'),
(102, 1, '323518-P9MHS8-840', '323518-p9mhs8-840', NULL, NULL, 'upload/product/323518-p9mhs8-840/feature/mpeTfnFhQGwVtzekxcvmeLuUxoVuKqb7axteB11t.jpeg', NULL, 56, NULL, 1, '2020-03-28 08:05:25', '2020-03-28 08:05:25'),
(104, 1, 'Product-3', 'product-3', NULL, NULL, 'upload/product/product-3/feature/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png', NULL, 59, NULL, 1, '2020-03-28 11:28:34', '2020-03-28 11:28:34'),
(105, 1, 'Product-4', 'product-4', NULL, NULL, 'upload/product/product-4/feature/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:34', '2020-03-28 11:28:34'),
(106, 1, 'Product-5', 'product-5', NULL, NULL, 'upload/product/product-5/feature/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:34', '2020-03-28 11:28:34'),
(107, 1, 'Product-6', 'product-6', NULL, NULL, 'upload/product/product-6/feature/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(108, 1, 'Product-7', 'product-7', NULL, NULL, 'upload/product/product-7/feature/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(109, 1, 'Product-8', 'product-8', NULL, NULL, 'upload/product/product-8/feature/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(110, 1, 'Product-9', 'product-9', NULL, NULL, 'upload/product/product-9/feature/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(111, 1, 'Product-10', 'product-10', NULL, NULL, 'upload/product/product-10/feature/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(112, 1, 'Product-11', 'product-11', NULL, NULL, 'upload/product/product-11/feature/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(113, 1, 'Product-13', 'product-13', NULL, NULL, 'upload/product/product-13/feature/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(114, 1, 'Product-14', 'product-14', NULL, NULL, 'upload/product/product-14/feature/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(115, 1, 'Product-15', 'product-15', NULL, NULL, 'upload/product/product-15/feature/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(116, 1, 'Product-16', 'product-16', NULL, NULL, 'upload/product/product-16/feature/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:35', '2020-03-28 11:28:35'),
(117, 1, 'Product-17fdf', 'product-17', NULL, NULL, 'upload/product/product-17/feature/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg', 'null', 59, NULL, 1, '2020-03-28 11:28:36', '2020-03-28 12:08:30'),
(118, 1, 'Product-18', 'product-18', NULL, NULL, 'upload/product/product-18/feature/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg', 'https://gitlab.com/yasin.sabir/erp/-/blob/master/app/Http/Controllers/AccountController.php', 59, NULL, 1, '2020-03-28 11:28:36', '2020-04-18 15:14:24'),
(119, 1, 'Product-19', 'product-19', NULL, NULL, 'upload/product/product-19/feature/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:36', '2020-03-28 11:28:36'),
(120, 1, 'Product-20', 'product-20', NULL, NULL, 'upload/product/product-20/feature/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:36', '2020-03-28 11:28:36'),
(121, 1, 'Product-21', 'product-21', NULL, NULL, 'upload/product/product-21/feature/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:36', '2020-03-28 11:28:36'),
(122, 1, 'Product-22', 'product-22', NULL, NULL, 'upload/product/product-22/feature/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg', NULL, 59, NULL, 1, '2020-03-28 11:28:36', '2020-03-28 11:28:36'),
(123, 1, 'qwerty098 7', 'qwerty098', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc semper, nisl id ultrices venenatis, diam odio posuere nulla, non consectetur arcu quam vel lorem. Nunc ornare diam in ligula porta, nec fermentum arcu vestibulum. Aliquam risus nisi, fringilla nec rutrum nec, placerat a elit.098 7</span><br></p>', NULL, 'upload/product/qwerty098/feature/4VPxihSjcrQnnyAgWhebxD4KcPxqP9VlijOzUdlp.jpeg', 'https://gitlab.com/yasin.sabir/erp/-/tree/master/app/Http/Controllers', 777, NULL, 1, '2020-04-04 07:07:36', '2020-04-04 07:08:50'),
(126, 1, 'Galactic Atoms', 'galactic-atoms', NULL, NULL, 'upload/product/galactic-atoms/feature/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png', NULL, 68, NULL, 1, '2020-04-22 15:28:14', '2020-04-22 15:28:14'),
(127, 1, 'Space 1', 'space-1', NULL, NULL, 'upload/product/space-1/feature/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg', NULL, 68, NULL, 1, '2020-04-22 15:28:14', '2020-04-22 15:28:14');

-- --------------------------------------------------------

--
-- Table structure for table `products_galleries`
--

CREATE TABLE `products_galleries` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image` longtext CHARACTER SET utf8mb4
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_galleries`
--

INSERT INTO `products_galleries` (`id`, `product_id`, `image`) VALUES
(52, 25, 'upload/product/product-001/gallery/zeuRYcbhmcxgZ4ce9HS17eIIXNkE7wehb4FmHBpk.png'),
(53, 25, 'upload/product/product-001/gallery/d1QFTuWCYy6Y2kU1DhBfzDcf6FsemI5qxhvyxgz9.png'),
(54, 26, 'upload/product/dfsdfsfadfdf/gallery/Tq30RDu9wm6NvWBsH8kFTRPa2zeFbvrCWchhhoiX.png'),
(55, 26, 'upload/product/dfsdfsfadfdf/gallery/YSHiyo10491nVgFQ3OMXgeA3fTNNjLwUFwSuaiDq.png'),
(85, 53, 'upload/product/sdsada/gallery/EFusDPkyDf0wAZtv07zNT54o0uT87YS6opmkq0lU.jpeg'),
(89, 55, 'upload/product/testing-produt/gallery/V3wi4gt9LQX7ptsZbAMqZMZJvsAWRw5nsnkMgAcF.png'),
(90, 55, 'upload/product/testing-produt/gallery/V3wi4gt9LQX7ptsZbAMqZMZJvsAWRw5nsnkMgAcF.png'),
(91, 56, 'upload/product/brandon-jimenez/gallery/GZXAnfWbQFASzkQO3MzpPphm7tfeHeeum21uYSeC.jpeg'),
(92, 57, 'upload/product/beau-holloway/gallery/d06IxHWr2fIF8Krj7b2zFJCGGTy2S3ndJ6vpbK6Z.png'),
(93, 57, 'upload/product/beau-holloway/gallery/d06IxHWr2fIF8Krj7b2zFJCGGTy2S3ndJ6vpbK6Z.png'),
(94, 57, 'upload/product/beau-holloway/gallery/d06IxHWr2fIF8Krj7b2zFJCGGTy2S3ndJ6vpbK6Z.png'),
(95, 58, 'upload/product/nomlanga-cleveland/gallery/0l1PfkVCwkgTnVaPnmoo0Aj6QREZ9JJ1owZfZ8bK.png'),
(96, 58, 'upload/product/nomlanga-cleveland/gallery/m9fMgzMSBrB5wwbIqvkDTvOTUqJRJTO06iLWSPSg.png'),
(97, 59, 'upload/product/ifeoma-davis/gallery/WsYw9ujnyukIoH61HXiJfHR9wEAHGZFj1NWaXa25.jpeg'),
(98, 59, 'upload/product/ifeoma-davis/gallery/XRTcdeOBlrmuJ4bO0GfOdu5tZdxtaSs6iUQoTs1i.jpeg'),
(99, 60, 'upload/product/tobias-riley/gallery/g8XczXYEysnESpekqa9JUI3GWdz4EJakV3YicheE.jpeg'),
(100, 60, 'upload/product/tobias-riley/gallery/QsVvHkUmWpIGRH94PDMJSNQc0sK5RbhzXWbaAftS.jpeg'),
(101, 61, 'upload/product/brandon-sheppard/gallery/Ffi8pdzKEhJBS878iOnJKKDDHPGCjfFdk3x1d2on.jpeg'),
(102, 61, 'upload/product/brandon-sheppard/gallery/Jpx2DSjVyJQxBNUbqQDFhtdiw6N3FqV1hzijKvt8.jpeg'),
(103, 62, 'upload/product/bradley-head/gallery/UMwi36zPj38tAgB3peuVvCMhIP4hyIufkavqo71T.jpeg'),
(104, 62, 'upload/product/bradley-head/gallery/p6oIArXzEqv8qxTEoWJj2BhGOEuEb0jjNJjYOl1L.jpeg'),
(105, 63, 'upload/product/nicholas-drake/gallery/wMLlmKMMrEs45Zgvwy8svKizwskHccuQb6hiGYfL.jpeg'),
(106, 63, 'upload/product/nicholas-drake/gallery/WkE1qZ6JvDd3RPR5gis03m63ZRkVTtx7GpNUxqib.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `products_metas`
--

CREATE TABLE `products_metas` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_meta` longtext CHARACTER SET utf8mb4 NOT NULL,
  `product_meta_value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_metas`
--

INSERT INTO `products_metas` (`id`, `product_id`, `product_meta`, `product_meta_value`) VALUES
(300, 53, 'slug', 'sdsada'),
(301, 53, 'price', '123'),
(302, 53, 'sale_price', '122'),
(303, 53, 'sale_start_date', '02/23/2020 '),
(304, 53, 'sale_end_date', ' 02/23/2020'),
(305, 53, 'sku', NULL),
(306, 53, 'tags', 'a:1:{i:0;s:1:\"6\";}'),
(307, 53, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(308, 53, 'featured_image', 'upload/product/sdsada/feature/7d5L1j9VVDz6UQxPOl49nbqia8KTv4DnIAP4VGa9.jpeg'),
(309, 53, 'stock', '1'),
(310, 53, 'stock_manage_chk', NULL),
(311, 53, 'stock_threshold', NULL),
(312, 53, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"6oys8MhyglXRxqlYhJfAirpokU1GDjBUua0mQp4T\";s:12:\"product_name\";s:6:\"sdsada\";s:11:\"description\";N;s:28:\"product_features_description\";N;s:13:\"regular_price\";s:3:\"123\";s:11:\"sale_prices\";s:3:\"122\";s:16:\"sales_price_schd\";s:23:\"02/23/2020 - 02/23/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:1:\"1\";s:15:\"stock_threshold\";N;s:16:\"product_material\";s:34:\"qwerty,helloworld,good_bye,gummies\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:1:{i:0;s:1:\"6\";}}'),
(326, 55, 'slug', 'testing-product'),
(327, 55, 'price', '332'),
(328, 55, 'sale_price', '33'),
(329, 55, 'sale_start_date', '02/26/2020 '),
(330, 55, 'sale_end_date', ' 02/26/2020'),
(331, 55, 'sku', 'RJHE9-44'),
(332, 55, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:1:\"9\";}'),
(333, 55, 'categories', 'N;'),
(334, 55, 'featured_image', 'upload/product/testing-produt/feature/w0zg7gj3LcvoolnlKTaj26T4pQVoUDcw2nE68K7p.jpeg'),
(335, 55, 'stock', '1'),
(336, 55, 'stock_manage_chk', NULL),
(337, 55, 'stock_threshold', NULL),
(338, 55, 'serialize_data', 'a:15:{s:6:\"_token\";s:40:\"YGjfEnZmJpYdbVA2KcOoKYwVZUJf23bfoWPVxpTJ\";s:12:\"product_name\";s:15:\"testing product\";s:11:\"description\";s:12:\"dfdfsdfsdfsd\";s:28:\"product_features_description\";N;s:13:\"regular_price\";s:3:\"332\";s:11:\"sale_prices\";s:2:\"33\";s:16:\"sales_price_schd\";s:23:\"02/26/2020 - 02/26/2020\";s:11:\"product_sku\";s:8:\"RJHE9-44\";s:14:\"stock_quantity\";s:1:\"1\";s:15:\"stock_threshold\";N;s:16:\"product_material\";N;s:13:\"product_sizes\";N;s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:4:\"tags\";a:3:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:1:\"9\";}}'),
(339, 56, 'slug', 'brandon-jimenez'),
(340, 56, 'price', '323'),
(341, 56, 'sale_price', '230'),
(342, 56, 'sale_start_date', '03/04/2020 '),
(343, 56, 'sale_end_date', ' 03/04/2020'),
(344, 56, 'sku', NULL),
(345, 56, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"9\";}'),
(346, 56, 'categories', 'a:4:{i:0;s:2:\"43\";i:1;s:2:\"50\";i:2;s:2:\"52\";i:3;s:2:\"53\";}'),
(347, 56, 'featured_image', 'upload/product/brandon-jimenez/feature/0lvRIbb9m6CYjuNcgDnmjI30qUFCirz3EzGmS8cF.jpeg'),
(348, 56, 'stock', '1'),
(349, 56, 'stock_manage_chk', NULL),
(350, 56, 'stock_threshold', NULL),
(351, 56, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"huz2KGEWdTkMZ0kgUkwZKBCuRXwfA3mOR76nEy1X\";s:12:\"product_name\";s:15:\"Brandon Jimenez\";s:11:\"description\";s:498:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>\";s:13:\"regular_price\";s:3:\"323\";s:11:\"sale_prices\";s:3:\"230\";s:16:\"sales_price_schd\";s:23:\"03/04/2020 - 03/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:1:\"1\";s:15:\"stock_threshold\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:4:{i:0;s:2:\"43\";i:1;s:2:\"50\";i:2;s:2:\"52\";i:3;s:2:\"53\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"9\";}}'),
(352, 57, 'slug', 'beau-holloway'),
(353, 57, 'price', '356'),
(354, 57, 'sale_price', '200'),
(355, 57, 'sale_start_date', '03/04/2020 '),
(356, 57, 'sale_end_date', ' 03/04/2020'),
(357, 57, 'sku', NULL),
(358, 57, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"7\";}'),
(359, 57, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(360, 57, 'featured_image', 'upload/product/beau-holloway/feature/mlb5da9t2eoQbsJIk0m4w8nBMu2YSPg8IoTIvRAI.jpeg'),
(361, 57, 'stock', '1'),
(362, 57, 'stock_manage_chk', NULL),
(363, 57, 'stock_threshold', NULL),
(364, 57, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"eLF2KKTv6LngGibibXJWvxIBdVstG9IQhE3ghIMl\";s:12:\"product_name\";s:13:\"Beau Holloway\";s:11:\"description\";s:498:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>\";s:28:\"product_features_description\";N;s:13:\"regular_price\";s:3:\"356\";s:11:\"sale_prices\";s:3:\"200\";s:16:\"sales_price_schd\";s:23:\"03/04/2020 - 03/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:1:\"1\";s:15:\"stock_threshold\";N;s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";s:677:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc semper, nisl id ultrices venenatis, diam odio posuere nulla, non consectetur arcu quam vel lorem. Nunc ornare diam in ligula porta, nec fermentum arcu vestibulum. Aliquam risus nisi, fringilla nec rutrum nec, placerat a elit.</span>\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:2:{i:0;s:1:\"6\";i:1;s:1:\"7\";}}'),
(365, 58, 'slug', 'nomlanga-cleveland'),
(366, 58, 'price', '549'),
(367, 58, 'sale_price', '300'),
(368, 58, 'sale_start_date', '03/04/2020 '),
(369, 58, 'sale_end_date', ' 03/04/2020'),
(370, 58, 'sku', NULL),
(371, 58, 'tags', 'a:3:{i:0;s:1:\"7\";i:1;s:1:\"8\";i:2;s:1:\"9\";}'),
(372, 58, 'categories', 'a:3:{i:0;s:2:\"43\";i:1;s:2:\"50\";i:2;s:2:\"52\";}'),
(373, 58, 'featured_image', 'upload/product/nomlanga-cleveland/feature/flhitiexBq7XwqBcewRfhX768lJ3INfa7EpMZQmS.jpeg'),
(374, 58, 'stock', '1'),
(375, 58, 'stock_manage_chk', NULL),
(376, 58, 'stock_threshold', 'no'),
(377, 58, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"huz2KGEWdTkMZ0kgUkwZKBCuRXwfA3mOR76nEy1X\";s:12:\"product_name\";s:18:\"Nomlanga Cleveland\";s:11:\"description\";s:498:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>\";s:13:\"regular_price\";s:3:\"549\";s:11:\"sale_prices\";s:3:\"300\";s:16:\"sales_price_schd\";s:23:\"03/04/2020 - 03/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:3:{i:0;s:2:\"43\";i:1;s:2:\"50\";i:2;s:2:\"52\";}s:4:\"tags\";a:3:{i:0;s:1:\"7\";i:1;s:1:\"8\";i:2;s:1:\"9\";}}'),
(378, 59, 'slug', 'ifeoma-davis'),
(379, 59, 'price', '252'),
(380, 59, 'sale_price', '150'),
(381, 59, 'sale_start_date', '03/04/2020 '),
(382, 59, 'sale_end_date', ' 03/04/2020'),
(383, 59, 'sku', NULL),
(384, 59, 'tags', 'a:1:{i:0;s:1:\"9\";}'),
(385, 59, 'categories', 'a:6:{i:0;s:2:\"43\";i:1;s:2:\"44\";i:2;s:2:\"49\";i:3;s:2:\"50\";i:4;s:2:\"52\";i:5;s:2:\"53\";}'),
(386, 59, 'featured_image', 'upload/product/ifeoma-davis/feature/gUYq7dG6GNwBmk2ZxVMXAYl9RVpVBcrTYzJcYEAH.jpeg'),
(387, 59, 'stock', '1'),
(388, 59, 'stock_manage_chk', NULL),
(389, 59, 'stock_threshold', 'no'),
(390, 59, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"huz2KGEWdTkMZ0kgUkwZKBCuRXwfA3mOR76nEy1X\";s:12:\"product_name\";s:12:\"Ifeoma Davis\";s:11:\"description\";s:498:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>\";s:13:\"regular_price\";s:3:\"252\";s:11:\"sale_prices\";s:3:\"150\";s:16:\"sales_price_schd\";s:23:\"03/04/2020 - 03/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:6:{i:0;s:2:\"43\";i:1;s:2:\"44\";i:2;s:2:\"49\";i:3;s:2:\"50\";i:4;s:2:\"52\";i:5;s:2:\"53\";}s:4:\"tags\";a:1:{i:0;s:1:\"9\";}}'),
(391, 60, 'slug', 'tobias-riley'),
(392, 60, 'price', '943'),
(393, 60, 'sale_price', '300'),
(394, 60, 'sale_start_date', '03/04/2020 '),
(395, 60, 'sale_end_date', ' 03/04/2020'),
(396, 60, 'sku', NULL),
(397, 60, 'tags', 'a:1:{i:0;s:1:\"8\";}'),
(398, 60, 'categories', 'a:4:{i:0;s:2:\"43\";i:1;s:2:\"44\";i:2;s:2:\"51\";i:3;s:2:\"52\";}'),
(399, 60, 'featured_image', 'upload/product/tobias-riley/feature/Da0r21JrtcdeJb59UCsYNIvtrSOAjEoyeJWdMvcP.jpeg'),
(400, 60, 'stock', '1'),
(401, 60, 'stock_manage_chk', NULL),
(402, 60, 'stock_threshold', 'no'),
(403, 60, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"huz2KGEWdTkMZ0kgUkwZKBCuRXwfA3mOR76nEy1X\";s:12:\"product_name\";s:12:\"Tobias Riley\";s:11:\"description\";s:498:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>\";s:13:\"regular_price\";s:3:\"943\";s:11:\"sale_prices\";s:3:\"300\";s:16:\"sales_price_schd\";s:23:\"03/04/2020 - 03/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:4:{i:0;s:2:\"43\";i:1;s:2:\"44\";i:2;s:2:\"51\";i:3;s:2:\"52\";}s:4:\"tags\";a:1:{i:0;s:1:\"8\";}}'),
(404, 61, 'slug', 'brandon-sheppard'),
(405, 61, 'price', '537'),
(406, 61, 'sale_price', '500'),
(407, 61, 'sale_start_date', '03/04/2020 '),
(408, 61, 'sale_end_date', ' 03/04/2020'),
(409, 61, 'sku', NULL),
(410, 61, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(411, 61, 'categories', 'a:2:{i:0;s:2:\"44\";i:1;s:2:\"49\";}'),
(412, 61, 'featured_image', 'upload/product/brandon-sheppard/feature/Dz8XYrQ47zqSPa7LuelmuzdGR1GYJ7MTeAgWIcQS.jpeg'),
(413, 61, 'stock', '1'),
(414, 61, 'stock_manage_chk', NULL),
(415, 61, 'stock_threshold', 'no'),
(416, 61, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"huz2KGEWdTkMZ0kgUkwZKBCuRXwfA3mOR76nEy1X\";s:12:\"product_name\";s:16:\"Brandon Sheppard\";s:11:\"description\";s:498:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>\";s:13:\"regular_price\";s:3:\"537\";s:11:\"sale_prices\";s:3:\"500\";s:16:\"sales_price_schd\";s:23:\"03/04/2020 - 03/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:2:{i:0;s:2:\"44\";i:1;s:2:\"49\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}}'),
(417, 62, 'slug', 'bradley-head'),
(418, 62, 'price', '662'),
(419, 62, 'sale_price', '156'),
(420, 62, 'sale_start_date', '03/04/2020 '),
(421, 62, 'sale_end_date', ' 03/04/2020'),
(422, 62, 'sku', NULL),
(423, 62, 'tags', 'a:2:{i:0;s:1:\"7\";i:1;s:1:\"8\";}'),
(424, 62, 'categories', 'a:4:{i:0;s:2:\"44\";i:1;s:2:\"50\";i:2;s:2:\"51\";i:3;s:2:\"53\";}'),
(425, 62, 'featured_image', 'upload/product/bradley-head/feature/VrONxJKgzeq7aCREr3luvkQgkhvU7p2KD6SrVgjO.jpeg'),
(426, 62, 'stock', '1'),
(427, 62, 'stock_manage_chk', NULL),
(428, 62, 'stock_threshold', 'no'),
(429, 62, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"huz2KGEWdTkMZ0kgUkwZKBCuRXwfA3mOR76nEy1X\";s:12:\"product_name\";s:12:\"Bradley Head\";s:11:\"description\";s:498:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>\";s:13:\"regular_price\";s:3:\"662\";s:11:\"sale_prices\";s:3:\"156\";s:16:\"sales_price_schd\";s:23:\"03/04/2020 - 03/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:4:{i:0;s:2:\"44\";i:1;s:2:\"50\";i:2;s:2:\"51\";i:3;s:2:\"53\";}s:4:\"tags\";a:2:{i:0;s:1:\"7\";i:1;s:1:\"8\";}}'),
(430, 63, 'slug', 'nicholas-drake'),
(431, 63, 'price', '168'),
(432, 63, 'sale_price', '120'),
(433, 63, 'sale_start_date', '03/04/2020 '),
(434, 63, 'sale_end_date', ' 03/04/2020'),
(435, 63, 'sku', NULL),
(436, 63, 'tags', 'a:2:{i:0;s:1:\"7\";i:1;s:1:\"8\";}'),
(437, 63, 'categories', 'a:3:{i:0;s:2:\"44\";i:1;s:2:\"49\";i:2;s:2:\"52\";}'),
(438, 63, 'featured_image', 'upload/product/nicholas-drake/feature/A4uRzpklXN3AWhm5lRB1GsHCIQmR9Wc8xk9U2Stq.jpeg'),
(439, 63, 'stock', '1'),
(440, 63, 'stock_manage_chk', NULL),
(441, 63, 'stock_threshold', 'no'),
(442, 63, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"huz2KGEWdTkMZ0kgUkwZKBCuRXwfA3mOR76nEy1X\";s:12:\"product_name\";s:14:\"Nicholas Drake\";s:11:\"description\";s:498:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas elit.</span>\";s:13:\"regular_price\";s:3:\"168\";s:11:\"sale_prices\";s:3:\"120\";s:16:\"sales_price_schd\";s:23:\"03/04/2020 - 03/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:3:{i:0;s:2:\"44\";i:1;s:2:\"49\";i:2;s:2:\"52\";}s:4:\"tags\";a:2:{i:0;s:1:\"7\";i:1;s:1:\"8\";}}'),
(443, 64, 'slug', 'dummy-0987'),
(444, 64, 'price', '123'),
(445, 64, 'sale_price', '34'),
(446, 64, 'sale_start_date', '03/10/2020 '),
(447, 64, 'sale_end_date', ' 03/10/2020'),
(448, 64, 'sku', 'sde-432333'),
(449, 64, 'tags', 'a:2:{i:0;s:1:\"7\";i:1;s:1:\"8\";}'),
(450, 64, 'categories', 'a:1:{i:0;s:2:\"50\";}'),
(451, 64, 'featured_image', 'upload/product/dummy-0987/feature/whxl7DnWQqdUHdf63zHjKAE1Jcv1746Nm18VLmeX.jpeg'),
(452, 64, 'stock', '1'),
(453, 64, 'material', 'qwerty,hello world,good bye,gummies'),
(454, 64, 'sizes', '234 cm , 350 cm'),
(455, 64, 'stock_manage_chk', NULL),
(456, 64, 'stock_threshold', NULL),
(457, 64, 'serialize_data', 'a:14:{s:6:\"_token\";s:40:\"upHJat51rsRdRS646FFKQZED669KsBfAs4tqEjCH\";s:12:\"product_name\";s:10:\"dummy 0987\";s:11:\"description\";s:22:\"asdadasdasdsdasdasdasd\";s:13:\"regular_price\";s:3:\"123\";s:11:\"sale_prices\";s:2:\"34\";s:16:\"sales_price_schd\";s:23:\"03/10/2020 - 03/10/2020\";s:11:\"product_sku\";s:10:\"sde-432333\";s:14:\"stock_quantity\";s:1:\"1\";s:15:\"stock_threshold\";N;s:16:\"product_material\";s:35:\"qwerty,hello world,good bye,gummies\";s:13:\"product_sizes\";s:15:\"234 cm , 350 cm\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"50\";}s:4:\"tags\";a:2:{i:0;s:1:\"7\";i:1;s:1:\"8\";}}'),
(458, 65, 'slug', 'asd-001'),
(459, 65, 'price', '120'),
(460, 65, 'sale_price', NULL),
(461, 65, 'sale_start_date', '03/10/2020 '),
(462, 65, 'sale_end_date', ' 03/10/2020'),
(463, 65, 'sku', 'sde-4432'),
(464, 65, 'tags', 'a:1:{i:0;s:1:\"6\";}'),
(465, 65, 'categories', 'a:2:{i:0;s:2:\"43\";i:1;s:2:\"44\";}'),
(466, 65, 'featured_image', 'upload/product/asd-001/feature/vAvsiHHkcfyj2Sau3ssZidUFAGwBtSqf0q1xeYzq.png'),
(467, 65, 'stock', '10'),
(468, 65, 'material', 'qwerty,hello world,good bye,gummies'),
(469, 65, 'sizes', '234cm , 350cm'),
(470, 65, 'stock_manage_chk', NULL),
(471, 65, 'stock_threshold', NULL),
(472, 65, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"upHJat51rsRdRS646FFKQZED669KsBfAs4tqEjCH\";s:12:\"product_name\";s:7:\"asd-001\";s:11:\"description\";s:475:\"<span style=\"font-family: roboto; font-size: 14px; background-color: rgb(255, 255, 255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget elit venenatis, tincidunt quam aliquet, congue mauris. Integer tempus nisi massa, non viverra dolor blandit eu. Fusce tempus dui nisi, vitae scelerisque ligula pharetra id. Morbi ut hendrerit nibh. Cras ultricies fermentum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec non egestas</span>\";s:28:\"product_features_description\";s:184:\"<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\"><br></p>\";s:13:\"regular_price\";s:3:\"120\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:23:\"03/10/2020 - 03/10/2020\";s:11:\"product_sku\";s:8:\"sde-4432\";s:20:\"manage_stock_checked\";N;s:14:\"stock_quantity\";s:2:\"10\";s:15:\"stock_threshold\";N;s:16:\"product_material\";s:35:\"qwerty,hello world,good bye,gummies\";s:13:\"product_sizes\";s:13:\"234cm , 350cm\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:2:{i:0;s:2:\"43\";i:1;s:2:\"44\";}s:4:\"tags\";a:1:{i:0;s:1:\"6\";}}'),
(473, 66, 'slug', 'felix-carter'),
(474, 66, 'price', '857'),
(475, 66, 'sale_price', '836'),
(476, 66, 'sale_start_date', '03/10/2020 '),
(477, 66, 'sale_end_date', ' 03/10/2020'),
(478, 66, 'sku', NULL),
(479, 66, 'tags', 'a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}'),
(480, 66, 'categories', 'a:3:{i:0;s:2:\"50\";i:1;s:2:\"51\";i:2;s:2:\"52\";}'),
(481, 66, 'featured_image', 'upload/product/felix-carter/feature/UOqdBgW3w404QBQvGpK1IeBPgFap3E279LP31x6I.jpeg'),
(482, 66, 'stock', '1'),
(483, 66, 'material', 'Enim, ipsum, ratione q'),
(484, 66, 'sizes', '234 cm , 350 cm'),
(485, 66, 'product_features_description', '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Ut elementum auctor quam, sit amet mollis dolor euismod nec. Proin iaculis ex ac cursus viverra. Phasellus nisl ipsum, vestibulum nec mollis quis, rutrum nec massa. Proin posuere sagittis orci, ultricies bibendum urna commodo eu. Suspendisse augue ligula, tincidunt eu mauris nec, condimentum mattis augue. Pellentesque vitae erat elit. Donec faucibus aliquet sapien quis semper. Quisque iaculis maximus elit, eget congue ante fringilla eu. Etiam ac efficitur est. Curabitur eget nibh ac libero efficitur molestie id eu ex. Phasellus leo nisi, consequat eu pharetra a, consectetur nec sem. Quisque ante mauris, tincidunt a diam bibendum, iaculis varius dolor. In nec nisl lacus. Pellentesque pharetra vel justo eget dictum. Vestibulum purus ligula, scelerisque id arcu vitae, accumsan malesuada arcu.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Integer venenatis eleifend lacinia. Cras venenatis magna id massa volutpat lacinia. Donec commodo nunc in congue pretium. Proin gravida tincidunt malesuada. Maecenas et diam in massa sollicitudin blandit. In velit erat, euismod et molestie eu, tincidunt sed sapien. Nunc a suscipit nisl. Etiam quis quam mauris. Nullam volutpat ante eu vestibulum auctor. Ut id diam non sapien bibendum sollicitudin in in ex.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Aliquam sit amet enim pharetra, mattis tellus at, eleifend erat. Sed laoreet erat sed urna rutrum, sit amet luctus ipsum mollis. Fusce congue purus sed viverra sodales. Aliquam a risus nunc. Curabitur a lorem sit amet ex facilisis aliquam non et nibh. In in justo a nunc mattis blandit in ac augue. Quisque sit amet auctor enim, sit amet fermentum arcu. Duis sit amet placerat tortor.</p>'),
(486, 66, 'stock_manage_chk', NULL),
(487, 66, 'stock_threshold', 'no'),
(488, 66, 'serialize_data', 'a:15:{s:6:\"_token\";s:40:\"upHJat51rsRdRS646FFKQZED669KsBfAs4tqEjCH\";s:12:\"product_name\";s:12:\"Felix Carter\";s:11:\"description\";s:691:\"<div style=\"text-align: left;\"><span style=\"text-align: justify;\">L</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">orem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel diam convallis, suscipit arcu et, pretium mauris. Etiam a tortor porttitor, gravida nulla in, hendrerit turpis. Quisque non scelerisque nunc. Pellentesque dictum efficitur sagittis. Nulla eget pulvinar sapien. Morbi id placerat lectus. Praesent non ipsum eu erat efficitur fringilla vel ut risus. Praesent pellentesque purus a leo eleifend pulvinar. Fusce commodo ligula lorem</span><span style=\"font-size: 1rem;\">ue nostrum .</span></div>\";s:28:\"product_features_description\";s:2132:\"<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Ut elementum auctor quam, sit amet mollis dolor euismod nec. Proin iaculis ex ac cursus viverra. Phasellus nisl ipsum, vestibulum nec mollis quis, rutrum nec massa. Proin posuere sagittis orci, ultricies bibendum urna commodo eu. Suspendisse augue ligula, tincidunt eu mauris nec, condimentum mattis augue. Pellentesque vitae erat elit. Donec faucibus aliquet sapien quis semper. Quisque iaculis maximus elit, eget congue ante fringilla eu. Etiam ac efficitur est. Curabitur eget nibh ac libero efficitur molestie id eu ex. Phasellus leo nisi, consequat eu pharetra a, consectetur nec sem. Quisque ante mauris, tincidunt a diam bibendum, iaculis varius dolor. In nec nisl lacus. Pellentesque pharetra vel justo eget dictum. Vestibulum purus ligula, scelerisque id arcu vitae, accumsan malesuada arcu.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Integer venenatis eleifend lacinia. Cras venenatis magna id massa volutpat lacinia. Donec commodo nunc in congue pretium. Proin gravida tincidunt malesuada. Maecenas et diam in massa sollicitudin blandit. In velit erat, euismod et molestie eu, tincidunt sed sapien. Nunc a suscipit nisl. Etiam quis quam mauris. Nullam volutpat ante eu vestibulum auctor. Ut id diam non sapien bibendum sollicitudin in in ex.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Aliquam sit amet enim pharetra, mattis tellus at, eleifend erat. Sed laoreet erat sed urna rutrum, sit amet luctus ipsum mollis. Fusce congue purus sed viverra sodales. Aliquam a risus nunc. Curabitur a lorem sit amet ex facilisis aliquam non et nibh. In in justo a nunc mattis blandit in ac augue. Quisque sit amet auctor enim, sit amet fermentum arcu. Duis sit amet placerat tortor.</p>\";s:13:\"regular_price\";s:3:\"857\";s:11:\"sale_prices\";s:3:\"836\";s:16:\"sales_price_schd\";s:23:\"03/10/2020 - 03/10/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:16:\"product_material\";s:22:\"Enim, ipsum, ratione q\";s:13:\"product_sizes\";s:15:\"234 cm , 350 cm\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:3:{i:0;s:2:\"50\";i:1;s:2:\"51\";i:2;s:2:\"52\";}s:4:\"tags\";a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}}'),
(579, 74, 'slug', 'jack-chambers'),
(580, 74, 'price', '625'),
(581, 74, 'sale_price', ''),
(582, 74, 'sale_start_date', ''),
(583, 74, 'sale_end_date', ''),
(584, 74, 'sku', 'Ratione eiusmod veli'),
(585, 74, 'tags', 'a:4:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";i:3;s:1:\"9\";}'),
(586, 74, 'categories', 'a:6:{i:0;s:2:\"44\";i:1;s:2:\"49\";i:2;s:2:\"51\";i:3;s:2:\"52\";i:4;s:2:\"53\";i:5;s:2:\"54\";}'),
(587, 74, 'featured_image', 'upload/product/jack-chambers/feature/fzyPGwqgFnetPwuo1FPhwn3wBdTku0bYQXZCmVf6.jpeg'),
(588, 74, 'stock', '121'),
(589, 74, 'material', ''),
(590, 74, 'sizes', ''),
(591, 74, 'product_features_description', ''),
(592, 74, 'stock_manage_chk', ''),
(593, 74, 'stock_threshold', ''),
(594, 74, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"izaqteG2SeIQSQ4XYmR8N6gC638WoHo4yKTjof3u\";s:10:\"image_name\";a:4:{i:0;s:74:\"upload/product_temperory_img/fzyPGwqgFnetPwuo1FPhwn3wBdTku0bYQXZCmVf6.jpeg\";i:1;s:73:\"upload/product_temperory_img/EBQYIji1up2NnFumXbhRIvXiu6ZvbR5eFv0yc5Q6.png\";i:2;s:73:\"upload/product_temperory_img/826JyNbZcxZwLycxzlPdL8mN70shKbQDjCeO7rsD.png\";i:3;s:74:\"upload/product_temperory_img/L9lLSnIoXVdEtVizU5OTLT7dBnjthDSn2HRZ9RBY.jpeg\";}s:12:\"image_row_id\";a:4:{i:0;s:3:\"232\";i:1;s:3:\"233\";i:2;s:3:\"234\";i:3;s:3:\"235\";}s:11:\"temp_img_id\";s:3:\"235\";s:12:\"product_name\";a:4:{i:0;s:13:\"Jack Chambers\";i:1;s:11:\"Jayme Wolfe\";i:2;s:18:\"Cynthia Valenzuela\";i:3;s:9:\"Oren Head\";}s:10:\"categories\";a:4:{s:12:\"categories_0\";a:6:{i:0;s:2:\"44\";i:1;s:2:\"49\";i:2;s:2:\"51\";i:3;s:2:\"52\";i:4;s:2:\"53\";i:5;s:2:\"54\";}s:12:\"categories_1\";a:4:{i:0;s:2:\"43\";i:1;s:2:\"50\";i:2;s:2:\"51\";i:3;s:2:\"54\";}s:12:\"categories_2\";a:6:{i:0;s:2:\"43\";i:1;s:2:\"44\";i:2;s:2:\"45\";i:3;s:2:\"50\";i:4;s:2:\"52\";i:5;s:2:\"54\";}s:12:\"categories_3\";a:2:{i:0;s:2:\"53\";i:1;s:2:\"54\";}}s:4:\"tags\";a:4:{s:6:\"tags_0\";a:4:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";i:3;s:1:\"9\";}s:6:\"tags_1\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"8\";i:2;s:1:\"9\";}s:6:\"tags_2\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"9\";}s:6:\"tags_3\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"8\";i:2;s:1:\"9\";}}s:3:\"sku\";a:4:{i:0;s:20:\"Ratione eiusmod veli\";i:1;s:20:\"Officia molestiae co\";i:2;s:20:\"Quidem illo magnam e\";i:3;s:20:\"Qui dicta distinctio\";}s:5:\"stock\";a:4:{i:0;s:3:\"121\";i:1;s:3:\"232\";i:2;s:3:\"323\";i:3;s:3:\"323\";}s:5:\"price\";a:4:{i:0;s:3:\"625\";i:1;s:3:\"674\";i:2;s:3:\"347\";i:3;s:3:\"387\";}s:6:\"status\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"0\";i:2;s:1:\"0\";i:3;s:1:\"1\";}s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(595, 74, 'product_video_description', ''),
(596, 74, 'product_video', ''),
(633, 77, 'slug', 'oren-head'),
(634, 77, 'price', '387'),
(635, 77, 'sale_price', ''),
(636, 77, 'sale_start_date', ''),
(637, 77, 'sale_end_date', ''),
(638, 77, 'sku', 'Qui dicta distinctio'),
(639, 77, 'tags', 'a:4:{i:0;s:1:\"6\";i:1;s:1:\"8\";i:2;s:1:\"9\";i:3;s:1:\"9\";}'),
(640, 77, 'categories', 'a:6:{i:0;s:2:\"53\";i:1;s:2:\"54\";i:2;s:2:\"45\";i:3;s:2:\"50\";i:4;s:2:\"52\";i:5;s:2:\"54\";}'),
(641, 77, 'featured_image', 'upload/product/oren-head/feature/L9lLSnIoXVdEtVizU5OTLT7dBnjthDSn2HRZ9RBY.jpeg'),
(642, 77, 'stock', '323'),
(643, 77, 'material', ''),
(644, 77, 'sizes', ''),
(645, 77, 'product_features_description', ''),
(646, 77, 'stock_manage_chk', ''),
(647, 77, 'stock_threshold', ''),
(648, 77, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"izaqteG2SeIQSQ4XYmR8N6gC638WoHo4yKTjof3u\";s:10:\"image_name\";a:4:{i:0;s:74:\"upload/product_temperory_img/fzyPGwqgFnetPwuo1FPhwn3wBdTku0bYQXZCmVf6.jpeg\";i:1;s:73:\"upload/product_temperory_img/EBQYIji1up2NnFumXbhRIvXiu6ZvbR5eFv0yc5Q6.png\";i:2;s:73:\"upload/product_temperory_img/826JyNbZcxZwLycxzlPdL8mN70shKbQDjCeO7rsD.png\";i:3;s:74:\"upload/product_temperory_img/L9lLSnIoXVdEtVizU5OTLT7dBnjthDSn2HRZ9RBY.jpeg\";}s:12:\"image_row_id\";a:4:{i:0;s:3:\"232\";i:1;s:3:\"233\";i:2;s:3:\"234\";i:3;s:3:\"235\";}s:11:\"temp_img_id\";s:3:\"235\";s:12:\"product_name\";a:4:{i:0;s:13:\"Jack Chambers\";i:1;s:11:\"Jayme Wolfe\";i:2;s:18:\"Cynthia Valenzuela\";i:3;s:9:\"Oren Head\";}s:10:\"categories\";a:4:{s:12:\"categories_0\";a:6:{i:0;s:2:\"44\";i:1;s:2:\"49\";i:2;s:2:\"51\";i:3;s:2:\"52\";i:4;s:2:\"53\";i:5;s:2:\"54\";}s:12:\"categories_1\";a:4:{i:0;s:2:\"43\";i:1;s:2:\"50\";i:2;s:2:\"51\";i:3;s:2:\"54\";}s:12:\"categories_2\";a:6:{i:0;s:2:\"43\";i:1;s:2:\"44\";i:2;s:2:\"45\";i:3;s:2:\"50\";i:4;s:2:\"52\";i:5;s:2:\"54\";}s:12:\"categories_3\";a:2:{i:0;s:2:\"53\";i:1;s:2:\"54\";}}s:4:\"tags\";a:4:{s:6:\"tags_0\";a:4:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";i:3;s:1:\"9\";}s:6:\"tags_1\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"8\";i:2;s:1:\"9\";}s:6:\"tags_2\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"9\";}s:6:\"tags_3\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"8\";i:2;s:1:\"9\";}}s:3:\"sku\";a:4:{i:0;s:20:\"Ratione eiusmod veli\";i:1;s:20:\"Officia molestiae co\";i:2;s:20:\"Quidem illo magnam e\";i:3;s:20:\"Qui dicta distinctio\";}s:5:\"stock\";a:4:{i:0;s:3:\"121\";i:1;s:3:\"232\";i:2;s:3:\"323\";i:3;s:3:\"323\";}s:5:\"price\";a:4:{i:0;s:3:\"625\";i:1;s:3:\"674\";i:2;s:3:\"347\";i:3;s:3:\"387\";}s:6:\"status\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"0\";i:2;s:1:\"0\";i:3;s:1:\"1\";}s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(649, 77, 'product_video_description', ''),
(650, 77, 'product_video', ''),
(1065, 102, 'slug', '323518-p9mhs8-840'),
(1066, 102, 'price', '56'),
(1067, 102, 'sale_price', ''),
(1068, 102, 'sale_start_date', ''),
(1069, 102, 'sale_end_date', ''),
(1070, 102, 'sku', ''),
(1071, 102, 'tags', 'a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}'),
(1072, 102, 'categories', 's:2:\"66\";'),
(1073, 102, 'featured_image', 'upload/product/323518-p9mhs8-840/feature/mpeTfnFhQGwVtzekxcvmeLuUxoVuKqb7axteB11t.jpeg'),
(1074, 102, 'stock', ''),
(1075, 102, 'material', 'Heavy Material , Soft Quality'),
(1076, 102, 'sizes', '240cm, 566cm'),
(1077, 102, 'product_features_description', ''),
(1078, 102, 'stock_manage_chk', ''),
(1079, 102, 'stock_threshold', ''),
(1080, 102, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"p7iqh7j5fPUMwIjoHTQnRagiuRGkeS6gTd7GUdi9\";s:12:\"product_name\";a:2:{i:0;s:17:\"323518-P9MHS8-840\";i:1;s:17:\"artsfon.com-51753\";}s:10:\"image_name\";a:2:{i:0;s:74:\"upload/product_temperory_img/mpeTfnFhQGwVtzekxcvmeLuUxoVuKqb7axteB11t.jpeg\";i:1;s:74:\"upload/product_temperory_img/6oqGt4E3elXsABE1G0vUD4VpKlGNt5S8pNeum3ld.jpeg\";}s:12:\"image_row_id\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}s:10:\"categories\";a:2:{i:0;s:2:\"66\";i:1;s:2:\"66\";}s:5:\"price\";a:2:{i:0;s:2:\"56\";i:1;s:2:\"56\";}s:4:\"tags\";a:2:{i:0;s:30:\"a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}\";i:1;s:30:\"a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}\";}s:8:\"material\";a:2:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:2:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:1:\"2\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1081, 102, 'product_video_description', ''),
(1082, 102, 'product_video', ''),
(1101, 104, 'slug', 'product-3'),
(1102, 104, 'price', '59.0'),
(1103, 104, 'sale_price', ''),
(1104, 104, 'sale_start_date', ''),
(1105, 104, 'sale_end_date', ''),
(1106, 104, 'sku', ''),
(1107, 104, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1108, 104, 'categories', 's:2:\"67\";'),
(1109, 104, 'featured_image', 'upload/product/product-3/feature/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png'),
(1110, 104, 'stock', ''),
(1111, 104, 'material', 'Heavy Material , Soft Quality'),
(1112, 104, 'sizes', '240cm, 566cm'),
(1113, 104, 'product_features_description', ''),
(1114, 104, 'stock_manage_chk', ''),
(1115, 104, 'stock_threshold', ''),
(1116, 104, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1117, 104, 'product_video_description', ''),
(1118, 104, 'product_video', ''),
(1119, 105, 'slug', 'product-4'),
(1120, 105, 'price', '59.0'),
(1121, 105, 'sale_price', ''),
(1122, 105, 'sale_start_date', ''),
(1123, 105, 'sale_end_date', ''),
(1124, 105, 'sku', ''),
(1125, 105, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1126, 105, 'categories', 's:2:\"67\";'),
(1127, 105, 'featured_image', 'upload/product/product-4/feature/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg'),
(1128, 105, 'stock', ''),
(1129, 105, 'material', 'Heavy Material , Soft Quality'),
(1130, 105, 'sizes', '240cm, 566cm'),
(1131, 105, 'product_features_description', ''),
(1132, 105, 'stock_manage_chk', ''),
(1133, 105, 'stock_threshold', ''),
(1134, 105, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1135, 105, 'product_video_description', ''),
(1136, 105, 'product_video', ''),
(1137, 106, 'slug', 'product-5'),
(1138, 106, 'price', '59.0'),
(1139, 106, 'sale_price', ''),
(1140, 106, 'sale_start_date', ''),
(1141, 106, 'sale_end_date', ''),
(1142, 106, 'sku', ''),
(1143, 106, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1144, 106, 'categories', 's:2:\"67\";'),
(1145, 106, 'featured_image', 'upload/product/product-5/feature/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg'),
(1146, 106, 'stock', ''),
(1147, 106, 'material', 'Heavy Material , Soft Quality'),
(1148, 106, 'sizes', '240cm, 566cm'),
(1149, 106, 'product_features_description', ''),
(1150, 106, 'stock_manage_chk', ''),
(1151, 106, 'stock_threshold', '');
INSERT INTO `products_metas` (`id`, `product_id`, `product_meta`, `product_meta_value`) VALUES
(1152, 106, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1153, 106, 'product_video_description', ''),
(1154, 106, 'product_video', ''),
(1155, 107, 'slug', 'product-6'),
(1156, 107, 'price', '59.0'),
(1157, 107, 'sale_price', ''),
(1158, 107, 'sale_start_date', ''),
(1159, 107, 'sale_end_date', ''),
(1160, 107, 'sku', ''),
(1161, 107, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1162, 107, 'categories', 's:2:\"67\";'),
(1163, 107, 'featured_image', 'upload/product/product-6/feature/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg'),
(1164, 107, 'stock', ''),
(1165, 107, 'material', 'Heavy Material , Soft Quality'),
(1166, 107, 'sizes', '240cm, 566cm'),
(1167, 107, 'product_features_description', ''),
(1168, 107, 'stock_manage_chk', ''),
(1169, 107, 'stock_threshold', ''),
(1170, 107, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1171, 107, 'product_video_description', ''),
(1172, 107, 'product_video', ''),
(1173, 108, 'slug', 'product-7'),
(1174, 108, 'price', '59.0'),
(1175, 108, 'sale_price', ''),
(1176, 108, 'sale_start_date', ''),
(1177, 108, 'sale_end_date', ''),
(1178, 108, 'sku', ''),
(1179, 108, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1180, 108, 'categories', 's:2:\"67\";'),
(1181, 108, 'featured_image', 'upload/product/product-7/feature/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg'),
(1182, 108, 'stock', ''),
(1183, 108, 'material', 'Heavy Material , Soft Quality'),
(1184, 108, 'sizes', '240cm, 566cm'),
(1185, 108, 'product_features_description', ''),
(1186, 108, 'stock_manage_chk', ''),
(1187, 108, 'stock_threshold', ''),
(1188, 108, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1189, 108, 'product_video_description', ''),
(1190, 108, 'product_video', ''),
(1191, 109, 'slug', 'product-8'),
(1192, 109, 'price', '59.0'),
(1193, 109, 'sale_price', ''),
(1194, 109, 'sale_start_date', ''),
(1195, 109, 'sale_end_date', ''),
(1196, 109, 'sku', ''),
(1197, 109, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1198, 109, 'categories', 's:2:\"67\";'),
(1199, 109, 'featured_image', 'upload/product/product-8/feature/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg'),
(1200, 109, 'stock', ''),
(1201, 109, 'material', 'Heavy Material , Soft Quality'),
(1202, 109, 'sizes', '240cm, 566cm'),
(1203, 109, 'product_features_description', ''),
(1204, 109, 'stock_manage_chk', ''),
(1205, 109, 'stock_threshold', ''),
(1206, 109, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1207, 109, 'product_video_description', ''),
(1208, 109, 'product_video', ''),
(1209, 110, 'slug', 'product-9'),
(1210, 110, 'price', '59.0'),
(1211, 110, 'sale_price', ''),
(1212, 110, 'sale_start_date', ''),
(1213, 110, 'sale_end_date', ''),
(1214, 110, 'sku', ''),
(1215, 110, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1216, 110, 'categories', 's:2:\"67\";'),
(1217, 110, 'featured_image', 'upload/product/product-9/feature/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg'),
(1218, 110, 'stock', ''),
(1219, 110, 'material', 'Heavy Material , Soft Quality'),
(1220, 110, 'sizes', '240cm, 566cm'),
(1221, 110, 'product_features_description', ''),
(1222, 110, 'stock_manage_chk', ''),
(1223, 110, 'stock_threshold', ''),
(1224, 110, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1225, 110, 'product_video_description', ''),
(1226, 110, 'product_video', ''),
(1227, 111, 'slug', 'product-10'),
(1228, 111, 'price', '59.0'),
(1229, 111, 'sale_price', ''),
(1230, 111, 'sale_start_date', ''),
(1231, 111, 'sale_end_date', ''),
(1232, 111, 'sku', ''),
(1233, 111, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1234, 111, 'categories', 's:2:\"67\";'),
(1235, 111, 'featured_image', 'upload/product/product-10/feature/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg'),
(1236, 111, 'stock', ''),
(1237, 111, 'material', 'Heavy Material , Soft Quality'),
(1238, 111, 'sizes', '240cm, 566cm'),
(1239, 111, 'product_features_description', ''),
(1240, 111, 'stock_manage_chk', ''),
(1241, 111, 'stock_threshold', ''),
(1242, 111, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1243, 111, 'product_video_description', ''),
(1244, 111, 'product_video', ''),
(1245, 112, 'slug', 'product-11'),
(1246, 112, 'price', '59.0'),
(1247, 112, 'sale_price', ''),
(1248, 112, 'sale_start_date', ''),
(1249, 112, 'sale_end_date', ''),
(1250, 112, 'sku', ''),
(1251, 112, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1252, 112, 'categories', 's:2:\"67\";'),
(1253, 112, 'featured_image', 'upload/product/product-11/feature/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg'),
(1254, 112, 'stock', ''),
(1255, 112, 'material', 'Heavy Material , Soft Quality'),
(1256, 112, 'sizes', '240cm, 566cm'),
(1257, 112, 'product_features_description', ''),
(1258, 112, 'stock_manage_chk', ''),
(1259, 112, 'stock_threshold', ''),
(1260, 112, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1261, 112, 'product_video_description', ''),
(1262, 112, 'product_video', ''),
(1263, 113, 'slug', 'product-13'),
(1264, 113, 'price', '59.0'),
(1265, 113, 'sale_price', ''),
(1266, 113, 'sale_start_date', ''),
(1267, 113, 'sale_end_date', ''),
(1268, 113, 'sku', ''),
(1269, 113, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1270, 113, 'categories', 's:2:\"67\";'),
(1271, 113, 'featured_image', 'upload/product/product-13/feature/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg'),
(1272, 113, 'stock', ''),
(1273, 113, 'material', 'Heavy Material , Soft Quality'),
(1274, 113, 'sizes', '240cm, 566cm'),
(1275, 113, 'product_features_description', ''),
(1276, 113, 'stock_manage_chk', ''),
(1277, 113, 'stock_threshold', '');
INSERT INTO `products_metas` (`id`, `product_id`, `product_meta`, `product_meta_value`) VALUES
(1278, 113, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1279, 113, 'product_video_description', ''),
(1280, 113, 'product_video', ''),
(1281, 114, 'slug', 'product-14'),
(1282, 114, 'price', '59.0'),
(1283, 114, 'sale_price', ''),
(1284, 114, 'sale_start_date', ''),
(1285, 114, 'sale_end_date', ''),
(1286, 114, 'sku', ''),
(1287, 114, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1288, 114, 'categories', 's:2:\"67\";'),
(1289, 114, 'featured_image', 'upload/product/product-14/feature/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg'),
(1290, 114, 'stock', ''),
(1291, 114, 'material', 'Heavy Material , Soft Quality'),
(1292, 114, 'sizes', '240cm, 566cm'),
(1293, 114, 'product_features_description', ''),
(1294, 114, 'stock_manage_chk', ''),
(1295, 114, 'stock_threshold', ''),
(1296, 114, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1297, 114, 'product_video_description', ''),
(1298, 114, 'product_video', ''),
(1299, 115, 'slug', 'product-15'),
(1300, 115, 'price', '59.0'),
(1301, 115, 'sale_price', ''),
(1302, 115, 'sale_start_date', ''),
(1303, 115, 'sale_end_date', ''),
(1304, 115, 'sku', ''),
(1305, 115, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1306, 115, 'categories', 's:2:\"67\";'),
(1307, 115, 'featured_image', 'upload/product/product-15/feature/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg'),
(1308, 115, 'stock', ''),
(1309, 115, 'material', 'Heavy Material , Soft Quality'),
(1310, 115, 'sizes', '240cm, 566cm'),
(1311, 115, 'product_features_description', ''),
(1312, 115, 'stock_manage_chk', ''),
(1313, 115, 'stock_threshold', ''),
(1314, 115, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1315, 115, 'product_video_description', ''),
(1316, 115, 'product_video', ''),
(1317, 116, 'slug', 'product-16'),
(1318, 116, 'price', '59.0'),
(1319, 116, 'sale_price', ''),
(1320, 116, 'sale_start_date', ''),
(1321, 116, 'sale_end_date', ''),
(1322, 116, 'sku', ''),
(1323, 116, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1324, 116, 'categories', 's:2:\"67\";'),
(1325, 116, 'featured_image', 'upload/product/product-16/feature/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg'),
(1326, 116, 'stock', ''),
(1327, 116, 'material', 'Heavy Material , Soft Quality'),
(1328, 116, 'sizes', '240cm, 566cm'),
(1329, 116, 'product_features_description', ''),
(1330, 116, 'stock_manage_chk', ''),
(1331, 116, 'stock_threshold', ''),
(1332, 116, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1333, 116, 'product_video_description', ''),
(1334, 116, 'product_video', ''),
(1335, 117, 'slug', 'product-17fdf'),
(1336, 117, 'price', '59.0'),
(1337, 117, 'sale_price', NULL),
(1338, 117, 'sale_start_date', 'Invalid date '),
(1339, 117, 'sale_end_date', ' Invalid date'),
(1340, 117, 'sku', NULL),
(1341, 117, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1342, 117, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(1343, 117, 'featured_image', 'upload/product/product-17/feature/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg'),
(1344, 117, 'stock', NULL),
(1345, 117, 'material', 'Heavy Material , Soft Quality'),
(1346, 117, 'sizes', '240cm, 566cm'),
(1347, 117, 'product_features_description', ''),
(1348, 117, 'stock_manage_chk', NULL),
(1349, 117, 'stock_threshold', NULL),
(1350, 117, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";s:13:\"Product-17fdf\";s:11:\"description\";N;s:28:\"product_features_description\";N;s:13:\"regular_price\";s:4:\"59.0\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:29:\"Heavy Material , Soft Quality\";s:13:\"product_sizes\";s:12:\"240cm, 566cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}}'),
(1351, 117, 'product_video_description', NULL),
(1352, 117, 'product_video', NULL),
(1353, 118, 'slug', 'product-18'),
(1354, 118, 'price', '59.0'),
(1355, 118, 'sale_price', '0'),
(1356, 118, 'sale_start_date', 'Invalid date '),
(1357, 118, 'sale_end_date', ' Invalid date'),
(1358, 118, 'sku', NULL),
(1359, 118, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1360, 118, 'categories', 'a:1:{i:0;s:2:\"64\";}'),
(1361, 118, 'featured_image', 'upload/product/product-18/feature/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg'),
(1362, 118, 'stock', NULL),
(1363, 118, 'material', 'heelo,world,gummiesworld,goodbuy'),
(1364, 118, 'sizes', '240cm,566cm'),
(1365, 118, 'product_features_description', ''),
(1366, 118, 'stock_manage_chk', NULL),
(1367, 118, 'stock_threshold', NULL),
(1368, 118, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"8to1Gwy0RSjhqtKPUDkbowhguLtF8sl2hp01ykfC\";s:12:\"product_name\";s:10:\"Product-18\";s:11:\"description\";N;s:28:\"product_features_description\";N;s:13:\"regular_price\";s:4:\"59.0\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:32:\"heelo,world,gummiesworld,goodbuy\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"64\";}s:4:\"tags\";a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}}'),
(1369, 118, 'product_video_description', NULL),
(1370, 118, 'product_video', 'https://gitlab.com/yasin.sabir/erp/-/blob/master/app/Http/Controllers/AccountController.php'),
(1371, 119, 'slug', 'product-19'),
(1372, 119, 'price', '59.0'),
(1373, 119, 'sale_price', ''),
(1374, 119, 'sale_start_date', ''),
(1375, 119, 'sale_end_date', ''),
(1376, 119, 'sku', ''),
(1377, 119, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1378, 119, 'categories', 's:2:\"67\";'),
(1379, 119, 'featured_image', 'upload/product/product-19/feature/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg'),
(1380, 119, 'stock', ''),
(1381, 119, 'material', 'Heavy Material , Soft Quality'),
(1382, 119, 'sizes', '240cm, 566cm'),
(1383, 119, 'product_features_description', ''),
(1384, 119, 'stock_manage_chk', ''),
(1385, 119, 'stock_threshold', ''),
(1386, 119, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1387, 119, 'product_video_description', ''),
(1388, 119, 'product_video', ''),
(1389, 120, 'slug', 'product-20'),
(1390, 120, 'price', '59.0'),
(1391, 120, 'sale_price', ''),
(1392, 120, 'sale_start_date', ''),
(1393, 120, 'sale_end_date', ''),
(1394, 120, 'sku', ''),
(1395, 120, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1396, 120, 'categories', 's:2:\"67\";'),
(1397, 120, 'featured_image', 'upload/product/product-20/feature/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg'),
(1398, 120, 'stock', ''),
(1399, 120, 'material', 'Heavy Material , Soft Quality'),
(1400, 120, 'sizes', '240cm, 566cm'),
(1401, 120, 'product_features_description', ''),
(1402, 120, 'stock_manage_chk', ''),
(1403, 120, 'stock_threshold', ''),
(1404, 120, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1405, 120, 'product_video_description', ''),
(1406, 120, 'product_video', ''),
(1407, 121, 'slug', 'product-21'),
(1408, 121, 'price', '59.0'),
(1409, 121, 'sale_price', ''),
(1410, 121, 'sale_start_date', ''),
(1411, 121, 'sale_end_date', ''),
(1412, 121, 'sku', ''),
(1413, 121, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1414, 121, 'categories', 's:2:\"67\";'),
(1415, 121, 'featured_image', 'upload/product/product-21/feature/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg'),
(1416, 121, 'stock', ''),
(1417, 121, 'material', 'Heavy Material , Soft Quality'),
(1418, 121, 'sizes', '240cm, 566cm'),
(1419, 121, 'product_features_description', ''),
(1420, 121, 'stock_manage_chk', ''),
(1421, 121, 'stock_threshold', ''),
(1422, 121, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1423, 121, 'product_video_description', ''),
(1424, 121, 'product_video', ''),
(1425, 122, 'slug', 'product-22'),
(1426, 122, 'price', '59.0'),
(1427, 122, 'sale_price', ''),
(1428, 122, 'sale_start_date', ''),
(1429, 122, 'sale_end_date', ''),
(1430, 122, 'sku', ''),
(1431, 122, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1432, 122, 'categories', 's:2:\"67\";'),
(1433, 122, 'featured_image', 'upload/product/product-22/feature/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg'),
(1434, 122, 'stock', ''),
(1435, 122, 'material', 'Heavy Material , Soft Quality'),
(1436, 122, 'sizes', '240cm, 566cm'),
(1437, 122, 'product_features_description', ''),
(1438, 122, 'stock_manage_chk', ''),
(1439, 122, 'stock_threshold', '');
INSERT INTO `products_metas` (`id`, `product_id`, `product_meta`, `product_meta_value`) VALUES
(1440, 122, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";a:19:{i:0;s:9:\"Product-3\";i:1;s:9:\"Product-4\";i:2;s:9:\"Product-5\";i:3;s:9:\"Product-6\";i:4;s:9:\"Product-7\";i:5;s:9:\"Product-8\";i:6;s:9:\"Product-9\";i:7;s:10:\"Product-10\";i:8;s:10:\"Product-11\";i:9;s:10:\"Product-13\";i:10;s:10:\"Product-14\";i:11;s:10:\"Product-15\";i:12;s:10:\"Product-16\";i:13;s:10:\"Product-17\";i:14;s:10:\"Product-18\";i:15;s:10:\"Product-19\";i:16;s:10:\"Product-20\";i:17;s:10:\"Product-21\";i:18;s:10:\"Product-22\";}s:10:\"image_name\";a:19:{i:0;s:73:\"upload/product_temperory_img/xR91IwOqvSPVih6eLQyiRqwpHyBZAui26lH0VmnB.png\";i:1;s:74:\"upload/product_temperory_img/AC5hXRhRqXsMCiTQP0VWCtgFTsFlb71MH9VLo6WX.jpeg\";i:2;s:74:\"upload/product_temperory_img/4abC4rWO2ReFoxeH9xv8bwj5nvc7rxd6VbI0A1Xr.jpeg\";i:3;s:74:\"upload/product_temperory_img/IlB0YRbkjfjbE7hyYZNaP4tQmOnQ0k9W6U06f2lp.jpeg\";i:4;s:74:\"upload/product_temperory_img/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg\";i:5;s:74:\"upload/product_temperory_img/DDlYsO1xRpFdWrWiieXHZwzq2WFjDs7l6VMC4R0e.jpeg\";i:6;s:74:\"upload/product_temperory_img/n64N2wRoFmdR67cQfzRgFQscGaMRXGT4ie3LcpS8.jpeg\";i:7;s:74:\"upload/product_temperory_img/KSLFyultYU5BC4LcewB97McVmsXVwa1Kxf2QqXBc.jpeg\";i:8;s:74:\"upload/product_temperory_img/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg\";i:9;s:74:\"upload/product_temperory_img/peZv9Y4eZ3mosX7v9k7Uj9YNez80cVEp27QxEKZU.jpeg\";i:10;s:74:\"upload/product_temperory_img/dz85oUJLdghZGg6narwWy4pgydr6cdUbdwGAtqUW.jpeg\";i:11;s:74:\"upload/product_temperory_img/CqHWnOHhDD64nywqw6UOMOvTnS64DgfJmnPfgZ8U.jpeg\";i:12;s:74:\"upload/product_temperory_img/e0qnTxDxcL27QCyDEGgfNZnYU0bVBfiZiUbHqaHy.jpeg\";i:13;s:74:\"upload/product_temperory_img/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg\";i:14;s:74:\"upload/product_temperory_img/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";i:15;s:74:\"upload/product_temperory_img/mprGaDJ8yWDq9Q6nCJKDLc31cVa2NrZJ3estuD9Z.jpeg\";i:16;s:74:\"upload/product_temperory_img/YGJ1QNwDZcjVoZ6DQQ4IusO20VgedLjStz5nHgSS.jpeg\";i:17;s:74:\"upload/product_temperory_img/5HpCY8VOrT63pFz0Xz0eA8jAvlKVCEeBroBHfhSQ.jpeg\";i:18;s:74:\"upload/product_temperory_img/scVhrnGePqCKBeJ5JzAUXcWgSmjhJkVCZkXNddCG.jpeg\";}s:12:\"image_row_id\";a:19:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"16\";i:13;s:2:\"17\";i:14;s:2:\"18\";i:15;s:2:\"19\";i:16;s:2:\"20\";i:17;s:2:\"21\";i:18;s:2:\"22\";}s:10:\"categories\";a:19:{i:0;s:2:\"67\";i:1;s:2:\"67\";i:2;s:2:\"67\";i:3;s:2:\"67\";i:4;s:2:\"67\";i:5;s:2:\"67\";i:6;s:2:\"67\";i:7;s:2:\"67\";i:8;s:2:\"67\";i:9;s:2:\"67\";i:10;s:2:\"67\";i:11;s:2:\"67\";i:12;s:2:\"67\";i:13;s:2:\"67\";i:14;s:2:\"67\";i:15;s:2:\"67\";i:16;s:2:\"67\";i:17;s:2:\"67\";i:18;s:2:\"67\";}s:5:\"price\";a:19:{i:0;s:4:\"59.0\";i:1;s:4:\"59.0\";i:2;s:4:\"59.0\";i:3;s:4:\"59.0\";i:4;s:4:\"59.0\";i:5;s:4:\"59.0\";i:6;s:4:\"59.0\";i:7;s:4:\"59.0\";i:8;s:4:\"59.0\";i:9;s:4:\"59.0\";i:10;s:4:\"59.0\";i:11;s:4:\"59.0\";i:12;s:4:\"59.0\";i:13;s:4:\"59.0\";i:14;s:4:\"59.0\";i:15;s:4:\"59.0\";i:16;s:4:\"59.0\";i:17;s:4:\"59.0\";i:18;s:4:\"59.0\";}s:4:\"tags\";a:19:{i:0;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:8;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:9;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:10;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:11;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:12;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:13;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:14;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:15;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:16;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:17;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";i:18;s:42:\"a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}\";}s:8:\"material\";a:19:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";i:8;s:29:\"Heavy Material , Soft Quality\";i:9;s:29:\"Heavy Material , Soft Quality\";i:10;s:29:\"Heavy Material , Soft Quality\";i:11;s:29:\"Heavy Material , Soft Quality\";i:12;s:29:\"Heavy Material , Soft Quality\";i:13;s:29:\"Heavy Material , Soft Quality\";i:14;s:29:\"Heavy Material , Soft Quality\";i:15;s:29:\"Heavy Material , Soft Quality\";i:16;s:29:\"Heavy Material , Soft Quality\";i:17;s:29:\"Heavy Material , Soft Quality\";i:18;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:19:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";i:8;s:12:\"240cm, 566cm\";i:9;s:12:\"240cm, 566cm\";i:10;s:12:\"240cm, 566cm\";i:11;s:12:\"240cm, 566cm\";i:12;s:12:\"240cm, 566cm\";i:13;s:12:\"240cm, 566cm\";i:14;s:12:\"240cm, 566cm\";i:15;s:12:\"240cm, 566cm\";i:16;s:12:\"240cm, 566cm\";i:17;s:12:\"240cm, 566cm\";i:18;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"22\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1441, 122, 'product_video_description', ''),
(1442, 122, 'product_video', ''),
(1443, 55, 'material', NULL),
(1444, 55, 'sizes', NULL),
(1445, 55, 'product_video_description', NULL),
(1446, 55, 'product_video', 'null'),
(1447, 53, 'material', 'qwerty,helloworld,good_bye,gummies'),
(1448, 53, 'sizes', '234cm,350cm'),
(1449, 53, 'product_video_description', NULL),
(1450, 53, 'product_video', 'www.youtube.com/embed/z15Akk6Z4OY'),
(1451, 123, 'slug', 'qwerty098-7'),
(1452, 123, 'price', '777'),
(1453, 123, 'sale_price', '600'),
(1454, 123, 'sale_start_date', '04/04/2020 '),
(1455, 123, 'sale_end_date', ' 04/04/2020'),
(1456, 123, 'sku', NULL),
(1457, 123, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"6\";}'),
(1458, 123, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(1459, 123, 'featured_image', 'upload/product/qwerty098/feature/4VPxihSjcrQnnyAgWhebxD4KcPxqP9VlijOzUdlp.jpeg'),
(1460, 123, 'stock', NULL),
(1461, 123, 'material', 'Hello,Worlf'),
(1462, 123, 'sizes', '234cm,350cm'),
(1463, 123, 'stock_manage_chk', NULL),
(1464, 123, 'stock_threshold', NULL),
(1465, 123, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"jIXrg7gYJxWtTcvcrUi4HERESS5K0lHHqBbilSRi\";s:12:\"product_name\";s:11:\"qwerty098 7\";s:11:\"description\";s:693:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc semper, nisl id ultrices venenatis, diam odio posuere nulla, non consectetur arcu quam vel lorem. Nunc ornare diam in ligula porta, nec fermentum arcu vestibulum. Aliquam risus nisi, fringilla nec rutrum nec, placerat a elit.098 7</span><br></p>\";s:28:\"product_features_description\";N;s:13:\"regular_price\";s:3:\"777\";s:11:\"sale_prices\";s:3:\"600\";s:16:\"sales_price_schd\";s:23:\"04/04/2020 - 04/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";s:373:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula.098</span><br></p>\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:2:{i:0;s:1:\"5\";i:1;s:1:\"6\";}}'),
(1466, 123, 'product_video_description', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula.098</span><br></p>'),
(1467, 123, 'product_video', 'https://gitlab.com/yasin.sabir/erp/-/tree/master/app/Http/Controllers'),
(1468, 57, 'material', 'Hello,Worlf'),
(1469, 57, 'sizes', '234cm,350cm'),
(1470, 57, 'product_video_description', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc semper, nisl id ultrices venenatis, diam odio posuere nulla, non consectetur arcu quam vel lorem. Nunc ornare diam in ligula porta, nec fermentum arcu vestibulum. Aliquam risus nisi, fringilla nec rutrum nec, placerat a elit.</span>'),
(1471, 57, 'product_video', 'https://gitlab.com/yasin.sabir/erp/-/tree/master/app/Http/Controllers'),
(1508, 126, 'slug', 'galactic-atoms'),
(1509, 126, 'price', '68'),
(1510, 126, 'sale_price', ''),
(1511, 126, 'sale_start_date', ''),
(1512, 126, 'sale_end_date', ''),
(1513, 126, 'sku', ''),
(1514, 126, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1515, 126, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1516, 126, 'featured_image', 'upload/product/galactic-atoms/feature/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png'),
(1517, 126, 'stock', ''),
(1518, 126, 'material', 'Heavy Material,Soft Quality'),
(1519, 126, 'sizes', '240cm, 566c'),
(1520, 126, 'product_features_description', ''),
(1521, 126, 'stock_manage_chk', ''),
(1522, 126, 'stock_threshold', ''),
(1523, 126, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"s0JtAbtSjXp6N42hL2LNJAw375OKLUL5fqnViji4\";s:12:\"product_name\";a:2:{i:0;s:14:\"Galactic Atoms\";i:1;s:7:\"Space 1\";}s:10:\"image_name\";a:2:{i:0;s:73:\"upload/product_temperory_img/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png\";i:1;s:74:\"upload/product_temperory_img/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg\";}s:12:\"image_row_id\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"4\";}s:10:\"categories\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:5:\"price\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";i:1;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";}s:8:\"material\";a:2:{i:0;s:27:\"Heavy Material,Soft Quality\";i:1;s:27:\"Heavy Material,Soft Quality\";}s:5:\"sizes\";a:2:{i:0;s:11:\"240cm, 566c\";i:1;s:11:\"240cm, 566c\";}s:11:\"temp_img_id\";s:1:\"4\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1524, 126, 'product_video_description', ''),
(1525, 126, 'product_video', ''),
(1526, 127, 'slug', 'space-1'),
(1527, 127, 'price', '68'),
(1528, 127, 'sale_price', ''),
(1529, 127, 'sale_start_date', ''),
(1530, 127, 'sale_end_date', ''),
(1531, 127, 'sku', ''),
(1532, 127, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1533, 127, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1534, 127, 'featured_image', 'upload/product/space-1/feature/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg'),
(1535, 127, 'stock', ''),
(1536, 127, 'material', 'Heavy Material,Soft Quality'),
(1537, 127, 'sizes', '240cm, 566c'),
(1538, 127, 'product_features_description', ''),
(1539, 127, 'stock_manage_chk', ''),
(1540, 127, 'stock_threshold', ''),
(1541, 127, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"s0JtAbtSjXp6N42hL2LNJAw375OKLUL5fqnViji4\";s:12:\"product_name\";a:2:{i:0;s:14:\"Galactic Atoms\";i:1;s:7:\"Space 1\";}s:10:\"image_name\";a:2:{i:0;s:73:\"upload/product_temperory_img/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png\";i:1;s:74:\"upload/product_temperory_img/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg\";}s:12:\"image_row_id\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"4\";}s:10:\"categories\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:5:\"price\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";i:1;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";}s:8:\"material\";a:2:{i:0;s:27:\"Heavy Material,Soft Quality\";i:1;s:27:\"Heavy Material,Soft Quality\";}s:5:\"sizes\";a:2:{i:0;s:11:\"240cm, 566c\";i:1;s:11:\"240cm, 566c\";}s:11:\"temp_img_id\";s:1:\"4\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1542, 127, 'product_video_description', ''),
(1543, 127, 'product_video', '');

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `flag` varchar(255) DEFAULT NULL,
  `description` longtext,
  `image` longtext,
  `video` longtext,
  `parent_id` int(11) DEFAULT NULL,
  `voted` varchar(255) DEFAULT NULL,
  `unvoted` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `user_id`, `product_id`, `status`, `flag`, `description`, `image`, `video`, `parent_id`, `voted`, `unvoted`, `created_at`, `updated_at`) VALUES
(5, 1, 126, 'on', 'average', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies tortor sit amet sagittis imperdiet.', 'a:2:{i:0;s:95:\"upload/product_review/product_126/user_1/review_5/Mr6p0B01tHarH79e8Poy9G2J3s0wbP758NEv2sZg.jpeg\";i:1;s:95:\"upload/product_review/product_126/user_1/review_5/r0urDvXmvaztMO1AwRId5ZAaeYLx8ax8p3EiyRmQ.jpeg\";}', NULL, NULL, NULL, NULL, '2020-04-26 15:27:34', '2020-04-26 15:27:35');

-- --------------------------------------------------------

--
-- Table structure for table `product_tag`
--

CREATE TABLE `product_tag` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_tag`
--

INSERT INTO `product_tag` (`id`, `product_id`, `tag_id`) VALUES
(19, 53, 6),
(21, 55, 5),
(22, 55, 7),
(23, 55, 9),
(24, 56, 6),
(25, 56, 7),
(26, 56, 9),
(27, 57, 6),
(28, 57, 7),
(29, 58, 7),
(30, 58, 8),
(31, 58, 9),
(32, 59, 9),
(33, 60, 8),
(34, 61, 6),
(35, 61, 7),
(36, 61, 8),
(37, 62, 7),
(38, 62, 8),
(39, 63, 7),
(40, 63, 8),
(41, 64, 7),
(42, 64, 8),
(43, 65, 6),
(44, 66, 8),
(45, 66, 9),
(59, 74, 6),
(60, 74, 7),
(61, 74, 8),
(62, 74, 9),
(69, 77, 6),
(70, 77, 8),
(71, 77, 9),
(122, 102, 8),
(123, 102, 9),
(126, 104, 5),
(127, 104, 6),
(128, 104, 9),
(129, 105, 5),
(130, 105, 6),
(131, 105, 9),
(132, 106, 5),
(133, 106, 6),
(134, 106, 9),
(135, 107, 5),
(136, 107, 6),
(137, 107, 9),
(138, 108, 5),
(139, 108, 6),
(140, 108, 9),
(141, 109, 5),
(142, 109, 6),
(143, 109, 9),
(144, 110, 5),
(145, 110, 6),
(146, 110, 9),
(147, 111, 5),
(148, 111, 6),
(149, 111, 9),
(150, 112, 5),
(151, 112, 6),
(152, 112, 9),
(153, 113, 5),
(154, 113, 6),
(155, 113, 9),
(156, 114, 5),
(157, 114, 6),
(158, 114, 9),
(159, 115, 5),
(160, 115, 6),
(161, 115, 9),
(162, 116, 5),
(163, 116, 6),
(164, 116, 9),
(165, 117, 5),
(166, 117, 6),
(167, 117, 9),
(168, 118, 5),
(169, 118, 6),
(170, 118, 9),
(171, 119, 5),
(172, 119, 6),
(173, 119, 9),
(174, 120, 5),
(175, 120, 6),
(176, 120, 9),
(177, 121, 5),
(178, 121, 6),
(179, 121, 9),
(180, 122, 5),
(181, 122, 6),
(182, 122, 9),
(183, 123, 5),
(184, 123, 6),
(185, 124, 6),
(186, 124, 9),
(187, 125, 6),
(188, 125, 9),
(189, 126, 6),
(190, 126, 9),
(191, 127, 6),
(192, 127, 9);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-02-13 15:14:24', '2020-02-13 15:14:24'),
(2, 'vendor', 'web', '2020-02-13 15:14:33', '2020-02-13 15:14:33'),
(3, 'user', 'web', '2020-02-13 15:14:39', '2020-02-13 15:14:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_setting`
--

CREATE TABLE `site_setting` (
  `id` int(11) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_setting`
--

INSERT INTO `site_setting` (`id`, `key`, `value`) VALUES
(1, 'site_maintenance_status', 'off');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `alias`, `created_at`, `updated_at`) VALUES
(5, 'hello', 'hello', '2020-02-22 07:04:07', '2020-02-22 07:04:07'),
(6, 'Arts', 'arts', '2020-02-23 03:57:18', '2020-02-23 03:57:18'),
(7, 'Garments', 'garments', '2020-02-23 03:57:23', '2020-02-23 03:57:23'),
(8, 'Discovery', 'discovery', '2020-02-23 03:57:29', '2020-02-23 03:57:29'),
(9, 'Abstract', 'abstract', '2020-02-23 03:57:42', '2020-02-23 03:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `verified` int(11) DEFAULT '0',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `profile_pic` longtext COLLATE utf8_unicode_ci,
  `provider_id` longtext COLLATE utf8_unicode_ci,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_customer_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `verified`, `password`, `status`, `profile_pic`, `provider_id`, `provider`, `remember_token`, `stripe_customer_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, 1, '$2y$10$EdCgnoxnbqMxzpJGTdCkC.EPSc5YcsAzXS6w/doZpJ.uOEr3asrA.', 1, NULL, NULL, NULL, NULL, NULL, '2020-02-13 15:20:20', '2020-02-13 15:20:31'),
(21, 'Yasin Sabir', 'yasin.100@hotmail.com', NULL, 1, '$2y$10$70bnco.KAKfRVljRgEyppOA/l/S7OyUvPGECOHBruwqTmzreiBsFu', 1, NULL, NULL, NULL, NULL, NULL, '2020-04-05 05:07:52', '2020-04-05 07:34:38');

-- --------------------------------------------------------

--
-- Table structure for table `users_meta`
--

CREATE TABLE `users_meta` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_meta`
--

INSERT INTO `users_meta` (`id`, `user_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(63, 1, 'street_name', 'standfield rd', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(64, 1, 'street_no', 'ivor rd - 0984', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(66, 1, 'city', 'monaal', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(67, 1, 'telno', '034233455555', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(68, 1, 'shipping_address', 'sdsdasdxczxczxcz', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(69, 1, 'billing_address', 'fefrgredffvvxvvxxxz', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(71, 1, 'country', 'Argentina', '2020-03-28 07:34:05', '2020-03-28 07:34:05'),
(92, 21, 'street_name', 'qwert no222 RD', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(93, 21, 'street_no', 'sdd', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(94, 21, 'country', 'Afghanistan', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(95, 21, 'city', 'Karachi', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(96, 21, 'telno', '3131312', '2020-04-05 05:07:52', '2020-04-05 05:07:52');

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `verify_users`
--

INSERT INTO `verify_users` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, 1, 'YQ18USY0zHuDuu3stUrLoHKNQoCJWAYmN7XUaL9B', '2020-02-13 15:20:20', '2020-02-13 15:20:20'),
(2, 2, 'ZOoBKMmAHcCGCH4cIu9I5US7GgqmBqMd76g9plX2', '2020-02-13 15:22:25', '2020-02-13 15:22:25'),
(3, 17, 'BDomKCMmy1stRhyG7AVS263MkrXbyGq1RWbtFBUy', '2020-04-05 04:09:27', '2020-04-05 04:09:27'),
(4, 18, 'uKdLWg7vBLkFeOS1QaNDPIIOiI0dkH8UMf2mXD2L', '2020-04-05 04:56:21', '2020-04-05 04:56:21'),
(5, 19, 'YJUs9z2gK4HdOsdP9Zw865LX6D7LoSceq2HTJS0o', '2020-04-05 05:01:41', '2020-04-05 05:01:41'),
(6, 20, 'P2m7Epvt2JMTRdiysFP4iigqp3q2aDaJycGYeZKy', '2020-04-05 05:04:00', '2020-04-05 05:04:00'),
(7, 21, 'Kla1bxjm6lm6BlN4fmEzyScQb0QOOPZd5IuS1UqJ', '2020-04-05 05:07:52', '2020-04-05 05:07:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_meta`
--
ALTER TABLE `category_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_setting`
--
ALTER TABLE `category_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shop_country_code_unique` (`code`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_product`
--
ALTER TABLE `coupon_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_user`
--
ALTER TABLE `coupon_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_reviews`
--
ALTER TABLE `customer_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_galleries`
--
ALTER TABLE `products_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_metas`
--
ALTER TABLE `products_metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tag`
--
ALTER TABLE `product_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `site_setting`
--
ALTER TABLE `site_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_meta`
--
ALTER TABLE `users_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artists`
--
ALTER TABLE `artists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `category_meta`
--
ALTER TABLE `category_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;

--
-- AUTO_INCREMENT for table `category_setting`
--
ALTER TABLE `category_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_product`
--
ALTER TABLE `coupon_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_user`
--
ALTER TABLE `coupon_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_reviews`
--
ALTER TABLE `customer_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `products_galleries`
--
ALTER TABLE `products_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `products_metas`
--
ALTER TABLE `products_metas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1544;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_tag`
--
ALTER TABLE `product_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `site_setting`
--
ALTER TABLE `site_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users_meta`
--
ALTER TABLE `users_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
