-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2020 at 08:35 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `order_details` longtext NOT NULL,
  `product_detail` longtext NOT NULL,
  `user_meta` longtext NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subtotal` varchar(200) NOT NULL,
  `shipping` varchar(200) DEFAULT NULL,
  `discount` varchar(200) DEFAULT NULL,
  `tax` varchar(200) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_email`, `order_details`, `product_detail`, `user_meta`, `user_id`, `subtotal`, `shipping`, `discount`, `tax`, `status`, `created_at`, `updated_at`) VALUES
(3, 'ahsan.amin334@gmail.com', 'a:9:{s:6:\"_token\";s:40:\"wJzbtuDjuK6xbXZk6XtgAjHut5ypY4D70NeWtpDL\";s:4:\"name\";s:5:\"Ahsan\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:7:\"address\";s:7:\"testing\";s:6:\"mobile\";s:10:\"+358423423\";s:5:\"pcode\";s:6:\"354354\";s:4:\"city\";s:6:\"kaghan\";s:9:\"d-country\";s:11:\"Afghanistan\";s:7:\"payment\";s:6:\"paypal\";}', '[{\"id\":40,\"user_id\":\"1\",\"product_id\":\"92\",\"product_name\":\"photo2\",\"product_meta\":\"{\\\"id\\\":92,\\\"user_id\\\":1,\\\"title\\\":\\\"photo2\\\",\\\"slug\\\":\\\"photo2\\\",\\\"description\\\":null,\\\"sku\\\":null,\\\"image\\\":\\\"upload\\\\\\/product\\\\\\/photo2\\\\\\/feature\\\\\\/ihJGniKXqFeEUWRGRiIBLn00zB5oTXAgx8JDYrMF.png\\\",\\\"video\\\":null,\\\"price\\\":50,\\\"stock\\\":null,\\\"status\\\":1,\\\"created_at\\\":\\\"2020-03-23 11:47:29\\\",\\\"updated_at\\\":\\\"2020-03-23 11:47:29\\\"}\",\"image\":\"upload\\/product\\/photo2\\/feature\\/ihJGniKXqFeEUWRGRiIBLn00zB5oTXAgx8JDYrMF.png\",\"user_meta\":\"{\\\"id\\\":1,\\\"name\\\":\\\"admin\\\",\\\"email\\\":\\\"admin@admin.com\\\",\\\"email_verified_at\\\":null,\\\"verified\\\":1,\\\"status\\\":1,\\\"provider_id\\\":null,\\\"provider\\\":null,\\\"created_at\\\":\\\"2020-02-05 10:37:29\\\",\\\"updated_at\\\":\\\"2020-02-05 10:37:29\\\"}\",\"price\":\"50\",\"qty\":1,\"material\":null,\"size\":null,\"extra\":\"Yes\",\"created_at\":\"2020-03-29 11:06:07\",\"updated_at\":\"2020-03-29 11:06:07\"}]', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', 1, '50', '', '', '', 'onhold', '2020-03-29 06:15:35', '2020-03-29 06:15:35'),
(6, 'ahsan.amin334@gmail.com', 'a:9:{s:6:\"_token\";s:40:\"wJzbtuDjuK6xbXZk6XtgAjHut5ypY4D70NeWtpDL\";s:4:\"name\";s:10:\"ahsan amin\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:7:\"address\";s:7:\"testing\";s:6:\"mobile\";s:10:\"+358423423\";s:5:\"pcode\";s:6:\"354354\";s:4:\"city\";s:6:\"kaghan\";s:9:\"d-country\";s:14:\"Åland Islands\";s:7:\"payment\";s:6:\"paypal\";}', '[{\"id\":44,\"user_id\":\"1\",\"product_id\":\"99\",\"product_name\":\"Bike 19\",\"product_meta\":\"{\\\"id\\\":99,\\\"user_id\\\":1,\\\"title\\\":\\\"Bike 19\\\",\\\"slug\\\":\\\"bike-19\\\",\\\"description\\\":null,\\\"sku\\\":null,\\\"image\\\":\\\"upload\\\\\\/product\\\\\\/bike-19\\\\\\/feature\\\\\\/U3mEjvQpMeySm9YNHfNAxrDIRhwX8XX4y692wTDB.jpeg\\\",\\\"video\\\":null,\\\"price\\\":50,\\\"stock\\\":null,\\\"status\\\":1,\\\"created_at\\\":\\\"2020-03-23 13:34:28\\\",\\\"updated_at\\\":\\\"2020-03-23 13:34:28\\\"}\",\"image\":\"upload\\/product\\/bike-19\\/feature\\/U3mEjvQpMeySm9YNHfNAxrDIRhwX8XX4y692wTDB.jpeg\",\"user_meta\":\"{\\\"id\\\":1,\\\"name\\\":\\\"admin\\\",\\\"email\\\":\\\"admin@admin.com\\\",\\\"email_verified_at\\\":null,\\\"verified\\\":1,\\\"status\\\":1,\\\"provider_id\\\":null,\\\"provider\\\":null,\\\"created_at\\\":\\\"2020-02-05 10:37:29\\\",\\\"updated_at\\\":\\\"2020-02-05 10:37:29\\\"}\",\"price\":\"50\",\"qty\":1,\"material\":null,\"size\":null,\"extra\":\"Yes\",\"created_at\":\"2020-03-29 11:39:32\",\"updated_at\":\"2020-03-29 11:39:32\"},{\"id\":45,\"user_id\":\"1\",\"product_id\":\"90\",\"product_name\":\"avatar5\",\"product_meta\":\"{\\\"id\\\":90,\\\"user_id\\\":1,\\\"title\\\":\\\"avatar5\\\",\\\"slug\\\":\\\"avatar5\\\",\\\"description\\\":null,\\\"sku\\\":null,\\\"image\\\":\\\"upload\\\\\\/product\\\\\\/avatar5\\\\\\/feature\\\\\\/lOTD1rxBRJRub8FriEERAFNIudMjrRYMIwMzw3nj.png\\\",\\\"video\\\":\\\"null\\\",\\\"price\\\":50,\\\"stock\\\":null,\\\"status\\\":1,\\\"created_at\\\":\\\"2020-03-23 11:47:28\\\",\\\"updated_at\\\":\\\"2020-03-23 13:37:58\\\"}\",\"image\":\"upload\\/product\\/avatar5\\/feature\\/lOTD1rxBRJRub8FriEERAFNIudMjrRYMIwMzw3nj.png\",\"user_meta\":\"{\\\"id\\\":1,\\\"name\\\":\\\"admin\\\",\\\"email\\\":\\\"admin@admin.com\\\",\\\"email_verified_at\\\":null,\\\"verified\\\":1,\\\"status\\\":1,\\\"provider_id\\\":null,\\\"provider\\\":null,\\\"created_at\\\":\\\"2020-02-05 10:37:29\\\",\\\"updated_at\\\":\\\"2020-02-05 10:37:29\\\"}\",\"price\":\"50\",\"qty\":1,\"material\":\"SIZE\",\"size\":\"SIZE\",\"extra\":\"Yes\",\"created_at\":\"2020-03-29 11:39:43\",\"updated_at\":\"2020-03-29 11:39:43\"}]', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', 2, '100', '', '', '', 'onhold', '2020-03-29 06:41:01', '2020-03-29 06:41:01'),
(7, 'ahsan.amin334@gmail.com', 'a:9:{s:6:\"_token\";s:40:\"wJzbtuDjuK6xbXZk6XtgAjHut5ypY4D70NeWtpDL\";s:4:\"name\";s:10:\"ahsan amin\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:7:\"address\";s:7:\"testing\";s:6:\"mobile\";s:10:\"+358423423\";s:5:\"pcode\";s:6:\"354354\";s:4:\"city\";s:6:\"kaghan\";s:9:\"d-country\";s:14:\"Åland Islands\";s:7:\"payment\";s:6:\"paypal\";}', '[{\"id\":46,\"user_id\":\"1\",\"product_id\":\"98\",\"product_name\":\"default-placeholder-avatar\",\"product_meta\":\"{\\\"id\\\":98,\\\"user_id\\\":1,\\\"title\\\":\\\"default-placeholder-avatar\\\",\\\"slug\\\":\\\"default-placeholder-avatar\\\",\\\"description\\\":null,\\\"sku\\\":null,\\\"image\\\":\\\"upload\\\\\\/product\\\\\\/default-placeholder-avatar\\\\\\/feature\\\\\\/hLDrwMXyclmNeWxqDUwLZLuKAHuRqyKXoWkYDAD5.png\\\",\\\"video\\\":null,\\\"price\\\":50,\\\"stock\\\":null,\\\"status\\\":1,\\\"created_at\\\":\\\"2020-03-23 13:28:15\\\",\\\"updated_at\\\":\\\"2020-03-23 13:28:15\\\"}\",\"image\":\"upload\\/product\\/default-placeholder-avatar\\/feature\\/hLDrwMXyclmNeWxqDUwLZLuKAHuRqyKXoWkYDAD5.png\",\"user_meta\":\"{\\\"id\\\":1,\\\"name\\\":\\\"admin\\\",\\\"email\\\":\\\"admin@admin.com\\\",\\\"email_verified_at\\\":null,\\\"verified\\\":1,\\\"status\\\":1,\\\"provider_id\\\":null,\\\"provider\\\":null,\\\"created_at\\\":\\\"2020-02-05 10:37:29\\\",\\\"updated_at\\\":\\\"2020-02-05 10:37:29\\\"}\",\"price\":\"50\",\"qty\":1,\"material\":null,\"size\":null,\"extra\":\"Yes\",\"created_at\":\"2020-03-29 11:43:19\",\"updated_at\":\"2020-03-29 11:43:19\"},{\"id\":47,\"user_id\":\"1\",\"product_id\":\"92\",\"product_name\":\"photo2\",\"product_meta\":\"{\\\"id\\\":92,\\\"user_id\\\":1,\\\"title\\\":\\\"photo2\\\",\\\"slug\\\":\\\"photo2\\\",\\\"description\\\":null,\\\"sku\\\":null,\\\"image\\\":\\\"upload\\\\\\/product\\\\\\/photo2\\\\\\/feature\\\\\\/ihJGniKXqFeEUWRGRiIBLn00zB5oTXAgx8JDYrMF.png\\\",\\\"video\\\":null,\\\"price\\\":50,\\\"stock\\\":null,\\\"status\\\":1,\\\"created_at\\\":\\\"2020-03-23 11:47:29\\\",\\\"updated_at\\\":\\\"2020-03-23 11:47:29\\\"}\",\"image\":\"upload\\/product\\/photo2\\/feature\\/ihJGniKXqFeEUWRGRiIBLn00zB5oTXAgx8JDYrMF.png\",\"user_meta\":\"{\\\"id\\\":1,\\\"name\\\":\\\"admin\\\",\\\"email\\\":\\\"admin@admin.com\\\",\\\"email_verified_at\\\":null,\\\"verified\\\":1,\\\"status\\\":1,\\\"provider_id\\\":null,\\\"provider\\\":null,\\\"created_at\\\":\\\"2020-02-05 10:37:29\\\",\\\"updated_at\\\":\\\"2020-02-05 10:37:29\\\"}\",\"price\":\"50\",\"qty\":1,\"material\":null,\"size\":null,\"extra\":\"Yes\",\"created_at\":\"2020-03-29 11:43:23\",\"updated_at\":\"2020-03-29 11:43:23\"}]', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', 1, '100', '', '', '', 'onhold', '2020-03-29 06:46:13', '2020-03-29 06:46:13'),
(8, 'ahsan.amin334@gmail.com', 'a:9:{s:6:\"_token\";s:40:\"wJzbtuDjuK6xbXZk6XtgAjHut5ypY4D70NeWtpDL\";s:4:\"name\";s:10:\"ahsan amin\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:7:\"address\";s:7:\"testing\";s:6:\"mobile\";s:10:\"+358423423\";s:5:\"pcode\";s:6:\"354354\";s:4:\"city\";s:6:\"kaghan\";s:9:\"d-country\";s:11:\"Afghanistan\";s:7:\"payment\";s:6:\"paypal\";}', '[{\"id\":48,\"user_id\":\"1\",\"product_id\":\"98\",\"product_name\":\"default-placeholder-avatar\",\"product_meta\":\"{\\\"id\\\":98,\\\"user_id\\\":1,\\\"title\\\":\\\"default-placeholder-avatar\\\",\\\"slug\\\":\\\"default-placeholder-avatar\\\",\\\"description\\\":null,\\\"sku\\\":null,\\\"image\\\":\\\"upload\\\\\\/product\\\\\\/default-placeholder-avatar\\\\\\/feature\\\\\\/hLDrwMXyclmNeWxqDUwLZLuKAHuRqyKXoWkYDAD5.png\\\",\\\"video\\\":null,\\\"price\\\":50,\\\"stock\\\":null,\\\"status\\\":1,\\\"created_at\\\":\\\"2020-03-23 13:28:15\\\",\\\"updated_at\\\":\\\"2020-03-23 13:28:15\\\"}\",\"image\":\"upload\\/product\\/default-placeholder-avatar\\/feature\\/hLDrwMXyclmNeWxqDUwLZLuKAHuRqyKXoWkYDAD5.png\",\"user_meta\":\"{\\\"id\\\":1,\\\"name\\\":\\\"admin\\\",\\\"email\\\":\\\"admin@admin.com\\\",\\\"email_verified_at\\\":null,\\\"verified\\\":1,\\\"status\\\":1,\\\"provider_id\\\":null,\\\"provider\\\":null,\\\"created_at\\\":\\\"2020-02-05 10:37:29\\\",\\\"updated_at\\\":\\\"2020-02-05 10:37:29\\\"}\",\"price\":\"50\",\"qty\":1,\"material\":null,\"size\":null,\"extra\":\"Yes\",\"created_at\":\"2020-03-29 11:47:52\",\"updated_at\":\"2020-03-29 11:47:52\"},{\"id\":49,\"user_id\":\"1\",\"product_id\":\"89\",\"product_name\":\"QRSTU\",\"product_meta\":\"{\\\"id\\\":89,\\\"user_id\\\":1,\\\"title\\\":\\\"QRSTU\\\",\\\"slug\\\":\\\"qrstu\\\",\\\"description\\\":null,\\\"sku\\\":\\\"sku-10001\\\",\\\"image\\\":\\\"upload\\\\\\/product\\\\\\/qrstu\\\\\\/feature\\\\\\/e6PY0wJ0d5UC7a541CDLfTjN5dz4r2jLBolWccyC.jpeg\\\",\\\"video\\\":\\\"null\\\",\\\"price\\\":30,\\\"stock\\\":26,\\\"status\\\":1,\\\"created_at\\\":\\\"2020-03-15 19:55:41\\\",\\\"updated_at\\\":\\\"2020-03-15 19:55:41\\\"}\",\"image\":\"upload\\/product\\/qrstu\\/feature\\/e6PY0wJ0d5UC7a541CDLfTjN5dz4r2jLBolWccyC.jpeg\",\"user_meta\":\"{\\\"id\\\":1,\\\"name\\\":\\\"admin\\\",\\\"email\\\":\\\"admin@admin.com\\\",\\\"email_verified_at\\\":null,\\\"verified\\\":1,\\\"status\\\":1,\\\"provider_id\\\":null,\\\"provider\\\":null,\\\"created_at\\\":\\\"2020-02-05 10:37:29\\\",\\\"updated_at\\\":\\\"2020-02-05 10:37:29\\\"}\",\"price\":\"30\",\"qty\":1,\"material\":\"SIZE\",\"size\":\"SIZE\",\"extra\":\"Yes\",\"created_at\":\"2020-03-29 11:48:01\",\"updated_at\":\"2020-03-29 11:48:01\"}]', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', 1, '80', '', '', '', 'onhold', '2020-03-29 06:48:57', '2020-03-29 06:48:57'),
(9, 'ahsan.amin334@gmail.com', 'a:9:{s:6:\"_token\";s:40:\"wJzbtuDjuK6xbXZk6XtgAjHut5ypY4D70NeWtpDL\";s:4:\"name\";s:10:\"Ahsan Amin\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:7:\"address\";s:7:\"Houston\";s:6:\"mobile\";s:8:\"32312321\";s:5:\"pcode\";s:6:\"354354\";s:4:\"city\";s:6:\"kaghan\";s:9:\"d-country\";s:6:\"Angola\";s:7:\"payment\";s:6:\"paypal\";}', '[{\"id\":50,\"user_id\":\"1\",\"product_id\":\"88\",\"product_name\":\"LMNOP\",\"product_meta\":\"{\\\"id\\\":88,\\\"user_id\\\":1,\\\"title\\\":\\\"LMNOP\\\",\\\"slug\\\":\\\"lmnop\\\",\\\"description\\\":null,\\\"sku\\\":\\\"sku-1000\\\",\\\"image\\\":\\\"upload\\\\\\/product\\\\\\/lmnop\\\\\\/feature\\\\\\/vclT40UXpSNeYikWGH32GpczJLiUNkWYn9R1BMQZ.png\\\",\\\"video\\\":\\\"null\\\",\\\"price\\\":20,\\\"stock\\\":20,\\\"status\\\":1,\\\"created_at\\\":\\\"2020-03-15 19:55:41\\\",\\\"updated_at\\\":\\\"2020-03-15 19:55:41\\\"}\",\"image\":\"upload\\/product\\/lmnop\\/feature\\/vclT40UXpSNeYikWGH32GpczJLiUNkWYn9R1BMQZ.png\",\"user_meta\":\"{\\\"id\\\":1,\\\"name\\\":\\\"admin\\\",\\\"email\\\":\\\"admin@admin.com\\\",\\\"email_verified_at\\\":null,\\\"verified\\\":1,\\\"status\\\":1,\\\"provider_id\\\":null,\\\"provider\\\":null,\\\"created_at\\\":\\\"2020-02-05 10:37:29\\\",\\\"updated_at\\\":\\\"2020-02-05 10:37:29\\\"}\",\"price\":\"20\",\"qty\":1,\"material\":\"SIZE\",\"size\":\"SIZE\",\"extra\":\"Yes\",\"created_at\":\"2020-03-29 11:51:03\",\"updated_at\":\"2020-03-29 11:51:03\"},{\"id\":51,\"user_id\":\"1\",\"product_id\":\"87\",\"product_name\":\"XYZ\",\"product_meta\":\"{\\\"id\\\":87,\\\"user_id\\\":1,\\\"title\\\":\\\"XYZ\\\",\\\"slug\\\":\\\"xyz\\\",\\\"description\\\":\\\"XYZ\\\",\\\"sku\\\":\\\"sku-001\\\",\\\"image\\\":\\\"upload\\\\\\/product\\\\\\/xyz\\\\\\/feature\\\\\\/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg\\\",\\\"video\\\":\\\"null\\\",\\\"price\\\":20,\\\"stock\\\":1,\\\"status\\\":1,\\\"created_at\\\":\\\"2020-03-12 05:12:37\\\",\\\"updated_at\\\":\\\"2020-03-20 21:19:31\\\"}\",\"image\":\"upload\\/product\\/xyz\\/feature\\/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg\",\"user_meta\":\"{\\\"id\\\":1,\\\"name\\\":\\\"admin\\\",\\\"email\\\":\\\"admin@admin.com\\\",\\\"email_verified_at\\\":null,\\\"verified\\\":1,\\\"status\\\":1,\\\"provider_id\\\":null,\\\"provider\\\":null,\\\"created_at\\\":\\\"2020-02-05 10:37:29\\\",\\\"updated_at\\\":\\\"2020-02-05 10:37:29\\\"}\",\"price\":\"20\",\"qty\":1,\"material\":\"Hard\",\"size\":\"100\",\"extra\":\"Yes\",\"created_at\":\"2020-03-29 11:51:13\",\"updated_at\":\"2020-03-29 11:51:13\"}]', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', 1, '40', '', '', '', 'onhold', '2020-03-29 06:52:24', '2020-03-29 06:52:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
