-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2020 at 04:41 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4,
  `sku` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `image` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `price` int(200) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `slug`, `description`, `sku`, `image`, `price`, `stock`, `status`, `created_at`, `updated_at`) VALUES
(37, 'sadasdsdasdasdas', 'sadasdsdasdasdas', NULL, 'sde-432', 'upload/product/sadasdsdasdasdas/feature/nrvPgpjo6s8oFs2lnLsdm927rhKfC3YLHmaMR5rA.png', 123, 1, 1, '2020-02-22 07:56:05', '2020-02-22 07:56:05'),
(38, 'Classic Formal shirt', 'classic-formal-shirt', 'sdadasdasdasdasds', 'sde-432', 'upload/product/classic-formal-shirt/feature/mbQrJN2zTuCusWSkpprCd1osWrpdTmHh0kqyNGbV.png', 123, 1, 1, '2020-02-22 07:58:05', '2020-02-22 07:58:05'),
(40, 'product - 001wdd', 'product-001wdd', 'sdasdsdasdsd', 'sde-432', 'upload/product/product-001wdd/feature/VZwHNccl1EAB9gOaVManql86fPkYxnoHhfMdHPKt.png', 123, 1, 1, '2020-02-22 08:20:40', '2020-02-22 08:20:40'),
(41, 'p1', 'p1', 'sdsadas', NULL, 'upload/product/p1/feature/nUjF15OBENR4KNgyYtFdjAwxiPqh3099TfDD4VrV.png', NULL, 1, 1, '2020-02-22 08:37:10', '2020-02-22 08:37:10'),
(42, 'dummy my product', 'dummy-my-product', NULL, 'sde-433', 'upload/product/dummy-my-product/feature/sFFSKhlq4mdEBSRTG3VH6059m7JLwnj1aZsLRsoK.jpeg', 34, 1, 1, '2020-02-22 10:37:32', '2020-02-22 10:37:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
