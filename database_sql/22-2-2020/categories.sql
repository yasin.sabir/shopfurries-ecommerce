-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2020 at 04:41 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Alias` varchar(255) NOT NULL,
  `Image` longtext,
  `Parent` int(11) DEFAULT NULL,
  `Top` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `Name`, `Alias`, `Image`, `Parent`, `Top`, `Status`, `created_at`, `updated_at`) VALUES
(42, 'Mens', 'mens', 'null', 0, 1, 1, '2020-02-22 02:25:46', '2020-02-22 02:25:46'),
(43, 'Shirts', 'shirts', 'null', 42, 0, 1, '2020-02-22 02:26:01', '2020-02-22 02:26:01'),
(44, 'Formal', 'formal', 'null', 43, 0, 1, '2020-02-22 02:28:00', '2020-02-22 02:28:00'),
(45, 'formal-cat-1', 'formal-cat-1', 'null', 44, 0, 1, '2020-02-22 04:24:39', '2020-02-22 04:24:39'),
(49, 'Womans', 'womans', 'null', 0, 1, 1, '2020-02-22 04:52:15', '2020-02-22 04:52:15'),
(50, 'kurti', 'kurti', 'null', 49, 0, 1, '2020-02-22 04:52:37', '2020-02-22 04:52:37'),
(51, 'formal khurti', 'formal-khurti', 'null', 49, 0, 1, '2020-02-22 05:08:55', '2020-02-22 05:08:55'),
(52, 'formal-cat-kurti-1', 'formal-cat-kurti-1', 'null', 51, 0, 1, '2020-02-22 05:09:11', '2020-02-22 05:09:11'),
(53, 'formal-cat-kurti-2', 'formal-cat-kurti-2', 'null', 51, 0, 1, '2020-02-22 05:09:43', '2020-02-22 05:09:43'),
(54, 'qwer', 'qwer', 'null', 52, 0, 1, '2020-02-22 05:09:56', '2020-02-22 05:09:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
