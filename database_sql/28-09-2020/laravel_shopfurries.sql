-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2020 at 10:51 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `image` longtext,
  `status` varchar(255) DEFAULT NULL,
  `additional_details` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`, `email`, `image`, `status`, `additional_details`, `created_at`, `updated_at`) VALUES
(9, 'Jackie Wilson', 'wilson@gmail.com', 'upload/artists/9/1bxVvBuVNJeVEpzRN3F2iClyNXdpnDCn1z41XNYI.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-04-15 15:39:33', '2020-04-16 15:23:59'),
(11, 'Frenandus', 'frenan@gmail.com', 'upload/artists/11/MTxXt9mMzzBpvgqYPZlPV91l599qe4CcWwKW5hTf.jpeg', 'on', 'a:5:{s:8:\"facebook\";N;s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 14:31:18', '2020-05-05 14:31:18'),
(12, 'Ahmed', 'ahmed@gmail.com', 'upload/artists/12/9YqZVb25aBM4079T78OrFeX7EXEwoa18eu34cIsi.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 14:58:41', '2020-05-05 14:58:41'),
(13, 'Runkin', 'runkin@gmail.com', 'upload/artists/13/jikjJ4aHttfSVvWfr8nPDJeOoxvd8f8ulhXUb2vy.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 14:59:07', '2020-05-05 14:59:07'),
(14, 'Scarlett', 'scarlett@outlook.com', 'upload/artists/14/UPWPkiPfDib3XpAYoaIZr1V9Qlbz93IN207yt3M3.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";s:25:\"https://www.instagram.com\";s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:00:05', '2020-05-05 15:00:05'),
(15, 'Grayson', 'grayson@yahoo.com', 'upload/artists/15/fIC17ucLIUPAueAw5E8gBzr2r2IvGrkaQ0xQuF04.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";s:25:\"https://www.instagram.com\";s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:00:44', '2020-05-05 15:00:44'),
(16, 'Watt', 'watt@gmail.com', 'upload/artists/16/Fnb9uF39GKDcZlK1T4C6EfYRhvhwAMY0kIYttJET.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:01:21', '2020-05-05 15:01:21'),
(17, 'Scott', 'scott@gmail.com', 'upload/artists/17/x5gw0xZei1T9al5cRHVaim8MjolE6jo7nyfPcmfR.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";s:25:\"https://www.instagram.com\";s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:01:49', '2020-05-05 15:01:50'),
(18, 'Ella', 'ella0192@yahoo.com', 'upload/artists/18/W71S04wD6XGPTlf6ayNL54s1AYHpLppAgIZOnME2.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:02:15', '2020-05-05 15:02:15'),
(19, 'Noreen', 'nor123@gmail.com', 'upload/artists/19/iNoiBi6b6J53XDPh3SvZNsBCjBCaJbT9YCDj9iuY.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";s:25:\"https://www.instagram.com\";s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:02:50', '2020-05-05 15:02:50');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Alias` varchar(255) NOT NULL,
  `Image` longtext,
  `Parent` varchar(255) DEFAULT NULL,
  `Top` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Additional_Req` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `Name`, `Alias`, `Image`, `Parent`, `Top`, `Status`, `Additional_Req`, `created_at`, `updated_at`) VALUES
(64, 'Mens', 'mens', 'upload/category/mens_64/thumbnail/uawAE6pguoTIylpcsomID6yLLjajz0Ub9JkUWGtp.jpeg', NULL, 1, 1, 0, '2020-03-21 12:50:20', '2020-05-19 20:13:20'),
(65, 'Shirts', 'shirts', 'upload/category/shirts_65/thumbnail/g8cUGSUwzFPdmqnfsLRt4PR95xX7GmhOUlmcHeey.jpeg', '64', 1, 1, 1, '2020-03-21 12:51:36', '2020-05-19 20:13:01'),
(66, 'Art', 'art', 'upload/category/art_66/thumbnail/8uOJzAHHvRuMYT3xXgHSRxgansfVFEULrUf4vCaW.jpeg', NULL, 1, 1, 1, '2020-03-28 04:16:47', '2020-05-19 20:12:52'),
(67, 'Abstract Art', 'abstract-art', 'upload/category/abstract-art_67/thumbnail/QwFtUcGXYQwHJ7t1cMFWNziWaJsycFFJnrVE7329.jpeg', NULL, 1, 1, 1, '2020-03-28 11:27:08', '2020-05-19 20:12:41'),
(68, 'Florida Shirts', 'florida-shirts', 'upload/category/florida-shirts_68/thumbnail/qRywPuYhFcq00gUG2wRLLjI9VXzRJxQMW7Xd00xR.png', NULL, 1, 1, 1, '2020-04-15 17:54:55', '2020-05-19 20:12:25'),
(70, 'Slim Shirts', 'slim-shirts', 'upload/category/slim-shirts_70/thumbnail/gfzIjvpQMZSiK6dkfemgP6c3h3TQKOnQfBpYRMCy.png', NULL, 1, 1, 0, '2020-04-21 15:27:24', '2020-05-19 20:12:10'),
(72, 'Slim Art Cups', 'slim-art-cups', 'upload/category/slim-art-cups_72/thumbnail/GSpA6DXHW7wvPhFp29sMMeP5BIZc4OdAIRYvNunz.jpeg', NULL, 1, 1, 1, '2020-05-04 13:38:50', '2020-07-22 15:44:23'),
(73, 'Armani Design', 'armani-design', 'upload/category/armani-design_73/thumbnail/Fq7DmGmfE64PeZRH1qO1xVeokza06uR8411YHYTy.jpeg', NULL, 1, 1, 1, '2020-05-07 14:02:16', '2020-05-18 16:18:18');

-- --------------------------------------------------------

--
-- Table structure for table `category_meta`
--

CREATE TABLE `category_meta` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category_meta`
--

INSERT INTO `category_meta` (`id`, `category_id`, `meta_key`, `meta_value`) VALUES
(42, 64, 'price', NULL),
(43, 64, 'tags', NULL),
(44, 64, 'poster_images', 'a:0:{}'),
(45, 64, 'actual_material_images', 'a:0:{}'),
(46, 64, 'category_reason_title', NULL),
(47, 64, 'category_reason_detail', NULL),
(48, 64, 'category_prod_features', NULL),
(49, 65, 'price', '68'),
(50, 65, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(51, 65, 'poster_images', 'a:2:{i:0;s:84:\"upload/category/shirts_65/posters-pics//SEYncNtoIVPXc6Icz5T0tr4Tn1NqCgCjZe5KPJl6.png\";i:1;s:84:\"upload/category/shirts_65/posters-pics//j2d9Vc7gbXp4tRaZHt2DwIToo43gv2xho8uuRDhU.png\";}'),
(52, 65, 'actual_material_images', 'a:3:{i:0;s:93:\"upload/category/shirts_65/actual-material-pics//Xa26lMVzr0dvkvo0YvEZUUPorns5l0ipTPcObeVM.jpeg\";i:1;s:93:\"upload/category/shirts_65/actual-material-pics//gVNjMKnFl5X8cqyOOlsSqs3IEgrCrUvYToAXJ6Kz.jpeg\";i:2;s:93:\"upload/category/shirts_65/actual-material-pics//CRd8ElNCy5zUnpF4NqXYRFQdvyklLBsosC6T7PmQ.jpeg\";}'),
(53, 65, 'category_reason_title', 'a:1:{i:0;s:8:\"Reason 1\";}'),
(54, 65, 'category_reason_detail', 'a:1:{i:0;s:11:\"dsdasdasdsd\";}'),
(55, 65, 'category_prod_features', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dapibus elit finibus, lacinia libero vel, mollis est. Phasellus vitae nulla nec quam pulvinar porttitor at quis ex. Vivamus dui lorem, elementum eget tristique et, auctor vitae lorem. Sed urna lacus, tempor a convallis ut, hendrerit at nisl. Curabitur aliquet consectetur cursus.'),
(56, 65, 'material', 'Heavy Material , Soft Quality'),
(57, 65, 'sizes', '240cm, 566c,'),
(58, 66, 'price', '56'),
(59, 66, 'tags', 'a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}'),
(60, 66, 'material', 'Heavy Material , Soft Quality'),
(61, 66, 'sizes', '240cm, 566cm'),
(62, 66, 'poster_images', 'a:2:{i:0;s:82:\"upload/category/art_66/posters-pics//Dg814bd92OZuxiqotzaDhZf65r1hEPzS4Gibeiaq.jpeg\";i:1;s:82:\"upload/category/art_66/posters-pics//APnAeuiKH20YpvwyTi4k98ZT4KcfyLjy1J0BRyUD.jpeg\";}'),
(63, 66, 'actual_material_images', 'a:3:{i:0;s:90:\"upload/category/art_66/actual-material-pics//rwRvDeYvwOzE0wD4mG2udBysdEQR9XkGWJCb35gM.jpeg\";i:1;s:90:\"upload/category/art_66/actual-material-pics//x6csmO81T9dYdlBvgY8kWQi343zYMjsmKXYzIaRQ.jpeg\";i:2;s:90:\"upload/category/art_66/actual-material-pics//ClyBnU6mVQKLfkA8lV25dZtTM53KgQb1n6WoEMIW.jpeg\";}'),
(64, 66, 'category_reason_title', 'a:1:{i:0;s:16:\"UNIQUE MATERIAL:\";}'),
(65, 66, 'category_reason_detail', 'a:1:{i:0;s:94:\"Unlike All Those Cheaply-Made Peach Skin Pillow Covers, You Can Finally Pamper Yourself With A\";}'),
(66, 66, 'category_prod_features', '<div><b>HELLO</b></div><ul><li>fdfdf</li><li>dfsdfsdf</li><li>fsdfsdfsd</li><li>dfsdfsdf</li></ul><p><b>Art Gallery</b></p><ul><li>fdfssfsdf</li><li>fsdfsdf</li><li>sdfdff</li><li>g343434</li></ul>'),
(67, 66, 'category_prod_feature_title', 'a:2:{i:0;s:51:\"HUG & CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";i:1;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(68, 66, 'category_prod_feature_detail', 'a:2:{i:0;s:220:\"Are You Ready To Meet Your Next Best Furry Friend? The Torben Goldmund\'s Art \'N\' Prints Anime Body Pillow Cases Are Here To Help You Sleep Better At Night Thanks To The Ultra-Soft Fabric And Eye-Catching Printed Designs.\";i:1;s:98:\"Are You Ready To Meet Your Next Best Furry Friend? The Torben Goldmund\'s Art \'N\' Prints Anime Body\";}'),
(69, 67, 'price', '59.0'),
(70, 67, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(71, 67, 'material', 'Heavy Material , Soft Quality'),
(72, 67, 'sizes', '240cm, 566cm'),
(73, 67, 'poster_images', 'a:1:{i:0;s:91:\"upload/category/abstract-art_67/posters-pics//nOgSD8dmGuttVIoemdeg5kYd4LzFDiU7whIp2d1D.jpeg\";}'),
(74, 67, 'actual_material_images', 'a:3:{i:0;s:99:\"upload/category/abstract-art_67/actual-material-pics//elr8jzjdXo2Zk2ecf3Nc3aLuVBTDnVEHPPeHTi1g.jpeg\";i:1;s:99:\"upload/category/abstract-art_67/actual-material-pics//OUyeH0U0DSaXC6FnGPPBDQS0rVXxiEJCn3PkYZvj.jpeg\";i:2;s:98:\"upload/category/abstract-art_67/actual-material-pics//Ff1TYqWXvH5fvtQan8yhSvY6kazxqfdhxBj3TaVi.png\";}'),
(75, 67, 'category_reason_title', 'a:3:{i:0;s:8:\"Reason 1\";i:1;s:8:\"Reason 2\";i:2;s:8:\"Reason 3\";}'),
(76, 67, 'category_reason_detail', 'a:3:{i:0;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";i:1;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";i:2;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";}'),
(77, 67, 'category_prod_feature_title', 'a:2:{i:0;s:51:\"HUG & CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";i:1;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(78, 67, 'category_prod_feature_detail', 'a:2:{i:0;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";i:1;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";}'),
(79, 64, 'material', NULL),
(80, 64, 'sizes', NULL),
(81, 64, 'category_prod_feature_title', NULL),
(82, 64, 'category_prod_feature_detail', NULL),
(83, 68, 'artist', '9'),
(84, 68, 'price', '68'),
(85, 68, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(86, 68, 'material', 'Heavy Material,Soft Quality'),
(87, 68, 'sizes', '240cm, 566c'),
(88, 68, 'poster_images', 'a:1:{i:0;s:93:\"upload/category/florida-shirts_68/posters-pics//NJmp4Bm27fWoXCKaALKrztM1df90YrEgjFlPfuxC.jpeg\";}'),
(89, 68, 'actual_material_images', 'a:1:{i:0;s:100:\"upload/category/florida-shirts_68/actual-material-pics//D14CAqm402Bf9RqhwlPzbFjPDHwdSjnrHPhp0hRO.png\";}'),
(90, 68, 'category_reason_title', 'a:1:{i:0;s:8:\"Reason 1\";}'),
(91, 68, 'category_reason_detail', 'a:1:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(92, 68, 'category_prod_feature_title', 'a:1:{i:0;s:11:\"SNUGGLIEST2\";}'),
(93, 68, 'category_prod_feature_detail', 'a:1:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(94, 67, 'artist', '9'),
(95, 69, 'artist', '6'),
(96, 69, 'price', '68'),
(97, 69, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"6\";}'),
(98, 69, 'material', 'Heavy-Material,Soft-Quality'),
(99, 69, 'sizes', '240cm,566c'),
(100, 69, 'poster_images', 'a:1:{i:0;s:88:\"upload/category/slim-card_69/posters-pics//MdkOrz7uqB2K0nVhZGdToWedMITcrmu7BaQnR1MF.jpeg\";}'),
(101, 69, 'actual_material_images', 'a:1:{i:0;s:96:\"upload/category/slim-card_69/actual-material-pics//9dat5yxfLFf2BticSOeE7Pf8IQ8dZrvpQuWFwNcM.jpeg\";}'),
(102, 69, 'category_reason_title', 'a:1:{i:0;s:16:\"UNIQUE MATERIAL:\";}'),
(103, 69, 'category_reason_detail', 'a:1:{i:0;s:17:\"dsasdasdasdasdasd\";}'),
(104, 69, 'category_prod_feature_title', 'a:1:{i:0;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(105, 69, 'category_prod_feature_detail', 'a:1:{i:0;s:13:\"dsdadsdasdasd\";}'),
(106, 70, 'artist', '17'),
(107, 70, 'price', NULL),
(108, 70, 'tags', NULL),
(109, 70, 'material', NULL),
(110, 70, 'sizes', NULL),
(111, 70, 'poster_images', 'a:0:{}'),
(112, 70, 'actual_material_images', 'a:0:{}'),
(113, 70, 'category_reason_title', NULL),
(114, 70, 'category_reason_detail', NULL),
(115, 70, 'category_prod_feature_title', NULL),
(116, 70, 'category_prod_feature_detail', NULL),
(128, 72, 'artist', '11'),
(129, 72, 'price', '145'),
(130, 72, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(131, 72, 'material', 'Heavy Material , Soft Quality'),
(132, 72, 'sizes', '240cm, 566cm'),
(133, 72, 'poster_images', 'a:2:{i:0;s:92:\"upload/category/slim-art-cups_72/posters-pics//Hjc8x8S7kTiB7exA5aLTmVY20oifMddJ1sIdFyit.jpeg\";i:1;s:91:\"upload/category/slim-art-cups_72/posters-pics//mtqHOgbWmZiC6MQMMyxJnjcjTZOlMEbkrhQKUDrl.png\";}'),
(134, 72, 'actual_material_images', 'a:1:{i:0;s:100:\"upload/category/slim-art-cups_72/actual-material-pics//7KwZCPRLU7bgbaPSTKDIBGaDjzcpVKQxBrRAICm2.jpeg\";}'),
(135, 72, 'category_reason_title', 'a:2:{i:0;s:8:\"Reason 1\";i:1;s:8:\"Reason 2\";}'),
(136, 72, 'category_reason_detail', 'a:2:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";i:1;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(137, 72, 'category_prod_feature_title', 'a:2:{i:0;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";i:1;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(138, 72, 'category_prod_feature_detail', 'a:2:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";i:1;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(139, 73, 'artist', '13'),
(140, 73, 'price', '126'),
(141, 73, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}'),
(142, 73, 'material', 'Heavy_Material,Soft_Quality'),
(143, 73, 'sizes', '240cm,566cm'),
(144, 73, 'poster_images', 'a:1:{i:0;s:92:\"upload/category/armani-design_73/posters-pics//sO4IlExAY9bOf1wq9kl0azUMokqir9cZURIjmaTt.jpeg\";}'),
(145, 73, 'actual_material_images', 'a:2:{i:0;s:100:\"upload/category/armani-design_73/actual-material-pics//tXJiNReXocLgjREGq2cqDAqFhkQyl1mFVByFwmYO.jpeg\";i:1;s:100:\"upload/category/armani-design_73/actual-material-pics//2vVQb8Zrg1Hp2jYALZcGX4hS8njYWuywNb7hZuWE.jpeg\";}'),
(146, 73, 'category_reason_title', 'a:1:{i:0;s:8:\"Reason 1\";}'),
(147, 73, 'category_reason_detail', 'a:1:{i:0;s:128:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus.\";}'),
(148, 73, 'category_prod_feature_title', 'a:1:{i:0;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(149, 73, 'category_prod_feature_detail', 'a:1:{i:0;s:128:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus.\";}'),
(150, 73, 'cate_sale_tax', '7'),
(151, 72, 'cate_sale_tax', '3'),
(152, 70, 'cate_sale_tax', '3'),
(153, 68, 'cate_sale_tax', '5'),
(154, 66, 'artist', '11'),
(155, 66, 'cate_sale_tax', '9'),
(156, 65, 'artist', '17'),
(157, 65, 'cate_sale_tax', '6'),
(158, 65, 'category_prod_feature_title', NULL),
(159, 65, 'category_prod_feature_detail', NULL),
(160, 67, 'cate_sale_tax', '2'),
(161, 64, 'artist', '11'),
(162, 64, 'cate_sale_tax', '5');

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `product_id`, `category_id`) VALUES
(221, 117, 67),
(239, 118, 64),
(249, 123, 67),
(250, 124, 68),
(251, 125, 68),
(252, 126, 68),
(253, 127, 68),
(254, 128, 67),
(255, 129, 68),
(256, 130, 68),
(257, 131, 68),
(258, 132, 68),
(268, 142, 72),
(270, 144, 72),
(271, 145, 72),
(272, 146, 72),
(273, 147, 72),
(274, 148, 72),
(275, 149, 72),
(276, 150, 73),
(277, 151, 73),
(281, 155, 73),
(282, 156, 73),
(283, 157, 73),
(284, 158, 65),
(285, 158, 66),
(286, 147, 67),
(288, 160, 65),
(290, 162, 65),
(319, 185, 65);

-- --------------------------------------------------------

--
-- Table structure for table `category_setting`
--

CREATE TABLE `category_setting` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `reason_titles` longtext,
  `reason_descriptions` longtext,
  `feature_titles` longtext,
  `feature_descriptions` longtext,
  `posters` longtext,
  `actual_material_pics` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_setting`
--

INSERT INTO `category_setting` (`id`, `status`, `reason_titles`, `reason_descriptions`, `feature_titles`, `feature_descriptions`, `posters`, `actual_material_pics`, `created_at`, `updated_at`) VALUES
(7, 'on', 'a:1:{i:0;s:8:\"Reason 1\";}', 'a:1:{i:0;s:56:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\";}', 'a:1:{i:0;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}', 'a:1:{i:0;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}', 'a:1:{i:0;s:83:\"upload/category_setting/posters-pics//LPJ92fy7vucGFwChPAsq3QZwYz7O1R09MNy3VzVn.jpeg\";}', 'a:1:{i:0;s:91:\"upload/category_setting/actual-material-pics//WOy5l7DzODy7VcfYZudTNLt1PToJjYKIhWMupyC6.jpeg\";}', '2020-04-11 15:51:39', '2020-04-21 16:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'AL', 'Albania'),
(2, 'DZ', 'Algeria'),
(3, 'DS', 'American Samoa'),
(4, 'AD', 'Andorra'),
(5, 'AO', 'Angola'),
(6, 'AI', 'Anguilla'),
(7, 'AQ', 'Antarctica'),
(8, 'AG', 'Antigua and Barbuda'),
(9, 'AR', 'Argentina'),
(10, 'AM', 'Armenia'),
(11, 'AW', 'Aruba'),
(12, 'AU', 'Australia'),
(13, 'AT', 'Austria'),
(14, 'AZ', 'Azerbaijan'),
(15, 'BS', 'Bahamas'),
(16, 'BH', 'Bahrain'),
(17, 'BD', 'Bangladesh'),
(18, 'BB', 'Barbados'),
(19, 'BY', 'Belarus'),
(20, 'BE', 'Belgium'),
(21, 'BZ', 'Belize'),
(22, 'BJ', 'Benin'),
(23, 'BM', 'Bermuda'),
(24, 'BT', 'Bhutan'),
(25, 'BO', 'Bolivia'),
(26, 'BA', 'Bosnia and Herzegovina'),
(27, 'BW', 'Botswana'),
(28, 'BV', 'Bouvet Island'),
(29, 'BR', 'Brazil'),
(30, 'IO', 'British Indian Ocean Territory'),
(31, 'BN', 'Brunei Darussalam'),
(32, 'BG', 'Bulgaria'),
(33, 'BF', 'Burkina Faso'),
(34, 'BI', 'Burundi'),
(35, 'KH', 'Cambodia'),
(36, 'CM', 'Cameroon'),
(37, 'CA', 'Canada'),
(38, 'CV', 'Cape Verde'),
(39, 'KY', 'Cayman Islands'),
(40, 'CF', 'Central African Republic'),
(41, 'TD', 'Chad'),
(42, 'CL', 'Chile'),
(43, 'CN', 'China'),
(44, 'CX', 'Christmas Island'),
(45, 'CC', 'Cocos (Keeling) Islands'),
(46, 'CO', 'Colombia'),
(47, 'KM', 'Comoros'),
(48, 'CG', 'Congo'),
(49, 'CK', 'Cook Islands'),
(50, 'CR', 'Costa Rica'),
(51, 'HR', 'Croatia (Hrvatska)'),
(52, 'CU', 'Cuba'),
(53, 'CY', 'Cyprus'),
(54, 'CZ', 'Czech Republic'),
(55, 'DK', 'Denmark'),
(56, 'DJ', 'Djibouti'),
(57, 'DM', 'Dominica'),
(58, 'DO', 'Dominican Republic'),
(59, 'TP', 'East Timor'),
(60, 'EC', 'Ecuador'),
(61, 'EG', 'Egypt'),
(62, 'SV', 'El Salvador'),
(63, 'GQ', 'Equatorial Guinea'),
(64, 'ER', 'Eritrea'),
(65, 'EE', 'Estonia'),
(66, 'ET', 'Ethiopia'),
(67, 'FK', 'Falkland Islands (Malvinas)'),
(68, 'FO', 'Faroe Islands'),
(69, 'FJ', 'Fiji'),
(70, 'FI', 'Finland'),
(71, 'FR', 'France'),
(72, 'FX', 'France, Metropolitan'),
(73, 'GF', 'French Guiana'),
(74, 'PF', 'French Polynesia'),
(75, 'TF', 'French Southern Territories'),
(76, 'GA', 'Gabon'),
(77, 'GM', 'Gambia'),
(78, 'GE', 'Georgia'),
(79, 'DE', 'Germany'),
(80, 'GH', 'Ghana'),
(81, 'GI', 'Gibraltar'),
(82, 'GK', 'Guernsey'),
(83, 'GR', 'Greece'),
(84, 'GL', 'Greenland'),
(85, 'GD', 'Grenada'),
(86, 'GP', 'Guadeloupe'),
(87, 'GU', 'Guam'),
(88, 'GT', 'Guatemala'),
(89, 'GN', 'Guinea'),
(90, 'GW', 'Guinea-Bissau'),
(91, 'GY', 'Guyana'),
(92, 'HT', 'Haiti'),
(93, 'HM', 'Heard and Mc Donald Islands'),
(94, 'HN', 'Honduras'),
(95, 'HK', 'Hong Kong'),
(96, 'HU', 'Hungary'),
(97, 'IS', 'Iceland'),
(98, 'IN', 'India'),
(99, 'IM', 'Isle of Man'),
(100, 'ID', 'Indonesia'),
(101, 'IR', 'Iran (Islamic Republic of)'),
(102, 'IQ', 'Iraq'),
(103, 'IE', 'Ireland'),
(104, 'IL', 'Israel'),
(105, 'IT', 'Italy'),
(106, 'CI', 'Ivory Coast'),
(107, 'JE', 'Jersey'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'JO', 'Jordan'),
(111, 'KZ', 'Kazakhstan'),
(112, 'KE', 'Kenya'),
(113, 'KI', 'Kiribati'),
(114, 'KP', 'Korea,Democratic People\'s Republic of'),
(115, 'KR', 'Korea, Republic of'),
(116, 'XK', 'Kosovo'),
(117, 'KW', 'Kuwait'),
(118, 'KG', 'Kyrgyzstan'),
(119, 'LA', 'Lao People\'s Democratic Republic'),
(120, 'LV', 'Latvia'),
(121, 'LB', 'Lebanon'),
(122, 'LS', 'Lesotho'),
(123, 'LR', 'Liberia'),
(124, 'LY', 'Libyan Arab Jamahiriya'),
(125, 'LI', 'Liechtenstein'),
(126, 'LT', 'Lithuania'),
(127, 'LU', 'Luxembourg'),
(128, 'MO', 'Macau'),
(129, 'MK', 'Macedonia'),
(130, 'MG', 'Madagascar'),
(131, 'MW', 'Malawi'),
(132, 'MY', 'Malaysia'),
(133, 'MV', 'Maldives'),
(134, 'ML', 'Mali'),
(135, 'MT', 'Malta'),
(136, 'MH', 'Marshall Islands'),
(137, 'MQ', 'Martinique'),
(138, 'MR', 'Mauritania'),
(139, 'MU', 'Mauritius'),
(140, 'TY', 'Mayotte'),
(141, 'MX', 'Mexico'),
(142, 'FM', 'Micronesia, Federated States of'),
(143, 'MD', 'Moldova, Republic of'),
(144, 'MC', 'Monaco'),
(145, 'MN', 'Mongolia'),
(146, 'ME', 'Montenegro'),
(147, 'MS', 'Montserrat'),
(148, 'MA', 'Morocco'),
(149, 'MZ', 'Mozambique'),
(150, 'MM', 'Myanmar'),
(151, 'NA', 'Namibia'),
(152, 'NR', 'Nauru'),
(153, 'NP', 'Nepal'),
(154, 'NL', 'Netherlands'),
(155, 'AN', 'Netherlands Antilles'),
(156, 'NC', 'New Caledonia'),
(157, 'NZ', 'New Zealand'),
(158, 'NI', 'Nicaragua'),
(159, 'NE', 'Niger'),
(160, 'NG', 'Nigeria'),
(161, 'NU', 'Niue'),
(162, 'NF', 'Norfolk Island'),
(163, 'MP', 'Northern Mariana Islands'),
(164, 'NO', 'Norway'),
(165, 'OM', 'Oman'),
(166, 'PK', 'Pakistan'),
(167, 'PW', 'Palau'),
(168, 'PS', 'Palestine'),
(169, 'PA', 'Panama'),
(170, 'PG', 'Papua New Guinea'),
(171, 'PY', 'Paraguay'),
(172, 'PE', 'Peru'),
(173, 'PH', 'Philippines'),
(174, 'PN', 'Pitcairn'),
(175, 'PL', 'Poland'),
(176, 'PT', 'Portugal'),
(177, 'PR', 'Puerto Rico'),
(178, 'QA', 'Qatar'),
(179, 'RE', 'Reunion'),
(180, 'RO', 'Romania'),
(181, 'RU', 'Russian Federation'),
(182, 'RW', 'Rwanda'),
(183, 'KN', 'Saint Kitts and Nevis'),
(184, 'LC', 'Saint Lucia'),
(185, 'VC', 'Saint Vincent and the Grenadines'),
(186, 'WS', 'Samoa'),
(187, 'SM', 'San Marino'),
(188, 'ST', 'Sao Tome and Principe'),
(189, 'SA', 'Saudi Arabia'),
(190, 'SN', 'Senegal'),
(191, 'RS', 'Serbia'),
(192, 'SC', 'Seychelles'),
(193, 'SL', 'Sierra Leone'),
(194, 'SG', 'Singapore'),
(195, 'SK', 'Slovakia'),
(196, 'SI', 'Slovenia'),
(197, 'SB', 'Solomon Islands'),
(198, 'SO', 'Somalia'),
(199, 'ZA', 'South Africa'),
(200, 'GS', 'South Georgia South Sandwich Islands'),
(201, 'SS', 'South Sudan'),
(202, 'ES', 'Spain'),
(203, 'LK', 'Sri Lanka'),
(204, 'SH', 'St. Helena'),
(205, 'PM', 'St. Pierre and Miquelon'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen Islands'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TG', 'Togo'),
(218, 'TK', 'Tokelau'),
(219, 'TO', 'Tonga'),
(220, 'TT', 'Trinidad and Tobago'),
(221, 'TN', 'Tunisia'),
(222, 'TR', 'Turkey'),
(223, 'TM', 'Turkmenistan'),
(224, 'TC', 'Turks and Caicos Islands'),
(225, 'TV', 'Tuvalu'),
(226, 'UG', 'Uganda'),
(227, 'UA', 'Ukraine'),
(228, 'AE', 'United Arab Emirates'),
(229, 'GB', 'United Kingdom'),
(230, 'US', 'United States'),
(231, 'UM', 'United States minor outlying islands'),
(232, 'UY', 'Uruguay'),
(233, 'UZ', 'Uzbekistan'),
(234, 'VU', 'Vanuatu'),
(235, 'VA', 'Vatican City State'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Vietnam'),
(238, 'VG', 'Virgin Islands (British)'),
(239, 'VI', 'Virgin Islands (U.S.)'),
(240, 'WF', 'Wallis and Futuna Islands'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'ZR', 'Zaire'),
(244, 'ZM', 'Zambia'),
(245, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(250) CHARACTER SET latin1 NOT NULL,
  `type` varchar(250) CHARACTER SET latin1 NOT NULL,
  `max_uses` int(200) NOT NULL,
  `max_uses_user` int(200) DEFAULT NULL,
  `discount` int(200) DEFAULT NULL,
  `status` int(200) NOT NULL,
  `freedelivery` int(200) DEFAULT NULL,
  `exclude_category` longtext CHARACTER SET latin1,
  `exclude_product` longtext CHARACTER SET latin1,
  `start_date` date DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `startandexpire` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_product`
--

CREATE TABLE `coupon_product` (
  `id` int(11) NOT NULL,
  `product_id` int(20) NOT NULL,
  `coupon_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_user`
--

CREATE TABLE `coupon_user` (
  `id` int(11) NOT NULL,
  `user_id` int(20) NOT NULL,
  `coupon_id` int(20) NOT NULL,
  `status` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coupon_user`
--

INSERT INTO `coupon_user` (`id`, `user_id`, `coupon_id`, `status`) VALUES
(9, 1, 2, 1),
(10, 1, 11, 1),
(11, 1, 12, 1),
(12, 1, 13, 1),
(13, 1, 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `code` varchar(200) NOT NULL,
  `symbol` varchar(200) NOT NULL,
  `exchange_rate` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reviews`
--

CREATE TABLE `customer_reviews` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` longtext,
  `rating` varchar(255) DEFAULT NULL,
  `detail` longtext,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_reviews`
--

INSERT INTO `customer_reviews` (`id`, `name`, `image`, `rating`, `detail`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Jack', 'upload/customer-reviews/1/5KCaWKDoe7faxomPao2p4FkzQ5BQ9ivBYcPUf4mx.jpeg', '5', '<p>I\'m going to do more business with them very soon. FANTASTCI SERVICE! AN<br></p>', 'off', '2020-04-30 18:01:03', '2020-05-01 10:51:08'),
(2, 'Wilson', 'upload/customer-reviews/2/NjGlUXDLOOU7jyfwYzF7hNEPXJAsun7ivMqOy7PG.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:17:47', '2020-05-01 10:17:47'),
(3, 'Anna', 'upload/customer-reviews/3/Yplxo8yjtBbwvoupvJUNUQboITIoOGI7HnGMfZK9.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:19:58', '2020-05-01 10:19:58'),
(4, 'Jennifer', 'upload/customer-reviews/4/Eu3CvSd4bnzeeyN8StPK0ONsxx3lvsvhIAv0GBbM.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:20:50', '2020-05-01 10:20:50'),
(5, 'Lilly', 'upload/customer-reviews/5/YSfnJfeYaLNlIuSgIfD8t1S0HakksP16kHXSRDju.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:21:09', '2020-05-01 10:21:09'),
(6, 'Charlotte', 'upload/customer-reviews/6/kxIrYNnE5OsWEATrhKazcd2iOoykWFRl3gox3CLm.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:21:26', '2020-05-01 10:21:26'),
(7, 'Peter Wock', 'upload/customer-reviews/7/I2Zar4JhQqZ9TI5OVkxAjemvmWuDTaHtlAjQkgK3.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:21:53', '2020-05-01 10:21:53'),
(8, 'Fillis', 'upload/customer-reviews/8/bape8yUHbhPN0CckWv5fmdyx1ZxICDmDRWXuo1L4.png', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:23:08', '2020-05-01 10:32:35'),
(9, 'Richard', 'upload/customer-reviews/9/HfACUJG7BudGGCluQlejlxP7TtEqmookuZ1RyIxr.jpeg', '4', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:51:47', '2020-05-04 11:09:21');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'on',
  `address` longtext,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `image` longtext,
  `timing` varchar(255) DEFAULT NULL,
  `details` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `status`, `address`, `state`, `city`, `country`, `image`, `timing`, `details`, `created_at`, `updated_at`) VALUES
(1, 'Art Center Manatee', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'West Virgina', 'Huntington', 'United States', 'upload/events/1/wIbxTZTiNDuWciRqZXJ4jdMq6Abr9P6jNfTTfa8j.jpeg', '05/30/2020 12:00 PM', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel felis dictum, efficitur nulla ac, luctus ipsum. Ut placerat sapien nec orci dictum, sit amet accumsan nisi tristique.', '2020-04-30 14:13:43', '2020-04-30 15:09:05'),
(2, 'Otto Hubbard', 'on', 'Excepteur porro ab r', 'Bavaria', 'Munich', 'Germany', 'upload/events/2/o5w96Iw6wXDBau7DoPbopPh0KE3FCLisgrFg90HO.jpeg', '05/25/2020 1:11 PM', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</span>', '2020-04-30 15:12:44', '2020-04-30 16:55:51'),
(3, 'Art Center Venice', 'on', 'Excepteur porro ab r', NULL, 'Venice', 'Italy', 'upload/events/3/EeE5Ha2pQsgCeIerWmInreFUzotG2ouRxwV10M6s.jpeg', '06/18/2020 1:16 PM', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</span><br></p>', '2020-04-30 15:16:40', '2020-04-30 16:55:53'),
(4, 'Exhibition Of Art Culture', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 'London', 'United Kingdom', 'upload/events/4/zPXg38Ps3ue9QyWlWJeohNsvsaYAsQuB2AoYgDh9.jpeg', '06/27/2020 1:17 PM', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel felis dictum, efficitur nulla ac.</span><br></p>', '2020-04-30 15:19:11', '2020-05-04 10:56:09'),
(6, 'Art Center Dubai', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 'Dubai', 'United Arab Emirates', 'upload/events/6/56Mzr9c3ryMxP0phVf2wMur1X1krW4QfGpE4EXwQ.jpeg', '05/30/2020 10:57 PM', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br></p>', '2020-05-04 10:57:28', '2020-05-04 10:57:28');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` longtext,
  `answer` longtext,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(1, '<div>What Is Torben Goldmund’s Art ‘N’</div>', '<div style=\"text-align: justify;\"><font face=\"Open Sans, Arial, sans-serif\"><span style=\"font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris molestie ornare laoreet. Nam sed mattis ante. Nam ullamcorper molestie pellentesque. Pellentesque finibus metus a nulla volutpat, eget bibendum ligula aliquet. Duis consectetur ut justo sit amet dignissim. Sed laoreet mauris sed nulla finibus hendrerit. Proin semper vestibulum maximus. Curabitur maximus nunc metus, sed bibendum dui lacinia sit amet. Quisque porta, elit eget facilisis faucibus, nulla nulla ultrices odio, ut elementum metus nisl et massa. Donec pharetra, arcu dignissim varius bibendum, elit turpis porta odio, vitae dapibus augue nulla nec nibh. Praesent hendrerit lacus neque, sed pellentesque enim ullamcorper vitae. Interdum et malesuada fames ac ante ipsum primis in faucibus.</span></font></div>', 1, '2020-02-27 16:28:17', '2020-03-25 16:45:49'),
(14, 'Pellentesque habitant morbi tristique senectus et netus et malesuada.dddd', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna.', 0, '2020-05-04 11:15:00', '2020-06-25 16:08:56');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(50) NOT NULL,
  `artist_id` int(50) DEFAULT NULL,
  `title` text,
  `image` text,
  `status` varchar(255) DEFAULT NULL,
  `submission` varchar(255) DEFAULT NULL,
  `description` text,
  `tags` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `artist_id`, `title`, `image`, `status`, `submission`, `description`, `tags`, `created_at`, `updated_at`) VALUES
(12, 21, 'Cartoon Art Work', 'upload/gallery_work_upload/gXIXJhYvI5aTCGpWf1HE3cTyj8OcN38N4ruhMgUu.png', '1', 'approved', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet risus facilisis, malesuada sem et, volutpat tortor. Nunc consequat lorem nec est aliquet scelerisque.', 'Cartoon,Art,children', '2020-08-08 17:20:02', '2020-08-08 17:27:58');

-- --------------------------------------------------------

--
-- Table structure for table `guests`
--

CREATE TABLE `guests` (
  `id` int(11) NOT NULL,
  `guestID` bigint(50) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `guest_meta` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `stripe_customer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `place_order_status` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guests`
--

INSERT INTO `guests` (`id`, `guestID`, `name`, `email`, `guest_meta`, `stripe_customer_id`, `place_order_status`, `created_at`, `updated_at`) VALUES
(4, 1594463441968, 'Yasni Guest', 'yasin.100@hotmail.com', NULL, 'cus_Hcw32k2KJjUyC3', 1, '2020-07-11 05:30:46', '2020-07-11 05:43:28'),
(5, 1594471858744, 'Faith Waller', 'yasinmaknojia@gmail.com', NULL, NULL, 1, '2020-07-11 07:51:02', '2020-07-11 08:34:48'),
(23, 1595449240716, NULL, NULL, NULL, NULL, 0, '2020-07-22 15:20:48', '2020-07-22 15:20:48');

-- --------------------------------------------------------

--
-- Table structure for table `inquires`
--

CREATE TABLE `inquires` (
  `id` int(50) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` text,
  `message` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inquires`
--

INSERT INTO `inquires` (`id`, `name`, `email`, `phone`, `message`, `created_at`, `updated_at`) VALUES
(5, 'qwerty', 'yasinmaknojia@gmail.com', '879789', 'adasdasdasdas', '2020-08-23 04:13:13', '2020-08-23 04:13:13'),
(6, 'warehouse_manger-1', 'yasinmaknojia@gmail.com', '46545465454', 'dsadsadsadasd', '2020-09-06 02:23:11', '2020-09-06 02:23:11'),
(7, 'warehouse_manger-1', 'yasinmaknojia@gmail.com', '46545465454', 'dsadsadsadasd', '2020-09-06 02:25:03', '2020-09-06 02:25:03'),
(8, 'warehouse_manger-1', 'yasinmaknojia@gmail.com', '46545465454', 'dsadsadsadasd', '2020-09-06 02:27:55', '2020-09-06 02:27:55'),
(9, 'warehouse_manger-1', 'yasinmaknojia@gmail.com', '46545465454', 'dsadsadsadasd', '2020-09-06 02:29:21', '2020-09-06 02:29:21'),
(10, 'dasdsds', 'yasin.100@hotmail.com', '+14742443359', 'ddsdasdassad', '2020-09-06 02:36:14', '2020-09-06 02:36:14'),
(11, 'File-023-QWE-3', 'yasinmaknojia@gmail.com', '46545465454', 'ddasdsad', '2020-09-06 02:43:39', '2020-09-06 02:43:39'),
(12, 'File-023-QWE-3', 'yasinmaknojia@gmail.com', '46545465454', 'ddasdsad', '2020-09-06 02:44:58', '2020-09-06 02:44:58'),
(13, '+14742443359', 'yasinmaknojia@gmail.com', '+14742443359', 'sdasdasdas', '2020-09-22 00:21:35', '2020-09-22 00:21:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_02_05_102643_create_permission_tables', 1),
(4, '2020_02_09_203836_create_verify_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(3, 'App\\User', 2),
(3, 'App\\User', 3),
(3, 'App\\User', 4),
(3, 'App\\User', 5),
(3, 'App\\User', 6),
(3, 'App\\User', 7),
(3, 'App\\User', 8),
(3, 'App\\User', 9),
(3, 'App\\User', 10),
(3, 'App\\User', 11),
(3, 'App\\User', 12),
(3, 'App\\User', 13),
(3, 'App\\User', 14),
(3, 'App\\User', 15),
(3, 'App\\User', 16),
(3, 'App\\User', 17),
(3, 'App\\User', 18),
(3, 'App\\User', 19),
(3, 'App\\User', 20),
(3, 'App\\User', 21),
(3, 'App\\User', 22),
(3, 'App\\User', 23),
(3, 'App\\User', 24),
(3, 'App\\User', 25),
(3, 'App\\User', 26),
(3, 'App\\User', 27),
(3, 'App\\User', 28),
(3, 'App\\User', 29),
(3, 'App\\User', 31),
(3, 'App\\User', 32),
(3, 'App\\User', 33),
(3, 'App\\User', 34),
(3, 'App\\User', 35),
(3, 'App\\User', 36),
(3, 'App\\User', 37),
(3, 'App\\User', 38),
(3, 'App\\User', 39),
(3, 'App\\User', 40),
(3, 'App\\User', 41),
(3, 'App\\User', 42),
(3, 'App\\User', 43),
(3, 'App\\User', 44),
(3, 'App\\User', 45),
(3, 'App\\User', 46),
(3, 'App\\User', 47),
(3, 'App\\User', 48),
(3, 'App\\User', 49),
(3, 'App\\User', 50),
(3, 'App\\User', 51),
(3, 'App\\User', 52),
(3, 'App\\User', 53),
(3, 'App\\User', 54),
(3, 'App\\User', 55),
(3, 'App\\User', 56),
(3, 'App\\User', 57),
(3, 'App\\User', 58),
(3, 'App\\User', 59),
(3, 'App\\User', 60),
(3, 'App\\User', 61),
(3, 'App\\User', 62),
(3, 'App\\User', 63),
(3, 'App\\User', 64),
(3, 'App\\User', 65);

-- --------------------------------------------------------

--
-- Table structure for table `orderparts`
--

CREATE TABLE `orderparts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `parts` int(50) DEFAULT NULL,
  `days` int(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderparts`
--

INSERT INTO `orderparts` (`id`, `title`, `parts`, `days`, `created_at`, `updated_at`) VALUES
(3, 'Order Divide Into Two Parts', 2, 10, '2020-07-18 08:29:05', '2020-07-18 09:09:58'),
(4, 'Order Divide Into Three Parts', 3, 5, '2020-07-18 15:54:23', '2020-07-18 15:54:23');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `order_details` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `product_detail` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `product_extra_detail` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `user_meta` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `user_id` bigint(50) DEFAULT NULL,
  `subtotal` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `shipping` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `selected_order_part_id` int(50) DEFAULT NULL,
  `order_part_done` int(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `user_email`, `order_details`, `product_detail`, `product_extra_detail`, `user_meta`, `user_id`, `subtotal`, `shipping`, `discount`, `tax`, `status`, `selected_order_part_id`, `order_part_done`, `created_at`, `updated_at`) VALUES
(22, '8TD92GngaN', 'yasin.100@hotmail.com', 'a:14:{s:6:\"_token\";s:40:\"eduT7uPocjW47w9pd9pf2vPXe95gXOYu1ND1nQqQ\";s:4:\"name\";s:5:\"ADMIN\";s:5:\"email\";s:21:\"yasin.100@hotmail.com\";s:7:\"address\";s:20:\"In nulla corporis ve\";s:6:\"mobile\";s:20:\"Praesentium nobis fu\";s:5:\"pcode\";s:18:\"Sed iure provident\";s:4:\"city\";s:20:\"Dolorum facere cupid\";s:9:\"d-country\";s:11:\"Afghanistan\";s:13:\"other-address\";N;s:10:\"other-city\";N;s:11:\"other-pcode\";N;s:8:\"shipping\";s:3:\"dhl\";s:7:\"payment\";s:6:\"stripe\";s:13:\"other-country\";s:11:\"Afghanistan\";}', 'a:4:{i:0;s:1133:\"a:12:{s:2:\"id\";i:158;s:7:\"user_id\";i:1;s:5:\"title\";s:20:\"Testing Product Dumy\";s:4:\"slug\";s:20:\"testing-product-dumy\";s:11:\"description\";s:786:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at mi eget tortor egestas rhoncus. Mauris dictum, dui a luctus aliquet, quam magna laoreet massa, vel fringilla libero leo ac massa. Quisque quam odio, finibus quis tempor vitae, ultricies sagittis est. Donec pharetra lacinia velit ac tincidunt. Aenean et ligula pulvinar, tristique justo non, facilisis augue. Nunc a auctor ligula, sed facilisis sapien. Sed lectus ligula, semper eget ultricies non, sagittis eget nisl. Nam et leo semper, mollis tortor quis, accumsan metus. Morbi eget sem eu lectus aliquet congue. Vivamus interdum elit et est aliquet maximus. Sed convallis malesuada tristique.</span><br></p>\";s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_158/feature/eiCpdpOuGpXOXoQ1IYwrZJxHd4qJ8Oph5PDGABsR.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:55;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";i:1;s:933:\"a:12:{s:2:\"id\";i:157;s:7:\"user_id\";i:1;s:5:\"title\";s:11:\"cat-widget1\";s:4:\"slug\";s:11:\"cat-widget1\";s:11:\"description\";s:604:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis. Fusce at pretium urna, quis pharetra elit. Curabitur sollicitudin pellentesque ex sit amet imperdiet. Aenean erat ante, accumsan venenatis tincidunt a, tincidunt sed leo. Morbi scelerisque tempus neque vel pretium. Vivamus ac erat cursus, commodo ligula et, eleifend nulla. Nulla sit amet volutpat turpis. Suspendisse pretium maximus sem, at fermentum velit viverra sodales.</span>\";s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_157/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";i:63;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";i:2;s:316:\"a:12:{s:2:\"id\";i:155;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"post-img1\";s:4:\"slug\";s:9:\"post-img1\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_155/feature/nEfHsDtaInZ4ArHS2FtOvPjJZCRhoMk3fYQHKjC0.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";i:63;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";i:3;s:935:\"a:12:{s:2:\"id\";i:156;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"post-img2\";s:4:\"slug\";s:9:\"post-img2\";s:11:\"description\";s:612:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>\";s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_156/feature/SJJ05fADEODOCttITm0UsVZXYQZBDut4PBBrxiFX.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";i:20;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:4:{i:0;s:274:\"a:2:{i:0;a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:5.4;}i:1;a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:5.4;}}\";i:1;s:130:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:3.6;}\";i:2;s:130:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}\";i:3;s:128:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";i:6;}\";}', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":\"2020-06-04 15:22:05\",\"verified\":1,\"status\":1,\"profile_pic\":\"upload\\/users\\/user_1\\/profile_pic\\/rFVZ3h3807vwHApf9PcbSlzmgSBIzMZN9J3gRmNM.jpeg\",\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-06-22 20:05:24\"}', 1, '657.84', '', '', '', 'Processing', NULL, NULL, '2020-06-23 17:34:46', '2020-06-23 17:43:18'),
(23, 'aZmCVpQqKZ', 'yasin.100@hotmail.com', 'a:23:{s:6:\"_token\";s:40:\"2UxpzrPQSi7TVmaZY35u2Q0VwpS5eTOHCir0LA4N\";s:4:\"name\";s:11:\"Yasni Guest\";s:5:\"email\";s:21:\"yasin.100@hotmail.com\";s:7:\"address\";s:13:\"Bolden Market\";s:6:\"mobile\";s:10:\"5645656446\";s:5:\"pcode\";s:4:\"5669\";s:4:\"city\";s:7:\"Karachi\";s:5:\"state\";s:5:\"Sindh\";s:9:\"d-country\";s:14:\"Åland Islands\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:13:\"Bolden Market\";s:11:\"other-pcode\";s:4:\"5669\";s:10:\"other-city\";s:7:\"Karachi\";s:11:\"other-state\";s:5:\"Sindh\";s:13:\"other-country\";s:14:\"Åland Islands\";s:8:\"shipping\";N;s:7:\"payment\";s:6:\"stripe\";s:8:\"subtotal\";s:6:\"126.69\";s:13:\"currentUserID\";s:13:\"1594463441968\";s:11:\"stripeToken\";s:28:\"tok_1H3gJoAedkiOiGZjvmv1HaLb\";}', 'a:1:{i:0;s:1133:\"a:12:{s:2:\"id\";i:158;s:7:\"user_id\";i:1;s:5:\"title\";s:20:\"Testing Product Dumy\";s:4:\"slug\";s:20:\"testing-product-dumy\";s:11:\"description\";s:786:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at mi eget tortor egestas rhoncus. Mauris dictum, dui a luctus aliquet, quam magna laoreet massa, vel fringilla libero leo ac massa. Quisque quam odio, finibus quis tempor vitae, ultricies sagittis est. Donec pharetra lacinia velit ac tincidunt. Aenean et ligula pulvinar, tristique justo non, facilisis augue. Nunc a auctor ligula, sed facilisis sapien. Sed lectus ligula, semper eget ultricies non, sagittis eget nisl. Nam et leo semper, mollis tortor quis, accumsan metus. Morbi eget sem eu lectus aliquet congue. Vivamus interdum elit et est aliquet maximus. Sed convallis malesuada tristique.</span><br></p>\";s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_158/feature/eiCpdpOuGpXOXoQ1IYwrZJxHd4qJ8Oph5PDGABsR.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:55;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:130:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:5.4;}\";}', '{\"id\":4,\"guestID\":\"1594463441968\",\"name\":\"Yasni Guest\",\"email\":\"yasin.100@hotmail.com\",\"guest_meta\":null,\"stripe_customer_id\":\"cus_Hcw32k2KJjUyC3\",\"place_order_status\":1,\"created_at\":\"2020-07-11 10:30:46\",\"updated_at\":\"2020-07-11 10:43:28\"}', 1594463441968, '126.69', '', '', '', 'Confirm', NULL, NULL, '2020-07-11 05:52:34', '2020-07-11 05:52:34'),
(24, 'HQk8ohBAVg', 'yasin.100@hotmail.com', 'a:23:{s:6:\"_token\";s:40:\"o7LvSqatS3QooWQF1QacdoyljGKS1SLyuVRt6C2R\";s:4:\"name\";s:13:\"Beverly Rojas\";s:5:\"email\";s:21:\"yasin.100@hotmail.com\";s:7:\"address\";s:18:\"Laboriosam officia\";s:6:\"mobile\";s:9:\"221312312\";s:5:\"pcode\";s:4:\"2232\";s:4:\"city\";s:7:\"Karachi\";s:5:\"state\";s:5:\"Sindh\";s:9:\"d-country\";s:11:\"Afghanistan\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:18:\"Laboriosam officia\";s:11:\"other-pcode\";s:4:\"2232\";s:10:\"other-city\";s:7:\"Karachi\";s:11:\"other-state\";s:5:\"Sindh\";s:13:\"other-country\";s:11:\"Afghanistan\";s:8:\"shipping\";N;s:7:\"payment\";s:6:\"stripe\";s:8:\"subtotal\";s:6:\"134.82\";s:13:\"currentUserID\";s:13:\"1594463441968\";s:11:\"stripeToken\";s:28:\"tok_1H3gpeAedkiOiGZjTX73woOI\";}', 'a:1:{i:0;s:933:\"a:12:{s:2:\"id\";i:157;s:7:\"user_id\";i:1;s:5:\"title\";s:11:\"cat-widget1\";s:4:\"slug\";s:11:\"cat-widget1\";s:11:\"description\";s:604:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis. Fusce at pretium urna, quis pharetra elit. Curabitur sollicitudin pellentesque ex sit amet imperdiet. Aenean erat ante, accumsan venenatis tincidunt a, tincidunt sed leo. Morbi scelerisque tempus neque vel pretium. Vivamus ac erat cursus, commodo ligula et, eleifend nulla. Nulla sit amet volutpat turpis. Suspendisse pretium maximus sem, at fermentum velit viverra sodales.</span>\";s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_157/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";i:63;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:130:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:3.6;}\";}', '{\"id\":4,\"guestID\":\"1594463441968\",\"name\":\"Yasni Guest\",\"email\":\"yasin.100@hotmail.com\",\"guest_meta\":null,\"stripe_customer_id\":\"cus_Hcw32k2KJjUyC3\",\"place_order_status\":1,\"created_at\":\"2020-07-11 10:30:46\",\"updated_at\":\"2020-07-11 10:43:28\"}', 1594463441968, '134.82', '', '', '', 'Confirm', NULL, NULL, '2020-07-11 06:25:27', '2020-07-11 06:25:27'),
(25, '0PzYMr1ckH', 'yasinmaknojia@gmail.com', 'a:21:{s:6:\"_token\";s:40:\"o7LvSqatS3QooWQF1QacdoyljGKS1SLyuVRt6C2R\";s:4:\"name\";s:5:\"Yasin\";s:5:\"email\";s:23:\"yasinmaknojia@gmail.com\";s:7:\"address\";s:20:\"Odit vitae qui exped\";s:6:\"mobile\";s:20:\"Iusto sed id enim al\";s:5:\"pcode\";s:19:\"Hic ipsa iusto volu\";s:4:\"city\";s:18:\"Fugiat nesciunt qu\";s:5:\"state\";s:19:\"Reprehenderit labor\";s:9:\"d-country\";s:14:\"Åland Islands\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:20:\"Odit vitae qui exped\";s:11:\"other-pcode\";s:19:\"Hic ipsa iusto volu\";s:10:\"other-city\";s:18:\"Fugiat nesciunt qu\";s:11:\"other-state\";s:19:\"Reprehenderit labor\";s:13:\"other-country\";s:14:\"Åland Islands\";s:7:\"payment\";s:6:\"paypal\";s:8:\"subtotal\";s:6:\"149.35\";s:13:\"currentUserID\";s:13:\"1594471858744\";}', 'a:1:{i:0;s:317:\"a:12:{s:2:\"id\";i:146;s:7:\"user_id\";i:1;s:5:\"title\";s:10:\"Product-10\";s:4:\"slug\";s:10:\"product-10\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_146/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:128:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";i:0;}\";}', NULL, 1594471858744, '149.35', '', '', '', 'Confirm', NULL, NULL, '2020-07-11 08:21:28', '2020-07-11 08:21:28'),
(26, 'BzJE35hkmb', 'yasinmaknojia@gmail.com', 'a:21:{s:6:\"_token\";s:40:\"o7LvSqatS3QooWQF1QacdoyljGKS1SLyuVRt6C2R\";s:4:\"name\";s:13:\"Ahmed Collier\";s:5:\"email\";s:23:\"yasinmaknojia@gmail.com\";s:7:\"address\";s:13:\"dlaskdjasldjk\";s:6:\"mobile\";s:8:\"56456456\";s:5:\"pcode\";s:5:\"45645\";s:4:\"city\";s:7:\"Karachi\";s:5:\"state\";s:5:\"Sindh\";s:9:\"d-country\";s:7:\"Bermuda\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:13:\"dlaskdjasldjk\";s:11:\"other-pcode\";s:5:\"45645\";s:10:\"other-city\";s:7:\"Karachi\";s:11:\"other-state\";s:5:\"Sindh\";s:13:\"other-country\";s:7:\"Bermuda\";s:7:\"payment\";s:6:\"paypal\";s:8:\"subtotal\";s:6:\"149.35\";s:13:\"currentUserID\";s:13:\"1594471858744\";}', 'a:1:{i:0;s:317:\"a:12:{s:2:\"id\";i:146;s:7:\"user_id\";i:1;s:5:\"title\";s:10:\"Product-10\";s:4:\"slug\";s:10:\"product-10\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_146/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:128:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";i:0;}\";}', NULL, 1594471858744, '149.35', '', '', '', 'Confirm', NULL, NULL, '2020-07-11 08:23:19', '2020-07-11 08:23:19'),
(27, 'v6P3LbxLUO', 'yasinmaknojia@gmail.com', 'a:21:{s:6:\"_token\";s:40:\"o7LvSqatS3QooWQF1QacdoyljGKS1SLyuVRt6C2R\";s:4:\"name\";s:14:\"Juliet Guthrie\";s:5:\"email\";s:23:\"yasinmaknojia@gmail.com\";s:7:\"address\";s:20:\"Nisi id magni quos q\";s:6:\"mobile\";s:12:\"344234234234\";s:5:\"pcode\";s:6:\"323232\";s:4:\"city\";s:7:\"Karachi\";s:5:\"state\";s:5:\"Sindh\";s:9:\"d-country\";s:11:\"Afghanistan\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:20:\"Nisi id magni quos q\";s:11:\"other-pcode\";s:6:\"323232\";s:10:\"other-city\";s:7:\"Karachi\";s:11:\"other-state\";s:5:\"Sindh\";s:13:\"other-country\";s:11:\"Afghanistan\";s:7:\"payment\";s:6:\"paypal\";s:8:\"subtotal\";s:6:\"149.35\";s:13:\"currentUserID\";s:13:\"1594471858744\";}', 'a:1:{i:0;s:313:\"a:12:{s:2:\"id\";i:144;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"Product-6\";s:4:\"slug\";s:9:\"product-6\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_144/feature/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:128:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";i:0;}\";}', NULL, 1594471858744, '149.35', '', '', '', 'Confirm', NULL, NULL, '2020-07-11 08:27:35', '2020-07-11 08:27:35'),
(28, 'e2e8z9dyRF', 'yasinmaknojia@gmail.com', 'a:21:{s:6:\"_token\";s:40:\"o7LvSqatS3QooWQF1QacdoyljGKS1SLyuVRt6C2R\";s:4:\"name\";s:14:\"Juliet Guthrie\";s:5:\"email\";s:23:\"yasinmaknojia@gmail.com\";s:7:\"address\";s:20:\"Nisi id magni quos q\";s:6:\"mobile\";s:12:\"344234234234\";s:5:\"pcode\";s:6:\"323232\";s:4:\"city\";s:7:\"Karachi\";s:5:\"state\";s:5:\"Sindh\";s:9:\"d-country\";s:11:\"Afghanistan\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:20:\"Nisi id magni quos q\";s:11:\"other-pcode\";s:6:\"323232\";s:10:\"other-city\";s:7:\"Karachi\";s:11:\"other-state\";s:5:\"Sindh\";s:13:\"other-country\";s:11:\"Afghanistan\";s:7:\"payment\";s:6:\"paypal\";s:8:\"subtotal\";s:6:\"149.35\";s:13:\"currentUserID\";s:13:\"1594471858744\";}', 'a:1:{i:0;s:313:\"a:12:{s:2:\"id\";i:144;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"Product-6\";s:4:\"slug\";s:9:\"product-6\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_144/feature/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:128:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";i:0;}\";}', NULL, 1594471858744, '149.35', '', '', '', 'Confirm', NULL, NULL, '2020-07-11 08:27:56', '2020-07-11 08:27:56'),
(29, 'LiJ2MzqY5i', 'yasin.100@hotmail.com', 'a:21:{s:6:\"_token\";s:40:\"o7LvSqatS3QooWQF1QacdoyljGKS1SLyuVRt6C2R\";s:4:\"name\";s:11:\"Alan Farmer\";s:5:\"email\";s:21:\"yasin.100@hotmail.com\";s:7:\"address\";s:19:\"Quis magni voluptas\";s:6:\"mobile\";s:18:\"Culpa sint digniss\";s:5:\"pcode\";s:20:\"Exercitationem elige\";s:4:\"city\";s:19:\"Ipsum cupidatat nul\";s:5:\"state\";s:20:\"Eaque sit enim fugia\";s:9:\"d-country\";s:7:\"Albania\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:19:\"Quis magni voluptas\";s:11:\"other-pcode\";s:20:\"Exercitationem elige\";s:10:\"other-city\";s:19:\"Ipsum cupidatat nul\";s:11:\"other-state\";s:20:\"Eaque sit enim fugia\";s:13:\"other-country\";s:7:\"Albania\";s:7:\"payment\";s:6:\"paypal\";s:8:\"subtotal\";s:6:\"149.35\";s:13:\"currentUserID\";s:13:\"1594471858744\";}', 'a:1:{i:0;s:313:\"a:12:{s:2:\"id\";i:144;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"Product-6\";s:4:\"slug\";s:9:\"product-6\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_144/feature/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:128:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";i:0;}\";}', NULL, 1594471858744, '149.35', '', '', '', 'Confirm', NULL, NULL, '2020-07-11 08:29:57', '2020-07-11 08:29:57'),
(30, 'EExFUXas7y', 'yasin.100@hotmail.com', 'a:21:{s:6:\"_token\";s:40:\"o7LvSqatS3QooWQF1QacdoyljGKS1SLyuVRt6C2R\";s:4:\"name\";s:11:\"Alan Farmer\";s:5:\"email\";s:21:\"yasin.100@hotmail.com\";s:7:\"address\";s:19:\"Quis magni voluptas\";s:6:\"mobile\";s:18:\"Culpa sint digniss\";s:5:\"pcode\";s:20:\"Exercitationem elige\";s:4:\"city\";s:19:\"Ipsum cupidatat nul\";s:5:\"state\";s:20:\"Eaque sit enim fugia\";s:9:\"d-country\";s:7:\"Albania\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:19:\"Quis magni voluptas\";s:11:\"other-pcode\";s:20:\"Exercitationem elige\";s:10:\"other-city\";s:19:\"Ipsum cupidatat nul\";s:11:\"other-state\";s:20:\"Eaque sit enim fugia\";s:13:\"other-country\";s:7:\"Albania\";s:7:\"payment\";s:6:\"paypal\";s:8:\"subtotal\";s:6:\"149.35\";s:13:\"currentUserID\";s:13:\"1594471858744\";}', 'a:1:{i:0;s:313:\"a:12:{s:2:\"id\";i:144;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"Product-6\";s:4:\"slug\";s:9:\"product-6\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_144/feature/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:128:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";i:0;}\";}', NULL, 1594471858744, '149.35', '', '', '', 'Confirm', NULL, NULL, '2020-07-11 08:31:16', '2020-07-11 08:31:16'),
(31, 'aTOCeE6bw3', 'yasinmaknojia@gmail.com', 'a:21:{s:6:\"_token\";s:40:\"o7LvSqatS3QooWQF1QacdoyljGKS1SLyuVRt6C2R\";s:4:\"name\";s:12:\"Faith Waller\";s:5:\"email\";s:23:\"yasinmaknojia@gmail.com\";s:7:\"address\";s:20:\"Accusantium error pr\";s:6:\"mobile\";s:18:\"Rerum labore velit\";s:5:\"pcode\";s:19:\"Voluptas proident o\";s:4:\"city\";s:20:\"Autem dolorum recusa\";s:5:\"state\";s:20:\"Quibusdam unde irure\";s:9:\"d-country\";s:8:\"Pakistan\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:20:\"Accusantium error pr\";s:11:\"other-pcode\";s:19:\"Voluptas proident o\";s:10:\"other-city\";s:20:\"Autem dolorum recusa\";s:11:\"other-state\";s:20:\"Quibusdam unde irure\";s:13:\"other-country\";s:8:\"Pakistan\";s:7:\"payment\";s:6:\"paypal\";s:8:\"subtotal\";s:6:\"149.35\";s:13:\"currentUserID\";s:13:\"1594471858744\";}', 'a:1:{i:0;s:313:\"a:12:{s:2:\"id\";i:144;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"Product-6\";s:4:\"slug\";s:9:\"product-6\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_144/feature/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:128:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";i:0;}\";}', '{\"id\":5,\"guestID\":\"1594471858744\",\"name\":null,\"email\":null,\"guest_meta\":null,\"stripe_customer_id\":null,\"place_order_status\":0,\"created_at\":\"2020-07-11 12:51:02\",\"updated_at\":\"2020-07-11 12:51:02\"}', 1594471858744, '149.35', '', '', '', 'Confirm', NULL, NULL, '2020-07-11 08:34:48', '2020-07-11 08:34:48'),
(32, 'wcXyia7xeH', 'admin@admin.com', 'a:24:{s:6:\"_token\";s:40:\"FNdl2ZbcRxwQxf8KZs5FsUExKr8VJOAMpvW0jK8b\";s:4:\"name\";s:5:\"admin\";s:5:\"email\";s:15:\"admin@admin.com\";s:7:\"address\";s:13:\"Bolden Market\";s:6:\"mobile\";s:10:\"5645656446\";s:5:\"pcode\";s:6:\"112121\";s:4:\"city\";s:7:\"Karachi\";s:5:\"state\";s:5:\"Sindh\";s:9:\"d-country\";s:8:\"Anguilla\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:13:\"Bolden Market\";s:11:\"other-pcode\";s:6:\"112121\";s:10:\"other-city\";s:7:\"Karachi\";s:11:\"other-state\";s:5:\"Sindh\";s:13:\"other-country\";s:8:\"Anguilla\";s:8:\"shipping\";N;s:7:\"payment\";s:6:\"stripe\";s:10:\"orderParts\";s:1:\"3\";s:13:\"currentUserID\";s:1:\"1\";s:8:\"subtotal\";s:5:\"63.35\";s:11:\"stripeToken\";s:28:\"tok_1H6f5tAedkiOiGZjhRJktTgo\";}', 'a:1:{i:0;s:1133:\"a:12:{s:2:\"id\";i:158;s:7:\"user_id\";i:1;s:5:\"title\";s:20:\"Testing Product Dumy\";s:4:\"slug\";s:20:\"testing-product-dumy\";s:11:\"description\";s:786:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at mi eget tortor egestas rhoncus. Mauris dictum, dui a luctus aliquet, quam magna laoreet massa, vel fringilla libero leo ac massa. Quisque quam odio, finibus quis tempor vitae, ultricies sagittis est. Donec pharetra lacinia velit ac tincidunt. Aenean et ligula pulvinar, tristique justo non, facilisis augue. Nunc a auctor ligula, sed facilisis sapien. Sed lectus ligula, semper eget ultricies non, sagittis eget nisl. Nam et leo semper, mollis tortor quis, accumsan metus. Morbi eget sem eu lectus aliquet congue. Vivamus interdum elit et est aliquet maximus. Sed convallis malesuada tristique.</span><br></p>\";s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_158/feature/eiCpdpOuGpXOXoQ1IYwrZJxHd4qJ8Oph5PDGABsR.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:55;s:6:\"status\";i:1;s:12:\"variation_id\";N;}\";}', 'a:1:{i:0;s:130:\"a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:5.4;}\";}', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":\"2020-06-04 15:22:05\",\"verified\":1,\"status\":1,\"profile_pic\":\"upload\\/users\\/user_1\\/profile_pic\\/rFVZ3h3807vwHApf9PcbSlzmgSBIzMZN9J3gRmNM.jpeg\",\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-06-22 20:05:24\"}', 1, '63.35', '', '', '', 'Confirm', 3, 1, '2020-07-19 11:25:34', '2020-07-19 11:25:34'),
(38, 'jXst8OHMxZ', 'yasinmaknojia@gmail.com', 'a:21:{s:6:\"_token\";s:40:\"65zU3fcEHu2ds4pS9syCzrA6FQS1tDLCAnDlXmRf\";s:4:\"name\";s:15:\"yasin Developer\";s:5:\"email\";s:23:\"yasinmaknojia@gmail.com\";s:7:\"address\";s:14:\"56 New Freeway\";s:6:\"mobile\";s:12:\"+14742443359\";s:5:\"pcode\";s:5:\"11721\";s:4:\"city\";s:20:\"Fugiat ut consequunt\";s:5:\"state\";s:3:\"usa\";s:9:\"d-country\";s:7:\"Andorra\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:14:\"56 New Freeway\";s:11:\"other-pcode\";s:5:\"11721\";s:10:\"other-city\";s:20:\"Fugiat ut consequunt\";s:11:\"other-state\";s:3:\"usa\";s:13:\"other-country\";s:7:\"Andorra\";s:6:\"userID\";s:2:\"21\";s:25:\"subtotal-number-formatted\";s:6:\"123.90\";s:8:\"subtotal\";s:5:\"240.7\";}', 'a:1:{i:0;s:421:\"a:12:{s:2:\"id\";i:118;s:7:\"user_id\";i:1;s:5:\"title\";s:10:\"Product-18\";s:4:\"slug\";s:10:\"product-18\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_118/feature/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg\";s:5:\"video\";s:91:\"https://gitlab.com/yasin.sabir/erp/-/blob/master/app/Http/Controllers/AccountController.php\";s:5:\"price\";i:59;s:5:\"stock\";i:15;s:6:\"status\";i:1;s:12:\"variation_id\";s:0:\"\";}\";}', 'a:1:{i:0;N;}', '{\"id\":21,\"name\":\"Developer Tester\",\"email\":\"yasin.100@hotmail.com\",\"email_verified_at\":\"2020-06-04 16:11:31\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD73Y5nUfTEjyr\",\"created_at\":\"2020-04-05 10:07:52\",\"updated_at\":\"2020-06-04 16:11:31\"}', 21, '240.7', '', '', '', 'Confirm', NULL, NULL, '2020-09-22 01:15:58', '2020-09-22 01:15:58'),
(39, 'qBGl07aBre', 'catiqoce@mailinator.com', 'a:21:{s:6:\"_token\";s:40:\"7A2jao3rEJzBPQRDbFtZNdd3uELwy0MIXpNxG9me\";s:4:\"name\";s:15:\"yasin Developer\";s:5:\"email\";s:23:\"catiqoce@mailinator.com\";s:7:\"address\";s:14:\"56 New Freeway\";s:6:\"mobile\";s:12:\"+14742443359\";s:5:\"pcode\";s:5:\"11721\";s:4:\"city\";s:20:\"Fugiat ut consequunt\";s:5:\"state\";s:3:\"usa\";s:9:\"d-country\";s:4:\"Togo\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:14:\"56 New Freeway\";s:11:\"other-pcode\";s:5:\"11721\";s:10:\"other-city\";s:20:\"Fugiat ut consequunt\";s:11:\"other-state\";s:3:\"usa\";s:13:\"other-country\";s:4:\"Togo\";s:6:\"userID\";s:2:\"33\";s:25:\"subtotal-number-formatted\";s:6:\"134.82\";s:8:\"subtotal\";s:7:\"165.785\";}', 'a:1:{i:0;s:321:\"a:12:{s:2:\"id\";i:155;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"post-img1\";s:4:\"slug\";s:9:\"post-img1\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_155/feature/nEfHsDtaInZ4ArHS2FtOvPjJZCRhoMk3fYQHKjC0.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";i:63;s:6:\"status\";i:1;s:12:\"variation_id\";s:0:\"\";}\";}', 'a:1:{i:0;N;}', '{\"id\":33,\"name\":\"Ahmed\",\"email\":\"ahmed@gmail.com\",\"email_verified_at\":\"2020-05-05 20:04:08\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":null,\"created_at\":\"2020-05-05 20:04:08\",\"updated_at\":\"2020-05-05 20:04:08\"}', 33, '165.785', '', '', '', 'Confirm', NULL, NULL, '2020-09-27 12:37:14', '2020-09-27 12:37:14'),
(40, '6sQfVi1l5T', 'yasin.100@hotmail.com', 'a:21:{s:6:\"_token\";s:40:\"7A2jao3rEJzBPQRDbFtZNdd3uELwy0MIXpNxG9me\";s:4:\"name\";s:15:\"yasin Developer\";s:5:\"email\";s:21:\"yasin.100@hotmail.com\";s:7:\"address\";s:14:\"56 New Freeway\";s:6:\"mobile\";s:12:\"+14742443359\";s:5:\"pcode\";s:5:\"11721\";s:4:\"city\";s:20:\"Fugiat ut consequunt\";s:5:\"state\";s:3:\"usa\";s:9:\"d-country\";s:4:\"Togo\";s:9:\"country_0\";s:3:\"8.5\";s:9:\"country_1\";s:3:\"5.6\";s:20:\"default_weight_price\";s:4:\"5.99\";s:21:\"default_shipping_cost\";s:5:\"15.99\";s:13:\"other-address\";s:14:\"56 New Freeway\";s:11:\"other-pcode\";s:5:\"11721\";s:10:\"other-city\";s:20:\"Fugiat ut consequunt\";s:11:\"other-state\";s:3:\"usa\";s:13:\"other-country\";s:4:\"Togo\";s:6:\"userID\";s:2:\"21\";s:25:\"subtotal-number-formatted\";s:5:\"71.40\";s:8:\"subtotal\";s:8:\"123.2701\";}', 'a:1:{i:0;s:324:\"a:12:{s:2:\"id\";i:130;s:7:\"user_id\";i:1;s:5:\"title\";s:10:\"Product-20\";s:4:\"slug\";s:10:\"product-20\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_130/feature/ffTcVtIHULgLm6wH3WPnLquDZHukVxOkLDQNb191.jpeg\";s:5:\"video\";N;s:5:\"price\";i:68;s:5:\"stock\";i:15;s:6:\"status\";i:1;s:12:\"variation_id\";s:0:\"\";}\";}', 'a:1:{i:0;N;}', '{\"id\":21,\"name\":\"Developer Tester\",\"email\":\"yasin.100@hotmail.com\",\"email_verified_at\":\"2020-06-04 16:11:31\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD73Y5nUfTEjyr\",\"created_at\":\"2020-04-05 10:07:52\",\"updated_at\":\"2020-06-04 16:11:31\"}', 21, '123.2701', '', '', '', 'Confirm', NULL, NULL, '2020-09-27 12:42:51', '2020-09-27 12:42:51');

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

CREATE TABLE `order_history` (
  `id` int(11) NOT NULL,
  `content` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  `product_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_meta` longtext,
  `product_variation_id` int(11) DEFAULT NULL,
  `image` longtext,
  `user_meta` longtext,
  `price` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `material` varchar(200) DEFAULT NULL,
  `size` varchar(200) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `extra_detail` longtext,
  `order_type` varchar(255) DEFAULT NULL,
  `back_order_status` int(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_log`
--

INSERT INTO `order_log` (`id`, `user_id`, `product_id`, `product_name`, `product_meta`, `product_variation_id`, `image`, `user_meta`, `price`, `qty`, `material`, `size`, `extra`, `weight`, `extra_detail`, `order_type`, `back_order_status`, `created_at`, `updated_at`) VALUES
(68, '1595446377681', '160', 'product - 001fsdfsdfd', 'a:12:{s:2:\"id\";i:160;s:7:\"user_id\";i:1;s:5:\"title\";s:21:\"product - 001fsdfsdfd\";s:4:\"slug\";s:19:\"product-001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:3:\"sku\";s:7:\"sde-432\";s:5:\"image\";s:80:\"upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:23;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', '', '123', 1, 'Heavy Material', '234cm', 'Yes', 2.5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}', 'front', NULL, '2020-07-22 14:33:07', '2020-07-22 14:33:07'),
(69, '1595446737098', '160', 'product - 001fsdfsdfd', 'a:12:{s:2:\"id\";i:160;s:7:\"user_id\";i:1;s:5:\"title\";s:21:\"product - 001fsdfsdfd\";s:4:\"slug\";s:19:\"product-001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:3:\"sku\";s:7:\"sde-432\";s:5:\"image\";s:80:\"upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:23;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', '', '123', 1, 'Heavy Material', '234cm', 'Yes', 2.5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}', 'front', NULL, '2020-07-22 14:39:17', '2020-07-22 14:39:17'),
(70, '1595446868611', '160', 'product - 001fsdfsdfd', 'a:12:{s:2:\"id\";i:160;s:7:\"user_id\";i:1;s:5:\"title\";s:21:\"product - 001fsdfsdfd\";s:4:\"slug\";s:19:\"product-001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:3:\"sku\";s:7:\"sde-432\";s:5:\"image\";s:80:\"upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:23;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', '', '123', 1, 'Heavy Material', '234cm', 'Yes', 2.5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}', 'front', NULL, '2020-07-22 14:41:24', '2020-07-22 14:41:24'),
(72, '1', '185', 'dasdasdas', 'a:12:{s:2:\"id\";i:185;s:7:\"user_id\";i:21;s:5:\"title\";s:9:\"dasdasdas\";s:4:\"slug\";s:9:\"dasdasdas\";s:11:\"description\";s:17:\"<p>dasdasdasd</p>\";s:3:\"sku\";s:10:\"sde-432ddd\";s:5:\"image\";s:80:\"upload/product/product_185/feature/oM7SuuOkvP7nneIAJgSqUHD8vjnfWTNDzBNjYAV3.jpeg\";s:5:\"video\";N;s:5:\"price\";i:550;s:5:\"stock\";i:30;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/product_185/feature/oM7SuuOkvP7nneIAJgSqUHD8vjnfWTNDzBNjYAV3.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"yasin@hztech.biz\",\"email_verified_at\":\"2020-06-04 15:22:05\",\"verified\":1,\"status\":1,\"profile_pic\":\"upload\\/users\\/user_1\\/profile_pic\\/rFVZ3h3807vwHApf9PcbSlzmgSBIzMZN9J3gRmNM.jpeg\",\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-08-23 08:03:27\"}', '550', 1, 'Heavy Material', '234cm', 'Yes', 5.4, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:5.4;}', 'front', NULL, '2020-08-23 13:45:59', '2020-08-23 13:45:59'),
(191, '21', '146', 'Product-10', 'a:12:{s:2:\"id\";i:146;s:7:\"user_id\";i:1;s:5:\"title\";s:10:\"Product-10\";s:4:\"slug\";s:10:\"product-10\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_146/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";i:43;s:6:\"status\";i:1;s:12:\"variation_id\";s:0:\"\";}', NULL, 'upload/product/product_146/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg', '{\"id\":21,\"name\":\"Developer Tester\",\"email\":\"yasin.100@hotmail.com\",\"email_verified_at\":\"2020-06-04 16:11:31\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD73Y5nUfTEjyr\",\"created_at\":\"2020-04-05 10:07:52\",\"updated_at\":\"2020-06-04 16:11:31\"}', '145', 1, 'Hello,Worlf', '240cm,566cm', 'Yes', 5.99, NULL, 'back-order', 1, '2020-09-27 15:48:18', '2020-09-27 15:48:18');

-- --------------------------------------------------------

--
-- Table structure for table `order_meta`
--

CREATE TABLE `order_meta` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `name`) VALUES
(1, 'processing'),
(2, 'hold');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('qwerty@gmail.com', '$2y$10$6B0tWaLL4uyCoVnzzcBSLuVuV1jvB.NM33OYyhFPue9L5eDPeCxpW', '2020-03-01 12:34:10'),
('yasin@hztech.biz', '$2y$10$0jcNi56MV.cn/WQaeR2UrePLpNs/O4.rNBFZUHpfvfdY0k5cYbj4C', '2020-05-03 08:42:33'),
('ahmed@gmail.com', '$2y$10$NIv2PhGAAVJssEw42Y.h/.7SBKVe35adVJwjEQ3NGyMd08iwAzIKO', '2020-06-04 04:25:08'),
('yasin.100@hotmail.com', '$2y$10$mTod541/7LV9.wwpzCoznO0e4n56w7nJE4Qs0yz.CUBh0FE/M4lR.', '2020-06-06 03:50:09');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productimgtemp`
--

CREATE TABLE `productimgtemp` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` longtext,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4,
  `sku` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `image` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `video` longtext COLLATE utf8_unicode_ci,
  `price` int(200) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `title`, `slug`, `description`, `sku`, `image`, `video`, `price`, `stock`, `status`, `created_at`, `updated_at`) VALUES
(117, 1, 'Product-17fdf', 'product-17', NULL, NULL, 'upload/product/product_117/feature/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg', 'null', 59, 15, 1, '2020-03-28 11:28:36', '2020-08-02 12:48:22'),
(118, 1, 'Product-18', 'product-18', NULL, NULL, 'upload/product/product_118/feature/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg', 'https://gitlab.com/yasin.sabir/erp/-/blob/master/app/Http/Controllers/AccountController.php', 59, 15, 1, '2020-03-28 11:28:36', '2020-08-02 12:48:06'),
(123, 1, 'qwerty098 7', 'qwerty098', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc semper, nisl id ultrices venenatis, diam odio posuere nulla, non consectetur arcu quam vel lorem. Nunc ornare diam in ligula porta, nec fermentum arcu vestibulum. Aliquam risus nisi, fringilla nec rutrum nec, placerat a elit.098 7</span><br></p>', NULL, 'upload/product/product_123/feature/4VPxihSjcrQnnyAgWhebxD4KcPxqP9VlijOzUdlp.jpeg', 'https://gitlab.com/yasin.sabir/erp/-/tree/master/app/Http/Controllers', 777, 15, 1, '2020-04-04 07:07:36', '2020-08-02 12:47:55'),
(126, 1, 'Galactic Atoms', 'galactic-atoms', NULL, NULL, 'upload/product/product_126/feature/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png', NULL, 68, 15, 1, '2020-04-22 15:28:14', '2020-08-02 12:44:45'),
(127, 1, 'Space 1', 'space-1', NULL, NULL, 'upload/product/product_127/feature/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg', NULL, 68, 15, 1, '2020-04-22 15:28:14', '2020-08-02 12:44:16'),
(128, 1, 'Furries Special Product 001', 'furries-special-product-001', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>', 'sde-432133', 'upload/product/product_128/feature/MHnah64XPz60yOFOKDfitas5E5d5al5GLcQpyJuk.jpeg', NULL, 123, 10, 1, '2020-05-03 05:53:54', '2020-08-02 12:43:47'),
(129, 1, 'Dummy Product 001', 'dummy-product-001', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>', NULL, 'upload/product/product_129/feature/7caLagd95OpXYOnah5zZNJyhgv2AFBQU3wUjTAnL.jpeg', NULL, 550, 11, 1, '2020-05-03 05:57:31', '2020-08-02 12:43:37'),
(130, 1, 'Product-20', 'product-20', NULL, NULL, 'upload/product/product_130/feature/ffTcVtIHULgLm6wH3WPnLquDZHukVxOkLDQNb191.jpeg', NULL, 68, 14, 1, '2020-05-03 05:58:21', '2020-09-27 12:42:51'),
(131, 1, 'Product-21', 'product-21', NULL, NULL, 'upload/product/product_131/feature/kdoWT5HZY8OEZnzuJz3aDaRajZBm2nEDCTOEckSf.jpeg', NULL, 68, 15, 1, '2020-05-03 05:58:22', '2020-08-02 12:40:44'),
(132, 21, 'Artist Product -001', 'artist-product-001', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>', NULL, 'upload/product/product_132/feature/ameE08LT2Pp8xq3c8bN2IpRgmFWHJP96KcfggdxK.jpeg', NULL, 857, 1, 0, '2020-05-03 06:41:10', '2020-06-11 15:14:45'),
(142, 1, 'Product-4', 'product-4', NULL, NULL, 'upload/product/product_142/feature/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg', NULL, 145, 15, 1, '2020-05-04 13:39:53', '2020-08-02 12:39:51'),
(144, 1, 'Product-6', 'product-6', NULL, NULL, 'upload/product/product_144/feature/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg', NULL, 145, 33, 1, '2020-05-04 13:39:53', '2020-08-02 12:38:35'),
(145, 1, 'Product-7', 'product-7', NULL, NULL, 'upload/product/product_145/feature/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg', NULL, 145, 33, 1, '2020-05-04 13:39:53', '2020-08-02 12:38:09'),
(146, 1, 'Product-10', 'product-10', NULL, NULL, 'upload/product/product_146/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg', NULL, 145, 43, 1, '2020-05-04 13:39:53', '2020-08-02 12:36:32'),
(147, 1, 'Product-11', 'product-11', NULL, NULL, 'upload/product/product_147/feature/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg', NULL, 145, 23, 1, '2020-05-04 13:39:53', '2020-08-02 12:36:05'),
(148, 1, 'Product-13', 'product-13', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>', NULL, 'upload/product/product_148/feature/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg', 'https://www.youtube.com/watch?v=6qBTWBd7TGM', 145, 55, 1, '2020-05-04 13:39:54', '2020-07-22 15:30:20'),
(149, 1, 'Product-19', 'product-19', NULL, NULL, 'upload/product/product_149/feature/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg', NULL, 145, 85, 1, '2020-05-04 13:39:54', '2020-08-02 12:35:40'),
(155, 1, 'post-img1', 'post-img1', NULL, NULL, 'upload/product/product_155/feature/nEfHsDtaInZ4ArHS2FtOvPjJZCRhoMk3fYQHKjC0.jpeg', NULL, 126, 63, 1, '2020-05-07 15:03:10', '2020-06-02 14:41:27'),
(156, 1, 'post-img2', 'post-img2', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>', NULL, 'upload/product/product_156/feature/SJJ05fADEODOCttITm0UsVZXYQZBDut4PBBrxiFX.jpeg', NULL, 126, 20, 1, '2020-05-07 15:03:11', '2020-06-08 12:17:35'),
(157, 1, 'cat-widget1', 'cat-widget1', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis. Fusce at pretium urna, quis pharetra elit. Curabitur sollicitudin pellentesque ex sit amet imperdiet. Aenean erat ante, accumsan venenatis tincidunt a, tincidunt sed leo. Morbi scelerisque tempus neque vel pretium. Vivamus ac erat cursus, commodo ligula et, eleifend nulla. Nulla sit amet volutpat turpis. Suspendisse pretium maximus sem, at fermentum velit viverra sodales.</span>', NULL, 'upload/product/product_157/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg', NULL, 126, 63, 1, '2020-05-07 15:14:23', '2020-06-08 12:17:22'),
(158, 1, 'Testing Product Dumy', 'testing-product-dumy', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at mi eget tortor egestas rhoncus. Mauris dictum, dui a luctus aliquet, quam magna laoreet massa, vel fringilla libero leo ac massa. Quisque quam odio, finibus quis tempor vitae, ultricies sagittis est. Donec pharetra lacinia velit ac tincidunt. Aenean et ligula pulvinar, tristique justo non, facilisis augue. Nunc a auctor ligula, sed facilisis sapien. Sed lectus ligula, semper eget ultricies non, sagittis eget nisl. Nam et leo semper, mollis tortor quis, accumsan metus. Morbi eget sem eu lectus aliquet congue. Vivamus interdum elit et est aliquet maximus. Sed convallis malesuada tristique.</span><br></p>', NULL, 'upload/product/product_158/feature/eiCpdpOuGpXOXoQ1IYwrZJxHd4qJ8Oph5PDGABsR.jpeg', NULL, 123, 55, 1, '2020-05-19 22:09:35', '2020-06-08 12:07:54'),
(160, 1, 'product - 001fsdfsdfd', 'product-001fsdfsdfd', '<p>fsdfdfdfsdf</p>', 'sde-432', 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', NULL, 123, 23, 1, '2020-06-11 15:50:25', '2020-06-13 07:45:59'),
(185, 21, 'dasdasdas', 'dasdasdas', '<p>dasdasdasd</p>', 'sde-432ddd', 'upload/product/product_185/feature/oM7SuuOkvP7nneIAJgSqUHD8vjnfWTNDzBNjYAV3.jpeg', NULL, 550, 30, 1, '2020-06-13 07:37:18', '2020-06-16 04:14:51');

-- --------------------------------------------------------

--
-- Table structure for table `products_galleries`
--

CREATE TABLE `products_galleries` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image` longtext CHARACTER SET utf8mb4
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_galleries`
--

INSERT INTO `products_galleries` (`id`, `product_id`, `image`) VALUES
(107, 128, 'upload/product/product_128/gallery/z8uCIFusBb56gx0AU5fgH4QsKoGgRbUnrPsknzmg.jpeg'),
(108, 128, 'upload/product/product_128/gallery/z8uCIFusBb56gx0AU5fgH4QsKoGgRbUnrPsknzmg.jpeg'),
(109, 128, 'upload/product/product_128/gallery/z8uCIFusBb56gx0AU5fgH4QsKoGgRbUnrPsknzmg.jpeg'),
(110, 129, 'upload/product/product_129/gallery/5HOGLRMppscEk3XmylWqNqqAHrioYOcQK1uS7rnM.jpeg'),
(111, 129, 'upload/product/product_129/gallery/5HOGLRMppscEk3XmylWqNqqAHrioYOcQK1uS7rnM.jpeg'),
(112, 132, 'upload/product/product_132/gallery/agqE1ipVY6CRqNcLWX5u59s25Bqi6zVzo7wYDQ0M.jpeg'),
(113, 132, 'upload/product/product_132/gallery/MfdyXWAmI7otVlITFGghoR7SXeiEPDRi7xjmUMvi.jpeg'),
(114, 157, 'upload/product/product_157/gallery/cmhXG0RA7PWKJrj6mxHDfIw5kNy9bxX4hPrnaHvs.jpeg'),
(121, 157, 'upload/product/product_157/gallery/1Qe9fxCERoqd4gFPpgERzc4CmCzwjlZAGORFj1dA.jpeg'),
(123, 157, 'upload/product/product_157/gallery/8plZYKK34u3q3g4BURJf9MvNFSwKkLUn0LWmhRHm.jpeg'),
(124, 157, 'upload/product/product_157/gallery/bRruO0Vix1wa3EKih0Gj4uSDYfv10N7Ndoznk6T6.jpeg'),
(125, 158, 'upload/product/product_158/gallery/Uxw44QdU2G7ec2GZrvOIV2VPKP8Bx29Pvp8Nvvm6.jpeg'),
(126, 158, 'upload/product/product_158/gallery/Uxw44QdU2G7ec2GZrvOIV2VPKP8Bx29Pvp8Nvvm6.jpeg'),
(129, 160, 'upload/product/product_160/gallery/i3ZPKCr2YY7PZhSUGOM3QG7BrQ68fw2AZpdlVYjZ.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `products_metas`
--

CREATE TABLE `products_metas` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_meta` longtext CHARACTER SET utf8mb4 NOT NULL,
  `product_meta_value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_metas`
--

INSERT INTO `products_metas` (`id`, `product_id`, `product_meta`, `product_meta_value`) VALUES
(1335, 117, 'slug', 'product-17fdf'),
(1336, 117, 'price', '59.0'),
(1337, 117, 'sale_price', '0'),
(1338, 117, 'sale_start_date', 'Invalid date '),
(1339, 117, 'sale_end_date', ' Invalid date'),
(1340, 117, 'sku', NULL),
(1341, 117, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1342, 117, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(1343, 117, 'featured_image', 'upload/product/product_117/feature/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg'),
(1344, 117, 'stock', '15'),
(1345, 117, 'material', 'Heavy_Material,Soft_Quality'),
(1346, 117, 'sizes', '240cm,566cm'),
(1347, 117, 'product_features_description', ''),
(1348, 117, 'stock_manage_chk', 'unchecked'),
(1349, 117, 'stock_threshold', '10'),
(1350, 117, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:13:\"Product-17fdf\";s:11:\"description\";N;s:13:\"regular_price\";s:4:\"59.0\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"15\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}s:7:\"artists\";s:2:\"18\";s:12:\"prevs_artist\";N;}'),
(1351, 117, 'product_video_description', NULL),
(1352, 117, 'product_video', 'null'),
(1353, 118, 'slug', 'product-18'),
(1354, 118, 'price', '59.0'),
(1355, 118, 'sale_price', '0'),
(1356, 118, 'sale_start_date', 'Invalid date '),
(1357, 118, 'sale_end_date', ' Invalid date'),
(1358, 118, 'sku', NULL),
(1359, 118, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1360, 118, 'categories', 'a:1:{i:0;s:2:\"64\";}'),
(1361, 118, 'featured_image', 'upload/product/product_118/feature/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg'),
(1362, 118, 'stock', '15'),
(1363, 118, 'material', 'heelo,world,gummiesworld,goodbuy'),
(1364, 118, 'sizes', '240cm,566cm'),
(1365, 118, 'product_features_description', ''),
(1366, 118, 'stock_manage_chk', 'unchecked'),
(1367, 118, 'stock_threshold', '10'),
(1368, 118, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:10:\"Product-18\";s:11:\"description\";N;s:13:\"regular_price\";s:4:\"59.0\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"15\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:32:\"heelo,world,gummiesworld,goodbuy\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"64\";}s:4:\"tags\";a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}s:7:\"artists\";s:2:\"11\";s:12:\"prevs_artist\";N;}'),
(1369, 118, 'product_video_description', NULL),
(1370, 118, 'product_video', 'https://gitlab.com/yasin.sabir/erp/-/blob/master/app/Http/Controllers/AccountController.php'),
(1451, 123, 'slug', 'qwerty098-7'),
(1452, 123, 'price', '777'),
(1453, 123, 'sale_price', '600'),
(1454, 123, 'sale_start_date', '04/04/2020 '),
(1455, 123, 'sale_end_date', ' 04/04/2020'),
(1456, 123, 'sku', NULL),
(1457, 123, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"6\";}'),
(1458, 123, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(1459, 123, 'featured_image', 'upload/product/product_123/feature/4VPxihSjcrQnnyAgWhebxD4KcPxqP9VlijOzUdlp.jpeg'),
(1460, 123, 'stock', '15'),
(1461, 123, 'material', 'Hello,Worlf'),
(1462, 123, 'sizes', '234cm,350cm'),
(1463, 123, 'stock_manage_chk', 'unchecked'),
(1464, 123, 'stock_threshold', '10'),
(1465, 123, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:11:\"qwerty098 7\";s:11:\"description\";s:693:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc semper, nisl id ultrices venenatis, diam odio posuere nulla, non consectetur arcu quam vel lorem. Nunc ornare diam in ligula porta, nec fermentum arcu vestibulum. Aliquam risus nisi, fringilla nec rutrum nec, placerat a elit.098 7</span><br></p>\";s:13:\"regular_price\";s:3:\"777\";s:11:\"sale_prices\";s:3:\"600\";s:16:\"sales_price_schd\";s:23:\"04/04/2020 - 04/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"15\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";s:373:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula.098</span><br></p>\";s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:2:{i:0;s:1:\"5\";i:1;s:1:\"6\";}s:7:\"artists\";s:1:\"9\";s:12:\"prevs_artist\";N;}'),
(1466, 123, 'product_video_description', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula.098</span><br></p>'),
(1467, 123, 'product_video', 'https://gitlab.com/yasin.sabir/erp/-/tree/master/app/Http/Controllers'),
(1508, 126, 'slug', 'galactic-atoms'),
(1509, 126, 'price', '68'),
(1510, 126, 'sale_price', '0'),
(1511, 126, 'sale_start_date', 'Invalid date '),
(1512, 126, 'sale_end_date', ' Invalid date'),
(1513, 126, 'sku', NULL),
(1514, 126, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1515, 126, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1516, 126, 'featured_image', 'upload/product/product_126/feature/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png'),
(1517, 126, 'stock', '15'),
(1518, 126, 'material', 'Heavy_Material,Soft_Quality'),
(1519, 126, 'sizes', '240cm,599cm'),
(1520, 126, 'product_features_description', ''),
(1521, 126, 'stock_manage_chk', 'unchecked'),
(1522, 126, 'stock_threshold', '10'),
(1523, 126, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:14:\"Galactic Atoms\";s:11:\"description\";N;s:13:\"regular_price\";s:2:\"68\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"15\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,599cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}s:7:\"artists\";s:2:\"12\";s:12:\"prevs_artist\";N;}'),
(1524, 126, 'product_video_description', NULL),
(1525, 126, 'product_video', NULL),
(1526, 127, 'slug', 'space-1'),
(1527, 127, 'price', '68'),
(1528, 127, 'sale_price', '0'),
(1529, 127, 'sale_start_date', 'Invalid date '),
(1530, 127, 'sale_end_date', ' Invalid date'),
(1531, 127, 'sku', NULL),
(1532, 127, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1533, 127, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1534, 127, 'featured_image', 'upload/product/product_127/feature/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg'),
(1535, 127, 'stock', '15'),
(1536, 127, 'material', 'Heavy_Material,Soft_Quality'),
(1537, 127, 'sizes', '240cm,566cm'),
(1538, 127, 'product_features_description', ''),
(1539, 127, 'stock_manage_chk', 'unchecked'),
(1540, 127, 'stock_threshold', '10'),
(1541, 127, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:7:\"Space 1\";s:11:\"description\";N;s:13:\"regular_price\";s:2:\"68\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"15\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}s:7:\"artists\";s:2:\"15\";s:12:\"prevs_artist\";N;}'),
(1542, 127, 'product_video_description', NULL),
(1543, 127, 'product_video', NULL),
(1544, 128, 'slug', 'furries-special-product-001'),
(1545, 128, 'price', '123'),
(1546, 128, 'sale_price', '56'),
(1547, 128, 'sale_start_date', '05/03/2020 '),
(1548, 128, 'sale_end_date', ' 05/03/2020'),
(1549, 128, 'sku', 'sde-432133'),
(1550, 128, 'tags', 'a:1:{i:0;s:1:\"5\";}'),
(1551, 128, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(1552, 128, 'featured_image', 'upload/product/product_128/feature/MHnah64XPz60yOFOKDfitas5E5d5al5GLcQpyJuk.jpeg'),
(1553, 128, 'stock', '10'),
(1554, 128, 'material', 'qwerty,helloworld,good,bye,gummies'),
(1555, 128, 'sizes', '234cm,350cm'),
(1556, 128, 'stock_manage_chk', 'unchecked'),
(1557, 128, 'stock_threshold', '10'),
(1558, 128, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:27:\"Furries Special Product 001\";s:11:\"description\";s:455:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>\";s:13:\"regular_price\";s:3:\"123\";s:11:\"sale_prices\";s:2:\"56\";s:16:\"sales_price_schd\";s:23:\"05/03/2020 - 05/03/2020\";s:11:\"product_sku\";s:10:\"sde-432133\";s:14:\"stock_quantity\";s:2:\"10\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:34:\"qwerty,helloworld,good,bye,gummies\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:1:{i:0;s:1:\"5\";}s:7:\"artists\";s:2:\"13\";s:12:\"prevs_artist\";N;}'),
(1559, 128, 'product_video_description', NULL),
(1560, 128, 'product_video', NULL),
(1561, 129, 'slug', 'dummy-product-001'),
(1562, 129, 'price', '550'),
(1563, 129, 'sale_price', '230'),
(1564, 129, 'sale_start_date', '05/03/2020 '),
(1565, 129, 'sale_end_date', ' 05/03/2020'),
(1566, 129, 'sku', NULL),
(1567, 129, 'tags', 'a:1:{i:0;s:1:\"6\";}'),
(1568, 129, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1569, 129, 'featured_image', 'upload/product/product_129/feature/7caLagd95OpXYOnah5zZNJyhgv2AFBQU3wUjTAnL.jpeg'),
(1570, 129, 'stock', '11'),
(1571, 129, 'material', 'Hello,Worlf'),
(1572, 129, 'sizes', '234cm,350cm'),
(1573, 129, 'stock_manage_chk', 'unchecked'),
(1574, 129, 'stock_threshold', '10'),
(1575, 129, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:17:\"Dummy Product 001\";s:11:\"description\";s:455:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>\";s:13:\"regular_price\";s:3:\"550\";s:11:\"sale_prices\";s:3:\"230\";s:16:\"sales_price_schd\";s:23:\"05/03/2020 - 05/03/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"11\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"68\";}s:4:\"tags\";a:1:{i:0;s:1:\"6\";}s:7:\"artists\";s:2:\"14\";s:12:\"prevs_artist\";N;}'),
(1576, 129, 'product_video_description', NULL),
(1577, 129, 'product_video', NULL),
(1578, 130, 'slug', 'product-20'),
(1579, 130, 'price', '68'),
(1580, 130, 'sale_price', '0'),
(1581, 130, 'sale_start_date', 'Invalid date '),
(1582, 130, 'sale_end_date', ' Invalid date'),
(1583, 130, 'sku', NULL),
(1584, 130, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1585, 130, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1586, 130, 'featured_image', 'upload/product/product_130/feature/ffTcVtIHULgLm6wH3WPnLquDZHukVxOkLDQNb191.jpeg'),
(1587, 130, 'stock', '14'),
(1588, 130, 'material', 'Heavy_Material,Soft_Quality'),
(1589, 130, 'sizes', '234cm,350cm'),
(1590, 130, 'product_features_description', ''),
(1591, 130, 'stock_manage_chk', 'unchecked'),
(1592, 130, 'stock_threshold', '10'),
(1593, 130, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:10:\"Product-20\";s:11:\"description\";N;s:13:\"regular_price\";s:2:\"68\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"15\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}s:7:\"artists\";s:2:\"16\";s:12:\"prevs_artist\";N;}'),
(1594, 130, 'product_video_description', NULL),
(1595, 130, 'product_video', NULL),
(1596, 131, 'slug', 'product-21'),
(1597, 131, 'price', '68'),
(1598, 131, 'sale_price', '0'),
(1599, 131, 'sale_start_date', 'Invalid date '),
(1600, 131, 'sale_end_date', ' Invalid date'),
(1601, 131, 'sku', NULL),
(1602, 131, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1603, 131, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1604, 131, 'featured_image', 'upload/product/product_131/feature/kdoWT5HZY8OEZnzuJz3aDaRajZBm2nEDCTOEckSf.jpeg'),
(1605, 131, 'stock', '15'),
(1606, 131, 'material', 'Heavy_Material,Soft_Quality'),
(1607, 131, 'sizes', '240cm,566cm'),
(1608, 131, 'product_features_description', ''),
(1609, 131, 'stock_manage_chk', 'unchecked'),
(1610, 131, 'stock_threshold', '10'),
(1611, 131, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:10:\"Product-21\";s:11:\"description\";N;s:13:\"regular_price\";s:2:\"68\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"15\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}s:7:\"artists\";s:2:\"14\";s:12:\"prevs_artist\";N;}'),
(1612, 131, 'product_video_description', NULL),
(1613, 131, 'product_video', NULL),
(1614, 132, 'slug', 'artist-product-001'),
(1615, 132, 'price', '857'),
(1616, 132, 'sale_price', '0'),
(1617, 132, 'sale_start_date', '05/03/2020 '),
(1618, 132, 'sale_end_date', ' 05/03/2020'),
(1619, 132, 'sku', NULL),
(1620, 132, 'tags', 'a:1:{i:0;s:1:\"6\";}'),
(1621, 132, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1622, 132, 'featured_image', 'upload/product/artist-product-001/feature/ameE08LT2Pp8xq3c8bN2IpRgmFWHJP96KcfggdxK.jpeg'),
(1623, 132, 'stock', '15'),
(1624, 132, 'material', 'Hello,Worlf'),
(1625, 132, 'sizes', '34cm,343cm'),
(1626, 132, 'stock_manage_chk', NULL),
(1627, 132, 'stock_threshold', '10'),
(1628, 132, 'serialize_data', 'a:15:{s:6:\"_token\";s:40:\"hgcoNGT5S62RVbq5Xbv9Jk7Ys3cz8H7u3uu69hRe\";s:12:\"product_name\";s:19:\"Artist Product -001\";s:11:\"description\";s:455:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>\";s:13:\"regular_price\";s:3:\"857\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:23:\"05/03/2020 - 05/03/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:10:\"34cm,343cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"68\";}s:4:\"tags\";a:1:{i:0;s:1:\"6\";}}'),
(1629, 132, 'product_video_description', NULL),
(1630, 132, 'product_video', NULL),
(1793, 142, 'slug', 'product-4'),
(1794, 142, 'price', '145'),
(1795, 142, 'sale_price', '0'),
(1796, 142, 'sale_start_date', 'Invalid date '),
(1797, 142, 'sale_end_date', ' Invalid date'),
(1798, 142, 'sku', NULL),
(1799, 142, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1800, 142, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1801, 142, 'featured_image', 'upload/product/product_142/feature/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg'),
(1802, 142, 'stock', '15'),
(1803, 142, 'material', 'Heavy_Material,Soft_Quality'),
(1804, 142, 'sizes', '240cm,566cm'),
(1805, 142, 'product_features_description', ''),
(1806, 142, 'stock_manage_chk', 'unchecked'),
(1807, 142, 'stock_threshold', '10'),
(1808, 142, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:9:\"Product-4\";s:11:\"description\";N;s:13:\"regular_price\";s:3:\"145\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"15\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"72\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}s:7:\"artists\";N;s:12:\"prevs_artist\";N;}'),
(1809, 142, 'product_video_description', NULL),
(1810, 142, 'product_video', NULL),
(1829, 144, 'slug', 'product-6'),
(1830, 144, 'price', '145'),
(1831, 144, 'sale_price', '0'),
(1832, 144, 'sale_start_date', 'Invalid date '),
(1833, 144, 'sale_end_date', ' Invalid date'),
(1834, 144, 'sku', NULL),
(1835, 144, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1836, 144, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1837, 144, 'featured_image', 'upload/product/product_144/feature/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg'),
(1838, 144, 'stock', '33'),
(1839, 144, 'material', 'Hello,Worlf'),
(1840, 144, 'sizes', '240cm,566cm'),
(1841, 144, 'product_features_description', ''),
(1842, 144, 'stock_manage_chk', 'unchecked'),
(1843, 144, 'stock_threshold', '10'),
(1844, 144, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:9:\"Product-6\";s:11:\"description\";N;s:13:\"regular_price\";s:3:\"145\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"33\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"72\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}s:7:\"artists\";N;s:12:\"prevs_artist\";N;}'),
(1845, 144, 'product_video_description', NULL),
(1846, 144, 'product_video', NULL),
(1847, 145, 'slug', 'product-7'),
(1848, 145, 'price', '145'),
(1849, 145, 'sale_price', '0'),
(1850, 145, 'sale_start_date', 'Invalid date '),
(1851, 145, 'sale_end_date', ' Invalid date'),
(1852, 145, 'sku', NULL),
(1853, 145, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1854, 145, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1855, 145, 'featured_image', 'upload/product/product_145/feature/3lZq07RUTwAvVFRZzKIIo9jS7r9mHc019vSM0zVW.jpeg'),
(1856, 145, 'stock', '33'),
(1857, 145, 'material', 'Heavy_Material,Soft_Quality'),
(1858, 145, 'sizes', '240cm,566cm'),
(1859, 145, 'product_features_description', ''),
(1860, 145, 'stock_manage_chk', 'unchecked'),
(1861, 145, 'stock_threshold', '10'),
(1862, 145, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:9:\"Product-7\";s:11:\"description\";N;s:13:\"regular_price\";s:3:\"145\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"33\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"72\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}s:7:\"artists\";s:2:\"17\";s:12:\"prevs_artist\";N;}'),
(1863, 145, 'product_video_description', NULL),
(1864, 145, 'product_video', NULL),
(1865, 146, 'slug', 'product-10'),
(1866, 146, 'price', '145'),
(1867, 146, 'sale_price', '0'),
(1868, 146, 'sale_start_date', 'Invalid date '),
(1869, 146, 'sale_end_date', ' Invalid date'),
(1870, 146, 'sku', NULL),
(1871, 146, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1872, 146, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1873, 146, 'featured_image', 'upload/product/product_146/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg'),
(1874, 146, 'stock', '43'),
(1875, 146, 'material', 'Hello,Worlf'),
(1876, 146, 'sizes', '240cm,566cm'),
(1877, 146, 'product_features_description', ''),
(1878, 146, 'stock_manage_chk', 'unchecked'),
(1879, 146, 'stock_threshold', '10'),
(1880, 146, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:10:\"Product-10\";s:11:\"description\";N;s:13:\"regular_price\";s:3:\"145\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"43\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"72\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}s:7:\"artists\";s:2:\"12\";s:12:\"prevs_artist\";N;}'),
(1881, 146, 'product_video_description', NULL),
(1882, 146, 'product_video', NULL),
(1883, 147, 'slug', 'product-11'),
(1884, 147, 'price', '145'),
(1885, 147, 'sale_price', '0'),
(1886, 147, 'sale_start_date', 'Invalid date '),
(1887, 147, 'sale_end_date', ' Invalid date'),
(1888, 147, 'sku', NULL),
(1889, 147, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1890, 147, 'categories', 'a:2:{i:0;s:2:\"67\";i:1;s:2:\"72\";}'),
(1891, 147, 'featured_image', 'upload/product/product_147/feature/S6cEsyM2uVWYTXIS4vwaX7yQgey1uIQ8iUy5pGcn.jpeg'),
(1892, 147, 'stock', '23'),
(1893, 147, 'material', 'Heavy_Material,Soft_Quality'),
(1894, 147, 'sizes', '234cm,350cm'),
(1895, 147, 'product_features_description', ''),
(1896, 147, 'stock_manage_chk', 'unchecked'),
(1897, 147, 'stock_threshold', '10'),
(1898, 147, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:10:\"Product-11\";s:11:\"description\";N;s:13:\"regular_price\";s:3:\"145\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"23\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:2:{i:0;s:2:\"67\";i:1;s:2:\"72\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}s:7:\"artists\";s:2:\"16\";s:12:\"prevs_artist\";N;}'),
(1899, 147, 'product_video_description', NULL),
(1900, 147, 'product_video', NULL),
(1901, 148, 'slug', 'product-13'),
(1902, 148, 'price', '145'),
(1903, 148, 'sale_price', '0'),
(1904, 148, 'sale_start_date', 'Invalid date '),
(1905, 148, 'sale_end_date', ' Invalid date'),
(1906, 148, 'sku', NULL),
(1907, 148, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1908, 148, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1909, 148, 'featured_image', 'upload/product/product_148/feature/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg'),
(1910, 148, 'stock', '55'),
(1911, 148, 'material', 'Hello_world,Worlf_ggg'),
(1912, 148, 'sizes', '234cm,350cm'),
(1913, 148, 'product_features_description', ''),
(1914, 148, 'stock_manage_chk', 'unchecked'),
(1915, 148, 'stock_threshold', '20'),
(1916, 148, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"tvw4UTNbhuii9v6CSZ4yBICFZRuZ0KQ5GmdnG47v\";s:12:\"product_name\";s:10:\"Product-13\";s:11:\"description\";s:612:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>\";s:13:\"regular_price\";s:3:\"145\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"55\";s:19:\"low_stock_threshold\";s:2:\"20\";s:16:\"product_material\";s:21:\"Hello_world,Worlf_ggg\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";s:612:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>\";s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"72\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}s:7:\"artists\";s:1:\"9\";s:12:\"prevs_artist\";s:1:\"9\";}'),
(1917, 148, 'product_video_description', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>'),
(1918, 148, 'product_video', 'https://www.youtube.com/watch?v=6qBTWBd7TGM'),
(1919, 149, 'slug', 'product-19'),
(1920, 149, 'price', '145'),
(1921, 149, 'sale_price', '0'),
(1922, 149, 'sale_start_date', 'Invalid date '),
(1923, 149, 'sale_end_date', ' Invalid date'),
(1924, 149, 'sku', NULL),
(1925, 149, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1926, 149, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1927, 149, 'featured_image', 'upload/product/product_149/feature/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg'),
(1928, 149, 'stock', '85'),
(1929, 149, 'material', 'Hello,Worlf'),
(1930, 149, 'sizes', '240cm,566cm'),
(1931, 149, 'product_features_description', ''),
(1932, 149, 'stock_manage_chk', 'unchecked'),
(1933, 149, 'stock_threshold', '10'),
(1934, 149, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:10:\"Product-19\";s:11:\"description\";N;s:13:\"regular_price\";s:3:\"145\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"85\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";N;s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"72\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}s:7:\"artists\";s:2:\"11\";s:12:\"prevs_artist\";N;}'),
(1935, 149, 'product_video_description', NULL),
(1936, 149, 'product_video', NULL),
(1937, 148, 'artist', '9'),
(2019, 155, 'slug', 'post-img1'),
(2020, 155, 'artist', '13'),
(2021, 155, 'artist_updated_separately', 'no'),
(2022, 155, 'price', '126'),
(2023, 155, 'sale_price', '0'),
(2024, 155, 'sale_start_date', 'Invalid date '),
(2025, 155, 'sale_end_date', ' Invalid date'),
(2026, 155, 'sku', NULL),
(2027, 155, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}'),
(2028, 155, 'categories', 'a:1:{i:0;s:2:\"73\";}'),
(2029, 155, 'featured_image', 'upload/product/post-img1/feature/nEfHsDtaInZ4ArHS2FtOvPjJZCRhoMk3fYQHKjC0.jpeg'),
(2030, 155, 'stock', '63'),
(2031, 155, 'material', 'Heavy_Material,Soft_Quality'),
(2032, 155, 'sizes', '240cm,566cm'),
(2033, 155, 'product_features_description', ''),
(2034, 155, 'stock_manage_chk', 'unchecked'),
(2035, 155, 'stock_threshold', '20'),
(2036, 155, 'serialize_data', 'a:18:{s:6:\"_token\";s:40:\"KNVJ5JbvU7EaSKYSThRRsM8JNk786iIYCrPBUiWe\";s:12:\"product_name\";s:9:\"post-img1\";s:11:\"description\";N;s:13:\"regular_price\";s:3:\"126\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"63\";s:15:\"stock_threshold\";s:2:\"20\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";s:3:\"2.5\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"73\";}s:4:\"tags\";a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}s:7:\"artists\";s:2:\"13\";s:12:\"prevs_artist\";s:2:\"13\";}'),
(2037, 155, 'product_video_description', NULL),
(2038, 155, 'product_video', NULL),
(2039, 156, 'slug', 'post-img2'),
(2040, 156, 'artist', '16'),
(2041, 156, 'artist_updated_separately', 'no'),
(2042, 156, 'price', '126'),
(2043, 156, 'sale_price', '0'),
(2044, 156, 'sale_start_date', 'Invalid date '),
(2045, 156, 'sale_end_date', ' Invalid date'),
(2046, 156, 'sku', NULL),
(2047, 156, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}'),
(2048, 156, 'categories', 'a:1:{i:0;s:2:\"73\";}'),
(2049, 156, 'featured_image', 'upload/product/post-img2/feature/SJJ05fADEODOCttITm0UsVZXYQZBDut4PBBrxiFX.jpeg'),
(2050, 156, 'stock', '20'),
(2051, 156, 'material', 'Heavy_Material,Soft_Quality'),
(2052, 156, 'sizes', '240cm,566cm'),
(2053, 156, 'product_features_description', ''),
(2054, 156, 'stock_manage_chk', 'unchecked'),
(2055, 156, 'stock_threshold', '5'),
(2056, 156, 'serialize_data', 'a:18:{s:6:\"_token\";s:40:\"KNVJ5JbvU7EaSKYSThRRsM8JNk786iIYCrPBUiWe\";s:12:\"product_name\";s:9:\"post-img2\";s:11:\"description\";s:612:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>\";s:13:\"regular_price\";s:3:\"126\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"20\";s:15:\"stock_threshold\";s:1:\"5\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";s:1:\"6\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"73\";}s:4:\"tags\";a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}s:7:\"artists\";s:2:\"16\";s:12:\"prevs_artist\";s:2:\"16\";}'),
(2057, 156, 'product_video_description', NULL),
(2058, 156, 'product_video', NULL),
(2059, 157, 'slug', 'cat-widget1'),
(2060, 157, 'artist', '13'),
(2061, 157, 'artist_updated_separately', 'no'),
(2062, 157, 'price', '126'),
(2063, 157, 'sale_price', '0'),
(2064, 157, 'sale_start_date', 'Invalid date '),
(2065, 157, 'sale_end_date', ' Invalid date'),
(2066, 157, 'sku', NULL),
(2067, 157, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}'),
(2068, 157, 'categories', 'a:1:{i:0;s:2:\"73\";}'),
(2069, 157, 'featured_image', 'upload/product/cat-widget1/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg'),
(2070, 157, 'stock', '63'),
(2071, 157, 'material', 'Heavy_Material,Soft_Quality'),
(2072, 157, 'sizes', '240cm,566cm'),
(2073, 157, 'product_features_description', ''),
(2074, 157, 'stock_manage_chk', 'unchecked'),
(2075, 157, 'stock_threshold', '12'),
(2076, 157, 'serialize_data', 'a:18:{s:6:\"_token\";s:40:\"KNVJ5JbvU7EaSKYSThRRsM8JNk786iIYCrPBUiWe\";s:12:\"product_name\";s:11:\"cat-widget1\";s:11:\"description\";s:604:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis. Fusce at pretium urna, quis pharetra elit. Curabitur sollicitudin pellentesque ex sit amet imperdiet. Aenean erat ante, accumsan venenatis tincidunt a, tincidunt sed leo. Morbi scelerisque tempus neque vel pretium. Vivamus ac erat cursus, commodo ligula et, eleifend nulla. Nulla sit amet volutpat turpis. Suspendisse pretium maximus sem, at fermentum velit viverra sodales.</span>\";s:13:\"regular_price\";s:3:\"126\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"63\";s:15:\"stock_threshold\";s:2:\"12\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";s:3:\"3.6\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"73\";}s:4:\"tags\";a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}s:7:\"artists\";s:2:\"13\";s:12:\"prevs_artist\";s:2:\"13\";}'),
(2077, 157, 'product_video_description', NULL),
(2078, 157, 'product_video', NULL),
(2079, 158, 'slug', 'testing-product-dumy'),
(2080, 158, 'artist', '19'),
(2081, 158, 'artist_updated_separately', 'no'),
(2082, 158, 'price', '123'),
(2083, 158, 'sale_price', '0'),
(2084, 158, 'sale_start_date', '05/20/2020 '),
(2085, 158, 'sale_end_date', ' 05/20/2020'),
(2086, 158, 'sku', NULL),
(2087, 158, 'tags', 'a:1:{i:0;s:1:\"5\";}'),
(2088, 158, 'categories', 'a:2:{i:0;s:2:\"65\";i:1;s:2:\"66\";}'),
(2089, 158, 'featured_image', 'upload/product/product_158/feature/eiCpdpOuGpXOXoQ1IYwrZJxHd4qJ8Oph5PDGABsR.jpeg'),
(2090, 158, 'stock', '55'),
(2091, 158, 'material', 'Heavy_Material,Soft_Quality'),
(2092, 158, 'sizes', '234cm,350cm'),
(2093, 158, 'stock_manage_chk', 'unchecked'),
(2094, 158, 'stock_threshold', '11'),
(2095, 158, 'serialize_data', 'a:20:{s:6:\"_token\";s:40:\"gExAZ0BEUfPJXIabJekibrVKgOfA0Jh5eHdp8PUY\";s:12:\"product_name\";s:20:\"Testing Product Dumy\";s:11:\"description\";s:786:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at mi eget tortor egestas rhoncus. Mauris dictum, dui a luctus aliquet, quam magna laoreet massa, vel fringilla libero leo ac massa. Quisque quam odio, finibus quis tempor vitae, ultricies sagittis est. Donec pharetra lacinia velit ac tincidunt. Aenean et ligula pulvinar, tristique justo non, facilisis augue. Nunc a auctor ligula, sed facilisis sapien. Sed lectus ligula, semper eget ultricies non, sagittis eget nisl. Nam et leo semper, mollis tortor quis, accumsan metus. Morbi eget sem eu lectus aliquet congue. Vivamus interdum elit et est aliquet maximus. Sed convallis malesuada tristique.</span><br></p>\";s:13:\"regular_price\";s:3:\"123\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:23:\"05/20/2020 - 05/20/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";s:2:\"55\";s:19:\"low_stock_threshold\";s:2:\"11\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";s:3:\"5.4\";s:22:\"product_shipping_time1\";s:1:\"3\";s:22:\"product_shipping_time2\";s:1:\"5\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:2:{i:0;s:2:\"65\";i:1;s:2:\"66\";}s:4:\"tags\";a:1:{i:0;s:1:\"5\";}s:7:\"artists\";s:2:\"19\";s:12:\"prevs_artist\";s:2:\"19\";}'),
(2096, 158, 'product_video_description', NULL),
(2097, 158, 'product_video', NULL),
(2098, 145, 'artist', '17'),
(2099, 145, 'artist_updated_separately', 'yes'),
(2100, 147, 'artist', '16'),
(2101, 147, 'artist_updated_separately', 'yes'),
(2102, 148, 'artist_updated_separately', 'no'),
(2103, 158, 'weight', '5.4'),
(2104, 157, 'weight', '3.6'),
(2105, 156, 'weight', '6'),
(2106, 155, 'weight', '2.5'),
(2127, 160, 'slug', 'product-001fsdfsdfd'),
(2128, 160, 'artist', '18'),
(2129, 160, 'artist_updated_separately', 'yes'),
(2130, 160, 'price', '123'),
(2131, 160, 'sale_price', '56'),
(2132, 160, 'sale_start_date', '06/12/2020 '),
(2133, 160, 'sale_end_date', ' 07/11/2020'),
(2134, 160, 'sku', 'sde-432'),
(2135, 160, 'tags', 'a:1:{i:0;s:1:\"9\";}'),
(2136, 160, 'categories', 'a:1:{i:0;s:2:\"65\";}'),
(2137, 160, 'featured_image', 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg'),
(2138, 160, 'stock', '23'),
(2139, 160, 'material', 'Heavy_Material,Soft_Quality'),
(2140, 160, 'sizes', '234cm,350cm'),
(2141, 160, 'weight', '2.5'),
(2142, 160, 'stock_manage_chk', 'checked'),
(2143, 160, 'stock_threshold', '5'),
(2144, 160, 'serialize_data', 'a:21:{s:6:\"_token\";s:40:\"DTnVgadzP23Mes5prSScrT5IMUtf6BQm4gEjQ3dM\";s:12:\"product_name\";s:21:\"product - 001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:13:\"regular_price\";s:3:\"123\";s:11:\"sale_prices\";s:2:\"56\";s:16:\"sales_price_schd\";s:23:\"06/12/2020 - 07/11/2020\";s:11:\"product_sku\";s:7:\"sde-432\";s:20:\"manage_stock_checked\";N;s:14:\"stock_quantity\";s:2:\"23\";s:19:\"low_stock_threshold\";s:1:\"5\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";s:3:\"2.5\";s:22:\"product_shipping_time1\";N;s:22:\"product_shipping_time2\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"65\";}s:4:\"tags\";a:1:{i:0;s:1:\"9\";}s:7:\"artists\";s:2:\"18\";s:12:\"prevs_artist\";N;}'),
(2145, 160, 'product_video_description', NULL),
(2146, 160, 'product_video', NULL),
(2167, 162, 'slug', 'ffdfsdfdf'),
(2168, 162, 'artist', '21'),
(2169, 162, 'artist_updated_separately', '21'),
(2170, 162, 'price', '123'),
(2171, 162, 'sale_price', '56'),
(2172, 162, 'sale_start_date', '06/12/2020 '),
(2173, 162, 'sale_end_date', ' 06/12/2020'),
(2174, 162, 'sku', 'sde-432fff'),
(2175, 162, 'tags', 'a:1:{i:0;s:1:\"6\";}'),
(2176, 162, 'categories', 'a:1:{i:0;s:2:\"65\";}'),
(2177, 162, 'featured_image', 'upload/product/ffdfsdfdf/feature/3hNatNVrHAjHXDibjCcoWZYtyDaQRYMfTNZcaXFB.jpeg'),
(2178, 162, 'stock', '34'),
(2179, 162, 'material', 'Heavy_Material,Soft_Quality'),
(2180, 162, 'sizes', '234cm,350cm'),
(2181, 162, 'weight', '22'),
(2182, 162, 'stock_manage_chk', 'checked'),
(2183, 162, 'stock_threshold', '10'),
(2184, 162, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"2Ia1rnZHPTr04wGu5SPqeRxJZbcePg5DHI0Fv33S\";s:12:\"product_name\";s:9:\"ffdfsdfdf\";s:11:\"description\";s:20:\"<p>sdfsdfsdfsdfd</p>\";s:13:\"regular_price\";s:3:\"123\";s:11:\"sale_prices\";s:2:\"56\";s:16:\"sales_price_schd\";s:23:\"06/12/2020 - 06/12/2020\";s:11:\"product_sku\";s:10:\"sde-432fff\";s:20:\"manage_stock_checked\";s:7:\"checked\";s:14:\"stock_quantity\";s:2:\"34\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";s:2:\"22\";s:10:\"categories\";a:1:{i:0;s:2:\"65\";}s:4:\"tags\";a:1:{i:0;s:1:\"6\";}}'),
(2185, 162, 'product_video_description', NULL),
(2186, 162, 'product_video', NULL),
(2627, 185, 'slug', 'dasdasdas'),
(2628, 185, 'artist', '21'),
(2629, 185, 'artist_updated_separately', '21'),
(2630, 185, 'price', '550'),
(2631, 185, 'sale_price', '0'),
(2632, 185, 'sale_start_date', '06/13/2020 '),
(2633, 185, 'sale_end_date', ' 06/13/2020'),
(2634, 185, 'sku', 'sde-432ddd'),
(2635, 185, 'tags', 'a:1:{i:0;s:1:\"9\";}'),
(2636, 185, 'categories', 'a:1:{i:0;s:2:\"65\";}'),
(2637, 185, 'featured_image', 'upload/product/product_185/feature/oM7SuuOkvP7nneIAJgSqUHD8vjnfWTNDzBNjYAV3.jpeg'),
(2638, 185, 'stock', '14'),
(2639, 185, 'material', 'Heavy_Material,Soft_Quality'),
(2640, 185, 'sizes', '234cm,350cm'),
(2641, 185, 'weight', '5.4'),
(2642, 185, 'stock_manage_chk', 'checked'),
(2643, 185, 'stock_threshold', '10'),
(2644, 185, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"a7Wn3YLX61poMYBzCl75XWS91UuYQBnECbXE6LkH\";s:12:\"product_name\";s:9:\"dasdasdas\";s:11:\"description\";s:17:\"<p>dasdasdasd</p>\";s:13:\"regular_price\";s:3:\"550\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:23:\"06/13/2020 - 06/13/2020\";s:11:\"product_sku\";s:10:\"sde-432ddd\";s:20:\"manage_stock_checked\";s:7:\"checked\";s:14:\"stock_quantity\";s:2:\"30\";s:19:\"low_stock_threshold\";s:2:\"10\";s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:14:\"product_weight\";s:3:\"5.4\";s:10:\"categories\";a:1:{i:0;s:2:\"65\";}s:4:\"tags\";a:1:{i:0;s:1:\"9\";}}'),
(2645, 185, 'product_video_description', NULL),
(2646, 185, 'product_video', NULL),
(2651, 185, 'user_product_status', '0'),
(2652, 158, 'estimate_shipping', '3-5'),
(2653, 148, 'weight', NULL),
(2654, 148, 'estimate_shipping', '-'),
(2655, 160, 'estimate_shipping', '-'),
(2656, 149, 'artist', '11'),
(2657, 149, 'artist_updated_separately', 'yes'),
(2658, 149, 'weight', NULL),
(2659, 149, 'estimate_shipping', '-'),
(2660, 147, 'weight', NULL),
(2661, 147, 'estimate_shipping', '-'),
(2662, 146, 'artist', '12'),
(2663, 146, 'artist_updated_separately', 'yes'),
(2664, 146, 'weight', NULL),
(2665, 146, 'estimate_shipping', '-'),
(2666, 145, 'weight', NULL),
(2667, 145, 'estimate_shipping', '-'),
(2668, 144, 'artist', NULL),
(2669, 144, 'artist_updated_separately', 'no'),
(2670, 144, 'weight', NULL),
(2671, 144, 'estimate_shipping', '-'),
(2672, 142, 'artist', NULL),
(2673, 142, 'artist_updated_separately', 'no'),
(2674, 142, 'weight', NULL),
(2675, 142, 'estimate_shipping', '-'),
(2676, 131, 'artist', '14'),
(2677, 131, 'artist_updated_separately', 'yes'),
(2678, 131, 'weight', NULL),
(2679, 131, 'estimate_shipping', '-'),
(2680, 130, 'artist', '16'),
(2681, 130, 'artist_updated_separately', 'yes'),
(2682, 130, 'weight', NULL),
(2683, 130, 'estimate_shipping', '-'),
(2684, 129, 'artist', '14'),
(2685, 129, 'artist_updated_separately', 'yes'),
(2686, 129, 'weight', NULL),
(2687, 129, 'estimate_shipping', '-'),
(2688, 128, 'artist', '13'),
(2689, 128, 'artist_updated_separately', 'yes'),
(2690, 128, 'weight', NULL),
(2691, 128, 'estimate_shipping', '-'),
(2692, 127, 'artist', '15'),
(2693, 127, 'artist_updated_separately', 'yes'),
(2694, 127, 'weight', NULL),
(2695, 127, 'estimate_shipping', '-'),
(2696, 126, 'artist', '12'),
(2697, 126, 'artist_updated_separately', 'yes'),
(2698, 126, 'weight', NULL),
(2699, 126, 'estimate_shipping', '-'),
(2700, 123, 'artist', '9'),
(2701, 123, 'artist_updated_separately', 'yes'),
(2702, 123, 'weight', NULL),
(2703, 123, 'estimate_shipping', '-'),
(2704, 118, 'artist', '11'),
(2705, 118, 'artist_updated_separately', 'yes'),
(2706, 118, 'weight', NULL),
(2707, 118, 'estimate_shipping', '-'),
(2708, 117, 'artist', '18'),
(2709, 117, 'artist_updated_separately', 'yes'),
(2710, 117, 'weight', NULL),
(2711, 117, 'estimate_shipping', '-');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute` varchar(255) DEFAULT NULL,
  `value` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `attribute`, `value`, `created_at`, `updated_at`) VALUES
(1, 157, 'color', 'a:4:{i:0;s:3:\"red\";i:1;s:5:\"black\";i:2;s:5:\"green\";i:3;s:4:\"blue\";}', '2020-05-09 16:10:56', '2020-05-09 16:10:56'),
(2, 157, 'size', 'a:3:{i:0;s:5:\"Small\";i:1;s:6:\"Medium\";i:2;s:5:\"Large\";}', '2020-05-09 17:17:46', '2020-05-11 13:36:09');

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `flag` varchar(255) DEFAULT NULL,
  `description` longtext,
  `image` longtext,
  `video` longtext,
  `parent_id` int(11) DEFAULT NULL,
  `voted` varchar(255) DEFAULT NULL,
  `unvoted` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `user_id`, `product_id`, `status`, `flag`, `description`, `image`, `video`, `parent_id`, `voted`, `unvoted`, `created_at`, `updated_at`) VALUES
(5, 1, 126, 'off', 'average', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies tortor sit amet sagittis imperdiet.', 'a:2:{i:0;s:95:\"upload/product_review/product_126/user_1/review_5/Mr6p0B01tHarH79e8Poy9G2J3s0wbP758NEv2sZg.jpeg\";i:1;s:95:\"upload/product_review/product_126/user_1/review_5/r0urDvXmvaztMO1AwRId5ZAaeYLx8ax8p3EiyRmQ.jpeg\";}', NULL, NULL, NULL, NULL, '2020-04-26 15:27:34', '2020-05-13 16:57:14'),
(6, 21, 148, 'on', 'good', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'a:2:{i:0;s:96:\"upload/product_review/product_148/user_21/review_6/Q9DXZa5nRN3zj6g9FwcABmuFvkNsWe9VHQ8j55iB.jpeg\";i:1;s:96:\"upload/product_review/product_148/user_21/review_6/rkIIjnDjThgkbJXjc0H5Q1TWBa5VFXSJNa5zVAW1.jpeg\";}', NULL, NULL, NULL, NULL, '2020-05-04 14:10:11', '2020-05-13 16:57:16'),
(9, 1, 148, 'on', 'perfect', 'good product', 'a:3:{i:0;s:95:\"upload/product_review/product_148/user_1/review_9/kLo3JMyYJRyEVIvVnO3XCepakzNU6yrIAVkBx9IH.jpeg\";i:1;s:95:\"upload/product_review/product_148/user_1/review_9/C0pvRHdfk814WkMNDJTP41XlAIcQy8ako1zkX6dr.jpeg\";i:2;s:95:\"upload/product_review/product_148/user_1/review_9/BQaNXQvzj1sgwnBlUzp6yzYWrQzqhQYJuvKKxvL5.jpeg\";}', NULL, NULL, NULL, NULL, '2020-05-04 14:23:18', '2020-05-05 13:22:37'),
(10, 33, 158, 'on', 'perfect', 'Really an amazing product thumbs up', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:16:30', '2020-07-28 01:16:30'),
(11, 33, 149, 'on', 'average', 'Just Ok', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:17:12', '2020-07-28 01:17:12'),
(12, 33, 148, 'on', 'good', 'I used that product is good but expecting more than that', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:18:09', '2020-07-28 01:18:09'),
(13, 34, 158, 'on', 'perfect', 'Tremendous', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:19:16', '2020-07-28 01:19:16'),
(14, 34, 157, 'on', 'moderate', 'Not Happy', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:19:29', '2020-07-28 01:19:29'),
(15, 34, 156, 'on', 'perfect', 'Perfect', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:19:40', '2020-07-28 01:19:40'),
(16, 34, 149, 'on', 'good', 'Good', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:19:52', '2020-07-28 01:19:52'),
(17, 35, 158, 'on', 'perfect', 'wow', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:20:46', '2020-07-28 01:20:46'),
(18, 35, 157, 'on', 'average', 'Just ok', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:20:58', '2020-07-28 01:20:58'),
(19, 35, 185, 'on', 'good', 'amazing', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:21:07', '2020-07-28 01:21:07'),
(20, 35, 148, 'on', 'perfect', 'Tremendous', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:21:21', '2020-07-28 01:21:21'),
(21, 35, 155, 'on', 'average', 'Ok', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:21:37', '2020-07-28 01:21:37'),
(22, 35, 156, 'on', 'perfect', 'wow', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:21:46', '2020-07-28 01:21:46'),
(23, 36, 158, 'on', 'perfect', 'perfect', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:22:32', '2020-07-28 01:22:32'),
(24, 36, 157, 'on', 'average', 'not soo good but just ok', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:23:24', '2020-07-28 01:23:24'),
(25, 36, 185, 'on', 'perfect', 'good', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:23:41', '2020-07-28 01:23:41'),
(26, 36, 155, 'on', 'good', 'good product', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:30:56', '2020-07-28 01:30:56'),
(27, 36, 156, 'on', 'perfect', 'good product', 'a:1:{i:0;s:97:\"upload/product_review/product_156/user_36/review_27/NpqDJvR1tI2C2zuDSiQCTFa1CL9y9ldqlenKkQL3.jpeg\";}', NULL, NULL, NULL, NULL, '2020-07-28 01:31:45', '2020-07-28 01:31:45'),
(28, 37, 158, 'on', 'good', 'good ok', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:32:29', '2020-07-28 01:32:29'),
(29, 37, 157, 'on', 'perfect', 'Amazing', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:32:54', '2020-07-28 01:32:55'),
(30, 37, 185, 'on', 'perfect', 'good tremendous', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:33:22', '2020-07-28 01:33:22'),
(31, 37, 148, 'on', 'perfect', 'wow', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:33:39', '2020-07-28 01:33:39'),
(32, 37, 155, 'on', 'perfect', 'wow', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:34:14', '2020-07-28 01:34:14'),
(33, 37, 156, 'on', 'perfect', 'wow', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:34:38', '2020-07-28 01:34:38'),
(34, 37, 156, 'on', 'perfect', 'wow', 'a:0:{}', NULL, NULL, NULL, NULL, '2020-07-28 01:35:07', '2020-07-28 01:35:07');

-- --------------------------------------------------------

--
-- Table structure for table `product_tag`
--

CREATE TABLE `product_tag` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_tag`
--

INSERT INTO `product_tag` (`id`, `product_id`, `tag_id`) VALUES
(165, 117, 5),
(166, 117, 6),
(167, 117, 9),
(168, 118, 5),
(169, 118, 6),
(170, 118, 9),
(183, 123, 5),
(184, 123, 6),
(185, 124, 6),
(186, 124, 9),
(187, 125, 6),
(188, 125, 9),
(189, 126, 6),
(190, 126, 9),
(191, 127, 6),
(192, 127, 9),
(193, 128, 5),
(194, 129, 6),
(195, 130, 6),
(196, 130, 9),
(197, 131, 6),
(198, 131, 9),
(199, 132, 6),
(218, 142, 6),
(219, 142, 7),
(220, 142, 8),
(224, 144, 6),
(225, 144, 7),
(226, 144, 8),
(227, 145, 6),
(228, 145, 7),
(229, 145, 8),
(230, 146, 6),
(231, 146, 7),
(232, 146, 8),
(233, 147, 6),
(234, 147, 7),
(235, 147, 8),
(236, 148, 6),
(237, 148, 7),
(238, 148, 8),
(239, 149, 6),
(240, 149, 7),
(241, 149, 8),
(242, 150, 5),
(243, 150, 8),
(244, 151, 5),
(245, 151, 8),
(252, 155, 5),
(253, 155, 8),
(254, 156, 5),
(255, 156, 8),
(256, 157, 5),
(257, 157, 8),
(258, 158, 5),
(260, 160, 9),
(262, 162, 6),
(297, 185, 9);

-- --------------------------------------------------------

--
-- Table structure for table `product_variations`
--

CREATE TABLE `product_variations` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `product_id` int(11) NOT NULL,
  `variation` varchar(500) DEFAULT NULL,
  `variation_meta` longtext,
  `image` longtext,
  `price` int(200) DEFAULT NULL,
  `sale_price` int(200) DEFAULT NULL,
  `stock` int(200) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `description` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_variations`
--

INSERT INTO `product_variations` (`id`, `status`, `product_id`, `variation`, `variation_meta`, `image`, `price`, `sale_price`, `stock`, `sku`, `description`, `created_at`, `updated_at`) VALUES
(5, 'on', 157, 'a:2:{i:0;s:9:\"color-red\";i:1;s:10:\"size-small\";}', 'a:2:{i:0;a:2:{s:9:\"attribute\";s:5:\"color\";s:9:\"variation\";s:3:\"red\";}i:1;a:2:{s:9:\"attribute\";s:4:\"size\";s:9:\"variation\";s:5:\"small\";}}', 'upload/product_variation/product_157/variation_5/thumbnail/1rmCnt5KWyiom9fnDtcOOJztbRhjH4LqTQJW79LC.jpeg', 120, NULL, 12, 'ds222-op97', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis.', '2020-05-12 18:31:22', '2020-06-06 08:50:20'),
(6, 'on', 157, 'a:2:{i:0;s:11:\"color-black\";i:1;s:11:\"size-medium\";}', 'a:2:{i:0;a:2:{s:9:\"attribute\";s:5:\"color\";s:9:\"variation\";s:5:\"black\";}i:1;a:2:{s:9:\"attribute\";s:4:\"size\";s:9:\"variation\";s:6:\"medium\";}}', 'upload/product_variation/product_157/variation_6/thumbnail/V4VyMZCfYJZZOLZGuXIU9ezr9YKeRKJnqK7AuUK3.jpeg', 100, NULL, 15, 'ds222-op97-A', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis.', '2020-05-12 18:54:32', '2020-05-17 14:27:12'),
(10, 'on', 157, 'a:2:{i:0;s:10:\"color-blue\";i:1;s:10:\"size-small\";}', 'a:2:{i:0;a:2:{s:9:\"attribute\";s:5:\"color\";s:9:\"variation\";s:4:\"blue\";}i:1;a:2:{s:9:\"attribute\";s:4:\"size\";s:9:\"variation\";s:5:\"small\";}}', NULL, 126, NULL, NULL, NULL, NULL, '2020-05-16 14:24:46', '2020-05-17 14:27:32');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-02-13 15:14:24', '2020-02-13 15:14:24'),
(2, 'vendor', 'web', '2020-02-13 15:14:33', '2020-02-13 15:14:33'),
(3, 'user', 'web', '2020-02-13 15:14:39', '2020-02-13 15:14:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE `shipping` (
  `id` int(11) NOT NULL,
  `country` varchar(250) NOT NULL,
  `weight` varchar(250) NOT NULL,
  `price` varchar(250) NOT NULL,
  `shipping_cost_country` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`id`, `country`, `weight`, `price`, `shipping_cost_country`) VALUES
(2, 'Andorra', 'kg', '8.5', 15),
(3, 'Pakistan', 'kg', '5.6', 10);

-- --------------------------------------------------------

--
-- Table structure for table `site_setting`
--

CREATE TABLE `site_setting` (
  `id` int(11) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_setting`
--

INSERT INTO `site_setting` (`id`, `key`, `value`) VALUES
(1, 'site_maintenance_status', 'off'),
(11, 'default_sales_tax', '3'),
(12, 'default_weight_price', '5.99'),
(14, 'default_shipping_cost', '15.99'),
(15, 'upto_review', '4'),
(16, 'default_product_stock', '10');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `alias`, `created_at`, `updated_at`) VALUES
(5, 'hello', 'hello', '2020-02-22 07:04:07', '2020-02-22 07:04:07'),
(6, 'Arts', 'arts', '2020-02-23 03:57:18', '2020-02-23 03:57:18'),
(7, 'Garments', 'garments', '2020-02-23 03:57:23', '2020-02-23 03:57:23'),
(8, 'Discovery', 'discovery', '2020-02-23 03:57:29', '2020-02-23 03:57:29'),
(9, 'Abstract', 'abstract', '2020-02-23 03:57:42', '2020-02-23 03:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `verified` int(11) DEFAULT '0',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `profile_pic` longtext COLLATE utf8_unicode_ci,
  `provider_id` longtext COLLATE utf8_unicode_ci,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_customer_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `verified`, `password`, `status`, `profile_pic`, `provider_id`, `provider`, `remember_token`, `stripe_customer_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'yasin@hztech.biz', '2020-06-04 10:22:05', 1, '$2y$10$EdCgnoxnbqMxzpJGTdCkC.EPSc5YcsAzXS6w/doZpJ.uOEr3asrA.', 1, 'upload/users/user_1/profile_pic/rFVZ3h3807vwHApf9PcbSlzmgSBIzMZN9J3gRmNM.jpeg', NULL, NULL, NULL, 'cus_HD75mM8uUQk96X', '2020-02-13 15:20:20', '2020-08-23 03:03:27'),
(21, 'Developer Tester', 'yasin.100@hotmail.com', '2020-06-04 11:11:31', 1, '$2y$10$70bnco.KAKfRVljRgEyppOA/l/S7OyUvPGECOHBruwqTmzreiBsFu', 1, NULL, NULL, NULL, NULL, 'cus_HD73Y5nUfTEjyr', '2020-04-05 05:07:52', '2020-06-04 11:11:31'),
(33, 'Ahmed', 'ahmed@gmail.com', '2020-05-05 15:04:08', 1, '$2y$10$pJP78rfp1Yu09Ma7FwChzef7U6lDsGIynikjGbywOQU.hCDGM8Dxu', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(34, 'Noreen Jack', 'noreen_jack@gmail.com', '2020-05-05 15:06:18', 1, '$2y$10$G2vRdUgf8jvok0D.1B.KcOTFoDoIy0YtIm9.dC8mHHPqloxPJPjDa', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(35, 'Watt Wilson', 'watt@gmail.com', '2020-05-05 15:07:38', 1, '$2y$10$WZefNqTGBQSm.1j602/wnum9UdJgY0oDbbAa4nf4rFkdfgOZfM7Xi', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(36, 'Tahseen', 'tahseen123@gmail.com', '2020-05-05 15:08:24', 1, '$2y$10$pg8PD8g/sCgwhpEZPpNMOOGCrGoYc.fySxFXyGps0Bfjh84SqTcPi', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(37, 'Emma Watson', 'emma@gmail.com', '2020-05-05 15:09:06', 1, '$2y$10$2qFYF7ZX/wcIvi2VDmxK.u.s83Dd0ZW0VfFJDAGKuWFBPFmBb4Lii', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(38, 'Fisk Watt', 'fisk@gmail.com', '2020-05-05 15:10:13', 1, '$2y$10$dUWbSRS0AuGGmBrW.ZyTnejxbM2tEc5C91pPGP3ixkxbYuxpd7H.q', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(39, 'Tenni', 'tenni@gmail.com', '2020-05-05 15:10:49', 1, '$2y$10$.PJibWQJGE0oLMOp7cR1o.nV1fJZ0LTutl2ws702q7THfTvhB5vFK', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(40, 'Fahim', 'fahim@gmail.com', '2020-05-05 15:11:22', 1, '$2y$10$pyrnAETI06oGh9Hi7iJIsee9BSLTn0ss2vlfqQoOHb63TyKsRnsTy', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(65, 'Developer', 'yasinmaknojia@gmail.com', '2020-08-15 05:55:16', 0, '$2y$10$Fu12ofRc0hGBLkCzPO5jw.k2FP7ouKxlXciyX83hdEP.BgOwZiyqS', 1, NULL, NULL, NULL, NULL, NULL, '2020-08-15 05:54:57', '2020-08-15 05:55:16');

-- --------------------------------------------------------

--
-- Table structure for table `users_meta`
--

CREATE TABLE `users_meta` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `meta_key` varchar(200) DEFAULT NULL,
  `meta_value` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_meta`
--

INSERT INTO `users_meta` (`id`, `user_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(63, 1, 'street_name', 'standfield rd', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(64, 1, 'street_no', 'ivor rd - 0984', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(66, 1, 'city', 'monaal', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(67, 1, 'telno', '034233455555', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(68, 1, 'shipping_address', 'sdsdasdxczxczxcz', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(69, 1, 'billing_address', 'fefrgredffvvxvvxxxz', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(71, 1, 'country', 'Argentina', '2020-03-28 07:34:05', '2020-03-28 07:34:05'),
(92, 21, 'street_name', 'qwert no222 RD', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(93, 21, 'street_no', 'sdd', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(94, 21, 'country', 'Afghanistan', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(95, 21, 'city', 'Karachi', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(96, 21, 'telno', '3131312', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(97, 22, 'street_name', 'qwert no222 RD', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(98, 22, 'street_no', 'sdd', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(99, 22, 'country', 'Albania', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(100, 22, 'city', 'Karachi', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(101, 22, 'telno', '3131312', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(102, 23, 'street_name', 'Jillian Mcdaniel', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(103, 23, 'street_no', 'Velit iste qui optio', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(104, 23, 'country', 'Algeria', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(105, 23, 'city', 'Voluptate rerum sed', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(106, 23, 'telno', '5465456454', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(107, 24, 'street_name', 'Jillian Mcdaniel', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(108, 24, 'street_no', 'Velit iste qui optio', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(109, 24, 'country', 'Åland Islands', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(110, 24, 'city', 'Voluptate rerum sed', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(111, 24, 'telno', '5465456454', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(112, 25, 'street_name', 'Ivor Bowers', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(113, 25, 'street_no', 'Tempora qui cum cons', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(114, 25, 'country', 'Afghanistan', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(115, 25, 'city', 'Rerum voluptatem Ni', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(116, 25, 'telno', '4545654', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(117, 26, 'street_name', 'Ivor Bowers', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(118, 26, 'street_no', 'Tempora qui cum cons', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(119, 26, 'country', 'Albania', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(120, 26, 'city', 'Rerum voluptatem Ni', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(121, 26, 'telno', '4545654', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(122, 27, 'street_name', 'Joelle Riggs', '2020-05-03 09:02:08', '2020-05-03 09:02:08'),
(123, 27, 'street_no', 'Tempor officia cupid', '2020-05-03 09:02:08', '2020-05-03 09:02:08'),
(124, 27, 'country', 'Åland Islands', '2020-05-03 09:02:09', '2020-05-03 09:02:09'),
(125, 27, 'city', 'Iusto esse impedit', '2020-05-03 09:02:09', '2020-05-03 09:02:09'),
(126, 27, 'telno', '4546465', '2020-05-03 09:02:09', '2020-05-03 09:02:09'),
(127, 28, 'street_name', 'Garrett Singleton', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(128, 28, 'street_no', 'Obcaecati consectetu', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(129, 28, 'country', 'Åland Islands', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(130, 28, 'city', 'Perspiciatis omnis', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(131, 28, 'telno', '545465', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(132, 29, 'street_name', 'Asher Boyle', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(133, 29, 'street_no', 'Cupidatat et eu laud', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(134, 29, 'country', 'Afghanistan', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(135, 29, 'city', 'Et hic tenetur aut q', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(136, 29, 'telno', '32113123131', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(142, 31, 'street_name', 'Kelly Ball', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(143, 31, 'street_no', 'Inventore perferendi', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(144, 31, 'country', 'Albania', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(145, 31, 'city', 'Modi irure modi saep', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(146, 31, 'telno', '5456454564', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(147, 32, 'street_name', 'Kelly Ball', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(148, 32, 'street_no', 'Inventore perferendi', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(149, 32, 'country', 'Åland Islands', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(150, 32, 'city', 'Modi irure modi saep', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(151, 32, 'telno', '5456454564', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(152, 21, 'shipping_address', 'vbnbnv', '2020-05-04 22:18:20', '2020-05-04 22:18:20'),
(153, 21, 'billing_address', 'asdasdasdasdasd', '2020-05-04 22:18:20', '2020-05-04 22:18:20'),
(154, 33, 'street_name', 'qwert no222 RD', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(155, 33, 'street_no', 'sdd', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(156, 33, 'country', 'Åland Islands', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(157, 33, 'city', 'Karachi', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(158, 33, 'telno', '5645454', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(159, 34, 'street_name', '234 Route 34', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(160, 34, 'street_no', 'Asford rd 109.', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(161, 34, 'country', 'American Samoa', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(162, 34, 'city', 'dummy', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(163, 34, 'telno', '8797897998', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(164, 35, 'street_name', 'qwert no222 RD', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(165, 35, 'street_no', 'sdad', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(166, 35, 'country', 'Bolivia', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(167, 35, 'city', 'sdasda', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(168, 35, 'telno', '87897798789789', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(169, 36, 'street_name', 'qwert no222 RD', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(170, 36, 'street_no', 'sdd', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(171, 36, 'country', 'Mongolia', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(172, 36, 'city', 'Karachi', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(173, 36, 'telno', '978789789', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(174, 37, 'street_name', 'qwert no222 RD', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(175, 37, 'street_no', 'sdd', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(176, 37, 'country', 'Åland Islands', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(177, 37, 'city', 'Karachi', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(178, 37, 'telno', '9878745546', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(179, 38, 'street_name', 'qwert no222 RD', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(180, 38, 'street_no', 'dsadasdas', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(181, 38, 'country', 'Afghanistan', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(182, 38, 'city', 'sdasda', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(183, 38, 'telno', '5456454564', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(184, 39, 'street_name', 'qwert no222 RD', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(185, 39, 'street_no', 'sdad', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(186, 39, 'country', 'Benin', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(187, 39, 'city', 'Karachi', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(188, 39, 'telno', '3131312', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(189, 40, 'street_name', 'qwert no222 RD', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(190, 40, 'street_no', 'sdd', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(191, 40, 'country', 'Albania', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(192, 40, 'city', 'sdasda', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(193, 40, 'telno', '3131312', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(194, 41, 'street_name', 'qwert no222 RD', '2020-06-04 04:28:55', '2020-06-04 04:28:55'),
(195, 41, 'street_no', 'sdd', '2020-06-04 04:28:55', '2020-06-04 04:28:55'),
(196, 41, 'country', 'Afghanistan', '2020-06-04 04:28:55', '2020-06-04 04:28:55'),
(197, 41, 'city', 'Karachi', '2020-06-04 04:28:55', '2020-06-04 04:28:55'),
(198, 41, 'telno', '6454655', '2020-06-04 04:28:55', '2020-06-04 04:28:55'),
(199, 42, 'street_name', 'qwert no222 RD', '2020-06-04 04:37:21', '2020-06-04 04:37:21'),
(200, 42, 'street_no', 'sdd', '2020-06-04 04:37:21', '2020-06-04 04:37:21'),
(201, 42, 'country', 'Åland Islands', '2020-06-04 04:37:21', '2020-06-04 04:37:21'),
(202, 42, 'city', 'Karachi', '2020-06-04 04:37:21', '2020-06-04 04:37:21'),
(203, 42, 'telno', '223232', '2020-06-04 04:37:21', '2020-06-04 04:37:21'),
(204, 43, 'street_name', 'qwert no222 RD', '2020-06-04 04:39:08', '2020-06-04 04:39:08'),
(205, 43, 'street_no', 'sdd', '2020-06-04 04:39:08', '2020-06-04 04:39:08'),
(206, 43, 'country', 'Åland Islands', '2020-06-04 04:39:08', '2020-06-04 04:39:08'),
(207, 43, 'city', 'df', '2020-06-04 04:39:08', '2020-06-04 04:39:08'),
(208, 43, 'telno', '0954535534', '2020-06-04 04:39:08', '2020-06-04 04:39:08'),
(209, 44, 'street_name', 'qwert no222 RD', '2020-06-04 04:40:52', '2020-06-04 04:40:52'),
(210, 44, 'street_no', 'sdd', '2020-06-04 04:40:52', '2020-06-04 04:40:52'),
(211, 44, 'country', 'Algeria', '2020-06-04 04:40:52', '2020-06-04 04:40:52'),
(212, 44, 'city', 'sdasda', '2020-06-04 04:40:52', '2020-06-04 04:40:52'),
(213, 44, 'telno', '0954535534', '2020-06-04 04:40:52', '2020-06-04 04:40:52'),
(214, 45, 'street_name', 'qwert no222 RD', '2020-06-04 06:31:25', '2020-06-04 06:31:25'),
(215, 45, 'street_no', 'sdd', '2020-06-04 06:31:25', '2020-06-04 06:31:25'),
(216, 45, 'country', 'Åland Islands', '2020-06-04 06:31:25', '2020-06-04 06:31:25'),
(217, 45, 'city', 'df', '2020-06-04 06:31:25', '2020-06-04 06:31:25'),
(218, 45, 'telno', '223232', '2020-06-04 06:31:25', '2020-06-04 06:31:25'),
(219, 46, 'street_name', 'qwert no222 RD', '2020-06-04 06:33:12', '2020-06-04 06:33:12'),
(220, 46, 'street_no', 'sdad', '2020-06-04 06:33:12', '2020-06-04 06:33:12'),
(221, 46, 'country', 'Algeria', '2020-06-04 06:33:12', '2020-06-04 06:33:12'),
(222, 46, 'city', 'sdas', '2020-06-04 06:33:12', '2020-06-04 06:33:12'),
(223, 46, 'telno', '3131312', '2020-06-04 06:33:12', '2020-06-04 06:33:12'),
(224, 47, 'street_name', 'qwert no222 RD', '2020-06-04 06:36:48', '2020-06-04 06:36:48'),
(225, 47, 'street_no', 'sdd', '2020-06-04 06:36:48', '2020-06-04 06:36:48'),
(226, 47, 'country', 'American Samoa', '2020-06-04 06:36:48', '2020-06-04 06:36:48'),
(227, 47, 'city', 'df', '2020-06-04 06:36:48', '2020-06-04 06:36:48'),
(228, 47, 'telno', '0954535534', '2020-06-04 06:36:48', '2020-06-04 06:36:48'),
(229, 48, 'street_name', 'qwert no222 RD', '2020-06-04 06:40:53', '2020-06-04 06:40:53'),
(230, 48, 'street_no', 'sdd', '2020-06-04 06:40:53', '2020-06-04 06:40:53'),
(231, 48, 'country', 'Åland Islands', '2020-06-04 06:40:53', '2020-06-04 06:40:53'),
(232, 48, 'city', 'df', '2020-06-04 06:40:53', '2020-06-04 06:40:53'),
(233, 48, 'telno', '0954535534', '2020-06-04 06:40:53', '2020-06-04 06:40:53'),
(234, 49, 'street_name', 'qwert no222 RD', '2020-06-04 06:42:36', '2020-06-04 06:42:36'),
(235, 49, 'street_no', 'sdad', '2020-06-04 06:42:36', '2020-06-04 06:42:36'),
(236, 49, 'country', 'American Samoa', '2020-06-04 06:42:36', '2020-06-04 06:42:36'),
(237, 49, 'city', 'sdasda', '2020-06-04 06:42:36', '2020-06-04 06:42:36'),
(238, 49, 'telno', '0954535534', '2020-06-04 06:42:36', '2020-06-04 06:42:36'),
(239, 50, 'street_name', 'qwert no222 RD', '2020-06-04 06:45:19', '2020-06-04 06:45:19'),
(240, 50, 'street_no', 'sdad', '2020-06-04 06:45:19', '2020-06-04 06:45:19'),
(241, 50, 'country', 'Algeria', '2020-06-04 06:45:19', '2020-06-04 06:45:19'),
(242, 50, 'city', 'df', '2020-06-04 06:45:19', '2020-06-04 06:45:19'),
(243, 50, 'telno', '0954535534', '2020-06-04 06:45:19', '2020-06-04 06:45:19'),
(244, 51, 'street_name', 'Karen Orr', '2020-06-04 06:46:52', '2020-06-04 06:46:52'),
(245, 51, 'street_no', 'Omnis temporibus sit', '2020-06-04 06:46:52', '2020-06-04 06:46:52'),
(246, 51, 'country', 'Åland Islands', '2020-06-04 06:46:52', '2020-06-04 06:46:52'),
(247, 51, 'city', 'Earum eu vel ducimus', '2020-06-04 06:46:52', '2020-06-04 06:46:52'),
(248, 51, 'telno', '312312312321', '2020-06-04 06:46:52', '2020-06-04 06:46:52'),
(249, 52, 'street_name', 'Hermione Campos', '2020-06-04 06:49:29', '2020-06-04 06:49:29'),
(250, 52, 'street_no', 'Cupiditate placeat', '2020-06-04 06:49:29', '2020-06-04 06:49:29'),
(251, 52, 'country', 'American Samoa', '2020-06-04 06:49:29', '2020-06-04 06:49:29'),
(252, 52, 'city', 'Nihil consequatur re', '2020-06-04 06:49:29', '2020-06-04 06:49:29'),
(253, 52, 'telno', '1133123', '2020-06-04 06:49:29', '2020-06-04 06:49:29'),
(254, 53, 'street_name', 'Barbara Crosby', '2020-06-04 07:04:45', '2020-06-04 07:04:45'),
(255, 53, 'street_no', 'Ipsa a nisi delectu', '2020-06-04 07:04:45', '2020-06-04 07:04:45'),
(256, 53, 'country', 'American Samoa', '2020-06-04 07:04:45', '2020-06-04 07:04:45'),
(257, 53, 'city', 'Rerum unde voluptate', '2020-06-04 07:04:45', '2020-06-04 07:04:45'),
(258, 53, 'telno', '56456464', '2020-06-04 07:04:45', '2020-06-04 07:04:45'),
(259, 54, 'street_name', 'Fiona Torres', '2020-06-04 07:07:41', '2020-06-04 07:07:41'),
(260, 54, 'street_no', 'Iste recusandae Neq', '2020-06-04 07:07:41', '2020-06-04 07:07:41'),
(261, 54, 'country', 'American Samoa', '2020-06-04 07:07:41', '2020-06-04 07:07:41'),
(262, 54, 'city', 'Deserunt commodo mol', '2020-06-04 07:07:41', '2020-06-04 07:07:41'),
(263, 54, 'telno', '3324234', '2020-06-04 07:07:41', '2020-06-04 07:07:41'),
(264, 55, 'street_name', 'Bethany Farrell', '2020-06-04 07:22:58', '2020-06-04 07:22:58'),
(265, 55, 'street_no', 'Sint accusamus imped', '2020-06-04 07:22:58', '2020-06-04 07:22:58'),
(266, 55, 'country', 'Albania', '2020-06-04 07:22:58', '2020-06-04 07:22:58'),
(267, 55, 'city', 'Quia quia do volupta', '2020-06-04 07:22:58', '2020-06-04 07:22:58'),
(268, 55, 'telno', '423423423432', '2020-06-04 07:22:58', '2020-06-04 07:22:58'),
(269, 56, 'street_name', 'qwert no222 RD', '2020-06-04 07:26:13', '2020-06-04 07:26:13'),
(270, 56, 'street_no', 'sdd', '2020-06-04 07:26:13', '2020-06-04 07:26:13'),
(271, 56, 'country', 'Åland Islands', '2020-06-04 07:26:13', '2020-06-04 07:26:13'),
(272, 56, 'city', 'Karachi', '2020-06-04 07:26:13', '2020-06-04 07:26:13'),
(273, 56, 'telno', '0954535534', '2020-06-04 07:26:13', '2020-06-04 07:26:13'),
(274, 57, 'street_name', 'Jolene Clarke', '2020-06-04 09:24:07', '2020-06-04 09:24:07'),
(275, 57, 'street_no', 'Culpa cillum cillum', '2020-06-04 09:24:07', '2020-06-04 09:24:07'),
(276, 57, 'country', 'Algeria', '2020-06-04 09:24:07', '2020-06-04 09:24:07'),
(277, 57, 'city', 'Iusto do eveniet se', '2020-06-04 09:24:07', '2020-06-04 09:24:07'),
(278, 57, 'telno', '4234243424', '2020-06-04 09:24:07', '2020-06-04 09:24:07'),
(279, 58, 'street_name', 'Warren Cherry', '2020-06-04 09:31:57', '2020-06-04 09:31:57'),
(280, 58, 'street_no', 'Tempore rerum ut re', '2020-06-04 09:31:57', '2020-06-04 09:31:57'),
(281, 58, 'country', 'Afghanistan', '2020-06-04 09:31:57', '2020-06-04 09:31:57'),
(282, 58, 'city', 'Ad molestiae ipsa q', '2020-06-04 09:31:57', '2020-06-04 09:31:57'),
(283, 58, 'telno', '3131312312312', '2020-06-04 09:31:57', '2020-06-04 09:31:57'),
(284, 59, 'street_name', 'Jerome Jordan', '2020-06-04 09:56:32', '2020-06-04 09:56:32'),
(285, 59, 'street_no', 'Laboriosam sit repu', '2020-06-04 09:56:32', '2020-06-04 09:56:32'),
(286, 59, 'country', 'Barbados', '2020-06-04 09:56:32', '2020-06-04 09:56:32'),
(287, 59, 'city', 'Expedita voluptatem', '2020-06-04 09:56:32', '2020-06-04 09:56:32'),
(288, 59, 'telno', '45645456', '2020-06-04 09:56:32', '2020-06-04 09:56:32'),
(289, 60, 'street_name', 'Nina Duke', '2020-06-04 10:26:41', '2020-06-04 10:26:41'),
(290, 60, 'street_no', 'Voluptate nostrud ar', '2020-06-04 10:26:41', '2020-06-04 10:26:41'),
(291, 60, 'country', 'Albania', '2020-06-04 10:26:41', '2020-06-04 10:26:41'),
(292, 60, 'city', 'Similique qui perfer', '2020-06-04 10:26:41', '2020-06-04 10:26:41'),
(293, 60, 'telno', '564655646', '2020-06-04 10:26:41', '2020-06-04 10:26:41'),
(294, 61, 'street_name', 'Rebekah Price', '2020-06-04 13:45:48', '2020-06-04 13:45:48'),
(295, 61, 'street_no', 'Consequat Quis esse', '2020-06-04 13:45:48', '2020-06-04 13:45:48'),
(296, 61, 'country', 'Belgium', '2020-06-04 13:45:48', '2020-06-04 13:45:48'),
(297, 61, 'city', 'Dolor id minus nihi', '2020-06-04 13:45:48', '2020-06-04 13:45:48'),
(298, 61, 'telno', '3242342342', '2020-06-04 13:45:48', '2020-06-04 13:45:48'),
(299, 62, 'street_name', 'qwert no222 RD', '2020-08-15 05:42:43', '2020-08-15 05:42:43'),
(300, 62, 'street_no', '125 rd royal street', '2020-08-15 05:42:43', '2020-08-15 05:42:43'),
(301, 62, 'country', 'Algeria', '2020-08-15 05:42:43', '2020-08-15 05:42:43'),
(302, 62, 'city', 'milyaan', '2020-08-15 05:42:43', '2020-08-15 05:42:43'),
(303, 62, 'telno', '0954535534', '2020-08-15 05:42:43', '2020-08-15 05:42:43'),
(304, 63, 'street_name', 'qwert no222 RD', '2020-08-15 05:46:45', '2020-08-15 05:46:45'),
(305, 63, 'street_no', '468 Royal Streen', '2020-08-15 05:46:45', '2020-08-15 05:46:45'),
(306, 63, 'country', 'American Samoa', '2020-08-15 05:46:45', '2020-08-15 05:46:45'),
(307, 63, 'city', 'miyan', '2020-08-15 05:46:45', '2020-08-15 05:46:45'),
(308, 63, 'telno', '0954535534', '2020-08-15 05:46:45', '2020-08-15 05:46:45'),
(309, 64, 'street_name', 'Jillian Mcdaniel', '2020-08-15 05:53:04', '2020-08-15 05:53:04'),
(310, 64, 'street_no', '32 Royal Street', '2020-08-15 05:53:04', '2020-08-15 05:53:04'),
(311, 64, 'country', 'American Samoa', '2020-08-15 05:53:04', '2020-08-15 05:53:04'),
(312, 64, 'city', 'london', '2020-08-15 05:53:04', '2020-08-15 05:53:04'),
(313, 64, 'telno', '0954535534', '2020-08-15 05:53:04', '2020-08-15 05:53:04'),
(314, 65, 'street_name', 'Sigourney Vazquez', '2020-08-15 05:54:57', '2020-08-15 05:54:57'),
(315, 65, 'street_no', 'Tempora consectetur', '2020-08-15 05:54:57', '2020-08-15 05:54:57'),
(316, 65, 'country', 'Åland Islands', '2020-08-15 05:54:57', '2020-08-15 05:54:57'),
(317, 65, 'city', 'Sequi voluptatem So', '2020-08-15 05:54:57', '2020-08-15 05:54:57'),
(318, 65, 'telno', '43434343', '2020-08-15 05:54:57', '2020-08-15 05:54:57');

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `verify_users`
--

INSERT INTO `verify_users` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, 1, 'YQ18USY0zHuDuu3stUrLoHKNQoCJWAYmN7XUaL9B', '2020-02-13 15:20:20', '2020-02-13 15:20:20'),
(2, 2, 'ZOoBKMmAHcCGCH4cIu9I5US7GgqmBqMd76g9plX2', '2020-02-13 15:22:25', '2020-02-13 15:22:25'),
(3, 17, 'BDomKCMmy1stRhyG7AVS263MkrXbyGq1RWbtFBUy', '2020-04-05 04:09:27', '2020-04-05 04:09:27'),
(4, 18, 'uKdLWg7vBLkFeOS1QaNDPIIOiI0dkH8UMf2mXD2L', '2020-04-05 04:56:21', '2020-04-05 04:56:21'),
(5, 19, 'YJUs9z2gK4HdOsdP9Zw865LX6D7LoSceq2HTJS0o', '2020-04-05 05:01:41', '2020-04-05 05:01:41'),
(6, 20, 'P2m7Epvt2JMTRdiysFP4iigqp3q2aDaJycGYeZKy', '2020-04-05 05:04:00', '2020-04-05 05:04:00'),
(7, 21, 'Kla1bxjm6lm6BlN4fmEzyScQb0QOOPZd5IuS1UqJ', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(8, 22, 'RhYgcAN63mJkBvTrJrEuwlSmv08oTHhPHxwRN9QB', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(9, 23, 'dKFqfT5jfRuBtb5BCiNs26MtXZLYVOxTPbSXRcKE', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(10, 24, 'ulbYfi60eGOwzN4INyVUzssMJgOE7HL4YLunbE7Q', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(11, 25, '29DRIYDc1fvFiKz1xz6YVOkvdrFsdYoanCqo0s5Y', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(12, 26, 'm74273EdhrQKQ3nwTjnPlod28nnffcLQCiMzUaMO', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(13, 27, '3Glo7RSRmsydyOWFFrUDIeoStd8jUTF9xgg3Ek7S', '2020-05-03 09:02:09', '2020-05-03 09:02:09'),
(14, 28, 'MBhxmSxx9SzRPPwenMwpp6CtCwgCM7xzix5gJpFy', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(15, 29, 'DkfLKedOhgphknJUJx2UAjZF1P2n6LmfiTWqCtZN', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(16, 32, 'f0QMrKEfioLCmPogwjZGDFR3jEHDKV0je80hKvbT', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(17, 33, 'YYWKjPTWSJQ6YzxxW9wleJG2vE9hlQsklCkESCy4', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(18, 34, 'trbQNk30ydDYEwBPJrj2VXiRIK1slZgmkozwMfa2', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(19, 35, 'c5s9Q008bKpQZADbPDXKWjiMC7XlaBQqE9xgLgm3', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(20, 36, 'YIfxTRw8jAz8t9TQuy3sSE9pDzQY3JB9sJvmIFnQ', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(21, 37, 'CZpEoyjH4OF40y9knZ5zVS05GHnWZhBiGMZStQRg', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(22, 38, 'XsQ4tOdteP8fVZqr2fbBhVsFXsopyqKMRlUugvlH', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(23, 39, 'jmd4WTYx7d5tR5i5LlnWzFyOhqmdYXKuV864Dlj6', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(24, 40, 'HMjFWufrzSWezRJLv8O4KtysKrvAmIb8Tu2Ud6jW', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(25, 41, '2nA7Sou32NXGWffQ3Fui0Ws5eosj2otwBn6PEnCB', '2020-06-04 04:28:55', '2020-06-04 04:28:55'),
(26, 42, 'WVIlBH6lVwLVab5ykdYXvLKO1hS4bWem3QvTzSBF', '2020-06-04 04:37:21', '2020-06-04 04:37:21'),
(27, 43, 'Oz20WBKXnmOjA2RGkCDXI78wxBwVLl3lX5CRsfE5', '2020-06-04 04:39:08', '2020-06-04 04:39:08'),
(28, 44, 'YzIPrVNWAlM8JCXIUfJiCnqV1CXRuAT5KffC8rXt', '2020-06-04 04:40:52', '2020-06-04 04:40:52');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `type` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_meta`
--
ALTER TABLE `category_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_setting`
--
ALTER TABLE `category_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shop_country_code_unique` (`code`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_product`
--
ALTER TABLE `coupon_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_user`
--
ALTER TABLE `coupon_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_reviews`
--
ALTER TABLE `customer_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guests`
--
ALTER TABLE `guests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquires`
--
ALTER TABLE `inquires`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orderparts`
--
ALTER TABLE `orderparts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_galleries`
--
ALTER TABLE `products_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_metas`
--
ALTER TABLE `products_metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tag`
--
ALTER TABLE `product_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_setting`
--
ALTER TABLE `site_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_meta`
--
ALTER TABLE `users_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artists`
--
ALTER TABLE `artists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `category_meta`
--
ALTER TABLE `category_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT for table `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=320;

--
-- AUTO_INCREMENT for table `category_setting`
--
ALTER TABLE `category_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_product`
--
ALTER TABLE `coupon_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_user`
--
ALTER TABLE `coupon_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `customer_reviews`
--
ALTER TABLE `customer_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `guests`
--
ALTER TABLE `guests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `inquires`
--
ALTER TABLE `inquires`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orderparts`
--
ALTER TABLE `orderparts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT for table `products_galleries`
--
ALTER TABLE `products_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `products_metas`
--
ALTER TABLE `products_metas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2712;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `product_tag`
--
ALTER TABLE `product_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=298;

--
-- AUTO_INCREMENT for table `product_variations`
--
ALTER TABLE `product_variations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shipping`
--
ALTER TABLE `shipping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `site_setting`
--
ALTER TABLE `site_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `users_meta`
--
ALTER TABLE `users_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=319;

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
