-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2020 at 10:51 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  `product_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_meta` longtext,
  `product_variation_id` int(11) DEFAULT NULL,
  `image` longtext,
  `user_meta` longtext,
  `price` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `material` varchar(200) DEFAULT NULL,
  `size` varchar(200) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `extra_detail` longtext,
  `order_type` varchar(255) DEFAULT NULL,
  `back_order_status` int(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_log`
--

INSERT INTO `order_log` (`id`, `user_id`, `product_id`, `product_name`, `product_meta`, `product_variation_id`, `image`, `user_meta`, `price`, `qty`, `material`, `size`, `extra`, `weight`, `extra_detail`, `order_type`, `back_order_status`, `created_at`, `updated_at`) VALUES
(68, '1595446377681', '160', 'product - 001fsdfsdfd', 'a:12:{s:2:\"id\";i:160;s:7:\"user_id\";i:1;s:5:\"title\";s:21:\"product - 001fsdfsdfd\";s:4:\"slug\";s:19:\"product-001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:3:\"sku\";s:7:\"sde-432\";s:5:\"image\";s:80:\"upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:23;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', '', '123', 1, 'Heavy Material', '234cm', 'Yes', 2.5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}', 'front', NULL, '2020-07-22 14:33:07', '2020-07-22 14:33:07'),
(69, '1595446737098', '160', 'product - 001fsdfsdfd', 'a:12:{s:2:\"id\";i:160;s:7:\"user_id\";i:1;s:5:\"title\";s:21:\"product - 001fsdfsdfd\";s:4:\"slug\";s:19:\"product-001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:3:\"sku\";s:7:\"sde-432\";s:5:\"image\";s:80:\"upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:23;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', '', '123', 1, 'Heavy Material', '234cm', 'Yes', 2.5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}', 'front', NULL, '2020-07-22 14:39:17', '2020-07-22 14:39:17'),
(70, '1595446868611', '160', 'product - 001fsdfsdfd', 'a:12:{s:2:\"id\";i:160;s:7:\"user_id\";i:1;s:5:\"title\";s:21:\"product - 001fsdfsdfd\";s:4:\"slug\";s:19:\"product-001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:3:\"sku\";s:7:\"sde-432\";s:5:\"image\";s:80:\"upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:23;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', '', '123', 1, 'Heavy Material', '234cm', 'Yes', 2.5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}', 'front', NULL, '2020-07-22 14:41:24', '2020-07-22 14:41:24'),
(72, '1', '185', 'dasdasdas', 'a:12:{s:2:\"id\";i:185;s:7:\"user_id\";i:21;s:5:\"title\";s:9:\"dasdasdas\";s:4:\"slug\";s:9:\"dasdasdas\";s:11:\"description\";s:17:\"<p>dasdasdasd</p>\";s:3:\"sku\";s:10:\"sde-432ddd\";s:5:\"image\";s:80:\"upload/product/product_185/feature/oM7SuuOkvP7nneIAJgSqUHD8vjnfWTNDzBNjYAV3.jpeg\";s:5:\"video\";N;s:5:\"price\";i:550;s:5:\"stock\";i:30;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, 'upload/product/product_185/feature/oM7SuuOkvP7nneIAJgSqUHD8vjnfWTNDzBNjYAV3.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"yasin@hztech.biz\",\"email_verified_at\":\"2020-06-04 15:22:05\",\"verified\":1,\"status\":1,\"profile_pic\":\"upload\\/users\\/user_1\\/profile_pic\\/rFVZ3h3807vwHApf9PcbSlzmgSBIzMZN9J3gRmNM.jpeg\",\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-08-23 08:03:27\"}', '550', 1, 'Heavy Material', '234cm', 'Yes', 5.4, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:5.4;}', 'front', NULL, '2020-08-23 13:45:59', '2020-08-23 13:45:59'),
(191, '21', '146', 'Product-10', 'a:12:{s:2:\"id\";i:146;s:7:\"user_id\";i:1;s:5:\"title\";s:10:\"Product-10\";s:4:\"slug\";s:10:\"product-10\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/product_146/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";i:43;s:6:\"status\";i:1;s:12:\"variation_id\";s:0:\"\";}', NULL, 'upload/product/product_146/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg', '{\"id\":21,\"name\":\"Developer Tester\",\"email\":\"yasin.100@hotmail.com\",\"email_verified_at\":\"2020-06-04 16:11:31\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD73Y5nUfTEjyr\",\"created_at\":\"2020-04-05 10:07:52\",\"updated_at\":\"2020-06-04 16:11:31\"}', '145', 1, 'Hello,Worlf', '240cm,566cm', 'Yes', 5.99, NULL, 'back-order', 1, '2020-09-27 15:48:18', '2020-09-27 15:48:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
