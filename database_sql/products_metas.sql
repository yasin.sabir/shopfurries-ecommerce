-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2020 at 10:07 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `products_metas`
--

CREATE TABLE `products_metas` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_meta` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products_metas`
--

INSERT INTO `products_metas` (`id`, `product_id`, `product_meta`) VALUES
(7, 14, 'a:12:{s:6:\"_token\";s:40:\"2H9xH907WK3nxDU43gx4F0Ga1oPNLCc0GOPMKDhb\";s:12:\"product_name\";s:7:\"Tshirts\";s:11:\"description\";s:12:\"<p>shirt</p>\";s:13:\"regular_price\";s:2:\"20\";s:11:\"sale_prices\";s:2:\"10\";s:11:\"product_sku\";s:8:\"sku-0001\";s:20:\"manage_stock_checked\";s:20:\"manage_stock_checked\";s:14:\"stock_quantity\";s:2:\"10\";s:9:\"attr_name\";s:4:\"size\";s:10:\"attr_value\";s:22:\"small | medium | large\";s:6:\"status\";s:3:\"yes\";s:8:\"category\";a:3:{i:0;s:2:\"o1\";i:1;s:2:\"o2\";i:2;s:2:\"o3\";}}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products_metas`
--
ALTER TABLE `products_metas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products_metas`
--
ALTER TABLE `products_metas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
