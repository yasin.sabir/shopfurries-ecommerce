-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2020 at 07:57 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  `product_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_meta` longtext DEFAULT NULL,
  `product_variation_id` int(11) DEFAULT NULL,
  `upsell_product_id` int(11) DEFAULT NULL,
  `upsell_product_meta` longtext DEFAULT NULL,
  `image` longtext DEFAULT NULL,
  `user_meta` longtext DEFAULT NULL,
  `price` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `material` varchar(200) DEFAULT NULL,
  `size` varchar(200) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `extra_detail` longtext DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_log`
--

INSERT INTO `order_log` (`id`, `user_id`, `product_id`, `product_name`, `product_meta`, `product_variation_id`, `upsell_product_id`, `upsell_product_meta`, `image`, `user_meta`, `price`, `qty`, `material`, `size`, `extra`, `weight`, `extra_detail`, `created_at`, `updated_at`) VALUES
(68, '1595446377681', '160', 'product - 001fsdfsdfd', 'a:12:{s:2:\"id\";i:160;s:7:\"user_id\";i:1;s:5:\"title\";s:21:\"product - 001fsdfsdfd\";s:4:\"slug\";s:19:\"product-001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:3:\"sku\";s:7:\"sde-432\";s:5:\"image\";s:80:\"upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:23;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, NULL, NULL, 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', '', '123', 1, 'Heavy Material', '234cm', 'Yes', 2.5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}', '2020-07-22 14:33:07', '2020-07-22 14:33:07'),
(69, '1595446737098', '160', 'product - 001fsdfsdfd', 'a:12:{s:2:\"id\";i:160;s:7:\"user_id\";i:1;s:5:\"title\";s:21:\"product - 001fsdfsdfd\";s:4:\"slug\";s:19:\"product-001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:3:\"sku\";s:7:\"sde-432\";s:5:\"image\";s:80:\"upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:23;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, NULL, NULL, 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', '', '123', 1, 'Heavy Material', '234cm', 'Yes', 2.5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}', '2020-07-22 14:39:17', '2020-07-22 14:39:17'),
(70, '1595446868611', '160', 'product - 001fsdfsdfd', 'a:12:{s:2:\"id\";i:160;s:7:\"user_id\";i:1;s:5:\"title\";s:21:\"product - 001fsdfsdfd\";s:4:\"slug\";s:19:\"product-001fsdfsdfd\";s:11:\"description\";s:18:\"<p>fsdfdfdfsdf</p>\";s:3:\"sku\";s:7:\"sde-432\";s:5:\"image\";s:80:\"upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg\";s:5:\"video\";N;s:5:\"price\";i:123;s:5:\"stock\";i:23;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, NULL, NULL, 'upload/product/product_160/feature/S4ENICMEOjoPKiNRQUXb1CbomgV1xC5S935PtcRQ.jpeg', '', '123', 1, 'Heavy Material', '234cm', 'Yes', 2.5, 'a:5:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"234cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";d:2.5;}', '2020-07-22 14:41:24', '2020-07-22 14:41:24'),
(72, '1', '128', 'Tshirt', 'a:12:{s:2:\"id\";i:128;s:7:\"user_id\";i:1;s:5:\"title\";s:6:\"Tshirt\";s:4:\"slug\";s:6:\"tshirt\";s:11:\"description\";s:358:\"<p><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif;\">What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?</span><br></p>\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:74:\"upload/product/tshirt/feature/2jQn4543Tn9MU2aSfcUTAjU3dhqbnuXk9VaVbULA.png\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";N;}', NULL, NULL, NULL, 'upload/product/tshirt/feature/2jQn4543Tn9MU2aSfcUTAjU3dhqbnuXk9VaVbULA.png', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":\"2020-02-13 20:20:20\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HCnDSW2aX1XmGM\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-05-02 15:51:30\"}', '10', 1, 'Basic', 'Basic', 'Yes', 0, 'a:5:{s:8:\"material\";s:5:\"Basic\";s:4:\"size\";s:5:\"Basic\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";s:6:\"weight\";i:0;}', '2020-08-16 10:07:00', '2020-08-16 10:07:00'),
(74, '1', '128', 'Bunch Of Tshirt & Tshirt Design', 'a:11:{s:2:\"id\";i:128;s:7:\"user_id\";i:1;s:5:\"title\";s:6:\"Tshirt\";s:4:\"slug\";s:6:\"tshirt\";s:11:\"description\";s:358:\"<p><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif;\">What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?</span><br></p>\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:74:\"upload/product/tshirt/feature/2jQn4543Tn9MU2aSfcUTAjU3dhqbnuXk9VaVbULA.png\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";N;s:6:\"status\";i:1;}', NULL, 129, 'a:11:{s:2:\"id\";i:129;s:7:\"user_id\";i:1;s:5:\"title\";s:13:\"Tshirt Design\";s:4:\"slug\";s:13:\"tshirt-design\";s:11:\"description\";s:358:\"<p><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif;\">What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?</span><br></p>\";s:3:\"sku\";N;s:5:\"image\";s:81:\"upload/product/tshirt-design/feature/ctxaeStg1rQJka6gSKyCBhX45dHUIIVtq6pCyuCy.png\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:20;s:5:\"stock\";N;s:6:\"status\";i:1;}', 'upload/product/tshirt/feature/2jQn4543Tn9MU2aSfcUTAjU3dhqbnuXk9VaVbULA.png', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":\"2020-02-13 20:20:20\",\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HCnDSW2aX1XmGM\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-05-02 15:51:30\"}', '26', 1, 'Basic', 'Basic', 'Yes', NULL, 'a:3:{s:8:\"material\";s:5:\"Basic\";s:4:\"size\";s:5:\"Basic\";s:5:\"extra\";s:3:\"Yes\";}', '2020-08-23 12:35:11', '2020-08-23 12:35:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
