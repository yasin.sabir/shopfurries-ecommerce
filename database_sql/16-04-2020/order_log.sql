-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2020 at 10:53 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  `product_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_meta` longtext DEFAULT NULL,
  `image` longtext DEFAULT NULL,
  `user_meta` longtext DEFAULT NULL,
  `price` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `material` varchar(200) DEFAULT NULL,
  `size` varchar(200) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  `extra_detail` longtext DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_log`
--

INSERT INTO `order_log` (`id`, `user_id`, `product_id`, `product_name`, `product_meta`, `image`, `user_meta`, `price`, `qty`, `material`, `size`, `extra`, `extra_detail`, `created_at`, `updated_at`) VALUES
(93, '1', '85', 'ABC', 'a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}', 'upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', '10', 1, 'Hard', '23cm', 'Yes', 'a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}', '2020-04-14 16:10:17', '2020-04-14 16:10:17'),
(94, '1', '87', 'XYZ', 'a:11:{s:2:\"id\";i:87;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"XYZ\";s:4:\"slug\";s:3:\"xyz\";s:11:\"description\";s:3:\"XYZ\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/xyz/feature/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg\";s:5:\"video\";s:4:\"null\";s:5:\"price\";i:20;s:5:\"stock\";i:1;s:6:\"status\";i:1;}', 'upload/product/xyz/feature/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', '20', 1, 'Hard', '100', 'No', 'a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:2:\"No\";s:8:\"quantity\";s:1:\"1\";}', '2020-04-14 16:10:24', '2020-04-14 16:10:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
