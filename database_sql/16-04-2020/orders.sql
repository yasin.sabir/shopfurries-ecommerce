-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2020 at 10:53 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `order_details` longtext NOT NULL,
  `product_detail` longtext NOT NULL,
  `product_extra_detail` longtext DEFAULT NULL,
  `user_meta` longtext NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subtotal` varchar(200) NOT NULL,
  `shipping` varchar(200) DEFAULT NULL,
  `discount` varchar(200) DEFAULT NULL,
  `tax` varchar(200) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `user_email`, `order_details`, `product_detail`, `product_extra_detail`, `user_meta`, `user_id`, `subtotal`, `shipping`, `discount`, `tax`, `status`, `created_at`, `updated_at`) VALUES
(18, 'XmljHy1TW7', 'ahsan.amin334@gmail.com', 'a:14:{s:6:\"_token\";s:40:\"UXGOw6blOdn15xx2ue5gyMTBsY2AADxMFJjG9o5B\";s:4:\"name\";N;s:5:\"email\";N;s:7:\"address\";N;s:6:\"mobile\";N;s:5:\"pcode\";N;s:4:\"city\";N;s:9:\"d-country\";s:14:\"Åland Islands\";s:13:\"other-address\";N;s:10:\"other-city\";N;s:11:\"other-pcode\";N;s:8:\"shipping\";N;s:7:\"payment\";s:6:\"paypal\";s:13:\"other-country\";s:14:\"Åland Islands\";}', 'a:2:{i:0;s:327:\"a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";i:1;s:300:\"a:11:{s:2:\"id\";i:87;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"XYZ\";s:4:\"slug\";s:3:\"xyz\";s:11:\"description\";s:3:\"XYZ\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/xyz/feature/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg\";s:5:\"video\";s:4:\"null\";s:5:\"price\";i:20;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";}', 'a:2:{i:0;s:99:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";i:1;s:98:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";}', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', 1, '30', '', '', '', 'Confirm', '2020-04-11 10:30:02', '2020-04-13 15:51:45'),
(19, 'RULso3uyK5', 'tahseen@gmail.com', 'a:14:{s:6:\"_token\";s:40:\"UXGOw6blOdn15xx2ue5gyMTBsY2AADxMFJjG9o5B\";s:4:\"name\";N;s:5:\"email\";N;s:7:\"address\";N;s:6:\"mobile\";N;s:5:\"pcode\";N;s:4:\"city\";N;s:9:\"d-country\";s:14:\"Åland Islands\";s:13:\"other-address\";N;s:10:\"other-city\";N;s:11:\"other-pcode\";N;s:8:\"shipping\";s:3:\"dhl\";s:7:\"payment\";s:6:\"paypal\";s:13:\"other-country\";s:14:\"Åland Islands\";}', 'a:3:{i:0;s:327:\"a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";i:1;s:327:\"a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";i:2;s:300:\"a:11:{s:2:\"id\";i:87;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"XYZ\";s:4:\"slug\";s:3:\"xyz\";s:11:\"description\";s:3:\"XYZ\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/xyz/feature/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg\";s:5:\"video\";s:4:\"null\";s:5:\"price\";i:20;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";}', 'a:3:{i:0;s:99:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";i:1;s:98:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:2:\"No\";s:8:\"quantity\";s:1:\"1\";}\";i:2;s:210:\"a:2:{i:0;a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}i:1;a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}}\";}', '{\"id\":2,\"name\":\"ahsan\",\"email\":\"ahsan.amin334@gmail.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 12:30:34\",\"updated_at\":\"2020-02-05 12:30:34\"}', 2, '60', '', '', '', 'Confirm', '2020-04-12 14:53:04', '2020-04-13 16:48:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
