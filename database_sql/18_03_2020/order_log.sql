-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2020 at 09:50 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  `product_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_meta` longtext DEFAULT NULL,
  `user_meta` longtext DEFAULT NULL,
  `price` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `material` varchar(200) DEFAULT NULL,
  `size` varchar(200) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_log`
--

INSERT INTO `order_log` (`id`, `user_id`, `product_id`, `product_name`, `product_meta`, `user_meta`, `price`, `qty`, `material`, `size`, `extra`, `created_at`, `updated_at`) VALUES
(6, '1', '83', 'Mobile', '{\"id\":83,\"user_id\":1,\"title\":\"Mobile\",\"slug\":\"mobile\",\"description\":\"<em style=\\\"-webkit-tap-highlight-color: transparent; color: rgb(255, 255, 255); font-family: &quot;Mercury SSm A&quot;, &quot;Mercury SSm B&quot;, Georgia, Times, &quot;Times New Roman&quot;, &quot;Microsoft YaHei New&quot;, &quot;Microsoft Yahei&quot;, \\u5fae\\u8f6f\\u96c5\\u9ed1, \\u5b8b\\u4f53, SimSun, STXihei, \\u534e\\u6587\\u7ec6\\u9ed1, serif; font-size: 19px; background-color: rgb(85, 98, 113);\\\">Lorem ipsum<\\/em><span style=\\\"color: rgb(255, 255, 255); font-family: &quot;Mercury SSm A&quot;, &quot;Mercury SSm B&quot;, Georgia, Times, &quot;Times New Roman&quot;, &quot;Microsoft YaHei New&quot;, &quot;Microsoft Yahei&quot;, \\u5fae\\u8f6f\\u96c5\\u9ed1, \\u5b8b\\u4f53, SimSun, STXihei, \\u534e\\u6587\\u7ec6\\u9ed1, serif; font-size: 19px; background-color: rgb(85, 98, 113);\\\">, or&nbsp;<\\/span><em style=\\\"-webkit-tap-highlight-color: transparent; color: rgb(255, 255, 255); font-family: &quot;Mercury SSm A&quot;, &quot;Mercury SSm B&quot;, Georgia, Times, &quot;Times New Roman&quot;, &quot;Microsoft YaHei New&quot;, &quot;Microsoft Yahei&quot;, \\u5fae\\u8f6f\\u96c5\\u9ed1, \\u5b8b\\u4f53, SimSun, STXihei, \\u534e\\u6587\\u7ec6\\u9ed1, serif; font-size: 19px; background-color: rgb(85, 98, 113);\\\">lipsum<\\/em><span style=\\\"color: rgb(255, 255, 255); font-family: &quot;Mercury SSm A&quot;, &quot;Mercury SSm B&quot;, Georgia, Times, &quot;Times New Roman&quot;, &quot;Microsoft YaHei New&quot;, &quot;Microsoft Yahei&quot;, \\u5fae\\u8f6f\\u96c5\\u9ed1, \\u5b8b\\u4f53, SimSun, STXihei, \\u534e\\u6587\\u7ec6\\u9ed1, serif; font-size: 19px; background-color: rgb(85, 98, 113);\\\">&nbsp;as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s&nbsp;<\\/span><em style=\\\"-webkit-tap-highlight-color: transparent; color: rgb(255, 255, 255); font-family: &quot;Mercury SSm A&quot;, &quot;Mercury SSm B&quot;, Georgia, Times, &quot;Times New Roman&quot;, &quot;Microsoft YaHei New&quot;, &quot;Microsoft Yahei&quot;, \\u5fae\\u8f6f\\u96c5\\u9ed1, \\u5b8b\\u4f53, SimSun, STXihei, \\u534e\\u6587\\u7ec6\\u9ed1, serif; font-size: 19px; background-color: rgb(85, 98, 113);\\\">De Finibus Bonorum et Malorum<\\/em><span style=\\\"color: rgb(255, 255, 255); font-family: &quot;Mercury SSm A&quot;, &quot;Mercury SSm B&quot;, Georgia, Times, &quot;Times New Roman&quot;, &quot;Microsoft YaHei New&quot;, &quot;Microsoft Yahei&quot;, \\u5fae\\u8f6f\\u96c5\\u9ed1, \\u5b8b\\u4f53, SimSun, STXihei, \\u534e\\u6587\\u7ec6\\u9ed1, serif; font-size: 19px; background-color: rgb(85, 98, 113);\\\">&nbsp;for use in a type specimen book. It usually begins with:<\\/span>\",\"sku\":\"sku-1000\",\"image\":\"upload\\/product\\/mobile\\/feature\\/9oxHakwza7IWz0UGdp7wpvjZWKb10u8lxI3N5HY4.jpeg\",\"video\":null,\"price\":20,\"stock\":1,\"status\":1,\"created_at\":\"2020-03-10 20:37:14\",\"updated_at\":\"2020-03-10 20:37:14\"}', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', '20', 1, 'Basic', '200', 'Yes', '2020-03-16 16:05:11', '2020-03-16 16:05:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
