-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2020 at 01:02 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4  */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `image` longtext,
  `status` varchar(255) DEFAULT NULL,
  `additional_details` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`, `email`, `image`, `status`, `additional_details`, `created_at`, `updated_at`) VALUES
(9, 'Jackie Wilson', 'wilson@gmail.com', 'upload/artists/9/1bxVvBuVNJeVEpzRN3F2iClyNXdpnDCn1z41XNYI.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-04-15 15:39:33', '2020-04-16 15:23:59'),
(11, 'Frenandus', 'frenan@gmail.com', 'upload/artists/11/MTxXt9mMzzBpvgqYPZlPV91l599qe4CcWwKW5hTf.jpeg', 'on', 'a:5:{s:8:\"facebook\";N;s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 14:31:18', '2020-05-05 14:31:18'),
(12, 'Ahmed', 'ahmed@gmail.com', 'upload/artists/12/9YqZVb25aBM4079T78OrFeX7EXEwoa18eu34cIsi.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 14:58:41', '2020-05-05 14:58:41'),
(13, 'Runkin', 'runkin@gmail.com', 'upload/artists/13/jikjJ4aHttfSVvWfr8nPDJeOoxvd8f8ulhXUb2vy.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 14:59:07', '2020-05-05 14:59:07'),
(14, 'Scarlett', 'scarlett@outlook.com', 'upload/artists/14/UPWPkiPfDib3XpAYoaIZr1V9Qlbz93IN207yt3M3.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";s:25:\"https://www.instagram.com\";s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:00:05', '2020-05-05 15:00:05'),
(15, 'Grayson', 'grayson@yahoo.com', 'upload/artists/15/fIC17ucLIUPAueAw5E8gBzr2r2IvGrkaQ0xQuF04.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";s:25:\"https://www.instagram.com\";s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:00:44', '2020-05-05 15:00:44'),
(16, 'Watt', 'watt@gmail.com', 'upload/artists/16/Fnb9uF39GKDcZlK1T4C6EfYRhvhwAMY0kIYttJET.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:01:21', '2020-05-05 15:01:21'),
(17, 'Scott', 'scott@gmail.com', 'upload/artists/17/x5gw0xZei1T9al5cRHVaim8MjolE6jo7nyfPcmfR.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";s:25:\"https://www.instagram.com\";s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:01:49', '2020-05-05 15:01:50'),
(18, 'Ella', 'ella0192@yahoo.com', 'upload/artists/18/W71S04wD6XGPTlf6ayNL54s1AYHpLppAgIZOnME2.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";N;s:9:\"instagram\";N;s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:02:15', '2020-05-05 15:02:15'),
(19, 'Noreen', 'nor123@gmail.com', 'upload/artists/19/iNoiBi6b6J53XDPh3SvZNsBCjBCaJbT9YCDj9iuY.jpeg', 'on', 'a:5:{s:8:\"facebook\";s:24:\"https://www.facebook.com\";s:7:\"twitter\";s:23:\"https://www.twitter.com\";s:9:\"instagram\";s:25:\"https://www.instagram.com\";s:10:\"googleplus\";N;s:5:\"other\";N;}', '2020-05-05 15:02:50', '2020-05-05 15:02:50');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Alias` varchar(255) NOT NULL,
  `Image` longtext,
  `Parent` varchar(255) DEFAULT NULL,
  `Top` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Additional_Req` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `Name`, `Alias`, `Image`, `Parent`, `Top`, `Status`, `Additional_Req`, `created_at`, `updated_at`) VALUES
(64, 'Mens', 'mens', 'upload/category/mens_64/thumbnail/uawAE6pguoTIylpcsomID6yLLjajz0Ub9JkUWGtp.jpeg', NULL, 1, 1, 0, '2020-03-21 12:50:20', '2020-03-31 15:59:31'),
(65, 'Shirts', 'shirts', 'upload/category/shirts_65/thumbnail/g8cUGSUwzFPdmqnfsLRt4PR95xX7GmhOUlmcHeey.jpeg', '64', 1, 1, 1, '2020-03-21 12:51:36', '2020-03-28 04:04:53'),
(66, 'Art', 'art', 'upload/category/art_66/thumbnail/8uOJzAHHvRuMYT3xXgHSRxgansfVFEULrUf4vCaW.jpeg', NULL, 1, 1, 1, '2020-03-28 04:16:47', '2020-03-28 08:44:03'),
(67, 'Abstract Art', 'abstract-art', 'upload/category/abstract-art_67/thumbnail/QwFtUcGXYQwHJ7t1cMFWNziWaJsycFFJnrVE7329.jpeg', NULL, 1, 1, 1, '2020-03-28 11:27:08', '2020-04-21 14:59:01'),
(68, 'Florida Shirts', 'florida-shirts', 'upload/category/florida-shirts_68/thumbnail/qRywPuYhFcq00gUG2wRLLjI9VXzRJxQMW7Xd00xR.png', NULL, 1, 1, 1, '2020-04-15 17:54:55', '2020-04-23 15:34:05'),
(70, 'Slim Shirts', 'slim-shirts', 'upload/category/slim-shirts_70/thumbnail/gfzIjvpQMZSiK6dkfemgP6c3h3TQKOnQfBpYRMCy.png', 'root', 1, 1, 0, '2020-04-21 15:27:24', '2020-04-21 15:27:24'),
(72, 'Slim Art Cups', 'slim-art-cups', 'upload/category/slim-art-cups_72/thumbnail/GSpA6DXHW7wvPhFp29sMMeP5BIZc4OdAIRYvNunz.jpeg', 'root', 1, 1, 1, '2020-05-04 13:38:50', '2020-05-04 13:38:50'),
(73, 'Armani Design', 'armani-design', 'upload/category/armani-design_73/thumbnail/Fq7DmGmfE64PeZRH1qO1xVeokza06uR8411YHYTy.jpeg', 'root', 1, 1, 1, '2020-05-07 14:02:16', '2020-05-07 14:02:17');

-- --------------------------------------------------------

--
-- Table structure for table `category_meta`
--

CREATE TABLE `category_meta` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category_meta`
--

INSERT INTO `category_meta` (`id`, `category_id`, `meta_key`, `meta_value`) VALUES
(42, 64, 'price', NULL),
(43, 64, 'tags', NULL),
(44, 64, 'poster_images', 'a:0:{}'),
(45, 64, 'actual_material_images', 'a:0:{}'),
(46, 64, 'category_reason_title', NULL),
(47, 64, 'category_reason_detail', NULL),
(48, 64, 'category_prod_features', NULL),
(49, 65, 'price', '68'),
(50, 65, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(51, 65, 'poster_images', 'a:2:{i:0;s:84:\"upload/category/shirts_65/posters-pics//SEYncNtoIVPXc6Icz5T0tr4Tn1NqCgCjZe5KPJl6.png\";i:1;s:84:\"upload/category/shirts_65/posters-pics//j2d9Vc7gbXp4tRaZHt2DwIToo43gv2xho8uuRDhU.png\";}'),
(52, 65, 'actual_material_images', 'a:3:{i:0;s:93:\"upload/category/shirts_65/actual-material-pics//Xa26lMVzr0dvkvo0YvEZUUPorns5l0ipTPcObeVM.jpeg\";i:1;s:93:\"upload/category/shirts_65/actual-material-pics//gVNjMKnFl5X8cqyOOlsSqs3IEgrCrUvYToAXJ6Kz.jpeg\";i:2;s:93:\"upload/category/shirts_65/actual-material-pics//CRd8ElNCy5zUnpF4NqXYRFQdvyklLBsosC6T7PmQ.jpeg\";}'),
(53, 65, 'category_reason_title', 'a:1:{i:0;s:8:\"Reason 1\";}'),
(54, 65, 'category_reason_detail', 'a:1:{i:0;s:11:\"dsdasdasdsd\";}'),
(55, 65, 'category_prod_features', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dapibus elit finibus, lacinia libero vel, mollis est. Phasellus vitae nulla nec quam pulvinar porttitor at quis ex. Vivamus dui lorem, elementum eget tristique et, auctor vitae lorem. Sed urna lacus, tempor a convallis ut, hendrerit at nisl. Curabitur aliquet consectetur cursus.'),
(56, 65, 'material', 'Heavy Material , Soft Quality'),
(57, 65, 'sizes', '240cm, 566c,'),
(58, 66, 'price', '56'),
(59, 66, 'tags', 'a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}'),
(60, 66, 'material', 'Heavy Material , Soft Quality'),
(61, 66, 'sizes', '240cm, 566cm'),
(62, 66, 'poster_images', 'a:2:{i:0;s:82:\"upload/category/art_66/posters-pics//Dg814bd92OZuxiqotzaDhZf65r1hEPzS4Gibeiaq.jpeg\";i:1;s:82:\"upload/category/art_66/posters-pics//APnAeuiKH20YpvwyTi4k98ZT4KcfyLjy1J0BRyUD.jpeg\";}'),
(63, 66, 'actual_material_images', 'a:3:{i:0;s:90:\"upload/category/art_66/actual-material-pics//rwRvDeYvwOzE0wD4mG2udBysdEQR9XkGWJCb35gM.jpeg\";i:1;s:90:\"upload/category/art_66/actual-material-pics//x6csmO81T9dYdlBvgY8kWQi343zYMjsmKXYzIaRQ.jpeg\";i:2;s:90:\"upload/category/art_66/actual-material-pics//ClyBnU6mVQKLfkA8lV25dZtTM53KgQb1n6WoEMIW.jpeg\";}'),
(64, 66, 'category_reason_title', 'a:1:{i:0;s:16:\"UNIQUE MATERIAL:\";}'),
(65, 66, 'category_reason_detail', 'a:1:{i:0;s:94:\"Unlike All Those Cheaply-Made Peach Skin Pillow Covers, You Can Finally Pamper Yourself With A\";}'),
(66, 66, 'category_prod_features', '<div><b>HELLO</b></div><ul><li>fdfdf</li><li>dfsdfsdf</li><li>fsdfsdfsd</li><li>dfsdfsdf</li></ul><p><b>Art Gallery</b></p><ul><li>fdfssfsdf</li><li>fsdfsdf</li><li>sdfdff</li><li>g343434</li></ul>'),
(67, 66, 'category_prod_feature_title', 'a:2:{i:0;s:51:\"HUG & CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";i:1;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(68, 66, 'category_prod_feature_detail', 'a:2:{i:0;s:220:\"Are You Ready To Meet Your Next Best Furry Friend? The Torben Goldmund\'s Art \'N\' Prints Anime Body Pillow Cases Are Here To Help You Sleep Better At Night Thanks To The Ultra-Soft Fabric And Eye-Catching Printed Designs.\";i:1;s:98:\"Are You Ready To Meet Your Next Best Furry Friend? The Torben Goldmund\'s Art \'N\' Prints Anime Body\";}'),
(69, 67, 'price', '59.0'),
(70, 67, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(71, 67, 'material', 'Heavy Material , Soft Quality'),
(72, 67, 'sizes', '240cm, 566cm'),
(73, 67, 'poster_images', 'a:1:{i:0;s:91:\"upload/category/abstract-art_67/posters-pics//nOgSD8dmGuttVIoemdeg5kYd4LzFDiU7whIp2d1D.jpeg\";}'),
(74, 67, 'actual_material_images', 'a:3:{i:0;s:99:\"upload/category/abstract-art_67/actual-material-pics//elr8jzjdXo2Zk2ecf3Nc3aLuVBTDnVEHPPeHTi1g.jpeg\";i:1;s:99:\"upload/category/abstract-art_67/actual-material-pics//OUyeH0U0DSaXC6FnGPPBDQS0rVXxiEJCn3PkYZvj.jpeg\";i:2;s:98:\"upload/category/abstract-art_67/actual-material-pics//Ff1TYqWXvH5fvtQan8yhSvY6kazxqfdhxBj3TaVi.png\";}'),
(75, 67, 'category_reason_title', 'a:3:{i:0;s:8:\"Reason 1\";i:1;s:8:\"Reason 2\";i:2;s:8:\"Reason 3\";}'),
(76, 67, 'category_reason_detail', 'a:3:{i:0;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";i:1;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";i:2;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";}'),
(77, 67, 'category_prod_feature_title', 'a:2:{i:0;s:51:\"HUG & CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";i:1;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(78, 67, 'category_prod_feature_detail', 'a:2:{i:0;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";i:1;s:154:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra et nunc eget bibendum. Praesent eget dui eget risus maximus viverra ac vitae urna.\";}'),
(79, 64, 'material', NULL),
(80, 64, 'sizes', NULL),
(81, 64, 'category_prod_feature_title', NULL),
(82, 64, 'category_prod_feature_detail', NULL),
(83, 68, 'artist', '10'),
(84, 68, 'price', '68'),
(85, 68, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(86, 68, 'material', 'Heavy Material,Soft Quality'),
(87, 68, 'sizes', '240cm, 566c'),
(88, 68, 'poster_images', 'a:1:{i:0;s:93:\"upload/category/florida-shirts_68/posters-pics//NJmp4Bm27fWoXCKaALKrztM1df90YrEgjFlPfuxC.jpeg\";}'),
(89, 68, 'actual_material_images', 'a:1:{i:0;s:100:\"upload/category/florida-shirts_68/actual-material-pics//D14CAqm402Bf9RqhwlPzbFjPDHwdSjnrHPhp0hRO.png\";}'),
(90, 68, 'category_reason_title', 'a:1:{i:0;s:8:\"Reason 1\";}'),
(91, 68, 'category_reason_detail', 'a:1:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(92, 68, 'category_prod_feature_title', 'a:1:{i:0;s:11:\"SNUGGLIEST2\";}'),
(93, 68, 'category_prod_feature_detail', 'a:1:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(94, 67, 'artist', '9'),
(95, 69, 'artist', '6'),
(96, 69, 'price', '68'),
(97, 69, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"6\";}'),
(98, 69, 'material', 'Heavy-Material,Soft-Quality'),
(99, 69, 'sizes', '240cm,566c'),
(100, 69, 'poster_images', 'a:1:{i:0;s:88:\"upload/category/slim-card_69/posters-pics//MdkOrz7uqB2K0nVhZGdToWedMITcrmu7BaQnR1MF.jpeg\";}'),
(101, 69, 'actual_material_images', 'a:1:{i:0;s:96:\"upload/category/slim-card_69/actual-material-pics//9dat5yxfLFf2BticSOeE7Pf8IQ8dZrvpQuWFwNcM.jpeg\";}'),
(102, 69, 'category_reason_title', 'a:1:{i:0;s:16:\"UNIQUE MATERIAL:\";}'),
(103, 69, 'category_reason_detail', 'a:1:{i:0;s:17:\"dsasdasdasdasdasd\";}'),
(104, 69, 'category_prod_feature_title', 'a:1:{i:0;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(105, 69, 'category_prod_feature_detail', 'a:1:{i:0;s:13:\"dsdadsdasdasd\";}'),
(106, 70, 'artist', NULL),
(107, 70, 'price', NULL),
(108, 70, 'tags', NULL),
(109, 70, 'material', NULL),
(110, 70, 'sizes', NULL),
(111, 70, 'poster_images', 'a:0:{}'),
(112, 70, 'actual_material_images', 'a:0:{}'),
(113, 70, 'category_reason_title', NULL),
(114, 70, 'category_reason_detail', NULL),
(115, 70, 'category_prod_feature_title', NULL),
(116, 70, 'category_prod_feature_detail', NULL),
(128, 72, 'artist', '10'),
(129, 72, 'price', '145'),
(130, 72, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(131, 72, 'material', 'Heavy Material , Soft Quality'),
(132, 72, 'sizes', '240cm, 566cm'),
(133, 72, 'poster_images', 'a:2:{i:0;s:92:\"upload/category/slim-art-cups_72/posters-pics//Hjc8x8S7kTiB7exA5aLTmVY20oifMddJ1sIdFyit.jpeg\";i:1;s:91:\"upload/category/slim-art-cups_72/posters-pics//mtqHOgbWmZiC6MQMMyxJnjcjTZOlMEbkrhQKUDrl.png\";}'),
(134, 72, 'actual_material_images', 'a:1:{i:0;s:100:\"upload/category/slim-art-cups_72/actual-material-pics//7KwZCPRLU7bgbaPSTKDIBGaDjzcpVKQxBrRAICm2.jpeg\";}'),
(135, 72, 'category_reason_title', 'a:2:{i:0;s:8:\"Reason 1\";i:1;s:8:\"Reason 2\";}'),
(136, 72, 'category_reason_detail', 'a:2:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";i:1;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(137, 72, 'category_prod_feature_title', 'a:2:{i:0;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";i:1;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(138, 72, 'category_prod_feature_detail', 'a:2:{i:0;s:27:\"Lorem ipsum dolor sit amet,\";i:1;s:27:\"Lorem ipsum dolor sit amet,\";}'),
(139, 73, 'artist', '13'),
(140, 73, 'price', '126'),
(141, 73, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}'),
(142, 73, 'material', 'Heavy_Material,Soft_Quality'),
(143, 73, 'sizes', '240cm,566cm'),
(144, 73, 'poster_images', 'a:1:{i:0;s:92:\"upload/category/armani-design_73/posters-pics//sO4IlExAY9bOf1wq9kl0azUMokqir9cZURIjmaTt.jpeg\";}'),
(145, 73, 'actual_material_images', 'a:2:{i:0;s:100:\"upload/category/armani-design_73/actual-material-pics//tXJiNReXocLgjREGq2cqDAqFhkQyl1mFVByFwmYO.jpeg\";i:1;s:100:\"upload/category/armani-design_73/actual-material-pics//2vVQb8Zrg1Hp2jYALZcGX4hS8njYWuywNb7hZuWE.jpeg\";}'),
(146, 73, 'category_reason_title', 'a:1:{i:0;s:8:\"Reason 1\";}'),
(147, 73, 'category_reason_detail', 'a:1:{i:0;s:128:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus.\";}'),
(148, 73, 'category_prod_feature_title', 'a:1:{i:0;s:29:\"SNUGGLIEST DAKIMAKURA PILLOW!\";}'),
(149, 73, 'category_prod_feature_detail', 'a:1:{i:0;s:128:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus.\";}');

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `product_id`, `category_id`) VALUES
(221, 117, 67),
(239, 118, 64),
(249, 123, 67),
(250, 124, 68),
(251, 125, 68),
(252, 126, 68),
(253, 127, 68),
(254, 128, 67),
(255, 129, 68),
(256, 130, 68),
(257, 131, 68),
(258, 132, 68),
(268, 142, 72),
(270, 144, 72),
(271, 145, 72),
(272, 146, 72),
(273, 147, 72),
(274, 148, 72),
(275, 149, 72),
(276, 150, 73),
(277, 151, 73),
(281, 155, 73),
(282, 156, 73),
(283, 157, 73);

-- --------------------------------------------------------

--
-- Table structure for table `category_setting`
--

CREATE TABLE `category_setting` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `reason_titles` longtext,
  `reason_descriptions` longtext,
  `feature_titles` longtext,
  `feature_descriptions` longtext,
  `posters` longtext,
  `actual_material_pics` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_setting`
--

INSERT INTO `category_setting` (`id`, `status`, `reason_titles`, `reason_descriptions`, `feature_titles`, `feature_descriptions`, `posters`, `actual_material_pics`, `created_at`, `updated_at`) VALUES
(7, 'on', 'a:1:{i:0;s:8:\"Reason 1\";}', 'a:1:{i:0;s:56:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\";}', 'a:1:{i:0;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}', 'a:1:{i:0;s:45:\"CUDDLE WITH THE SNUGGLIEST DAKIMAKURA PILLOW!\";}', 'a:1:{i:0;s:83:\"upload/category_setting/posters-pics//LPJ92fy7vucGFwChPAsq3QZwYz7O1R09MNy3VzVn.jpeg\";}', 'a:1:{i:0;s:91:\"upload/category_setting/actual-material-pics//WOy5l7DzODy7VcfYZudTNLt1PToJjYKIhWMupyC6.jpeg\";}', '2020-04-11 15:51:39', '2020-04-21 16:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'AL', 'Albania'),
(2, 'DZ', 'Algeria'),
(3, 'DS', 'American Samoa'),
(4, 'AD', 'Andorra'),
(5, 'AO', 'Angola'),
(6, 'AI', 'Anguilla'),
(7, 'AQ', 'Antarctica'),
(8, 'AG', 'Antigua and Barbuda'),
(9, 'AR', 'Argentina'),
(10, 'AM', 'Armenia'),
(11, 'AW', 'Aruba'),
(12, 'AU', 'Australia'),
(13, 'AT', 'Austria'),
(14, 'AZ', 'Azerbaijan'),
(15, 'BS', 'Bahamas'),
(16, 'BH', 'Bahrain'),
(17, 'BD', 'Bangladesh'),
(18, 'BB', 'Barbados'),
(19, 'BY', 'Belarus'),
(20, 'BE', 'Belgium'),
(21, 'BZ', 'Belize'),
(22, 'BJ', 'Benin'),
(23, 'BM', 'Bermuda'),
(24, 'BT', 'Bhutan'),
(25, 'BO', 'Bolivia'),
(26, 'BA', 'Bosnia and Herzegovina'),
(27, 'BW', 'Botswana'),
(28, 'BV', 'Bouvet Island'),
(29, 'BR', 'Brazil'),
(30, 'IO', 'British Indian Ocean Territory'),
(31, 'BN', 'Brunei Darussalam'),
(32, 'BG', 'Bulgaria'),
(33, 'BF', 'Burkina Faso'),
(34, 'BI', 'Burundi'),
(35, 'KH', 'Cambodia'),
(36, 'CM', 'Cameroon'),
(37, 'CA', 'Canada'),
(38, 'CV', 'Cape Verde'),
(39, 'KY', 'Cayman Islands'),
(40, 'CF', 'Central African Republic'),
(41, 'TD', 'Chad'),
(42, 'CL', 'Chile'),
(43, 'CN', 'China'),
(44, 'CX', 'Christmas Island'),
(45, 'CC', 'Cocos (Keeling) Islands'),
(46, 'CO', 'Colombia'),
(47, 'KM', 'Comoros'),
(48, 'CG', 'Congo'),
(49, 'CK', 'Cook Islands'),
(50, 'CR', 'Costa Rica'),
(51, 'HR', 'Croatia (Hrvatska)'),
(52, 'CU', 'Cuba'),
(53, 'CY', 'Cyprus'),
(54, 'CZ', 'Czech Republic'),
(55, 'DK', 'Denmark'),
(56, 'DJ', 'Djibouti'),
(57, 'DM', 'Dominica'),
(58, 'DO', 'Dominican Republic'),
(59, 'TP', 'East Timor'),
(60, 'EC', 'Ecuador'),
(61, 'EG', 'Egypt'),
(62, 'SV', 'El Salvador'),
(63, 'GQ', 'Equatorial Guinea'),
(64, 'ER', 'Eritrea'),
(65, 'EE', 'Estonia'),
(66, 'ET', 'Ethiopia'),
(67, 'FK', 'Falkland Islands (Malvinas)'),
(68, 'FO', 'Faroe Islands'),
(69, 'FJ', 'Fiji'),
(70, 'FI', 'Finland'),
(71, 'FR', 'France'),
(72, 'FX', 'France, Metropolitan'),
(73, 'GF', 'French Guiana'),
(74, 'PF', 'French Polynesia'),
(75, 'TF', 'French Southern Territories'),
(76, 'GA', 'Gabon'),
(77, 'GM', 'Gambia'),
(78, 'GE', 'Georgia'),
(79, 'DE', 'Germany'),
(80, 'GH', 'Ghana'),
(81, 'GI', 'Gibraltar'),
(82, 'GK', 'Guernsey'),
(83, 'GR', 'Greece'),
(84, 'GL', 'Greenland'),
(85, 'GD', 'Grenada'),
(86, 'GP', 'Guadeloupe'),
(87, 'GU', 'Guam'),
(88, 'GT', 'Guatemala'),
(89, 'GN', 'Guinea'),
(90, 'GW', 'Guinea-Bissau'),
(91, 'GY', 'Guyana'),
(92, 'HT', 'Haiti'),
(93, 'HM', 'Heard and Mc Donald Islands'),
(94, 'HN', 'Honduras'),
(95, 'HK', 'Hong Kong'),
(96, 'HU', 'Hungary'),
(97, 'IS', 'Iceland'),
(98, 'IN', 'India'),
(99, 'IM', 'Isle of Man'),
(100, 'ID', 'Indonesia'),
(101, 'IR', 'Iran (Islamic Republic of)'),
(102, 'IQ', 'Iraq'),
(103, 'IE', 'Ireland'),
(104, 'IL', 'Israel'),
(105, 'IT', 'Italy'),
(106, 'CI', 'Ivory Coast'),
(107, 'JE', 'Jersey'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'JO', 'Jordan'),
(111, 'KZ', 'Kazakhstan'),
(112, 'KE', 'Kenya'),
(113, 'KI', 'Kiribati'),
(114, 'KP', 'Korea,Democratic People\'s Republic of'),
(115, 'KR', 'Korea, Republic of'),
(116, 'XK', 'Kosovo'),
(117, 'KW', 'Kuwait'),
(118, 'KG', 'Kyrgyzstan'),
(119, 'LA', 'Lao People\'s Democratic Republic'),
(120, 'LV', 'Latvia'),
(121, 'LB', 'Lebanon'),
(122, 'LS', 'Lesotho'),
(123, 'LR', 'Liberia'),
(124, 'LY', 'Libyan Arab Jamahiriya'),
(125, 'LI', 'Liechtenstein'),
(126, 'LT', 'Lithuania'),
(127, 'LU', 'Luxembourg'),
(128, 'MO', 'Macau'),
(129, 'MK', 'Macedonia'),
(130, 'MG', 'Madagascar'),
(131, 'MW', 'Malawi'),
(132, 'MY', 'Malaysia'),
(133, 'MV', 'Maldives'),
(134, 'ML', 'Mali'),
(135, 'MT', 'Malta'),
(136, 'MH', 'Marshall Islands'),
(137, 'MQ', 'Martinique'),
(138, 'MR', 'Mauritania'),
(139, 'MU', 'Mauritius'),
(140, 'TY', 'Mayotte'),
(141, 'MX', 'Mexico'),
(142, 'FM', 'Micronesia, Federated States of'),
(143, 'MD', 'Moldova, Republic of'),
(144, 'MC', 'Monaco'),
(145, 'MN', 'Mongolia'),
(146, 'ME', 'Montenegro'),
(147, 'MS', 'Montserrat'),
(148, 'MA', 'Morocco'),
(149, 'MZ', 'Mozambique'),
(150, 'MM', 'Myanmar'),
(151, 'NA', 'Namibia'),
(152, 'NR', 'Nauru'),
(153, 'NP', 'Nepal'),
(154, 'NL', 'Netherlands'),
(155, 'AN', 'Netherlands Antilles'),
(156, 'NC', 'New Caledonia'),
(157, 'NZ', 'New Zealand'),
(158, 'NI', 'Nicaragua'),
(159, 'NE', 'Niger'),
(160, 'NG', 'Nigeria'),
(161, 'NU', 'Niue'),
(162, 'NF', 'Norfolk Island'),
(163, 'MP', 'Northern Mariana Islands'),
(164, 'NO', 'Norway'),
(165, 'OM', 'Oman'),
(166, 'PK', 'Pakistan'),
(167, 'PW', 'Palau'),
(168, 'PS', 'Palestine'),
(169, 'PA', 'Panama'),
(170, 'PG', 'Papua New Guinea'),
(171, 'PY', 'Paraguay'),
(172, 'PE', 'Peru'),
(173, 'PH', 'Philippines'),
(174, 'PN', 'Pitcairn'),
(175, 'PL', 'Poland'),
(176, 'PT', 'Portugal'),
(177, 'PR', 'Puerto Rico'),
(178, 'QA', 'Qatar'),
(179, 'RE', 'Reunion'),
(180, 'RO', 'Romania'),
(181, 'RU', 'Russian Federation'),
(182, 'RW', 'Rwanda'),
(183, 'KN', 'Saint Kitts and Nevis'),
(184, 'LC', 'Saint Lucia'),
(185, 'VC', 'Saint Vincent and the Grenadines'),
(186, 'WS', 'Samoa'),
(187, 'SM', 'San Marino'),
(188, 'ST', 'Sao Tome and Principe'),
(189, 'SA', 'Saudi Arabia'),
(190, 'SN', 'Senegal'),
(191, 'RS', 'Serbia'),
(192, 'SC', 'Seychelles'),
(193, 'SL', 'Sierra Leone'),
(194, 'SG', 'Singapore'),
(195, 'SK', 'Slovakia'),
(196, 'SI', 'Slovenia'),
(197, 'SB', 'Solomon Islands'),
(198, 'SO', 'Somalia'),
(199, 'ZA', 'South Africa'),
(200, 'GS', 'South Georgia South Sandwich Islands'),
(201, 'SS', 'South Sudan'),
(202, 'ES', 'Spain'),
(203, 'LK', 'Sri Lanka'),
(204, 'SH', 'St. Helena'),
(205, 'PM', 'St. Pierre and Miquelon'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen Islands'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TG', 'Togo'),
(218, 'TK', 'Tokelau'),
(219, 'TO', 'Tonga'),
(220, 'TT', 'Trinidad and Tobago'),
(221, 'TN', 'Tunisia'),
(222, 'TR', 'Turkey'),
(223, 'TM', 'Turkmenistan'),
(224, 'TC', 'Turks and Caicos Islands'),
(225, 'TV', 'Tuvalu'),
(226, 'UG', 'Uganda'),
(227, 'UA', 'Ukraine'),
(228, 'AE', 'United Arab Emirates'),
(229, 'GB', 'United Kingdom'),
(230, 'US', 'United States'),
(231, 'UM', 'United States minor outlying islands'),
(232, 'UY', 'Uruguay'),
(233, 'UZ', 'Uzbekistan'),
(234, 'VU', 'Vanuatu'),
(235, 'VA', 'Vatican City State'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Vietnam'),
(238, 'VG', 'Virgin Islands (British)'),
(239, 'VI', 'Virgin Islands (U.S.)'),
(240, 'WF', 'Wallis and Futuna Islands'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'ZR', 'Zaire'),
(244, 'ZM', 'Zambia'),
(245, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(250) CHARACTER SET latin1 NOT NULL,
  `type` varchar(250) CHARACTER SET latin1 NOT NULL,
  `max_uses` int(200) NOT NULL,
  `max_uses_user` int(200) DEFAULT NULL,
  `discount` int(200) DEFAULT NULL,
  `status` int(200) NOT NULL,
  `freedelivery` int(200) DEFAULT NULL,
  `exclude_category` longtext CHARACTER SET latin1,
  `exclude_product` longtext CHARACTER SET latin1,
  `start_date` date DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `startandexpire` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `type`, `max_uses`, `max_uses_user`, `discount`, `status`, `freedelivery`, `exclude_category`, `exclude_product`, `start_date`, `expire_date`, `startandexpire`, `created_at`, `updated_at`) VALUES
(9, 'M^h$d-Ao^vy>2w9&Hj&$', 'fixed', 100, NULL, 50, 1, NULL, NULL, NULL, '2020-01-05', '1970-01-01', '05/01/2020 - 06/25/2020', '2020-05-03 08:14:10', '2020-05-03 08:15:31'),
(10, 'g>ymHfDMl8yEpfw^6P3F', 'fixed', 100, NULL, 50, 1, NULL, NULL, NULL, '2020-02-05', '1970-01-01', '05/02/2020 - 06/20/2020', '2020-05-03 08:16:21', '2020-05-03 08:16:21'),
(11, 'ueFG043@Ae$Ies%5#NAH', 'fixed', 99, NULL, 50, 1, NULL, NULL, NULL, '2020-05-06', '2020-06-24', '05/06/2020 - 06/24/2020', '2020-05-03 08:34:24', '2020-05-03 08:34:50');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_product`
--

CREATE TABLE `coupon_product` (
  `id` int(11) NOT NULL,
  `product_id` int(20) NOT NULL,
  `coupon_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_user`
--

CREATE TABLE `coupon_user` (
  `id` int(11) NOT NULL,
  `user_id` int(20) NOT NULL,
  `coupon_id` int(20) NOT NULL,
  `status` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coupon_user`
--

INSERT INTO `coupon_user` (`id`, `user_id`, `coupon_id`, `status`) VALUES
(9, 1, 2, 1),
(10, 1, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `code` varchar(200) NOT NULL,
  `symbol` varchar(200) NOT NULL,
  `exchange_rate` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reviews`
--

CREATE TABLE `customer_reviews` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` longtext,
  `rating` varchar(255) DEFAULT NULL,
  `detail` longtext,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_reviews`
--

INSERT INTO `customer_reviews` (`id`, `name`, `image`, `rating`, `detail`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Jack', 'upload/customer-reviews/1/5KCaWKDoe7faxomPao2p4FkzQ5BQ9ivBYcPUf4mx.jpeg', '5', '<p>I\'m going to do more business with them very soon. FANTASTCI SERVICE! AN<br></p>', 'off', '2020-04-30 18:01:03', '2020-05-01 10:51:08'),
(2, 'Wilson', 'upload/customer-reviews/2/NjGlUXDLOOU7jyfwYzF7hNEPXJAsun7ivMqOy7PG.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:17:47', '2020-05-01 10:17:47'),
(3, 'Anna', 'upload/customer-reviews/3/Yplxo8yjtBbwvoupvJUNUQboITIoOGI7HnGMfZK9.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:19:58', '2020-05-01 10:19:58'),
(4, 'Jennifer', 'upload/customer-reviews/4/Eu3CvSd4bnzeeyN8StPK0ONsxx3lvsvhIAv0GBbM.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:20:50', '2020-05-01 10:20:50'),
(5, 'Lilly', 'upload/customer-reviews/5/YSfnJfeYaLNlIuSgIfD8t1S0HakksP16kHXSRDju.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:21:09', '2020-05-01 10:21:09'),
(6, 'Charlotte', 'upload/customer-reviews/6/kxIrYNnE5OsWEATrhKazcd2iOoykWFRl3gox3CLm.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:21:26', '2020-05-01 10:21:26'),
(7, 'Peter Wock', 'upload/customer-reviews/7/I2Zar4JhQqZ9TI5OVkxAjemvmWuDTaHtlAjQkgK3.jpeg', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:21:53', '2020-05-01 10:21:53'),
(8, 'Fillis', 'upload/customer-reviews/8/bape8yUHbhPN0CckWv5fmdyx1ZxICDmDRWXuo1L4.png', '5', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:23:08', '2020-05-01 10:32:35'),
(9, 'Richard', 'upload/customer-reviews/9/HfACUJG7BudGGCluQlejlxP7TtEqmookuZ1RyIxr.jpeg', '4', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt posuere bibendum. Donec rutrum faucibus quam vel auctor.</span><br></p>', 'on', '2020-05-01 10:51:47', '2020-05-04 11:09:21');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'on',
  `address` longtext,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `image` longtext,
  `timing` varchar(255) DEFAULT NULL,
  `details` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `status`, `address`, `state`, `city`, `country`, `image`, `timing`, `details`, `created_at`, `updated_at`) VALUES
(1, 'Art Center Manatee', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'West Virgina', 'Huntington', 'United States', 'upload/events/1/wIbxTZTiNDuWciRqZXJ4jdMq6Abr9P6jNfTTfa8j.jpeg', '05/30/2020 12:00 PM', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel felis dictum, efficitur nulla ac, luctus ipsum. Ut placerat sapien nec orci dictum, sit amet accumsan nisi tristique.', '2020-04-30 14:13:43', '2020-04-30 15:09:05'),
(2, 'Otto Hubbard', 'on', 'Excepteur porro ab r', 'Bavaria', 'Munich', 'Germany', 'upload/events/2/o5w96Iw6wXDBau7DoPbopPh0KE3FCLisgrFg90HO.jpeg', '05/25/2020 1:11 PM', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</span>', '2020-04-30 15:12:44', '2020-04-30 16:55:51'),
(3, 'Art Center Venice', 'on', 'Excepteur porro ab r', NULL, 'Venice', 'Italy', 'upload/events/3/EeE5Ha2pQsgCeIerWmInreFUzotG2ouRxwV10M6s.jpeg', '06/18/2020 1:16 PM', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</span><br></p>', '2020-04-30 15:16:40', '2020-04-30 16:55:53'),
(4, 'Exhibition Of Art Culture', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 'London', 'United Kingdom', 'upload/events/4/zPXg38Ps3ue9QyWlWJeohNsvsaYAsQuB2AoYgDh9.jpeg', '06/27/2020 1:17 PM', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel felis dictum, efficitur nulla ac.</span><br></p>', '2020-04-30 15:19:11', '2020-05-04 10:56:09'),
(6, 'Art Center Dubai', 'on', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 'Dubai', 'United Arab Emirates', 'upload/events/6/56Mzr9c3ryMxP0phVf2wMur1X1krW4QfGpE4EXwQ.jpeg', '05/30/2020 10:57 PM', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br></p>', '2020-05-04 10:57:28', '2020-05-04 10:57:28');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` longtext,
  `answer` longtext,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(1, '<div>What Is Torben Goldmund’s Art ‘N’</div>', '<div style=\"text-align: justify;\"><font face=\"Open Sans, Arial, sans-serif\"><span style=\"font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris molestie ornare laoreet. Nam sed mattis ante. Nam ullamcorper molestie pellentesque. Pellentesque finibus metus a nulla volutpat, eget bibendum ligula aliquet. Duis consectetur ut justo sit amet dignissim. Sed laoreet mauris sed nulla finibus hendrerit. Proin semper vestibulum maximus. Curabitur maximus nunc metus, sed bibendum dui lacinia sit amet. Quisque porta, elit eget facilisis faucibus, nulla nulla ultrices odio, ut elementum metus nisl et massa. Donec pharetra, arcu dignissim varius bibendum, elit turpis porta odio, vitae dapibus augue nulla nec nibh. Praesent hendrerit lacus neque, sed pellentesque enim ullamcorper vitae. Interdum et malesuada fames ac ante ipsum primis in faucibus.</span></font></div>', 1, '2020-02-27 16:28:17', '2020-03-25 16:45:49'),
(13, 'dsadsdasasdas', 'dasdasdasdasdas', 1, '2020-04-20 14:38:33', '2020-04-20 14:38:33'),
(14, 'Pellentesque habitant morbi tristique senectus et netus et malesuada.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.', 0, '2020-05-04 11:15:00', '2020-05-04 11:15:22');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_02_05_102643_create_permission_tables', 1),
(4, '2020_02_09_203836_create_verify_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(3, 'App\\User', 2),
(3, 'App\\User', 3),
(3, 'App\\User', 4),
(3, 'App\\User', 5),
(3, 'App\\User', 6),
(3, 'App\\User', 7),
(3, 'App\\User', 8),
(3, 'App\\User', 9),
(3, 'App\\User', 10),
(3, 'App\\User', 11),
(3, 'App\\User', 12),
(3, 'App\\User', 13),
(3, 'App\\User', 14),
(3, 'App\\User', 15),
(3, 'App\\User', 16),
(3, 'App\\User', 17),
(3, 'App\\User', 18),
(3, 'App\\User', 19),
(3, 'App\\User', 20),
(3, 'App\\User', 21),
(3, 'App\\User', 22),
(3, 'App\\User', 23),
(3, 'App\\User', 24),
(3, 'App\\User', 25),
(3, 'App\\User', 26),
(3, 'App\\User', 27),
(3, 'App\\User', 28),
(3, 'App\\User', 29),
(3, 'App\\User', 31),
(3, 'App\\User', 32),
(3, 'App\\User', 33),
(3, 'App\\User', 34),
(3, 'App\\User', 35),
(3, 'App\\User', 36),
(3, 'App\\User', 37),
(3, 'App\\User', 38),
(3, 'App\\User', 39),
(3, 'App\\User', 40);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `order_details` longtext NOT NULL,
  `product_detail` longtext NOT NULL,
  `product_extra_detail` longtext,
  `user_meta` longtext NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subtotal` varchar(200) NOT NULL,
  `shipping` varchar(200) DEFAULT NULL,
  `discount` varchar(200) DEFAULT NULL,
  `tax` varchar(200) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `user_email`, `order_details`, `product_detail`, `product_extra_detail`, `user_meta`, `user_id`, `subtotal`, `shipping`, `discount`, `tax`, `status`, `created_at`, `updated_at`) VALUES
(18, 'XmljHy1TW7', 'ahsan.amin334@gmail.com', 'a:14:{s:6:\"_token\";s:40:\"UXGOw6blOdn15xx2ue5gyMTBsY2AADxMFJjG9o5B\";s:4:\"name\";N;s:5:\"email\";N;s:7:\"address\";N;s:6:\"mobile\";N;s:5:\"pcode\";N;s:4:\"city\";N;s:9:\"d-country\";s:14:\"Åland Islands\";s:13:\"other-address\";N;s:10:\"other-city\";N;s:11:\"other-pcode\";N;s:8:\"shipping\";N;s:7:\"payment\";s:6:\"paypal\";s:13:\"other-country\";s:14:\"Åland Islands\";}', 'a:2:{i:0;s:327:\"a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";i:1;s:300:\"a:11:{s:2:\"id\";i:87;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"XYZ\";s:4:\"slug\";s:3:\"xyz\";s:11:\"description\";s:3:\"XYZ\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/xyz/feature/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg\";s:5:\"video\";s:4:\"null\";s:5:\"price\";i:20;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";}', 'a:2:{i:0;s:99:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";i:1;s:98:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";}', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 10:37:29\",\"updated_at\":\"2020-02-05 10:37:29\"}', 1, '30', '', '', '', 'Confirm', '2020-04-11 10:30:02', '2020-04-13 15:51:45'),
(19, 'RULso3uyK5', 'tahseen@gmail.com', 'a:14:{s:6:\"_token\";s:40:\"UXGOw6blOdn15xx2ue5gyMTBsY2AADxMFJjG9o5B\";s:4:\"name\";N;s:5:\"email\";N;s:7:\"address\";N;s:6:\"mobile\";N;s:5:\"pcode\";N;s:4:\"city\";N;s:9:\"d-country\";s:14:\"Åland Islands\";s:13:\"other-address\";N;s:10:\"other-city\";N;s:11:\"other-pcode\";N;s:8:\"shipping\";s:3:\"dhl\";s:7:\"payment\";s:6:\"paypal\";s:13:\"other-country\";s:14:\"Åland Islands\";}', 'a:3:{i:0;s:327:\"a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";i:1;s:327:\"a:11:{s:2:\"id\";i:85;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"ABC\";s:4:\"slug\";s:3:\"abc\";s:11:\"description\";s:4:\"Test\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/abc/feature/uIrK5ssttkw9FlZjnnTKiKGxTlRdtKicvVeKNr2A.jpeg\";s:5:\"video\";s:29:\"https://gitlab.com/ahsangadit\";s:5:\"price\";i:10;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";i:2;s:300:\"a:11:{s:2:\"id\";i:87;s:7:\"user_id\";i:1;s:5:\"title\";s:3:\"XYZ\";s:4:\"slug\";s:3:\"xyz\";s:11:\"description\";s:3:\"XYZ\";s:3:\"sku\";s:7:\"sku-001\";s:5:\"image\";s:72:\"upload/product/xyz/feature/EUqVx9ogjygJucCbEUNmDKPnannKcFhAZO8Txf9F.jpeg\";s:5:\"video\";s:4:\"null\";s:5:\"price\";i:20;s:5:\"stock\";i:1;s:6:\"status\";i:1;}\";}', 'a:3:{i:0;s:99:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";i:1;s:98:\"a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:4:\"23cm\";s:5:\"extra\";s:2:\"No\";s:8:\"quantity\";s:1:\"1\";}\";i:2;s:210:\"a:2:{i:0;a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}i:1;a:4:{s:8:\"material\";s:4:\"Hard\";s:4:\"size\";s:3:\"100\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}}\";}', '{\"id\":2,\"name\":\"ahsan\",\"email\":\"ahsan.amin334@gmail.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"created_at\":\"2020-02-05 12:30:34\",\"updated_at\":\"2020-02-05 12:30:34\"}', 2, '60', '', '', '', 'Confirm', '2020-04-12 14:53:04', '2020-04-13 16:48:17'),
(20, 'VPTOf0R4Zr', 'vyfuta@mailinator.net', 'a:17:{s:6:\"_token\";s:40:\"pjX3U7Ae5Ge9ibT7DnRXAvIdMi43mmX4KcYfON5T\";s:4:\"name\";s:12:\"Merrill Pate\";s:5:\"email\";s:21:\"vyfuta@mailinator.net\";s:7:\"address\";s:20:\"Laborum eos aute ali\";s:6:\"mobile\";s:19:\"Aut a corrupti et a\";s:5:\"pcode\";s:20:\"Debitis corporis mag\";s:4:\"city\";s:17:\"Aut delectus fuga\";s:5:\"state\";s:20:\"Voluptates ut corpor\";s:9:\"d-country\";s:7:\"Albania\";s:13:\"other-address\";s:20:\"Debitis porro incidu\";s:11:\"other-pcode\";s:19:\"Voluptatem Maxime o\";s:10:\"other-city\";s:19:\"Qui culpa quia beat\";s:13:\"other-country\";s:7:\"Albania\";s:8:\"shipping\";N;s:7:\"payment\";s:6:\"stripe\";s:8:\"subtotal\";s:5:\"68.00\";s:11:\"stripeToken\";s:28:\"tok_1GehDVAedkiOiGZjCCP9vYWX\";}', 'a:1:{i:0;s:293:\"a:11:{s:2:\"id\";i:131;s:7:\"user_id\";i:1;s:5:\"title\";s:10:\"Product-21\";s:4:\"slug\";s:10:\"product-21\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:79:\"upload/product/product-21/feature/kdoWT5HZY8OEZnzuJz3aDaRajZBm2nEDCTOEckSf.jpeg\";s:5:\"video\";N;s:5:\"price\";i:68;s:5:\"stock\";N;s:6:\"status\";i:1;}\";}', 'a:1:{i:0;s:111:\"a:4:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";}', '{\"id\":21,\"name\":\"Yasin Sabir\",\"email\":\"yasin.100@hotmail.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD73Y5nUfTEjyr\",\"created_at\":\"2020-04-05 10:07:52\",\"updated_at\":\"2020-05-03 12:21:41\"}', 21, '68.00', '', '', '', 'Confirm', '2020-05-03 07:46:56', '2020-05-03 07:46:56'),
(21, '8nUDwBnuyi', 'yasin.100@hotmail.com', 'a:17:{s:6:\"_token\";s:40:\"cRAoB1wwfOnkYSaN80t5JNYQnzSUa0rYfJEP2fS6\";s:4:\"name\";s:16:\"Developer Tester\";s:5:\"email\";s:21:\"yasin.100@hotmail.com\";s:7:\"address\";s:20:\"Non cum natus quibus\";s:6:\"mobile\";s:9:\"546454556\";s:5:\"pcode\";s:6:\"225988\";s:4:\"city\";s:5:\"Dubai\";s:5:\"state\";N;s:9:\"d-country\";s:20:\"United Arab Emirates\";s:13:\"other-address\";s:20:\"Non cum natus quibus\";s:11:\"other-pcode\";s:6:\"225988\";s:10:\"other-city\";s:5:\"Dubai\";s:13:\"other-country\";s:20:\"United Arab Emirates\";s:8:\"shipping\";s:3:\"dhl\";s:7:\"payment\";s:6:\"stripe\";s:8:\"subtotal\";s:6:\"145.00\";s:11:\"stripeToken\";s:28:\"tok_1GfCl3AedkiOiGZjkAlTXp5V\";}', 'a:1:{i:0;s:289:\"a:11:{s:2:\"id\";i:143;s:7:\"user_id\";i:1;s:5:\"title\";s:9:\"Product-5\";s:4:\"slug\";s:9:\"product-5\";s:11:\"description\";N;s:3:\"sku\";N;s:5:\"image\";s:78:\"upload/product/product-5/feature/0pGpK7PbTXeprdJTCj1V19YTcmcWpTBKvg1oDktj.jpeg\";s:5:\"video\";N;s:5:\"price\";i:145;s:5:\"stock\";N;s:6:\"status\";i:1;}\";}', 'a:1:{i:0;s:111:\"a:4:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}\";}', '{\"id\":21,\"name\":\"Developer Tester\",\"email\":\"yasin.100@hotmail.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD73Y5nUfTEjyr\",\"created_at\":\"2020-04-05 10:07:52\",\"updated_at\":\"2020-05-04 22:16:49\"}', 21, '145.00', '', '', '', 'Confirm', '2020-05-04 17:27:30', '2020-05-04 17:27:30');

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

CREATE TABLE `order_history` (
  `id` int(11) NOT NULL,
  `content` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  `product_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_meta` longtext,
  `product_variation_id` int(11) DEFAULT NULL,
  `image` longtext,
  `user_meta` longtext,
  `price` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `material` varchar(200) DEFAULT NULL,
  `size` varchar(200) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  `extra_detail` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_log`
--

INSERT INTO `order_log` (`id`, `user_id`, `product_id`, `product_name`, `product_meta`, `product_variation_id`, `image`, `user_meta`, `price`, `qty`, `material`, `size`, `extra`, `extra_detail`, `created_at`, `updated_at`) VALUES
(14, '1', '157', 'cat-widget1', 'a:12:{s:2:\"id\";i:157;s:7:\"user_id\";i:1;s:5:\"title\";s:11:\"cat-widget1\";s:4:\"slug\";s:11:\"cat-widget1\";s:11:\"description\";s:604:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis. Fusce at pretium urna, quis pharetra elit. Curabitur sollicitudin pellentesque ex sit amet imperdiet. Aenean erat ante, accumsan venenatis tincidunt a, tincidunt sed leo. Morbi scelerisque tempus neque vel pretium. Vivamus ac erat cursus, commodo ligula et, eleifend nulla. Nulla sit amet volutpat turpis. Suspendisse pretium maximus sem, at fermentum velit viverra sodales.</span>\";s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/cat-widget1/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";s:1:\"5\";}', 5, 'upload/product/cat-widget1/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-05-03 12:23:20\"}', '120', 1, 'Heavy Material', '240cm', 'Yes', 'a:4:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}', '2020-05-17 15:40:28', '2020-05-17 17:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `order_meta`
--

CREATE TABLE `order_meta` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `name`) VALUES
(1, 'processing'),
(2, 'hold');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('qwerty@gmail.com', '$2y$10$6B0tWaLL4uyCoVnzzcBSLuVuV1jvB.NM33OYyhFPue9L5eDPeCxpW', '2020-03-01 12:34:10'),
('yasin@hztech.biz', '$2y$10$0jcNi56MV.cn/WQaeR2UrePLpNs/O4.rNBFZUHpfvfdY0k5cYbj4C', '2020-05-03 08:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productimgtemp`
--

CREATE TABLE `productimgtemp` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` longtext,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4,
  `sku` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `image` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `video` longtext COLLATE utf8_unicode_ci,
  `price` int(200) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `title`, `slug`, `description`, `sku`, `image`, `video`, `price`, `stock`, `status`, `created_at`, `updated_at`) VALUES
(117, 1, 'Product-17fdf', 'product-17', NULL, NULL, 'upload/product/product-17/feature/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg', 'null', 59, NULL, 1, '2020-03-28 11:28:36', '2020-03-28 12:08:30'),
(118, 1, 'Product-18', 'product-18', NULL, NULL, 'upload/product/product-18/feature/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg', 'https://gitlab.com/yasin.sabir/erp/-/blob/master/app/Http/Controllers/AccountController.php', 59, NULL, 1, '2020-03-28 11:28:36', '2020-04-18 15:14:24'),
(123, 1, 'qwerty098 7', 'qwerty098', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc semper, nisl id ultrices venenatis, diam odio posuere nulla, non consectetur arcu quam vel lorem. Nunc ornare diam in ligula porta, nec fermentum arcu vestibulum. Aliquam risus nisi, fringilla nec rutrum nec, placerat a elit.098 7</span><br></p>', NULL, 'upload/product/qwerty098/feature/4VPxihSjcrQnnyAgWhebxD4KcPxqP9VlijOzUdlp.jpeg', 'https://gitlab.com/yasin.sabir/erp/-/tree/master/app/Http/Controllers', 777, NULL, 1, '2020-04-04 07:07:36', '2020-04-04 07:08:50'),
(126, 1, 'Galactic Atoms', 'galactic-atoms', NULL, NULL, 'upload/product/galactic-atoms/feature/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png', NULL, 68, NULL, 1, '2020-04-22 15:28:14', '2020-04-22 15:28:14'),
(127, 1, 'Space 1', 'space-1', NULL, NULL, 'upload/product/space-1/feature/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg', NULL, 68, NULL, 1, '2020-04-22 15:28:14', '2020-04-22 15:28:14'),
(128, 1, 'Furries Special Product 001', 'furries-special-product-001', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>', 'sde-432133', 'upload/product/furries-special-product-001/feature/MHnah64XPz60yOFOKDfitas5E5d5al5GLcQpyJuk.jpeg', NULL, 123, NULL, 1, '2020-05-03 05:53:54', '2020-05-03 05:56:06'),
(129, 1, 'Dummy Product 001', 'dummy-product-001', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>', NULL, 'upload/product/dummy-product-001/feature/7caLagd95OpXYOnah5zZNJyhgv2AFBQU3wUjTAnL.jpeg', NULL, 550, 1, 1, '2020-05-03 05:57:31', '2020-05-03 05:57:31'),
(130, 1, 'Product-20', 'product-20', NULL, NULL, 'upload/product/product-20/feature/ffTcVtIHULgLm6wH3WPnLquDZHukVxOkLDQNb191.jpeg', NULL, 68, NULL, 1, '2020-05-03 05:58:21', '2020-05-03 05:58:21'),
(131, 1, 'Product-21', 'product-21', NULL, NULL, 'upload/product/product-21/feature/kdoWT5HZY8OEZnzuJz3aDaRajZBm2nEDCTOEckSf.jpeg', NULL, 68, NULL, 1, '2020-05-03 05:58:22', '2020-05-03 05:58:22'),
(132, 21, 'Artist Product -001', 'artist-product-001', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>', NULL, 'upload/product/artist-product-001/feature/ameE08LT2Pp8xq3c8bN2IpRgmFWHJP96KcfggdxK.jpeg', NULL, 857, 1, 1, '2020-05-03 06:41:10', '2020-05-03 06:41:10'),
(142, 1, 'Product-4', 'product-4', NULL, NULL, 'upload/product/product-4/feature/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg', NULL, 145, NULL, 1, '2020-05-04 13:39:53', '2020-05-04 13:39:53'),
(144, 1, 'Product-6', 'product-6', NULL, NULL, 'upload/product/product-6/feature/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg', NULL, 145, NULL, 1, '2020-05-04 13:39:53', '2020-05-04 13:39:53'),
(145, 1, 'Product-7', 'product-7', NULL, NULL, 'upload/product/product-7/feature/HfFVpbWs1f3nNUnYZtMcZyQmKWnJdXbKDn2AhysU.jpeg', NULL, 145, NULL, 1, '2020-05-04 13:39:53', '2020-05-04 13:39:53'),
(146, 1, 'Product-10', 'product-10', NULL, NULL, 'upload/product/product-10/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg', NULL, 145, NULL, 1, '2020-05-04 13:39:53', '2020-05-04 13:39:53'),
(147, 1, 'Product-11', 'product-11', NULL, NULL, 'upload/product/product-11/feature/xmmmtdp5wPLVyj5VRdphuGqtXyr2sWHjcIxjOWIJ.jpeg', NULL, 145, NULL, 1, '2020-05-04 13:39:53', '2020-05-04 13:39:53'),
(148, 1, 'Product-13', 'product-13', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>', NULL, 'upload/product/product-13/feature/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg', 'https://www.youtube.com/embed/-hSOuK7qBgM', 145, NULL, 1, '2020-05-04 13:39:54', '2020-05-05 14:26:10'),
(149, 1, 'Product-19', 'product-19', NULL, NULL, 'upload/product/product-19/feature/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg', NULL, 145, NULL, 1, '2020-05-04 13:39:54', '2020-05-04 13:39:54'),
(155, 1, 'post-img1', 'post-img1', NULL, NULL, 'upload/product/post-img1/feature/nEfHsDtaInZ4ArHS2FtOvPjJZCRhoMk3fYQHKjC0.jpeg', NULL, 126, NULL, 1, '2020-05-07 15:03:10', '2020-05-07 15:03:10'),
(156, 1, 'post-img2', 'post-img2', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>', NULL, 'upload/product/post-img2/feature/SJJ05fADEODOCttITm0UsVZXYQZBDut4PBBrxiFX.jpeg', NULL, 126, NULL, 1, '2020-05-07 15:03:11', '2020-05-07 15:07:36'),
(157, 1, 'cat-widget1', 'cat-widget1', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis. Fusce at pretium urna, quis pharetra elit. Curabitur sollicitudin pellentesque ex sit amet imperdiet. Aenean erat ante, accumsan venenatis tincidunt a, tincidunt sed leo. Morbi scelerisque tempus neque vel pretium. Vivamus ac erat cursus, commodo ligula et, eleifend nulla. Nulla sit amet volutpat turpis. Suspendisse pretium maximus sem, at fermentum velit viverra sodales.</span>', NULL, 'upload/product/cat-widget1/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg', NULL, 126, NULL, 1, '2020-05-07 15:14:23', '2020-05-14 15:45:08');

-- --------------------------------------------------------

--
-- Table structure for table `products_galleries`
--

CREATE TABLE `products_galleries` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image` longtext CHARACTER SET utf8mb4
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_galleries`
--

INSERT INTO `products_galleries` (`id`, `product_id`, `image`) VALUES
(52, 25, 'upload/product/product-001/gallery/zeuRYcbhmcxgZ4ce9HS17eIIXNkE7wehb4FmHBpk.png'),
(53, 25, 'upload/product/product-001/gallery/d1QFTuWCYy6Y2kU1DhBfzDcf6FsemI5qxhvyxgz9.png'),
(54, 26, 'upload/product/dfsdfsfadfdf/gallery/Tq30RDu9wm6NvWBsH8kFTRPa2zeFbvrCWchhhoiX.png'),
(55, 26, 'upload/product/dfsdfsfadfdf/gallery/YSHiyo10491nVgFQ3OMXgeA3fTNNjLwUFwSuaiDq.png'),
(107, 128, 'upload/product/furries-special-product-001/gallery/W3GH7zy6u4D7AOF37drbIvviB4M1Mm04mdEF2Jfq.jpeg'),
(108, 128, 'upload/product/furries-special-product-001/gallery/W3GH7zy6u4D7AOF37drbIvviB4M1Mm04mdEF2Jfq.jpeg'),
(109, 128, 'upload/product/furries-special-product-001/gallery/W3GH7zy6u4D7AOF37drbIvviB4M1Mm04mdEF2Jfq.jpeg'),
(110, 129, 'upload/product/dummy-product-001/gallery/eRdSjsWnaYoC4KVG67n8yzoNgvfi2GXAxdN3Ks9S.jpeg'),
(111, 129, 'upload/product/dummy-product-001/gallery/5HOGLRMppscEk3XmylWqNqqAHrioYOcQK1uS7rnM.jpeg'),
(112, 132, 'upload/product/artist-product-001/gallery/agqE1ipVY6CRqNcLWX5u59s25Bqi6zVzo7wYDQ0M.jpeg'),
(113, 132, 'upload/product/artist-product-001/gallery/MfdyXWAmI7otVlITFGghoR7SXeiEPDRi7xjmUMvi.jpeg'),
(114, 157, 'upload/product/cat-widget1/gallery/NB7WvEITZ6WsuJVyu2U89qVc9O2r6SpAoJxxR0E5.jpeg'),
(121, 157, 'upload/product/cat-widget1/gallery/IgAxora2WO8OCrv4bSnE5ev4wFlQPcDoZnvXEMF7.jpeg'),
(123, 157, 'upload/product/cat-widget1/gallery/yBZPIpNTyIfEGf7kGIMtgkb3rC004klYO5gsIJWP.jpeg'),
(124, 157, 'upload/product/cat-widget1/gallery/cmhXG0RA7PWKJrj6mxHDfIw5kNy9bxX4hPrnaHvs.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `products_metas`
--

CREATE TABLE `products_metas` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_meta` longtext CHARACTER SET utf8mb4 NOT NULL,
  `product_meta_value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_metas`
--

INSERT INTO `products_metas` (`id`, `product_id`, `product_meta`, `product_meta_value`) VALUES
(1335, 117, 'slug', 'product-17fdf'),
(1336, 117, 'price', '59.0'),
(1337, 117, 'sale_price', NULL),
(1338, 117, 'sale_start_date', 'Invalid date '),
(1339, 117, 'sale_end_date', ' Invalid date'),
(1340, 117, 'sku', NULL),
(1341, 117, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1342, 117, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(1343, 117, 'featured_image', 'upload/product/product-17/feature/aTZKFg81mC0K8am4j9z0ksdjcStm8tYZ2yt9tasF.jpeg'),
(1344, 117, 'stock', NULL),
(1345, 117, 'material', 'Heavy Material , Soft Quality'),
(1346, 117, 'sizes', '240cm, 566cm'),
(1347, 117, 'product_features_description', ''),
(1348, 117, 'stock_manage_chk', NULL),
(1349, 117, 'stock_threshold', NULL),
(1350, 117, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"gqzvj02I6jzFf8wWdZp7jeSP7iEupbiWrOC4SoSh\";s:12:\"product_name\";s:13:\"Product-17fdf\";s:11:\"description\";N;s:28:\"product_features_description\";N;s:13:\"regular_price\";s:4:\"59.0\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:29:\"Heavy Material , Soft Quality\";s:13:\"product_sizes\";s:12:\"240cm, 566cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}}'),
(1351, 117, 'product_video_description', NULL),
(1352, 117, 'product_video', NULL),
(1353, 118, 'slug', 'product-18'),
(1354, 118, 'price', '59.0'),
(1355, 118, 'sale_price', '0'),
(1356, 118, 'sale_start_date', 'Invalid date '),
(1357, 118, 'sale_end_date', ' Invalid date'),
(1358, 118, 'sku', NULL),
(1359, 118, 'tags', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}'),
(1360, 118, 'categories', 'a:1:{i:0;s:2:\"64\";}'),
(1361, 118, 'featured_image', 'upload/product/product-18/feature/w1T6Q7J49zpNwXmAMlh7z89LEezK9cUpCiByu1gl.jpeg'),
(1362, 118, 'stock', NULL),
(1363, 118, 'material', 'heelo,world,gummiesworld,goodbuy'),
(1364, 118, 'sizes', '240cm,566cm'),
(1365, 118, 'product_features_description', ''),
(1366, 118, 'stock_manage_chk', NULL),
(1367, 118, 'stock_threshold', NULL),
(1368, 118, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"8to1Gwy0RSjhqtKPUDkbowhguLtF8sl2hp01ykfC\";s:12:\"product_name\";s:10:\"Product-18\";s:11:\"description\";N;s:28:\"product_features_description\";N;s:13:\"regular_price\";s:4:\"59.0\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:32:\"heelo,world,gummiesworld,goodbuy\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"64\";}s:4:\"tags\";a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"9\";}}'),
(1369, 118, 'product_video_description', NULL),
(1370, 118, 'product_video', 'https://gitlab.com/yasin.sabir/erp/-/blob/master/app/Http/Controllers/AccountController.php'),
(1451, 123, 'slug', 'qwerty098-7'),
(1452, 123, 'price', '777'),
(1453, 123, 'sale_price', '600'),
(1454, 123, 'sale_start_date', '04/04/2020 '),
(1455, 123, 'sale_end_date', ' 04/04/2020'),
(1456, 123, 'sku', NULL),
(1457, 123, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"6\";}'),
(1458, 123, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(1459, 123, 'featured_image', 'upload/product/qwerty098/feature/4VPxihSjcrQnnyAgWhebxD4KcPxqP9VlijOzUdlp.jpeg'),
(1460, 123, 'stock', NULL),
(1461, 123, 'material', 'Hello,Worlf'),
(1462, 123, 'sizes', '234cm,350cm'),
(1463, 123, 'stock_manage_chk', NULL),
(1464, 123, 'stock_threshold', NULL),
(1465, 123, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"jIXrg7gYJxWtTcvcrUi4HERESS5K0lHHqBbilSRi\";s:12:\"product_name\";s:11:\"qwerty098 7\";s:11:\"description\";s:693:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc semper, nisl id ultrices venenatis, diam odio posuere nulla, non consectetur arcu quam vel lorem. Nunc ornare diam in ligula porta, nec fermentum arcu vestibulum. Aliquam risus nisi, fringilla nec rutrum nec, placerat a elit.098 7</span><br></p>\";s:28:\"product_features_description\";N;s:13:\"regular_price\";s:3:\"777\";s:11:\"sale_prices\";s:3:\"600\";s:16:\"sales_price_schd\";s:23:\"04/04/2020 - 04/04/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";s:373:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula.098</span><br></p>\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:2:{i:0;s:1:\"5\";i:1;s:1:\"6\";}}'),
(1466, 123, 'product_video_description', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi massa, maximus vel erat vitae, blandit gravida diam. Vestibulum vitae ultricies sem. Aenean feugiat porttitor nisl, ut dapibus felis ullamcorper sit amet. Cras eu cursus ligula.098</span><br></p>'),
(1467, 123, 'product_video', 'https://gitlab.com/yasin.sabir/erp/-/tree/master/app/Http/Controllers'),
(1508, 126, 'slug', 'galactic-atoms'),
(1509, 126, 'price', '68'),
(1510, 126, 'sale_price', ''),
(1511, 126, 'sale_start_date', ''),
(1512, 126, 'sale_end_date', ''),
(1513, 126, 'sku', ''),
(1514, 126, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1515, 126, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1516, 126, 'featured_image', 'upload/product/galactic-atoms/feature/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png'),
(1517, 126, 'stock', ''),
(1518, 126, 'material', 'Heavy Material,Soft Quality'),
(1519, 126, 'sizes', '240cm, 566c'),
(1520, 126, 'product_features_description', ''),
(1521, 126, 'stock_manage_chk', ''),
(1522, 126, 'stock_threshold', ''),
(1523, 126, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"s0JtAbtSjXp6N42hL2LNJAw375OKLUL5fqnViji4\";s:12:\"product_name\";a:2:{i:0;s:14:\"Galactic Atoms\";i:1;s:7:\"Space 1\";}s:10:\"image_name\";a:2:{i:0;s:73:\"upload/product_temperory_img/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png\";i:1;s:74:\"upload/product_temperory_img/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg\";}s:12:\"image_row_id\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"4\";}s:10:\"categories\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:5:\"price\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";i:1;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";}s:8:\"material\";a:2:{i:0;s:27:\"Heavy Material,Soft Quality\";i:1;s:27:\"Heavy Material,Soft Quality\";}s:5:\"sizes\";a:2:{i:0;s:11:\"240cm, 566c\";i:1;s:11:\"240cm, 566c\";}s:11:\"temp_img_id\";s:1:\"4\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1524, 126, 'product_video_description', ''),
(1525, 126, 'product_video', ''),
(1526, 127, 'slug', 'space-1'),
(1527, 127, 'price', '68'),
(1528, 127, 'sale_price', ''),
(1529, 127, 'sale_start_date', ''),
(1530, 127, 'sale_end_date', ''),
(1531, 127, 'sku', ''),
(1532, 127, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1533, 127, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1534, 127, 'featured_image', 'upload/product/space-1/feature/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg'),
(1535, 127, 'stock', ''),
(1536, 127, 'material', 'Heavy Material,Soft Quality'),
(1537, 127, 'sizes', '240cm, 566c'),
(1538, 127, 'product_features_description', ''),
(1539, 127, 'stock_manage_chk', ''),
(1540, 127, 'stock_threshold', ''),
(1541, 127, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"s0JtAbtSjXp6N42hL2LNJAw375OKLUL5fqnViji4\";s:12:\"product_name\";a:2:{i:0;s:14:\"Galactic Atoms\";i:1;s:7:\"Space 1\";}s:10:\"image_name\";a:2:{i:0;s:73:\"upload/product_temperory_img/uldMxU90eM2iArpGrYFKoJFhIvroURkbVGfGonxu.png\";i:1;s:74:\"upload/product_temperory_img/zbGBCmPIkei3MwlT3BSI8RPb2IlbaYGEKIAWoIX5.jpeg\";}s:12:\"image_row_id\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"4\";}s:10:\"categories\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:5:\"price\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";i:1;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";}s:8:\"material\";a:2:{i:0;s:27:\"Heavy Material,Soft Quality\";i:1;s:27:\"Heavy Material,Soft Quality\";}s:5:\"sizes\";a:2:{i:0;s:11:\"240cm, 566c\";i:1;s:11:\"240cm, 566c\";}s:11:\"temp_img_id\";s:1:\"4\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1542, 127, 'product_video_description', ''),
(1543, 127, 'product_video', ''),
(1544, 128, 'slug', 'furries-special-product-001'),
(1545, 128, 'price', '123'),
(1546, 128, 'sale_price', '56'),
(1547, 128, 'sale_start_date', '05/03/2020 '),
(1548, 128, 'sale_end_date', ' 05/03/2020'),
(1549, 128, 'sku', 'sde-432133'),
(1550, 128, 'tags', 'a:1:{i:0;s:1:\"5\";}'),
(1551, 128, 'categories', 'a:1:{i:0;s:2:\"67\";}'),
(1552, 128, 'featured_image', 'upload/product/furries-special-product-001/feature/MHnah64XPz60yOFOKDfitas5E5d5al5GLcQpyJuk.jpeg'),
(1553, 128, 'stock', NULL),
(1554, 128, 'material', 'qwerty,helloworld,good,bye,gummies'),
(1555, 128, 'sizes', '234cm,350cm'),
(1556, 128, 'stock_manage_chk', NULL),
(1557, 128, 'stock_threshold', NULL),
(1558, 128, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"T1AkkXXDjZu6IwZjVyO3VG9rXGtFAUvrrrs6Y4nA\";s:12:\"product_name\";s:27:\"Furries Special Product 001\";s:11:\"description\";s:455:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>\";s:28:\"product_features_description\";N;s:13:\"regular_price\";s:3:\"123\";s:11:\"sale_prices\";s:2:\"56\";s:16:\"sales_price_schd\";s:23:\"05/03/2020 - 05/03/2020\";s:11:\"product_sku\";s:10:\"sde-432133\";s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:34:\"qwerty,helloworld,good,bye,gummies\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"67\";}s:4:\"tags\";a:1:{i:0;s:1:\"5\";}}'),
(1559, 128, 'product_video_description', NULL),
(1560, 128, 'product_video', NULL),
(1561, 129, 'slug', 'dummy-product-001'),
(1562, 129, 'price', '550'),
(1563, 129, 'sale_price', '230'),
(1564, 129, 'sale_start_date', '05/03/2020 '),
(1565, 129, 'sale_end_date', ' 05/03/2020'),
(1566, 129, 'sku', NULL),
(1567, 129, 'tags', 'a:1:{i:0;s:1:\"6\";}'),
(1568, 129, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1569, 129, 'featured_image', 'upload/product/dummy-product-001/feature/7caLagd95OpXYOnah5zZNJyhgv2AFBQU3wUjTAnL.jpeg'),
(1570, 129, 'stock', NULL),
(1571, 129, 'material', 'Hello,Worlf'),
(1572, 129, 'sizes', '234cm,350cm'),
(1573, 129, 'stock_manage_chk', NULL),
(1574, 129, 'stock_threshold', ''),
(1575, 129, 'serialize_data', 'a:15:{s:6:\"_token\";s:40:\"T1AkkXXDjZu6IwZjVyO3VG9rXGtFAUvrrrs6Y4nA\";s:12:\"product_name\";s:17:\"Dummy Product 001\";s:11:\"description\";s:455:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>\";s:13:\"regular_price\";s:3:\"550\";s:11:\"sale_prices\";s:3:\"230\";s:16:\"sales_price_schd\";s:23:\"05/03/2020 - 05/03/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"68\";}s:4:\"tags\";a:1:{i:0;s:1:\"6\";}}'),
(1576, 129, 'product_video_description', NULL),
(1577, 129, 'product_video', NULL),
(1578, 130, 'slug', 'product-20'),
(1579, 130, 'price', '68'),
(1580, 130, 'sale_price', ''),
(1581, 130, 'sale_start_date', ''),
(1582, 130, 'sale_end_date', ''),
(1583, 130, 'sku', ''),
(1584, 130, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1585, 130, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1586, 130, 'featured_image', 'upload/product/product-20/feature/ffTcVtIHULgLm6wH3WPnLquDZHukVxOkLDQNb191.jpeg'),
(1587, 130, 'stock', ''),
(1588, 130, 'material', 'Heavy Material,Soft Quality'),
(1589, 130, 'sizes', '240cm, 566c'),
(1590, 130, 'product_features_description', ''),
(1591, 130, 'stock_manage_chk', ''),
(1592, 130, 'stock_threshold', ''),
(1593, 130, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"T1AkkXXDjZu6IwZjVyO3VG9rXGtFAUvrrrs6Y4nA\";s:12:\"product_name\";a:2:{i:0;s:10:\"Product-20\";i:1;s:10:\"Product-21\";}s:10:\"image_name\";a:2:{i:0;s:74:\"upload/product_temperory_img/ffTcVtIHULgLm6wH3WPnLquDZHukVxOkLDQNb191.jpeg\";i:1;s:74:\"upload/product_temperory_img/kdoWT5HZY8OEZnzuJz3aDaRajZBm2nEDCTOEckSf.jpeg\";}s:12:\"image_row_id\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}s:10:\"categories\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:5:\"price\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";i:1;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";}s:8:\"material\";a:2:{i:0;s:27:\"Heavy Material,Soft Quality\";i:1;s:27:\"Heavy Material,Soft Quality\";}s:5:\"sizes\";a:2:{i:0;s:11:\"240cm, 566c\";i:1;s:11:\"240cm, 566c\";}s:11:\"temp_img_id\";s:1:\"2\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1594, 130, 'product_video_description', ''),
(1595, 130, 'product_video', ''),
(1596, 131, 'slug', 'product-21'),
(1597, 131, 'price', '68'),
(1598, 131, 'sale_price', ''),
(1599, 131, 'sale_start_date', ''),
(1600, 131, 'sale_end_date', ''),
(1601, 131, 'sku', ''),
(1602, 131, 'tags', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}'),
(1603, 131, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1604, 131, 'featured_image', 'upload/product/product-21/feature/kdoWT5HZY8OEZnzuJz3aDaRajZBm2nEDCTOEckSf.jpeg'),
(1605, 131, 'stock', ''),
(1606, 131, 'material', 'Heavy Material,Soft Quality'),
(1607, 131, 'sizes', '240cm, 566c'),
(1608, 131, 'product_features_description', ''),
(1609, 131, 'stock_manage_chk', ''),
(1610, 131, 'stock_threshold', ''),
(1611, 131, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"T1AkkXXDjZu6IwZjVyO3VG9rXGtFAUvrrrs6Y4nA\";s:12:\"product_name\";a:2:{i:0;s:10:\"Product-20\";i:1;s:10:\"Product-21\";}s:10:\"image_name\";a:2:{i:0;s:74:\"upload/product_temperory_img/ffTcVtIHULgLm6wH3WPnLquDZHukVxOkLDQNb191.jpeg\";i:1;s:74:\"upload/product_temperory_img/kdoWT5HZY8OEZnzuJz3aDaRajZBm2nEDCTOEckSf.jpeg\";}s:12:\"image_row_id\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}s:10:\"categories\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:5:\"price\";a:2:{i:0;s:2:\"68\";i:1;s:2:\"68\";}s:4:\"tags\";a:2:{i:0;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";i:1;s:30:\"a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}\";}s:8:\"material\";a:2:{i:0;s:27:\"Heavy Material,Soft Quality\";i:1;s:27:\"Heavy Material,Soft Quality\";}s:5:\"sizes\";a:2:{i:0;s:11:\"240cm, 566c\";i:1;s:11:\"240cm, 566c\";}s:11:\"temp_img_id\";s:1:\"2\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1612, 131, 'product_video_description', ''),
(1613, 131, 'product_video', ''),
(1614, 132, 'slug', 'artist-product-001'),
(1615, 132, 'price', '857'),
(1616, 132, 'sale_price', '0'),
(1617, 132, 'sale_start_date', '05/03/2020 '),
(1618, 132, 'sale_end_date', ' 05/03/2020'),
(1619, 132, 'sku', NULL),
(1620, 132, 'tags', 'a:1:{i:0;s:1:\"6\";}'),
(1621, 132, 'categories', 'a:1:{i:0;s:2:\"68\";}'),
(1622, 132, 'featured_image', 'upload/product/artist-product-001/feature/ameE08LT2Pp8xq3c8bN2IpRgmFWHJP96KcfggdxK.jpeg'),
(1623, 132, 'stock', NULL),
(1624, 132, 'material', 'Hello,Worlf'),
(1625, 132, 'sizes', '34cm,343cm'),
(1626, 132, 'stock_manage_chk', NULL),
(1627, 132, 'stock_threshold', ''),
(1628, 132, 'serialize_data', 'a:15:{s:6:\"_token\";s:40:\"hgcoNGT5S62RVbq5Xbv9Jk7Ys3cz8H7u3uu69hRe\";s:12:\"product_name\";s:19:\"Artist Product -001\";s:11:\"description\";s:455:\"<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo semper gravida. Duis iaculis fringilla felis ac tincidunt. Etiam feugiat ipsum et ex mattis maximus. Cras et eros non metus mollis tincidunt sed in nisi. Curabitur eget mauris mollis, posuere ex a, mattis erat. Sed id lorem id arcu scelerisque fringilla.</span><br></p>\";s:13:\"regular_price\";s:3:\"857\";s:11:\"sale_prices\";N;s:16:\"sales_price_schd\";s:23:\"05/03/2020 - 05/03/2020\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:19:\"low_stock_threshold\";N;s:16:\"product_material\";s:11:\"Hello,Worlf\";s:13:\"product_sizes\";s:10:\"34cm,343cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"68\";}s:4:\"tags\";a:1:{i:0;s:1:\"6\";}}'),
(1629, 132, 'product_video_description', NULL),
(1630, 132, 'product_video', NULL),
(1793, 142, 'slug', 'product-4'),
(1794, 142, 'price', '145'),
(1795, 142, 'sale_price', ''),
(1796, 142, 'sale_start_date', ''),
(1797, 142, 'sale_end_date', ''),
(1798, 142, 'sku', ''),
(1799, 142, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1800, 142, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1801, 142, 'featured_image', 'upload/product/product-4/feature/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg'),
(1802, 142, 'stock', ''),
(1803, 142, 'material', 'Heavy Material , Soft Quality'),
(1804, 142, 'sizes', '240cm, 566cm'),
(1805, 142, 'product_features_description', ''),
(1806, 142, 'stock_manage_chk', ''),
(1807, 142, 'stock_threshold', ''),
(1808, 142, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"rKsO4imbwvDaDMuCqC4nSIGGF5BxBWLInV3OVzW8\";s:12:\"product_name\";a:8:{i:0;s:9:\"Product-4\";i:1;s:9:\"Product-5\";i:2;s:9:\"Product-6\";i:3;s:9:\"Product-7\";i:4;s:10:\"Product-10\";i:5;s:10:\"Product-11\";i:6;s:10:\"Product-13\";i:7;s:10:\"Product-19\";}s:10:\"image_name\";a:8:{i:0;s:74:\"upload/product_temperory_img/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg\";i:1;s:74:\"upload/product_temperory_img/0pGpK7PbTXeprdJTCj1V19YTcmcWpTBKvg1oDktj.jpeg\";i:2;s:74:\"upload/product_temperory_img/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";i:3;s:74:\"upload/product_temperory_img/HfFVpbWs1f3nNUnYZtMcZyQmKWnJdXbKDn2AhysU.jpeg\";i:4;s:74:\"upload/product_temperory_img/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";i:5;s:74:\"upload/product_temperory_img/xmmmtdp5wPLVyj5VRdphuGqtXyr2sWHjcIxjOWIJ.jpeg\";i:6;s:74:\"upload/product_temperory_img/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg\";i:7;s:74:\"upload/product_temperory_img/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg\";}s:12:\"image_row_id\";a:8:{i:0;s:2:\"11\";i:1;s:2:\"12\";i:2;s:2:\"13\";i:3;s:2:\"14\";i:4;s:2:\"15\";i:5;s:2:\"16\";i:6;s:2:\"17\";i:7;s:2:\"18\";}s:10:\"categories\";a:8:{i:0;s:2:\"72\";i:1;s:2:\"72\";i:2;s:2:\"72\";i:3;s:2:\"72\";i:4;s:2:\"72\";i:5;s:2:\"72\";i:6;s:2:\"72\";i:7;s:2:\"72\";}s:5:\"price\";a:8:{i:0;s:3:\"145\";i:1;s:3:\"145\";i:2;s:3:\"145\";i:3;s:3:\"145\";i:4;s:3:\"145\";i:5;s:3:\"145\";i:6;s:3:\"145\";i:7;s:3:\"145\";}s:4:\"tags\";a:8:{i:0;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";}s:8:\"material\";a:8:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:8:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"18\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1809, 142, 'product_video_description', ''),
(1810, 142, 'product_video', ''),
(1829, 144, 'slug', 'product-6'),
(1830, 144, 'price', '145'),
(1831, 144, 'sale_price', ''),
(1832, 144, 'sale_start_date', ''),
(1833, 144, 'sale_end_date', ''),
(1834, 144, 'sku', ''),
(1835, 144, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1836, 144, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1837, 144, 'featured_image', 'upload/product/product-6/feature/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg'),
(1838, 144, 'stock', ''),
(1839, 144, 'material', 'Heavy Material , Soft Quality'),
(1840, 144, 'sizes', '240cm, 566cm'),
(1841, 144, 'product_features_description', ''),
(1842, 144, 'stock_manage_chk', ''),
(1843, 144, 'stock_threshold', ''),
(1844, 144, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"rKsO4imbwvDaDMuCqC4nSIGGF5BxBWLInV3OVzW8\";s:12:\"product_name\";a:8:{i:0;s:9:\"Product-4\";i:1;s:9:\"Product-5\";i:2;s:9:\"Product-6\";i:3;s:9:\"Product-7\";i:4;s:10:\"Product-10\";i:5;s:10:\"Product-11\";i:6;s:10:\"Product-13\";i:7;s:10:\"Product-19\";}s:10:\"image_name\";a:8:{i:0;s:74:\"upload/product_temperory_img/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg\";i:1;s:74:\"upload/product_temperory_img/0pGpK7PbTXeprdJTCj1V19YTcmcWpTBKvg1oDktj.jpeg\";i:2;s:74:\"upload/product_temperory_img/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";i:3;s:74:\"upload/product_temperory_img/HfFVpbWs1f3nNUnYZtMcZyQmKWnJdXbKDn2AhysU.jpeg\";i:4;s:74:\"upload/product_temperory_img/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";i:5;s:74:\"upload/product_temperory_img/xmmmtdp5wPLVyj5VRdphuGqtXyr2sWHjcIxjOWIJ.jpeg\";i:6;s:74:\"upload/product_temperory_img/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg\";i:7;s:74:\"upload/product_temperory_img/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg\";}s:12:\"image_row_id\";a:8:{i:0;s:2:\"11\";i:1;s:2:\"12\";i:2;s:2:\"13\";i:3;s:2:\"14\";i:4;s:2:\"15\";i:5;s:2:\"16\";i:6;s:2:\"17\";i:7;s:2:\"18\";}s:10:\"categories\";a:8:{i:0;s:2:\"72\";i:1;s:2:\"72\";i:2;s:2:\"72\";i:3;s:2:\"72\";i:4;s:2:\"72\";i:5;s:2:\"72\";i:6;s:2:\"72\";i:7;s:2:\"72\";}s:5:\"price\";a:8:{i:0;s:3:\"145\";i:1;s:3:\"145\";i:2;s:3:\"145\";i:3;s:3:\"145\";i:4;s:3:\"145\";i:5;s:3:\"145\";i:6;s:3:\"145\";i:7;s:3:\"145\";}s:4:\"tags\";a:8:{i:0;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";}s:8:\"material\";a:8:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:8:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"18\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1845, 144, 'product_video_description', ''),
(1846, 144, 'product_video', ''),
(1847, 145, 'slug', 'product-7'),
(1848, 145, 'price', '145'),
(1849, 145, 'sale_price', ''),
(1850, 145, 'sale_start_date', ''),
(1851, 145, 'sale_end_date', ''),
(1852, 145, 'sku', ''),
(1853, 145, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1854, 145, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1855, 145, 'featured_image', 'upload/product/product-7/feature/HfFVpbWs1f3nNUnYZtMcZyQmKWnJdXbKDn2AhysU.jpeg'),
(1856, 145, 'stock', ''),
(1857, 145, 'material', 'Heavy Material , Soft Quality'),
(1858, 145, 'sizes', '240cm, 566cm'),
(1859, 145, 'product_features_description', ''),
(1860, 145, 'stock_manage_chk', ''),
(1861, 145, 'stock_threshold', ''),
(1862, 145, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"rKsO4imbwvDaDMuCqC4nSIGGF5BxBWLInV3OVzW8\";s:12:\"product_name\";a:8:{i:0;s:9:\"Product-4\";i:1;s:9:\"Product-5\";i:2;s:9:\"Product-6\";i:3;s:9:\"Product-7\";i:4;s:10:\"Product-10\";i:5;s:10:\"Product-11\";i:6;s:10:\"Product-13\";i:7;s:10:\"Product-19\";}s:10:\"image_name\";a:8:{i:0;s:74:\"upload/product_temperory_img/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg\";i:1;s:74:\"upload/product_temperory_img/0pGpK7PbTXeprdJTCj1V19YTcmcWpTBKvg1oDktj.jpeg\";i:2;s:74:\"upload/product_temperory_img/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";i:3;s:74:\"upload/product_temperory_img/HfFVpbWs1f3nNUnYZtMcZyQmKWnJdXbKDn2AhysU.jpeg\";i:4;s:74:\"upload/product_temperory_img/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";i:5;s:74:\"upload/product_temperory_img/xmmmtdp5wPLVyj5VRdphuGqtXyr2sWHjcIxjOWIJ.jpeg\";i:6;s:74:\"upload/product_temperory_img/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg\";i:7;s:74:\"upload/product_temperory_img/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg\";}s:12:\"image_row_id\";a:8:{i:0;s:2:\"11\";i:1;s:2:\"12\";i:2;s:2:\"13\";i:3;s:2:\"14\";i:4;s:2:\"15\";i:5;s:2:\"16\";i:6;s:2:\"17\";i:7;s:2:\"18\";}s:10:\"categories\";a:8:{i:0;s:2:\"72\";i:1;s:2:\"72\";i:2;s:2:\"72\";i:3;s:2:\"72\";i:4;s:2:\"72\";i:5;s:2:\"72\";i:6;s:2:\"72\";i:7;s:2:\"72\";}s:5:\"price\";a:8:{i:0;s:3:\"145\";i:1;s:3:\"145\";i:2;s:3:\"145\";i:3;s:3:\"145\";i:4;s:3:\"145\";i:5;s:3:\"145\";i:6;s:3:\"145\";i:7;s:3:\"145\";}s:4:\"tags\";a:8:{i:0;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";}s:8:\"material\";a:8:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:8:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"18\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1863, 145, 'product_video_description', ''),
(1864, 145, 'product_video', ''),
(1865, 146, 'slug', 'product-10'),
(1866, 146, 'price', '145'),
(1867, 146, 'sale_price', ''),
(1868, 146, 'sale_start_date', ''),
(1869, 146, 'sale_end_date', ''),
(1870, 146, 'sku', ''),
(1871, 146, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1872, 146, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1873, 146, 'featured_image', 'upload/product/product-10/feature/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg'),
(1874, 146, 'stock', ''),
(1875, 146, 'material', 'Heavy Material , Soft Quality'),
(1876, 146, 'sizes', '240cm, 566cm'),
(1877, 146, 'product_features_description', ''),
(1878, 146, 'stock_manage_chk', ''),
(1879, 146, 'stock_threshold', ''),
(1880, 146, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"rKsO4imbwvDaDMuCqC4nSIGGF5BxBWLInV3OVzW8\";s:12:\"product_name\";a:8:{i:0;s:9:\"Product-4\";i:1;s:9:\"Product-5\";i:2;s:9:\"Product-6\";i:3;s:9:\"Product-7\";i:4;s:10:\"Product-10\";i:5;s:10:\"Product-11\";i:6;s:10:\"Product-13\";i:7;s:10:\"Product-19\";}s:10:\"image_name\";a:8:{i:0;s:74:\"upload/product_temperory_img/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg\";i:1;s:74:\"upload/product_temperory_img/0pGpK7PbTXeprdJTCj1V19YTcmcWpTBKvg1oDktj.jpeg\";i:2;s:74:\"upload/product_temperory_img/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";i:3;s:74:\"upload/product_temperory_img/HfFVpbWs1f3nNUnYZtMcZyQmKWnJdXbKDn2AhysU.jpeg\";i:4;s:74:\"upload/product_temperory_img/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";i:5;s:74:\"upload/product_temperory_img/xmmmtdp5wPLVyj5VRdphuGqtXyr2sWHjcIxjOWIJ.jpeg\";i:6;s:74:\"upload/product_temperory_img/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg\";i:7;s:74:\"upload/product_temperory_img/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg\";}s:12:\"image_row_id\";a:8:{i:0;s:2:\"11\";i:1;s:2:\"12\";i:2;s:2:\"13\";i:3;s:2:\"14\";i:4;s:2:\"15\";i:5;s:2:\"16\";i:6;s:2:\"17\";i:7;s:2:\"18\";}s:10:\"categories\";a:8:{i:0;s:2:\"72\";i:1;s:2:\"72\";i:2;s:2:\"72\";i:3;s:2:\"72\";i:4;s:2:\"72\";i:5;s:2:\"72\";i:6;s:2:\"72\";i:7;s:2:\"72\";}s:5:\"price\";a:8:{i:0;s:3:\"145\";i:1;s:3:\"145\";i:2;s:3:\"145\";i:3;s:3:\"145\";i:4;s:3:\"145\";i:5;s:3:\"145\";i:6;s:3:\"145\";i:7;s:3:\"145\";}s:4:\"tags\";a:8:{i:0;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";}s:8:\"material\";a:8:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:8:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"18\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1881, 146, 'product_video_description', ''),
(1882, 146, 'product_video', ''),
(1883, 147, 'slug', 'product-11'),
(1884, 147, 'price', '145'),
(1885, 147, 'sale_price', ''),
(1886, 147, 'sale_start_date', ''),
(1887, 147, 'sale_end_date', ''),
(1888, 147, 'sku', ''),
(1889, 147, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1890, 147, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1891, 147, 'featured_image', 'upload/product/product-11/feature/xmmmtdp5wPLVyj5VRdphuGqtXyr2sWHjcIxjOWIJ.jpeg'),
(1892, 147, 'stock', ''),
(1893, 147, 'material', 'Heavy Material , Soft Quality'),
(1894, 147, 'sizes', '240cm, 566cm'),
(1895, 147, 'product_features_description', ''),
(1896, 147, 'stock_manage_chk', ''),
(1897, 147, 'stock_threshold', ''),
(1898, 147, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"rKsO4imbwvDaDMuCqC4nSIGGF5BxBWLInV3OVzW8\";s:12:\"product_name\";a:8:{i:0;s:9:\"Product-4\";i:1;s:9:\"Product-5\";i:2;s:9:\"Product-6\";i:3;s:9:\"Product-7\";i:4;s:10:\"Product-10\";i:5;s:10:\"Product-11\";i:6;s:10:\"Product-13\";i:7;s:10:\"Product-19\";}s:10:\"image_name\";a:8:{i:0;s:74:\"upload/product_temperory_img/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg\";i:1;s:74:\"upload/product_temperory_img/0pGpK7PbTXeprdJTCj1V19YTcmcWpTBKvg1oDktj.jpeg\";i:2;s:74:\"upload/product_temperory_img/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";i:3;s:74:\"upload/product_temperory_img/HfFVpbWs1f3nNUnYZtMcZyQmKWnJdXbKDn2AhysU.jpeg\";i:4;s:74:\"upload/product_temperory_img/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";i:5;s:74:\"upload/product_temperory_img/xmmmtdp5wPLVyj5VRdphuGqtXyr2sWHjcIxjOWIJ.jpeg\";i:6;s:74:\"upload/product_temperory_img/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg\";i:7;s:74:\"upload/product_temperory_img/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg\";}s:12:\"image_row_id\";a:8:{i:0;s:2:\"11\";i:1;s:2:\"12\";i:2;s:2:\"13\";i:3;s:2:\"14\";i:4;s:2:\"15\";i:5;s:2:\"16\";i:6;s:2:\"17\";i:7;s:2:\"18\";}s:10:\"categories\";a:8:{i:0;s:2:\"72\";i:1;s:2:\"72\";i:2;s:2:\"72\";i:3;s:2:\"72\";i:4;s:2:\"72\";i:5;s:2:\"72\";i:6;s:2:\"72\";i:7;s:2:\"72\";}s:5:\"price\";a:8:{i:0;s:3:\"145\";i:1;s:3:\"145\";i:2;s:3:\"145\";i:3;s:3:\"145\";i:4;s:3:\"145\";i:5;s:3:\"145\";i:6;s:3:\"145\";i:7;s:3:\"145\";}s:4:\"tags\";a:8:{i:0;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";}s:8:\"material\";a:8:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:8:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"18\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1899, 147, 'product_video_description', ''),
(1900, 147, 'product_video', ''),
(1901, 148, 'slug', 'product-13'),
(1902, 148, 'price', '145'),
(1903, 148, 'sale_price', '0'),
(1904, 148, 'sale_start_date', 'Invalid date '),
(1905, 148, 'sale_end_date', ' Invalid date'),
(1906, 148, 'sku', NULL),
(1907, 148, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1908, 148, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1909, 148, 'featured_image', 'upload/product/product-13/feature/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg'),
(1910, 148, 'stock', NULL),
(1911, 148, 'material', 'Hello_world,Worlf_ggg'),
(1912, 148, 'sizes', '234cm,350cm'),
(1913, 148, 'product_features_description', ''),
(1914, 148, 'stock_manage_chk', NULL),
(1915, 148, 'stock_threshold', NULL),
(1916, 148, 'serialize_data', 'a:16:{s:6:\"_token\";s:40:\"pIdoHNnMmk9we2ZVyeaWIGx5zJmj3byq4TxKgyGM\";s:12:\"product_name\";s:10:\"Product-13\";s:11:\"description\";s:612:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>\";s:13:\"regular_price\";s:3:\"145\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:21:\"Hello_world,Worlf_ggg\";s:13:\"product_sizes\";s:11:\"234cm,350cm\";s:25:\"product_video_description\";s:612:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>\";s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"72\";}s:4:\"tags\";a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}s:7:\"artists\";s:1:\"9\";}'),
(1917, 148, 'product_video_description', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>'),
(1918, 148, 'product_video', 'https://www.youtube.com/embed/-hSOuK7qBgM'),
(1919, 149, 'slug', 'product-19'),
(1920, 149, 'price', '145'),
(1921, 149, 'sale_price', ''),
(1922, 149, 'sale_start_date', ''),
(1923, 149, 'sale_end_date', ''),
(1924, 149, 'sku', ''),
(1925, 149, 'tags', 'a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}'),
(1926, 149, 'categories', 'a:1:{i:0;s:2:\"72\";}'),
(1927, 149, 'featured_image', 'upload/product/product-19/feature/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg'),
(1928, 149, 'stock', ''),
(1929, 149, 'material', 'Heavy Material , Soft Quality'),
(1930, 149, 'sizes', '240cm, 566cm'),
(1931, 149, 'product_features_description', ''),
(1932, 149, 'stock_manage_chk', ''),
(1933, 149, 'stock_threshold', ''),
(1934, 149, 'serialize_data', 'a:11:{s:6:\"_token\";s:40:\"rKsO4imbwvDaDMuCqC4nSIGGF5BxBWLInV3OVzW8\";s:12:\"product_name\";a:8:{i:0;s:9:\"Product-4\";i:1;s:9:\"Product-5\";i:2;s:9:\"Product-6\";i:3;s:9:\"Product-7\";i:4;s:10:\"Product-10\";i:5;s:10:\"Product-11\";i:6;s:10:\"Product-13\";i:7;s:10:\"Product-19\";}s:10:\"image_name\";a:8:{i:0;s:74:\"upload/product_temperory_img/MhrT5RI6L6UMWrgAhl5v6IxRCAPgPvEIEk9zc068.jpeg\";i:1;s:74:\"upload/product_temperory_img/0pGpK7PbTXeprdJTCj1V19YTcmcWpTBKvg1oDktj.jpeg\";i:2;s:74:\"upload/product_temperory_img/rNwb0QQmabRvSm87qevD7pGKQadeubet8uNsrlG1.jpeg\";i:3;s:74:\"upload/product_temperory_img/HfFVpbWs1f3nNUnYZtMcZyQmKWnJdXbKDn2AhysU.jpeg\";i:4;s:74:\"upload/product_temperory_img/L0PFePg1nYX2TSLYoh1HsG9bXK4G4Qq0eftQUWFk.jpeg\";i:5;s:74:\"upload/product_temperory_img/xmmmtdp5wPLVyj5VRdphuGqtXyr2sWHjcIxjOWIJ.jpeg\";i:6;s:74:\"upload/product_temperory_img/YEPmFzs8pTChW1S4WlRgHldrsco5e3gPH6lZTabm.jpeg\";i:7;s:74:\"upload/product_temperory_img/dh1uKudXi2hCShPwq8RkLrIis20TUCBztHFEXgbH.jpeg\";}s:12:\"image_row_id\";a:8:{i:0;s:2:\"11\";i:1;s:2:\"12\";i:2;s:2:\"13\";i:3;s:2:\"14\";i:4;s:2:\"15\";i:5;s:2:\"16\";i:6;s:2:\"17\";i:7;s:2:\"18\";}s:10:\"categories\";a:8:{i:0;s:2:\"72\";i:1;s:2:\"72\";i:2;s:2:\"72\";i:3;s:2:\"72\";i:4;s:2:\"72\";i:5;s:2:\"72\";i:6;s:2:\"72\";i:7;s:2:\"72\";}s:5:\"price\";a:8:{i:0;s:3:\"145\";i:1;s:3:\"145\";i:2;s:3:\"145\";i:3;s:3:\"145\";i:4;s:3:\"145\";i:5;s:3:\"145\";i:6;s:3:\"145\";i:7;s:3:\"145\";}s:4:\"tags\";a:8:{i:0;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:1;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:2;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:3;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:4;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:5;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:6;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";i:7;s:42:\"a:3:{i:0;s:1:\"6\";i:1;s:1:\"7\";i:2;s:1:\"8\";}\";}s:8:\"material\";a:8:{i:0;s:29:\"Heavy Material , Soft Quality\";i:1;s:29:\"Heavy Material , Soft Quality\";i:2;s:29:\"Heavy Material , Soft Quality\";i:3;s:29:\"Heavy Material , Soft Quality\";i:4;s:29:\"Heavy Material , Soft Quality\";i:5;s:29:\"Heavy Material , Soft Quality\";i:6;s:29:\"Heavy Material , Soft Quality\";i:7;s:29:\"Heavy Material , Soft Quality\";}s:5:\"sizes\";a:8:{i:0;s:12:\"240cm, 566cm\";i:1;s:12:\"240cm, 566cm\";i:2;s:12:\"240cm, 566cm\";i:3;s:12:\"240cm, 566cm\";i:4;s:12:\"240cm, 566cm\";i:5;s:12:\"240cm, 566cm\";i:6;s:12:\"240cm, 566cm\";i:7;s:12:\"240cm, 566cm\";}s:11:\"temp_img_id\";s:2:\"18\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(1935, 149, 'product_video_description', ''),
(1936, 149, 'product_video', ''),
(1937, 148, 'artist', '9'),
(2019, 155, 'slug', 'post-img1'),
(2020, 155, 'artist', '13'),
(2021, 155, 'artist_updated_separately', 'no'),
(2022, 155, 'price', '126'),
(2023, 155, 'sale_price', ''),
(2024, 155, 'sale_start_date', ''),
(2025, 155, 'sale_end_date', ''),
(2026, 155, 'sku', ''),
(2027, 155, 'tags', 'a:2:{i:0;s:30:\"a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}\";i:1;s:30:\"a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}\";}'),
(2028, 155, 'categories', 'a:1:{i:0;s:2:\"73\";}'),
(2029, 155, 'featured_image', 'upload/product/post-img1/feature/nEfHsDtaInZ4ArHS2FtOvPjJZCRhoMk3fYQHKjC0.jpeg'),
(2030, 155, 'stock', ''),
(2031, 155, 'material', 'Heavy_Material,Soft_Quality'),
(2032, 155, 'sizes', '240cm,566cm'),
(2033, 155, 'product_features_description', ''),
(2034, 155, 'stock_manage_chk', ''),
(2035, 155, 'stock_threshold', ''),
(2036, 155, 'serialize_data', 'a:12:{s:6:\"_token\";s:40:\"dgzEF17R6bvtablKbpMKiVWUOocKqVR4ROZU6nIj\";s:12:\"product_name\";a:2:{i:0;s:9:\"post-img1\";i:1;s:9:\"post-img2\";}s:10:\"image_name\";a:2:{i:0;s:74:\"upload/product_temperory_img/nEfHsDtaInZ4ArHS2FtOvPjJZCRhoMk3fYQHKjC0.jpeg\";i:1;s:74:\"upload/product_temperory_img/SJJ05fADEODOCttITm0UsVZXYQZBDut4PBBrxiFX.jpeg\";}s:12:\"image_row_id\";a:2:{i:0;s:2:\"28\";i:1;s:2:\"29\";}s:10:\"categories\";a:2:{i:0;s:2:\"73\";i:1;s:2:\"73\";}s:5:\"price\";a:2:{i:0;s:3:\"126\";i:1;s:3:\"126\";}s:4:\"tags\";a:2:{i:0;s:30:\"a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}\";i:1;s:30:\"a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}\";}s:8:\"material\";a:2:{i:0;s:27:\"Heavy_Material,Soft_Quality\";i:1;s:27:\"Heavy_Material,Soft_Quality\";}s:5:\"sizes\";a:2:{i:0;s:11:\"240cm,566cm\";i:1;s:11:\"240cm,566cm\";}s:6:\"artist\";a:2:{i:0;s:2:\"13\";i:1;s:2:\"13\";}s:11:\"temp_img_id\";s:2:\"29\";s:10:\"all_upload\";s:19:\"Submit All Products\";}'),
(2037, 155, 'product_video_description', ''),
(2038, 155, 'product_video', ''),
(2039, 156, 'slug', 'post-img2'),
(2040, 156, 'artist', '16'),
(2041, 156, 'artist_updated_separately', 'yes'),
(2042, 156, 'price', '126'),
(2043, 156, 'sale_price', '0'),
(2044, 156, 'sale_start_date', 'Invalid date '),
(2045, 156, 'sale_end_date', ' Invalid date'),
(2046, 156, 'sku', NULL),
(2047, 156, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}'),
(2048, 156, 'categories', 'a:1:{i:0;s:2:\"73\";}'),
(2049, 156, 'featured_image', 'upload/product/post-img2/feature/SJJ05fADEODOCttITm0UsVZXYQZBDut4PBBrxiFX.jpeg'),
(2050, 156, 'stock', NULL),
(2051, 156, 'material', 'Heavy_Material,Soft_Quality'),
(2052, 156, 'sizes', '240cm,566cm'),
(2053, 156, 'product_features_description', ''),
(2054, 156, 'stock_manage_chk', NULL),
(2055, 156, 'stock_threshold', NULL),
(2056, 156, 'serialize_data', 'a:17:{s:6:\"_token\";s:40:\"dgzEF17R6bvtablKbpMKiVWUOocKqVR4ROZU6nIj\";s:12:\"product_name\";s:9:\"post-img2\";s:11:\"description\";s:612:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis nulla posuere urna tristique, nec viverra sapien faucibus. Donec at metus odio. Vestibulum ornare vitae quam id laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam arcu quam, interdum ac est at, eleifend euismod urna. Quisque eu eros eget urna dignissim imperdiet. Quisque laoreet id sem vitae vulputate. Cras felis leo, porttitor a urna sed, molestie sollicitudin lorem.</span>\";s:13:\"regular_price\";s:3:\"126\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"73\";}s:4:\"tags\";a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}s:7:\"artists\";s:2:\"16\";s:12:\"prevs_artist\";s:2:\"18\";}'),
(2057, 156, 'product_video_description', NULL),
(2058, 156, 'product_video', NULL),
(2059, 157, 'slug', 'cat-widget1'),
(2060, 157, 'artist', '13'),
(2061, 157, 'artist_updated_separately', 'no'),
(2062, 157, 'price', '126'),
(2063, 157, 'sale_price', '0'),
(2064, 157, 'sale_start_date', 'Invalid date '),
(2065, 157, 'sale_end_date', ' Invalid date'),
(2066, 157, 'sku', NULL),
(2067, 157, 'tags', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}'),
(2068, 157, 'categories', 'a:1:{i:0;s:2:\"73\";}'),
(2069, 157, 'featured_image', 'upload/product/cat-widget1/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg'),
(2070, 157, 'stock', NULL),
(2071, 157, 'material', 'Heavy_Material,Soft_Quality'),
(2072, 157, 'sizes', '240cm,566cm'),
(2073, 157, 'product_features_description', ''),
(2074, 157, 'stock_manage_chk', NULL),
(2075, 157, 'stock_threshold', NULL),
(2076, 157, 'serialize_data', 'a:17:{s:6:\"_token\";s:40:\"7Kv3BXseacrNfZ8M7ZQ5bMV6GsigVv4ClwWpToAa\";s:12:\"product_name\";s:11:\"cat-widget1\";s:11:\"description\";s:604:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis. Fusce at pretium urna, quis pharetra elit. Curabitur sollicitudin pellentesque ex sit amet imperdiet. Aenean erat ante, accumsan venenatis tincidunt a, tincidunt sed leo. Morbi scelerisque tempus neque vel pretium. Vivamus ac erat cursus, commodo ligula et, eleifend nulla. Nulla sit amet volutpat turpis. Suspendisse pretium maximus sem, at fermentum velit viverra sodales.</span>\";s:13:\"regular_price\";s:3:\"126\";s:11:\"sale_prices\";s:1:\"0\";s:16:\"sales_price_schd\";s:27:\"Invalid date - Invalid date\";s:11:\"product_sku\";N;s:14:\"stock_quantity\";N;s:15:\"stock_threshold\";N;s:16:\"product_material\";s:27:\"Heavy_Material,Soft_Quality\";s:13:\"product_sizes\";s:11:\"240cm,566cm\";s:25:\"product_video_description\";N;s:6:\"status\";s:3:\"yes\";s:10:\"categories\";a:1:{i:0;s:2:\"73\";}s:4:\"tags\";a:2:{i:0;s:1:\"5\";i:1;s:1:\"8\";}s:7:\"artists\";s:2:\"13\";s:12:\"prevs_artist\";s:2:\"13\";}'),
(2077, 157, 'product_video_description', NULL),
(2078, 157, 'product_video', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute` varchar(255) DEFAULT NULL,
  `value` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `attribute`, `value`, `created_at`, `updated_at`) VALUES
(1, 157, 'color', 'a:4:{i:0;s:3:\"red\";i:1;s:5:\"black\";i:2;s:5:\"green\";i:3;s:4:\"blue\";}', '2020-05-09 16:10:56', '2020-05-09 16:10:56'),
(2, 157, 'size', 'a:3:{i:0;s:5:\"Small\";i:1;s:6:\"Medium\";i:2;s:5:\"Large\";}', '2020-05-09 17:17:46', '2020-05-11 13:36:09');

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `flag` varchar(255) DEFAULT NULL,
  `description` longtext,
  `image` longtext,
  `video` longtext,
  `parent_id` int(11) DEFAULT NULL,
  `voted` varchar(255) DEFAULT NULL,
  `unvoted` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `user_id`, `product_id`, `status`, `flag`, `description`, `image`, `video`, `parent_id`, `voted`, `unvoted`, `created_at`, `updated_at`) VALUES
(5, 1, 126, 'off', 'average', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies tortor sit amet sagittis imperdiet.', 'a:2:{i:0;s:95:\"upload/product_review/product_126/user_1/review_5/Mr6p0B01tHarH79e8Poy9G2J3s0wbP758NEv2sZg.jpeg\";i:1;s:95:\"upload/product_review/product_126/user_1/review_5/r0urDvXmvaztMO1AwRId5ZAaeYLx8ax8p3EiyRmQ.jpeg\";}', NULL, NULL, NULL, NULL, '2020-04-26 15:27:34', '2020-05-13 16:57:14'),
(6, 21, 148, 'on', 'good', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'a:2:{i:0;s:96:\"upload/product_review/product_148/user_21/review_6/Q9DXZa5nRN3zj6g9FwcABmuFvkNsWe9VHQ8j55iB.jpeg\";i:1;s:96:\"upload/product_review/product_148/user_21/review_6/rkIIjnDjThgkbJXjc0H5Q1TWBa5VFXSJNa5zVAW1.jpeg\";}', NULL, NULL, NULL, NULL, '2020-05-04 14:10:11', '2020-05-13 16:57:16'),
(9, 1, 148, 'on', 'perfect', 'good product', 'a:3:{i:0;s:95:\"upload/product_review/product_148/user_1/review_9/kLo3JMyYJRyEVIvVnO3XCepakzNU6yrIAVkBx9IH.jpeg\";i:1;s:95:\"upload/product_review/product_148/user_1/review_9/C0pvRHdfk814WkMNDJTP41XlAIcQy8ako1zkX6dr.jpeg\";i:2;s:95:\"upload/product_review/product_148/user_1/review_9/BQaNXQvzj1sgwnBlUzp6yzYWrQzqhQYJuvKKxvL5.jpeg\";}', NULL, NULL, NULL, NULL, '2020-05-04 14:23:18', '2020-05-05 13:22:37');

-- --------------------------------------------------------

--
-- Table structure for table `product_tag`
--

CREATE TABLE `product_tag` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_tag`
--

INSERT INTO `product_tag` (`id`, `product_id`, `tag_id`) VALUES
(165, 117, 5),
(166, 117, 6),
(167, 117, 9),
(168, 118, 5),
(169, 118, 6),
(170, 118, 9),
(183, 123, 5),
(184, 123, 6),
(185, 124, 6),
(186, 124, 9),
(187, 125, 6),
(188, 125, 9),
(189, 126, 6),
(190, 126, 9),
(191, 127, 6),
(192, 127, 9),
(193, 128, 5),
(194, 129, 6),
(195, 130, 6),
(196, 130, 9),
(197, 131, 6),
(198, 131, 9),
(199, 132, 6),
(218, 142, 6),
(219, 142, 7),
(220, 142, 8),
(224, 144, 6),
(225, 144, 7),
(226, 144, 8),
(227, 145, 6),
(228, 145, 7),
(229, 145, 8),
(230, 146, 6),
(231, 146, 7),
(232, 146, 8),
(233, 147, 6),
(234, 147, 7),
(235, 147, 8),
(236, 148, 6),
(237, 148, 7),
(238, 148, 8),
(239, 149, 6),
(240, 149, 7),
(241, 149, 8),
(242, 150, 5),
(243, 150, 8),
(244, 151, 5),
(245, 151, 8),
(252, 155, 5),
(253, 155, 8),
(254, 156, 5),
(255, 156, 8),
(256, 157, 5),
(257, 157, 8);

-- --------------------------------------------------------

--
-- Table structure for table `product_variations`
--

CREATE TABLE `product_variations` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `product_id` int(11) NOT NULL,
  `variation` varchar(500) DEFAULT NULL,
  `variation_meta` longtext,
  `image` longtext,
  `price` int(200) DEFAULT NULL,
  `sale_price` int(200) DEFAULT NULL,
  `stock` int(200) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `description` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_variations`
--

INSERT INTO `product_variations` (`id`, `status`, `product_id`, `variation`, `variation_meta`, `image`, `price`, `sale_price`, `stock`, `sku`, `description`, `created_at`, `updated_at`) VALUES
(5, 'on', 157, 'a:2:{i:0;s:9:\"color-red\";i:1;s:10:\"size-small\";}', 'a:2:{i:0;a:2:{s:9:\"attribute\";s:5:\"color\";s:9:\"variation\";s:3:\"red\";}i:1;a:2:{s:9:\"attribute\";s:4:\"size\";s:9:\"variation\";s:5:\"small\";}}', 'upload/product_variation/product_157/variation_5/thumbnail/1rmCnt5KWyiom9fnDtcOOJztbRhjH4LqTQJW79LC.jpeg', 120, NULL, 12, 'ds222-op97', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis.', '2020-05-12 18:31:22', '2020-05-17 14:26:58'),
(6, 'on', 157, 'a:2:{i:0;s:11:\"color-black\";i:1;s:11:\"size-medium\";}', 'a:2:{i:0;a:2:{s:9:\"attribute\";s:5:\"color\";s:9:\"variation\";s:5:\"black\";}i:1;a:2:{s:9:\"attribute\";s:4:\"size\";s:9:\"variation\";s:6:\"medium\";}}', 'upload/product_variation/product_157/variation_6/thumbnail/V4VyMZCfYJZZOLZGuXIU9ezr9YKeRKJnqK7AuUK3.jpeg', 100, NULL, 15, 'ds222-op97-A', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis.', '2020-05-12 18:54:32', '2020-05-17 14:27:12'),
(10, 'on', 157, 'a:2:{i:0;s:10:\"color-blue\";i:1;s:10:\"size-small\";}', 'a:2:{i:0;a:2:{s:9:\"attribute\";s:5:\"color\";s:9:\"variation\";s:4:\"blue\";}i:1;a:2:{s:9:\"attribute\";s:4:\"size\";s:9:\"variation\";s:5:\"small\";}}', NULL, 126, NULL, NULL, NULL, NULL, '2020-05-16 14:24:46', '2020-05-17 14:27:32');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-02-13 15:14:24', '2020-02-13 15:14:24'),
(2, 'vendor', 'web', '2020-02-13 15:14:33', '2020-02-13 15:14:33'),
(3, 'user', 'web', '2020-02-13 15:14:39', '2020-02-13 15:14:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_setting`
--

CREATE TABLE `site_setting` (
  `id` int(11) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_setting`
--

INSERT INTO `site_setting` (`id`, `key`, `value`) VALUES
(1, 'site_maintenance_status', 'off');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `alias`, `created_at`, `updated_at`) VALUES
(5, 'hello', 'hello', '2020-02-22 07:04:07', '2020-02-22 07:04:07'),
(6, 'Arts', 'arts', '2020-02-23 03:57:18', '2020-02-23 03:57:18'),
(7, 'Garments', 'garments', '2020-02-23 03:57:23', '2020-02-23 03:57:23'),
(8, 'Discovery', 'discovery', '2020-02-23 03:57:29', '2020-02-23 03:57:29'),
(9, 'Abstract', 'abstract', '2020-02-23 03:57:42', '2020-02-23 03:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `verified` int(11) DEFAULT '0',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `profile_pic` longtext COLLATE utf8_unicode_ci,
  `provider_id` longtext COLLATE utf8_unicode_ci,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_customer_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `verified`, `password`, `status`, `profile_pic`, `provider_id`, `provider`, `remember_token`, `stripe_customer_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, 1, '$2y$10$EdCgnoxnbqMxzpJGTdCkC.EPSc5YcsAzXS6w/doZpJ.uOEr3asrA.', 1, NULL, NULL, NULL, NULL, 'cus_HD75mM8uUQk96X', '2020-02-13 15:20:20', '2020-05-03 07:23:20'),
(21, 'Developer Tester', 'yasin.100@hotmail.com', NULL, 1, '$2y$10$70bnco.KAKfRVljRgEyppOA/l/S7OyUvPGECOHBruwqTmzreiBsFu', 1, NULL, NULL, NULL, NULL, 'cus_HD73Y5nUfTEjyr', '2020-04-05 05:07:52', '2020-05-05 13:15:32'),
(32, 'terer', 'yasin@hztech.biz', NULL, 1, '$2y$10$44JInJ2jj1c0aijkENluz.s4x7w6GWlVfSCUbnqtO.DoTVkBGyIc.', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-03 09:37:24', '2020-05-05 13:15:34'),
(33, 'Ahmed', 'ahmed@gmail.com', NULL, 1, '$2y$10$pJP78rfp1Yu09Ma7FwChzef7U6lDsGIynikjGbywOQU.hCDGM8Dxu', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(34, 'Noreen Jack', 'noreen_jack@gmail.com', NULL, 1, '$2y$10$G2vRdUgf8jvok0D.1B.KcOTFoDoIy0YtIm9.dC8mHHPqloxPJPjDa', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(35, 'Watt Wilson', 'watt@gmail.com', NULL, 1, '$2y$10$WZefNqTGBQSm.1j602/wnum9UdJgY0oDbbAa4nf4rFkdfgOZfM7Xi', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(36, 'Tahseen', 'tahseen123@gmail.com', NULL, 1, '$2y$10$pg8PD8g/sCgwhpEZPpNMOOGCrGoYc.fySxFXyGps0Bfjh84SqTcPi', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(37, 'Emma Watson', 'emma@gmail.com', NULL, 1, '$2y$10$2qFYF7ZX/wcIvi2VDmxK.u.s83Dd0ZW0VfFJDAGKuWFBPFmBb4Lii', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(38, 'Fisk Watt', 'fisk@gmail.com', NULL, 1, '$2y$10$dUWbSRS0AuGGmBrW.ZyTnejxbM2tEc5C91pPGP3ixkxbYuxpd7H.q', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(39, 'Tenni', 'tenni@gmail.com', NULL, 1, '$2y$10$.PJibWQJGE0oLMOp7cR1o.nV1fJZ0LTutl2ws702q7THfTvhB5vFK', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(40, 'Fahim', 'fahim@gmail.com', NULL, 1, '$2y$10$pyrnAETI06oGh9Hi7iJIsee9BSLTn0ss2vlfqQoOHb63TyKsRnsTy', 1, NULL, NULL, NULL, NULL, NULL, '2020-05-05 15:11:22', '2020-05-05 15:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `users_meta`
--

CREATE TABLE `users_meta` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `meta_key` varchar(200) DEFAULT NULL,
  `meta_value` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_meta`
--

INSERT INTO `users_meta` (`id`, `user_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(63, 1, 'street_name', 'standfield rd', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(64, 1, 'street_no', 'ivor rd - 0984', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(66, 1, 'city', 'monaal', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(67, 1, 'telno', '034233455555', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(68, 1, 'shipping_address', 'sdsdasdxczxczxcz', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(69, 1, 'billing_address', 'fefrgredffvvxvvxxxz', '2020-03-28 07:17:56', '2020-03-28 07:17:56'),
(71, 1, 'country', 'Argentina', '2020-03-28 07:34:05', '2020-03-28 07:34:05'),
(92, 21, 'street_name', 'qwert no222 RD', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(93, 21, 'street_no', 'sdd', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(94, 21, 'country', 'Afghanistan', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(95, 21, 'city', 'Karachi', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(96, 21, 'telno', '3131312', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(97, 22, 'street_name', 'qwert no222 RD', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(98, 22, 'street_no', 'sdd', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(99, 22, 'country', 'Albania', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(100, 22, 'city', 'Karachi', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(101, 22, 'telno', '3131312', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(102, 23, 'street_name', 'Jillian Mcdaniel', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(103, 23, 'street_no', 'Velit iste qui optio', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(104, 23, 'country', 'Algeria', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(105, 23, 'city', 'Voluptate rerum sed', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(106, 23, 'telno', '5465456454', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(107, 24, 'street_name', 'Jillian Mcdaniel', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(108, 24, 'street_no', 'Velit iste qui optio', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(109, 24, 'country', 'Åland Islands', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(110, 24, 'city', 'Voluptate rerum sed', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(111, 24, 'telno', '5465456454', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(112, 25, 'street_name', 'Ivor Bowers', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(113, 25, 'street_no', 'Tempora qui cum cons', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(114, 25, 'country', 'Afghanistan', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(115, 25, 'city', 'Rerum voluptatem Ni', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(116, 25, 'telno', '4545654', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(117, 26, 'street_name', 'Ivor Bowers', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(118, 26, 'street_no', 'Tempora qui cum cons', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(119, 26, 'country', 'Albania', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(120, 26, 'city', 'Rerum voluptatem Ni', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(121, 26, 'telno', '4545654', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(122, 27, 'street_name', 'Joelle Riggs', '2020-05-03 09:02:08', '2020-05-03 09:02:08'),
(123, 27, 'street_no', 'Tempor officia cupid', '2020-05-03 09:02:08', '2020-05-03 09:02:08'),
(124, 27, 'country', 'Åland Islands', '2020-05-03 09:02:09', '2020-05-03 09:02:09'),
(125, 27, 'city', 'Iusto esse impedit', '2020-05-03 09:02:09', '2020-05-03 09:02:09'),
(126, 27, 'telno', '4546465', '2020-05-03 09:02:09', '2020-05-03 09:02:09'),
(127, 28, 'street_name', 'Garrett Singleton', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(128, 28, 'street_no', 'Obcaecati consectetu', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(129, 28, 'country', 'Åland Islands', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(130, 28, 'city', 'Perspiciatis omnis', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(131, 28, 'telno', '545465', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(132, 29, 'street_name', 'Asher Boyle', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(133, 29, 'street_no', 'Cupidatat et eu laud', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(134, 29, 'country', 'Afghanistan', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(135, 29, 'city', 'Et hic tenetur aut q', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(136, 29, 'telno', '32113123131', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(142, 31, 'street_name', 'Kelly Ball', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(143, 31, 'street_no', 'Inventore perferendi', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(144, 31, 'country', 'Albania', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(145, 31, 'city', 'Modi irure modi saep', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(146, 31, 'telno', '5456454564', '2020-05-03 09:14:51', '2020-05-03 09:14:51'),
(147, 32, 'street_name', 'Kelly Ball', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(148, 32, 'street_no', 'Inventore perferendi', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(149, 32, 'country', 'Åland Islands', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(150, 32, 'city', 'Modi irure modi saep', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(151, 32, 'telno', '5456454564', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(152, 21, 'shipping_address', 'vbnbnv', '2020-05-04 22:18:20', '2020-05-04 22:18:20'),
(153, 21, 'billing_address', 'asdasdasdasdasd', '2020-05-04 22:18:20', '2020-05-04 22:18:20'),
(154, 33, 'street_name', 'qwert no222 RD', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(155, 33, 'street_no', 'sdd', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(156, 33, 'country', 'Åland Islands', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(157, 33, 'city', 'Karachi', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(158, 33, 'telno', '5645454', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(159, 34, 'street_name', '234 Route 34', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(160, 34, 'street_no', 'Asford rd 109.', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(161, 34, 'country', 'American Samoa', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(162, 34, 'city', 'dummy', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(163, 34, 'telno', '8797897998', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(164, 35, 'street_name', 'qwert no222 RD', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(165, 35, 'street_no', 'sdad', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(166, 35, 'country', 'Bolivia', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(167, 35, 'city', 'sdasda', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(168, 35, 'telno', '87897798789789', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(169, 36, 'street_name', 'qwert no222 RD', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(170, 36, 'street_no', 'sdd', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(171, 36, 'country', 'Mongolia', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(172, 36, 'city', 'Karachi', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(173, 36, 'telno', '978789789', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(174, 37, 'street_name', 'qwert no222 RD', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(175, 37, 'street_no', 'sdd', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(176, 37, 'country', 'Åland Islands', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(177, 37, 'city', 'Karachi', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(178, 37, 'telno', '9878745546', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(179, 38, 'street_name', 'qwert no222 RD', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(180, 38, 'street_no', 'dsadasdas', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(181, 38, 'country', 'Afghanistan', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(182, 38, 'city', 'sdasda', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(183, 38, 'telno', '5456454564', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(184, 39, 'street_name', 'qwert no222 RD', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(185, 39, 'street_no', 'sdad', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(186, 39, 'country', 'Benin', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(187, 39, 'city', 'Karachi', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(188, 39, 'telno', '3131312', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(189, 40, 'street_name', 'qwert no222 RD', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(190, 40, 'street_no', 'sdd', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(191, 40, 'country', 'Albania', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(192, 40, 'city', 'sdasda', '2020-05-05 15:11:22', '2020-05-05 15:11:22'),
(193, 40, 'telno', '3131312', '2020-05-05 15:11:22', '2020-05-05 15:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `verify_users`
--

INSERT INTO `verify_users` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, 1, 'YQ18USY0zHuDuu3stUrLoHKNQoCJWAYmN7XUaL9B', '2020-02-13 15:20:20', '2020-02-13 15:20:20'),
(2, 2, 'ZOoBKMmAHcCGCH4cIu9I5US7GgqmBqMd76g9plX2', '2020-02-13 15:22:25', '2020-02-13 15:22:25'),
(3, 17, 'BDomKCMmy1stRhyG7AVS263MkrXbyGq1RWbtFBUy', '2020-04-05 04:09:27', '2020-04-05 04:09:27'),
(4, 18, 'uKdLWg7vBLkFeOS1QaNDPIIOiI0dkH8UMf2mXD2L', '2020-04-05 04:56:21', '2020-04-05 04:56:21'),
(5, 19, 'YJUs9z2gK4HdOsdP9Zw865LX6D7LoSceq2HTJS0o', '2020-04-05 05:01:41', '2020-04-05 05:01:41'),
(6, 20, 'P2m7Epvt2JMTRdiysFP4iigqp3q2aDaJycGYeZKy', '2020-04-05 05:04:00', '2020-04-05 05:04:00'),
(7, 21, 'Kla1bxjm6lm6BlN4fmEzyScQb0QOOPZd5IuS1UqJ', '2020-04-05 05:07:52', '2020-04-05 05:07:52'),
(8, 22, 'RhYgcAN63mJkBvTrJrEuwlSmv08oTHhPHxwRN9QB', '2020-05-03 08:39:33', '2020-05-03 08:39:33'),
(9, 23, 'dKFqfT5jfRuBtb5BCiNs26MtXZLYVOxTPbSXRcKE', '2020-05-03 08:47:01', '2020-05-03 08:47:01'),
(10, 24, 'ulbYfi60eGOwzN4INyVUzssMJgOE7HL4YLunbE7Q', '2020-05-03 08:55:09', '2020-05-03 08:55:09'),
(11, 25, '29DRIYDc1fvFiKz1xz6YVOkvdrFsdYoanCqo0s5Y', '2020-05-03 09:00:38', '2020-05-03 09:00:38'),
(12, 26, 'm74273EdhrQKQ3nwTjnPlod28nnffcLQCiMzUaMO', '2020-05-03 09:01:31', '2020-05-03 09:01:31'),
(13, 27, '3Glo7RSRmsydyOWFFrUDIeoStd8jUTF9xgg3Ek7S', '2020-05-03 09:02:09', '2020-05-03 09:02:09'),
(14, 28, 'MBhxmSxx9SzRPPwenMwpp6CtCwgCM7xzix5gJpFy', '2020-05-03 09:06:12', '2020-05-03 09:06:12'),
(15, 29, 'DkfLKedOhgphknJUJx2UAjZF1P2n6LmfiTWqCtZN', '2020-05-03 09:10:47', '2020-05-03 09:10:47'),
(16, 32, 'f0QMrKEfioLCmPogwjZGDFR3jEHDKV0je80hKvbT', '2020-05-03 09:37:24', '2020-05-03 09:37:24'),
(17, 33, 'YYWKjPTWSJQ6YzxxW9wleJG2vE9hlQsklCkESCy4', '2020-05-05 15:04:08', '2020-05-05 15:04:08'),
(18, 34, 'trbQNk30ydDYEwBPJrj2VXiRIK1slZgmkozwMfa2', '2020-05-05 15:06:18', '2020-05-05 15:06:18'),
(19, 35, 'c5s9Q008bKpQZADbPDXKWjiMC7XlaBQqE9xgLgm3', '2020-05-05 15:07:38', '2020-05-05 15:07:38'),
(20, 36, 'YIfxTRw8jAz8t9TQuy3sSE9pDzQY3JB9sJvmIFnQ', '2020-05-05 15:08:24', '2020-05-05 15:08:24'),
(21, 37, 'CZpEoyjH4OF40y9knZ5zVS05GHnWZhBiGMZStQRg', '2020-05-05 15:09:06', '2020-05-05 15:09:06'),
(22, 38, 'XsQ4tOdteP8fVZqr2fbBhVsFXsopyqKMRlUugvlH', '2020-05-05 15:10:13', '2020-05-05 15:10:13'),
(23, 39, 'jmd4WTYx7d5tR5i5LlnWzFyOhqmdYXKuV864Dlj6', '2020-05-05 15:10:49', '2020-05-05 15:10:49'),
(24, 40, 'HMjFWufrzSWezRJLv8O4KtysKrvAmIb8Tu2Ud6jW', '2020-05-05 15:11:22', '2020-05-05 15:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_meta`
--
ALTER TABLE `category_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_setting`
--
ALTER TABLE `category_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shop_country_code_unique` (`code`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_product`
--
ALTER TABLE `coupon_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_user`
--
ALTER TABLE `coupon_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_reviews`
--
ALTER TABLE `customer_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_galleries`
--
ALTER TABLE `products_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_metas`
--
ALTER TABLE `products_metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tag`
--
ALTER TABLE `product_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `site_setting`
--
ALTER TABLE `site_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_meta`
--
ALTER TABLE `users_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artists`
--
ALTER TABLE `artists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `category_meta`
--
ALTER TABLE `category_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;

--
-- AUTO_INCREMENT for table `category_setting`
--
ALTER TABLE `category_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `coupon_product`
--
ALTER TABLE `coupon_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_user`
--
ALTER TABLE `coupon_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `customer_reviews`
--
ALTER TABLE `customer_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productimgtemp`
--
ALTER TABLE `productimgtemp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `products_galleries`
--
ALTER TABLE `products_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `products_metas`
--
ALTER TABLE `products_metas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2079;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `product_tag`
--
ALTER TABLE `product_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `product_variations`
--
ALTER TABLE `product_variations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `site_setting`
--
ALTER TABLE `site_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `users_meta`
--
ALTER TABLE `users_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
