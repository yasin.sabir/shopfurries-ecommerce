-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2020 at 12:52 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_variations`
--

CREATE TABLE `product_variations` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'on',
  `product_id` int(11) NOT NULL,
  `variation` varchar(500) DEFAULT NULL,
  `variation_meta` longtext,
  `image` longtext,
  `price` int(200) DEFAULT NULL,
  `sale_price` int(200) DEFAULT NULL,
  `stock` int(200) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `description` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_variations`
--

INSERT INTO `product_variations` (`id`, `status`, `product_id`, `variation`, `variation_meta`, `image`, `price`, `sale_price`, `stock`, `sku`, `description`, `created_at`, `updated_at`) VALUES
(5, 'on', 157, 'a:2:{i:0;s:9:\"color-red\";i:1;s:10:\"size-small\";}', 'a:2:{i:0;a:2:{s:9:\"attribute\";s:5:\"color\";s:9:\"variation\";s:3:\"red\";}i:1;a:2:{s:9:\"attribute\";s:4:\"size\";s:9:\"variation\";s:5:\"small\";}}', 'upload/product_variation/product_157/variation_5/thumbnail/1rmCnt5KWyiom9fnDtcOOJztbRhjH4LqTQJW79LC.jpeg', 120, NULL, 12, 'ds222-op97', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis.', '2020-05-12 18:31:22', '2020-05-17 14:26:58'),
(6, 'on', 157, 'a:2:{i:0;s:11:\"color-black\";i:1;s:11:\"size-medium\";}', 'a:2:{i:0;a:2:{s:9:\"attribute\";s:5:\"color\";s:9:\"variation\";s:5:\"black\";}i:1;a:2:{s:9:\"attribute\";s:4:\"size\";s:9:\"variation\";s:6:\"medium\";}}', 'upload/product_variation/product_157/variation_6/thumbnail/V4VyMZCfYJZZOLZGuXIU9ezr9YKeRKJnqK7AuUK3.jpeg', 100, NULL, 15, 'ds222-op97-A', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis.', '2020-05-12 18:54:32', '2020-05-17 14:27:12'),
(10, 'on', 157, 'a:2:{i:0;s:10:\"color-blue\";i:1;s:10:\"size-small\";}', 'a:2:{i:0;a:2:{s:9:\"attribute\";s:5:\"color\";s:9:\"variation\";s:4:\"blue\";}i:1;a:2:{s:9:\"attribute\";s:4:\"size\";s:9:\"variation\";s:5:\"small\";}}', NULL, 126, NULL, NULL, NULL, NULL, '2020-05-16 14:24:46', '2020-05-17 14:27:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_variations`
--
ALTER TABLE `product_variations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
