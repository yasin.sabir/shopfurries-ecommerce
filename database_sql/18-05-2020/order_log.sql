-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2020 at 12:51 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_shopfurries`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  `product_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_meta` longtext,
  `product_variation_id` int(11) DEFAULT NULL,
  `image` longtext,
  `user_meta` longtext,
  `price` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `material` varchar(200) DEFAULT NULL,
  `size` varchar(200) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  `extra_detail` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_log`
--

INSERT INTO `order_log` (`id`, `user_id`, `product_id`, `product_name`, `product_meta`, `product_variation_id`, `image`, `user_meta`, `price`, `qty`, `material`, `size`, `extra`, `extra_detail`, `created_at`, `updated_at`) VALUES
(14, '1', '157', 'cat-widget1', 'a:12:{s:2:\"id\";i:157;s:7:\"user_id\";i:1;s:5:\"title\";s:11:\"cat-widget1\";s:4:\"slug\";s:11:\"cat-widget1\";s:11:\"description\";s:604:\"<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique fermentum mi, sed consequat mi ornare quis. Fusce at pretium urna, quis pharetra elit. Curabitur sollicitudin pellentesque ex sit amet imperdiet. Aenean erat ante, accumsan venenatis tincidunt a, tincidunt sed leo. Morbi scelerisque tempus neque vel pretium. Vivamus ac erat cursus, commodo ligula et, eleifend nulla. Nulla sit amet volutpat turpis. Suspendisse pretium maximus sem, at fermentum velit viverra sodales.</span>\";s:3:\"sku\";N;s:5:\"image\";s:80:\"upload/product/cat-widget1/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg\";s:5:\"video\";N;s:5:\"price\";i:126;s:5:\"stock\";N;s:6:\"status\";i:1;s:12:\"variation_id\";s:1:\"5\";}', 5, 'upload/product/cat-widget1/feature/Stuo8KvsUOjIxVl9ZV0vG7PkSQ2HQsQTqd1JJxOt.jpeg', '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@admin.com\",\"email_verified_at\":null,\"verified\":1,\"status\":1,\"profile_pic\":null,\"provider_id\":null,\"provider\":null,\"stripe_customer_id\":\"cus_HD75mM8uUQk96X\",\"created_at\":\"2020-02-13 20:20:20\",\"updated_at\":\"2020-05-03 12:23:20\"}', '120', 1, 'Heavy Material', '240cm', 'Yes', 'a:4:{s:8:\"material\";s:14:\"Heavy Material\";s:4:\"size\";s:5:\"240cm\";s:5:\"extra\";s:3:\"Yes\";s:8:\"quantity\";s:1:\"1\";}', '2020-05-17 15:40:28', '2020-05-17 17:50:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
