jQuery(document).ready(function() {
   //     console.log('ahsan');

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    jQuery("#all_product_name").keyup(function(){
        jQuery(".product_name").val($(this).val())
    });
    jQuery("#all_product_desc").keyup(function(){
        jQuery(".description").val($(this).val())
    });
    jQuery("#all_product_price").keyup(function(){
        jQuery(".price").val($(this).val())
    });

        jQuery('.product_form').submit(function (e) {
       //  console.log('ahsan')
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            var form = $(this).serialize();
            var url = jQuery('#get_url').val();
            // console.log(form);
            // console.log(url)
            jQuery.ajax({
                url: url+'/Insert',
                method: 'post',
                data: form,
                success: function (result) {
                    alert(result.ok)
                    if(result.ok == "Product Insert Successfully") {
                         jQuery('.add_product_'+result.row).attr('disabled', true);
                        location.reload();
                        jQuery(".product_form :input").prop("disabled", true);
                    }
                }
            });
        });
    });
