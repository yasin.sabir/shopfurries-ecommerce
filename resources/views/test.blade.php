@extends('layouts.backend.app')

@section('section')

    <div class="content-wrapper" style="min-height: 1416.81px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-md-6">
                        <form action="{{route('test')}}" method="post">

                            <label for="">Text</label>
                            <input type="text" name="alert" id="">
                            @csrf
                            <input type="submit" value="Submit">

                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
