@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Event")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Event")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('event.edit' ,$event->id )}}">{{__("routes.Edit")}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Edit Section")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <div class="row mt-4">
                                    <div class="col-md-3 pl-5">
                                        <label for="CategoryThumbnail">{{__("routes.Status")}}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="hidden" id="event_id" value="{{$event->id}}">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch1" @if($event->status == "on" ) checked @else unchecked @endif >
                                                <label class="custom-control-label" for="customSwitch1"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <form action="{{route('event.update',$event->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="CategoryThumbnail">{{__("routes.Venue Image")}}:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="hidden" name="prev_event_img" value="{{$event->image}}">

                                                @if( $event->image != null)
                                                    <img src="{{asset('storage/'.$event->image)}}" id="thumbnail-tag"
                                                         width="100" height="100"/>
                                                @else
                                                    <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}" id="thumbnail-tag"
                                                         width="100" height="100"/>
                                                @endif


                                                <div class="custom-file mt-3">
                                                    <input type="file" name="event_img" class="custom-file-input"
                                                           id="thumbnail">
                                                    <label class="custom-file-label" for="customFile">{{__("routes.Choose Image")}}</label>
                                                </div>
                                                {{--<input type="file" name="file" id="profile-img">--}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistName">{{__("routes.Title")}} <span class="text-danger">*</span> :</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('event_name') is-invalid @enderror" value="{{$event->name}}" name="event_name" id="event_name">
                                                @error('event_name')
                                                <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">{{__("routes.Time")}}  <span class="text-danger">*</span>:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <div class="input-group date" id="timepicker" data-target-input="nearest">
                                                    <input type="text" value="{{$event->timing}}" name="event_datetime" class="form-control datetimepicker-input @error('event_datetime') is-invalid @enderror" data-target="#timepicker"/>
                                                    <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                                                    </div>
                                                </div>
                                                @error('event_datetime')
                                                <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">{{__("routes.Address")}}  <span class="text-danger">*</span>:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" value="{{$event->address}}" class="form-control @error('event_address') is-invalid @enderror" name="event_address" id="event_address">
                                                @error('event_address')
                                                <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">{{__("routes.City")}} <span class="text-danger">*</span>:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" value="{{$event->city}}" class="form-control @error('event_city') is-invalid @enderror" name="event_city" id="event_city">
                                                @error('event_city')
                                                <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">{{__("routes.State")}} :</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" value="@if(isset($event->state)) {{$event->state}} @endif" class="form-control" name="event_state" id="event_state">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">{{__("routes.Country")}} <span class="text-danger">*</span>:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control @error('event_country') is-invalid @enderror" name="event_country">
                                                    <option value="Afghanistan" @if($event->country == "Afghanistan" )selected @endif>Afghanistan</option>
                                                    <option value="Åland Islands" @if($event->country == "Åland Islands" )selected @endif>Åland Islands</option>
                                                    <option value="Albania" @if($event->country == "Albania" )selected @endif>Albania</option>
                                                    <option value="Algeria" @if($event->country == "Algeria" )selected @endif>Algeria</option>
                                                    <option value="American Samoa" @if($event->country == "American Samoa" )selected @endif>American Samoa</option>
                                                    <option value="Andorra" @if($event->country == "Andorra" )selected @endif>Andorra</option>
                                                    <option value="Angola" @if($event->country == "Angola" )selected @endif>Angola</option>
                                                    <option value="Anguilla" @if($event->country == "Anguilla" )selected @endif>Anguilla</option>
                                                    <option value="Antarctica" @if($event->country == "Antarctica" )selected @endif>Antarctica</option>
                                                    <option value="Antigua and Barbuda" @if($event->country == "Antigua and Barbuda" )selected @endif>Antigua and Barbuda</option>
                                                    <option value="Argentina" @if($event->country == "Argentina" )selected @endif>Argentina</option>
                                                    <option value="Armenia" @if($event->country == "Armenia" )selected @endif>Armenia</option>
                                                    <option value="Aruba" @if($event->country == "Aruba" )selected @endif>Aruba</option>
                                                    <option value="Australia" @if($event->country == "Australia" )selected @endif>Australia</option>
                                                    <option value="Austria" @if($event->country == "Austria" )selected @endif>Austria</option>
                                                    <option value="Azerbaijan" @if($event->country == "Azerbaijan" )selected @endif>Azerbaijan</option>
                                                    <option value="Bahamas" @if($event->country == "Bahamas" )selected @endif>Bahamas</option>
                                                    <option value="Bahrain" @if($event->country == "Bahrain" )selected @endif>Bahrain</option>
                                                    <option value="Bangladesh" @if($event->country == "Bangladesh" )selected @endif>Bangladesh</option>
                                                    <option value="Barbados" @if($event->country == "Barbados" )selected @endif>Barbados</option>
                                                    <option value="Belarus" @if($event->country == "Belarus" )selected @endif>Belarus</option>
                                                    <option value="Belgium" @if($event->country == "Belgium" )selected @endif>Belgium</option>
                                                    <option value="Belize" @if($event->country == "Belize" )selected @endif>Belize</option>
                                                    <option value="Benin" @if($event->country == "Benin" )selected @endif>Benin</option>
                                                    <option value="Bermuda" @if($event->country == "Bermuda" )selected @endif>Bermuda</option>
                                                    <option value="Bhutan" @if($event->country == "Bhutan" )selected @endif>Bhutan</option>
                                                    <option value="Bolivia" @if($event->country == "Bolivia" )selected @endif>Bolivia</option>
                                                    <option value="Bosnia and Herzegovina" @if($event->country == "Bosnia and Herzegovina" )selected @endif>Bosnia and Herzegovina</option>
                                                    <option value="Botswana" @if($event->country == "Botswana" )selected @endif>Botswana</option>
                                                    <option value="Bouvet Island" @if($event->country == "Bouvet Island" )selected @endif>Bouvet Island</option>
                                                    <option value="Brazil" @if($event->country == "Brazil" )selected @endif>Brazil</option>
                                                    <option value="British Indian Ocean Territory" @if($event->country == "British Indian Ocean Territory" )selected @endif>British Indian Ocean Territory</option>
                                                    <option value="Brunei Darussalam" @if($event->country == "Brunei Darussalam" )selected @endif>Brunei Darussalam</option>
                                                    <option value="Bulgaria" @if($event->country == "Bulgaria" )selected @endif>Bulgaria</option>
                                                    <option value="Burkina Faso" @if($event->country == "Burkina Faso" )selected @endif>Burkina Faso</option>
                                                    <option value="Burundi" @if($event->country == "Burundi" )selected @endif>Burundi</option>
                                                    <option value="Cambodia" @if($event->country == "Cambodia" )selected @endif>Cambodia</option>
                                                    <option value="Cameroon" @if($event->country == "Cameroon" )selected @endif>Cameroon</option>
                                                    <option value="Canada" @if($event->country == "Canada" )selected @endif>Canada</option>
                                                    <option value="Cape Verde" @if($event->country == "Cape Verde" )selected @endif>Cape Verde</option>
                                                    <option value="Cayman Islands" @if($event->country == "Cayman Islands" )selected @endif>Cayman Islands</option>
                                                    <option value="Central African Republic" @if($event->country == "Central African Republic" )selected @endif>Central African Republic</option>
                                                    <option value="Chad" @if($event->country == "Chad" )selected @endif>Chad</option>
                                                    <option value="Chile" @if($event->country == "Chile" )selected @endif>Chile</option>
                                                    <option value="China" @if($event->country == "China" )selected @endif>China</option>
                                                    <option value="Christmas Island" @if($event->country == "Christmas Island" )selected @endif>Christmas Island</option>
                                                    <option value="Cocos (Keeling) Islands" @if($event->country == "Cocos (Keeling) Islands" )selected @endif>Cocos (Keeling) Islands</option>
                                                    <option value="Colombia" @if($event->country == "Colombia" )selected @endif>Colombia</option>
                                                    <option value="Comoros" @if($event->country == "Comoros" )selected @endif>Comoros</option>
                                                    <option value="Congo" @if($event->country == "Congo" )selected @endif>Congo</option>
                                                    <option value="Congo, The Democratic Republic of The" @if($event->country == "Congo, The Democratic Republic of The" )selected @endif>Congo, The Democratic Republic of The</option>
                                                    <option value="Cook Islands" @if($event->country == "Cook Islands" )selected @endif>Cook Islands</option>
                                                    <option value="Costa Rica" @if($event->country == "Costa Rica" )selected @endif>Costa Rica</option>
                                                    <option value="Cote D'ivoire" @if($event->country == "Cote D'ivoire" )selected @endif>Cote D'ivoire</option>
                                                    <option value="Croatia" @if($event->country == "Croatia" )selected @endif>Croatia</option>
                                                    <option value="Cuba" @if($event->country == "Cuba" )selected @endif>Cuba</option>
                                                    <option value="Cyprus" @if($event->country == "Cyprus" )selected @endif>Cyprus</option>
                                                    <option value="Czech Republic" @if($event->country == "Czech Republic" )selected @endif>Czech Republic</option>
                                                    <option value="Denmark" @if($event->country == "Denmark" )selected @endif>Denmark</option>
                                                    <option value="Djibouti" @if($event->country == "Djibouti" )selected @endif>Djibouti</option>
                                                    <option value="Dominica" @if($event->country == "Dominica" )selected @endif>Dominica</option>
                                                    <option value="Dominican Republic" @if($event->country == "Dominican Republic" )selected @endif>Dominican Republic</option>
                                                    <option value="Ecuador" @if($event->country == "Ecuador" )selected @endif>Ecuador</option>
                                                    <option value="Egypt" @if($event->country == "Egypt" )selected @endif>Egypt</option>
                                                    <option value="El Salvador" @if($event->country == "El Salvador" )selected @endif>El Salvador</option>
                                                    <option value="Equatorial Guinea" @if($event->country == "Equatorial Guinea" )selected @endif>Equatorial Guinea</option>
                                                    <option value="Eritrea" @if($event->country == "Eritrea" )selected @endif>Eritrea</option>
                                                    <option value="Estonia" @if($event->country == "Estonia" )selected @endif>Estonia</option>
                                                    <option value="Ethiopia" @if($event->country == "Ethiopia" )selected @endif>Ethiopia</option>
                                                    <option value="Falkland Islands (Malvinas)" @if($event->country == "Falkland Islands (Malvinas)" )selected @endif>Falkland Islands (Malvinas)</option>
                                                    <option value="Faroe Islands" @if($event->country == "Faroe Islands" )selected @endif>Faroe Islands</option>
                                                    <option value="Fiji" @if($event->country == "Fiji" )selected @endif>Fiji</option>
                                                    <option value="Finland" @if($event->country == "Finland" )selected @endif>Finland</option>
                                                    <option value="France" @if($event->country == "France" )selected @endif>France</option>
                                                    <option value="French Guiana" @if($event->country == "French Guiana" )selected @endif>French Guiana</option>
                                                    <option value="French Polynesia" @if($event->country == "French Polynesia" )selected @endif>French Polynesia</option>
                                                    <option value="French Southern Territories" @if($event->country == "French Southern Territories" )selected @endif>French Southern Territories</option>
                                                    <option value="Gabon" @if($event->country == "Gabon" )selected @endif>Gabon</option>
                                                    <option value="Gambia" @if($event->country == "Gambia" )selected @endif>Gambia</option>
                                                    <option value="Georgia" @if($event->country == "Georgia" )selected @endif>Georgia</option>
                                                    <option value="Germany" @if($event->country == "Germany" )selected @endif>Germany</option>
                                                    <option value="Ghana" @if($event->country == "Ghana" )selected @endif>Ghana</option>
                                                    <option value="Gibraltar" @if($event->country == "Gibraltar" )selected @endif>Gibraltar</option>
                                                    <option value="Greece" @if($event->country == "Greece" )selected @endif>Greece</option>
                                                    <option value="Greenland" @if($event->country == "Greenland" )selected @endif>Greenland</option>
                                                    <option value="Grenada" @if($event->country == "Grenada" )selected @endif>Grenada</option>
                                                    <option value="Guadeloupe" @if($event->country == "Guadeloupe" )selected @endif>Guadeloupe</option>
                                                    <option value="Guam" @if($event->country == "Guam" )selected @endif>Guam</option>
                                                    <option value="Guatemala" @if($event->country == "Guatemala" )selected @endif>Guatemala</option>
                                                    <option value="Guernsey" @if($event->country == "Guernsey" )selected @endif>Guernsey</option>
                                                    <option value="Guinea" @if($event->country == "Guinea" )selected @endif>Guinea</option>
                                                    <option value="Guinea-bissau" @if($event->country == "Guinea-bissau" )selected @endif>Guinea-bissau</option>
                                                    <option value="Guyana" @if($event->country == "Guyana" )selected @endif>Guyana</option>
                                                    <option value="Haiti" @if($event->country == "Haiti" )selected @endif>Haiti</option>
                                                    <option value="Heard Island and Mcdonald Islands" @if($event->country == "Heard Island and Mcdonald Islands" )selected @endif>Heard Island and Mcdonald Islands</option>
                                                    <option value="Holy See (Vatican City State)" @if($event->country == "Holy See (Vatican City State)" )selected @endif>Holy See (Vatican City State)</option>
                                                    <option value="Honduras" @if($event->country == "Honduras" )selected @endif>Honduras</option>
                                                    <option value="Hong Kong" @if($event->country == "Hong Kong" )selected @endif>Hong Kong</option>
                                                    <option value="Hungary" @if($event->country == "Hungary" )selected @endif>Hungary</option>
                                                    <option value="Iceland" @if($event->country == "Iceland" )selected @endif>Iceland</option>
                                                    <option value="India" @if($event->country == "India" )selected @endif>India</option>
                                                    <option value="Indonesia" @if($event->country == "Indonesia" )selected @endif>Indonesia</option>
                                                    <option value="Iran" @if($event->country == "Iran" )selected @endif>Iran, Islamic Republic of</option>
                                                    <option value="Iraq" @if($event->country == "Iraq" )selected @endif>Iraq</option>
                                                    <option value="Ireland" @if($event->country == "Ireland" )selected @endif>Ireland</option>
                                                    <option value="Isle of Man" @if($event->country == "Isle of Man" )selected @endif>Isle of Man</option>
                                                    <option value="Israel" @if($event->country == "Israel" )selected @endif>Israel</option>
                                                    <option value="Italy" @if($event->country == "Italy" )selected @endif>Italy</option>
                                                    <option value="Jamaica" @if($event->country == "Jamaica" )selected @endif>Jamaica</option>
                                                    <option value="Japan" @if($event->country == "Japan" )selected @endif>Japan</option>
                                                    <option value="Jersey" @if($event->country == "Jersey" )selected @endif>Jersey</option>
                                                    <option value="Jordan" @if($event->country == "Jordan" )selected @endif>Jordan</option>
                                                    <option value="Kazakhstan" @if($event->country == "Kazakhstan" )selected @endif>Kazakhstan</option>
                                                    <option value="Kenya" @if($event->country == "Kenya" )selected @endif>Kenya</option>
                                                    <option value="Kiribati" @if($event->country == "Kiribati" )selected @endif>Kiribati</option>
                                                    <option value="Korea" @if($event->country == "Korea" )selected @endif>Korea</option>
                                                    <option value="Kuwait" @if($event->country == "Kuwait" )selected @endif>Kuwait</option>
                                                    <option value="Kyrgyzstan" @if($event->country == "Kyrgyzstan" )selected @endif>Kyrgyzstan</option>
                                                    <option value="Lao People's Democratic Republic" @if($event->country == "Lao People's Democratic Republic" )selected @endif>Lao People's Democratic Republic</option>
                                                    <option value="Latvia" @if($event->country == "Latvia" )selected @endif>Latvia</option>
                                                    <option value="Lebanon" @if($event->country == "Lebanon" )selected @endif>Lebanon</option>
                                                    <option value="Lesotho" @if($event->country == "Lesotho" )selected @endif>Lesotho</option>
                                                    <option value="Liberia" @if($event->country == "Liberia" )selected @endif>Liberia</option>
                                                    <option value="Libyan Arab Jamahiriya" @if($event->country == "Libyan Arab Jamahiriya" )selected @endif>Libyan Arab Jamahiriya</option>
                                                    <option value="Liechtenstein" @if($event->country == "Liechtenstein" )selected @endif>Liechtenstein</option>
                                                    <option value="Lithuania" @if($event->country == "Lithuania" )selected @endif>Lithuania</option>
                                                    <option value="Luxembourg" @if($event->country == "Luxembourg" )selected @endif>Luxembourg</option>
                                                    <option value="Macao" @if($event->country == "Macao" )selected @endif>Macao</option>
                                                    <option value="Macedonia" @if($event->country == "Macedonia" )selected @endif>Macedonia</option>
                                                    <option value="Madagascar" @if($event->country == "Madagascar" )selected @endif>Madagascar</option>
                                                    <option value="Malawi" @if($event->country == "Malawi" )selected @endif>Malawi</option>
                                                    <option value="Malaysia" @if($event->country == "Malaysia" )selected @endif>Malaysia</option>
                                                    <option value="Maldives" @if($event->country == "Maldives" )selected @endif>Maldives</option>
                                                    <option value="Mali" @if($event->country == "Mali" )selected @endif>Mali</option>
                                                    <option value="Malta" @if($event->country == "Malta" )selected @endif>Malta</option>
                                                    <option value="Marshall Islands" @if($event->country == "Marshall Islands" )selected @endif>Marshall Islands</option>
                                                    <option value="Martinique" @if($event->country == "Martinique" )selected @endif>Martinique</option>
                                                    <option value="Mauritania" @if($event->country == "Mauritania" )selected @endif>Mauritania</option>
                                                    <option value="Mauritius" @if($event->country == "Mauritius" )selected @endif>Mauritius</option>
                                                    <option value="Mayotte" @if($event->country == "Mayotte" )selected @endif>Mayotte</option>
                                                    <option value="Mexico" @if($event->country == "Mexico" )selected @endif>Mexico</option>
                                                    <option value="Micronesia" @if($event->country == "Micronesia" )selected @endif>Micronesia</option>
                                                    <option value="Moldova" @if($event->country == "Moldova" )selected @endif>Moldova, Republic of</option>
                                                    <option value="Monaco" @if($event->country == "Monaco" )selected @endif>Monaco</option>
                                                    <option value="Mongolia" @if($event->country == "Mongolia" )selected @endif>Mongolia</option>
                                                    <option value="Montenegro" @if($event->country == "Montenegro" )selected @endif>Montenegro</option>
                                                    <option value="Montserrat" @if($event->country == "Montserrat" )selected @endif>Montserrat</option>
                                                    <option value="Morocco" @if($event->country == "Morocco" )selected @endif>Morocco</option>
                                                    <option value="Mozambique" @if($event->country == "Mozambique" )selected @endif>Mozambique</option>
                                                    <option value="Myanmar" @if($event->country == "Myanmar" )selected @endif>Myanmar</option>
                                                    <option value="Namibia" @if($event->country == "Namibia" )selected @endif>Namibia</option>
                                                    <option value="Nauru" @if($event->country == "Nauru" )selected @endif>Nauru</option>
                                                    <option value="Nepal" @if($event->country == "Nepal" )selected @endif>Nepal</option>
                                                    <option value="Netherlands" @if($event->country == "Netherlands" )selected @endif>Netherlands</option>
                                                    <option value="Netherlands Antilles" @if($event->country == "Netherlands Antilles" )selected @endif>Netherlands Antilles</option>
                                                    <option value="New Caledonia" @if($event->country == "New Caledonia" )selected @endif>New Caledonia</option>
                                                    <option value="New Zealand" @if($event->country == "New Zealand" )selected @endif>New Zealand</option>
                                                    <option value="Nicaragua" @if($event->country == "Nicaragua" )selected @endif>Nicaragua</option>
                                                    <option value="Niger" @if($event->country == "Niger" )selected @endif>Niger</option>
                                                    <option value="Nigeria" @if($event->country == "Nigeria" )selected @endif>Nigeria</option>
                                                    <option value="Niue" @if($event->country == "Niue" )selected @endif>Niue</option>
                                                    <option value="Norfolk Island" @if($event->country == "Norfolk Island" )selected @endif>Norfolk Island</option>
                                                    <option value="Northern Mariana Islands" @if($event->country == "Northern Mariana Islands" )selected @endif>Northern Mariana Islands</option>
                                                    <option value="Norway" @if($event->country == "Norway" )selected @endif>Norway</option>
                                                    <option value="Oman" @if($event->country == "Oman" )selected @endif>Oman</option>
                                                    <option value="Pakistan" @if($event->country == "Pakistan" )selected @endif>Pakistan</option>
                                                    <option value="Palau" @if($event->country == "Palau" )selected @endif>Palau</option>
                                                    <option value="Palestinian" @if($event->country == "Palestinian" )selected @endif>Palestinian Territory, Occupied</option>
                                                    <option value="Panama" @if($event->country == "Panama" )selected @endif>Panama</option>
                                                    <option value="Papua New Guinea" @if($event->country == "Papua New Guinea" )selected @endif>Papua New Guinea</option>
                                                    <option value="Paraguay" @if($event->country == "Paraguay" )selected @endif>Paraguay</option>
                                                    <option value="Peru" @if($event->country == "Peru" )selected @endif>Peru</option>
                                                    <option value="Philippines" @if($event->country == "Philippines" )selected @endif>Philippines</option>
                                                    <option value="Pitcairn" @if($event->country == "Pitcairn" )selected @endif>Pitcairn</option>
                                                    <option value="Poland" @if($event->country == "Poland" )selected @endif>Poland</option>
                                                    <option value="Portugal" @if($event->country == "Portugal" )selected @endif>Portugal</option>
                                                    <option value="Puerto Rico" @if($event->country == "Puerto Rico" )selected @endif>Puerto Rico</option>
                                                    <option value="Qatar" @if($event->country == "Qatar" )selected @endif>Qatar</option>
                                                    <option value="Reunion" @if($event->country == "Reunion" )selected @endif>Reunion</option>
                                                    <option value="Romania" @if($event->country == "Romania" )selected @endif>Romania</option>
                                                    <option value="Russian Federation" @if($event->country == "Russian Federation" )selected @endif>Russian Federation</option>
                                                    <option value="Rwanda" @if($event->country == "Rwanda" )selected @endif>Rwanda</option>
                                                    <option value="Saint Helena" @if($event->country == "Saint Helena" )selected @endif>Saint Helena</option>
                                                    <option value="Saint Kitts and Nevis" @if($event->country == "Saint Kitts and Nevis" )selected @endif>Saint Kitts and Nevis</option>
                                                    <option value="Saint Lucia" @if($event->country == "Saint Lucia" )selected @endif>Saint Lucia</option>
                                                    <option value="Saint Pierre and Miquelon" @if($event->country == "Saint Pierre and Miquelon" )selected @endif>Saint Pierre and Miquelon</option>
                                                    <option value="Saint Vincent and The Grenadines" @if($event->country == "Saint Vincent and The Grenadines" )selected @endif>Saint Vincent and The Grenadines</option>
                                                    <option value="Samoa" @if($event->country == "Samoa" )selected @endif>Samoa</option>
                                                    <option value="San Marino" @if($event->country == "San Marino" )selected @endif>San Marino</option>
                                                    <option value="Sao Tome and Principe" @if($event->country == "Sao Tome and Principe" )selected @endif>Sao Tome and Principe</option>
                                                    <option value="Saudi Arabia" @if($event->country == "Saudi Arabia" )selected @endif>Saudi Arabia</option>
                                                    <option value="Senegal" @if($event->country == "Senegal" )selected @endif>Senegal</option>
                                                    <option value="Serbia" @if($event->country == "Serbia" )selected @endif>Serbia</option>
                                                    <option value="Seychelles" @if($event->country == "Seychelles" )selected @endif>Seychelles</option>
                                                    <option value="Sierra Leone" @if($event->country == "Sierra Leone" )selected @endif>Sierra Leone</option>
                                                    <option value="Singapore" @if($event->country == "Singapore" )selected @endif>Singapore</option>
                                                    <option value="Slovakia" @if($event->country == "Slovakia" )selected @endif>Slovakia</option>
                                                    <option value="Slovenia" @if($event->country == "Slovenia" )selected @endif>Slovenia</option>
                                                    <option value="Solomon Islands" @if($event->country == "Solomon Islands" )selected @endif>Solomon Islands</option>
                                                    <option value="Somalia" @if($event->country == "Somalia" )selected @endif>Somalia</option>
                                                    <option value="South Africa" @if($event->country == "South Africa" )selected @endif>South Africa</option>
                                                    <option value="South Georgia and The South Sandwich Islands" @if($event->country == "South Georgia and The South Sandwich Islands" )selected @endif>South Georgia and The South Sandwich
                                                        Islands</option>
                                                    <option value="Spain" @if($event->country == "Spain" )selected @endif>Spain</option>
                                                    <option value="Sri Lanka" @if($event->country == "Sri Lanka" )selected @endif>Sri Lanka</option>
                                                    <option value="Sudan" @if($event->country == "Sudan" )selected @endif>Sudan</option>
                                                    <option value="Suriname" @if($event->country == "Suriname" )selected @endif>Suriname</option>
                                                    <option value="Svalbard and Jan Mayen" @if($event->country == "Svalbard and Jan Mayen" )selected @endif>Svalbard and Jan Mayen</option>
                                                    <option value="Swaziland" @if($event->country == "Swaziland" )selected @endif>Swaziland</option>
                                                    <option value="Sweden" @if($event->country == "Sweden" )selected @endif>Sweden</option>
                                                    <option value="Switzerland" @if($event->country == "Switzerland" )selected @endif>Switzerland</option>
                                                    <option value="Syria" @if($event->country == "Syria" )selected @endif>Syrian Arab Republic</option>
                                                    <option value="Taiwan" @if($event->country == "Taiwan" )selected @endif>Taiwan, Province of China</option>
                                                    <option value="Tajikistan" @if($event->country == "Tajikistan" )selected @endif>Tajikistan</option>
                                                    <option value="Tanzania" @if($event->country == "Tanzania" )selected @endif>Tanzania, United Republic of</option>
                                                    <option value="Thailand" @if($event->country == "Thailand" )selected @endif>Thailand</option>
                                                    <option value="Timor-leste" @if($event->country == "Timor-leste" )selected @endif>Timor-leste</option>
                                                    <option value="Togo" @if($event->country == "Togo" )selected @endif>Togo</option>
                                                    <option value="Tokelau" @if($event->country == "Tokelau" )selected @endif>Tokelau</option>
                                                    <option value="Tonga" @if($event->country == "Tonga" )selected @endif>Tonga</option>
                                                    <option value="Trinidad and Tobago" @if($event->country == "Trinidad and Tobago" )selected @endif>Trinidad and Tobago</option>
                                                    <option value="Tunisia" @if($event->country == "Tunisia" )selected @endif>Tunisia</option>
                                                    <option value="Turkey" @if($event->country == "Turkey" )selected @endif>Turkey</option>
                                                    <option value="Turkmenistan" @if($event->country == "Turkmenistan" )selected @endif>Turkmenistan</option>
                                                    <option value="Turks and Caicos Islands" @if($event->country == "Turks and Caicos Islands" )selected @endif>Turks and Caicos Islands</option>
                                                    <option value="Tuvalu" @if($event->country == "Tuvalu" )selected @endif>Tuvalu</option>
                                                    <option value="Uganda" @if($event->country == "Uganda" )selected @endif>Uganda</option>
                                                    <option value="Ukraine" @if($event->country == "Ukraine" )selected @endif>Ukraine</option>
                                                    <option value="United Arab Emirates" @if($event->country == "United Arab Emirates" )selected @endif>United Arab Emirates</option>
                                                    <option value="United Kingdom" @if($event->country == "United Kingdom" )selected @endif>United Kingdom</option>
                                                    <option value="United States" @if($event->country === "United States" )selected @endif>United States</option>
                                                    <option value="United States Minor Outlying Islands" @if($event->country == "United States Minor Outlying Islands" )selected @endif>United States Minor Outlying Islands</option>
                                                    <option value="Uruguay" @if($event->country == "Uruguay" )selected @endif>Uruguay</option>
                                                    <option value="Uzbekistan" @if($event->country == "Uzbekistan" )selected @endif>Uzbekistan</option>
                                                    <option value="Vanuatu" @if($event->country == "Vanuatu" )selected @endif>Vanuatu</option>
                                                    <option value="Venezuela" @if($event->country == "Venezuela" )selected @endif>Venezuela</option>
                                                    <option value="Vietnam" @if($event->country == "Vietnam" )selected @endif>Viet Nam</option>
                                                    <option value="Virgin Islands, British" @if($event->country == "Virgin Islands, British" )selected @endif>Virgin Islands, British</option>
                                                    <option value="Virgin Islands, U.S." @if($event->country == "Virgin Islands, U.S." )selected @endif>Virgin Islands, U.S.</option>
                                                    <option value="Wallis and Futuna" @if($event->country == "Wallis and Futuna" )selected @endif>Wallis and Futuna</option>
                                                    <option value="Western Sahara" @if($event->country == "Western Sahara" )selected @endif>Western Sahara</option>
                                                    <option value="Yemen" @if($event->country == "Western Sahara" )selected @endif>Yemen</option>
                                                    <option value="Zambia" @if($event->country == "Zambia" )selected @endif>Zambia</option>
                                                    <option value="Zimbabwe" @if($event->country == "Zimbabwe") selected @endif>Zimbabwe</option>
                                                </select>
                                                @error('event_country')
                                                <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">{{__("routes.Details (Optional)")}}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                    <textarea id="textarea2"
                                              class="" name="event_details"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$event->details}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 text-left">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary" value="{{__("routes.Update")}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>



            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================

        var status = "";

        // input[type="checkbox"]
        $('#customSwitch1').click(function(){
            var customSwitch1 = $(this);
            if($(this).prop("checked") === true){
                console.log("checked");
                status = "on";

                $.ajax({
                    url:"{{route('event.status-update',$event->id)}}",
                    type: 'post',
                    data: {status : status},
                    success:function(response){
                        //console.log(response+" - Mode is on");
                        toastr.success('Event is active!', 'Event Status Alert!');
                    }
                });
            }
            else if($(this).prop("checked") === false){
                console.log("Unchecked");
                status = "off";
                $.ajax({
                    url:"{{route('event.status-update',$event->id)}}",
                    type: 'post',
                    data: {status : status},
                    success:function(response){
                        //console.log(response+" - Mode is off");
                        toastr.error('Event is deactivated!', 'Event Status Alert!');
                    }
                });
            }
        });

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

        $('#textarea2').summernote({
            height: 200,
            toolbar: false,
        });


        //=======================================================================================================

        //Timepicker
        $(document).ready(function(){
            $('#timepicker').date({
                format: 'LT'
            });
        });

        //=======================================================================================================

    </script>

@endsection
