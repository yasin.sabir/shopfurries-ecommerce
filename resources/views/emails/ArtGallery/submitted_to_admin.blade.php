@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <?php
            $appURL = env('APP_URL');
            ?>

            <img src="{{URL::to('front-end/assets/img/ShopFurries-LOGO.png')}}"  alt="" style="width: 100px;
            height: 95px;
            margin: 0 auto;
           " />
        @endcomponent
    @endslot

    @slot('subcopy')

        <table style="padding-left: 30px;display: flex !important;justify-content: center !important;background-color: #3490DC;box-shadow: 0 2px 3px #dedede !important;">
            <thead>
            <tr style="text-align: center !important;">
                <th colspan="2">
                    <h3 style="font-size: 20px;color: #fff;margin: 10px 0px;">  ShopFurries - Art Submission </h3>
                </th>
            </tr>
            </thead>
        </table>

        @component('mail::subcopy')
        @endcomponent

        <h1>Hello! Admin,</h1>

        <table>
            <tbody>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td colspan="2" style="font-size: 15px;"><strong>{{ucfirst($data["user_detail"]["name"])}}</strong> submitted art work</td></tr>
            <tr><td style="font-size: 15px;">Below User details:</td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td style="width: 30%;font-size: 15px;font-weight: 500;">Email:</td> <td style="font-size: 15px;"> {{$data['user_detail']['email']}}</td></tr>
            </tbody>
        </table>

        <table style="margin-top:20px;width: 100%;border-collapse: collapse;">
            <thead style="border: 1px solid #ededed;">
            <tr style="border: 1px solid #ededed;">
                <th style="border: 1px solid #ededed;">
                    No.
                </th>
                <th style="border: 1px solid #ededed;">
                    Title
                </th>
                <th style="border: 1px solid #ededed;">
                    Image
                </th>
            </tr>
            </thead>
            <tbody style="text-align: center;">
            @php $i = 1; @endphp
            @forelse($data["submitted_artWork"] as $key => $val)
                <tr>
                    <td style="border: 1px solid #ededed;">{{$i}}</td>
                    <td style="border: 1px solid #ededed;">{{ucfirst($val['title'])}}</td>
                    <td style="border: 1px solid #ededed;">
                        <img src="{{asset("storage/".$val['image'])}}"  alt="" style="width: 100px;
                        height: 95px;
                        margin: 0 auto;
                       " />
                    </td>
                </tr>
                @php
                    $i++;
                @endphp
            @empty

            @endforelse
            </tbody>
        </table>

    @endslot


    {{-- Footer --}}

    @slot('footer')
        @component('mail::footer')
            <table >
                <tr>
                    <td colspan="4" style="color: #aeaeae;">
                        &copy; {{ date('Y') }}  ShopFurries. All rights reserved.
                    </td>
                </tr>
            </table>
        @endcomponent
    @endslot

@endcomponent

<style>

    table tr,
    thead tr th ,
    tbody tr td ,
    table tr td {
        font-family: 'Roboto', sans-serif !important;
    }

</style>
