@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <?php
            $appURL = env('APP_URL');
            ?>

            <img src="{{URL::to('front-end/assets/img/ShopFurries-LOGO.png')}}"  alt="" style="width: 100px;
            height: 95px;
            margin: 0 auto;
           " />
        @endcomponent
    @endslot

    @slot('subcopy')

        <table style="padding-left: 30px;display: flex !important;justify-content: center !important;background-color: #3490DC;box-shadow: 0 2px 3px #dedede !important;">
            <thead>
            <tr style="text-align: center !important;">
                <th colspan="2">
                    <h3 style="font-size: 20px;color: #fff;margin: 10px 0px;">  ShopFurries - Art Submission Approved </h3>
                </th>
            </tr>
            </thead>
        </table>

        @component('mail::subcopy')
        @endcomponent

        <h1>Hello! {{ucwords($data['user_detail']['name'])}},</h1>

        <table>
            <tbody>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td colspan="2" style="font-size: 15px;">Congratulation!</td></tr>
            <tr><td colspan="2" style="font-size: 15px;"> Your "{{ucwords($data['submitted_artWork']['title'])}}" Has been Approved.</td></tr>
            <tr><td style="font-size: 15px;">Below your details:</td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            </tbody>
        </table>

        <table style="margin-top:20px;width: 100%;border-collapse: collapse;">
            <thead style="border: 1px solid #ededed;">
            <tr style="border: 1px solid #ededed;">
                <th style="border: 1px solid #ededed;">
                    No.
                </th>
                <th style="border: 1px solid #ededed;">
                    Title
                </th>
                <th style="border: 1px solid #ededed;">
                    Image
                </th>
            </tr>
            </thead>
            <tbody style="text-align: center;">
            <tr>
                <td style="border: 1px solid #ededed;">1.</td>
                <td style="border: 1px solid #ededed;">{{ucwords($data['submitted_artWork']['title'])}}</td>
                <td style="border: 1px solid #ededed;">
                    <img src="{{asset("storage/".$data['submitted_artWork']['image'])}}"  alt="" style="width: 100px;
                        height: 95px;
                        margin: 0 auto;
                       " />
                </td>
            </tr>
            </tbody>
        </table>

    @endslot


    {{-- Footer --}}

    @slot('footer')
        @component('mail::footer')
            <table >
                <tr>
                    <td colspan="4" style="color: #aeaeae;">
                        &copy; {{ date('Y') }}  ShopFurries. All rights reserved.
                    </td>
                </tr>
            </table>
        @endcomponent
    @endslot

@endcomponent

<style>

    table tr,
    thead tr th ,
    tbody tr td ,
    table tr td {
        font-family: 'Roboto', sans-serif !important;
    }

</style>
