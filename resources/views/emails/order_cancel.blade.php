@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{asset('images/logo/ShopFurries-Logo.png')}}" alt="" class="light-logo" style="width: 100px;
            height: 80px;
            margin: 0 auto;
            margin-bottom: 20px;" />
        @endcomponent
    @endslot

    @slot('subcopy')

        <table style="padding-left: 30px;display: flex !important;justify-content: center !important;background-color: #3490DC;box-shadow: 0 2px 3px #dedede !important;">
            <thead>
            <tr style="text-align: center">
                <th colspan="2">
                    <h3 style="font-size: 20px;color: #fff;margin: 10px 0px;"> ShopFurries - Order Cancellation </h3>
                </th>
            </tr>
            </thead>
        </table>

        @component('mail::subcopy')

            <table style="width:100%;background:#f8d7da">
                <tr>
                    <td colspan="4" style="padding-top: 10px; text-align: center">
                        <h2>Order Cancellation</h2>
                        <p style="text-align: center">Order ID : ( {{$order['order_id']}} ) </p>
                        <b><p style="text-align: center">{{$order_details['name']}} ({{$order_details['email']}}), Thank You for your order!</p></b>
                        <b><p style="text-align: center">Your order has been cancelled. Please retain this cancellataion information for yout record</p></b>
                    </td>
                </tr>
            </table>
        @endcomponent

        @component('mail::subcopy')
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Product Title</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                </tr>
                </thead>
                <tbody>

                @php
                    $num=1;
                    $subtotal=[];
                @endphp
                @forelse($product_detail as $data)
                    <tr>
                        <td>{{($data['title'])}}</td>
                        <td><img style="width:120px;height:60px;" src="{{asset('storage/'.$data['image'])}}"></td>
                        <td class="text-center">${{($data['price'])}}</td>
                        <td></td>
                        <td class="text-center">${{($data['price'])}}</td>
                    </tr>
                    @php
                        $num++;
                        $subtotal[] =  $data['price'];
                    @endphp

                @empty
                    <tr>
                        <td colspan="5" class="text-center">No Coupon Found</td>
                    </tr>
                @endforelse
                @php
                    $subtotal = array_sum($subtotal);
                @endphp
                <tr>
                    <td colspan="4" class="text-center"></td>
                    <td colspan="1" class="text-center">${{($subtotal)}}</td>
                </tr>

                </tbody>

            </table>
        @endcomponent

        @component('mail::subcopy')
            <table style="width:100%">
                <tr><td colspan="2" style="text-align: center"><h2 style="text-align: center">Order Summary </h2></td></tr>
                <tr><td colspan="2"><p style="text-align: center">May 07, 2020</p></td></tr>
                <tr><th>Subtotal Price :   </th><td> ${{($subtotal)}}</td></tr>
                <tr><th>Discount       :   </th><td>$0</td></tr>
                <tr><td colspan="2">==============================================</td></tr>
                <tr><th style="font-weight: bold;">Total Price :   </th><td style="font-weight: bold;color:red;">${{($subtotal)}}</td></tr>
                <tr><td colspan="2">==============================================</td></tr>

            </table>
        @endcomponent

    @endslot

    {{-- Footer --}}

    @slot('footer')
        @component('mail::footer')
            <table >
                <tr>
                    <td colspan="4" style="padding-top: 10px;">
                        &copy; {{ date('Y') }}  ShopFurries. All rights reserved.
                    </td>
                </tr>
            </table>
        @endcomponent
    @endslot

@endcomponent

<style>
    .social-links {
        text-align: center;
    }

    .social-links td img{
        width: 30px;
    }
    td.panel-content {
        padding: 0 !important;

        background-color: transparent !important;

    }
    .header a div{
        width:100%;
        padding: 15px !important;
        color: #3c6daf;
    }
    .header a{
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
        width:100%
    }
    table.footer ,
    td.header {
        width: 100% !important;
        background: #ffffff;
    }
    td.header {
        padding: 0px !important;
        padding-top: 15px !important;

        border-bottom: 2px solid #3c6daf;

    }

    table.footer .content-cell {
        padding: 20px !important;
        background-color: #4f93ef;
        color: #fff;
    }
    table.footer,
    table.content {
        width: 100%;
        max-width: 500px;
    }

    tr td table.footer {
        max-width: 570px !important;
    }



    table.footer p,
    td.header a {
        color: #3c6daf !important;
        text-shadow: unset !important;
    }
    table.subcopy th,
    table.subcopy td {
        padding: 5px;
        text-align: left;
    }

    td.header, td.header a {
        background: #3c8efe;
        padding-top: 20px;
    }
    td.header{
        padding-top: 0px !Important
    }
    .content-cell h1 {
        font-size: 13px !Important;
        font-weight: 600 !Important;
        margin-top: 0 !Important;
        text-align: left !Important;
    }
    .content-cell h2 {
        color: #ffffff !Important;
        background: #FF9800 !Important;
        padding: 8px !Important;
        text-align: center !Important;
    }

</style>
