@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <?php
            $appURL = env('APP_URL');
            ?>

            <img src="{{URL::to('front-end/assets/img/ShopFurries-LOGO.png')}}"  alt="" style="width: 100px;
            height: 95px;
            margin: 0 auto;
           " />
        @endcomponent
    @endslot

    @slot('subcopy')

        <table style="padding-left: 30px;display: flex !important;justify-content: center !important;background-color: #3490DC;box-shadow: 0 2px 3px #dedede !important;">
            <thead>
            <tr style="text-align: center">
                <th colspan="2">
                    <h3 style="font-size: 20px;color: #fff;margin: 10px 0px;"> ShopFurries - Product Submitted Mail </h3>
                </th>
            </tr>
            </thead>
        </table>

        @component('mail::subcopy')
        @endcomponent

        <h1>Hello! Admin,</h1>
        <table>
            <tbody>
                <tr><td></td></tr>
                <tr><td></td></tr>
                <tr><td colspan="2" style="font-size: 15px;">User "{{ucwords($detail_send_to_Admin[1]->name)}}" submitted their product in our website</td></tr>
                <tr><td style="font-size: 15px;">User Details:</td></tr>
                <tr><td style="font-size: 15px;"></td></tr>
                <tr><td style="font-size: 15px;"></td></tr>
                <tr><td style="font-size: 15px;">Name:</td><td>{{ucwords($detail_send_to_Admin[1]->name)}}</td></tr>
                <tr><td style="font-size: 15px;">Email:</td><td>{{$detail_send_to_Admin[1]->email}}</td></tr>
                <tr><td></td></tr>
                <tr><td></td></tr>
                <tr><td></td></tr>
                <tr><td style="font-size: 15px;">User Product Detail:</td></tr>
            </tbody>
        </table>

        @component('mail::subcopy')
            <table style="width: 100%;border-collapse: collapse;">
                <tbody>
                <tr style="background-color: #f2f2f2;border: 1px solid #ddd;">
                    <td style="padding: 8px;border: 1px solid #ddd;">1</td>
                    <td style="padding: 8px;border: 1px solid #ddd;">Feature Image</td>
                    <td style="padding: 8px;border: 1px solid #ddd;"><img src="{{asset('storage/'.$detail_send_to_Admin[1]->image)}}"  alt="" style="width: 70px;height: 70px;" /></td>
                </tr>
                <tr style="border: 1px solid #ddd;">
                    <td style="padding: 8px;border: 1px solid #ddd;">2</td>
                    <td style="padding: 8px;border: 1px solid #ddd;">Title</td>
                    <td style="padding: 8px;border: 1px solid #ddd;">{{ucwords($detail_send_to_Admin[2]->title)}}</td>
                </tr>
                <tr style="background-color: #f2f2f2;border: 1px solid #ddd;">
                    <td style="padding: 8px;border: 1px solid #ddd;">3</td>
                    <td style="padding: 8px;border: 1px solid #ddd;">Price</td>
                    <td style="padding: 8px;border: 1px solid #ddd;">{{"$".$detail_send_to_Admin[2]->price.""}}</td>
                </tr>
                <tr style="border: 1px solid #ddd;">
                    <td style="padding: 8px;border: 1px solid #ddd;">4</td>
                    <td style="padding: 8px;border: 1px solid #ddd;">Stock</td>
                    <td style="padding: 8px;border: 1px solid #ddd;">{{$detail_send_to_Admin[2]->stock}}</td>
                </tr>
                </tbody>
            </table>

            {{--            <table style="width: 100%;border-collapse: collapse;">--}}
            {{--                <tbody>--}}
            {{--                    <tr style="background-color: #f2f2f2;border: 1px solid #ddd;">--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">1</td>--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">Feature Image</td>--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;"><img src="{{URL::to('front-end/assets/img/ShopFurries-LOGO.png')}}"  alt="" style="width: 70px;height: 70px;" /></td>--}}
            {{--                    </tr>--}}
            {{--                    <tr style="border: 1px solid #ddd;">--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">2</td>--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">Title</td>--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">Product -001 </td>--}}
            {{--                    </tr>--}}
            {{--                    <tr style="background-color: #f2f2f2;border: 1px solid #ddd;">--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">3</td>--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">Price</td>--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;"> $45.00</td>--}}
            {{--                    </tr>--}}
            {{--                    <tr style="border: 1px solid #ddd;">--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">4</td>--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">Stock</td>--}}
            {{--                        <td style="padding: 8px;border: 1px solid #ddd;">34</td>--}}
            {{--                    </tr>--}}
            {{--                </tbody>--}}
            {{--            </table>--}}

        @endcomponent


    @endslot


    {{-- Footer --}}

    @slot('footer')
        @component('mail::footer')
            <table >
                <tr>
                    <td colspan="4" style="color: #aeaeae;">
                        &copy; {{ date('Y') }}  ShopFurries. All rights reserved.
                    </td>
                </tr>
            </table>
        @endcomponent
    @endslot

@endcomponent

<style>

    table tr,
    thead tr th ,
    tbody tr td ,
    table tr td {
        font-family: 'Roboto', sans-serif !important;
    }

</style>
