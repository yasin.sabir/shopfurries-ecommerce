@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])

        <img src="{{URL::to('front-end/assets/img/ShopFurries-LOGO.png')}}"  alt="" style="width: 100px;
            height: 95px;
            margin: 0 auto;
           " />
    @endcomponent
  @endslot


@slot('subcopy')

    <table style="padding-left: 30px;display: flex !important;justify-content: center !important;background-color: #3490DC;box-shadow: 0 2px 3px #dedede !important;">
        <thead>
        <tr style="text-align: center">
            <th colspan="2">
                <h3 style="font-size: 20px;color: #fff;margin: 10px 0px;"> ShopFurries - Verify your email address </h3>
            </th>
        </tr>
        </thead>
    </table>

    @component('mail::subcopy')
    @endcomponent

    <h1>Hello!</h1>
    <table>
        <tbody>
        <tr><td>Please click the button below to verify your email address.</td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td style="text-align: center;padding: 30px 0;"><a href="{{route('verifyUser_socialAuth',$token)}}" class="button button-{{ $color ?? 'primary' }}" target="_blank">Verify Email Address</a></td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td>If you did not create an account, no further action is required.</td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td>Regards,</td></tr>
        <tr><td>ShopFurries</td></tr>
        </tbody>
    </table>

   @component('mail::subcopy')

       <table>
           <tr>
               <td>
                    <p>
                        If you’re having trouble clicking the "Verify Email Address" button, copy and paste the URL below into your web browser:
                        <a href="{{route('verifyUser_socialAuth',$token)}}">{{route('verifyUser_socialAuth',$token)}}</a>
                    </p>
               </td>
           </tr>
       </table>


   @endcomponent
@endslot


{{-- Footer --}}

@slot('footer')
    @component('mail::footer')
        <table>
           <tr><td colspan="4" style="color: #aeaeae;font-size: 13px;">   &copy; {{ date('Y') }} Shopfurries. All rights reserved.</td></tr>
        </table>

    @endcomponent
@endslot

@endcomponent
<style>
    p,h1,
    table tr,
    thead tr th ,
    tbody tr td ,
    table tr td {
        font-family: 'Roboto', sans-serif !important;
    }

    table tr td{
        font-size: 16px;
    }

</style>
