@component('mail::layout')
    {{--    @slot('header')--}}
    {{--        @component('mail::header', ['url' => config('app.url')])--}}
    {{--            <img src="{{asset('images/logo/ShopFurries-Logo.png')}}" alt="" class="light-logo" style="width: 100px;--}}
    {{--            height: 80px;--}}
    {{--            margin: 0 auto;--}}
    {{--            margin-bottom: 20px;" />--}}
    {{--        @endcomponent--}}
    {{--    @endslot--}}

    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <?php $appURL = env('APP_URL'); ?>
            <img src="{{URL::to('front-end/assets/img/ShopFurries-LOGO.png')}}"  alt="" style="width: 100px;
                height: 95px;
                margin: 0 auto;
               " />
        @endcomponent
    @endslot


    @slot('subcopy')

        <table style="padding-left: 30px;display: flex !important;justify-content: center !important;background-color: #3490DC;box-shadow: 0 2px 3px #dedede !important;">
            <thead>
            <tr style="text-align: center !important;">
                <th colspan="2">
                    <h3 style="font-size: 20px;color: #fff;margin: 10px 0px;">  ShopFurries - Order Confirmation </h3>
                </th>
            </tr>
            </thead>
        </table>

        @component('mail::subcopy')
            <table style="width:100%;background:#ebf8f3">
                <tr>
                    <td colspan="4" style="padding-top: 10px; text-align: center">
                        <p style="text-align: center;font-weight: bold">Order ID : (323) </p>
                        <b><p style="text-align: center">Yasin (yasin@gmail.com), Thank You for your order!</p></b>
                        <b><p style="text-align: center">We have received your order
                                You can find your purchase information below:</p></b>
                    </td>
                </tr>
            </table>
        @endcomponent

        <table id="example1" class="table table-bordered table-striped" style="margin-top: 50px;width: 100%;border-collapse: collapse;">
            <thead>
            <tr><td style="font-size: 14px;padding: 10px 0px 0px 0px;font-weight: 600">Order Date: 11-10-2020</td></tr>
            <tr><td colspan="4" style="text-align: center"><h2 style="text-align: center">Order Summary </h2></td></tr>
            <tr>
                <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Product</th>
                <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Price</th>
                <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Quantity</th>
                <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Image</th>
            </tr>
            </thead>
            <tbody>
            <tr style="background-color: #f2f2f2;border: 1px solid #f1f1f1;">
                <td style="text-align: center;border: 1px solid #ddd;">Product - 011</td>
                <td style="text-align: center;border: 1px solid #ddd;">$223</td>
                <td style="text-align: center;border: 1px solid #ddd;">1x</td>
                <td style="text-align: center;border: 1px solid #ddd;">
                    <img style="width:120px;height:60px;" src="{{asset('storage/upload/product/qazxseec/gallery/otNO2TDJxEwpWjZqTAVXUXJXVFbxgtjYFL0qY6rN.png')}}">
                </td>
            </tr>
            <tr style="background-color: #f2f2f2;border: 1px solid #f1f1f1;">
                <td style="text-align: center;border: 1px solid #ddd;">Product - 011</td>
                <td style="text-align: center;border: 1px solid #ddd;">$223</td>
                <td style="text-align: center;border: 1px solid #ddd;">1x</td>
                <td style="text-align: center;border: 1px solid #ddd;">
                    <img style="width:120px;height:60px;" src="{{asset('storage/upload/product/qazxseec/gallery/otNO2TDJxEwpWjZqTAVXUXJXVFbxgtjYFL0qY6rN.png')}}">
                </td>
            </tr>
            </tbody>

        </table>

        <table style="width:100%;margin-top: 50px;">
            <tr style="width: 100%;text-align: left">
                <th>Additional Details</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Selected Country:</th>
                <th style="font-weight: 400">USA</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Shipping Cost:</th>
                <th style="font-weight: 400">$34.00</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Cost Per Country Weight:</th>
                <th style="font-weight: 400">$34.55</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Total Products Weight (Kg):</th>
                <th style="font-weight: 400">23.35</th>
            </tr>
        </table>

        @component('mail::subcopy')
        @endcomponent

        <table style="width:100%;margin-top: 50px;">
            <tr style="width: 100%;text-align: left">
                <th>Order Details</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400;width: 80%;">Subtotal Price :</th>
                <th style="font-weight: 400">$33.00</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Coupon Discount:</th>
                <th style="font-weight: 400">$0.00</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Weight :</th>
                <th style="font-weight: 400">$0.00</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Shipping Cost :</th>
                <th style="font-weight: 400">$0.00</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th colspan="2"><hr></th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th>Total Price :</th>
                <th>$33.00</th>
            </tr>
        </table>
    @endslot

    {{-- Footer --}}

    @slot('footer')
        @component('mail::footer')
            <table >
                <tr>
                    <td colspan="4" style="color: #aeaeae;">
                        &copy; {{ date('Y') }}  ShopFurries. All rights reserved.
                    </td>
                </tr>
            </table>
        @endcomponent
    @endslot

@endcomponent

<style>

    .content-cell h1 {
        font-size: 13px !Important;
        font-weight: 600 !Important;
        margin-top: 0 !Important;
        text-align: left !Important;
    }
    .content-cell h2 {
        color: #ffffff !Important;
        background: #3490DC !Important;
        padding: 8px !Important;
        text-align: center !Important;
        box-shadow: 0 2px 3px #dedede !important;
    }

</style>
