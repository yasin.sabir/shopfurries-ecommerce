@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <?php
            $appURL = env('APP_URL');
            ?>

            <img src="{{URL::to('front-end/assets/img/ShopFurries-LOGO.png')}}"  alt="" style="width: 100px;
            height: 95px;
            margin: 0 auto;
           " />
        @endcomponent
    @endslot

    @slot('subcopy')

        <table style="padding-left: 30px;display: flex !important;justify-content: center !important;background-color: #3490DC;box-shadow: 0 2px 3px #dedede !important;">
            <thead>
            <tr style="text-align: center !important;">
                <th colspan="2">
                    <h3 style="font-size: 20px;color: #fff;margin: 10px 0px;">  ShopFurries - Inquiry </h3>
                </th>
            </tr>
            </thead>
        </table>

        @component('mail::subcopy')
        @endcomponent

        <h1>Hello! Admin,</h1>

        <table>
            <tbody>

                <tr><td></td></tr>
                <tr><td></td></tr>
                <tr><td colspan="2" style="font-size: 15px;">Thank You For Contacting us. This is a testing mail</td></tr>

            </tbody>

        </table>

    @endslot


    {{-- Footer --}}

    @slot('footer')
        @component('mail::footer')
            <table >
                <tr>
                    <td colspan="4" style="color: #aeaeae;">
                        &copy; {{ date('Y') }}  ShopFurries. All rights reserved.
                    </td>
                </tr>
            </table>
        @endcomponent
    @endslot

@endcomponent

<style>

    table tr,
    thead tr th ,
    tbody tr td ,
    table tr td {
        font-family: 'Roboto', sans-serif !important;
    }

</style>
