@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{asset('images/logo/ShopFurries-Logo.png')}}" alt="" class="light-logo" style="width: 100px;
            height: 80px;
            margin: 0 auto;
            margin-bottom: 20px;" />
        @endcomponent
    @endslot

    @slot('subcopy')

        <table style="padding-left: 30px;display: flex !important;justify-content: center !important;background-color: #3490DC;box-shadow: 0 2px 3px #dedede !important;">
            <thead>
            <tr style="text-align: center">
                <th colspan="2">
                    <h3 style="font-size: 20px;color: #fff;margin: 10px 0px;"> ShopFurries - Order Confirmation  </h3>
                </th>
            </tr>
            </thead>
        </table>
        @component('mail::subcopy')
            <table style="width:100%;background:#ebf8f3">
                <tr>
                    <td colspan="4" style="padding-top: 10px; text-align: center">
                        <p style="text-align: center;font-weight: bold">Order ID : ({{$order['order_id']}}) </p>
                        <b><p style="text-align: center">{{ucwords($order_details['name'])}} ({{$order_details['email']}}), Thank You for your order!</p></b>
                        <b><p style="text-align: center">We have received your order
                                You can find your purchase information below:</p></b>
                    </td>
                </tr>
            </table>
        @endcomponent

        <table id="example1" class="table table-bordered table-striped" style="margin-top: 50px;width: 100%;border-collapse: collapse;">
            <thead>
            <tr><td style="font-size: 14px;padding: 10px 0px 0px 0px;font-weight: 600">Order Date: 11-10-2020</td></tr>
            <tr><td colspan="4" style="text-align: center"><h2 style="text-align: center">Order Summary </h2></td></tr>
            <tr>
                <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Product</th>
                <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Price</th>
{{--                <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Quantity</th>--}}
                <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Image</th>
            </tr>
            </thead>
            <tbody>
                @php
                    $num=1;
                    $subtotal=[];
                @endphp
                @forelse($product_detail as $data)
                    <tr style="background-color: #f2f2f2;border: 1px solid #f1f1f1;">
                        <td style="text-align: center;border: 1px solid #ddd;">{{ucwords($data['title'])}}</td>
                        <td style="text-align: center;border: 1px solid #ddd;">${{$data['price']}}</td>
{{--                        <td style="text-align: center;border: 1px solid #ddd;">{{$data['quantity']."x"}}</td>--}}
                        <td style="text-align: center;border: 1px solid #ddd;">

                            @php
                                $image = "";
                                $ff = @unserialize($data['image']);
                                if(!is_array($ff)){
                                    $image = $ff;
                                }else{
                                    $image = $data['image'];
                                }
                                $default_size = \App\SiteSetting::where(['key' => 'product_image_size'])->first();
                            @endphp

                            @if(!is_array($ff))
                                @if(!empty($val->image))
                                    <img style="width:120px;height:60px;" ff="af" src="{{ asset('storage/'. $data['image'])}}">
                                @else
                                    <img style="width:120px;height:60px;" src="{{asset('images/default-placeholder-600x600.png')}}"/>
                                @endif
                            @else
                                <img style="width:120px;height:60px;" src="{{asset('storage/'.$ff[$default_size->value])}}">
                            @endif


                        </td>
                    </tr>
                    @php
                        $num++;
                    @endphp
                @empty
                @endforelse
            </tbody>
        </table>


        <table style="width:100%;margin-top: 50px;">
            <tr style="width: 100%;text-align: left">
                <th style="width: 80%;">Additional Details</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Selected Country:</th>
                <th style="font-weight: 400">{{$orderCal['selected_country']}}</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Cost Per Country Weight (Kg):</th>
                <th style="font-weight: 400">${{$orderCal['cost_per_country_weight']}}</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Total Products Weight (Kg):</th>
                <th style="font-weight: 400;width: 80%;">{{$orderCal['total_products_weight']."(Kg)"}}</th>
            </tr>
        </table>


        <table style="width:100%;margin-top: 50px;">

            <tr style="width: 100%;text-align: left">
                <th>Order Details</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Total Weight Cost:</th>
                <th style="font-weight: 400">${{number_format($orderCal['weight_cost'],2)}}</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Shipping Cost - {{ucfirst($orderCal['selected_country'])}}:</th>
                <th style="font-weight: 400">${{$orderCal['shipping_cost']}}</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Subtotal of Products:</th>
                <th style="font-weight: 400">${{$orderCal['subtotal_of_products']}}</th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="font-weight: 400">Subtotal:</th>
                <th style="font-weight: 400">
                    @php
                        $subTotal = ( $orderCal['subtotal_of_products'] + $orderCal['shipping_cost'] + $orderCal['weight_cost'] );
                    @endphp
                    {{"$".number_format($subTotal,2)}}
                </th>
            </tr>

            @if( !empty($orderCal['coupon_discount']) && !empty($orderCal['coupon_type']) )
                <tr style="width: 100%;text-align: left">
                    <th style="font-weight: 400">Coupon Type:</th>
                    <th style="font-weight: 400">{{$orderCal['coupon_type']}}</th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="font-weight: 400">Coupon Discount:</th>
                    <th style="font-weight: 400">{{$orderCal['coupon_discount']}}</th>
                </tr>
            @endif

            <tr style="width: 100%;text-align: left">
                <th colspan="2"><hr></th>
            </tr>
            <tr style="width: 100%;text-align: left">
                <th style="width: 80%;">Total Price:</th>
                <th>${{$orderCal['subtotal']}}</th>
            </tr>
        </table>

    @endslot



    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <table >
                <tr>
                    <td colspan="4" style="color: #aeaeae;">
                        &copy; {{ date('Y') }}  ShopFurries. All rights reserved.
                    </td>
                </tr>
            </table>
        @endcomponent
    @endslot

@endcomponent

<style>
    .content-cell h1 {
        font-size: 13px !Important;
        font-weight: 600 !Important;
        margin-top: 0 !Important;
        text-align: left !Important;
    }
    .content-cell h2 {
        color: #ffffff !Important;
        background: #3490DC !Important;
        padding: 8px !Important;
        text-align: center !Important;
        box-shadow: 0 2px 3px #dedede !important;
    }

</style>
