@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> {{__("routes.Shipping")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Shipping")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('shipping.edit',$shipping->id)}}">{{__("routes.Edit")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Edit Shipping")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <form action="{{route('shipping.update',$shipping->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="shipping_country">{{__("routes.Country")}} <span class="text-danger">*</span>:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control @error('event_country') is-invalid @enderror" name="shipping_country">
                                                <option value="Afghanistan" @if($shipping->country == "Afghanistan" )selected @endif>Afghanistan</option>
                                                <option value="Åland Islands" @if($shipping->country == "Åland Islands" )selected @endif>Åland Islands</option>
                                                <option value="Albania" @if($shipping->country == "Albania" )selected @endif>Albania</option>
                                                <option value="Algeria" @if($shipping->country == "Algeria" )selected @endif>Algeria</option>
                                                <option value="American Samoa" @if($shipping->country == "American Samoa" )selected @endif>American Samoa</option>
                                                <option value="Andorra" @if($shipping->country == "Andorra" )selected @endif>Andorra</option>
                                                <option value="Angola" @if($shipping->country == "Angola" )selected @endif>Angola</option>
                                                <option value="Anguilla" @if($shipping->country == "Anguilla" )selected @endif>Anguilla</option>
                                                <option value="Antarctica" @if($shipping->country == "Antarctica" )selected @endif>Antarctica</option>
                                                <option value="Antigua and Barbuda" @if($shipping->country == "Antigua and Barbuda" )selected @endif>Antigua and Barbuda</option>
                                                <option value="Argentina" @if($shipping->country == "Argentina" )selected @endif>Argentina</option>
                                                <option value="Armenia" @if($shipping->country == "Armenia" )selected @endif>Armenia</option>
                                                <option value="Aruba" @if($shipping->country == "Aruba" )selected @endif>Aruba</option>
                                                <option value="Australia" @if($shipping->country == "Australia" )selected @endif>Australia</option>
                                                <option value="Austria" @if($shipping->country == "Austria" )selected @endif>Austria</option>
                                                <option value="Azerbaijan" @if($shipping->country == "Azerbaijan" )selected @endif>Azerbaijan</option>
                                                <option value="Bahamas" @if($shipping->country == "Bahamas" )selected @endif>Bahamas</option>
                                                <option value="Bahrain" @if($shipping->country == "Bahrain" )selected @endif>Bahrain</option>
                                                <option value="Bangladesh" @if($shipping->country == "Bangladesh" )selected @endif>Bangladesh</option>
                                                <option value="Barbados" @if($shipping->country == "Barbados" )selected @endif>Barbados</option>
                                                <option value="Belarus" @if($shipping->country == "Belarus" )selected @endif>Belarus</option>
                                                <option value="Belgium" @if($shipping->country == "Belgium" )selected @endif>Belgium</option>
                                                <option value="Belize" @if($shipping->country == "Belize" )selected @endif>Belize</option>
                                                <option value="Benin" @if($shipping->country == "Benin" )selected @endif>Benin</option>
                                                <option value="Bermuda" @if($shipping->country == "Bermuda" )selected @endif>Bermuda</option>
                                                <option value="Bhutan" @if($shipping->country == "Bhutan" )selected @endif>Bhutan</option>
                                                <option value="Bolivia" @if($shipping->country == "Bolivia" )selected @endif>Bolivia</option>
                                                <option value="Bosnia and Herzegovina" @if($shipping->country == "Bosnia and Herzegovina" )selected @endif>Bosnia and Herzegovina</option>
                                                <option value="Botswana" @if($shipping->country == "Botswana" )selected @endif>Botswana</option>
                                                <option value="Bouvet Island" @if($shipping->country == "Bouvet Island" )selected @endif>Bouvet Island</option>
                                                <option value="Brazil" @if($shipping->country == "Brazil" )selected @endif>Brazil</option>
                                                <option value="British Indian Ocean Territory" @if($shipping->country == "British Indian Ocean Territory" )selected @endif>British Indian Ocean Territory</option>
                                                <option value="Brunei Darussalam" @if($shipping->country == "Brunei Darussalam" )selected @endif>Brunei Darussalam</option>
                                                <option value="Bulgaria" @if($shipping->country == "Bulgaria" )selected @endif>Bulgaria</option>
                                                <option value="Burkina Faso" @if($shipping->country == "Burkina Faso" )selected @endif>Burkina Faso</option>
                                                <option value="Burundi" @if($shipping->country == "Burundi" )selected @endif>Burundi</option>
                                                <option value="Cambodia" @if($shipping->country == "Cambodia" )selected @endif>Cambodia</option>
                                                <option value="Cameroon" @if($shipping->country == "Cameroon" )selected @endif>Cameroon</option>
                                                <option value="Canada" @if($shipping->country == "Canada" )selected @endif>Canada</option>
                                                <option value="Cape Verde" @if($shipping->country == "Cape Verde" )selected @endif>Cape Verde</option>
                                                <option value="Cayman Islands" @if($shipping->country == "Cayman Islands" )selected @endif>Cayman Islands</option>
                                                <option value="Central African Republic" @if($shipping->country == "Central African Republic" )selected @endif>Central African Republic</option>
                                                <option value="Chad" @if($shipping->country == "Chad" )selected @endif>Chad</option>
                                                <option value="Chile" @if($shipping->country == "Chile" )selected @endif>Chile</option>
                                                <option value="China" @if($shipping->country == "China" )selected @endif>China</option>
                                                <option value="Christmas Island" @if($shipping->country == "Christmas Island" )selected @endif>Christmas Island</option>
                                                <option value="Cocos (Keeling) Islands" @if($shipping->country == "Cocos (Keeling) Islands" )selected @endif>Cocos (Keeling) Islands</option>
                                                <option value="Colombia" @if($shipping->country == "Colombia" )selected @endif>Colombia</option>
                                                <option value="Comoros" @if($shipping->country == "Comoros" )selected @endif>Comoros</option>
                                                <option value="Congo" @if($shipping->country == "Congo" )selected @endif>Congo</option>
                                                <option value="Congo, The Democratic Republic of The" @if($shipping->country == "Congo, The Democratic Republic of The" )selected @endif>Congo, The Democratic Republic of The</option>
                                                <option value="Cook Islands" @if($shipping->country == "Cook Islands" )selected @endif>Cook Islands</option>
                                                <option value="Costa Rica" @if($shipping->country == "Costa Rica" )selected @endif>Costa Rica</option>
                                                <option value="Cote D'ivoire" @if($shipping->country == "Cote D'ivoire" )selected @endif>Cote D'ivoire</option>
                                                <option value="Croatia" @if($shipping->country == "Croatia" )selected @endif>Croatia</option>
                                                <option value="Cuba" @if($shipping->country == "Cuba" )selected @endif>Cuba</option>
                                                <option value="Cyprus" @if($shipping->country == "Cyprus" )selected @endif>Cyprus</option>
                                                <option value="Czech Republic" @if($shipping->country == "Czech Republic" )selected @endif>Czech Republic</option>
                                                <option value="Denmark" @if($shipping->country == "Denmark" )selected @endif>Denmark</option>
                                                <option value="Djibouti" @if($shipping->country == "Djibouti" )selected @endif>Djibouti</option>
                                                <option value="Dominica" @if($shipping->country == "Dominica" )selected @endif>Dominica</option>
                                                <option value="Dominican Republic" @if($shipping->country == "Dominican Republic" )selected @endif>Dominican Republic</option>
                                                <option value="Ecuador" @if($shipping->country == "Ecuador" )selected @endif>Ecuador</option>
                                                <option value="Egypt" @if($shipping->country == "Egypt" )selected @endif>Egypt</option>
                                                <option value="El Salvador" @if($shipping->country == "El Salvador" )selected @endif>El Salvador</option>
                                                <option value="Equatorial Guinea" @if($shipping->country == "Equatorial Guinea" )selected @endif>Equatorial Guinea</option>
                                                <option value="Eritrea" @if($shipping->country == "Eritrea" )selected @endif>Eritrea</option>
                                                <option value="Estonia" @if($shipping->country == "Estonia" )selected @endif>Estonia</option>
                                                <option value="Ethiopia" @if($shipping->country == "Ethiopia" )selected @endif>Ethiopia</option>
                                                <option value="Falkland Islands (Malvinas)" @if($shipping->country == "Falkland Islands (Malvinas)" )selected @endif>Falkland Islands (Malvinas)</option>
                                                <option value="Faroe Islands" @if($shipping->country == "Faroe Islands" )selected @endif>Faroe Islands</option>
                                                <option value="Fiji" @if($shipping->country == "Fiji" )selected @endif>Fiji</option>
                                                <option value="Finland" @if($shipping->country == "Finland" )selected @endif>Finland</option>
                                                <option value="France" @if($shipping->country == "France" )selected @endif>France</option>
                                                <option value="French Guiana" @if($shipping->country == "French Guiana" )selected @endif>French Guiana</option>
                                                <option value="French Polynesia" @if($shipping->country == "French Polynesia" )selected @endif>French Polynesia</option>
                                                <option value="French Southern Territories" @if($shipping->country == "French Southern Territories" )selected @endif>French Southern Territories</option>
                                                <option value="Gabon" @if($shipping->country == "Gabon" )selected @endif>Gabon</option>
                                                <option value="Gambia" @if($shipping->country == "Gambia" )selected @endif>Gambia</option>
                                                <option value="Georgia" @if($shipping->country == "Georgia" )selected @endif>Georgia</option>
                                                <option value="Germany" @if($shipping->country == "Germany" )selected @endif>Germany</option>
                                                <option value="Ghana" @if($shipping->country == "Ghana" )selected @endif>Ghana</option>
                                                <option value="Gibraltar" @if($shipping->country == "Gibraltar" )selected @endif>Gibraltar</option>
                                                <option value="Greece" @if($shipping->country == "Greece" )selected @endif>Greece</option>
                                                <option value="Greenland" @if($shipping->country == "Greenland" )selected @endif>Greenland</option>
                                                <option value="Grenada" @if($shipping->country == "Grenada" )selected @endif>Grenada</option>
                                                <option value="Guadeloupe" @if($shipping->country == "Guadeloupe" )selected @endif>Guadeloupe</option>
                                                <option value="Guam" @if($shipping->country == "Guam" )selected @endif>Guam</option>
                                                <option value="Guatemala" @if($shipping->country == "Guatemala" )selected @endif>Guatemala</option>
                                                <option value="Guernsey" @if($shipping->country == "Guernsey" )selected @endif>Guernsey</option>
                                                <option value="Guinea" @if($shipping->country == "Guinea" )selected @endif>Guinea</option>
                                                <option value="Guinea-bissau" @if($shipping->country == "Guinea-bissau" )selected @endif>Guinea-bissau</option>
                                                <option value="Guyana" @if($shipping->country == "Guyana" )selected @endif>Guyana</option>
                                                <option value="Haiti" @if($shipping->country == "Haiti" )selected @endif>Haiti</option>
                                                <option value="Heard Island and Mcdonald Islands" @if($shipping->country == "Heard Island and Mcdonald Islands" )selected @endif>Heard Island and Mcdonald Islands</option>
                                                <option value="Holy See (Vatican City State)" @if($shipping->country == "Holy See (Vatican City State)" )selected @endif>Holy See (Vatican City State)</option>
                                                <option value="Honduras" @if($shipping->country == "Honduras" )selected @endif>Honduras</option>
                                                <option value="Hong Kong" @if($shipping->country == "Hong Kong" )selected @endif>Hong Kong</option>
                                                <option value="Hungary" @if($shipping->country == "Hungary" )selected @endif>Hungary</option>
                                                <option value="Iceland" @if($shipping->country == "Iceland" )selected @endif>Iceland</option>
                                                <option value="India" @if($shipping->country == "India" )selected @endif>India</option>
                                                <option value="Indonesia" @if($shipping->country == "Indonesia" )selected @endif>Indonesia</option>
                                                <option value="Iran" @if($shipping->country == "Iran" )selected @endif>Iran, Islamic Republic of</option>
                                                <option value="Iraq" @if($shipping->country == "Iraq" )selected @endif>Iraq</option>
                                                <option value="Ireland" @if($shipping->country == "Ireland" )selected @endif>Ireland</option>
                                                <option value="Isle of Man" @if($shipping->country == "Isle of Man" )selected @endif>Isle of Man</option>
                                                <option value="Israel" @if($shipping->country == "Israel" )selected @endif>Israel</option>
                                                <option value="Italy" @if($shipping->country == "Italy" )selected @endif>Italy</option>
                                                <option value="Jamaica" @if($shipping->country == "Jamaica" )selected @endif>Jamaica</option>
                                                <option value="Japan" @if($shipping->country == "Japan" )selected @endif>Japan</option>
                                                <option value="Jersey" @if($shipping->country == "Jersey" )selected @endif>Jersey</option>
                                                <option value="Jordan" @if($shipping->country == "Jordan" )selected @endif>Jordan</option>
                                                <option value="Kazakhstan" @if($shipping->country == "Kazakhstan" )selected @endif>Kazakhstan</option>
                                                <option value="Kenya" @if($shipping->country == "Kenya" )selected @endif>Kenya</option>
                                                <option value="Kiribati" @if($shipping->country == "Kiribati" )selected @endif>Kiribati</option>
                                                <option value="Kuwait" @if($shipping->country == "Kuwait" )selected @endif>Kuwait</option>
                                                <option value="Kyrgyzstan" @if($shipping->country == "Kyrgyzstan" )selected @endif>Kyrgyzstan</option>
                                                <option value="Lao People's Democratic Republic" @if($shipping->country == "Lao People's Democratic Republic" )selected @endif>Lao People's Democratic Republic</option>
                                                <option value="Latvia" @if($shipping->country == "Latvia" )selected @endif>Latvia</option>
                                                <option value="Lebanon" @if($shipping->country == "Lebanon" )selected @endif>Lebanon</option>
                                                <option value="Lesotho" @if($shipping->country == "Lesotho" )selected @endif>Lesotho</option>
                                                <option value="Liberia" @if($shipping->country == "Liberia" )selected @endif>Liberia</option>
                                                <option value="Libyan Arab Jamahiriya" @if($shipping->country == "Libyan Arab Jamahiriya" )selected @endif>Libyan Arab Jamahiriya</option>
                                                <option value="Liechtenstein" @if($shipping->country == "Liechtenstein" )selected @endif>Liechtenstein</option>
                                                <option value="Lithuania" @if($shipping->country == "Lithuania" )selected @endif>Lithuania</option>
                                                <option value="Luxembourg" @if($shipping->country == "Luxembourg" )selected @endif>Luxembourg</option>
                                                <option value="Macao" @if($shipping->country == "Macao" )selected @endif>Macao</option>
                                                <option value="Macedonia" @if($shipping->country == "Macedonia" )selected @endif>Macedonia</option>
                                                <option value="Madagascar" @if($shipping->country == "Madagascar" )selected @endif>Madagascar</option>
                                                <option value="Malawi" @if($shipping->country == "Malawi" )selected @endif>Malawi</option>
                                                <option value="Malaysia" @if($shipping->country == "Malaysia" )selected @endif>Malaysia</option>
                                                <option value="Maldives" @if($shipping->country == "Maldives" )selected @endif>Maldives</option>
                                                <option value="Mali" @if($shipping->country == "Mali" )selected @endif>Mali</option>
                                                <option value="Malta" @if($shipping->country == "Malta" )selected @endif>Malta</option>
                                                <option value="Marshall Islands" @if($shipping->country == "Marshall Islands" )selected @endif>Marshall Islands</option>
                                                <option value="Martinique" @if($shipping->country == "Martinique" )selected @endif>Martinique</option>
                                                <option value="Mauritania" @if($shipping->country == "Mauritania" )selected @endif>Mauritania</option>
                                                <option value="Mauritius" @if($shipping->country == "Mauritius" )selected @endif>Mauritius</option>
                                                <option value="Mayotte" @if($shipping->country == "Mayotte" )selected @endif>Mayotte</option>
                                                <option value="Mexico" @if($shipping->country == "Mexico" )selected @endif>Mexico</option>
                                                <option value="Micronesia" @if($shipping->country == "Micronesia" )selected @endif>Micronesia</option>
                                                <option value="Moldova" @if($shipping->country == "Moldova" )selected @endif>Moldova, Republic of</option>
                                                <option value="Monaco" @if($shipping->country == "Monaco" )selected @endif>Monaco</option>
                                                <option value="Mongolia" @if($shipping->country == "Mongolia" )selected @endif>Mongolia</option>
                                                <option value="Montenegro" @if($shipping->country == "Montenegro" )selected @endif>Montenegro</option>
                                                <option value="Montserrat" @if($shipping->country == "Montserrat" )selected @endif>Montserrat</option>
                                                <option value="Morocco" @if($shipping->country == "Morocco" )selected @endif>Morocco</option>
                                                <option value="Mozambique" @if($shipping->country == "Mozambique" )selected @endif>Mozambique</option>
                                                <option value="Myanmar" @if($shipping->country == "Myanmar" )selected @endif>Myanmar</option>
                                                <option value="Namibia" @if($shipping->country == "Namibia" )selected @endif>Namibia</option>
                                                <option value="Nauru" @if($shipping->country == "Nauru" )selected @endif>Nauru</option>
                                                <option value="Nepal" @if($shipping->country == "Nepal" )selected @endif>Nepal</option>
                                                <option value="Netherlands" @if($shipping->country == "Netherlands" )selected @endif>Netherlands</option>
                                                <option value="Netherlands Antilles" @if($shipping->country == "Netherlands Antilles" )selected @endif>Netherlands Antilles</option>
                                                <option value="New Caledonia" @if($shipping->country == "New Caledonia" )selected @endif>New Caledonia</option>
                                                <option value="New Zealand" @if($shipping->country == "New Zealand" )selected @endif>New Zealand</option>
                                                <option value="Nicaragua" @if($shipping->country == "Nicaragua" )selected @endif>Nicaragua</option>
                                                <option value="Niger" @if($shipping->country == "Niger" )selected @endif>Niger</option>
                                                <option value="Nigeria" @if($shipping->country == "Nigeria" )selected @endif>Nigeria</option>
                                                <option value="Niue" @if($shipping->country == "Niue" )selected @endif>Niue</option>
                                                <option value="Norfolk Island" @if($shipping->country == "Norfolk Island" )selected @endif>Norfolk Island</option>
                                                <option value="Northern Mariana Islands" @if($shipping->country == "Northern Mariana Islands" )selected @endif>Northern Mariana Islands</option>
                                                <option value="Norway" @if($shipping->country == "Norway" )selected @endif>Norway</option>
                                                <option value="Oman" @if($shipping->country == "Oman" )selected @endif>Oman</option>
                                                <option value="Pakistan" @if($shipping->country == "Pakistan" )selected @endif>Pakistan</option>
                                                <option value="Palau" @if($shipping->country == "Palau" )selected @endif>Palau</option>
                                                <option value="Palestinian" @if($shipping->country == "Palestinian" )selected @endif>Palestinian Territory, Occupied</option>
                                                <option value="Panama" @if($shipping->country == "Panama" )selected @endif>Panama</option>
                                                <option value="Papua New Guinea" @if($shipping->country == "Papua New Guinea" )selected @endif>Papua New Guinea</option>
                                                <option value="Paraguay" @if($shipping->country == "Paraguay" )selected @endif>Paraguay</option>
                                                <option value="Peru" @if($shipping->country == "Peru" )selected @endif>Peru</option>
                                                <option value="Philippines" @if($shipping->country == "Philippines" )selected @endif>Philippines</option>
                                                <option value="Pitcairn" @if($shipping->country == "Pitcairn" )selected @endif>Pitcairn</option>
                                                <option value="Poland" @if($shipping->country == "Poland" )selected @endif>Poland</option>
                                                <option value="Portugal" @if($shipping->country == "Portugal" )selected @endif>Portugal</option>
                                                <option value="Puerto Rico" @if($shipping->country == "Puerto Rico" )selected @endif>Puerto Rico</option>
                                                <option value="Qatar" @if($shipping->country == "Qatar" )selected @endif>Qatar</option>
                                                <option value="Reunion" @if($shipping->country == "Reunion" )selected @endif>Reunion</option>
                                                <option value="Romania" @if($shipping->country == "Romania" )selected @endif>Romania</option>
                                                <option value="Russian Federation" @if($shipping->country == "Russian Federation" )selected @endif>Russian Federation</option>
                                                <option value="Rwanda" @if($shipping->country == "Rwanda" )selected @endif>Rwanda</option>
                                                <option value="Saint Helena" @if($shipping->country == "Saint Helena" )selected @endif>Saint Helena</option>
                                                <option value="Saint Kitts and Nevis" @if($shipping->country == "Saint Kitts and Nevis" )selected @endif>Saint Kitts and Nevis</option>
                                                <option value="Saint Lucia" @if($shipping->country == "Saint Lucia" )selected @endif>Saint Lucia</option>
                                                <option value="Saint Pierre and Miquelon" @if($shipping->country == "Saint Pierre and Miquelon" )selected @endif>Saint Pierre and Miquelon</option>
                                                <option value="Saint Vincent and The Grenadines" @if($shipping->country == "Saint Vincent and The Grenadines" )selected @endif>Saint Vincent and The Grenadines</option>
                                                <option value="Samoa" @if($shipping->country == "Samoa" )selected @endif>Samoa</option>
                                                <option value="San Marino" @if($shipping->country == "San Marino" )selected @endif>San Marino</option>
                                                <option value="Sao Tome and Principe" @if($shipping->country == "Sao Tome and Principe" )selected @endif>Sao Tome and Principe</option>
                                                <option value="Saudi Arabia" @if($shipping->country == "Saudi Arabia" )selected @endif>Saudi Arabia</option>
                                                <option value="Senegal" @if($shipping->country == "Senegal" )selected @endif>Senegal</option>
                                                <option value="Serbia" @if($shipping->country == "Serbia" )selected @endif>Serbia</option>
                                                <option value="Seychelles" @if($shipping->country == "Seychelles" )selected @endif>Seychelles</option>
                                                <option value="Sierra Leone" @if($shipping->country == "Sierra Leone" )selected @endif>Sierra Leone</option>
                                                <option value="Singapore" @if($shipping->country == "Singapore" )selected @endif>Singapore</option>
                                                <option value="Slovakia" @if($shipping->country == "Slovakia" )selected @endif>Slovakia</option>
                                                <option value="Slovenia" @if($shipping->country == "Slovenia" )selected @endif>Slovenia</option>
                                                <option value="Solomon Islands" @if($shipping->country == "Solomon Islands" )selected @endif>Solomon Islands</option>
                                                <option value="Somalia" @if($shipping->country == "Somalia" )selected @endif>Somalia</option>
                                                <option value="South Africa" @if($shipping->country == "South Africa" )selected @endif>South Africa</option>
                                                <option value="South Georgia and The South Sandwich Islands" @if($shipping->country == "South Georgia and The South Sandwich Islands" )selected @endif>South Georgia and The South Sandwich
                                                    Islands</option>
                                                <option value="Spain" @if($shipping->country == "Spain" )selected @endif>Spain</option>
                                                <option value="Sri Lanka" @if($shipping->country == "Sri Lanka" )selected @endif>Sri Lanka</option>
                                                <option value="Sudan" @if($shipping->country == "Sudan" )selected @endif>Sudan</option>
                                                <option value="Suriname" @if($shipping->country == "Suriname" )selected @endif>Suriname</option>
                                                <option value="Svalbard and Jan Mayen" @if($shipping->country == "Svalbard and Jan Mayen" )selected @endif>Svalbard and Jan Mayen</option>
                                                <option value="Swaziland" @if($shipping->country == "Swaziland" )selected @endif>Swaziland</option>
                                                <option value="Sweden" @if($shipping->country == "Sweden" )selected @endif>Sweden</option>
                                                <option value="Switzerland" @if($shipping->country == "Switzerland" )selected @endif>Switzerland</option>
                                                <option value="Syria" @if($shipping->country == "Syria" )selected @endif>Syrian Arab Republic</option>
                                                <option value="Taiwan" @if($shipping->country == "Taiwan" )selected @endif>Taiwan, Province of China</option>
                                                <option value="Tajikistan" @if($shipping->country == "Tajikistan" )selected @endif>Tajikistan</option>
                                                <option value="Tanzania" @if($shipping->country == "Tanzania" )selected @endif>Tanzania, United Republic of</option>
                                                <option value="Thailand" @if($shipping->country == "Thailand" )selected @endif>Thailand</option>
                                                <option value="Timor-leste" @if($shipping->country == "Timor-leste" )selected @endif>Timor-leste</option>
                                                <option value="Togo" @if($shipping->country == "Togo" )selected @endif>Togo</option>
                                                <option value="Tokelau" @if($shipping->country == "Tokelau" )selected @endif>Tokelau</option>
                                                <option value="Tonga" @if($shipping->country == "Tonga" )selected @endif>Tonga</option>
                                                <option value="Trinidad and Tobago" @if($shipping->country == "Trinidad and Tobago" )selected @endif>Trinidad and Tobago</option>
                                                <option value="Tunisia" @if($shipping->country == "Tunisia" )selected @endif>Tunisia</option>
                                                <option value="Turkey" @if($shipping->country == "Turkey" )selected @endif>Turkey</option>
                                                <option value="Turkmenistan" @if($shipping->country == "Turkmenistan" )selected @endif>Turkmenistan</option>
                                                <option value="Turks and Caicos Islands" @if($shipping->country == "Turks and Caicos Islands" )selected @endif>Turks and Caicos Islands</option>
                                                <option value="Tuvalu" @if($shipping->country == "Tuvalu" )selected @endif>Tuvalu</option>
                                                <option value="Uganda" @if($shipping->country == "Uganda" )selected @endif>Uganda</option>
                                                <option value="Ukraine" @if($shipping->country == "Ukraine" )selected @endif>Ukraine</option>
                                                <option value="United Arab Emirates" @if($shipping->country == "United Arab Emirates" )selected @endif>United Arab Emirates</option>
                                                <option value="United Kingdom" @if($shipping->country == "United Kingdom" )selected @endif>United Kingdom</option>
                                                <option value="United States" @if($shipping->country === "United States" )selected @endif>United States</option>
                                                <option value="United States Minor Outlying Islands" @if($shipping->country == "United States Minor Outlying Islands" )selected @endif>United States Minor Outlying Islands</option>
                                                <option value="Uruguay" @if($shipping->country == "Uruguay" )selected @endif>Uruguay</option>
                                                <option value="Uzbekistan" @if($shipping->country == "Uzbekistan" )selected @endif>Uzbekistan</option>
                                                <option value="Vanuatu" @if($shipping->country == "Vanuatu" )selected @endif>Vanuatu</option>
                                                <option value="Venezuela" @if($shipping->country == "Venezuela" )selected @endif>Venezuela</option>
                                                <option value="Vietnam" @if($shipping->country == "Vietnam" )selected @endif>Viet Nam</option>
                                                <option value="Virgin Islands, British" @if($shipping->country == "Virgin Islands, British" )selected @endif>Virgin Islands, British</option>
                                                <option value="Virgin Islands, U.S." @if($shipping->country == "Virgin Islands, U.S." )selected @endif>Virgin Islands, U.S.</option>
                                                <option value="Wallis and Futuna" @if($shipping->country == "Wallis and Futuna" )selected @endif>Wallis and Futuna</option>
                                                <option value="Western Sahara" @if($shipping->country == "Western Sahara" )selected @endif>Western Sahara</option>
                                                <option value="Yemen" @if($shipping->country == "Western Sahara" )selected @endif>Yemen</option>
                                                <option value="Zambia" @if($shipping->country == "Zambia" )selected @endif>Zambia</option>
                                                <option value="Zimbabwe" @if($shipping->country == "Zimbabwe") selected @endif>Zimbabwe</option>
                                            </select>
                                            @error('shipping_country')
                                            <span class="invalid-feedback d-block" role="alert">
                                          <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="shipping_weight">{{__("routes.Weight")}} <span class="text-danger">*</span> :</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control @error('shipping_weight') is-invalid @enderror" value="{{$shipping->weight}}"  name="shipping_weight" id="shipping_weight">
                                            @error('artist_name')
                                            <span class="invalid-feedback d-block" role="alert">
                                          <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="Price">{{__("routes.Cost Per Weight")}}<span class="text-danger">*</span> :</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control @error('Price') is-invalid @enderror" value="{{$shipping->price}}" name="shipping_price" id="Price">
                                            <small>
                                                <cite title="Source Title">
                                                    {{__("routes.You can also enter price in decimal like")}} 1,4.5 .....
                                                </cite>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-3 pl-5">
                                            <label for="Price">{{__("routes.Cost Per Country")}}<span class="text-danger">*</span> :</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control @error('shipping_cost_country') is-invalid @enderror" value="{{$shipping->shipping_cost_country}}" name="shipping_cost_country" id="shipping_cost_country">
                                        </div>
                                    </div>


                                    <div class="row mt-4">
                                        <div class="col-md-3 text-left">
                                            <input type="submit" class="btn btn-primary" value="{{__("routes.Update")}}">
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>


                                </form>

                            </div>

                        </div>
                    </div>
                </div>



            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">

        $("#Price , #shipping_cost_country").keydown(function (event) {


            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {

            } else {
                event.preventDefault();
            }

            if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
                event.preventDefault();

        });

    </script>

@endsection
