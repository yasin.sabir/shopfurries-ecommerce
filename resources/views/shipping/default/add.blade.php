@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Default Shipping Section")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Shipping")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('shipping.default-add')}}">{{__("routes.Default Shipping Section")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        @php

            $default_weight_price  = App\SiteSetting::where(['key' => 'default_weight_price'])->first();
            $default_shipping_cost = App\SiteSetting::where(['key' => 'default_shipping_cost'])->first();

        @endphp

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            {{__("routes.Default Shipping & Weight Cost")}}
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form action="{{route('shipping.default-create')}}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="row mt-4">
                                <div class="col-md-3 pl-5">
                                    <label for="default_weight_price">{{__("routes.Default Cost Per Weight")}}(kg)<span class="text-danger">*</span> :</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control @error('default_weight_price') is-invalid @enderror" name="default_weight_price" id="default_weight_price" value="{{isset($default_weight_price) ? $default_weight_price->value :null}}">
                                    <small>
                                        <cite title="Source Title">
                                            {{__("routes.You can also enter price in decimal like")}} 1,4.5 .....
                                        </cite>
                                    </small>
                                    @error('default_weight_price')
                                        <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-md-3 pl-5">
                                    <label for="default_shipping_cost">{{__("routes.Default Shipping Cost")}} <span class="text-danger">*</span> :</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control @error('default_shipping_cost') is-invalid @enderror" name="default_shipping_cost" id="default_shipping_cost" value="{{isset($default_shipping_cost) ? $default_shipping_cost->value :null}}">
                                    @error('default_shipping_cost')
                                        <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="row mt-4">
                                <div class="col-md-3 text-left">
                                    <input type="submit" class="btn btn-primary" value="{{__("routes.Add")}}" />
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">

        $("#default_weight_price , #default_shipping_cost").keydown(function (event) {


            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {

            } else {
                event.preventDefault();
            }

            if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
                event.preventDefault();

        });


    </script>

@endsection
