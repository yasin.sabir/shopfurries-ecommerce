@extends('layouts.backend.app')

@section('page-css')
    @php
        $num=1;
    @endphp

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__("routes.Shipping List")}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Shipping")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('shipping.alllist')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-12">

                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-striped table-bordered nowrap ">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>{{__("routes.Country")}}</th>
                                        <th>{{__("routes.Weight")}}</th>
                                        <th>{{__("routes.Cost Per Weight")}}</th>
                                        <th>{{__("routes.Cost Per Country")}}</th>
                                        <th>{{__("routes.Action")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($shipping as $data)
                                        <tr>
                                            <td>{{$num}}</td>
                                            <td>{{$data->country}}</td>
                                            <td>{{$data->weight}}</td>
                                            <td>{{"$".number_format($data->price,2)}}</td>
                                            <td>{{"$".number_format($data->shipping_cost_country,2)}}</td>
                                            <td>
                                                <a href="{{route('shipping.edit',encrypt($data->id))}}" class="btn btn-info btn-sm mr-3"><i class="fa fa-edit"></i></a>
                                                <a href="#_" class="custom-delete-btn btn btn-danger btn-sm"
                                                   shipping_id="{{$data->id}}"
                                                ><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @php
                                            $num++;
                                        @endphp
                                    @empty
                                        <tr>
                                            <td colspan="5" class="text-center">{{__("routes.No Shipping Found")}}</td>
                                        </tr>

                                    @endforelse
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Shipping")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("routes.You want to sure to delete this shipping")}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection

@section('page-script')

    <script type="text/javascript">

        $('#example1').DataTable({
            "paging": true,
            "pageLength": 7,
            "lengthChange": false,
            "responsive": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("shipping_id");
            var route = '{{ route('shipping.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            })
        });
    </script>

@endsection
