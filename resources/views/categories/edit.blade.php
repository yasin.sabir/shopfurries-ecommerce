@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> Edit category</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__('routes.Category')}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('category.edit',$category_id)}}">{{__('routes.List')}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        @if ($errors->any())
                            <div class="custom-alert-box">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Edit Category
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{route('category.update', $category_id)}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="CategoryThumbnail">Thumbnail:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                @if($category_details->Image != 'null')
                                                    <img src="{{asset('storage/'.$category_details->Image)}}" id="thumbnail-tag"
                                                         width="100" height="100"/>
                                                @else
                                                    <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}" id="thumbnail-tag"
                                                         width="100" height="100"/>
                                                @endif

                                                <div class="custom-file mt-3">
                                                    <input type="file" name="thumbnail_image" class="custom-file-input"
                                                           id="thumbnail">
                                                    <label class="custom-file-label" for="customFile">Choose Image</label>
                                                </div>
                                                {{--<input type="file" name="file" id="profile-img">--}}
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="CategoryName">Category Name <span class="text-danger">*</span> :</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text"
                                                       class="form-control form-control-sm @error('category') is-invalid @enderror "
                                                       id="category" name="category"
                                                       placeholder="Category like.. Mens , Womens , Childs"
                                                       value="{{$category_details->Name}}"
                                                >
                                                @error('category')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                                <small>
                                                    <cite title="Source Title">
                                                        The name is how it appears on your site.
                                                    </cite>
                                                </small>
                                                <input type="hidden" name="status" value="{{$category_details->Status}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3">
                                            <label for="ParentCategory">Parent Category:</label>
                                        </div>
                                        <div class="col-md-6">

                                            <select class="form-control form-control-sm" name="parent_category">
                                                <option value="">None</option>

                                                @foreach($categories as $key => $category)
                                                    <option value="{{$category->id}}" @if($category->id == $category_details->Parent) selected @endif>{{ucfirst($category->Name)}}</option>
                                                @endforeach
                                            </select>
                                            <small>
                                                <cite title="Source Title">
                                                    Assign a parent term to create a hierarchy.
                                                    The term Jazz, for example, would be the parent of Bebop and Big Band.
                                                </cite>
                                            </small>

                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3">
                                            <label for="Artist">Select Artist:</label>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <select class="form-control @error('artists') is-invalid @enderror" name="artists" id="artists">
                                                    <option value="">Select Artist</option>
                                                    @php
                                                        $metas = isset($category_metas['artist']) ? array($category_metas['artist']) : "";
                                                    @endphp

                                                    @if($category_metas['artist'] == Auth::user()->id)
                                                        <option value="{{Auth::user()->id}}"  {{ ( $category_metas['artist'] == Auth::user()->id ) ? 'selected' : '' }}>Your Self</option>
                                                        @foreach($artists as $key => $val)
                                                            <option value="{{ $val->id }}" {{ in_array($val->id  , array_values($metas)) ? 'selected' : '' }}> {{ $val->name }}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="{{Auth::user()->id}}">Your Self</option>
                                                        @forelse($artists as $key => $val)
                                                            <option value="{{ $val->id }}" {{ in_array($val->id  , array_values($metas)) ? 'selected' : '' }}> {{ $val->name }}</option>
                                                        @empty
                                                        @endforelse
                                                    @endif

                                                </select>
                                                <small class="d-block mb-2">
                                                    <cite title="Source Title">
                                                        Select an artist that associate with this category
                                                    </cite>
                                                </small>

                                                @error('artists')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3">
                                            <label for="Artist">Add Sales Tax :</label>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <input type="number"
                                                       class="form-control @error('cate_sale_tax') is-invalid @enderror"
                                                       name="cate_sale_tax"
                                                       id="cate_sale_tax"
                                                       value="{{ isset($category_metas['cate_sale_tax']) ? $category_metas['cate_sale_tax'] : "" }}"
                                                >
                                                <small class="d-block mb-2">
                                                    <cite title="Source Title">
                                                        Category sales tax should be only in digits
                                                    </cite>
                                                </small>
                                                @error('cate_sale_tax')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3">
                                            <label for="CategoryPrice(Optional)">Category Price <span class="text-danger">*</span>:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text"
                                                       class="form-control @error('category_price') is-invalid @enderror "
                                                       id="category_price" name="category_price"
                                                       value="{{ isset($category_metas['price']) ? $category_metas['price'] : "" }}"
                                                       placeholder="Enter price for particular category"
                                                >
                                                @error('category_price')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3">
                                            <label for="CategoryName">Additional Requirements <span class="text-danger">*</span> :</label>

                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control @error('additional_req') is-invalid @enderror" name="additional_req" id="additional_req">
                                                <option value="">Select Option</option>
                                                <option value="0" {{ $category_details->Additional_Req == 0? 'selected="selected"' : ''}}>No</option>
                                                <option value="1" {{ $category_details->Additional_Req == 1 ? 'selected="selected"' : ''}}>Yes</option>
                                            </select>
                                            @error('additional_req')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div id="show_additional_req" class="mt-5">

                                        @php

                                            if(isset($category_metas['material'])){

                                                $material = unserialize($category_metas['material']);

                                                $material = array_map(function ($i){
                                                    return str_replace("_"," ",$i);
                                                },$material);

                                                $material = implode(",",$material);
                                            }

                                            if(isset($category_metas['sizes'])){

                                                $sizes = unserialize($category_metas['sizes']);

                                                $sizes = array_map(function ($i){
                                                    return str_replace("_"," ",$i);
                                                },$sizes);

                                                $sizes = implode(",",$sizes);
                                            }

                                        @endphp

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryPrice(Optional)">Category Product Material:</label>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <input type="text"
                                                           class="form-control @error('category_prod_material') is-invalid @enderror "
                                                           id="category_prod_material" name="category_prod_material"
                                                           value="{{ $material }}"
                                                           placeholder="Each value separate by comma like Basic , Hard "
                                                    >
                                                    @error('category_prod_material')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>


                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryPrice(Optional)">Category Product Size :</label>

                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <input type="text"
                                                           class="form-control @error('category_prod_size') is-invalid @enderror "
                                                           id="category_prod_size" name="category_prod_size"
                                                           value="{{ $sizes }}"
                                                           placeholder="Each value separate by comma like 234 cm , 350 cm"
                                                    >
                                                    <small>
                                                        <cite>Size in cm </cite>
                                                    </small>
                                                    @error('category_prod_size')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>


                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryTags(Optional)">Category Tags (Optional):</label>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="select2-blue">
                                                        <select class="select2" name="category_tags[]" multiple="multiple"
                                                                data-placeholder="Select tags" data-dropdown-css-class="select2-blue"
                                                                style="width: 100%;">
                                                                @php
                                                                    $metas = ( unserialize($category_metas['tags']) ? unserialize($category_metas['tags']) : [] );
                                                                @endphp
                                                                @foreach($tags as $i => $tag)
                                                                    <option value="{{ $tag->id }}"   {{ in_array($tag->id  , array_values($metas)) ? 'selected' : '' }} > {{ $tag->name }}</option>
                                                                @endforeach

                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="Categorydescription(Optional)" class="mt-1">Category Products Reasons (Optional):</label>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <input type="hidden" id="category_reasons" value="{{ isset($category_metas['category_reason_title']) ? count(unserialize($category_metas['category_reason_title'])) :"" }}">
                                                    <input type="hidden" id="category_reasons_titles" value="{{ isset($category_metas['category_reason_title']) ? json_encode(unserialize($category_metas['category_reason_title'])) : "" }}">
                                                    <input type="hidden" id="category_reasons_details" value="{{ isset($category_metas['category_reason_detail']) ? json_encode(unserialize($category_metas['category_reason_detail'])) :"" }}">

                                                    <div class="input_fields_wrap">
                                                        <button class="add_field_button btn btn-primary btn-sm mb-2">Add Field</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryPoster(Optional)" class="mt-1">Category Posters (Optional):</label>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <small class="d-block mb-2">
                                                        <cite title="Source Title">
                                                            Min Width 800px , Max height 2000px
                                                        </cite>
                                                    </small>
                                                    <input id="file-1" type="file" name="category_poster_pics[]" multiple class="file">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryActualPics(Optional)" class="mt-1">Category Actual Material Pictures (Optional):</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="hidden" id="category_actual_material_images" value="{{ isset($category_metas['actual_material_images']) ? json_encode(unserialize($category_metas['actual_material_images'])) : "" }}">
                                                    <input id="file-2" type="file" name="category_actual_material_pics[]" multiple class="file">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryProductFeatures(Optional)" class="mt-1">Category Products Features (Optional):</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <input type="hidden" id="category_features" value="@if(isset($category_metas['category_prod_feature_title'])) {{count(unserialize($category_metas['category_prod_feature_title']))}} @endif">
                                                    <input type="hidden" id="category_feature_titles" value="@if(isset($category_metas['category_prod_feature_title'])) {{json_encode(unserialize($category_metas['category_prod_feature_title']))}} @else {{json_encode(unserialize( serialize(array()) ))}} @endif">
                                                    <input type="hidden" id="category_feature_details" value="@if(isset($category_metas['category_prod_feature_detail'])){{json_encode(unserialize($category_metas['category_prod_feature_detail']))}} @else{{json_encode(unserialize( serialize(array())   ))}} @endif">

                                                    <div class="input_feature_fields_wrap">
                                                        <button class="add_feature_field_button btn btn-primary btn-sm mb-2">Add Field</button>
                                                    </div>
                                                    {{--                                    <div class="mt-1">--}}
                                                    {{--                                    <textarea id="textarea1" class="textarea" rows="30" placeholder="Place some text here"--}}
                                                    {{--                                              name="category_prod_features"--}}
                                                    {{--                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ isset($category_metas['category_prod_features']) ? $category_metas['category_prod_features'] : "" }}</textarea>--}}
                                                    {{--                                    </div>--}}
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary btn-sm" value="Update">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


    </div>



@endsection

@section('page-script')

    <script>
        $('.select2').select2();
        //=======================================================================================================

        $("#show_additional_req").hide();

        var additional_req_value =  parseInt($("#additional_req option:selected").val());

        if(additional_req_value === 0){
            //alert("0");
            $("#show_additional_req").hide();
        }else if(additional_req_value === 1){
            //alert("1");
            $("#show_additional_req").show();
        }


        $("#additional_req").change(function() {
            var additional_req_value =  parseInt($("#additional_req option:selected").val());

            if(additional_req_value === 0){
                //alert("0");
                $("#show_additional_req").hide();
            }else if(additional_req_value === 1){
                //alert("1");
                $("#show_additional_req").show();
            }

        });

        //=======================================================================================================
        // Below code belongs to product reasons

        var max_fields = 6; //maximum input boxes allowed
        var fields_exist = JSON.parse($("#category_reasons_titles").val());
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID
        var x = "";

        if(fields_exist.length){
            x = fields_exist.length+1;
        }else{
            x = 1; //initlal text box count
        }


        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                var val = x;
                x++; //text box increment

                $(wrapper).append('  <div>\n' +
                    '<label for="description" class="mt-1">Reason ' + val + '</label>\n' +
                    '<input type="text" class="mb-2 form-control"\n' +
                    '  id="category_reason_title" name="category_reason_title[]"\n' +
                    '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                    '<textarea name="category_reason_detail[]" placeholder="Reason: Explanation ...." id="category_reason_detail" class="form-control" cols="30" rows="3"></textarea>\n' +
                    '<a href="#" class="remove_field">Remove</a></div></div>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        });


        //=======================================================================================================
        // Below code belongs to product features
        var max_feature_fields = 6; //maximum input boxes allowed
        var feature_fields_exist = JSON.parse($("#category_feature_titles").val());
        var feature_wrapper = $(".input_feature_fields_wrap"); //Fields wrapper
        var feature_add_button = $(".add_feature_field_button"); //Add button ID
        var x1 = "";

        if(feature_fields_exist.length){
            x1 = feature_fields_exist.length+1;
        }else{
            x1 = 1; //initlal text box count
        }


        $(feature_add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x1 < max_feature_fields) { //max input box allowed
                var val = x1;
                x1++; //text box increment

                $(feature_wrapper).append('  <div>\n' +
                    '<label for="description" class="mt-1">Feature ' + val + '</label>\n' +
                    '<input type="text" class="mb-2 form-control"\n' +
                    '  id="category_feature_title" name="category_feature_title[]"\n' +
                    '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                    '<textarea name="category_feature_detail[]" placeholder="Reason: Explanation ...." id="category_feature_detail" class="form-control" cols="30" rows="3"></textarea>\n' +
                    '<a href="#" class="remove_feature_field">Remove</a></div></div>'); //add input box
            }
        });

        $(feature_wrapper).on("click", ".remove_feature_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x1--;
        });



        //=======================================================================================================


        //Poster Images
        $("#file-1").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 5000,
            maxFilesNum: 3,
            showClose: true,
            showUpload: false,
            showCancel: true,
            showZoom: true,
            minImageWidth:  800,
            maxImageHeight: 2000,
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            @isset($category_metas['poster_images'])
            initialPreview: [
                @foreach(unserialize($category_metas['poster_images']) as $key => $val)
                    '<img src="{{asset('storage/'.$val.'')}}" class="kv-preview-data file-preview-image">',
                @endforeach
            ],
            initialPreviewConfig: [
                @foreach(unserialize($category_metas['poster_images']) as $key => $val)
                {
                    url: "{{route('category.delete-poster-images',$category_details->id)}}",
                    _token: '{{csrf_token()}}',
                    key: '{{json_encode($val)}}',
                },
                @endforeach
            ],
            deleteExtraData: {
                '_token': '{{csrf_token()}}',
            },
            @endisset

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        //=======================================================================================================

        var actual_mate_images = JSON.parse($("#category_actual_material_images").val());
        // alert(actual_mate_images);
        $("#file-2").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 5000,
            maxFilesNum: 3,
            showClose: true,
            showUpload: false,
            showCancel: true,
            showZoom: true,
            //maxImageWidth: 250,
            //maxImageHeight: 250,
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            @isset($category_metas['actual_material_images'])
            initialPreview: [
                @foreach(unserialize($category_metas['actual_material_images']) as $key => $val)
                    '<img src="{{asset('storage/'.$val.'')}}" class="kv-preview-data file-preview-image">',
                @endforeach
            ],
            initialPreviewConfig: [
                @foreach(unserialize($category_metas['actual_material_images']) as $key => $val)
                {
                    // $category_details->id
                    url: "{{route('category.delete-actual-material-images',$category_details->id)}}",
                    _token: '{{csrf_token()}}',
                    key: '{{json_encode($val)}}',

                },
                @endforeach
            ],
            deleteExtraData: {
                '_token': '{{csrf_token()}}',
            },
            @endisset
            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });


        //=======================================================================================================

        $('#textarea1').summernote({
            height: 200,
        });

        //=======================================================================================================

        $('.select2').select2();

        //=======================================================================================================


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
            console.log(readURL(this));
        });

        //=======================================================================================================
        // for category features
        //Render Fields according to data
        var feature_titles = JSON.parse($("#category_feature_titles").val());
        var feature_details = JSON.parse($("#category_feature_details").val());

        var i1;
        for(i1 = 0 ; i1 < feature_titles.length ; i1++){
            var num1 = i1+1;
            $(feature_wrapper).append('  <div>\n' +
                '<label for="description" class="mt-1">Feature ' + num1 + '</label>\n' +
                '<input type="text" class="mb-2 form-control"\n' +
                '  id="category_feature_title" value="'+feature_titles[i1]+'" name="category_feature_title[]"\n' +
                '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                '<textarea name="category_feature_detail[]" placeholder="Reason: Explanation ...." id="category_feature_detail" class="form-control" cols="30" rows="3">'+feature_details[i1]+'</textarea>\n' +
                '<a href="#" class="remove_field">Remove</a></div></div>'); //add input box
        }


        //=======================================================================================================

        //Render Fields according to data
        var titles = JSON.parse($("#category_reasons_titles").val());
        var title_details = JSON.parse($("#category_reasons_details").val());

        var i;
        for(i = 0 ; i < titles.length ; i++){
            var num = i+1;
            $(wrapper).append('  <div>\n' +
                '<label for="description" class="mt-1">Reason ' + num + '</label>\n' +
                '<input type="text" class="mb-2 form-control"\n' +
                '  id="category_reason_title" value="'+titles[i]+'" name="category_reason_title[]"\n' +
                '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                '<textarea name="category_reason_detail[]" placeholder="Reason: Explanation ...." id="category_reason_detail" class="form-control" cols="30" rows="3">'+title_details[i]+'</textarea>\n' +
                '<a href="#" class="remove_field">Remove</a></div></div>'); //add input box
        }



    </script>

@endsection
