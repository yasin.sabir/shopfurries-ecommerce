@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> {{__('routes.Product categories')}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__('routes.Category')}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('category.list')}}">{{__('routes.List')}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__('routes.List')}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <button class="btn btn-danger btn-sm mb-3" id="delete-all-btn">{{__('routes.Trash All')}}</button>

                                <table id="example1" class="table table-bordered table-striped nowrap">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" name="checkAll" id="customCheckbox001">
                                                <label for="customCheckbox001" class="custom-control-label"></label>
                                            </div>
                                        </th>
                                        <th>{{__('routes.No')}}:</th>
                                        <th>{{__('routes.Image')}}:</th>
                                        <th>{{__('routes.Category')}}:</th>
                                        <th>{{__('routes.Parent')}}:</th>
                                        <th>{{__('routes.Action')}}:</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($categories as $key => $category)
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox" name="faq" id="customCheckbox{{$key}}" value="{{$category->id}}">
                                                    <label for="customCheckbox{{$key}}" class="custom-control-label"></label>
                                                </div>
                                            </td>
                                            <td>{{$key+1}}</td>
                                            <td>
                                                @if(!$category->Parent)

                                                    <img width="30" height="30"
                                                         src="
                                                            @if($category->Image != 'null')
                                                         {{asset('storage/'.$category->Image)}}
                                                         @else
                                                         {{  asset('images/placeholders/default-placeholder-600x600.png') }}
                                                         @endif
                                                             " alt=""
                                                         srcset="">
                                                @else

                                                    <img width="30" height="30"
                                                         src="
                                                             @if($category->Image != 'null')
                                                         {{asset('storage/'.$category->Image)}}
                                                         @else
                                                         {{  asset('images/placeholders/default-placeholder-600x600.png') }}
                                                         @endif
                                                             " alt=""
                                                         srcset="">

                                                @endif

                                            </td>
                                            <td>

                                                {{$category->Name}}

                                                {{--                                                @if(!$category->Parent)--}}

                                                {{--                                                    <span class="ml-3">--}}
                                                {{--                                                         {{$category->Name}}--}}
                                                {{--                                                    </span>--}}

                                                {{--                                                @else--}}

                                                {{--                                                    <span class="ml-3">--}}
                                                {{--                                                        @php--}}
                                                {{--                                                            $parent_category = \App\Category::where(['id'=> $category->Parent])->first();--}}
                                                {{--                                                        @endphp--}}
                                                {{--                                                        {{$category->Name}} - {{$parent_category->Name}}--}}
                                                {{--                                                    </span>--}}

                                                {{--                                                @endif--}}


                                            </td>
                                            <td>
                                                {{ getCategoryName($category->Parent)  }}

                                            </td>
                                            <td class="d-flex">

                                                <a href="{{route('category.edit',encrypt($category->id))}}"
                                                   class="btn btn-info btn-sm mr-3"><i class="fa fa-edit"></i></a>

                                                <a href="#_" class="custom-delete-btn btn btn-danger btn-sm"
                                                   category_id="{{$category->id}}"
                                                   category_name="{{ $category->Name }}"><i class="fa fa-trash"></i></a>

                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                </div>

            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__('routes.Delete Category')}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__('routes.You want to sure to delete this category')}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('routes.Close')}}</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">{{__('routes.Yes')}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>


    <!-- Delete more than once -->
    <div class="modal fade" id="delete-all-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-all-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="delete_ids" id="delete_ids" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__('routes.Delete Category')}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__('routes.You want to sure to delete all the selected categories')}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('routes.Close')}}</button>
                        <button type="submit" class="btn btn-danger delete_all_modal_btn" data-dismiss="modal">{{__('routes.Yes')}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection

@section('page-script')

    <script type="text/javascript">

        var max_fields = 6; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                var val = x;
                x++; //text box increment

                $(wrapper).append('  <div>\n' +
                    '<label for="description" class="mt-1">Reason ' + val + '</label>\n' +
                    '<input type="text" class="mb-2 form-control"\n' +
                    '  id="category_reason_title" name="category_reason_title[]"\n' +
                    '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                    '<textarea name="category_reason_detail[]" placeholder="Reason: Explanation ...." id="category_reason_detail" class="form-control" cols="30" rows="3"></textarea>\n' +
                    '<a href="#" class="remove_field">Remove</a></div></div>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        });

        //=======================================================================================================

        $("#file-1").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 5000,
            maxFilesNum: 3,
            showClose: true,
            showUpload: false,
            showCancel: true,
            showZoom: true,
            minImageWidth:  800,
            maxImageWidth:  900,
            minImageHeight: 1200,
            maxImageHeight: 1400,

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        //=======================================================================================================

        $("#file-2").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 5000,
            maxFilesNum: 3,
            showClose: true,
            showUpload: false,
            showCancel: true,
            showZoom: true,
            //maxImageWidth: 250,
            //maxImageHeight: 250,
            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });


        //=======================================================================================================

        $('#textarea1').summernote({
            height: 200,
        });

        //=======================================================================================================

        $('.select2').select2()

        //=======================================================================================================

        $('#example1').DataTable({
            "paging": true,
            "pageLength": 7,
            "lengthChange": true,
            "responsive": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });

        //=======================================================================================================

        $("input[name='checkAll']").click(function(){
            $("input[name='faq']").not(this).prop('checked', this.checked);
        });

        // =======================================================================================================

        $("#delete-all-btn").on('click' , function () {
            var ids = [];

            var route = '{{ route('category.delete-all') }}';

            $("#delete-all-modal").modal('show');
            $("#delete-all-modal-form").attr("action", route);

            $.each($("input[name='faq']:checked"), function(){
                ids.push($(this).val());
            });
            console.log(ids);
            $("#delete_ids").attr("value",JSON.stringify(ids));

            $('.delete_all_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-all-modal-form").submit();
            });

        });


        //=======================================================================================================

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("category_id");
            var route = '{{ route('category.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            });

        });

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

    </script>

@endsection
