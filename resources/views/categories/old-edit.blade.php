@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> Edit category</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Edit</a></li>
                            <li class="breadcrumb-item active">Category</li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <form action="{{route('category.update', $category_id)}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-md-3">
                            <label for="exampleInputEmail1">Category Name:</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text"
                                       class="form-control form-control-sm @error('category') is-invalid @enderror "
                                       id="category" name="category"
                                       placeholder="Category like.. Mens , Womens , Childs"
                                       value="{{$category_details->Name}}"
                                >
                                @error('category')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <small>
                                    <cite title="Source Title">
                                        The name is how it appears on your site.
                                    </cite>
                                </small>
                                <input type="hidden" name="status" value="{{$category_details->Status}}">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-3">
                            <label for="exampleInputEmail1">Parent Category:</label>
                        </div>
                        <div class="col-md-6">

                            <select class="form-control form-control-sm" name="parent_category">
                                <option value="">None</option>

                                @foreach($categories as $key => $category)
                                    <option value="{{$category->id}}" @if($category->id == $category_details->Parent) selected @endif>{{ucfirst($category->Name)}}</option>
                                @endforeach
                            </select>
                            <small>
                                <cite title="Source Title">
                                    Assign a parent term to create a hierarchy.
                                    The term Jazz, for example, would be the parent of Bebop and Big Band.
                                </cite>
                            </small>

                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-3">
                            <label for="exampleInputEmail1">Thumbnail:</label>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">

                                @if($category_details->Image != 'null')

                                    <img src="{{asset('storage/'.$category_details->Image)}}" id="thumbnail-tag"
                                         width="100" height="100"/>

                                @else

                                    <img src="{{asset('images/default-placeholder-600x600.png')}}" id="thumbnail-tag"
                                         width="100" height="100"/>

                                @endif

                                <div class="custom-file mt-3">
                                    <input type="file" name="thumbnail_image" class="custom-file-input"
                                           id="thumbnail">
                                    <label class="custom-file-label" for="customFile">Choose Image</label>
                                </div>
                                {{--<input type="file" name="file" id="profile-img">--}}
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-sm" value="Update">
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>

                </form>
            </div>
        </section>


    </div>



@endsection

@section('page-script')

    <script>

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
            console.log(readURL(this));
        });

    </script>

@endsection
