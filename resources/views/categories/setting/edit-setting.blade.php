@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> {{__("routes.By Default Additional Category Setting")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Category")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('category.setting')}}">{{__("routes.Additional Setting")}}</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('category.setting-edit',$cst->id)}}">{{__("routes.Edit")}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Edit")}}{{__("routes.Additional Setting")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{route('category.setting-update' , $cst->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div id="show_additional_req">

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="Categorydescription(Optional)" class="mt-1">{{__("routes.Set Default Category Products Reasons")}}:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="hidden" id="category_reasons" value="{{ isset($cst->reason_titles) ? count(unserialize($cst->reason_titles)) :"" }}">
                                                    <input type="hidden" id="category_reasons_titles" value="{{ isset($cst->reason_titles) ? json_encode(unserialize($cst->reason_titles)) : 0 }}">
                                                    <input type="hidden" id="category_reasons_details" value="{{ isset($cst->reason_descriptions) ? json_encode(unserialize($cst->reason_descriptions)) : 0 }}">
                                                    <div class="input_fields_wrap">
                                                        <button class="add_field_button btn btn-primary btn-sm mb-2">Add Field</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryPoster(Optional)" class="mt-1">{{__("routes.Set Default Category Posters")}}:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <small class="d-block mb-2">
                                                        <cite title="Source Title">
                                                            {{__("routes.Min Width")}} 800px , {{__("routes.Max Height")}} 2000px
                                                        </cite>
                                                    </small>
                                                    <input id="file-1" type="file" name="category_poster_pics[]" multiple class="file">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryActualPics(Optional)" class="mt-1">{{__("routes.Set Default Category Posters")}}:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input id="file-2" type="file" name="category_actual_material_pics[]" multiple class="file">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryProductFeatures(Optional)" class="mt-1">{{__("routes.Set Default Category Actual Material Pictures")}}:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="hidden" id="category_features" value="@if(isset($cst->feature_titles)) {{count(unserialize($cst->feature_titles))}} @endif">
                                                    <input type="hidden" id="category_feature_titles" value="@if(isset($cst->feature_titles)) {{json_encode(unserialize($cst->feature_titles))}} @else {{json_encode(unserialize( serialize(array()) ))}} @endif">
                                                    <input type="hidden" id="category_feature_details" value="@if(isset($cst->feature_descriptions)){{json_encode(unserialize($cst->feature_descriptions))}} @else{{json_encode(unserialize( serialize(array())   ))}} @endif">
                                                    <div class="input_feature_fields_wrap">
                                                        <button class="add_feature_field_button btn btn-primary btn-sm mb-2">Add Field</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="row mt-4">
                                        <div class="col-md-3 text-left">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary" value="{{__("routes.Update")}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>

                                </form>
                            </div>


                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">

        // Below code belongs to product reasons
        // $(".add_field_button").click(function(e){
        //    e.preventDefault();
        // });
        //
        // $(".add_feature_field_button").click(function (e) {
        //    e.preventDefault();
        // });

        var max_fields = 6; //maximum input boxes allowed
        var fields_exist = JSON.parse($("#category_reasons_titles").val());
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID
        var x = "";

        if(fields_exist.length){
            x = fields_exist.length+1;
        }else{
            x = 1; //initlal text box count
        }

        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                var val = x;
                x++; //text box increment

                $(wrapper).append('  <div>\n' +
                    '<label for="description" class="mt-1">Reason ' + val + '</label>\n' +
                    '<input type="text" class="mb-2 form-control"\n' +
                    '  id="category_reason_title" name="category_reason_title[]"\n' +
                    '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                    '<textarea name="category_reason_detail[]" placeholder="Reason: Explanation ...." id="category_reason_detail" class="form-control" cols="30" rows="3"></textarea>\n' +
                    '<a href="#" class="remove_field">Remove</a></div></div>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        });

        //=======================================================================================================
        // Below code belongs to product features

        var max_feature_fields = 6; //maximum input boxes allowed
        var feature_fields_exist = JSON.parse($("#category_feature_titles").val());
        var feature_wrapper = $(".input_feature_fields_wrap"); //Fields wrapper
        var feature_add_button = $(".add_feature_field_button"); //Add button ID
        var x1 = "";

        if(feature_fields_exist.length){
            x1 = feature_fields_exist.length+1;
        }else{
            x1 = 1; //initlal text box count
        }

        $(feature_add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x1 < max_feature_fields) { //max input box allowed
                var val = x1;
                x1++; //text box increment

                $(feature_wrapper).append('  <div>\n' +
                    '<label for="description" class="mt-1">Feature ' + val + '</label>\n' +
                    '<input type="text" class="mb-2 form-control"\n' +
                    '  id="category_feature_title" name="category_feature_title[]"\n' +
                    '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                    '<textarea name="category_feature_detail[]" placeholder="Reason: Explanation ...." id="category_feature_detail" class="form-control" cols="30" rows="3"></textarea>\n' +
                    '<a href="#" class="remove_feature_field">Remove</a></div></div>'); //add input box
            }
        });

        $(feature_wrapper).on("click", ".remove_feature_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x1--;
        });


        //=======================================================================================================
        $("#file-1").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 5000,
            maxFilesNum: 3,
            showClose: true,
            showUpload: false,
            showCancel: true,
            showZoom: true,
            minImageWidth:  800,
            maxImageHeight: 2000,
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            @if(!empty($cst->posters))
                initialPreview: [
                    @foreach(unserialize($cst->posters) as $key => $val)
                        '<img src="{{asset('storage/'.$val.'')}}" class="kv-preview-data file-preview-image">',
                    @endforeach
                ],
                initialPreviewConfig: [
                    @foreach(unserialize($cst->posters) as $key => $val)
                    {
                        url: "{{route('category.delete-cateST-poster-img',$cst->id)}}",
                        _token: '{{csrf_token()}}',
                        key: '{{json_encode($val)}}',
                    },
                    @endforeach
                ],
                deleteExtraData: {
                    '_token': '{{csrf_token()}}',
                },
            @endif

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        //=======================================================================================================

        $("#file-2").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 5000,
            maxFilesNum: 3,
            showClose: true,
            showUpload: false,
            showCancel: true,
            showZoom: true,
            //maxImageWidth: 250,
            //maxImageHeight: 250,
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            @isset($cst->actual_material_pics)
            initialPreview: [
                @foreach(unserialize($cst->actual_material_pics) as $key => $val)
                    '<img src="{{asset('storage/'.$val.'')}}" class="kv-preview-data file-preview-image">',
                @endforeach
            ],
            initialPreviewConfig: [
                @foreach(unserialize($cst->actual_material_pics) as $key => $val)
                {
                    // $category_details->id
                    url: "{{route('category.delete-cateST-actual-material-img',$cst->id)}}",
                    _token: '{{csrf_token()}}',
                    key: '{{json_encode($val)}}',

                },
                @endforeach
            ],
            deleteExtraData: {
                '_token': '{{csrf_token()}}',
            },
            @endisset
            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });


        //=======================================================================================================

        $('#textarea1').summernote({
            height: 200,
        });

        //=======================================================================================================

        $('.select2').select2()


        //=======================================================================================================
        // for category features
        //Render Fields according to data
        var feature_titles = JSON.parse($("#category_feature_titles").val());
        var feature_details = JSON.parse($("#category_feature_details").val());

        var i1;
        for(i1 = 0 ; i1 < feature_titles.length ; i1++){
            var num1 = i1+1;
            $(feature_wrapper).append('  <div>\n' +
                '<label for="description" class="mt-1">Feature ' + num1 + '</label>\n' +
                '<input type="text" class="mb-2 form-control"\n' +
                '  id="category_feature_title" value="'+feature_titles[i1]+'" name="category_feature_title[]"\n' +
                '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                '<textarea name="category_feature_detail[]" placeholder="Reason: Explanation ...." id="category_feature_detail" class="form-control" cols="30" rows="3">'+feature_details[i1]+'</textarea>\n' +
                '<a href="#" class="remove_field">Remove</a></div></div>'); //add input box
        }


        //=======================================================================================================
        //Render Fields according to data
        var titles = JSON.parse($("#category_reasons_titles").val());
        var title_details = JSON.parse($("#category_reasons_details").val());

        var i;
        for(i = 0 ; i < titles.length ; i++){
            var num = i+1;
            $(wrapper).append('  <div>\n' +
                '<label for="description" class="mt-1">Reason ' + num + '</label>\n' +
                '<input type="text" class="mb-2 form-control"\n' +
                '  id="category_reason_title" value="'+titles[i]+'" name="category_reason_title[]"\n' +
                '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                '<textarea name="category_reason_detail[]" placeholder="Reason: Explanation ...." id="category_reason_detail" class="form-control" cols="30" rows="3">'+title_details[i]+'</textarea>\n' +
                '<a href="#" class="remove_field">Remove</a></div></div>'); //add input box
        }

    </script>

@endsection
