@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> {{__("routes.By Default Additional Category Setting")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Category")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('category.setting')}}">{{__("routes.Additional Setting")}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Additional Setting")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{route('category.setting-create')}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div id="show_additional_req">

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="Categorydescription(Optional)" class="mt-1">{{__("routes.Set Default Category Products Reasons")}}:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="input_fields_wrap">
                                                        <button class="add_field_button btn btn-primary btn-sm mb-2">{{__("routes.Add Field")}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryPoster(Optional)" class="mt-1">{{__("routes.Set Default Category Posters")}}:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <small class="d-block mb-2">
                                                        <cite title="Source Title">
                                                            {{__("routes.Min Width")}} 800px , {{__("routes.Max Height")}} 2000px
                                                        </cite>
                                                    </small>
                                                    <input id="file-1" type="file" name="category_poster_pics[]" multiple class="file">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryActualPics(Optional)" class="mt-1">{{__("routes.Set Default Category Actual Material Pictures")}}:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input id="file-2" type="file" name="category_actual_material_pics[]" multiple class="file">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <label for="CategoryProductFeatures(Optional)" class="mt-1">{{__("routes.Set Default Category Products Features")}}:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="input_feature_fields_wrap">
                                                        <button class="add_feature_field_button btn btn-primary btn-sm mb-2">{{__("routes.Add Field")}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="row mt-4">
                                        <div class="col-md-3 text-left">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary" value="{{__("routes.Add Setting")}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>

                                </form>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">


        $("#additional_req").change(function() {
            var additional_req_value =  parseInt($("#additional_req option:selected").val());

            if(additional_req_value === 0){
                //alert("0");
                $("#show_additional_req").hide();
            }else if(additional_req_value === 1){
                //alert("1");
                $("#show_additional_req").show();
            }

        });

        //=======================================================================================================

        // Below code belongs to product reasons

        var max_fields = 6; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                var val = x;
                x++; //text box increment

                $(wrapper).append('  <div>\n' +
                    '<label for="description" class="mt-1">Reason ' + val + '</label>\n' +
                    '<input type="text" class="mb-2 form-control"\n' +
                    '  id="category_reason_title" name="category_reason_title[]"\n' +
                    '  placeholder="Title: Unique Material,Vibrant Colors ....">\n' +
                    '<textarea name="category_reason_detail[]" placeholder="Reason: Explanation ...." id="category_reason_detail" class="form-control" cols="30" rows="3"></textarea>\n' +
                    '<a href="#" class="remove_field">Remove</a></div></div>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        });

        //=======================================================================================================

        // Below code belongs to product features
        var max_feature_fields = 6; //maximum input boxes allowed
        var feature_wrapper = $(".input_feature_fields_wrap"); //Fields wrapper
        var add_feature_button = $(".add_feature_field_button"); //Add button ID

        var x1 = 1; //initlal text box count
        $(add_feature_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x1 < max_feature_fields) { //max input box allowed
                var val = x1;
                x1++; //text box increment

                $(feature_wrapper).append('  <div>\n' +
                    '<label for="feature_heading" class="mt-1">Feature ' + val + '</label>\n' +
                    '<input type="text" class="mb-2 form-control"\n' +
                    '  id="category_feature_title" name="category_feature_title[]"\n' +
                    '  placeholder="Title:  HUG & CUDDLE WITH THE SNUGGLIEST....">\n' +
                    '<textarea name="category_feature_detail[]" placeholder="Reason: Explanation ...." id="category_feature_detail" class="form-control" cols="30" rows="3"></textarea>\n' +
                    '<a href="#" class="remove_feature_field">Remove</a></div></div>'); //add input box
            }
        });

        $(feature_wrapper).on("click", ".remove_feature_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x1--;
        });


        //=======================================================================================================
        $("#file-1").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 5000,
            maxFilesNum: 3,
            showClose: true,
            showUpload: false,
            showCancel: true,
            showZoom: true,
            minImageWidth:  800,
            maxImageHeight: 2000,

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        //=======================================================================================================

        $("#file-2").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 5000,
            maxFilesNum: 3,
            showClose: true,
            showUpload: false,
            showCancel: true,
            showZoom: true,
            //maxImageWidth: 250,
            //maxImageHeight: 250,
            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });


        //=======================================================================================================

        $('#textarea1').summernote({
            height: 200,
        });

        //=======================================================================================================

        $('.select2').select2()

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

    </script>

@endsection
