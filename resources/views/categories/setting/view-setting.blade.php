@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> {{__("routes.By Default Additional Category Setting")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Category")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('category.setting')}}">{{__("routes.Additional Setting")}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Additional Setting")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="CategoryName">{{__("routes.Settings")}}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        @if(!empty($cst->id))
                                            <div class="custom-control custom-switch">
                                                <input type="hidden" name="cate_id" id="cat_id" value="@if(!empty($cst->id)){{$cst->id}}@endif">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch1" @if(isset($cst->status) && $cst->status == "on" ) checked @else uncheckec @endif>
                                                <label class="custom-control-label" for="customSwitch1"></label>
                                                <small>
                                                    <cite title="Source Title" for="customSwitch1">
                                                        {{__("routes.Default Setting Switch")}}
                                                    </cite>
                                                </small>
                                            </div>
                                        @else
                                            <small>
                                                <cite title="Source Title" for="customSwitch1">
                                                    {{__("routes.No Data Found...")}}
                                                </cite>
                                            </small>
                                        @endif
                                    </div>
                                </div>
                                <hr>

                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <label for="CategoryName"> {{__("routes.Posters")}}:</label>
                                    </div>

                                    @php
                                        $poster = ( !empty($cst->posters) ? unserialize($cst->posters) : array() )
                                    @endphp

                                    <div class="col-md-6">

                                        @forelse($poster as $key => $post_img)
                                            <img src="{{asset('storage/'.$post_img)}}" class="poster_img" alt="">
                                        @empty
                                            <small>
                                                <cite title="Source Title">
                                                    {{__("routes.No Posters Found...")}}
                                                </cite>
                                            </small>
                                        @endforelse

                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <label for="CategoryName"> {{__("routes.Actual Material Images")}}:</label>
                                    </div>

                                    @php
                                        $material_imgs =  ( !empty($cst->actual_material_pics) ? unserialize($cst->actual_material_pics) : array() );
                                    @endphp

                                    <div class="col-md-6">
                                        @forelse($material_imgs as $key => $img)
                                            <img src="{{asset('storage/'.$img)}}" class="act_material_img" alt="">
                                        @empty
                                            <small>
                                                <cite title="Source Title">
                                                    {{__("routes.No Materials Found...")}}
                                                </cite>
                                            </small>
                                        @endforelse
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <label for="CategoryName"> {{__("routes.Reasons")}}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        @forelse($reasons as $key => $val)
                                            <dl>
                                                <dt>{{$val['title_'.$key]}}</dt>
                                                <dd>{{$val['detail_'.$key]}}</dd>
                                            </dl>
                                        @empty
                                            <small>
                                                <cite title="Source Title">
                                                    {{__("routes.No Reasons Found...")}}
                                                </cite>
                                            </small>
                                        @endforelse
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <label for="CategoryName">{{__("routes.Featured")}}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        @forelse($features as $key => $val)
                                            <dl>
                                                <dt>{{$val['title_'.$key]}}</dt>
                                                <dd>{{$val['detail_'.$key]}}</dd>
                                            </dl>
                                        @empty
                                            <small>
                                                <cite title="Source Title">
                                                    {{__("routes.No Features Found...")}}
                                                </cite>
                                            </small>
                                        @endforelse
                                    </div>
                                </div>


                                <div class="row mt-4">

                                    <div class="col-md-12">

                                        @if(!isset($cst))
                                            <a href="{{route('category.setting-add')}}" class="btn btn-primary btn-sm">{{__("routes.Add")}} {{__("routes.Additional Setting")}}</a>
                                        @else
                                            <a href="{{route('category.setting-edit',encrypt($cst->id))}}" class="btn btn-primary btn-sm">{{__("routes.Edit")}} {{__("routes.Additional Setting")}}</a>
                                        @endif

                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>



            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================
        var status = "";

        $('#customSwitch1').click(function(){

            var customSwitch1 = $(this);

            if($(this).prop("checked") === true){

                console.log("checked");
                status = "on";

                $.ajax({
                    context: this,
                    @if(!empty($cst))
                    url:"{{route('category.setting-status-update',$cst->id)}}",
                    @endif
                    type: 'post',
                    data: {"status":status},
                    success:function(response){
                        //console.log(response+" - Mode is on");
                        toastr.success('Category setting is <strong>On</strong> !', 'Category Setting Alert!');
                    }
                });
            }
            else if($(this).prop("checked") === false){

                console.log("Unchecked");
                status = "off";
                $.ajax({
                    context: this,
                    @if(!empty($cst))
                    url:"{{route('category.setting-status-update',$cst->id)}}",
                    @endif
                    type: 'post',
                    data: {"status":status},
                    success:function(response){
                        //console.log(response+" - Mode is off");
                        toastr.error('Category setting is <strong>Off</strong> !', 'Category Setting Alert!');

                    }
                });
            }
        });


        //=======================================================================================================
    </script>

@endsection
