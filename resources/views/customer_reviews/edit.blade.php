@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Customer Reviews")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Customer Reviews")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('customer-reviews.edit',$cr->id)}}">{{__("routes.Edit")}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Add Section")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <div class="row mt-4">
                                    <div class="col-md-3 pl-5">
                                        <label for="CategoryThumbnail">{{__("routes.State")}}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="hidden" id="cr_id" value="{{$cr->id}}">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch1" @if($cr->status == "on" ) checked @else unchecked @endif >
                                                <label class="custom-control-label" for="customSwitch1"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <form action="{{route('customer-reviews.update',$cr->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="CategoryThumbnail">{{__("routes.Avatar")}}:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="hidden" name="prev_cr_image" id="prev_cr_image" value="{{$cr->image}}">

                                                @if( $cr->image != null)
                                                    <img src="{{asset('storage/'.$cr->image)}}" id="thumbnail-tag"
                                                         width="100" height="100"/>
                                                @else
                                                    <img src="{{asset('images/placeholders/Profile_avatar_placeholder_lg.png')}}"
                                                         id="thumbnail-tag"
                                                         width="100" height="100"/>
                                                @endif

                                                <div class="custom-file mt-3">
                                                    <input type="file" name="cr_image" class="custom-file-input"
                                                           id="thumbnail">
                                                    <label class="custom-file-label" for="customFile">{{__("routes.Choose Image")}}</label>
                                                </div>
                                                {{--<input type="file" name="file" id="profile-img">--}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistName">{{__("routes.Name")}}<span class="text-danger">*</span> :</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('cr_name') is-invalid @enderror"
                                                       value="{{$cr->name}}" name="cr_name" id="cr_name">
                                                @error('cr_name')
                                                <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">{{__("routes.Rating")}} <span class="text-danger">*</span>:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="number" value="{{$cr->rating}}"
                                                       class="form-control @error('cr_rating') is-invalid @enderror"
                                                       name="cr_rating" id="cr_rating">
                                                @error('cr_rating')
                                                <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">{{__("routes.Details (Optional)")}}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                    <textarea id="textarea2" class="" name="cr_detail"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$cr->detail}}</textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mt-4">
                                        <div class="col-md-3 text-left">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary" value="{{__("routes.Update")}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>



            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================

        var status = "";

        // input[type="checkbox"]
        $('#customSwitch1').click(function(){
            var customSwitch1 = $(this);
            if($(this).prop("checked") === true){
                console.log("checked");
                status = "on";

                $.ajax({
                    url:"{{route('customer-reviews.status-update',$cr->id)}}",
                    type: 'post',
                    data: {status : status},
                    success:function(response){
                        //console.log(response+" - Mode is on");
                        toastr.success('Customer Review is active!', 'Review Status Alert!');
                    }
                });
            }
            else if($(this).prop("checked") === false){
                console.log("Unchecked");
                status = "off";
                $.ajax({
                    url:"{{route('customer-reviews.status-update',$cr->id)}}",
                    type: 'post',
                    data: {status : status},
                    success:function(response){
                        //console.log(response+" - Mode is off");
                        toastr.error('Customer Review is deactivated!', 'Review Status Alert!');
                    }
                });
            }
        });

        //=======================================================================================================
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

        $('#textarea2').summernote({
            height: 200,
            toolbar: false,
        });


        //=======================================================================================================

    </script>

@endsection
