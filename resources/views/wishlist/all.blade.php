<!-- Front End View -->
@extends('front-layout.app')

@section('title')
    Your Favorites
@endsection


@section('custom-front-css')

@endsection


@section('Main')

    <!-- product-title GROUP -->
    <div class="product-titleGroup">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="title-text wishlist-title-text">
                        <h1>Wishlist</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product-title GROUP -->

    <!-- product-area START -->
    <div class="product-area shop wishlist-product-area">
        <div class="container">
            <div class="products">
                <div class="row">
                    @forelse($products as $key => $product)
                        <div class="col-lg-3 col-md-6">
                            <div class="product-box wishlist-box">

{{--                                @if($product['image'] != 'null')--}}
{{--                                    <img src="{{ asset('storage/'.$product['image'])}}"--}}
{{--                                         alt="{{ucfirst($product['title'])}}">--}}
{{--                                @else--}}
{{--                                    <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"/>--}}
{{--                                @endif--}}

                                @php

                                    $image = "";

                                    $ff = @unserialize($product['image']);

                                    if(!is_array($ff)){
                                        $image = $ff;
                                    }else{
                                        $image = $product['image'];
                                    }

                                    $default_size = \App\SiteSetting::where(['key' => 'product_image_size'])->first();
                                @endphp


                                @if(!is_array($ff))

                                    @if(!empty($product['image']))
                                        <img ff="af" src="{{ asset('storage/'.$product['image'])}}"
                                             alt="{{ucfirst($product['image'])}}">
                                    @else
                                        <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"/>
                                    @endif

                                @else
                                    <img src="{{ asset('storage/'.$ff[$default_size->value])}}">
                                @endif



                                <div class="product-love">
                                    <a href="" product_id="{{$product['id']}}" class="removed_wishlist_btn"><i
                                            class="fas fa-heart"></i></a>
                                </div>
                                <div class="productBox-content">
                                    <h5><a class="cu-text-black"
                                           href="{{route('single_product',['name'=>$product['title'],'id' => $product['id']])}}">{{substr(ucfirst($product['title']),0,20)}}</a>

                                        <span>${{number_format($product['price'],2)}}</span></h5>
                                    <p>{{substr(strip_tags($product['description']),0,70)."..."}}</p>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="col-md-12 text-center"><h3>No product is found in wishlist!</h3></div>
                    @endforelse

                </div>
            </div>
        </div>
    </div>
    <!-- product-area END -->


    <!-- product-title GROUP -->
    <div class="product-titleGroup">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <div class="title-text wishlist-title-text">
                        <h1>Wishlist - Art Works</h1>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- product-title GROUP -->


    <!-- Art Work Start -->

    <div class="gallery-area">
        <div class="container">
            <div class="row">
                @forelse($art_works as $key => $arkWork)

                    <div class="col-lg-4 col-md-6">
                        <div class="gallery">


                            @if(!empty($arkWork['image']))
                                <img src="{{asset('storage/'.$arkWork['image'])}}" alt="">
                            @else
                                <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"
                                     alt="">
                            @endif

                            <div class="gallery-wrep">
                                <h5>Commission: {{ucwords($arkWork['title'])}}</h5>
                                <h4>
                                    @php
                                        $user = \App\User::find($arkWork['artist_id']);
                                    @endphp

                                    @if(!empty($user->profile_pic))
                                        <img src="{{asset('storage/'.$user->profile_pic)}}" alt="">
                                    @else
                                        <img
                                            src="{{asset('images/placeholders/Profile_avatar_placeholder_lg.png')}}"
                                            alt="">
                                    @endif

                                    Art by

                                    {{ucfirst($user->name)}}

                                    <span>Twitter. Facebook. FurAffinity</span>
                                    <span class="gDATE">Tags:
                                        @php
                                            $tags = [];
                                            $tags_data = explode(',',$arkWork['tags']);
                                            foreach ($tags_data as $key => $val){
                                                $tags [] = ucwords($val);
                                            }
                                            $tag = implode(' , ' , $tags);
                                        @endphp
                                        {{$tag}}
                                    </span></h4>
                            </div>

                                <a href="#" product_id="{{$arkWork['id']}}" class="heart artWork-remove-wishlist-btn" style="color: #DD574E;background: #016fb9;border-color: #016fb9;"><i class="fas fa-heart"></i></a>

                        </div>
                    </div>

                @empty
                    <div class="col-md-12 text-center"><h3>No art work is found in wishlist!</h3></div>
                @endforelse
            </div>
        </div>
    </div>

    <!-- Art Work End -->

@endsection


@section('custom-front-script')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================

        $(document).ready(function () {

            //removed_wishlist button
            $(".removed_wishlist_btn").click(function (e) {
                e.preventDefault();

                var _self = $(this);
               // var type = "product";
                var prod_id = $(this).attr("product_id");
                var route = "{{route('wishlist.delete',['id'=> 'id'])}}";
                route = route.replace('id', prod_id);
                var status = "remove";

                $.ajax({
                    url: route,
                    type: 'post',
                    data: {status: status },
                    success: function (response) {
                        _self.hide();
                        setTimeout(function () {
                            location.reload()
                        }, 500);
                    }
                });

            });

            // Submit form when product feature is selected
            $("#select_product_feature").change(function () {
                localStorage.setItem('product-featured', $(this).val());
                $("#product_featured_form").submit();
            });

            //=======================================================================================================


            $(document).on('click', 'a.artWork-remove-wishlist-btn', function (e) {
                e.preventDefault();


                var _self = $(this);
               // var type = "art_work";
                var prod_id = $(this).attr("product_id");
                var route = "{{route('wishlist.delete',['id'=> 'id'])}}";
                route = route.replace('id', prod_id);
                var status = "remove";

                $.ajax({
                    url: route,
                    type: 'post',
                    data: {status: status},
                    success: function (response) {
                        _self.hide();
                        setTimeout(function () {
                            location.reload()
                        }, 500);
                    }
                });

            });

            //=======================================================================================================



        });
    </script>
@endsection
