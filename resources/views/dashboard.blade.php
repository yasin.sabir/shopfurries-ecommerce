@extends('layouts.backend.app')

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{__("routes.Dashboard")}}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right" >
                        <li class="breadcrumb-item"><a href="#">{{__("routes.Home")}}</a></li>
                            <li class="breadcrumb-item active">{{__("routes.Dashboard")}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

            @if (Auth::user()->isAdmin())

                @php
                    $default_category_saleTax = \App\SiteSetting::where(['key' => 'default_sales_tax'])->first();
                    $default_weight_price     = \App\SiteSetting::where(['key' => 'default_weight_price'])->first();
                    $default_shipping_cost    = \App\SiteSetting::where(['key' => 'default_shipping_cost'])->first();
                @endphp

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{$user->count()}}</h3>

                                <p>{{__("routes.Users")}}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <a href="{{route('user.list')}}" class="small-box-footer">{{__("routes.More info")}}<i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{$product->count()}}</h3>
                                <p>{{__("routes.Products")}}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-box-open"></i>
                            </div>
                            <a href="{{route('product.list')}}" class="small-box-footer">{{__("routes.More info")}} <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{$coupon->count()}}</h3>
                                <p>{{__("routes.Coupons")}}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-ticket-alt"></i>
                            </div>
                            <a href="{{route('coupon.list')}}" class="small-box-footer">{{__("routes.More info")}} <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{$orders->count()}}</h3>

                                <p>{{__("routes.Orders")}}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-shopping-bag"></i>
                            </div>
                            <a href="{{route('order.alllist')}}" class="small-box-footer">{{__("routes.More info")}} <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-gradient-olive">
                            <div class="inner">
                                <h3>{{getInvoicesCount()}}</h3>

                                <p>{{__("routes.Invoices")}}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-file-invoice"></i>
                            </div>
                            <a href="{{route('invoice.list')}}" class="small-box-footer">{{__("routes.More info")}} <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->

                    @if(!isset($default_category_saleTax))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="callout callout-danger">
                                    <p><i class="fas fa-exclamation mr-3"></i>
                                        Please set default category sale tax from setting -> general config tab.
                                        <a href="#">view page</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(!isset($default_weight_price))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="callout callout-danger">
                                    <p><i class="fas fa-exclamation mr-3"></i>
                                        Please set default weight cost from Shop Commerce -> Shipping -> Default Setting tab.
                                        <a href="{{route('shipping.default-add')}}">view page</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(!isset($default_shipping_cost))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="callout callout-danger">
                                    <p><i class="fas fa-exclamation mr-3"></i>
                                        Please set default shipping cost from Shop Commerce -> Shipping -> Default Setting tab.
                                        <a href="{{route('shipping.default-add')}}">view page</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                @endif


            @else
                <!-- Main row -->
                <div class="row">

                    <div class="col-lg-12">
                        @if( isUserVerify_SocialLogin() == 1 )
                            <div class="custom-alert-box">
                                <ul>
                                    Verify your email first! <a href="{{route('user.edit-account',encrypt(Auth::user()->id))}}" class="white">Edit Profile</a>
                                </ul>
                            </div>
                        @endif
                    </div>


                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{getUserProductsCount()}}</h3>

                                <p>My Products</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-box-open"></i>
                            </div>
                            <a href="{{route('product.list')}}" class="small-box-footer">{{__("routes.More info")}} <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{$orders_by_user->count()}}</h3>

                                <p>My Orders</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-shopping-bag"></i>
                            </div>
                            <a href="{{route('order.mylist')}}" class="small-box-footer">{{__("routes.More info")}} <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                </div>

            @endif
                <!-- /.row (main row) -->


            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

