@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> Edit tag</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Edit</a></li>
                            <li class="breadcrumb-item active">Tag</li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <form action="{{route('product.tags.update', $tag_id)}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-md-3">
                            <label for="exampleInputEmail1">Tag Name:</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text"
                                       class="form-control form-control-sm @error('tag') is-invalid @enderror "
                                       id="category" name="tag"
                                       placeholder="Tag like.. Mens , Womens , Childs"
                                       value="{{$tag_details->name}}"
                                >
                                @error('tag')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <small>
                                    <cite title="Source Title">
                                        The name is how it appears on your site.
                                    </cite>
                                </small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-sm" value="Update">
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>

                </form>
            </div>
        </section>


    </div>


@endsection

@section('page-script')


@endsection
