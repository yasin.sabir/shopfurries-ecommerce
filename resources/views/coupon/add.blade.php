@extends('layouts.backend.app') @section('page-css')

    <style>
        .note-editable.card-block {
            height: 300px;
        }
    </style>

@endsection @section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Add Coupon </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Coupon</a></li>
                            <li class="breadcrumb-item active">New Coupon</li>
                        </ol>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <!-- form start -->
                <form method="post" action="{{route('coupon.create')}}" class="form-horizontal"
                      enctype="multipart/form-data" enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-md-9">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-default">
                                        <div class="card-body pad">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="product_nae" class="mt-1">Coupon Name</label>
                                                        <input type="text"
                                                               class="form-control  @error('code') is-invalid @enderror"
                                                               value="" name="code">
                                                        @error('code')
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="product_nae" class="mt-1">Coupon Name</label>
                                                        <input type="text"
                                                               class="form-control  @error('code') is-invalid @enderror"
                                                               value="" name="code">
                                                        @error('code')
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="product_nae" class="mt-1">Coupon Name</label>
                                                        <input type="text"
                                                               class="form-control  @error('code') is-invalid @enderror"
                                                               value="" name="code">
                                                        @error('code')
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="product_nae" class="mt-1">Coupon Name</label>
                                                        <input type="text"
                                                               class="form-control  @error('code') is-invalid @enderror"
                                                               value="" name="code">
                                                        @error('code')
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- Product Data Section Start-->


                                </div>
                            </div>

                        </div>

                        <div class="col-md-3">

                            <!-- Product Info Section Start-->
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Action</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i></button>
                                    </div>
                                </div>

                                <div class="card-body pad">
                                    <div class="form-group text-left">
                                        <label for="attr_name" class="form-check-label">Status:</label>
                                        <select class="form-control" name="status">
                                            <option value="yes">yes</option>
                                            <option value="no">no</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary btn-sm" value="Publish">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- Product Info Section End-->
                        </div>

                        <!-- /.row -->
                </form>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection @section('page-script')

@endsection
