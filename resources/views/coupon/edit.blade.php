@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> Coupons</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__('routes.Coupons')}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('coupon.list')}}">{{__('routes.List')}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">


                <form action="{{route('coupon.update',$coupon->id)}}" method="post" enctype="multipart/form-data">

                    <div class="card card-default">
                        <div class="card-body pad">

                            <div class="row">

                                <div class="col-md-6 pr-5">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Coupon Code:</label>
                                        <input type="text"
                                               class="form-control form-control-sm @error('coupon') is-invalid @enderror "
                                               id="coupon" name="coupon" value="{{$coupon->code}}">
                                        @error('coupon')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror

                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Type:</label>
                                        <select class="form-control form-control-sm" name="type" id="type">
                                            <option value="fixed" {{( $coupon->type == 'fixed') ? 'selected' : '' }}>
                                                fixed
                                            </option>
                                            <option
                                                value="percent" {{( $coupon->type == 'percent') ? 'selected' : '' }}>
                                                percent
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Use Frequency:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-sm" id="usefrequency"
                                                   name="usefrequency" value="{{$coupon->max_uses}}">
                                        </div>
                                    </div>

                                    {{--                            <div class="form-group">--}}
                                    {{--                                <label for="exampleInputEmail1">Free Delivery:</label>--}}
                                    {{--                                <select class="form-control form-control-sm" name="delivery">--}}
                                    {{--                                    <option value="1" {{( $coupon->freedelivery == 1) ? 'selected' : '' }}>yes</option>--}}
                                    {{--                                    <option value="0" {{( $coupon->freedelivery == 0) ? 'selected' : '' }}>no</option>--}}
                                    {{--                                </select>--}}
                                    {{--                            </div>--}}

                                    {{--      Exclude Categories--}}

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Status:</label>
                                        <select class="form-control form-control-sm" name="status">
                                            <option value="1">yes</option>
                                            <option value="0">no</option>
                                        </select>
                                    </div>
                                    <!-- /.card-body -->
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group pr-5">
                                        <label for="exampleInputEmail1"></label>
                                        <div class="form-group">
                                            <input type="button" class="btn btn-success" value="Generate"
                                                   onClick="generate();" tabindex="2">
                                        </div>

                                    </div>

                                    <div class="form-group pr-5">
                                        <label for="exampleInputEmail1">Discount:</label>

                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-sm"
                                                   value="{{$coupon->discount}}" id="discount" name="discount">
                                        </div>

                                    </div>

                                    {{--                           <div class="form-group pr-5">--}}
                                    {{--                                <label for="exampleInputEmail1">Max Users:</label>--}}

                                    {{--                                <div class="form-group">--}}
                                    {{--                                   <input type="text" class="form-control form-control-sm" value="{{$coupon->max_uses_user}}" id="maxusers" name="maxusers">--}}
                                    {{--                                </div>--}}

                                    {{--                           </div>--}}

                                    <div class="form-group pr-5">
                                        <label for="exampleInputEmail1">Start & Expire Date:</label>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                                </div>
                                                <input type="text" value="{{$coupon->startandexpire}}"
                                                       class="form-control float-right" name="startandexpire"
                                                       id="startandexpire">
                                            </div>
                                            <!-- /.input group -->
                                        </div>

                                        {{--                                    <div class="form-group">--}}
                                        {{--                                        <label>Exclude Categories</label>--}}
                                        {{--                                        <select class="select2bs4" multiple="multiple" name="excludecategory[]" data-placeholder="Select a Category"--}}
                                        {{--                                                style="width: 100%;">--}}
                                        {{--                                            <option value="">Select Category</option>     --}}
                                        {{--                                     @if(isset($exclude_categories))  --}}
                                        {{--                                        @foreach( $exclude_categories as $num)--}}
                                        {{--                                            @foreach($category as $categories)--}}
                                        {{--                                            <option value="{{ $categories->id }}" {{ ($categories->id == $num)  ? 'selected' : ''}}>{{ $categories->Name }}</option> --}}
                                        {{--                                            @endforeach--}}
                                        {{--                                        @endforeach--}}
                                        {{--                                        @else--}}
                                        {{--                                        @foreach($category as $categories)--}}
                                        {{--                                            <option value="{{ $categories->id }}">{{ $categories->Name }}</option> --}}
                                        {{--                                            @endforeach--}}

                                        {{--                                    @endif--}}
                                        {{--                                        </select>--}}
                                        {{--                                    </div>--}}
                                    </div>

                                    <div class="form-group">
                                        <br>
                                        <input type="submit" class="btn btn-primary btn-sm" value="Update Coupon">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->
    <!-- /.modal -->
    </div>
@endsection

@section('page-script')

    <script type="text/javascript">

        $('#type').on('change', function () {
            if (this.value === "percent") {
                $('#discount').attr('maxlength', '2');
                if ($('#discount').val() > 100) {
                    $('#discount').val('100');
                }
            } else {
                $('#discount').removeAttr('maxlength');
            }
        });

        $('.select2bs4').select2()
        $('input[name="startandexpire"]').daterangepicker();

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("category_id");
            var route = '{{ route('category.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            })
        });


        $('#example1').DataTable({
            "paging": true,
            "pageLength": 7,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            jQuery('[name="coupon"]').val(randomPassword(20))
        }

    </script>

@endsection
