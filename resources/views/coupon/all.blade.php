@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Coupons")}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"> {{__("routes.Coupons")}} </li>
                            <li class="breadcrumb-item active"><a href="{{route('coupon.list')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>



        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Add New Coupon")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{route('coupon.create')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 pr-5">

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">{{__("routes.Coupon Code")}}:</label>
                                                <input type="text"
                                                       class="form-control form-control-sm @error('coupon') is-invalid @enderror "
                                                       id="coupon" name="coupon">
                                                @error('coupon')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror

                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">{{__("routes.Type")}}:</label>
                                                <select class="form-control form-control-sm" name="type" id="type">
                                                    <option value="fixed">{{__("routes.fixed")}}</option>
                                                    <option value="percent">{{__("routes.percent")}}</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">{{__("routes.Use Frequency")}}:</label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control form-control-sm"
                                                           id="usefrequency"
                                                           name="usefrequency">
                                                </div>
                                            </div>

                                            {{--                                    <div class="form-group">--}}
                                            {{--                                        <label for="exampleInputEmail1">Free Delivery:</label>--}}
                                            {{--                                        <select class="form-control form-control-sm" name="delivery">--}}
                                            {{--                                            <option value="1">yes</option>--}}
                                            {{--                                            <option value="0">no</option>--}}
                                            {{--                                        </select>--}}
                                            {{--                                    </div>--}}

                                            {{--                                    <div class="form-group">--}}
                                            {{--                                        <label>Exclude Products</label>--}}
                                            {{--                                        <select class="select2bs4" multiple="multiple" name="excludeproduct[]"--}}
                                            {{--                                                data-placeholder="Select a Product" style="width: 100%;">--}}
                                            {{--                                            <option value="">Select Product</option>--}}
                                            {{--                                            @foreach($product as $product)--}}
                                            {{--                                                <option value="{{$product->id}}">{{$product->title}}</option>--}}
                                            {{--                                            @endforeach--}}
                                            {{--                                        </select>--}}
                                            {{--                                    </div>--}}

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">{{__("routes.Status")}}:</label>
                                                <select class="form-control form-control-sm" name="status">
                                                    <option value="1">{{__("routes.Yes")}}</option>
                                                    <option value="0">{{__("routes.No")}}</option>
                                                </select>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group pr-5">
                                                <label for="exampleInputEmail1"></label>
                                                <div class="form-group">
                                                    <input type="button" class="btn btn-success" value="{{__("routes.Generate")}}"
                                                           onClick="generate();"
                                                           tabindex="2">
                                                </div>
                                            </div>

                                            <div class="form-group pr-5">
                                                <label for="exampleInputEmail1">{{__("routes.Discount")}}:</label>
                                                <input type="text" class="form-control form-control-sm" id="discount"
                                                       name="discount">
                                            </div>

                                            {{--                                    <div class="form-group pr-5">--}}
                                            {{--                                        <label for="exampleInputEmail1">Max Users:</label>--}}
                                            {{--                                        <input type="text" class="form-control form-control-sm" id="maxusers"--}}
                                            {{--                                               name="maxusers">--}}
                                            {{--                                    </div>--}}

                                            <div class="form-group pr-5">
                                                <label for="exampleInputEmail1">{{__("routes.Start & Expire Date")}}:</label>

                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="far fa-calendar-alt"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control float-right"
                                                           name="startandexpire"
                                                           id="startandexpire">
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>

                                                {{--                                        <div class="form-group">--}}
                                                {{--                                            <label>Exclude Categories</label>--}}
                                                {{--                                            <select class="select2bs4" multiple="multiple" name="excludecategory[]"--}}
                                                {{--                                                    data-placeholder="Select a Category"--}}
                                                {{--                                                    style="width: 100%;">--}}
                                                {{--                                                <option value="">Select Category</option>--}}
                                                {{--                                                @foreach($category as $category)--}}
                                                {{--                                                    <option value="{{$category->id}}">{{$category->Name}}</option>--}}
                                                {{--                                                @endforeach--}}
                                                {{--                                            </select>--}}
                                                {{--                                        </div>--}}
                                                {{--                                    </div>--}}

                                                <div class="form-group">
                                                    <br>
                                                    <input type="submit" class="btn btn-primary btn-sm"
                                                           value="{{__("routes.Add New Coupon")}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.List")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped nowrap">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>{{__("routes.Code")}}</th>
                                        <th>{{__("routes.Type")}}</th>
                                        <th>{{__("routes.Discount")}}</th>
                                        <th>{{__("routes.Status")}}</th>
                                        <th>{{__("routes.Action")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $num=1; @endphp
                                    @forelse($coupon as $coupon)
                                        <tr>
                                            <td>{{$num}}</td>
                                            <td>{{$coupon->code}} </td>
                                            <td>{{$coupon->type}}</td>
                                            <td>{{$coupon->discount}}</td>
                                            <td> <?php echo ($coupon->status == 1) ? 'Active' : 'Not Active' ?></td>
                                            <td>
                                                <a href="{{route('coupon.edit',encrypt($coupon->id))}}"
                                                   class="btn btn-info btn-sm mr-3">{{__("routes.Edit")}}</a>
                                                <a href="#_" class="custom-delete-btn btn btn-danger btn-sm"
                                                   coupon_id="{{$coupon->id}}"
                                                   coupon_name="{{ $coupon->code }}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @php $num++; @endphp
                                    @empty
                                        <tr>
                                            <td colspan="7" class="text-center">{{__("routes.No Coupon Found")}}</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Coupon</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete this coupon</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection

@section('page-script')

    <script type="text/javascript">

        //=======================================================================================================

        $('#type').on('change', function () {
            if (this.value === "percent") {
                $('#discount').attr('maxlength', '2');
                if ($('#discount').val() > 100) {
                    $('#discount').val('100');
                }
            } else {
                $('#discount').removeAttr('maxlength');
            }
        });

        $('.select2bs4').select2()
        $('input[name="startandexpire"]').daterangepicker();

        //=======================================================================================================

        // $("input[name='checkAll']").click(function(){
        //     $("input[name='faq']").not(this).prop('checked', this.checked);
        // });

        // =======================================================================================================

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("coupon_id");
            var route = '{{ route('coupon.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            })
        });

        //=======================================================================================================

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 7,
            "autoWidth": false,
        });

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        //=======================================================================================================

        function generate() {
            jQuery('[name="coupon"]').val(randomPassword(20))
        }

        //=======================================================================================================

    </script>

@endsection
