@if ($paginator->hasPages())
    <div class="product-pagination">
        <ul class="pagination">
            {{--    <ul class="pagination" role="navigation">--}}
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')"><a href="#" aria-hidden="true"><i
                            class="far fa-angle-left"></i></a></li>
                {{--            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">--}}
                {{--                <span class="page-link" aria-hidden="true">&lsaquo;</span>--}}
                {{--            </li>--}}
            @else
                <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev"
                       aria-label="@lang('pagination.previous')"><i class="far fa-angle-left"></i></a></li>
                {{--            <li class="page-item">--}}
                {{--                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>--}}
                {{--            </li>--}}
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><a href="#_">{{ $element }}</a></li>
                    {{--                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>--}}
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li><a href="#" class="active">{{ $page }}</a></li>
                            {{--                        <li class="page-item custom-page-item-active active" aria-current="page"><span class="page-link">{{ $page }}</span></li>--}}
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                            {{--                        <li class="page-item custom-page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>--}}
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li><a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><i
                            class="far fa-angle-right"></i></a></li>
                {{--            <li class="page-item">--}}
                {{--                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>--}}
                {{--            </li>--}}
            @else
                <li><a href="{{ $paginator->nextPageUrl() }}" class="disabled" aria-disabled="true"
                       aria-label="@lang('pagination.next')"><i class="far fa-angle-right"></i></a></li>
                {{--            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">--}}
                {{--                <span class="page-link" aria-hidden="true">&rsaquo;</span>--}}
                {{--            </li>--}}
            @endif
            {{--    </ul>--}}
        </ul>
    </div>
@endif


{{--<div class="product-pagination">--}}
{{--    <ul class="pagination">--}}
{{--        <li><a href="#" class="active">1</a></li>--}}
{{--        <li><a href="#">2</a></li>--}}
{{--        <li><a href="#">3</a></li>--}}
{{--        <li><a href="#" class="doted">....</a></li>--}}
{{--        <li><a href="#"><i class="far fa-angle-right"></i></a></li>--}}
{{--    </ul>--}}
{{--</div>--}}
