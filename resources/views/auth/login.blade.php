@extends('front-layout.front-layout-auth.app')

@section('title')
    Login
@endsection

@section('custom-front-css')
    <style>
        .row.main-detailse-products {
            margin-top: 100px;
        }

        .Product-detailse.logIN-wrep{
            margin-top: 50px;
            margin-bottom: 50px;
        }

    </style>
@endsection

@section('Main')


    <!-- Product-detailse START -->
    <div class="Product-detailse logIN-wrep">
        <div class="container">
            <div id="Section"></div>
            <div class="row main-detailse-products mt-0">
                <div class="col-lg-6 offset-lg-3 text-center">

                    <div class="company-logo">
                        <img src="{{asset('front-end/assets/img/ShopFurries-LOGO.png')}}" width="150" alt="logo" srcset="">
                    </div>

                    <div class="logInForm SiUP">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" href="{{route('login')}}#Section" role="tab" aria-controls="pills-home" aria-selected="true">login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" href="{{route('register')}}#Section" role="tab" aria-controls="pills-profile" aria-selected="false">sign up  </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <h4>Welcome Back,</h4>
                                    @if(Auth::check() && !Auth::user()->hasVerifiedEmail())
                                        <p>Please verify your email address. An email containing verification instructions was sent to {{ Auth::user()->email }}.</p>
                                    @endif
                                    @if(session('verified'))
                                        <p>You've successfully verified your email!</p>
                                    @endif
                                    <div class="row">
                                        <div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2 col-11">
                                            <div class="logINform text-center">

                                                <div class="email mb-0">
                                                    <label for="email">Email <span>*</span></label> <br>
                                                    <input type="email" placeholder="E.g Howardjones@gmail.com" name="email" class="@error('email') is-invalid @enderror" id="email">
                                                    <img src="{{ asset('front-end/assets/img/envelops.svg')}}" alt="">
                                                </div>
                                                @error('email')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror


                                                <div class="password mt-3">
                                                    <label for="Password">Password <span>*</span></label> <br>
                                                    <input type="password" id="Password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                                    <img src="{{ asset('front-end/assets/img/lock.svg')}}" alt="">
                                                </div>
                                                @error('password')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                <div class="login-section">
                                                    <button type="submit" class="THEME-BTN pl-2 pr-2 w-100">
                                                        {{ __('Login') }}
                                                        <i class="fal fa-angle-right"></i>
                                                    </button>
                                                </div>


                                                @if (Route::has('password.request'))
                                                    <a class="Fpassword" href="{{ route('password.request') }}">
                                                        {{ __('Forgot Your Password?') }}
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="col-lg-8 offset-lg-2">
                                            <div class="log-inMoreDetailse">
                                                <a href="{{url('login/google')}}" class="google"><i class="fab fa-google"></i> Google</a>
                                                <a href="{{url('login/facebook')}}" class="facebook"><i class="fab fa-facebook-f"></i> Facebook</a>
                                                <a href="{{url('login/deviantart')}}" class="deviantart"><i class="fab fa-deviantart"></i> Deviantart</a>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product-detailse END -->

@endsection


@section('custom-front-script')

@endsection
