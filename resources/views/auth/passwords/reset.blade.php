@extends('front-layout.app')

@section('title')
    Login
@endsection

@section('custom-front-css')
    <style>
        .row.main-detailse-products {
            margin-top: 70px;
        }
    </style>
@endsection

@section('Main')

{{--    <!-- product-title GROUP -->--}}
{{--    <div class="product-titleGroup faq-area">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-3">--}}
{{--                    <div class="title-textLEFT">--}}
{{--                        <p>Home <i class="far fa-angle-right"></i> <span style="color: #000;font-weight: 700;">My Accounts  </span></p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-5 text-center">--}}
{{--                    <div class="title-text">--}}
{{--                        <h1>Reset Account Password</h1>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3"></div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <!-- product-title GROUP -->--}}

    <!-- Product-detailse START -->
    <div class="Product-detailse logIN-wrep">
        <div class="container">
            <div id="Section"></div>
            <div class="row main-detailse-products">

                <div class="col-lg-6 offset-lg-3 text-center">

                    <div class="company-logo">
                        <img src="{{asset('front-end/assets/img/ShopFurries-LOGO.png')}}" width="150" alt="logo" srcset="">
                    </div>


                    <div class="logInForm SiUP">

                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item w-100">
                                <a class="nav-link active" id="pills-home-tab" href="{{route('login')}}#Section" role="tab" aria-controls="pills-home" aria-selected="true" style="border-right: 0px !important;">Reset Password</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <form method="POST" action="{{ route('password.update') }}">
                                    @csrf
                                    <input type="hidden" name="token" value="{{ $token }}">

                                    <div class="row p-3 pb-5">

                                        <div class="col-lg-12 col-md-12">
                                            <div class="logINform text-center logINform text-center w-75 ml-auto mr-auto">

                                                <div class="emails mb-0">
                                                    <label for="email">E-Mail Address <span>*</span></label> <br>
                                                    <input type="email" placeholder="E.g Howardjones@gmail.com" name="email" class="@error('email') is-invalid @enderror" id="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                                    <img src="{{ asset('front-end/assets/img/envelops.svg')}}" alt="">
                                                </div>
                                                @error('email')
                                                <span class="invalid-feedback d-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                <div class="password mt-2 mb-0">
                                                    <label for="Password">Password <span>*</span></label> <br>
                                                    <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password"  autocomplete="new-password">
                                                    <img src="{{asset('front-end/assets/img/lock.svg')}}" alt="">
                                                </div>

                                                @error('password')
                                                <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                <div class="password mt-2">
                                                    <label for="Password">Confirm Password <span>*</span></label> <br>
                                                    <input id="password-confirm" type="password" class="" name="password_confirmation"  autocomplete="new-password">
                                                    <img src="{{asset('front-end/assets/img/lock.svg')}} " alt="">
                                                </div>


                                                <div class="reset-password-section">
                                                    <button type="submit" class="THEME-BTN mt-2 pl-2 pr-2 w-100">
                                                        {{ __('Reset Password') }}
                                                        <i class="fal fa-angle-right"></i>
                                                    </button>
                                                </div>



                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product-detailse END -->

@endsection


@section('custom-front-script')

@endsection
