@extends('front-layout.front-layout-auth.app')

@section('title')
    Login
@endsection


@section('custom-front-css')
    <style>
        .row.main-detailse-products {
            margin-top: 70px;
        }

        /*.Product-detailse .logIN-wrep{*/
        /*   margin-bottom: 50px !important;*/
        /*}*/


    </style>
@endsection

@section('Main')

    <!-- Product-detailse START -->
    <div class="Product-detailse .logIN-wrep my-0">
        <div class="container">
            <div id="Section"></div>
            <div class="row main-detailse-products">

                <div class="col-lg-6 offset-lg-3 text-center">

                    <div class="company-logo">
                        <img src="{{asset('front-end/assets/img/ShopFurries-LOGO.png')}}" width="150" alt="logo" srcset="">
                    </div>

                    <div class="logInForm SiUP">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item w-100">
                                <a class="nav-link active" id="pills-home-tab" href="{{route('login')}}#Section" role="tab" aria-controls="pills-home" aria-selected="true" style="border-right: 0px !important;">Reset Password</a>
                            </li>
                        </ul>

                        @if (session('status'))
                            <div class="alert custom-fv-success-alert" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <form method="POST" action="{{ route('password.email') }}">
                                    @csrf

                                    <div class="row p-3">

                                        <div class="col-lg-12 col-md-12">
                                            <div class="logINform text-center logINform text-center w-75 ml-auto mr-auto">

                                                <div class="emails">
                                                    <label for="email">E-Mail Address <span>*</span></label> <br>
                                                    <input type="email" placeholder="E.g Howardjones@gmail.com" name="email" class="@error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                    <img src="{{ asset('front-end/assets/img/envelops.svg')}}" alt="">
                                                </div>
                                                @error('email')
                                                <span class="invalid-feedback d-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                <div class="reset-password-section">
                                                    <button type="submit" class="THEME-BTN pl-2 pr-2 w-100">
                                                        {{ __('Send Password Reset Link') }}
                                                        <i class="fal fa-angle-right"></i>
                                                    </button>
                                                </div>


                                                @if (Route::has('login') || Route::has('register'))
                                                    <div class="row mb-0 pb-0">

                                                        <div class="col-md-6 ">
                                                            <a class="Fpassword text-right" href="{{ route('login') }}">
                                                                {{ __('Login ') }}
                                                            </a>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <a class="Fpassword text-left" href="{{ route('register') }}">
                                                                {{ __('Signup ?') }}
                                                            </a>
                                                        </div>

                                                    </div>
                                                @endif



                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product-detailse END -->

@endsection


@section('custom-front-script')

@endsection
