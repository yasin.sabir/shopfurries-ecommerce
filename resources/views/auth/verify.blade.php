@extends('front-layout.front-layout-auth.app')

@section('title')
    Login
@endsection


@section('custom-front-css')
    <style>
        .row.main-detailse-products {
            margin-top: 70px;
        }
    </style>
@endsection

@section('Main')

    <!-- Product-detailse START -->
    <div class="Product-detailse logIN-wrep">
        <div class="container">
            <div id="Section"></div>
            <div class="row main-detailse-products">

                <div class="col-lg-6 offset-lg-3 text-center">

                    <div class="company-logo">
                        <img src="{{asset('front-end/assets/img/ShopFurries-LOGO.png')}}" width="150" alt="logo" srcset="">
                    </div>

                    <div class="logInForm SiUP">

                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item w-100">
                                <a class="nav-link active" id="pills-home-tab" href="{{route('login')}}#Section"
                                   role="tab" aria-controls="pills-home" aria-selected="true"
                                   style="border-right: 0px !important;">{{ __('Verify Your Email Address') }}</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                 aria-labelledby="pills-home-tab">
                                <div class="row p-3 pb-5">

                                    <div class="col-lg-12 col-md-12">
                                        <div class="logINform text-center logINform text-center">

                                            <div class="emails">
                                                <p>
                                                    {{ __('Before proceeding, please check your email for a verification link.') }}
                                                    <br>{{ __('If you did not receive the email') }}
                                                </p>
                                                <p>
                                                   <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                                                </p>
                                                <p class="verify-logout-section">
                                                    <a class="THEME-BTN pl-2 pr-2 d-inline-block w-50" href="{{route('logout')}}">Logout
                                                        <i class="fal fa-angle-right"></i>
                                                    </a>
                                                </p>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product-detailse END -->

@endsection


@section('custom-front-script')

@endsection
