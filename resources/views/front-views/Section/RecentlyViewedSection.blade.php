@php
    $recently_viewed = !empty(Cookie::get('viewed_products_items')) ? unserialize(Cookie::get('viewed_products_items')) : [];

@endphp

@if(isset($recently_viewed) && count($recently_viewed) > 0 )

    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-center">
                    <h1 class="mb-0">Recently Viewed Product</h1> <br>

                        <div class="products">
                            <div class="row">

                                @forelse($recently_viewed as $key => $val)

                                    <div class="col-lg-3 col-md-6">
                                        @php
                                            $product            = \App\product::find($val);
                                            $product_meta       = \App\products_meta::where(['product_id' => $product->id])->get();

                                            $current_time       = time();
                                            $product_created_at = strtotime($product->created_at);
                                            $diff_days          = ( ($current_time - $product_created_at) / 86400);
                                        @endphp
                                        <div class="product-box">

                                            @if($diff_days <= 5)
                                                <div class="ribbon-wrapper ribbon-lg">
                                                    <div class="ribbon bg-gradient-danger">
                                                        New Product
                                                    </div>
                                                </div>
                                            @endif

                                            @forelse($product_meta as $key => $meta)
                                                @if($meta['product_meta'] == "sale_price")
                                                    @if(!empty($meta['product_meta_value']))
                                                        <div class="ribbon-wrapper ribbon-lg">
                                                            <div class="ribbon bg-gradient-warning">
                                                                Sale Offer
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endif
                                            @empty
                                            @endforelse



                                            @if($product != 'null')
                                                @php
                                                    $image = "";

                                                    $ff = @unserialize($product->image);

                                                    if(!is_array($ff)){
                                                        $image = $ff;
                                                    }else{
                                                        $image = $product->image;
                                                    }
                                                    $default_size = \App\SiteSetting::where(['key' => 'product_image_size'])->first();
                                                @endphp

                                                @if(!is_array($ff))

                                                    @if(!empty($val))
                                                        <img ff="af" src="{{asset('storage/'.$product->image)}}" id="product-img"/>
                                                    @else
                                                        <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}" id="product-img"/>
                                                    @endif
                                                @else
                                                    <img src="{{ asset('storage/'.$ff[$default_size->value])}}" >
                                                @endif

                                            @else
                                                <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"/>
                                            @endif

                                            <div class="product-btns">
                                                <a href="{{route('single_product',['name'=>$product->title,'id' => $product->id])}}"
                                                   product="{{ucfirst($product->title)}}" class="THEME-BTN">View Product</a>
                                            </div>
                                            <div class="productBox-content text-left">
                                                <h5>{{ucfirst($product->title)}}</h5>
                                                @forelse($product_meta as $key => $meta)
                                                    @if($meta['product_meta'] == "sale_price")
                                                        @if(!empty($meta['product_meta_value']))
                                                            <div class="custom-shopProduct-price">
                                                                <span
                                                                    style="text-decoration: line-through;font-size: 12px;top:-15px">
                                                                    ${{number_format($product->price,2)}}
                                                                </span>
                                                                <span>
                                                                    ${{number_format($meta['product_meta_value'],2)}}
                                                                </span>
                                                            </div>
                                                        @else
                                                            <span>
                                                                ${{number_format($product->price,2)}}
                                                            </span>
                                                        @endif
                                                    @endif
                                                @empty
                                                @endforelse
                                                <p>{{strip_tags($product->description)}}</p>

                                            </div>

                                        </div>
                                    </div>

                                @empty
                                    <div class="text-center">
                                        <h3>No Recently Product Viewed</h3>
                                    </div>
                                @endforelse

                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

@endif
