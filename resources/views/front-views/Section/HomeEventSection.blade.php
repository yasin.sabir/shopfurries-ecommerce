@php
    $events = App\Event::where(['status' => 'on'])->get();
@endphp
<!-- events-area START -->

@if(isset($events) && count($events) >0 )

    <div class="events-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h1>OUR NEXT EVENTS</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="event-slidet owl-carousel">

                        @forelse($events as $key => $event)

                            <div class="event-slider-iteam">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <img src="{{ asset('front-end/assets/img/map.jpg')}}" alt="">
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="event-iteam-content">
                                            <h4><a href="{{route('single_event' , ['name'=> strtolower( str_replace( ' ' , '_' , $event->name) ) ,'event_id' => custom_base64_encode($event->id) ])}}">{{strtoupper($event->city)}} {{strtoupper($event->country)}}</a> <br> <span>{{$event->timing}}</span></h4>
                                            <p>{{substr(strip_tags($event->details),0,100)}}.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @empty

                        @endforelse

                        {{--                    <div class="event-slider-iteam">--}}
                        {{--                        <div class="row">--}}
                        {{--                            <div class="col-lg-6 col-md-6">--}}
                        {{--                                <img src="{{ asset('front-end/assets/img/map.jpg')}}" alt="">--}}
                        {{--                            </div>--}}
                        {{--                            <div class="col-lg-6 col-md-6">--}}
                        {{--                                <div class="event-iteam-content">--}}
                        {{--                                    <h4>BERLIN, GERMANY <br> <span>Thu, Mar 26, 10:00am</span></h4>--}}
                        {{--                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magn</p>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                    </div>--}}
                        {{--                    <div class="event-slider-iteam">--}}
                        {{--                        <div class="row">--}}
                        {{--                            <div class="col-lg-6 col-md-6">--}}
                        {{--                                <img src="{{ asset('front-end/assets/img/map.jpg')}} " alt="">--}}
                        {{--                            </div>--}}
                        {{--                            <div class="col-lg-6 col-md-6">--}}
                        {{--                                <div class="event-iteam-content">--}}
                        {{--                                    <h4>BERLIN, GERMANY <br> <span>Thu, Mar 26, 10:00am</span></h4>--}}
                        {{--                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magn</p>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                    </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- events-area END -->

@endif

