@php

    $products       = \App\product::all();
    $product_artist = [];

    foreach ($products as $key => $product){
        $product_meta = \App\products_meta::where(['product_id' => $product->id])->where(['product_meta' => 'artist'])->get();

        foreach ($product->productReviews as $key1 => $reviews){

            if($reviews->flag === "perfect"){

                 foreach ($product_meta as $key2 => $val){
                    if( $val->product_meta_value != "" ){
                        $product_artist[$product->id] = $val->product_meta_value;
                    }
                 }
            }
        }
    }

@endphp

<div class="row">
    <div class="col-lg-12 text-center">
        <div class="item-slider owl-carousel">

            @forelse($product_artist as $key => $val)
                @php
                    $artist_detail = \App\Artist::where(['id' => $val])->first();
                @endphp

                @if($artist_detail['image'] != "")
                    <div class="single-item">
                        <h1 style="background-image: url('{{asset('storage/'.$artist_detail['image'])}}');background-position:center;background-size: cover;background-repeat: no-repeat;">
                        </h1>
                        <p>{{ucfirst($artist_detail['name'])}}</p>
                    </div>
                @endif

            @empty
                <div class="text-center">
                    <h3>No Seller found!</h3>
                </div>
            @endforelse

        </div>
    </div>
</div>
