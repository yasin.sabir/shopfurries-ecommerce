@php
    $cr = \App\CustomerReviews::where(['status' => 'on'])->get();
@endphp
<!-- clients-area START -->

@if(isset($cr) && count($cr) > 0)

    <div class="client-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h1>OUR HAPPY CUSTOMERS</h1>
                    </div>
                </div>
            </div>
            <div class="row">

                @forelse($cr as $key => $val)
                    <div class="col-lg-3 col-md-6 col-sm-6 text-center">
                        <div class="customers-blk">
                            <div class="customers-img">
                                @if( $val->image != null)
                                    <img class="client-main-img" src="{{ asset('storage/'.$val->image)}}" alt="">
                                @else
                                    <img class="client-main-img" src="{{ asset('images/placeholders/Profile_avatar_placeholder_lg.png')}}" alt="">
                                @endif
                            </div>
                            <div class="customers-wrepper">
                                <h5><span>{{ucfirst($val->name)}}</span>

                                    @switch($val->rating)

                                        @case('1')
                                            <ul>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                            </ul>
                                        @break

                                        @case('2')
                                            <ul>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                            </ul>
                                        @break

                                        @case('3')
                                            <ul>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                            </ul>
                                        @break

                                        @case('4')
                                            <ul>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                            </ul>
                                        @break

                                        @case('5')
                                            <ul>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                                <li><a href="#"><i class="fas fa-star"></i></a></li>
                                            </ul>
                                        @break

                                        @default
                                        <ul>
                                            <li><a href="#"><i class="fas fa-star"></i></a></li>
                                            <li><a href="#"><i class="fas fa-star"></i></a></li>
                                            <li><a href="#"><i class="fas fa-star"></i></a></li>
                                            <li><a href="#"><i class="fas fa-star"></i></a></li>
                                            <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        </ul>

                                    @endswitch

{{--                                    <ul>--}}
{{--                                        <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                        <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                        <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                        <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                        <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    </ul>--}}
                                </h5>
                                <p>{{substr(strip_tags($val->detail),0,100)}}</p>
                            </div>
                        </div>
                    </div>
                @empty

                @endforelse


{{--                <div class="col-lg-3 col-md-6 col-sm-6 text-center">--}}
{{--                    <div class="customers-blk">--}}
{{--                        <div class="customers-img">--}}
{{--                            <img class="client-main-img" src="{{ asset('front-end/assets/img/client2.jpg')}}" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="customers-wrepper">--}}
{{--                            <h5><span>Peter</span>--}}
{{--                                <ul>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                </ul>--}}
{{--                            </h5>--}}
{{--                            <p>I'm going to do more business with them very soon. FANTASTCI SERVICE!--}}
{{--                                AND ON TIME.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-6 text-center">--}}
{{--                    <div class="customers-blk">--}}
{{--                        <div class="customers-img">--}}
{{--                            <img class="client-main-img" src="{{ asset('front-end/assets/img/client3.jpg')}}" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="customers-wrepper">--}}
{{--                            <h5><span>Peter</span>--}}
{{--                                <ul>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                </ul>--}}
{{--                            </h5>--}}
{{--                            <p>I'm going to do more business with them very soon. FANTASTCI SERVICE!--}}
{{--                                AND ON TIME.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-6 text-center">--}}
{{--                    <div class="customers-blk">--}}
{{--                        <div class="customers-img">--}}
{{--                            <img class="client-main-img" src="{{ asset('front-end/assets/img/client4.jpg')}}" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="customers-wrepper">--}}
{{--                            <h5><span>Peter</span>--}}
{{--                                <ul>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                </ul>--}}
{{--                            </h5>--}}
{{--                            <p>I'm going to do more business with them very soon. FANTASTCI SERVICE!--}}
{{--                                AND ON TIME.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-6 text-center">--}}
{{--                    <div class="customers-blk">--}}
{{--                        <div class="customers-img">--}}
{{--                            <img class="client-main-img" src="{{ asset('front-end/assets/img/client5.jpg')}}" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="customers-wrepper">--}}
{{--                            <h5><span>Peter</span>--}}
{{--                                <ul>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                </ul>--}}
{{--                            </h5>--}}
{{--                            <p>I'm going to do more business with them very soon. FANTASTCI SERVICE!--}}
{{--                                AND ON TIME.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-6 text-center">--}}
{{--                    <div class="customers-blk">--}}
{{--                        <div class="customers-img">--}}
{{--                            <img class="client-main-img" src="{{ asset('front-end/assets/img/client6.jpg')}}" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="customers-wrepper">--}}
{{--                            <h5><span>Peter</span>--}}
{{--                                <ul>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                </ul>--}}
{{--                            </h5>--}}
{{--                            <p>I'm going to do more business with them very soon. FANTASTCI SERVICE!--}}
{{--                                AND ON TIME.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-6 text-center">--}}
{{--                    <div class="customers-blk">--}}
{{--                        <div class="customers-img">--}}
{{--                            <img class="client-main-img" src="{{ asset('front-end/assets/img/client.jpg')}}" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="customers-wrepper">--}}
{{--                            <h5><span>Peter</span>--}}
{{--                                <ul>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                </ul>--}}
{{--                            </h5>--}}
{{--                            <p>I'm going to do more business with them very soon. FANTASTCI SERVICE!--}}
{{--                                AND ON TIME.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-6 text-center">--}}
{{--                    <div class="customers-blk">--}}
{{--                        <div class="customers-img">--}}
{{--                            <img class="client-main-img" src="{{ asset('front-end/assets/img/client7.jpg')}}" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="customers-wrepper">--}}
{{--                            <h5><span>Peter</span>--}}
{{--                                <ul>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                    <li><a href="#"><i class="fas fa-star"></i></a></li>--}}
{{--                                </ul>--}}
{{--                            </h5>--}}
{{--                            <p>I'm going to do more business with them very soon. FANTASTCI SERVICE!--}}
{{--                                AND ON TIME.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </div>
        </div>
    </div>
    <!-- clients-area END -->

@endif
