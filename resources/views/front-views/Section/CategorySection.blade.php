@php
    $categories = App\Category::all();
@endphp
<div class="row">
    <div class="col-lg-12 text-center">
        <div class="item-slider owl-carousel">

            @forelse($categories as $key => $val)
                <div class="single-item">
                    <h1 style="background-image: url('{{asset('storage/'.$val->Image)}}');background-position:center;background-size: cover;background-repeat: no-repeat;">
                        {{--<img src="{{ asset('storage/'.$val->Image)}}" alt="">--}}
                    </h1>
                    <p><a target="_blank" href="{{route('category_products' , ['name' => strtolower( str_replace(' ' , '_', $val->Name) ) , 'cat_id' => custom_base64_encode($val->id)] ) }}">{{ucfirst($val->Name)}}</a></p>
                </div>
            @empty
            @endforelse

        </div>
    </div>
</div>
