@php

    use \App\Product;

    $wishlist_products = [];

    if( Auth::check()){
        $current_user_id = Auth::user()->id;
        $wishlist = \App\Wishlist::where(['user_id' => $current_user_id])->get();

        $wishlist_products['user_id'] = $current_user_id;

        foreach ($wishlist as $key => $v){
            $wishlist_products['product'][] = $v->product_id;
        }
    }


    $products = product::all();
    $flags = [];

    foreach ($products as $key => $product){

        $prods = \App\product::find($product->id);

        foreach ($prods->productReviews as $key1 => $reviews){
            if($reviews->flag === "perfect"){
                $flags [$product->id][] =$reviews->flag;
            }
        }
    }

    $flags_val = [];

    foreach ($flags as $key => $val){
        $flags_val [$key] = count($val);
    }

    // customVarDump_die($flags_val);

    $upto_review = \App\SiteSetting::where(['key' => 'upto_review'])->first();

@endphp

<div class="products">
    <div class="row">

            @forelse($flags_val as $key => $val)

                @if($val >= $upto_review->value)
                    <div class="col-lg-3 col-md-6">
                        @php
                            $product            = \App\product::find($key);
                            $product_meta       = \App\products_meta::where(['product_id' => $product->id])->get();

                            $current_time       = time();
                            $product_created_at = strtotime($product->created_at);
                            $diff_days          = ( ($current_time - $product_created_at) / 86400);
                        @endphp
                        <div class="product-box">

                            @if($diff_days <= 5)
                                <div class="ribbon-wrapper ribbon-lg">
                                    <div class="ribbon bg-gradient-danger">
                                        New Product
                                    </div>
                                </div>
                            @endif

                            @forelse($product_meta as $key => $meta)
                                @if($meta['product_meta'] == "sale_price")
                                    @if(!empty($meta['product_meta_value']))
                                        <div class="ribbon-wrapper ribbon-lg">
                                            <div class="ribbon bg-gradient-warning">
                                                Sale Offer
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            @empty
                            @endforelse

                            @if($product != 'null')
                                <img src="{{ asset('storage/'.$product->image)}}" alt="{{ucfirst($product->title)}}">
                            @else
                                <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"/>
                            @endif

                            <div class="product-btns">
                                <a href="{{route('single_product',['name'=>$product->title,'id' => $product->id])}}"
                                   product="{{ucfirst($product->title)}}" class="THEME-BTN">Buy Now</a>

                                @if(isset($wishlist_products['product']) && count($wishlist_products['product']) > 0)
                                    @if(in_array($product->id , $wishlist_products['product']))
                                        <a href="" product_id="{{$product->id}}"
                                           class="THEME-BTN transparent-btn remove-wishlist-btn">
                                            <i class="fas fa-heart" style="color:#DD574E"></i>Remove</a>
                                    @else
                                        <a href="" product_id="{{$product->id}}"
                                           class="THEME-BTN transparent-btn add-wishlist-btn"><i
                                                class="fal fa-heart"></i>
                                            Add to Wishlist</a>
                                    @endif
                                @else
                                    @if(Auth::check())
                                        <a href="" product_id="{{$product->id}}"
                                           class="THEME-BTN transparent-btn add-wishlist-btn"><i
                                                class="fal fa-heart"></i>
                                            Add to Wishlist</a>
                                    @endif
                                @endif

                            </div>
                            <div class="productBox-content">
                                <h5> {{ucfirst($product->title)}}</h5>
                                @forelse($product_meta as $key => $meta)
                                    @if($meta['product_meta'] == "sale_price")

                                        @if(!empty($meta['product_meta_value']))

                                            <div class="custom-shopProduct-price">
                                                <span
                                                    style="text-decoration: line-through;font-size: 12px;top:-15px">
                                                    ${{number_format($product->price,2)}}
                                                </span>
                                                <span>
                                                    ${{number_format($meta['product_meta_value'],2)}}
                                                </span>
                                            </div>
                                        @else
                                            <span>
                                                ${{number_format($product->price,2)}}
                                            </span>
                                        @endif
                                    @endif
                                @empty
                                @endforelse
                                <p>{{strip_tags($product->description)}}</p>
                                <div class="row mt-2">
                                    <div class="col-lg-12 d-flex">
                                        <div class="rivew-START">
                                            <ul class="list-inline">
                                                <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                                                <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                                                <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                                                <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                                                <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                                            </ul>
                                        </div>
                                        <span class="ml-2">({{$val}})</span>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                @endif

            @empty
                <div class="text-center">
                    <h3>No Trending Product found!</h3>
                </div>
            @endforelse

    </div>
</div>
