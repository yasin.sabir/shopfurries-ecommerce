@extends('front-layout.app')

@section('title')
    Home
@endsection

@section('Main')

    <script>
        localStorage.setItem('count', '<?php echo $count ?>');
    </script>

    <!-- hero-area START -->
    <div class="hero-area">
        <div class="container">
            <div class="hero-area-slider owl-carousel">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="hero-wrepper text-center">
                            <h1>
                                HIGHEST QUALITY PRINT FOR MERCHANDISE
                            </h1>
                            <div class="btn-group">
                                <a href="{{route('shop')}}" class="THEME-BTN">Browse Products</a>
                                @hasrole('admin')
                                    <a href="{{route('product.bulk.add')}}" class="THEME-BTN transparent-btn">+ Upload Picture  </a>
                                @endhasrole
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- hero-area END -->

    <!-- info-area START -->
    <div class="info-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="info-wrep">
                        <h4><img src="{{ asset('front-end/assets/img/Premium.svg')}}" alt=""> PREMIUM PRODUCTS <br> <span>We never ever compromise on quality</span></h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="info-wrep">
                        <h4><img src="{{ asset('front-end/assets/img/Customerexperience.svg')}}" alt=""> 100% CUSTOMER SATISFACTION <br> <span>Our customers are our top priority</span></h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="info-wrep">
                        <h4><img src="{{ asset('front-end/assets/img/phone-call.svg')}}" alt="">NEED IT SOONER? <br> <span>Call us now at +49-157-5552-458 </span></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- info-area END -->

    <!-- product-area START -->
    <div class="product-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h1 class="mb-0">MOST TRENDING PRODUCTS</h1> <br>
                        <h4 class="mb-5">Top Sales</h4>
                    </div>
                </div>
            </div>

            @include('front-views.Section.HomeTopSales')

        </div>
    </div>
    <!-- product-area END -->


    <!-- product-area START -->
    <div class="item-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h4 class="mt-5 mb-5">Top Sellers</h4>
                    </div>
                    @include('front-views.Section.HomeTopSeller')
                </div>
            </div>
        </div>
    </div>

    <!-- product-area END -->


    <!-- item-area START -->
    <div class="item-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h1>MOST TRENDING PRODUCTS</h1>
                    </div>
                </div>
            </div>

            <!-- Category Section -->
            @include('front-views.Section.CategorySection')

        </div>
    </div>
    <!-- item-area END -->

    <!-- buying-area START -->
    <div class="buying-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="buying-img">
                        <img src="{{ asset('front-end/assets/img/man.png')}}" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="buying-wrepper">
                        <h2>WHY BUY FROM US?</h2>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- buying-area END -->


    <!-- Customer Review Section Start-->
    @include('front-views.Section.HomeCustomerReviewSection')
    <!-- Customer Review Section End-->

    <!-- Recently Viewed Product Start-->
    @include('front-views.Section.RecentlyViewedSection')
    <!-- Recently Viewed Product End-->

    <!-- Event Section Start -->
    @include('front-views.Section.HomeEventSection')
    <!-- Event Section End -->

@endsection


@section('custom-front-script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================

        $(document).on('click', 'a.add-wishlist-btn', function (e) {
            e.preventDefault();
            var _self = $(this);
            var prod_id = $(this).attr("product_id");
            var route = "{{route('wishlist.create',['id'=>'id'])}}";
            route = route.replace('id', prod_id);
            var status = "add";
            var remove_btn = "<a href='' product_id='" + prod_id + "' class='THEME-BTN transparent-btn remove-wishlist-btn'><i class='fas fa-heart' style='color:#DD574E'></i>Remove</a>";

            $.ajax({
                url: route,
                type: 'post',
                data: {status: status},
                success: function (response) {
                    _self.hide();
                    _self.parent().append(remove_btn);
                }
            });
        });

        $(document).on('click', 'a.remove-wishlist-btn', function (e) {
            e.preventDefault();
            var _self = $(this);
            var prod_id = $(this).attr("product_id");
            var route = "{{route('wishlist.delete',['id'=> 'id'])}}";
            route = route.replace('id', prod_id);
            var status = "remove";
            var add_wishlist_btn = "<a href='' product_id='" + prod_id + "' class='THEME-BTN transparent-btn add-wishlist-btn'><i class='fal fa-heart'></i>Add to Wishlist</a>";

            $.ajax({
                url: route,
                type: 'post',
                data: {status: status},
                success: function (response) {
                    _self.hide();
                    _self.parent().append(add_wishlist_btn);
                }
            });

        });

        //=======================================================================================================

    </script>
@endsection
