@extends('front-layout.app')

@section('title')
    Checkout
@endsection

@section('Main')

    <style>
        .update-bun {
            background: white;
            border: none;
        }

        .checkout-btn {
            color: #fff;
            background: #157ABE;
            width: 220px;
            height: 50px;
            display: inline-block;
            text-align: center;
            line-height: 38px;
            font-size: 18px;
            font-weight: 900;
            border-radius: 10px;
            margin-top: 36px;
        }

    </style>

    <script>
        localStorage.setItem('count', '<?php echo $count ?>');
    </script>

    <!-- product-title GROUP -->
    <div class="product-titleGroup">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 text-center">
                    <div class="title-text wishlist-title-text">
                        <h1>Checkout</h1>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- product-title GROUP -->


    <div class="checkout-top-area">

        <div class="container">

            <form method="post" action="{{route('update_temp_item')}}" id="update-cart-form"  enctype="multipart/form-data" class="update-qty">
                @csrf
                @forelse($order_log as $cart)
                    <div class="row" style="padding-bottom: 10px;">
                        <div class="col-lg-12">
                            <div class="checkout-price">

                                @if(isset($cart->product_variation_id))
                                    @php
                                        $variation = \App\productVariation::where(['id' => $cart->product_variation_id])->first();
                                    @endphp
                                    <img src="{{ url('/storage/'.$variation->image) }}" style="width:150px;height:100px;" alt="">
                                @else
                                    @php
                                        $img = getProductFeatureImage($cart->image);
                                    @endphp
                                    <img src="{{asset($img)}}" style="width:150px;height:100px;" alt="">
                                @endif

                                <div class="checkout-price-containt">
                                    <h3>{{$cart->product_name}}
                                    </h3>

                                    <div class="checkout-product-details">
                                        <span>${{number_format($cart->price,2)}}</span>

                                        @if(!empty($cart->size))
                                            <span style="display:inline-block;padding-right:10px;">Size: {{$cart->size}}</span>
                                        @endif

                                        @if(!empty($cart->material))
                                            <span style="display:inline-block;padding-right:10px;">Material: {{$cart->material}}</span>
                                        @endif

                                        @if(!empty($cart->extra))
                                            <span style="display:inline-block;padding-right:10px;">Extra: {{$cart->extra}}</span>
                                        @endif

                                        <span style="display:inline-block;padding-right:10px;">Weight (kg): {{$cart->weight}}</span>

                                    </div>


                                    @if(isset($cart->product_variation_id))
                                        @php
                                            $vari = unserialize($variation->variation);
                                        @endphp

                                        <div class="checkout-product-variation">
                                        @foreach($vari as $k => $v)
                                            <span style="display:inline-block;padding-right:10px;">
                                              @php
                                                   $vv = str_replace("-"," : " ,$v);
                                              @endphp
                                                    {{ucwords($vv)}}
                                            </span>
                                        @endforeach
                                        </div>
                                    @endif

                                    <div class="nice-number2">
                                        {{--<form method="post" action="{{route('update_temp_item')}}" class="update-qty">--}}
                                        {{--@csrf--}}

                                        <input type="number" name="qty[{{$cart->product_id}}]" value="{{$cart->qty}}" style="width: 4ch;">
                                        <input type="hidden" name="product[]" value="{{$cart->product_id}}">
                                        <input type="hidden" name="id[{{$cart->product_id}}]" value="{{$cart->id}}">
                                        {{--</form>--}}
                                    </div>
                                </div>

{{--                                <a href="{{route('delete_temp_item', $cart->id)}}" cart_id="{{$cart->id}}" class="dellet-bun">--}}
{{--                                    {{ method_field('DELETE') }}--}}
{{--                                    <img src="{{ asset('front-end/assets/img/ico_trashbin.svg')}}" alt="">Delete</a>--}}

                                    <a href="#" cart_id="{{$cart->id}}" class="delete_cart_item dellet-bun">
                                        <img src="{{ asset('front-end/assets/img/ico_trashbin.svg')}}" alt="">Delete</a>

                            </div>
                        </div>
                    </div>
                @empty
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h3>Cart is empty!</h3>
                        </div>
                    </div>
                @endforelse
            </form>
        </div>


        @if(isset($order_log) && count($order_log) > 0)
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-6 py-md-4">
                        <form method="post" id="coupon-apply" class="couponapply">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6 col-md-4 py-3 py-sm-0">
                                    <input type="submit" id="coupon-btn" class="btn-cus btn-cus-primary" value="Apply Coupon">
                                </div>
                                <div class="col-sm-6 col-md-8">
                                    <input type="text" name="coupon" id="coupon" class="form-control" placeholder="Enter Coupon">
                                        @error('coupon')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-4 col-lg-6 py-md-4 py-3 py-sm-0 text-md-right text-sm-left">
                        <input type="submit" class="btn-cus btn-cus-primary" id="update-cart-btn" value="Update Cart">
                    </div>
                </div>
            </div>
        @endif

    </div>

    @if(isset($order_log) && count($order_log) > 0)
        <form action="{{route('checkout.paymentselect')}}" id="payment-form" method="post" enctype="multipart/form-data">
            <div class="container">
                <div class="row delivery-info-section">

                    <div class="col-lg-12">
                        <h3>Delivery Information</h3>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="dalivery-addres">
                            <h4>Billing Address:</h4>
                            @csrf
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>Name: <span style="color:#dc3545">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="name" name="name"
                                           class="form-control form-control-sm name @error('name') is-invalid @enderror "/>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>Email: <span style="color:#dc3545">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="email" name="email"
                                           class="form-control form-control-sm  address @error('email') is-invalid @enderror "/>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>Address:<span style="color:#dc3545">*</span> </label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="address" name="address"
                                           class="form-control form-control-sm address @error('address') is-invalid @enderror "/>
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>Mobile No: <span style="color:#dc3545">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="mobile" name="mobile"
                                           class="form-control form-control-sm address @error('mobile') is-invalid @enderror "/>
                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>Post Code: <span style="color:#dc3545">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="pcode" name="pcode"
                                           class="form-control form-control-sm pcode @error('pcode') is-invalid @enderror "/>
                                    @error('pcode')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>City: <span style="color:#dc3545">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="city" name="city"
                                           class="form-control form-control-sm city @error('city') is-invalid @enderror "/>
                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>State: </label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="state" name="state"
                                           class="form-control form-control-sm city"/>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>Country: <span style="color:#dc3545">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <select id="country" name="d-country" class="wide cus-nice-select-sm">
                                        <option value>Select Country</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Åland Islands">Åland Islands</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                        <option value="Antarctica">Antarctica</option>
                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Azerbaijan">Azerbaijan</option>
                                        <option value="Bahamas">Bahamas</option>
                                        <option value="Bahrain">Bahrain</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Belgium">Belgium</option>
                                        <option value="Belize">Belize</option>
                                        <option value="Benin">Benin</option>
                                        <option value="Bermuda">Bermuda</option>
                                        <option value="Bhutan">Bhutan</option>
                                        <option value="Bolivia">Bolivia</option>
                                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                        <option value="Botswana">Botswana</option>
                                        <option value="Bouvet Island">Bouvet Island</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="British Indian Ocean Territory">British Indian Ocean Territory
                                        </option>
                                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                                        <option value="Bulgaria">Bulgaria</option>
                                        <option value="Burkina Faso">Burkina Faso</option>
                                        <option value="Burundi">Burundi</option>
                                        <option value="Cambodia">Cambodia</option>
                                        <option value="Cameroon">Cameroon</option>
                                        <option value="Canada">Canada</option>
                                        <option value="Cape Verde">Cape Verde</option>
                                        <option value="Cayman Islands">Cayman Islands</option>
                                        <option value="Central African Republic">Central African Republic</option>
                                        <option value="Chad">Chad</option>
                                        <option value="Chile">Chile</option>
                                        <option value="China">China</option>
                                        <option value="Christmas Island">Christmas Island</option>
                                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="Comoros">Comoros</option>
                                        <option value="Congo">Congo</option>
                                        <option value="Congo, The Democratic Republic of The">Congo, The Democratic
                                            Republic of The
                                        </option>
                                        <option value="Cook Islands">Cook Islands</option>
                                        <option value="Costa Rica">Costa Rica</option>
                                        <option value="Cote D'ivoire">Cote D'ivoire</option>
                                        <option value="Croatia">Croatia</option>
                                        <option value="Cuba">Cuba</option>
                                        <option value="Cyprus">Cyprus</option>
                                        <option value="Czech Republic">Czech Republic</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Djibouti">Djibouti</option>
                                        <option value="Dominica">Dominica</option>
                                        <option value="Dominican Republic">Dominican Republic</option>
                                        <option value="Ecuador">Ecuador</option>
                                        <option value="Egypt">Egypt</option>
                                        <option value="El Salvador">El Salvador</option>
                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                        <option value="Eritrea">Eritrea</option>
                                        <option value="Estonia">Estonia</option>
                                        <option value="Ethiopia">Ethiopia</option>
                                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                        <option value="Faroe Islands">Faroe Islands</option>
                                        <option value="Fiji">Fiji</option>
                                        <option value="Finland">Finland</option>
                                        <option value="France">France</option>
                                        <option value="French Guiana">French Guiana</option>
                                        <option value="French Polynesia">French Polynesia</option>
                                        <option value="French Southern Territories">French Southern Territories</option>
                                        <option value="Gabon">Gabon</option>
                                        <option value="Gambia">Gambia</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="Ghana">Ghana</option>
                                        <option value="Gibraltar">Gibraltar</option>
                                        <option value="Greece">Greece</option>
                                        <option value="Greenland">Greenland</option>
                                        <option value="Grenada">Grenada</option>
                                        <option value="Guadeloupe">Guadeloupe</option>
                                        <option value="Guam">Guam</option>
                                        <option value="Guatemala">Guatemala</option>
                                        <option value="Guernsey">Guernsey</option>
                                        <option value="Guinea">Guinea</option>
                                        <option value="Guinea-bissau">Guinea-bissau</option>
                                        <option value="Guyana">Guyana</option>
                                        <option value="Haiti">Haiti</option>
                                        <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald
                                            Islands
                                        </option>
                                        <option value="Holy See (Vatican City State)">Holy See (Vatican City State)
                                        </option>
                                        <option value="Honduras">Honduras</option>
                                        <option value="Hong Kong">Hong Kong</option>
                                        <option value="Hungary">Hungary</option>
                                        <option value="Iceland">Iceland</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                        <option value="Iraq">Iraq</option>
                                        <option value="Ireland">Ireland</option>
                                        <option value="Isle of Man">Isle of Man</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Jamaica">Jamaica</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Jersey">Jersey</option>
                                        <option value="Jordan">Jordan</option>
                                        <option value="Kazakhstan">Kazakhstan</option>
                                        <option value="Kenya">Kenya</option>
                                        <option value="Kiribati">Kiribati</option>
{{--                                        <option value="Korea, Democratic People's Republic of">Korea, Democratic--}}
{{--                                            People's Republic of--}}
{{--                                        </option>--}}
{{--                                        <option value="Korea, Republic of">Korea, Republic of</option>--}}
                                        <option value="Kuwait">Kuwait</option>
                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                        <option value="Lao People's Democratic Republic">Lao People's Democratic
                                            Republic
                                        </option>
                                        <option value="Latvia">Latvia</option>
                                        <option value="Lebanon">Lebanon</option>
                                        <option value="Lesotho">Lesotho</option>
                                        <option value="Liberia">Liberia</option>
                                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                        <option value="Liechtenstein">Liechtenstein</option>
                                        <option value="Lithuania">Lithuania</option>
                                        <option value="Luxembourg">Luxembourg</option>
                                        <option value="Macao">Macao</option>
                                        <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former
                                            Yugoslav Republic of
                                        </option>
                                        <option value="Madagascar">Madagascar</option>
                                        <option value="Malawi">Malawi</option>
                                        <option value="Malaysia">Malaysia</option>
                                        <option value="Maldives">Maldives</option>
                                        <option value="Mali">Mali</option>
                                        <option value="Malta">Malta</option>
                                        <option value="Marshall Islands">Marshall Islands</option>
                                        <option value="Martinique">Martinique</option>
                                        <option value="Mauritania">Mauritania</option>
                                        <option value="Mauritius">Mauritius</option>
                                        <option value="Mayotte">Mayotte</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Micronesia, Federated States of">Micronesia, Federated States
                                            of
                                        </option>
                                        <option value="Moldova, Republic of">Moldova, Republic of</option>
                                        <option value="Monaco">Monaco</option>
                                        <option value="Mongolia">Mongolia</option>
                                        <option value="Montenegro">Montenegro</option>
                                        <option value="Montserrat">Montserrat</option>
                                        <option value="Morocco">Morocco</option>
                                        <option value="Mozambique">Mozambique</option>
                                        <option value="Myanmar">Myanmar</option>
                                        <option value="Namibia">Namibia</option>
                                        <option value="Nauru">Nauru</option>
                                        <option value="Nepal">Nepal</option>
                                        <option value="Netherlands">Netherlands</option>
                                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                                        <option value="New Caledonia">New Caledonia</option>
                                        <option value="New Zealand">New Zealand</option>
                                        <option value="Nicaragua">Nicaragua</option>
                                        <option value="Niger">Niger</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Niue">Niue</option>
                                        <option value="Norfolk Island">Norfolk Island</option>
                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Oman">Oman</option>
                                        <option value="Pakistan">Pakistan</option>
                                        <option value="Palau">Palau</option>
                                        <option value="Palestinian Territory, Occupied">Palestinian Territory,
                                            Occupied
                                        </option>
                                        <option value="Panama">Panama</option>
                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                        <option value="Paraguay">Paraguay</option>
                                        <option value="Peru">Peru</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Pitcairn">Pitcairn</option>
                                        <option value="Poland">Poland</option>
                                        <option value="Portugal">Portugal</option>
                                        <option value="Puerto Rico">Puerto Rico</option>
                                        <option value="Qatar">Qatar</option>
                                        <option value="Reunion">Reunion</option>
                                        <option value="Romania">Romania</option>
                                        <option value="Russian Federation">Russian Federation</option>
                                        <option value="Rwanda">Rwanda</option>
                                        <option value="Saint Helena">Saint Helena</option>
                                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                        <option value="Saint Lucia">Saint Lucia</option>
                                        <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                        <option value="Saint Vincent and The Grenadines">Saint Vincent and The
                                            Grenadines
                                        </option>
                                        <option value="Samoa">Samoa</option>
                                        <option value="San Marino">San Marino</option>
                                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                        <option value="Senegal">Senegal</option>
                                        <option value="Serbia">Serbia</option>
                                        <option value="Seychelles">Seychelles</option>
                                        <option value="Sierra Leone">Sierra Leone</option>
                                        <option value="Singapore">Singapore</option>
                                        <option value="Slovakia">Slovakia</option>
                                        <option value="Slovenia">Slovenia</option>
                                        <option value="Solomon Islands">Solomon Islands</option>
                                        <option value="Somalia">Somalia</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="South Georgia and The South Sandwich Islands">South Georgia and
                                            The South Sandwich
                                            Islands
                                        </option>
                                        <option value="Spain">Spain</option>
                                        <option value="Sri Lanka">Sri Lanka</option>
                                        <option value="Sudan">Sudan</option>
                                        <option value="Suriname">Suriname</option>
                                        <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                        <option value="Swaziland">Swaziland</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Switzerland">Switzerland</option>
                                        <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                        <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                        <option value="Tajikistan">Tajikistan</option>
                                        <option value="Tanzania, United Republic of">Tanzania, United Republic of
                                        </option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Timor-leste">Timor-leste</option>
                                        <option value="Togo">Togo</option>
                                        <option value="Tokelau">Tokelau</option>
                                        <option value="Tonga">Tonga</option>
                                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                        <option value="Tunisia">Tunisia</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Turkmenistan">Turkmenistan</option>
                                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                        <option value="Tuvalu">Tuvalu</option>
                                        <option value="Uganda">Uganda</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                        <option value="United States Minor Outlying Islands">United States Minor
                                            Outlying Islands
                                        </option>
                                        <option value="Uruguay">Uruguay</option>
                                        <option value="Uzbekistan">Uzbekistan</option>
                                        <option value="Vanuatu">Vanuatu</option>
                                        <option value="Venezuela">Venezuela</option>
                                        <option value="Viet Nam">Viet Nam</option>
                                        <option value="Virgin Islands, British">Virgin Islands, British</option>
                                        <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                        <option value="Wallis and Futuna">Wallis and Futuna</option>
                                        <option value="Western Sahara">Western Sahara</option>
                                        <option value="Yemen">Yemen</option>
                                        <option value="Zambia">Zambia</option>
                                        <option value="Zimbabwe">Zimbabwe</option>
                                    </select>
                                </div>

                                @forelse($shipping as $key => $val)
                                    <input type="hidden" name="country_{{$key}}" class="shipping_country" shipping_country="{{$val->country}}" weight="{{$val->weight}}" shipping_cost_country="{{$val->shipping_cost_country}}"  value="{{$val->price}}" >
                                @empty

                                @endforelse
                                <input type="hidden" name="default_weight_price" id="default_weight_price" value="{{$Default_Shipping_Setting ['default_weight_price']}}">
                                <input type="hidden" name="default_shipping_cost" id="default_shipping_cost" value="{{$Default_Shipping_Setting ['default_shipping_cost']}}">
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-6">
                                    <label>Mailing address same as billing address: <span style="color:#dc3545;float: right">*</span></label>
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="same-as-billingAddress" required>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="dalivery-addres mt-sm-5 mt-md-0">
                            <h4>Mailing Address:(Optional)</h4>
                            {{-- <span class="edit-btn">edit<img src="{{ asset('front-end/assets/img/Compound%20Path_1.svg')}}" alt=""></span></h4>--}}
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>Address: </label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="other-address" id="other-address" class="form-control form-control-sm name"/>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>Post Code: </label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="other-pcode" id="other-pcode" class="form-control form-control-sm pcode"/>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>City: </label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="other-city" id="other-city" class="form-control form-control-sm city"/>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>State: </label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="other-state" id="other-state"
                                           class="form-control form-control-sm state"/>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <label>Country: </label>
                                </div>
                                <div class="col-sm-9">
{{--                                    cus-nice-select-height cus-nice-select-sm--}}
                                    <select id="other-country" class="wide cus-nice-select-sm @error('country') is-invalid @enderror"
                                            name="other-country">
                                        <option value>Select Country</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Åland Islands">Åland Islands</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                        <option value="Antarctica">Antarctica</option>
                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Azerbaijan">Azerbaijan</option>
                                        <option value="Bahamas">Bahamas</option>
                                        <option value="Bahrain">Bahrain</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Belgium">Belgium</option>
                                        <option value="Belize">Belize</option>
                                        <option value="Benin">Benin</option>
                                        <option value="Bermuda">Bermuda</option>
                                        <option value="Bhutan">Bhutan</option>
                                        <option value="Bolivia">Bolivia</option>
                                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                        <option value="Botswana">Botswana</option>
                                        <option value="Bouvet Island">Bouvet Island</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="British Indian Ocean Territory">British Indian Ocean Territory
                                        </option>
                                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                                        <option value="Bulgaria">Bulgaria</option>
                                        <option value="Burkina Faso">Burkina Faso</option>
                                        <option value="Burundi">Burundi</option>
                                        <option value="Cambodia">Cambodia</option>
                                        <option value="Cameroon">Cameroon</option>
                                        <option value="Canada">Canada</option>
                                        <option value="Cape Verde">Cape Verde</option>
                                        <option value="Cayman Islands">Cayman Islands</option>
                                        <option value="Central African Republic">Central African Republic</option>
                                        <option value="Chad">Chad</option>
                                        <option value="Chile">Chile</option>
                                        <option value="China">China</option>
                                        <option value="Christmas Island">Christmas Island</option>
                                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="Comoros">Comoros</option>
                                        <option value="Congo">Congo</option>
                                        <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic
                                            of The
                                        </option>
                                        <option value="Cook Islands">Cook Islands</option>
                                        <option value="Costa Rica">Costa Rica</option>
                                        <option value="Cote D'ivoire">Cote D'ivoire</option>
                                        <option value="Croatia">Croatia</option>
                                        <option value="Cuba">Cuba</option>
                                        <option value="Cyprus">Cyprus</option>
                                        <option value="Czech Republic">Czech Republic</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Djibouti">Djibouti</option>
                                        <option value="Dominica">Dominica</option>
                                        <option value="Dominican Republic">Dominican Republic</option>
                                        <option value="Ecuador">Ecuador</option>
                                        <option value="Egypt">Egypt</option>
                                        <option value="El Salvador">El Salvador</option>
                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                        <option value="Eritrea">Eritrea</option>
                                        <option value="Estonia">Estonia</option>
                                        <option value="Ethiopia">Ethiopia</option>
                                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                        <option value="Faroe Islands">Faroe Islands</option>
                                        <option value="Fiji">Fiji</option>
                                        <option value="Finland">Finland</option>
                                        <option value="France">France</option>
                                        <option value="French Guiana">French Guiana</option>
                                        <option value="French Polynesia">French Polynesia</option>
                                        <option value="French Southern Territories">French Southern Territories</option>
                                        <option value="Gabon">Gabon</option>
                                        <option value="Gambia">Gambia</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="Ghana">Ghana</option>
                                        <option value="Gibraltar">Gibraltar</option>
                                        <option value="Greece">Greece</option>
                                        <option value="Greenland">Greenland</option>
                                        <option value="Grenada">Grenada</option>
                                        <option value="Guadeloupe">Guadeloupe</option>
                                        <option value="Guam">Guam</option>
                                        <option value="Guatemala">Guatemala</option>
                                        <option value="Guernsey">Guernsey</option>
                                        <option value="Guinea">Guinea</option>
                                        <option value="Guinea-bissau">Guinea-bissau</option>
                                        <option value="Guyana">Guyana</option>
                                        <option value="Haiti">Haiti</option>
                                        <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald
                                            Islands
                                        </option>
                                        <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                        <option value="Honduras">Honduras</option>
                                        <option value="Hong Kong">Hong Kong</option>
                                        <option value="Hungary">Hungary</option>
                                        <option value="Iceland">Iceland</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                        <option value="Iraq">Iraq</option>
                                        <option value="Ireland">Ireland</option>
                                        <option value="Isle of Man">Isle of Man</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Jamaica">Jamaica</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Jersey">Jersey</option>
                                        <option value="Jordan">Jordan</option>
                                        <option value="Kazakhstan">Kazakhstan</option>
                                        <option value="Kenya">Kenya</option>
                                        <option value="Kiribati">Kiribati</option>
{{--                                        <option value="Korea, Democratic People's Republic of">Korea, Democratic People's--}}
{{--                                            Republic of--}}
{{--                                        </option>--}}
{{--                                        <option value="Korea, Republic of">Korea, Republic of</option>--}}
                                        <option value="Kuwait">Kuwait</option>
                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                        <option value="Lao People's Democratic Republic">Lao People's Democratic Republic
                                        </option>
                                        <option value="Latvia">Latvia</option>
                                        <option value="Lebanon">Lebanon</option>
                                        <option value="Lesotho">Lesotho</option>
                                        <option value="Liberia">Liberia</option>
                                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                        <option value="Liechtenstein">Liechtenstein</option>
                                        <option value="Lithuania">Lithuania</option>
                                        <option value="Luxembourg">Luxembourg</option>
                                        <option value="Macao">Macao</option>
                                        <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former
                                            Yugoslav Republic of
                                        </option>
                                        <option value="Madagascar">Madagascar</option>
                                        <option value="Malawi">Malawi</option>
                                        <option value="Malaysia">Malaysia</option>
                                        <option value="Maldives">Maldives</option>
                                        <option value="Mali">Mali</option>
                                        <option value="Malta">Malta</option>
                                        <option value="Marshall Islands">Marshall Islands</option>
                                        <option value="Martinique">Martinique</option>
                                        <option value="Mauritania">Mauritania</option>
                                        <option value="Mauritius">Mauritius</option>
                                        <option value="Mayotte">Mayotte</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Micronesia, Federated States of">Micronesia, Federated States of
                                        </option>
                                        <option value="Moldova, Republic of">Moldova, Republic of</option>
                                        <option value="Monaco">Monaco</option>
                                        <option value="Mongolia">Mongolia</option>
                                        <option value="Montenegro">Montenegro</option>
                                        <option value="Montserrat">Montserrat</option>
                                        <option value="Morocco">Morocco</option>
                                        <option value="Mozambique">Mozambique</option>
                                        <option value="Myanmar">Myanmar</option>
                                        <option value="Namibia">Namibia</option>
                                        <option value="Nauru">Nauru</option>
                                        <option value="Nepal">Nepal</option>
                                        <option value="Netherlands">Netherlands</option>
                                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                                        <option value="New Caledonia">New Caledonia</option>
                                        <option value="New Zealand">New Zealand</option>
                                        <option value="Nicaragua">Nicaragua</option>
                                        <option value="Niger">Niger</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Niue">Niue</option>
                                        <option value="Norfolk Island">Norfolk Island</option>
                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Oman">Oman</option>
                                        <option value="Pakistan">Pakistan</option>
                                        <option value="Palau">Palau</option>
                                        <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied
                                        </option>
                                        <option value="Panama">Panama</option>
                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                        <option value="Paraguay">Paraguay</option>
                                        <option value="Peru">Peru</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Pitcairn">Pitcairn</option>
                                        <option value="Poland">Poland</option>
                                        <option value="Portugal">Portugal</option>
                                        <option value="Puerto Rico">Puerto Rico</option>
                                        <option value="Qatar">Qatar</option>
                                        <option value="Reunion">Reunion</option>
                                        <option value="Romania">Romania</option>
                                        <option value="Russian Federation">Russian Federation</option>
                                        <option value="Rwanda">Rwanda</option>
                                        <option value="Saint Helena">Saint Helena</option>
                                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                        <option value="Saint Lucia">Saint Lucia</option>
                                        <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                        <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines
                                        </option>
                                        <option value="Samoa">Samoa</option>
                                        <option value="San Marino">San Marino</option>
                                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                        <option value="Senegal">Senegal</option>
                                        <option value="Serbia">Serbia</option>
                                        <option value="Seychelles">Seychelles</option>
                                        <option value="Sierra Leone">Sierra Leone</option>
                                        <option value="Singapore">Singapore</option>
                                        <option value="Slovakia">Slovakia</option>
                                        <option value="Slovenia">Slovenia</option>
                                        <option value="Solomon Islands">Solomon Islands</option>
                                        <option value="Somalia">Somalia</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="South Georgia and The South Sandwich Islands">South Georgia and The
                                            South Sandwich
                                            Islands
                                        </option>
                                        <option value="Spain">Spain</option>
                                        <option value="Sri Lanka">Sri Lanka</option>
                                        <option value="Sudan">Sudan</option>
                                        <option value="Suriname">Suriname</option>
                                        <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                        <option value="Swaziland">Swaziland</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Switzerland">Switzerland</option>
                                        <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                        <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                        <option value="Tajikistan">Tajikistan</option>
                                        <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Timor-leste">Timor-leste</option>
                                        <option value="Togo">Togo</option>
                                        <option value="Tokelau">Tokelau</option>
                                        <option value="Tonga">Tonga</option>
                                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                        <option value="Tunisia">Tunisia</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Turkmenistan">Turkmenistan</option>
                                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                        <option value="Tuvalu">Tuvalu</option>
                                        <option value="Uganda">Uganda</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                        <option value="United States Minor Outlying Islands">United States Minor Outlying
                                            Islands
                                        </option>
                                        <option value="Uruguay">Uruguay</option>
                                        <option value="Uzbekistan">Uzbekistan</option>
                                        <option value="Vanuatu">Vanuatu</option>
                                        <option value="Venezuela">Venezuela</option>
                                        <option value="Viet Nam">Viet Nam</option>
                                        <option value="Virgin Islands, British">Virgin Islands, British</option>
                                        <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                        <option value="Wallis and Futuna">Wallis and Futuna</option>
                                        <option value="Western Sahara">Western Sahara</option>
                                        <option value="Yemen">Yemen</option>
                                        <option value="Zambia">Zambia</option>
                                        <option value="Zimbabwe">Zimbabwe</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="shoping-bill">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-6 col-md-6">
                            <div class="shipping-section shipping-left">

                                <div class="payment-top">
                                    <h4>Shipping </h4>
                                    {{--<span class="edit-btn">edit<img src="{{ asset('front-end/assets/img/Compound%20Path_1.svg')}}" alt=""></span></h4>--}}
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label>Shipping Method: </label>
                                        </div>
                                        <div class="col-lg-6 shipping-Section">
                                            <select id="shipping" name="shipping" class="cus-nice-select-sm pl-2 pr-2">
                                                <option value>Select Shipping Method</option>
                                                <option value="dhl">DHL</option>
                                                <option value="courier">Courier</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h4>Payment</h4>
                                <div class="payment-top">
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label>Payment Method: <span style="color:#dc3545">*</span></label>
                                        </div>
                                        <div class="col-lg-6 payment-Section">
                                            <select id="payment" name="payment" class="cus-nice-select-sm pl-2 pr-2">
                                                <option value>Select Payment Method</option>
                                                <option value="paypal">Paypal</option>
                                                <option value="stripe">Stripe</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="stripe-card-section mt-4">

                                                <div class="row mb-3">
                                                    <div class="col-md-6">
                                                        <label for="card-element">
                                                            Credit or debit card
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6 text-right">
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <img width="50"
                                                                     src="{{asset('images/Payment-Icons/LightColor/1.png')}}">
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <img width="50"
                                                                     src="{{asset('images/Payment-Icons/LightColor/2.png')}}">
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <img width="50"
                                                                     src="{{asset('images/Payment-Icons/LightColor/14.png')}}">
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <img width="50"
                                                                     src="{{asset('images/Payment-Icons/LightColor/22.png')}}">
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div id="card-element">
                                                    <!-- A Stripe Element will be inserted here. -->
                                                </div>

                                                <!-- Used to display form errors. -->
                                                <div id="card-errors" role="alert"></div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr>
                                <div class="payment-totle">
                                    <div class="payment-totle-left">
                                        <p>Cart Subtotal</p>
                                        <p>Coupon Discount</p>
                                        <p>Total</p>
                                    </div>
                                    <div class="payment-totle-left">
                                        <p class="carttotal" data-total="{{number_format($total_sum,2)}}">$ {{number_format($total_sum,2)}}</p>
                                        <p><span class="dollar"> </span><span class="discount">$0</span><span class="percent"> </span></p>
                                        <p>${{number_format(0,2)}}</p>
                                        <p class="total">$ {{number_format($total_sum,2)}}</p>
                                    </div>
                                </div>
                            </div>

                            @php

                                $orderParts = \App\Orderparts::all();

                            @endphp


                            @if( isset($orderParts) && count($orderParts) > 0 )
                                <!-- Order Divison Section -->
                                    <div class="shipping-section shipping-left my-3">

                                        <div class="payment-top mb-0">
                                            <h4>Order </h4>
                                            <label>If you don't want to pay full amount at a time ShopFurries give you relaxation pay amount into parts !</label>
                                            <div class="row mt-3">
                                                <div class="col-lg-6">
                                                    <label>Order Parts : </label>
                                                </div>
                                                <div class="col-lg-6 orderInstallment-Section">
                                                    <select id="orderParts" name="orderParts" class="cus-nice-select-sm pl-2 pr-2">
                                                        <option value>Select Parts</option>
                                                        @foreach($orderParts as $key => $val)
                                                            <option value="{{$val->id}}" parts="{{$val->parts}}" days="{{$val->days}}">{{ucwords($val->title)}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="row mt-3 order-parts-detail-section">

                                            </div>

                                        </div>
                                    </div>

                            @endif


                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="shipping-section shipping-right">
                                <h4>Total Details</h4>
                                <hr>

                                <!-- Products group by with one category -->
                                @forelse($product_groupBy_category as $key => $val)

                                   @foreach($val as $k => $v)
                                       @php
                                            $product            = \App\product::find($k);
                                            $product_price []   = $val[$k];
                                            //$order_log_product  = \App\order_log::where('user_id',Auth::user()->id)
                                                                      //->where(['product_id' => $product->id])->first();

                                            $order_log_product  = \App\order_log::where('user_id',$userID)
                                                                      ->where(['product_id' => $product->id])->first();
                                       @endphp
                                        <div class="checkout-per-product-detail">
                                            <p class="product-check-out-details">{{ucwords($product->title)}}</p><p class="product-check-out-price">{{"$".number_format($val[$k],2)}} - {{"x".$order_log_product->qty}}</p>
                                        </div>

                                   @endforeach
                                   @php
                                       $category = \App\Category::find($key);
                                       $category_SalesTax = \App\Category_Meta::where(['category_id' => $key])
                                                ->where(['meta_key' => 'cate_sale_tax'])
                                                ->first();
                                   @endphp
                                       <p><strong><em>Category Note</em></strong>: {{ucwords($category->Name)}} VAT Sales Tax Applied : {{$category_SalesTax->meta_value}}%</p>
                                       <div class="Total-VAT">
                                       <p>Total with VAT:</p> <p class="product-check-out-price"><strong><em>{{"=/ $".number_format($product_groupBy_category_with_salesTax[$key][0],2)}}</em></strong></p>
                                   </div>
                                   <hr>

                                @empty

                                @endforelse


                                <!-- Products that have morethan 2 categories apply default sales tax VAT -->
                                @if(isset($product_groupBy_moreThan_two_categories) && count($product_groupBy_moreThan_two_categories) >= 1)
                                    <span class="check-out-note-default">Note: Default VAT applied when product contain more than two catgories</span>

                                    @forelse($product_groupBy_moreThan_two_categories as $key => $val)
                                        @php
                                            $product = \App\product::find($key);
                                            //$order_log_product  = \App\order_log::where('user_id',Auth::user()->id)
                                                                      //->where(['product_id' => $product->id])->first();

                                            $order_log_product  = \App\order_log::where('user_id',$userID)
                                                                      ->where(['product_id' => $product->id])->first();
                                        @endphp
                                        <div class="checkout-per-product-detail">
                                            <p class="product-check-out-details">{{ucwords($product->title)}}</p><p class="product-check-out-price">{{"$".number_format($val,2)}} - {{"x".$order_log_product->qty}}</p>
                                        </div>
                                    @empty

                                    @endforelse

                                    @php
                                        $dst        = \App\SiteSetting::where(['key' => 'default_sales_tax'])->first();
                                    @endphp
                                    <p><strong><em>Default</em></strong> Sales Tax VAT Applied: {{$dst->value}}%</p>
                                    <div class="Total-VAT">
                                        <p>Total with VAT:</p>  <p class="product-check-out-price"><strong><em>{{"=/ $".number_format($defaultSaleTax,2)}}</em></strong></p>
                                    </div>

                                @endif

                                @if(isset($total_weight))
                                    <hr>

                                    <span class="check-out-note-default">
                                           Note: Weight price applied according to country wise:
                                    </span>

                                    <div class="weight-section">
                                        <div class="Total-VAT">
                                            <p class="mb-0">Selected Country: </p>  <p class="product-check-out-price selected_country_by_shipping_address"></p>
                                        </div>
                                        <div class="Total-VAT">
                                            <p class="mb-0">Shipping Cost Per Country: </p>  <p class="product-check-out-price cost_per_country"></p>
                                        </div>
                                        <div class="Total-VAT">
                                            <p class="mb-0">Cost Per Weight(kg): </p>  <p class="product-check-out-price cost_per_weight"></p>
                                        </div>
                                        <div class="Total-VAT">
                                            <p class="mb-0">Total Products Weights:</p>  <p class="product-check-out-price"><strong><em>{{$total_weight." (Kg)"}}</em></strong></p>
{{--                                            <input type="hidden" id="total_product_weight" value="{{$total_weight}}">--}}
                                        </div>

                                    </div>
                                    <hr>
                                @endif

                                <div class="Total-VAT">
                                    <p class="mb-0">Weight Cost: </p>  <p class="product-check-out-price weight_cost">$0.00</p>
                                </div>
                                <div class="Total-VAT">
                                    <p class="mb-0">Shipping Cost: </p>  <p class="product-check-out-price shipping_cost">$0.00</p>
                                </div>

                                <div class="Total-VAT">
                                    <p class="mb-0">Cart Subtotal:</p>  <p class="product-check-out-price carttotal" data-total="{{number_format($total_sum,2)}}">${{number_format($total_sum,2)}}</p>
                                </div>

                                <div class="Total-VAT">
                                    <p class="mb-0">Coupon Discount:</p>  <p class="product-check-out-price coupon_discount">${{number_format(0,2)}}</p>
                                </div>
                                <div class="Total-VAT">
                                    <p class="mb-0"><strong>Total:</strong></p>  <p class="product-check-out-price total">${{number_format($total_sum,2)}}</p>
{{--                                    <input type="hidden" id="subtotal" name="subtotal" value="{{number_format($total_sum,2)}}" >--}}
                                    <input type="hidden" id="subtotal" name="subtotal" value="{{$total_sum}}">
                                    <input type="hidden" id="total_product_weight" name="total_product_weight" value="{{$total_weight}}">
                                    <input type="hidden" id="selected_country" name="selected_country" value="">
                                    <input type="hidden" id="coupon_discount" name="coupon_discount" value="">
                                    <input type="hidden" id="coupon_type" name="coupon_type" value="">
                                    <input type="hidden" id="cost_per_country_weight" name="cost_per_country_weight" value="">
                                    <input type="hidden" id="shipping_cost" name="shipping_cost" value="">
                                    <input type="hidden" id="weight_cost" name="weight_cost" value="">
                                    <input type="hidden" id="subtotal_of_products" name="subtotal_of_products" value="{{$total_sum}}">
                                    <input type="hidden" id="order_log" name="order_log" value="{{$log_data}}">
                                </div>
                                <div id="checkout-Proceed-Button-insert-After"></div>
                                <div class="text-center checkout-btn-section">
                                    <input type="hidden" id="currentUserID" name="currentUserID" value="{{$userID}}">
                                    <button type="submit" class="btn btn-checkout text-center">Proceed to Checkout</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    @endif



@endsection

@section('custom-front-script')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================


        $(document).ready(function () {

            var stripe_public_Key = "pk_test_oRJf6RQMqYkga3fxQ09enR9d";
            // var stripe_public_Key = "pk_test_oRJf6RQMqYkga3fxQ09enR9d";
            var stripe_secret_Key = "";


            $('select').niceSelect();

            $(".stripe-card-section").hide();

            $("select#payment").change(function () {
                var selected_payment = $(this).val();

                if (selected_payment === "stripe") {

                    $(".stripe-card-section").show();
                    Stripe_PaymentGateway(stripe_public_Key);

                } else {
                    $(".stripe-card-section").hide();
                }

            });

        });

        //=======================================================================================================


        $('input[type="number"]').niceNumber({

            // the number of extra character
            autoSizeBuffer: 1,
            // custom button text
            buttonDecrement: '-',
            buttonIncrement: "+",

            // 'around', 'left', or 'right'
            buttonPosition: 'around'

        });


        function Stripe_PaymentGateway(public_key) {

            // Create a Stripe client.
            // pk_test_oRJf6RQMqYkga3fxQ09enR9d
            var stripe = Stripe(public_key);

            // Create an instance of Elements.
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element.
            var card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');

            // Handle real-time validation errors from the card Element.
            card.addEventListener('change', function (event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });

            // Handle form submission.
            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function (event) {
                event.preventDefault();

                stripe.createToken(card).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error.
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                // Submit the form
                form.submit();
            }
        }

        $("#update-cart-btn").on('click', function () {
            $("#update-cart-form").submit();
        });

        //=======================================================================================================


        $(".delete_cart_item").on('click' , function (e) {
            e.preventDefault();

            var cart_id = $(this).attr("cart_id");
            var route   = "{{route('delete_temp_item',  ['id' => 'id'])}}";
            route       = route.replace('id' , cart_id);
            $.ajax({
                url : route,
                type: 'post',
                data: {cart_id : cart_id},
                success:function(res){
                    toastr.error(res.Cart, res.message);
                    location.reload();
                }
            });

        });

        var carttotal = $('.carttotal').attr("data-total");
        var print_type =  localStorage.getItem('type');
        var print_coupon_discount =  localStorage.getItem('coupon_discount');
        // $('.btn-checkout').click(function(){
        //
        // })

        if(print_type == 'fixed'){
            $('.dollar').html(' $ ');
            var total = carttotal - print_coupon_discount;
            $('.total').html('$ '+ total )
            $('#coupon-btn').css('display','none');
            $('#coupon').css('display','none');
        }
        if(print_type == 'percent'){
            $('.percent').html(' % ');
            var calculation = ((carttotal/100)*print_coupon_discount);
            var total = carttotal - calculation;
            $('.total').html('$ '+ total )
            $('#coupon-btn').css('display','none');
            $('#coupon').css('display','none');
        }
        $('.discount').html(print_coupon_discount);

        if(total){
            $('#subtotal').val(total);
        }


        $("#coupon-apply").on('submit' , function (e) {
            e.preventDefault();

            var coupon = $('#coupon').val();
            var carttotal = $('.carttotal').attr("data-total");
            var route   = "{{route('couponapply')}}";
            $.ajax({
                url : route,
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {coupon : coupon},
                success:function(response){
                    var type = response.type ;
                    var coupon_discount = response.coupon_discount ;
                    localStorage.setItem('type', type);
                    localStorage.setItem('coupon_discount', coupon_discount);

                    var noti = response.noti;
                    //alert(noti);
                    var print_type =  localStorage.getItem('type');
                    var print_coupon_discount =  localStorage.getItem('coupon_discount');
                        $('#coupon-btn').css('display','none');
                    $('#coupon').css('display','none');
                    if(print_type == 'fixed'){
                        $('.dollar').html(' $ ');
                        var total = carttotal - print_coupon_discount;
                        $('.total').html('$ '+ total )
                    }
                    if(print_type == 'percent'){
                        $('.percent').html(' % ');
                        var calculation = ((carttotal/100)*print_coupon_discount);
                        var total = carttotal - calculation;
                        $('.total').html('$ '+ total )
                    }
                    $('.discount').html(print_coupon_discount);

                    if(total){
                        $('#subtotal').val(total);
                    }

                    if(noti == "Coupon Applied Successfully!"){
                        toastr.success('Coupon Applied Successfully', 'Temporary Item');
                    }else if(noti == "Coupon use limit cross!"){
                        toastr.warning('Coupon use limit cross!', 'Temporary Item');
                    }else if(noti == "Coupon Expired!"){
                        toastr.warning('Coupon Expire!', 'Temporary Item');
                    }else if(noti == "Coupon Not Found!"){
                        toastr.warning('Coupon Not Found!', 'Temporary Item');
                    }
                    else if(noti == "Coupon Already Used By User!"){
                        toastr.warning('Coupon Already Used By User!', 'Temporary Item');
                    }

                },
                error:function(response){
                    console.log(response);
                    toastr.success('Coupon Applied Failed', 'Temporary Item');
                }
            });

        });

        //=======================================================================================================

        var total_product_weight = $("#total_product_weight").val();
        //selected_country_by_shipping_address
        var total_price                    =  parseFloat($("#subtotal").val());
        var default_weight_price           =  parseFloat($("#default_weight_price").val());
        var default_shipping_cost          =  parseFloat($("#default_shipping_cost").val());
       // var shipping_country               = "";
        var shipping_cost_country          = "";
        var shipping_country_weight_price  = "";
        var shipping_countries             = [];

        $(".shipping_country").each(function(i){
           shipping_countries.push($(this).attr("shipping_country"));
        });

        $("select#country").change(function() {
            var selectedCountry = $(this).children("option:selected").val();
            console.log(selectedCountry);

            $(".shipping_country").each(function(){
                 //shipping_country = $(this).attr("shipping_country");
                 shipping_country_weight_price = $(this).val();

                if(selectedCountry ===  $(this).attr("shipping_country")){

                    shipping_cost_country   = $(this).attr("shipping_cost_country");

                    if(shipping_cost_country === "0" || shipping_cost_country === ""){
                        shipping_cost_country = default_shipping_cost;
                    }
                    console.log(shipping_cost_country);

                    if(shipping_country_weight_price === "0" || shipping_country_weight_price === ""){
                        shipping_country_weight_price = default_weight_price;
                    }

                    var print_type =  localStorage.getItem('type');
                    var print_coupon_discount =  localStorage.getItem('coupon_discount');

                    if(print_type == 'fixed'){

                        total_val_              = ( total_price - print_coupon_discount );
                        var applied_weight      = (total_product_weight * shipping_country_weight_price);
                        var sub_total           = ( parseFloat(applied_weight) + parseFloat(total_val_) + parseFloat(shipping_cost_country) ).toFixed(1);

                        $("p.cost_per_country").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                        $("p.cost_per_weight").html("<strong><em>"+"$"+$.number(shipping_country_weight_price , 2)+"</em></strong>");
                        $("p.weight_cost").html("<strong><em>"+"$"+$.number(applied_weight , 2)+"</em></strong>");
                        $("p.shipping_cost").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                        $("p.total").html("$"+$.number(sub_total,2));
                        $("#subtotal").val(sub_total);

                    }else if(print_type == 'percent'){

                        var calculation         = parseFloat(((total_price/100 )* print_coupon_discount));
                        var total_val_          = ( total_price - calculation );
                        var applied_weight      = (total_product_weight * shipping_country_weight_price);
                        var sub_total           = ( parseFloat(applied_weight) + parseFloat(total_val_) + parseFloat(shipping_cost_country) ).toFixed(1);

                        $("p.cost_per_country").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                        $("p.cost_per_weight").html("<strong><em>"+"$"+$.number(shipping_country_weight_price , 2)+"</em></strong>");
                        $("p.weight_cost").html("<strong><em>"+"$"+$.number(applied_weight , 2)+"</em></strong>");
                        $("p.shipping_cost").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                        $("p.total").html("$"+$.number(sub_total,2));
                        $("#subtotal").val(sub_total);

                    }else{

                        var applied_weight      = (total_product_weight * shipping_country_weight_price);
                        var sub_total           = (parseFloat(applied_weight) + parseFloat($("#subtotal").val()) + parseFloat(shipping_cost_country));

                        $("p.cost_per_country").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                        $("p.cost_per_weight").html("<strong><em>"+"$"+$.number(shipping_country_weight_price , 2)+"</em></strong>");
                        $("p.weight_cost").html("<strong><em>"+"$"+$.number(applied_weight , 2)+"</em></strong>");
                        $("p.shipping_cost").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                        $("p.total").html("$"+$.number(sub_total,2));
                        $("#subtotal").val(sub_total);

                    }

                    //set hidden fields
                    $("#selected_country").val(selectedCountry);
                    $("#cost_per_country_weight").val(shipping_country_weight_price);
                    $("#shipping_cost").val(shipping_cost_country);
                    $("#weight_cost").val(applied_weight);


                    // var applied_weight      = (total_product_weight * shipping_country_weight_price);
                    // var sub_total           = (parseFloat(applied_weight) + parseFloat($("#subtotal").val()) + parseFloat(shipping_cost_country));
                    //
                    // $("p.cost_per_country").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                    // $("p.cost_per_weight").html("<strong><em>"+"$"+$.number(shipping_country_weight_price , 2)+"</em></strong>");
                    // $("p.weight_cost").html("<strong><em>"+"$"+$.number(applied_weight , 2)+"</em></strong>");
                    // $("p.shipping_cost").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                    // $("p.total").html("$"+$.number(sub_total,2));
                    // $("#subtotal").val(sub_total);

                }

                //If Country not exist in shipping table
                if(jQuery.inArray(selectedCountry , shipping_countries) < 0){

                    //check if coupon is applied or not
                    var couponValue = parseFloat($("#coupon_discount").val());
                    if( couponValue !== "" && couponValue > 0){

                        var print_type =  localStorage.getItem('type');
                        var print_coupon_discount =  localStorage.getItem('coupon_discount');

                        if(print_type == 'fixed'){

                            total_val_                      = ( total_price - print_coupon_discount );
                            var default_applied_weight      = (total_product_weight * default_weight_price);
                            var sub_total2                  = $.number( (parseFloat(default_applied_weight) + parseFloat(total_val_) + parseFloat(default_shipping_cost)) , 2);

                            $("p.cost_per_country").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");
                            $("p.cost_per_weight").html("<strong><em>"+"$"+$.number(default_weight_price , 2)+"</em></strong>");
                            $("p.weight_cost").html("<strong><em>"+"$"+$.number(default_applied_weight , 2)+"</em></strong>");
                            $("p.shipping_cost").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");
                            $("p.total").html("$"+$.number(sub_total2,2));
                            $("#subtotal").val(sub_total2);

                        }else if(print_type == 'percent'){

                            var calculation                 = parseFloat(((total_price/100 )*print_coupon_discount));
                            var total_val_                  = ( total_price - calculation );
                            var default_applied_weight      = (total_product_weight * default_weight_price);
                            var sub_total2                  = $.number( (parseFloat(default_applied_weight) + parseFloat(total_val_) + parseFloat(default_shipping_cost)) , 2 );

                            $("p.cost_per_country").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");
                            $("p.cost_per_weight").html("<strong><em>"+"$"+$.number(default_weight_price , 2)+"</em></strong>");
                            $("p.weight_cost").html("<strong><em>"+"$"+$.number(default_applied_weight , 2)+"</em></strong>");
                            $("p.shipping_cost").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");
                            $("p.total").html("$"+$.number(sub_total2,2));
                            $("#subtotal").val(sub_total2);

                        }else{
                        }


                    }else{
                        var default_applied_weight      = (total_product_weight * default_weight_price);
                        var sub_total2                  = (parseFloat(default_applied_weight) + (total_price) + parseFloat(default_shipping_cost));

                        $("p.cost_per_country").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");
                        $("p.cost_per_weight").html("<strong><em>"+"$"+$.number(default_weight_price , 2)+"</em></strong>");
                        $("p.weight_cost").html("<strong><em>"+"$"+$.number(default_applied_weight , 2)+"</em></strong>");
                        $("p.shipping_cost").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");
                        $("p.total").html("$"+$.number(sub_total2,2));
                        $("#subtotal").val(sub_total2);
                    }

                    //set hidden fields
                    $("#selected_country").val(selectedCountry);
                    $("#cost_per_country_weight").val(default_weight_price);
                    $("#shipping_cost").val(default_shipping_cost);
                    $("#weight_cost").val(default_applied_weight);

                    // var default_applied_weight      = (total_product_weight * default_weight_price);
                    // var sub_total2                  = (parseFloat(default_applied_weight) + (total_price) + parseFloat(default_shipping_cost));
                    //
                    // $("p.cost_per_country").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");
                    // $("p.cost_per_weight").html("<strong><em>"+"$"+$.number(default_weight_price , 2)+"</em></strong>");
                    // $("p.weight_cost").html("<strong><em>"+"$"+$.number(default_applied_weight , 2)+"</em></strong>");
                    // $("p.shipping_cost").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");
                    // $("p.total").html("$"+$.number(sub_total2,2));
                }

            });

            $("p.selected_country_by_shipping_address").html("<strong><em>"+selectedCountry+"</em></strong>")

        });


        //console.log(shipping_countries[0]+"DD");

        //=======================================================================================================

        $("#same-as-billingAddress").click(function(){
            if($(this).prop("checked") === true){

                var address = $("#address").val();
                var pcode   = $("#pcode").val();
                var city    = $("#city").val();
                var state   = $("#state").val();
                var country = $("#country option:selected").val();

                if(address !== "" || pcode !== "" || city !== "" || state !== "" || country !== "" ){

                    $("#other-address").val(address);
                    $("#other-pcode").val(pcode);
                    $("#other-city").val(city);
                    $("#other-state").val(state);
                    $("[name=other-country]").val(country);
                    $('[name=other-country]').niceSelect('update');
                }

            }else if($(this).prop("checked") === false){

                $("#other-address").val("");
                $("#other-pcode").val("");
                $("#other-city").val("");
                $("#other-state").val("");
                $('#other-country').val("");
                $('#other-country').niceSelect('update');

            }
        });

        //=======================================================================================================

            var sub_total = parseFloat($("#subtotal").val());

            $("select#orderParts").change(function(){
                var element  = $(this).find('option:selected');
                var parts    = element.attr("parts");
                var days     = element.attr("days");
                var division =  $.number( (sub_total / parts) , 2);
                //alert(division + "Parts :"+ parts +" Days :" +days);

                if( parts != undefined){

                    $(".order-parts-detail-section").html('<div class="col-lg-12">\n' +
                        '                                                    <div class="Total-VAT">\n' +
                        '                                                        <label>Parts:</label>  <p class="product-check-out-price"><strong>'+parts+'</strong></p>\n' +
                        '                                                    </div>\n' +
                        '                                                    <div class="Total-VAT">\n' +
                        '                                                        <label>Days:</label>  <p class="product-check-out-price"><strong>'+days+'</strong></p>\n' +
                        '                                                    </div>\n' +
                        '                                                    <div class="Total-VAT">\n' +
                        '                                                        <label>Price:</label>  <p class="product-check-out-price"><strong>$'+division+'</strong></p>\n' +
                        '                                                    </div>\n' +
                        '                                                </div>\n' +
                        '\n' +
                        '\n' +
                        '                                                <div class="col-lg-12">\n' +
                        '                                                    <div class="Total-VAT">\n' +
                        '                                                        <label for="">\n' +
                        '                                                            <em>\n' +
                        '                                                                For the next payment will be notify to you via email after '+days+' days from now! ,\n' +
                        '                                                            </em>\n' +
                        '                                                        </label>\n' +
                        '                                                    </div>\n' +
                        '                                                </div>\n' +
                        '');

                        $(".checkout-btn-section").insertAfter(".order-parts-detail-section");
                        $("#subtotal").attr("value",division);


                        {{--.append('<div class="text-center checkout-btn-section">\n' +--}}
                        {{--    '                                    <input type="hidden" id="currentUserID" name="currentUserID" value="{{$userID}}">\n' +--}}
                        {{--    '                                    <button type="submit" class="btn btn-checkout text-center">Proceed to Checkout</button>\n' +--}}
                        {{--    '                                </div>');--}}


                }else{
                    $(".order-parts-detail-section").html("");
                    $(".checkout-btn-section").insertAfter("#checkout-Proceed-Button-insert-After");
                    $("#subtotal").attr("value",sub_total);
                }



            });

        //=======================================================================================================

    </script>
@endsection
