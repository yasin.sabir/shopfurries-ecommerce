@extends('front-layout.app')

@section('title')
    Billing
@endsection

@section('Main')

    <!-- sidbar -->
    <div class="complete-heading-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="complete-text text-center">
                        <a href="#" class="check-icon"><i class="fal fa-check"></i></a>
                        <h3>Complete Successfully</h3>
                        <p>Your ordering information will be forwarded to your email</p>
                        <a href="mailto:example_mail@mail.com">example_mail@mail.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="compelete-dalivery">
        <div class="container">
            <div class="row dalivery-padding">
                <div class="col-lg-4 col-md-4">
                    <div class="dalivery-text text-center">
                        <div class="dalivery-img">
                            <img src="{{ asset('front-end/assets/img/Billing-Summary-page-img/Compound-Path_3.svg')}}" alt="">
                        </div>
                        <p>We will inform you when the <br>
                            package is ready</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="dalivery-text text-center">
                        <div class="dalivery-img">
                            <img src="{{ asset('front-end/assets/img/Billing-Summary-page-img/Compound-Path_1.svg')}}" alt="">
                        </div>
                        <p>We will inform you when the <br>
                            package is ready</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="dalivery-text text-center">
                        <div class="dalivery-img">
                            <img src="{{ asset('front-end/assets/img/Billing-Summary-page-img/Compound-Path_2.svg')}}" alt="">
                        </div>
                        <p>We will inform you when the <br>
                            package is ready</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="dalivery-btn text-center">
                        <a href="#" class="THEME-BTN">Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom-front-script')
    <script>
        $('.container-gallery').gallery({
            height: 650,
            items: 6,
            480: {
                items: 2,
                height: 400,
                thmbHeight: 100
            },
            768: {

                items: 3,
                height: 500,
                thmbHeight: 120
            },
            600: {

                items: 4
            },
            992: {

                items: 5,
                height: 350
            }

        });

    </script>
@endsection

