@extends('front-layout.app')

@section('title')
    Faqs
@endsection

@section('Main')

    <!-- product-title GROUP -->
    <div class="product-titleGroup faq-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="title-textLEFT">
                        <p>Home <i class="far fa-angle-right"></i> <span style="color: #000;font-weight: 700;">FAQs</span></p>
                    </div>
                </div>
                <div class="col-lg-6 text-center">
                    <div class="title-text">
                        <h1>Frequently Asked Questions (FAQs)  </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product-title GROUP -->

    <!-- faq-area START -->
    <div class="faq-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="faqs">




                        <div class="accordion" id="accordionExample">

                            @forelse($faqs as $key => $faq)

                                <div class="card">
                                    <div class="card-header" id="heading{{$key}}">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="false" aria-controls="collapse{{$key}}">
                                                {{$key+1}}. {{strip_tags($faq->question)}}  <span><i class="fas fa-window-minimize"></i></span>
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapse{{$key}}" class="collapse" aria-labelledby="heading{{$key}}" data-parent="#accordionExample">
                                        <div class="card-body">
                                           {{strip_tags($faq->answer)}}
                                        </div>
                                    </div>
                                </div>

                            @empty
                                <div class="col-md-12 mt-5 text-center">
                                    <div class="title-text">
                                        <h3>No FAQs Found! <i class="fa fa-thumbs-down"></i> </h3>
                                    </div>
                                </div>
                            @endforelse


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- faq-area END -->


@endsection
