@extends('front-layout.app')

@section('title')
    About
@endsection

@section('Main')


    <!-- product-title GROUP -->
    <div class="product-titleGroup faq-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="title-textLEFT">
                        <p>Home <i class="far fa-angle-right"></i> <span style="color: #000;font-weight: 700;">About Us</span></p>
                    </div>
                </div>
                <div class="col-lg-5 text-center">
                    <div class="title-text">
                        <h1>About Us </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product-title GROUP -->

    <!-- about-area START -->
    <div class="about-area">
        <div class="container">


            <div class="row kiel">
                <div class="col-lg-6">
                    <div class="description">
                        <h4>Once Upon A Time In Kiel... </h4>
                        <p>Allow me to introduce myself. My name is Torben Goldmund and my story begins in the medieval city of Kiel in the northern part of Germany. Ever since I was a child, I have been fascinated by arts. You could always find me doodling, cutting, gluing or coloring something. </p>
                        <p>So when I grew up, I decided to turn my pastime into a full-time job. My first adventures with wood board heat burning provided the spark that ignited my passion. After successfully completing my Apprenticeship, I decided to start my very own business.</p>
                        <p>The Brony community was my first step towards my future success. After establishing myself as the #1 German print supplier for the Brony community and building a reputation as the go-to brand for Brony products, I have decided to broaden my horizons and present premium products for you, the Furrys!
                        </p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="description-img">
                        <img src="{{asset('front-end/assets/img/CROP.jpg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="row about-imgs">
                <div class="col-lg-12">
                    <div class="about-born">
                        <h4>So, Art ‘N’ Prints Was Born! </h4>
                        <p>Being an Brony and Furry fan myself, I wanted to offer everyone in the Furry community the chance to finally own premium products without having to spend a small fortune. My goal is to create a direct connection to my customers, who can choose their own designs, provide insights, suggest improvements, and even take part in my decision-making process. </p>
                        <img src="{{asset('front-end/assets/img/hero-bac.png')}}" class="first-img" alt="">
                        <img src="{{asset('front-end/assets/img/hero-bac.png')}}" class="second-img" alt="">
                    </div>
                </div>
            </div>
            <div class="row about-mission">
                <div class="col-lg-12">
                    <div class="mission">
                        <h5>My Mission. </h5>
                        <p>You are the epicenter of Art ‘N’ Prints. You are the sun and everything revolves around you. That is why I have partnered with some of the most talented artists available in order to offer YOU the highest-quality, hand-designed furry fan products that will allow you to express yourself. </p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <p class="mFeture-title">Are You Ready?</p>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="single-item">
                        <h1><img src="{{asset('front-end/assets/img/Premium.svg')}}" alt=""> </h1>
                        <h4>PREMIUM PRODUCTS <br> <span>We never ever compromise on quality</span></h4>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="single-item">
                        <h1><img src="{{asset('front-end/assets/img/Customerexperience.svg')}}" alt=""></h1>
                        <h4> 100% CUSTOMER SATISFACTION <br> <span>Our customers are our top priority</span></h4>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="single-item">
                        <h1><img src="{{asset('front-end/assets/img/phone-call.svg')}}" alt=""></h1>
                        <h4>NEED IT SOONER? <br> <span>Call us now at +49-157-5552-458 </span></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- about-area END -->


@endsection



@section('custom-front-script')

@endsection
