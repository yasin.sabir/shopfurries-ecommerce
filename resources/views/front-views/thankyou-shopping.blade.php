@extends('front-layout.app')

@section('title')
   Thank You For Shopping!
@endsection

@section('Main')

    <!-- product-title GROUP -->
    <div class="product-titleGroup faq-area">
        <div class="container">

        </div>
    </div>
    <!-- product-title GROUP -->

    <!-- faq-area START -->
    <div class="faq-area" style="margin: 150px 0px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">

                    <h1 class="thankyou-text">
                        Thank You <br>For Shopping <br> with us.
                    </h1>

                </div>
            </div>
        </div>
    </div>
    <!-- faq-area END -->


@endsection
