@extends('front-layout.app')

@section('title')
    Product
@endsection


@section('custom-front-css')

@endsection


@section('Main')

    <!-- Product-detailse START -->
    <div class="Product-detailse">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="img-gellery">
                        <div class="jquery-script-clear"></div>
                        <div class="container-gallery">
                            <style>
                                .minus-decrement {
                                    display: flex;
                                    margin-top: -15px;
                                    margin-left: 210px;
                                }

                                .plus-increment {
                                    display: flex;
                                    margin-top: -27px;
                                    margin-left: 20px;
                                }

                            </style>
                            @php
                                //Default Category Setting
                                 $cst = \App\category_setting::all();
                                 $cst  = $cst->toArray();

                                 $cate_setting = \App\category_setting::all();

                                 $category_metas = [];

                                 foreach ($cate_setting as $key => $val){
                                    $category_metas["category_reason_title"]            = $val->reason_titles;
                                    $category_metas["category_reason_detail"]            = $val->reason_descriptions;
                                    $category_metas["category_prod_feature_title"]      = $val->feature_titles;
                                    $category_metas["category_prod_feature_detail"]     = $val->feature_descriptions;
                                    $category_metas["poster_images"]                    = $val->posters;
                                    $category_metas["actual_material_images"]           = $val->actual_material_pics;
                                 }

                                 $gallery_images = [];
                                 $gallery_images[0] = $product->image;
                                 $images = isset($galleries) ? $galleries : array();

                                 foreach ($images as $key => $val ){
                                        $gallery_images[$key+1] = $val->image;
                                 }

                                 //dd($gallery_images);
                            @endphp

                            @forelse($gallery_images as $key => $val)
                                <img src="{{ asset('storage/'.$val)}}" alt="product_image">
                            @empty
                                <img src="{{ asset('images/placeholders/default-placeholder-600x600.png')}}" alt="placeholder">
                            @endforelse
                            {{--                            <img src="{{ asset('front-end/assets/img/product-img2.png')}}" alt="">--}}
                            {{--                            <img src="{{ asset('front-end/assets/img/productDemoThumbnail2.jpg')}}" alt="">--}}
                            {{--                            <img src="{{ asset('front-end/assets/img/product-img2.png')}}" alt="">--}}
                            {{--                            <img src="{{ asset('front-end/assets/img/product-img2.png')}}" alt="">--}}
                            {{--                            <img src="{{ asset('front-end/assets/img/productDemoThumbnail2.jpg')}}" alt="">--}}
                            {{--                            <img src="{{ asset('front-end/assets/img/product-img2.png')}}" alt="">--}}
                            {{--                            <img src="{{ asset('front-end/assets/img/product-img2.png')}}" alt="">--}}
                            {{--                            <img src="{{ asset('front-end/assets/img/productDemoThumbnail2.jpg')}}" alt="">--}}
                            {{--                            <img src="{{ asset('front-end/assets/img/product-img2.png')}}" alt="">--}}
                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <form class="buy-now" action="post">
                        @csrf
                        <div class="product-pay">

                            <h3> {{ ucfirst($product->title)  }} <br> <span>SHORT DESCRIPTION</span></h3>
                            <p>  {{substr(strip_tags($product->description),0,500)."..."}}</p>

                            <h4 class="price">
                                price <br>
                                <span>
                                @if($product->price)
                                        ${{number_format($product->price,2)}}
                                    @endif
                            </span>
                            </h4>
                            <div class="main-detailse">
                                <div class="row">
                                    <a href="#" class="share-btn"><i class="fas fa-share-alt"></i></a>
                                    <div class="col-lg-6 pl-0 col-md-6">

                                        <label for="material">MATERIAL <span>*</span></label> <br>

                                        @php
                                            $materials = isset($metas['material']) ? explode(",",$metas['material']) : array();
                                            $materials = str_replace("_"," ",$materials);
                                        @endphp

                                        <select name="SIZE" id="SIZE" class="material">

                                            @forelse($materials as $key => $val)
                                                <option value="{{$val}}">{{$val}}</option>
                                            @empty
                                                <option value="SIZE">No material is available</option>
                                            @endforelse

                                        </select>

                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <label for="SIZE">SIZE <span>*</span></label> <br>

                                        @php
                                            $sizes = isset($metas['sizes']) ? explode(",",$metas['sizes']) : array();
                                            $sizes = str_replace("_"," ",$sizes);
                                        @endphp

                                        <select name="SIZE" id="SIZE" class="size">
                                            @forelse($sizes as $key => $val)
                                                <option value="{{$val}}">{{$val}}</option>
                                            @empty
                                                <option value="SIZE">No sizes is available</option>
                                            @endforelse
                                        </select>

                                    </div>
                                    <div class="col-lg-6 pl-0 col-md-6">
                                        <label for="EXTRA">EXTRA STUFFING <span>*</span></label> <br>
                                        <select name="EXTRA" id="EXTRA" class="extra">
                                            <option value="Yes">YES</option>
                                            <option value="No">NO</option>
                                        </select>

                                        <input type="hidden" value="1" class="quantity">
                                        <input type="hidden" value="{{$product->id}}" class="product_id">
                                        <input type="hidden" value="{{ (Auth::id()) ? Auth::id() : 'null'}}"
                                               class="user_id">
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="QUANTITY">
                                            <label for="material">QUANTITY <span>*</span></label> <br>
                                            <form action="" class="product-quantity">
                                                <input type="text" class="quantity" value="1">
                                                <i class="far fa-plus plus-increment"></i>
                                                <i class="far fa-minus minus-decrement"></i>
                                            </form>

                                        </div>
                                    </div>
                                    <div class="col-lg-6 pl-0 col-md-6 artist-text">
                                        <h4 class="user-text">
                                            @if(isset($artist) && count($artist) > 0)
                                                <img src="{{ asset('storage/'.$artist['image'])}}" alt=""> {{ucfirst($artist['name'])}}
                                                <div class="d-flex">
                                                    @forelse($artist['additional_details'] as $key => $val)
                                                        @if($val != null)
                                                            <a href="{{$val}}" class="artist_socialLinks mr-1">{{ucfirst($key)}}</a>
                                                        @endif
                                                    @empty
                                                    @endforelse
                                                </div>
                                            @endif
                                        </h4>
                                    </div>
                                    <div class="col-lg-6 col-md-6 artist-text  artist-textTWO">
                                        <h4><b>Shipping In: </b> 2 to 5 days</h4>
                                    </div>
                                    <div class="col-lg-12 pl-0">
                                        <div class="product-btns">
                                            {{--                                        <a href="#" class="THEME-BTN Buy-now">Buy Now <i class="fal fa-angle-right"></i></a>--}}
                                            <button type="submit" class="THEME-BTN Buy-now">Buy Now <i
                                                    class="fal fa-angle-right"></i></button>
                                            <a href="#" class="THEME-BTN transparent-btn"><i class="fal fa-heart"></i>
                                                Add to Wishlist </a>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 pl-0">
                                        <div class="payment-mathoda">
                                            <ul>
                                                <li><a href="#"><img src="{{ asset('front-end/assets/img/Visa.png')}}"
                                                                     alt=""></a></li>
                                                <li><a href="#"><img
                                                            src="{{ asset('front-end/assets/img/Mastercard-logo.png')}}"
                                                            alt=""></a></li>
                                                <li><a href="#"><img src="{{ asset('front-end/assets/img/payPal.png')}}"
                                                                     alt=""></a></li>
                                                <li><a href="#"><img src="{{ asset('front-end/assets/img/sofort.png')}}"
                                                                     alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row main-detailse-products">
                <div class="col-lg-12">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                               role="tab" aria-controls="pills-home" aria-selected="true">FULL DESCRIPTION</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                               role="tab" aria-controls="pills-profile" aria-selected="false">CUSTOMER REVIEWS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                               role="tab" aria-controls="pills-contact" aria-selected="false">SHIPPING & RETURNS</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                             aria-labelledby="pills-home-tab">

                            <div class="row intro-area">
                                <div class="col-lg-6">
                                    <div class="intro-area">
                                        <h3>{{ ucfirst($product->title)  }} </h3>
                                        <p>{{ isset($metas['product_video_description']) ? strip_tags($metas['product_video_description']) : "" }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="introVideo">

                                        {{--                                        @if($product['video'])--}}
                                        {{--                                            <iframe width="560" height="315" src="{{$product['video']}}" frameborder="0"></iframe>--}}
                                        {{--                                        @else--}}
                                        {{--                                            <a href="https://www.youtube.com/watch?v=FzG4uDgje3M" class="playBTN"><img src="{{ asset('front-end/assets/img/youTube.png')}}" alt=""></a>--}}
                                        {{--                                        @endif--}}

                                        {{--                                        <img src="{{ asset('front-end/assets/img/1.png')}}" alt="">--}}
                                        <?php if(isset($product['video'])) {?>
                                        {{--                                        <a href="{{url('/storage/'.$product['video'])}}" class="playBTN"><img src="{{ asset('front-end/assets/img/youTube.png')}}" alt=""></a>--}}
                                        <?php }else{?>
                                        {{--                                        <a href="{{url('#')}}" class="playBTN"><img src="{{ asset('front-end/assets/img/youTube.png')}}" alt=""></a>--}}
                                        <?php } ?>


                                    </div>
                                </div>
                            </div>
                            <div class="row buyProduct">
                                <div class="col-lg-8">
                                    <div class="product-img">
                                        <img src="{{ asset('front-end/assets/img/client2.jpg')}}" alt="">
                                        <h1>+</h1>
                                        <img src="{{ asset('storage/'.$product->image)}}" alt="product_featured_img">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="proBuyDetailse">
                                        <h2>Buy both of it & save <b>$45.00</b></h2>
                                        <a href="#" class="THEME-BTN">Buy Now </a>
                                    </div>
                                </div>
                            </div>

                            <div class="row product-feture">

                                <ul>
                                    <!-- Handle From backend category when apply default setting of category if status is on -->
                                    @forelse($features as $key => $val)
                                        <li>{{$val['title_'.$key]}} <br> <span>{{$val['detail_'.$key]}}</span></li>
                                    @empty
                                        @php
                                            //Show default category setting when meta key exists but empty
                                            if($cst[0]["status"] == "on"){

                                                $features    = [];
                                                $f_title    = unserialize($category_metas['category_prod_feature_title']);
                                                $f_detail   = unserialize($category_metas['category_prod_feature_detail']);

                                                if( !empty($f_title) ){
                                                    foreach ($f_title as $key => $val){
                                                      $features[$key]['title_'.$key]  = $val;
                                                      $features[$key]['detail_'.$key] = $f_detail[$key];
                                                    }
                                                }
                                            }
                                        @endphp
                                        @forelse($features as $key => $val)
                                            <li>{{$val['title_'.$key]}} <br> <span>{{$val['detail_'.$key]}}</span></li>
                                        @empty
                                            <li> Data Not Found</li>
                                        @endforelse
                                    @endforelse
                                </ul>

                            </div>


                            <div class="row reasons">
                                <ul>
                                    <!-- Handle From backend category when apply default setting of category if status is on -->
                                    @forelse($reasons as $key => $val)
                                        <li><span><b>*</b> {{$val['title_'.$key]}} </span> {{$val['detail_'.$key]}}</li>
                                    @empty

                                        @php
                                            //Show default category setting when meta exists but empty
                                            if($cst[0]["status"] == "on"){

                                                $reasons    = [];
                                                $r_title    = unserialize($category_metas["category_reason_title"]);
                                                $r_detail   = unserialize($category_metas["category_reason_detail"] );

                                                if( !empty($r_title) ){
                                                    foreach ($r_title as $key => $val){
                                                      $reasons[$key]['title_'.$key]  = $val;
                                                      $reasons[$key]['detail_'.$key] = $r_detail[$key];
                                                    }
                                                }
                                            }
                                        @endphp

                                        @forelse($reasons as $key => $val)
                                            <li><span><b>*</b> {{$val['title_'.$key]}} </span> {{$val['detail_'.$key]}}
                                            </li>
                                        @empty
                                            <li><span><b>*</b> Data Not Found</span></li>
                                        @endforelse

                                    @endforelse
                                </ul>
                            </div>
                            <div class="row gift">
                                <ul>
                                    <li><span>Spoil Your Loved Ones With A Stunning Gift!</span> <br>
                                        Surprise Your Wife, Girlfriend, Husband, Boyfriends, Kids Or Friends With An
                                        Almost Life-Size Furry Friend And Offer Them A Breath-Taking Birthday,
                                        Anniversary Or Holiday Present!
                                    </li>
                                    <li><span>Exclusive Peace-Of-Mind Warranty!</span> <br>
                                        Our Elite Dakimakura Pillow Covers Come With An Exclusive 3-Year Wash, Print,
                                        And Production Warranty, So You Can Sleep Better At Night!
                                    </li>
                                    <li><span>Please Remember!</span> <br> The Pillow Insert Or Dakimakura Pillow Is Not
                                        Included In The Package. Available In 2 Standard Sizes: 150Cm X 50Cm (59” X 20”)
                                        & 160Cm X 50Cm (63” X 20”)

                                    </li>
                                    <li><span> What Are You Waiting For? Click “Add To Cart” NOW!  </span> <br>
                                    </li>
                                </ul>
                            </div>

                            @php
                                $posters = unserialize($category_meta['poster_images']);
                                $actual_materials_pic = unserialize($category_meta['actual_material_images']);
                            @endphp

                            @forelse($posters as $key => $val)
                                <div class="row products-img">
                                    <div class="col-lg-12">
                                        <img src="{{ asset('storage/'.$val)}}" alt="">
                                    </div>
                                </div>
                            @empty
                                <div class="row products-img">
                                    <div class="col-lg-12">
                                        <img src="{{ asset('front-end/assets/img/comic.png')}}" alt="">
                                    </div>
                                </div>
                                <div class="row products-img">
                                    <div class="col-lg-12">
                                        <img src="{{ asset('front-end/assets/img/comic2.png')}}" alt="">
                                    </div>
                                </div>
                            @endforelse

                            <div class="row products-img threeimg" style="margin-top: 50px;">
                                <div class="col-lg-12">
                                    @forelse($actual_materials_pic as $key => $val)
                                        <img src="{{ asset('storage/'.$val)}}" alt="">
                                    @empty
                                        <img src="{{ asset('front-end/assets/img/1.png')}}" alt="">
                                        <img src="{{ asset('front-end/assets/img/2.png')}}" alt="">
                                        <img src="{{ asset('front-end/assets/img/3.png')}}" alt="">
                                    @endforelse
                                </div>
                            </div>


                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                             aria-labelledby="pills-profile-tab">
                            <div class="row classreview-tabs">
                                <div class="col-lg-12">
                                    <div class="customars_review">
                                        <div class="row">
                                            <div class="col-lg-1 col-md-2">
                                                <div class="customars-img">
                                                    <h5><img src="{{ asset('front-end/assets/img/man.png')}}" alt="">
                                                        Kale S.</h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-10">
                                                <p class="review-text">"100% satisfied by their professional service,
                                                    WILL ORDER AGAIN!!!"</p>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="rivew-START">
                                                    <ul>
                                                        <li><p>12/5/19 13:00</p></li>
                                                        <li>5.0</li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="customars_review">
                                        <div class="row">
                                            <div class="col-lg-1 col-md-2">
                                                <div class="customars-img">
                                                    <h5><img src="{{ asset('front-end/assets/img/man.png')}}" alt="">
                                                        Kale S.</h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-10">
                                                <p class="review-text">"100% satisfied by their professional service,
                                                    WILL ORDER AGAIN!!!"</p>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="rivew-START">
                                                    <ul>
                                                        <li><p>12/5/19 13:00</p></li>
                                                        <li>5.0</li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="customars_review">
                                        <div class="row">
                                            <div class="col-lg-1 col-md-2">
                                                <div class="customars-img">
                                                    <h5><img src="{{ asset('front-end/assets/img/man.png')}}" alt="">
                                                        Kale S.</h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-10">
                                                <p class="review-text">"100% satisfied by their professional service,
                                                    WILL ORDER AGAIN!!!"</p>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="rivew-START">
                                                    <ul>
                                                        <li><p>12/5/19 13:00</p></li>
                                                        <li>5.0</li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="customars_review">
                                        <div class="row">
                                            <div class="col-lg-1 col-md-2">
                                                <div class="customars-img">
                                                    <h5><img src="{{ asset('front-end/assets/img/man.png')}}" alt="">
                                                        Kale S.</h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-10">
                                                <p class="review-text">"100% satisfied by their professional service,
                                                    WILL ORDER AGAIN!!!"</p>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="rivew-START">
                                                    <ul>
                                                        <li><p>12/5/19 13:00</p></li>
                                                        <li>5.0</li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="customars_review">
                                        <div class="row">
                                            <div class="col-lg-1 col-md-2">
                                                <div class="customars-img">
                                                    <h5><img src="{{ asset('front-end/assets/img/man.png')}}" alt="">
                                                        Kale S.</h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-10">
                                                <p class="review-text">"100% satisfied by their professional service,
                                                    WILL ORDER AGAIN!!!"</p>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="rivew-START">
                                                    <ul>
                                                        <li><p>12/5/19 13:00</p></li>
                                                        <li>5.0</li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="customars_review last-review-boxes">
                                        <div class="row">
                                            <div class="col-lg-1 col-md-2">
                                                <div class="customars-img">
                                                    <h5><img src="{{ asset('front-end/assets/img/man.png')}}" alt="">
                                                        Kale S.</h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-10">
                                                <p class="review-text">"100% satisfied by their professional service,
                                                    WILL ORDER AGAIN!!!"</p>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="rivew-START">
                                                    <ul>
                                                        <li><p>12/5/19 13:00</p></li>
                                                        <li>5.0</li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                        <li><i class="fas fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="more-review text-center">
                                        <a href="#" class="seeMORE">SEE MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                             aria-labelledby="pills-contact-tab">
                            <div class="row SHIPPING_RETURNS">
                                <div class="col-lg-12">
                                    <p><b>*NOTE:</b> This product is made to order and requires 3 to 5 business days
                                        production time. This is to ensure all products shipped out are high quality and
                                        have undergone strict quality assurance procedure.</p>
                                    <h5> Where Can it Ship? </h5>
                                    <h6>1. Normal Airmail Delivery Schedule:</h6>
                                    <p class="cost-text">Cost: <b>FREE</b> for orders above $100.00 USD*, please add
                                        items to your cart and use the shipping estimator on the cart page</p>
                                    <ul>
                                        <li><b>United States:</b> <span>7 to 14 business days.</span></li>
                                        <li><b>Canada:</b> <span>7 to 14 business days.</span></li>
                                        <li><b>Europe: </b> <span>7 to 14 business days.</span></li>
                                        <li><b>Hong Kong:</b> <span>2 to 4 business days </span></li>
                                        <li><b>Asian & Australia: </b> <span>4 to 17 business days </span></li>
                                        <li><b>Mexico & South America:</b> <span>12 - 25 business days </span></li>
                                        <li><b>Middle East: </b> <span> 10 - 25 business days </span></li>
                                    </ul>
                                    <h5 class="express">2. Express Mail Service (EMS) / UPS / DHL / Fedex Delivery
                                        Schedule:</h5>
                                    <div class="alp">
                                        <p class="cost-two">Cost: $37.00 USD and up, please add items to your cart and
                                            use the shipping estimator on the cart page</p>
                                        <p>United States: 2 - 5 business days (up to 10 business days for Hawaii)</p>
                                        <p>Canada: 2 - 5 business days</p>
                                        <p>Europe: 1 - 6 business days</p>
                                        <p>Hong Kong (Local Courier): 1 - 2 business days</p>
                                        <p>Asia: 1 - 4 business days</p>
                                        <p>Australia: 2 - 6 business days</p>
                                        <p>Mexico & South American: 4 - 7 business days (*EMS shipping is highly
                                            recommended for these countries)</p>
                                        <p>Middle East: 6 - 7 business days (*EMS shipping is highly recommended for
                                            these countries)</p>
                                    </div>

                                    <h5 class="about">About Shipping</h5>
                                    <p class="ship">We ship via fast and trackable shipping services.</p>
                                    <h5 class="Expected">Expected Shipment Date</h5>
                                    <p class="products">This product is expected to ship within 2 business days.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product-detailse END -->

    <!-- product-area START -->
    <div class="product-area single-product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h1>YOU MAY ALSO BE INTERESTED IN</h1>
                    </div>
                </div>
            </div>
            <div class="products">
                <div class="row product-slider owl-carousel">
                    <div class="product-slide-item">
                        <div class="product-box">
                            <img src="{{ asset('front-end/assets/img/product-img.png')}}" alt="">
                            <div class="product-btns">
                                <a href="#" class="THEME-BTN">Buy Now </a>
                                <a href="#" class="THEME-BTN transparent-btn"><img
                                        src="{{ asset('front-end/assets/img/heart.svg')}}" alt=""> Add to Wishlist</a>
                            </div>
                            <div class="productBox-content">
                                <h5>Body Pillow 150x50cm <span>$55.00</span></h5>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                            </div>
                        </div>
                    </div>
                    <div class="product-slide-item">
                        <div class="product-box">
                            <img src="{{ asset('front-end/assets/img/product-img2.png')}}" alt="">
                            <div class="product-btns">
                                <a href="#" class="THEME-BTN">Buy Now </a>
                                <a href="#" class="THEME-BTN transparent-btn"><img
                                        src="{{ asset('front-end/assets/img/heart.svg')}}" alt=""> Add to Wishlist</a>
                            </div>
                            <div class="productBox-content">
                                <h5>Furry Magic Mug <span>$25.00</span></h5>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                            </div>
                        </div>
                    </div>
                    <div class="product-slide-item">
                        <div class="product-box">
                            <img src="{{ asset('front-end/assets/img/product-img3.png')}}" alt="">
                            <div class="product-btns">
                                <a href="#" class="THEME-BTN">Buy Now </a>
                                <a href="#" class="THEME-BTN transparent-btn"><img
                                        src="{{ asset('front-end/assets/img/heart.svg')}}" alt=""> Add to Wishlist</a>
                            </div>
                            <div class="productBox-content">
                                <h5>Fuzzy Soft Blanket... <span>$50.00</span></h5>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                            </div>
                        </div>
                    </div>
                    <div class="product-slide-item">
                        <div class="product-box">
                            <img src="{{ asset('front-end/assets/img/product-img4.png')}}" alt="">
                            <div class="product-btns">
                                <a href="#" class="THEME-BTN">Buy Now </a>
                                <a href="#" class="THEME-BTN transparent-btn"><img
                                        src="{{ asset('front-end/assets/img/heart.svg')}}" alt=""> Add to Wishlist</a>
                            </div>
                            <div class="productBox-content">
                                <h5>Mega Poster A1 Size <span>$16.00</span></h5>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                            </div>
                        </div>
                    </div>
                    <div class="product-slide-item">
                        <div class="product-box">
                            <img src="{{ asset('front-end/assets/img/product-img5.png')}}" alt="">
                            <div class="product-btns">
                                <a href="#" class="THEME-BTN">Buy Now </a>
                                <a href="#" class="THEME-BTN transparent-btn"><img
                                        src="{{ asset('front-end/assets/img/heart.svg')}}" alt=""> Add to Wishlist</a>
                            </div>
                            <div class="productBox-content">
                                <h5>Furry Backpack <span>$32.00</span></h5>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product-area END -->

@endsection


@section('custom-front-script')
    <script>

        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

        //   $('.plus-increment').css('position','absolute','margin-top','-27px !Important','margin-left','20px!Important')
        // $('.minus-decrement').css('position','absolute','margin-top','-27px !Important','margin-left','200px!Important')

        var quantity = $('.quantity').val();

        $('body').on('click', '.plus-increment', function () {
            var $qty = $(this).parent().find('.quantity');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal)) {
                $qty.val(currentVal + 1);
                quantity = $qty.val(currentVal + 1);
                quantity = quantity[0].value;
            }
        });
        $('body').on('click', '.minus-decrement', function () {
            var $qty = $(this).parent().find('.quantity');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
                // console.log($qty.val(currentVal - 1))
                quantity = $qty.val(currentVal - 1);
                quantity = quantity[0].value;
            }
        });


        $(".buy-now").submit(function (e) {

            e.preventDefault();


            var size = $(".size").val();
            var material = $(".material").val();
            var extra = $(".extra").val();
            var product_id = $(".product_id").val();
            var route = '{{ route('order.buynow')}}';
            var user_id = $(".user_id").val();
            var extra = $(".extra").val();

            $.ajax({
                type: 'POST',
                url: route,
                data: {
                    size: size,
                    material: material,
                    quantity: quantity,
                    product_id: product_id,
                    user_id: user_id,
                    extra: extra
                },
                success: function (data) {
                    localStorage.setItem('count', data.count);
                    $('.custom-badge-cart-primary').html(data.count);
                    console.log(data.count)
                    toastr.success('Product added in cart successfully ', 'Temporary Item');
                },
                error: function (data) {
                    toastr.warning('Product failed to add in cart', 'Temporary Item');
                }

            });


        });

        $('.container-gallery').gallery({
            height: 650,
            items: 6,
            480: {
                items: 2,
                height: 400,
                thmbHeight: 100
            },
            768: {

                items: 3,
                height: 500,
                thmbHeight: 120
            },
            600: {

                items: 4
            },
            992: {

                items: 5,
                height: 350
            }

        });

    </script>
@endsection
