@extends('front-layout.app')

@section('title')
    Product
@endsection


@section('custom-front-css')
    <style>
        .minus-decrement {
            /*display: flex;*/
            /*margin-top: -15px;*/
            /*margin-left: 210px;*/
            display: block;
            cursor: pointer;
            position: absolute !important;
            left: 0px !important;
            font-size: 20px !important;
            top: 43px !important;
            border: 1px solid #a2a2a2 !important;
            width: 60px !important;
            height: 42px !important;
            padding: 10px !important;
            border-radius: 30px 0px 0px 30px !important;
            text-align: center !important;
            border-top: none !important;
            border-bottom: none !important;

        }

        .plus-increment {
            /*display: flex;*/
            /*margin-top: -27px;*/
            /*margin-left: 20px;*/
            display: block;
            cursor: pointer;
            position: absolute !important;
            right: 0px !important;
            font-size: 20px !important;
            top: 43px !important;
            border: 1px solid #a2a2a2 !important;
            width: 60px !important;
            height: 42px !important;
            padding: 10px !important;
            border-radius: 0px 30px 30px 0 !important;
            text-align: center !important;
            border-top: none !important;
            border-bottom: none !important;
        }

    </style>

@endsection


@section('Main')
    @php
        //Default Category Setting
         $cst = \App\category_setting::all();
         $cst  = $cst->toArray();

         $cate_setting = \App\category_setting::all();

         $category_metas = [];

         foreach ($cate_setting as $key => $val){
            $category_metas["category_reason_title"]            = $val->reason_titles;
            $category_metas["category_reason_detail"]           = $val->reason_descriptions;
            $category_metas["category_prod_feature_title"]      = $val->feature_titles;
            $category_metas["category_prod_feature_detail"]     = $val->feature_descriptions;
            $category_metas["poster_images"]                    = $val->posters;
            $category_metas["actual_material_images"]           = $val->actual_material_pics;
         }


    @endphp


    <!-- Product-detailse START -->
    <div class="Product-detailse">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="img-gellery">
                        <div class="jquery-script-clear"></div>
                        <div class="container-gallery">

                            @php
                                $current_time       = time();
                                $product_created_at = strtotime($product->created_at);

                                $diff_days = ( ($current_time - $product_created_at) / 86400)  ;
                            @endphp

                            @if($diff_days <= 5)
                                <div class="ribbon-wrapper ribbon-lg">
                                    <div class="ribbon bg-gradient-danger">
                                        New Product
                                    </div>
                                </div>
                            @endif

                            @if(!empty($metas['sale_price']))
                                <div class="ribbon-wrapper ribbon-lg">
                                    <div class="ribbon bg-gradient-warning">
                                        Sale Offer
                                    </div>
                                </div>
                            @endif


                            @forelse($selectedProductImages as $key => $val)
                                <img src="{{ asset($val)}}" alt="">
                            @empty
                                <img src="{{ asset('images/placeholders/default-placeholder-600x600.png')}}" alt="placeholder">
                            @endforelse

                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <form class="buy-now" action="post">
                        @csrf
                        <div class="product-pay">

                            <h3> {{ ucfirst($product->title)  }} <br> <span>SHORT DESCRIPTION</span></h3>
                            <p class="original_description"> {{substr(strip_tags($product->description),0,500)."..."}}</p>

                            <h4 class="price">
                                price <br>
                                <span>
                                    @if($product->price)

                                        @if(!empty($metas['sale_price']))
                                            <div class="custom-shopProduct-price">
                                                <span style="text-decoration: line-through;top:-15px">
                                                    ${{number_format($product->price,2)}}
                                                    <br/>
                                                </span>
                                                <span class="original_price">
                                                    ${{number_format($metas['sale_price'],2)}}
                                                </span>
                                            </div>
                                        @else
                                            <span class="original_price">
                                                ${{number_format($product->price,2)}}
                                            </span>
                                        @endif

                                    @endif
                            </span>
                            </h4>
                            <div class="main-detailse">
                                <div class="row">
                                    <a href="#" class="share-btn"><i class="fas fa-share-alt"></i></a>

{{--                                    <div class="col-lg-6 pl-0 col-md-6">--}}
{{--                                        <label for="material">MATERIAL <span>*</span></label> <br>--}}
{{--                                        @php--}}
{{--                                            $materials = isset($metas['material']) ? unserialize($metas['material'])  : array();--}}
{{--                                        @endphp--}}
{{--                                        <select name="SIZE" id="SIZE" class="material">--}}
{{--                                            @forelse($materials as $key => $val)--}}
{{--                                                <option value="{{str_replace("_"," ",$val)}}">{{str_replace("_"," ",$val)}}</option>--}}
{{--                                            @empty--}}
{{--                                                <option value="SIZE"></option>--}}
{{--                                            @endforelse--}}
{{--                                        </select>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-lg-6 col-md-6">--}}
{{--                                        <label for="SIZE">SIZE <span>*</span></label> <br>--}}
                                        @php
                                            $sizes = isset($metas['sizes']) ? unserialize($metas['sizes']) : array();
                                            $weight = (isset($metas['weight']) ? $metas['weight'] : null);
                                        @endphp
{{--                                        <select name="SIZE" id="SIZE" class="size">--}}
{{--                                            @forelse($sizes as $key => $val)--}}
{{--                                                <option value="{{str_replace("_"," ",$val)}}">{{str_replace("_"," ",$val)}}</option>--}}
{{--                                            @empty--}}
{{--                                                <option value="SIZE"></option>--}}
{{--                                            @endforelse--}}
{{--                                        </select>--}}
{{--                                    </div>--}}

                                    <div class="col-lg-6 col-md-6">
                                        <div class="QUANTITY">
                                            <label for="material">QUANTITY <span>*</span></label> <br>
                                            <form action="" class="product-quantity">
                                                <input type="text" class="quantity" value="1">
                                                <i class="far fa-plus plus-increment"></i>
                                                <i class="far fa-minus minus-decrement"></i>
                                            </form>
                                        </div>

                                        <div class="stock-msg-section mt-4">
                                            <span class="stock-msg"></span>
                                        </div>

                                    </div>


                                    <div class="col-lg-6 pl-0 col-md-6">
{{--                                        <label for="EXTRA">EXTRA STUFFING <span>*</span></label> <br>--}}
{{--                                        <select name="EXTRA" id="EXTRA" class="extra">--}}
{{--                                            <option value="Yes">YES</option>--}}
{{--                                            <option value="No">NO</option>--}}
{{--                                        </select>--}}

                                        <input type="hidden" value="1" class="quantity">
                                        <input type="hidden" value="{{$weight}}" id="product_weight" class="product_weight">
                                        <input type="hidden" value="{{$product->id}}" class="product_id">
                                        <input type="hidden" value="{{ (Auth::id()) ? Auth::id() : 'null'}}"
                                               class="user_id">
                                    </div>


                                    @if(isset($product_variations) && count($product_variations) > 0)

                                        <div class="col-lg-6 pl-0 col-md-6">
                                            <div class="QUANTITY">
                                                <label for="variation">VARIATIONS<span></span></label> <br>
                                                <select name="product_avail_variation" id="product_avail_variation" class="extra">
                                                    <option value="">Select Variation</option>
                                                    @forelse($product_variations as $key => $val)
                                                        @php
                                                            $ff = unserialize($val->variation);
                                                        @endphp
                                                        <option value="{{$val->id}}">Variation {{$key+1}}</option>
                                                    @empty

                                                    @endforelse
                                                </select>
                                            </div>

                                        </div>

                                        <div class="col-lg-6 col-md-6">
                                            @forelse($product_variations as $key => $val)
                                                @php
                                                    $ff = unserialize($val->variation_meta);
                                                @endphp

                                                @foreach($ff as $k => $v)
                                                    <span variation_id="{{$val->id}}"
                                                          attribute="{{$v['attribute']}}"
                                                          variation="{{$v['variation']}}"
                                                          variation_image="{{$val->image}}"
                                                          variation_price="{{$val->price}}"
                                                          variation_description="{{$val->description}}"
                                                          class="variation-class"
                                                          style="display: none">
                                                    </span>
                                                @endforeach
                                            @empty

                                            @endforelse

                                            <div class="product-variation-section">

                                                <div class="vari-img-section">
                                                </div>

                                                <ul class="prod_vari_ul"></ul>

                                            </div>


                                        </div>

                                    @endif

                                    <div class="col-lg-6 col-md-6 artist-text  artist-textTWO">

                                        @php
                                            $estimate_time = isset( $metas['estimate_shipping']) ?  $metas['estimate_shipping'] : null;

                                            $prod_estimate_data = "";

                                            if($estimate_time != null){
                                                $prod_estimate_data = explode("-",$estimate_time);
                                            }

                                            $d1 = isset($prod_estimate_data[0]) ? $prod_estimate_data[0] : null;
                                            $d2 = isset($prod_estimate_data[1]) ? $prod_estimate_data[1] : null;
                                        @endphp

                                        @if(isset($prod_estimate_data) && $prod_estimate_data > 0 )
                                            @if( ($prod_estimate_data[0] != "" ) && ($prod_estimate_data[1] != "" ))
                                                <h4><b>Shipping In: </b>
                                                    {{$d1." to ".$d2}} days
                                                </h4>
                                            @endif
                                        @endif

                                    </div>

                                    <div class="col-lg-6 pl-0 col-md-6 artist-text">
                                        <h4 class="user-text">
{{--                                            @if(isset($artist) && count($artist) > 0)--}}
{{--                                                <img src="{{ asset('storage/'.$artist['image'])}}"--}}
{{--                                                     alt=""> {{ucfirst($artist['name'])}}--}}
{{--                                                <div class="d-flex">--}}
{{--                                                    @forelse($artist['additional_details'] as $key => $val)--}}
{{--                                                        @if($val != null)--}}
{{--                                                            <a href="{{$val}}"--}}
{{--                                                               class="artist_socialLinks mr-1">{{ucfirst($key)}}</a>--}}
{{--                                                        @endif--}}
{{--                                                    @empty--}}
{{--                                                    @endforelse--}}
{{--                                                </div>--}}
{{--                                            @endif--}}
                                        </h4>
                                    </div>

                                    <div class="col-lg-12 pl-0">

                                        @php
                                            $wishlist_products = [];

                                            if( Auth::check()){
                                                $current_user_id = Auth::user()->id;
                                                $wishlist = \App\Wishlist::where(['user_id' => $current_user_id])->get();

                                                $wishlist_products['user_id'] = $current_user_id;

                                                foreach ($wishlist as $key => $v){
                                                    $wishlist_products['product'][] = $v->product_id;
                                                }

                                            }
                                        @endphp


                                        <div class="product-btns">
                                            {{--<a href="#" class="THEME-BTN Buy-now">Buy Now <i class="fal fa-angle-right"></i></a>--}}

                                            @if (auth()->guest())
                                                {{--   data-toggle="modal" data-target="#non-auth-guest-modal"--}}
                                                <button type="button" class="THEME-BTN" id="BuyNow-non-auth" data-toggle="modal" data-target="#non-auth-guest-modal">Add to cart<i class="ml-2 fal fa-angle-right"></i></button>
                                            @else
                                                <button type="submit" class="THEME-BTN Buy-now BuyNow-auth">Add to cart<i class="ml-2 fal fa-angle-right"></i></button>
                                            @endif

                                            @if(isset($wishlist_products['product']) && count($wishlist_products['product']) > 0)

                                                @if(in_array($product->id , $wishlist_products['product']))
                                                    <a href="" product_id="{{$product->id}}" class="THEME-BTN transparent-btn transparent-btn-single-product remove-wishlist-btn">
                                                        <i class="fas fa-heart" style='color:#DD574E'></i>
                                                        Remove</a>
                                                @else
                                                    <a href="" product_id="{{$product->id}}" class="THEME-BTN transparent-btn transparent-btn-single-product add-wishlist-btn">
                                                        <i class="fal fa-heart"></i>
                                                        Add to Wishlist</a>
                                                @endif

                                            @else

                                                @if(Auth::check())
                                                    <a href="" product_id="{{$product->id}}" class="THEME-BTN transparent-btn transparent-btn-single-product add-wishlist-btn">
                                                        <i class="fal fa-heart"></i>
                                                        Add to Wishlist</a>
                                                @endif

                                            @endif

                                        </div>
                                    </div>

                                    <!-- Modal #non-auth-guest-modal -->
                                    <div class="modal fade" id="non-auth-guest-modal" tabindex="-1" role="dialog" aria-labelledby="non-auth-guest-modalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="non-auth-guest-modalLabel">Alert! - Add to Cart</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    You want to sure without creating an account in Shopfurries ?
                                                    <br/>
                                                    If you want to creat an account in Shopfurries here is the link <a
                                                        href="{{route('register')}}">Register</a>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" id="guestBtn-buynow">Yes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 pl-0">
                                        <div class="payment-mathoda">
                                            <ul>
                                                <li><a href="#"><img src="{{ asset('front-end/assets/img/Visa.png')}}"
                                                                     alt=""></a></li>
                                                <li><a href="#"><img
                                                            src="{{ asset('front-end/assets/img/Mastercard-logo.png')}}"
                                                            alt=""></a></li>
                                                <li><a href="#"><img src="{{ asset('front-end/assets/img/payPal.png')}}"
                                                                     alt=""></a></li>
                                                <li><a href="#"><img src="{{ asset('front-end/assets/img/sofort.png')}}"
                                                                     alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row main-detailse-products">
                <div class="col-lg-12">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                               role="tab" aria-controls="pills-home" aria-selected="true">FULL DESCRIPTION</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                               role="tab" aria-controls="pills-profile" aria-selected="true">CUSTOMER REVIEWS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                               role="tab" aria-controls="pills-contact" aria-selected="false">SHIPPING & RETURNS</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                             aria-labelledby="pills-home-tab">

                            <div class="row intro-area">
                                <div class="col-lg-6">
                                    <div class="intro-area">
                                        <h3>{{ ucfirst($product->title)  }} </h3>
                                        <p>{{ isset($metas['product_video_description']) ? strip_tags($metas['product_video_description']) : "" }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">

                                    <div class="introVideo">
                                        <img src="{{ asset('front-end/assets/img/1.png')}}" alt="">
                                        <?php if(isset($product['video'])) {?>
                                        <a href="{{url('/storage/'.$product['video'])}}" class="playBTN"><img src="{{ asset('front-end/assets/img/youTube.png')}}" alt=""></a>
                                        <?php }else{?>
                                        <a href="{{url('#')}}" class="playBTN"><img src="{{ asset('front-end/assets/img/youTube.png')}}" alt=""></a>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>


                            <div class="row product-feture">

                                <ul>
                                    <!-- Handle From backend category when apply default setting of category if status is on -->
                                    @forelse($features as $key => $val)
                                        <li>{{$val['title_'.$key]}} <br> <span>{{$val['detail_'.$key]}}</span></li>
                                    @empty
                                        @php
                                            //Show default category setting when meta key exists but empty
                                            if($cst[0]["status"] == "on"){

                                                $features    = [];
                                                $f_title    = unserialize($category_metas['category_prod_feature_title']);
                                                $f_detail   = unserialize($category_metas['category_prod_feature_detail']);

                                                if( !empty($f_title) ){
                                                    foreach ($f_title as $key => $val){
                                                      $features[$key]['title_'.$key]  = $val;
                                                      $features[$key]['detail_'.$key] = $f_detail[$key];
                                                    }
                                                }
                                            }
                                        @endphp
                                        @forelse($features as $key => $val)
                                            <li>{{$val['title_'.$key]}} <br> <span>{{$val['detail_'.$key]}}</span></li>
                                        @empty
                                            <li> Data Not Found</li>
                                        @endforelse
                                    @endforelse
                                </ul>

                            </div>


                            <div class="row reasons">
                                <ul>
                                    <!-- Handle From backend category when apply default setting of category if status is on -->
                                    @forelse($reasons as $key => $val)
                                        <li><span><b>*</b> {{$val['title_'.$key]}} </span> {{$val['detail_'.$key]}}</li>
                                    @empty

                                        @php
                                            //Show default category setting when meta exists but empty
                                            if($cst[0]["status"] == "on"){

                                                $reasons    = [];
                                                $r_title    = unserialize($category_metas["category_reason_title"]);
                                                $r_detail   = unserialize($category_metas["category_reason_detail"] );

                                                if( !empty($r_title) ){
                                                    foreach ($r_title as $key => $val){
                                                      $reasons[$key]['title_'.$key]  = $val;
                                                      $reasons[$key]['detail_'.$key] = $r_detail[$key];
                                                    }
                                                }
                                            }
                                        @endphp

                                        @forelse($reasons as $key => $val)
                                            <li><span><b>*</b> {{$val['title_'.$key]}} </span> {{$val['detail_'.$key]}}
                                            </li>
                                        @empty
                                            <li><span><b>*</b> Data Not Found</span></li>
                                        @endforelse

                                    @endforelse
                                </ul>
                            </div>
                            <div class="row gift">
                                <ul>
                                    <li><span>Spoil Your Loved Ones With A Stunning Gift!</span> <br>
                                        Surprise Your Wife, Girlfriend, Husband, Boyfriends, Kids Or Friends With An
                                        Almost Life-Size Furry Friend And Offer Them A Breath-Taking Birthday,
                                        Anniversary Or Holiday Present!
                                    </li>
                                    <li><span>Exclusive Peace-Of-Mind Warranty!</span> <br>
                                        Our Elite Dakimakura Pillow Covers Come With An Exclusive 3-Year Wash, Print,
                                        And Production Warranty, So You Can Sleep Better At Night!
                                    </li>
                                    <li><span>Please Remember!</span> <br> The Pillow Insert Or Dakimakura Pillow Is Not
                                        Included In The Package. Available In 2 Standard Sizes: 150Cm X 50Cm (59” X 20”)
                                        & 160Cm X 50Cm (63” X 20”)

                                    </li>
                                    <li><span> What Are You Waiting For? Click “Add To Cart” NOW!  </span> <br>
                                    </li>
                                </ul>
                            </div>

                            @php
                                $posters = unserialize($category_meta['poster_images']);
                                $actual_materials_pic = unserialize($category_meta['actual_material_images']);
                            @endphp

                            @forelse($posters as $key => $val)
                                <div class="row products-img">
                                    <div class="col-lg-12">
                                        <img src="{{ asset('storage/'.$val)}}" alt="">
                                    </div>
                                </div>
                            @empty

                                @php
                                    //Show default category setting when meta exists but empty
                                    if($cst[0]["status"] == "on"){
                                        $posters  = unserialize($category_metas["poster_images"]);
                                    }
                                @endphp

                                @forelse($posters as $key => $val)
                                    <div class="row products-img">
                                        <div class="col-lg-12">
                                            <img src="{{ asset('storage/'.$val)}}" alt="">
                                        </div>
                                    </div>
                                @empty
                                    <div class="row products-img">
                                        <div class="col-lg-12">
                                            Data Not Found
                                        </div>
                                    </div>
                                @endforelse

                            @endforelse

                            <div class="row products-img threeimg" style="margin-top: 50px;">
                                <div class="col-lg-12">
                                    @forelse($actual_materials_pic as $key => $val)
                                        <img src="{{ asset('storage/'.$val)}}" alt="">
                                    @empty

                                        @php
                                            //Show default category setting when meta exists but empty
                                            if($cst[0]["status"] == "on"){
                                                $actual_materials_pic = unserialize($category_metas["actual_material_images"]);
                                            }
                                        @endphp

                                        @forelse($actual_materials_pic as $key => $val)
                                            <img src="{{ asset('storage/'.$val)}}" alt="">
                                        @empty
                                            Data Not Found
                                        @endforelse

                                    @endforelse
                                </div>
                            </div>


                        </div>

                        <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                             aria-labelledby="pills-profile-tab">
                            <div class="row classreview-tabs ">


                                @if(Auth::check())
                                    <nav class="custom-review-tabs w-100">
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                               href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Leave
                                                a Review</a>
                                        </div>
                                    </nav>
                                    <div class="tab-content w-100" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                             aria-labelledby="nav-home-tab">

                                            <form action="{{route('product.review-create')}}" method="post"
                                                  enctype="multipart/form-data">
                                                <div class="dalivery-info-area" id="customer-review-section">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <h3>Lets us know what you think...</h3>
                                                        </div>
                                                    </div>

                                                    <div class="row pt-0 pb-2">
                                                        <div class="col-md-3 col-lg-2">
                                                            <div class="rivew-START text-justify">
                                                                <span>Poor</span>
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                </ul>
                                                                <input type="radio" name="flag" id="inlineRadio2"
                                                                       value="poor">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-lg-2">
                                                            <div class="rivew-START text-justify">
                                                                <span>Moderate</span>
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                </ul>
                                                                <input type="radio" name="flag" id="inlineRadio2"
                                                                       value="moderate">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-lg-2">
                                                            <div class="rivew-START text-justify">
                                                                <span>Average</span>
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                </ul>
                                                                <input type="radio" name="flag" id="inlineRadio2"
                                                                       value="average">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-lg-2">
                                                            <div class="rivew-START text-justify">
                                                                <span>Good</span>
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                </ul>
                                                                <input type="radio" name="flag" id="inlineRadio2"
                                                                       value="good">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-lg-2">
                                                            <div class="rivew-START text-justify">
                                                                <span>Perfect</span>
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                    <li class="list-inline-item">
                                                                        <svg class="svg-inline--fa fa-star fa-w-18"
                                                                             aria-hidden="true" focusable="false"
                                                                             data-prefix="fas" data-icon="star" role="img"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 576 512" data-fa-i2svg="">
                                                                            <path fill="currentColor"
                                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                        </svg><!-- <i class="fas fa-star"></i> --></li>
                                                                </ul>
                                                                <input type="radio" name="flag" id="inlineRadio2"
                                                                       value="perfect">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            @error('flag')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-12 custom-review-descrition-sec">
                                                            <div class="dalivery-addres">

                                                                <div class="row mt-3">
                                                                    <div class="col-md-12">
                                                                        <label>Description <span
                                                                                class="text-danger">*</span> : </label>
                                                                    </div>
                                                                    <div class="col-md-12">

                                                                    <textarea
                                                                        class="form-control form-control-sm address @error('description') is-invalid @enderror"
                                                                        name="description" id="description" cols="30"
                                                                        rows="4"></textarea>

                                                                        @error('description')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror

                                                                        @if(Auth::check())
                                                                            <input type="hidden" name="user_id" id="user_id"
                                                                                   value="{{Auth::user()->id}}">
                                                                        @endif
                                                                        <input type="hidden" name="product_id"
                                                                               id="product_id" value="{{$product->id}}">

                                                                    </div>
                                                                </div>
                                                                <div class="row mt-3">
                                                                    <div class="col-md-12">
                                                                        <label>Photo: </label>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <input type="file" multiple name="photo[]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-3">
                                                                    <div class="col-md-12">
                                                                        <label>Enter Video URL: </label>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <input type="text" name="video"
                                                                               class="form-control form-control-sm address"/>
                                                                    </div>
                                                                </div>

                                                                <div class="row mt-5">
                                                                    <div class="col-md-12">
                                                                        <input type="submit"
                                                                               class="btn btn-primary btn-sm custom-primary-btn"
                                                                               value="Submit Review">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endif

                                @forelse($reviews as $key => $val)
                                    @php
                                        $user = App\User::find($val->user_id);
                                        $photos = unserialize($val->image);
                                    @endphp
                                    <div class="col-lg-12 mt-3">
                                        <div class="customars_review last-review-boxes">
                                            <div class="row">
                                                <div class="col-lg-1 col-md-2">
                                                    <div class="customars-img">
                                                        <h5>
                                                            <a href="{{asset(profile_pic($user->id))}}"
                                                               data-lightbox="image-1">
                                                                <img src="{{asset(profile_pic($user->id))}}"
                                                                     alt="">
                                                            </a>
                                                            {{ucfirst($user->name)}}
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-10">
                                                    <p class="review-text">
                                                        "{{$val->description}}"
                                                    </p>

                                                    @forelse($photos as $key => $v)
                                                        <a href="{{asset('storage/'.$v)}}" data-lightbox="image-2">
                                                            <img src="{{asset('storage/'.$v)}}" alt="review_image" class="custom-review-image ml-2 mr-2" width="50" height="50">
                                                        </a>
                                                    @empty

                                                    @endforelse

                                                    {{--                                                    <div class="row mt-2">--}}
                                                    {{--                                                        <div class="col-md-2">--}}
                                                    {{--                                                            <span id="custom-review-reply" class="custom-review-reply">Reply</span>--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                        <div class="col-md-6">--}}
                                                    {{--                                                            <span class="ml-1 mr-1 custom-review-vote-btn" id="custom-review-vote-btn">--}}
                                                    {{--                                                              <i class="fa fa-thumbs-up"></i>(0)--}}
                                                    {{--                                                            </span>--}}
                                                    {{--                                                            <span class="ml-1 mr-1 custom-review-unvote-btn" id="custom-review-unvote-btn">--}}
                                                    {{--                                                                <i class="fa fa-thumbs-down"></i>(0)--}}
                                                    {{--                                                            </span>--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                    </div>--}}

                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="rivew-START">
                                                        <ul>
                                                            <li>
                                                                <p>{{\Carbon\Carbon::parse($val->created_at)->format('d/m/Y g:i a')}}</p>
                                                            </li>
                                                            @switch($val->flag)

                                                                @case('poor')
                                                                <li>1.0</li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                @break


                                                                @case('moderate')
                                                                <li>2.0</li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                @break

                                                                @case('average')
                                                                <li>3.0</li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                @break

                                                                @case('good')
                                                                <li>4.0</li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                @break

                                                                @case('perfect')
                                                                <li>5.0</li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                @break

                                                                @default
                                                                <li>0.0</li>

                                                            @endswitch
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <div class="col-lg-12 mt-3">
                                        <div class="customars_review last-review-boxes">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 text-center">
                                                    <h4>No Review Found!</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforelse

                                {{--                                <div class="col-lg-12">--}}
                                {{--                                    <div class="more-review text-center">--}}
                                {{--                                        <a href="#" class="seeMORE">SEE MORE</a>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}


                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                             aria-labelledby="pills-contact-tab">
                            <div class="row SHIPPING_RETURNS">
                                <div class="col-lg-12">
                                    <p><b>*NOTE:</b> This product is made to order and requires 3 to 5 business days
                                        production time. This is to ensure all products shipped out are high quality and
                                        have undergone strict quality assurance procedure.</p>
                                    <h5> Where Can it Ship? </h5>
                                    <h6>1. Normal Airmail Delivery Schedule:</h6>
                                    <p class="cost-text">Cost: <b>FREE</b> for orders above $100.00 USD*, please add
                                        items to your cart and use the shipping estimator on the cart page</p>
                                    <ul>
                                        <li><b>United States:</b> <span>7 to 14 business days.</span></li>
                                        <li><b>Canada:</b> <span>7 to 14 business days.</span></li>
                                        <li><b>Europe: </b> <span>7 to 14 business days.</span></li>
                                        <li><b>Hong Kong:</b> <span>2 to 4 business days </span></li>
                                        <li><b>Asian & Australia: </b> <span>4 to 17 business days </span></li>
                                        <li><b>Mexico & South America:</b> <span>12 - 25 business days </span></li>
                                        <li><b>Middle East: </b> <span> 10 - 25 business days </span></li>
                                    </ul>
                                    <h5 class="express">2. Express Mail Service (EMS) / UPS / DHL / Fedex Delivery
                                        Schedule:</h5>
                                    <div class="alp">
                                        <p class="cost-two">Cost: $37.00 USD and up, please add items to your cart and
                                            use the shipping estimator on the cart page</p>
                                        <p>United States: 2 - 5 business days (up to 10 business days for Hawaii)</p>
                                        <p>Canada: 2 - 5 business days</p>
                                        <p>Europe: 1 - 6 business days</p>
                                        <p>Hong Kong (Local Courier): 1 - 2 business days</p>
                                        <p>Asia: 1 - 4 business days</p>
                                        <p>Australia: 2 - 6 business days</p>
                                        <p>Mexico & South American: 4 - 7 business days (*EMS shipping is highly
                                            recommended for these countries)</p>
                                        <p>Middle East: 6 - 7 business days (*EMS shipping is highly recommended for
                                            these countries)</p>
                                    </div>

                                    <h5 class="about">About Shipping</h5>
                                    <p class="ship">We ship via fast and trackable shipping services.</p>
                                    <h5 class="Expected">Expected Shipment Date</h5>
                                    <p class="products">This product is expected to ship within 2 business days.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product-detailse END -->

    <!-- product-area START Carousel Slider-->
    <div class="product-area single-product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h1>YOU MAY ALSO BE INTERESTED IN</h1>
                    </div>
                </div>
            </div>
            <div class="products">
                <div class="row product-slider owl-carousel related_products">

                    @forelse($related_products as $key => $val)

                        @php
                            $product_meta       = \App\products_meta::where(['product_id' => $val->id])->get();

                            $current_time       = time();
                            $product_created_at = strtotime($val->created_at);
                            $diff_days          = ( ($current_time - $product_created_at) / 86400)  ;
                            $img_related_prod   = getProductFeatureImage($val->image);
                        @endphp

                        <div class="product-slide-item">
                            <div class="product-box">

                                @if($diff_days <= 5)
                                    <div class="ribbon-wrapper ribbon-lg">
                                        <div class="ribbon bg-gradient-danger">
                                            New Product
                                        </div>
                                    </div>
                                @endif

                                @forelse($product_meta as $key => $meta)
                                    @if($meta['product_meta'] == "sale_price")
                                        @if(!empty($meta['product_meta_value']))
                                            <div class="ribbon-wrapper ribbon-lg">
                                                <div class="ribbon bg-gradient-warning">
                                                    Sale Offer
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                @empty
                                @endforelse



                                <img src="{{ asset($img_related_prod)}}" alt="">

                                <div class="product-btns">
                                    <a href="#" class="THEME-BTN">Buy Now </a>

                                    @if(isset($wishlist_products['product']) && count($wishlist_products['product']) > 0)

                                        @if(in_array($val->id , $wishlist_products['product']))
                                            <a href="" product_id="{{$val->id}}"
                                               class="THEME-BTN transparent-btn remove-wishlist-btn-rp">
                                                <i class="fas fa-heart mr-1" style="color:#DD574E"></i>Remove</a>
                                        @else
                                            <a href="" product_id="{{$val->id}}"
                                               class="THEME-BTN transparent-btn add-wishlist-btn-rp"><i
                                                    class="fal fa-heart mr-1"></i>
                                                Add to Wishlist</a>
                                        @endif

                                    @else

                                        @if(Auth::check())
                                            <a href="" product_id="{{$val->id}}"
                                               class="THEME-BTN transparent-btn add-wishlist-btn-rp"><i
                                                    class="fal fa-heart mr-1"></i>
                                                Add to Wishlist</a>
                                        @endif

                                    @endif

                                    {{--                                    <a href="#" class="THEME-BTN transparent-btn"><img--}}
                                    {{--                                            src="{{ asset('front-end/assets/img/heart.svg')}}" alt=""> Add to Wishlist</a>--}}
                                </div>
                                <div class="productBox-content">
                                    <h5>{{substr(ucfirst($val->title),0,20)."..."}}<span>{{"$".number_format($val->price,2)}}</span></h5>
                                    <p> {{substr(strip_tags($val->description),0,70)."..."}}</p>
                                </div>
                            </div>
                        </div>

                    @empty



                    @endforelse


                    <div class="product-slide-item">
                        <div class="product-box">
                            <img src="{{ asset('front-end/assets/img/product-img5.png')}}" alt="">
                            <div class="product-btns">
                                <a href="#" class="THEME-BTN">Buy Now </a>
                                <a href="#" class="THEME-BTN transparent-btn"><img
                                        src="{{ asset('front-end/assets/img/heart.svg')}}" alt=""> Add to Wishlist</a>
                            </div>
                            <div class="productBox-content">
                                <h5>Furry Backpack <span>$32.00</span></h5>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product-area END -->

@endsection


@section('custom-front-script')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================

        $(document).on('click', '.add-wishlist-btn',function (e) {
            e.preventDefault();
            var _self = $(this);
            var prod_id = $(this).attr("product_id");
            var route = "{{route('wishlist.create',['id'=>'id'])}}";
            route = route.replace('id', prod_id);
            var status = "add";
            var remove_btn = "<a href='' product_id='" + prod_id + "' class='THEME-BTN transparent-btn transparent-btn-single-product remove-wishlist-btn'><i class='fas fa-heart mr-1' style='color:#DD574E'></i>Remove</a>";

            $.ajax({
                url: route,
                type: 'post',
                data: {status: status},
                success: function (response) {
                    _self.hide();
                    _self.parent().append(remove_btn);
                }
            });

        });

        //=======================================================================================================


        $(document).on('click','.remove-wishlist-btn',function (e) {
            e.preventDefault();
            var _self = $(this);
            var prod_id = $(this).attr("product_id");
            var route = "{{route('wishlist.delete',['id'=> 'id'])}}";
            route = route.replace('id', prod_id);
            var status = "remove";

            var add_wishlist_btn = "<a href='' product_id='" + prod_id + "' class='THEME-BTN transparent-btn transparent-btn-single-product add-wishlist-btn'><i class='fal fa-heart mr-1'></i>Add to Wishlist</a>";


            $.ajax({
                url: route,
                type: 'post',
                data: {status: status},
                success: function (response) {
                    _self.hide();
                    _self.parent().append(add_wishlist_btn);
                }
            });

        });

        //=======================================================================================================

        //add to wishlist button for related products

        $(document).on('click', '.add-wishlist-btn-rp', function (e) {
            e.preventDefault();
            var _self = $(this);
            var prod_id = $(this).attr("product_id");
            var route = "{{route('wishlist.create',['id'=>'id'])}}";
            route = route.replace('id', prod_id);
            var status = "add";
            var remove_btn = "<a href='' product_id='" + prod_id + "' class='THEME-BTN transparent-btn remove-wishlist-btn-rp'><i class='fas fa-heart mr-1' style='color:#DD574E'></i>Remove</a>";

            $.ajax({
                url: route,
                type: 'post',
                data: {status: status},
                success: function (response) {
                    _self.hide();
                    _self.parent().append(remove_btn);
                }
            });
        });


        $(document).on('click', 'a.remove-wishlist-btn-rp', function (e) {
            e.preventDefault();
            var _self = $(this);
            var prod_id = $(this).attr("product_id");
            var route = "{{route('wishlist.delete',['id'=> 'id'])}}";
            route = route.replace('id', prod_id);
            var status = "remove";
            var add_wishlist_btn = "<a href='' product_id='" + prod_id + "' class='THEME-BTN transparent-btn add-wishlist-btn-rp'><i class='fal fa-heart mr-1'></i>Add to Wishlist</a>";

            $.ajax({
                url: route,
                type: 'post',
                data: {status: status},
                success: function (response) {
                    _self.hide();
                    _self.parent().append(add_wishlist_btn);
                }
            });

        });


        //=======================================================================================================

        //selecting product variation
        var vari_id = "";
        var original_price = $(".original_price").text();
        var original_description = $(".original_description").text();


        $("#product_avail_variation").change(function(){

            vari_id = $(this).children("option:selected").val();
            var base_url = '{!! url('/') !!}';
            var attribute_value = "";
            var variation_value = "";
            var variation_img   = "";
            var price           = "";
            var description     = "";

            $(".product-variation-section > ul.prod_vari_ul").empty();
            $(".product-variation-section > .vari-img-section").empty();

            $(".variation-class").each(function(){

                if(vari_id === $(this).attr("variation_id") ){

                    attribute_value     = $(this).attr("attribute");
                    variation_value     = $(this).attr("variation");
                    price               = parseFloat($(this).attr("variation_price"));
                    description         = $(this).attr("variation_description");

                    if($(this).attr("variation_image") !== ""){
                        variation_img       = base_url+"/storage/"+$(this).attr("variation_image");
                    }

                    $(".product-variation-section > ul.prod_vari_ul").append(
                        " <li class='prod_vari_li'>\n" +
                        "<label for='variation_item'>"+attribute_value.toUpperCase()+" : "+ variation_value.toUpperCase() +"</label>\n" +
                        "</li>\n");
                }

            });


            if(vari_id !== ""){

                if(description !== ""){
                    $(".product-pay > p").html(description);
                }else{
                    $(".product-pay > p").html(original_description);
                }

                $(".price > span > span").html("$"+$.number(price , 2));
            }else{
                $(".product-pay > p").html(original_description);
                $(".price > span > span").html(original_price);
            }

            if(variation_img !== ""){
                $(".product-variation-section > .vari-img-section").append('<a href="'+variation_img+'" data-lightbox="image-033">' +
                    '<img class="variation-img" src="'+variation_img+'" alt="">'+
                    '</a>');

            }


        });

        function addCommas(nStr){
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        //=======================================================================================================

        // $('.plus-increment').css('position','absolute','margin-top','-27px !Important','margin-left','20px!Important')
        // $('.minus-decrement').css('position','absolute','margin-top','-27px !Important','margin-left','200px!Important')

        var quantity = $('.quantity').val();


        $('body').on('click', '.plus-increment', function () {

            var $qty = $(this).parent().find('.quantity');
            var currentVal = parseInt($qty.val());

            if (!isNaN(currentVal)) {
                $qty.val(currentVal + 1);
                quantity = $qty.val(currentVal + 1);
                quantity = quantity[0].value;
            }

            var prodStock = "{{$product->stock}}";

            if(parseFloat(quantity) >  parseFloat(prodStock)){
                $(".stock-msg").text("Out Of Stock!");
                $("#BuyNow-non-auth").css("display","none");
                $(".BuyNow-auth").css("display","none");
            }

        });

        $('body').on('click', '.minus-decrement', function () {

            var $qty = $(this).parent().find('.quantity');
            var currentVal = parseInt($qty.val());

            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
                // console.log($qty.val(currentVal - 1))
                quantity = $qty.val(currentVal - 1);
                quantity = quantity[0].value;
            }

            var prodStock = "{{$product->stock}}";

            if(parseFloat(quantity) <=  parseFloat(prodStock)){
                $(".stock-msg").text("");
                $("#BuyNow-non-auth").css("display","inline");
                $(".BuyNow-auth").css("display","inline");
            }

        });

        //=======================================================================================================

        $(".buy-now").submit(function (e) {
            e.preventDefault();

            var size        = $(".size").val();
            var material    = $(".material").val();
            var extra       = $(".extra").val();
            var weight      = $("#product_weight").val();
            var product_id  = $(".product_id").val();
            var route       = '{{ route('order.buynow')}}';
            var user_id     = $(".user_id").val();
            //var extra       = $(".extra").val();

            $.ajax({
                type: 'POST',
                url: route,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    size: size,
                    material: material,
                    quantity: quantity,
                    product_id: product_id,
                    user_id: user_id,
                    extra: extra,
                    weight: weight,
                    vari_id:vari_id,
                    order_type: "front",
                },
                success: function (data) {
                    localStorage.setItem('count', data.count);
                    $('.custom-badge-cart-primary').html(data.count);
                    console.log(data.count);
                    toastr.success('Product added in cart successfully ', 'Temporary Item');
                },
                error: function (data) {

                    //If non-auth user want to purchasing apply fast checkout providing guest user ID.
                    //var login_route = "{{route('login')}}";
                    //window.location.assign(login_route);
                    //toastr.warning('Product failed to add in cart'+data.message, 'Temporary Item');
                }

            });


        });

        //=======================================================================================================

        function setWithExpiry(key, value,ttl) {
            const now = new Date();
            // 3600000 == 1 hr
            const item = {
                ID: value,
                expiry: now.getTime() + ttl
            };
            localStorage.setItem(key, JSON.stringify(item))
        }

        // function getWithExpiry(key) {
        //     const itemStr = localStorage.getItem(key);
        //     // if the item doesn't exist, return null;
        //     if (!itemStr) {
        //         return null
        //     }
        //     const item = JSON.parse(itemStr);
        //     const now = new Date();
        //     // compare the expiry time of the item with the current time
        //     if (now.getTime() > item.expiry) {
        //         // If the item is expired, delete the item from storage
        //         // and return null
        //         localStorage.removeItem(key);
        //         return null
        //     }
        //     return item.value
        // }

        function Generator() {};

        Generator.prototype.rand =  Math.floor(Math.random() * 26) + Date.now();

        Generator.prototype.getId = function() {
            return this.rand++;
        };

        var idGen = new Generator();


        //open modal if key not exist
        $("#BuyNow-non-auth").on('click',function(e){
            e.stopPropagation();
            var gusestDetail = JSON.parse(localStorage.getItem("guestDetail"));

            if (localStorage.hasOwnProperty("guestDetail")) {

                var size = $(".size").val();
                var material = $(".material").val();
                var extra = $(".extra").val();
                var weight = $("#product_weight").val();
                var product_id = $(".product_id").val();
                var route = '{{ route('order.buynow')}}';
                var user_id = gusestDetail.ID;
                //var extra       = $(".extra").val();
                // console.log(size+" - "+material+" - "+extra+" - "+weight+" - "+product_id+" - "+route+" - "+user_id);

                $.ajax({
                    type: 'POST',
                    url: route,
                    data: {
                        size: size,
                        material: material,
                        quantity: quantity,
                        product_id: product_id,
                        user_id: user_id,
                        extra: extra,
                        weight: weight,
                        vari_id: vari_id,
                        order_type: "front",
                    },
                    success: function (data) {
                        localStorage.setItem('count', data.count);
                        $('.custom-badge-cart-primary').html(data.count);
                        console.log(data.count);
                        toastr.success('Product added in cart successfully ', 'Temporary Item');
                    },
                    error: function (data) {
                        //If non-auth user want to purchasing apply fast checkout providing guest user ID.
                        //var login_route = "{{route('login')}}";
                        //window.location.assign(login_route);
                        //toastr.warning('Product failed to add in cart'+data.message, 'Temporary Item');
                    }

                });

            }else{
                $("#non-auth-guest-modal").modal('show');
            }
        });

        $("#guestBtn-buynow").click(function(e){
            e.preventDefault();
            //3600000
            var guestID = idGen.rand;
            setWithExpiry('guestDetail',guestID,3600000);

            var size = $(".size").val();
            var material = $(".material").val();
            var extra = $(".extra").val();
            var weight = $("#product_weight").val();
            var product_id = $(".product_id").val();
            var route = '{{ route('order.buynow')}}';
            var userID = guestID;
            //var extra       = $(".extra").val();
            // console.log(size+" - "+material+" - "+extra+" - "+weight+" - "+product_id+" - "+route+" - "+user_id);

            $.ajax({
                type: 'POST',
                url: route,
                data: {
                    size: size,
                    material: material,
                    quantity: quantity,
                    product_id: product_id,
                    user_id: userID,
                    extra: extra,
                    weight: weight,
                    vari_id: vari_id,
                    order_type: "front",
                },
                success: function (data) {
                    localStorage.setItem('count', data.count);
                    $('.custom-badge-cart-primary').html(data.count);
                    console.log(data.count);
                    toastr.success('Product added in cart successfully ', 'Temporary Item');

                },
                error: function (data) {
                    //If non-auth user want to purchasing apply fast checkout providing guest user ID.
                    //var login_route = "{{route('login')}}";
                    //window.location.assign(login_route);
                    //toastr.warning('Product failed to add in cart'+data.message, 'Temporary Item');
                }
            });

            var route2 = '{{ route('guest.store-id')}}';

            $.ajax({
                type: 'POST',
                url: route2,
                data: {
                    guest_id: userID,
                },
                success: function (res) {
                    console.log(res.message);
                }
            });

            $("#non-auth-guest-modal").modal('hide');


        });

    </script>
@endsection
