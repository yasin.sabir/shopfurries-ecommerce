<!-- Front End View -->
@extends('front-layout.app')

@section('title')
    Your Wishlist
@endsection


@section('custom-front-css')

@endsection


@section('Main')

    <!-- product-title GROUP -->
    <div class="product-titleGroup">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="title-text wishlist-title-text">
                        <h1>{{$event->name}}</h1>
                        <span class="event_sub_title">Location: {{$event->city}} {{$event->country}}</span>
                        <span class="event_sub_timing">
                            Timing: {{$event->timing}}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product-title GROUP -->


    <!-- Art Work Start -->
    <div class="gallery-area">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <div class="map_seciton">

                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="event_details">
                        <p> {{strip_tags($event->details)}} </p>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!-- Art Work End -->

@endsection


@section('custom-front-script')


@endsection
