@extends('front-layout.app')

@section('title')
    Gallery
@endsection

@section('Main')

    @php

        $wishlist_products = [];

        if( Auth::check()){
            $current_user_id = Auth::user()->id;
            $wishlist = \App\Wishlist::where(['user_id' => $current_user_id])->where(['type' => 'art_work'])->get();

            $wishlist_products['user_id'] = $current_user_id;

            foreach ($wishlist as $key => $v){
                $wishlist_products['product'][] = $v->product_id;
            }

        }
    @endphp


    <!-- product-title GROUP -->
    <div class="product-titleGroup gallery-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="title-textLEFT gallery-page">
                        <p>Home <i class="far fa-angle-right"></i> <span style="color: #000;font-weight: 700;">Gallery</span></p>
                        <form action="{{route('gallery.search-result')}}" id="gallery-search-form" method="post">
                            @csrf
                            <input type="text" name="find" placeholder="Search here...">
                            <span class="custom-gallery-search">
                                <i class="far fa-search"></i>
                            </span>
                        </form>
                    </div>
                </div>
                <div class="col-lg-5 text-center">
                    <div class="title-text">
                        <h1>Gallery</h1>
                    </div>
                </div>
                <div class="col-lg-4">
{{--                    <div class="select-ber text-right">--}}
{{--                        <h5><span class="shortBy">sort by </span>--}}
{{--                            <select name="select-product" id="">--}}
{{--                                <option value="product">Featured Products</option>--}}
{{--                                <option value="product">Featured Products</option>--}}
{{--                                <option value="product">Featured Products</option>--}}
{{--                                <option value="product">Featured Products</option>--}}
{{--                                <option value="product">Featured Products</option>--}}
{{--                                <option value="product">Featured Products</option>--}}
{{--                            </select>--}}
{{--                        </h5>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    <!-- product-title GROUP -->

    <!-- gallery-area START -->
    <div class="gallery-area">
        <div class="container">
            <div class="row">

{{--                fetch search data--}}
                @forelse($search_result as $key => $gallery)
                    <div class="col-lg-4 col-md-6">
                        <div class="gallery">
                            <img src="{{asset('storage/'.$gallery->image)}}" alt="">
                            <div class="gallery-wrep">
                                <h5>Commission: {{ucwords($gallery->title)}}</h5>
                                <h4>
                                    @php
                                        $user = \App\User::find($gallery->artist_id);
                                    @endphp

                                    @if(!empty($user->profile_pic))
                                        <img src="{{asset('storage/'.$user->profile_pic)}}" alt="">
                                    @else
                                        <img src="{{asset('images/placeholders/Profile_avatar_placeholder_lg.png')}}" alt="">
                                    @endif

                                    Art by

                                    {{ucfirst($user->name)}}

                                    <span>Twitter. Facebook. FurAffinity</span>
                                    <span class="gDATE">Tags:
                                        @php
                                            $tags = [];
                                            $tags_data = explode(',',$gallery->tags);
                                            foreach ($tags_data as $key => $val){
                                                $tags [] = ucwords($val);
                                            }
                                            $tag = implode(' , ' , $tags);
                                        @endphp
                                        {{$tag}}
                                    </span></h4>
                            </div>


                            @if(isset($wishlist_products['product']) && count($wishlist_products['product']) > 0)

                                @if(in_array($gallery->id , $wishlist_products['product']))
                                    <a href="#" product_id="{{$gallery->id}}" class="heart remove-wishlist-btn" style="color: #DD574E;background: #016fb9;border-color: #016fb9;"><i class="fas fa-heart"></i></a>
                                @else
                                    <a href="#" product_id="{{$gallery->id}}" class="heart add-wishlist-btn"><i class="far fa-heart"></i></a>
                                @endif

                            @else
                                @if(Auth::check())
                                    <a href="#" product_id="{{$gallery->id}}" class="heart add-wishlist-btn"><i class="far fa-heart"></i></a>
                                @endif
                            @endif

                        </div>
                    </div>
                @empty

{{--                    fetch all gallery data--}}
                    @forelse($galleries as $key => $gallery)
                        <div class="col-lg-4 col-md-6">
                            <div class="gallery">
                                <img src="{{asset('storage/'.$gallery->image)}}" alt="">
                                <div class="gallery-wrep">
                                    <h5>Commission: {{ucwords($gallery->title)}}</h5>
                                    <h4>
                                        @php
                                            $user = \App\User::find($gallery->artist_id);
                                        @endphp

                                        @if(!empty($user->profile_pic))
                                            <img src="{{asset('storage/'.$user->profile_pic)}}" alt="">
                                        @else
                                            <img src="{{asset('images/placeholders/Profile_avatar_placeholder_lg.png')}}" alt="">
                                        @endif

                                        Art by

                                        {{ucfirst($user->name)}}

                                        <span>Twitter. Facebook. FurAffinity</span>
                                        <span class="gDATE">Tags:
                                        @php
                                            $tags = [];
                                            $tags_data = explode(',',$gallery->tags);
                                            foreach ($tags_data as $key => $val){
                                                $tags [] = ucwords($val);
                                            }
                                            $tag = implode(' , ' , $tags);
                                        @endphp
                                            {{$tag}}
                                    </span></h4>
                                </div>


                                @if(isset($wishlist_products['product']) && count($wishlist_products['product']) > 0)

                                    @if(in_array($gallery->id , $wishlist_products['product']))
                                        <a href="#" product_id="{{$gallery->id}}" class="heart remove-wishlist-btn" style="color: #DD574E;background: #016fb9;border-color: #016fb9;"><i class="fas fa-heart"></i></a>
                                    @else
                                        <a href="#" product_id="{{$gallery->id}}" class="heart add-wishlist-btn"><i class="far fa-heart"></i></a>
                                    @endif

                                @else
                                    @if(Auth::check())
                                        <a href="#" product_id="{{$gallery->id}}" class="heart add-wishlist-btn"><i class="far fa-heart"></i></a>
                                    @endif
                                @endif

                            </div>
                        </div>
                    @empty
                        <div class="col-lg-12 text-center mt-4 mb-4">
                            <h3>No Art Work Found!</h3>
                        </div>
                    @endforelse

                @endforelse

            </div>
        </div>
    </div>
    <!-- gallery-area END -->

@endsection

@section('custom-front-script')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================


        $(document).on('click', 'a.add-wishlist-btn', function (e) {
            e.preventDefault();
            var _self = $(this);
            var type = "art_work";
            var prod_id = $(this).attr("product_id");
            var route = "{{route('wishlist.create',['id'=>'id'])}}";
            route = route.replace('id', prod_id);
            var status = "add";
            var remove_btn = "<a href='#' product_id='" + prod_id + "' class='heart remove-wishlist-btn' style='color: #DD574E;background: #016fb9;border-color: #016fb9;'><i class='fas fa-heart'></i></a>";

            $.ajax({
                url: route,
                type: 'post',
                data: {status: status , type:type},
                success: function (response) {
                    _self.hide();
                    _self.parent().append(remove_btn);
                }
            });
        });


        $(document).on('click', 'a.remove-wishlist-btn', function (e) {
            e.preventDefault();
            var _self = $(this);
            var type = "art_work";
            var prod_id = $(this).attr("product_id");
            var route = "{{route('wishlist.delete',['id'=> 'id'])}}";
            route = route.replace('id', prod_id);
            var status = "remove";
            var add_wishlist_btn = "<a href='' product_id='" + prod_id + "' class='heart add-wishlist-btn'><i class='far fa-heart'></i></a>";

            $.ajax({
                url: route,
                type: 'post',
                data: {status: status, type:type},
                success: function (response) {
                    _self.hide();
                    _self.parent().append(add_wishlist_btn);
                }
            });

        });

        //=======================================================================================================

        $(".custom-gallery-search").on('click' , function(){
            $("#gallery-search-form").submit();
        });

        //=======================================================================================================
    </script>
@endsection

