    @extends('front-layout.app')

    @section('title')
        Shop
    @endsection


    @section('custom-front-css')

    @endsection


    @section('Main')

        <!-- product-title GROUP -->
        <div class="product-titleGroup">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="title-textLEFT">
                            <p>Home <i class="far fa-angle-right"></i> <span
                                    style="color: #000;font-weight: 700;">Shop</span></p>
                            <a href="#section-views" class="grid_view_btn"><i class="fas fa-th"
                                                                              style="color: #7070709e;"></i></a>
                            <a href="#section-views" class="list_view_btn"><i class="far fa-list-ul"
                                                                              style="color: #7070709e;"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-5 text-center">
                        <div class="title-text">
                            <h1>Featured Products</h1>
                        </div>
                    </div>

                    <div class="col-lg-4">

                        <form id="product_featured_form" method="post" action="{{route('filter_data')}}">
                            @csrf
                            {{--<input type="hidden" name="product_featured_value" id="product_featured_value">--}}
                            <div class="select-ber text-right">
                                <h5><span class="shortBy">sort by </span>
                                    <select name="select_product_feature" id="select_product_feature">
                                        <option value="sort">Sort Products</option>
                                        <option value="l-h">Price: Low to High</option>
                                        <option value="h-l">Price: High to Low</option>
                                        <option value="a-z">Products A-Z</option>
                                        <option value="z-a">Products Z-A</option>
                                        <option value="new">Newest Products</option>
                                    </select>
                                </h5>
                            </div>
                        </form>

                    </div>


                </div>
            </div>
        </div>
        <!-- product-title GROUP -->

        <!-- item-area START -->
        <div class="item-area shop">
            <div class="container">

                <!-- Category Section -->
                @include('front-views.Section.CategorySection')

            </div>
        </div>
        <!-- item-area END -->



        <!-- Area Focus Section For List & Grid Views -->
        <div id="section-views"></div>
        @php

            $wishlist_products = [];

            if( Auth::check()){
                $current_user_id = Auth::user()->id;
                $wishlist = \App\Wishlist::where(['user_id' => $current_user_id])->get();

                $wishlist_products['user_id'] = $current_user_id;

                foreach ($wishlist as $key => $v){
                    $wishlist_products['product'][] = $v->product_id;
                }

            }
        @endphp

        <!-- product-area START List View -->
        <div class="product-area shop List-View" id="List-View">
            <div class="container">
                <div class="products">

                    <!-- List View -->
                    <div class="row">
                        @forelse($products as $key => $val)

                            @php
                                $product_meta        = \App\products_meta::where(['product_id' => $val->id])->get();

                                $current_time       = time();
                                $product_created_at = strtotime($val->created_at);
                                $diff_days          = ( ($current_time - $product_created_at) / 86400)  ;
                            @endphp

                            <div class="col-lg-12 px-0">

                                @if($diff_days <= 5)
                                    <div class="ribbon-wrapper ribbon-lg">
                                        <div class="ribbon bg-gradient-danger">
                                            New Product
                                        </div>
                                    </div>
                                @endif

                                @forelse($product_meta as $key => $meta)
                                    @if($meta['product_meta'] == "sale_price")
                                        @if(!empty($meta['product_meta_value']))
                                            <div class="ribbon-wrapper ribbon-lg">
                                                <div class="ribbon bg-gradient-warning">
                                                    Sale Offer
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                @empty
                                @endforelse

                                <div class="grid-desing-all">
                                    <div class="single-grid-design">
                                        <div class="row">
                                            <div class="col-lg-5 col-md-4">

                                                @php
                                                    $image = "";

                                                    $ff = @unserialize($val->image);

                                                    if(!is_array($ff)){
                                                        $image = $ff;
                                                    }else{
                                                        $image = $val->image;
                                                    }

                                                    $default_size = \App\SiteSetting::where(['key' => 'product_image_size'])->first();
                                                @endphp


                                                @if(!is_array($ff))

                                                    @if(!empty($val->image))
                                                        <img ff="af" src="{{ asset('storage/'.$val->image)}}"
                                                             alt="{{ucfirst($val->title)}}">
                                                    @else
                                                        <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"/>
                                                    @endif

                                                @else
                                                    <img src="{{ asset('storage/'.$ff[$default_size->value])}}">
                                                @endif


    {{--                                            @if($val != 'null')--}}
    {{--                                                <img src="{{ asset('storage/'.$val->image)}}"--}}
    {{--                                                     alt="{{ucfirst($val->title)}}">--}}
    {{--                                            @else--}}
    {{--                                                <img src="{{asset('images/default-placeholder-600x600.png')}}"/>--}}
    {{--                                            @endif--}}

                                            </div>
                                            <div class="col-lg-7 col-md-8">
                                                <div class="grid-content">
                                                    <h5>{{ substr(ucfirst($val->title),0,10)  }}

                                                        @forelse($product_meta as $key => $meta)
                                                            @if($meta['product_meta'] == "sale_price")

                                                                @if(!empty($meta['product_meta_value']))
                                                                    <div class="custom-shopProduct-price d-inline">
                                                                        <span
                                                                            style="text-decoration: line-through;font-size: 15px;top:-20px">
                                                                            ${{number_format($val->price,2)}}
                                                                        </span>
                                                                        <span>
                                                                            ${{number_format($meta['product_meta_value'],2)}}
                                                                        </span>
                                                                    </div>
                                                                @else
                                                                    <span>
                                                            ${{number_format($val->price,2)}}
                                                        </span>
                                                                @endif
                                                            @endif
                                                        @empty
                                                        @endforelse

                                                    </h5>
                                                    <p>
                                                        {{substr(strip_tags($val->description),0,200)."..."}}
                                                    </p>
                                                    <div class="product-btns">

                                                        <a href="{{route('single_product',['name'=>$val->title,'id' => $val->id])}}"
                                                           product="{{ucfirst($val->title)}}" class="THEME-BTN">Buy Now</a>

                                                        {{--                                                    <a href="#" product_id="{{$val->id}}"--}}
                                                        {{--                                                       class="THEME-BTN transparent-btn add-wishlist-btn"><i--}}
                                                        {{--                                                            class="fal fa-heart"></i> Add to Wishlist</a>--}}

                                                        @if(isset($wishlist_products['product']) && count($wishlist_products['product']) > 0)

                                                            @if(in_array($val->id , $wishlist_products['product']))
                                                                <a href="" product_id="{{$val->id}}"
                                                                   class="THEME-BTN transparent-btn remove-wishlist-btn">
                                                                    <i class="fas fa-heart" style="color:#DD574E"></i>Remove</a>
                                                            @else
                                                                <a href="" product_id="{{$val->id}}"
                                                                   class="THEME-BTN transparent-btn add-wishlist-btn"><i
                                                                        class="fal fa-heart"></i>
                                                                    Add to Favorite</a>
                                                            @endif

                                                        @else

                                                            @if(Auth::check())
                                                                <a href="" product_id="{{$val->id}}"
                                                                   class="THEME-BTN transparent-btn add-wishlist-btn"><i
                                                                        class="fal fa-heart"></i>
                                                                    Add to Favorite</a>
                                                            @endif

                                                        @endif


                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @empty

                        @endforelse
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            {{$products->links() }}
                        </div>
                    </div>

                    <div class="container">
                        <div class="row mt-5">
                            <div class="col-lg-12">
                                <div class="text-center mt-4">
                                    <a href="{{route('bundle_shop')}}" product="Testing Product Dumy" class="THEME-BTN">Bundle Product</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- product-area END -->

        <!-- product-area START Grid View-->
        <div class="product-area shop  Grid-View" id="Grid-View">
            <div class="container">
                <div class="products">

                    <!-- Grid View -->
                    <div class="row">

                        @forelse($products as $key => $val)

                            @php
                                $product_meta = \App\products_meta::where(['product_id' => $val->id])->get();
                            @endphp

                            <div class="col-lg-3 col-md-6">
                                <div class="product-box">

                                    @php
                                        $current_time       = time();
                                        $product_created_at = strtotime($val->created_at);

                                        $diff_days = ( ($current_time - $product_created_at) / 86400)  ;
                                    @endphp

                                    @if($diff_days <= 5)
                                        <div class="ribbon-wrapper ribbon-lg">
                                            <div class="ribbon bg-gradient-danger">
                                                New Product
                                            </div>
                                        </div>
                                    @endif

                                    @forelse($product_meta as $key => $meta)
                                        @if($meta['product_meta'] == "sale_price")
                                            @if(!empty($meta['product_meta_value']))
                                                <div class="ribbon-wrapper ribbon-lg">
                                                    <div class="ribbon bg-gradient-warning">
                                                        Sale Offer
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    @empty
                                    @endforelse



                                    @php

                                        $image = "";

                                        $ff = @unserialize($val->image);

                                        if(!is_array($ff)){
                                            $image = $ff;
                                        }else{
                                            $image = $val->image;
                                        }

                                        $default_size = \App\SiteSetting::where(['key' => 'product_image_size'])->first();
                                    @endphp


                                    @if(!is_array($ff))

                                        @if(!empty($val->image))
                                            <img ff="af" src="{{ asset('storage/'.$val->image)}}"
                                                 alt="{{ucfirst($val->title)}}">
                                        @else
                                            <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"/>
                                        @endif

                                    @else
                                        <img src="{{ asset('storage/'.$ff[$default_size->value])}}">
                                    @endif


    {{--                                @if($val != 'null')--}}
    {{--                                    <img src="{{ asset('storage/'.$val->image)}}" alt="{{ucfirst($val->title)}}">--}}
    {{--                                @else--}}
    {{--                                    <img src="{{asset('images/default-placeholder-600x600.png')}}"/>--}}
    {{--                                @endif--}}

                                    <div class="product-btns">

    {{--                                    <a href="{{route('single_product',['name'=> str_replace(' ','-',$val->title) ,'id' => $val->id])}}"--}}
    {{--                                       product="{{ucfirst($val->title)}}" class="THEME-BTN">Buy Now</a>--}}
                                        <a href="{{route('single_product',['name'=> str_replace(' ','-',$val->title)])}}"
                                           product="{{ucfirst($val->title)}}" class="THEME-BTN">Buy Now</a>

                                        @if(isset($wishlist_products['product']) && count($wishlist_products['product']) > 0)

                                            @if(in_array($val->id , $wishlist_products['product']))
                                                <a href="" product_id="{{$val->id}}"
                                                   class="THEME-BTN transparent-btn remove-wishlist-btn">
                                                    <i class="fas fa-heart" style="color:#DD574E"></i>Remove</a>
                                            @else
                                                <a href="" product_id="{{$val->id}}"
                                                   class="THEME-BTN transparent-btn add-wishlist-btn"><i
                                                        class="fal fa-heart"></i>
                                                    Add to Favorite</a>
                                            @endif

                                        @else

                                            @if(Auth::check())
                                                <a href="" product_id="{{$val->id}}"
                                                   class="THEME-BTN transparent-btn add-wishlist-btn"><i
                                                        class="fal fa-heart"></i>
                                                    Add to Favorite</a>
                                            @endif

                                        @endif

                                    </div>

                                    <div class="productBox-content">
                                        <h5> {{substr(ucfirst($val->title),0,20)."..."}}


                                            @forelse($product_meta as $key => $meta)
                                                @if($meta['product_meta'] == "sale_price")

                                                    @if(!empty($meta['product_meta_value']))

                                                        <div class="custom-shopProduct-price">
                                                            <span
                                                                style="text-decoration: line-through;font-size: 12px;top:-15px">
                                                                ${{number_format($val->price,2)}}
                                                            </span>
                                                                <span>
                                                                ${{number_format($meta['product_meta_value'],2)}}
                                                            </span>
                                                        </div>
                                                    @else
                                                        <span>
                                                            ${{number_format($val->price,2)}}
                                                        </span>
                                                    @endif
                                                @endif
                                            @empty
                                            @endforelse

                                        </h5>
                                        <p>
                                            {{substr(strip_tags($val->description),0,70)."..."}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @empty
                        @endforelse

                    </div>


                    <div class="row">
                        <div class="col-lg-12 text-center">
                            {{$products->links() }}
                        </div>
                    </div>

                    <div class="container">
                        <div class="row mt-5">
                           <div class="col-lg-12">
                               <div class="text-center mt-4">
                                   <a href="{{route('bundle_shop')}}" product="Testing Product Dumy" class="THEME-BTN">Bundle Product</a>
                               </div>
                           </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- product-area END -->

    @endsection

    @section('custom-front-script')
        <script>


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //=======================================================================================================


            $(document).on('click', 'a.add-wishlist-btn', function (e) {
                e.preventDefault();
                var _self = $(this);
                var type = "product";
                var prod_id = $(this).attr("product_id");
                var route = "{{route('wishlist.create',['id'=>'id'])}}";
                route = route.replace('id', prod_id);
                var status = "add";
                var remove_btn = "<a href='' product_id='" + prod_id + "' class='THEME-BTN transparent-btn remove-wishlist-btn'><i class='fas fa-heart' style='color:#DD574E'></i>Remove</a>";

                $.ajax({
                    url: route,
                    type: 'post',
                    data: {status: status, type:type},
                    success: function (response) {
                        _self.hide();
                        _self.parent().append(remove_btn);
                    }
                });
            });


            $(document).on('click', 'a.remove-wishlist-btn', function (e) {
                e.preventDefault();
                var _self = $(this);
                var type = "product";
                var prod_id = $(this).attr("product_id");
                var route = "{{route('wishlist.delete',['id'=> 'id'])}}";
                route = route.replace('id', prod_id);
                var status = "remove";
                var add_wishlist_btn = "<a href='' product_id='" + prod_id + "' class='THEME-BTN transparent-btn add-wishlist-btn'><i class='fal fa-heart'></i>Add to Favorite</a>";

                $.ajax({
                    url: route,
                    type: 'post',
                    data: {status: status, type:type},
                    success: function (response) {
                        _self.hide();
                        _self.parent().append(add_wishlist_btn);
                    }
                });

            });

            //=======================================================================================================

            $(document).ready(function () {

                //$(".remove-wishlist-btn").hide();
                //wishlist button

                $(".List-View").hide();
                var view = localStorage.getItem('view');
                var product_featured = "";


                $(".list_view_btn").on('click', function () {
                    $(".Grid-View").hide();
                    $(".List-View").show();
                    setView('list');

                    //Change Grid View Icon Color
                    $(".fa-th").attr("style").val("color;lightgrey !important");
                    $(".fa-list-ul").attr("style").val("color;lightgrey !important");

                });

                $(".grid_view_btn").on('click', function () {
                    $(".List-View").hide();
                    $(".Grid-View").show();
                    setView('grid');

                    //Change List View Icon Color
                    // $(".fa-list-ul").css("color","lightgrey !important");
                    // $(".fa-list-th").css("color","#016FB9 !important")

                });

                function setView(view) {
                    localStorage.setItem('view', view);
                }

                console.log(view);
                if (view == 'list') {
                    $(".Grid-View").hide();
                    $(".List-View").show();
                } else if (view == 'grid') {
                    $(".List-View").hide();
                    $(".Grid-View").show();
                } else {

                }

                // Submit form when product feature is selected
                $("#select_product_feature").change(function () {
                    localStorage.setItem('product-featured', $(this).val());
                    $("#product_featured_form").submit();
                });


            });
        </script>
    @endsection
