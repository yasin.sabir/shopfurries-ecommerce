@extends('front-layout.app')

@section('title')
    Contact
@endsection

@section('Main')

    <!-- product-title GROUP -->
    <div class="product-titleGroup contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="title-textLEFT">
                        <p>Home <i class="far fa-angle-right"></i> <span style="color: #000;font-weight: 700;">Contact Us</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-5 text-center">
                    <div class="title-text">
                        <h1>Contact Us</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product-title GROUP -->

    <!-- contact-area START -->
    <div class="contact-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="contact-form">
                        <form method="post" action="{{route('contact-send')}}" class="form-horizontal"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="name">Name <span>*</span></label><br>
                                    <input name="name" type="text" class="@error('name') is-invalid @enderror mb-0" placeholder="E.g Howard jones">
                                    @error('name')
                                        <span class="invalid-feedback d-block" role="alert">
                                          <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-lg-6">
                                    <label for="email">Email <span>*</span></label><br>
                                    <input type="text" name="email" class="@error('email') is-invalid @enderror mb-0" placeholder="E.g howardjones@yahoo.com">
                                    @error('email')
                                        <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-lg-12 mt-3">
                                    <label for="name">Mobile Number (Optional)</label><br>
                                    <input type="text" name="mobile" placeholder="E.g howardjones@yahoo.com">
                                </div>
                                <div class="col-lg-12">
                                    <label for="message">Message <span>*</span></label> <br>
                                    <textarea name="message" class="@error('message') is-invalid @enderror mb-0" id="message" placeholder="Your Message"></textarea>
                                    @error('message')
                                        <span class="invalid-feedback d-block" role="alert">
                                                  <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-lg-12 mt-4">
                                    <button type="submit" class="THEME-BTN">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="map">
                        <div id="test" class="gmap3" style="width: 100%;height: 390px;"></div>
                        <div class="map-marker">
                            <img src="{{asset('front-end/assets/img/TorbanResone.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- contact-area END -->

@endsection

@section('custom-front-script')
    <script>
        $(function () {
            var center = [37.772323, -122.214897];
            $('#test')
                .gmap3({
                    center: center,
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                })
                .fit()
            ;
        });
    </script>
@endsection
