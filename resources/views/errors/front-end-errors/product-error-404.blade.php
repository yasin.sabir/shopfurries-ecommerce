@extends('front-layout.app')

@section('title')
    404 - Not Found
@endsection

@section('custom-front-css')
    <style>
        .not-found-font-heading{
            font-size: 18em;
            color: #016FB9;
            text-shadow: 0px 0px 3px black;
        }

        .height{
            height: 300px;
        }

        .sub-heading-404{
            font-size: 3em;
             color: #000;
            text-shadow: 0px 0px 3px black;
        }

        .footer-area{
            margin-top: 15em;
        }

    </style>
@endsection

@section('Main')

    <!-- 404-area START -->
    <div class="about-area">
        <div class="container">

            <div class="row height-control">
                <div class="col-md-12 text-center">
                    <div class="my-5 height">
                        <h1 class="not-found-font-heading">
                            404
                        </h1>
                        <span class="sub-heading-404">
                            Product Not Found!
                        </span>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- 404-area END -->

@endsection



@section('custom-front-script')

@endsection
