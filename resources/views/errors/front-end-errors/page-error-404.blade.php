<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        @yield('title')
    </title>


    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--<link rel="manifest" href="site.webmanifest">--}}
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
    <!-- Place favicon.ico in the root directory -->
    <!-- CSS here -->
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{asset('theme-assets/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/meanmenu.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/slick.css') }}">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/gallery.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/jquery.nice-number.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/jquery.nice-number.css') }}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/toastr/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('other-assets/lightbox/css/lightbox.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/assets/css/custom-stripe.css')}}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/custom-css.css') }}">
    <style>
        .not-found-font-heading{
            font-size: 18em;
            color: #016FB9;
            text-shadow: 0px 0px 3px black;
        }

        .height{
            height: 300px;
        }

        .sub-heading-404{
            font-size: 3em;
            color: #000;
            text-shadow: 0px 0px 3px black;
        }

        .footer-area{
            margin-top: 15em;
        }

        #myBtn {
            display: none; /* Hidden by default */
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            border: none;
            outline: none;
            background-color: #016FB9;
            color: white;
            cursor: pointer;
            padding: 12px;
            border-radius: 100%;
            font-size: 18px;
            height: 50px;
            width: 50px;
            box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
        }

        #myBtn:hover {
            background-color: #555; /* Add a dark-grey background on hover */
        }

    </style>
</head>
<body>

<!-- header-area START -->
<div class="search-area">
    <div class="container">
        <form id="product_search_form" method="post" action="{{route('searchResult')}}">
            @csrf
            <div class="inp">

                <input id="searchBX" name="search_product" type="text" placeholder="Search here...">
                <span class="search_ICON">
                    <i class="far fa-search"></i>
                </span>
            </div>
            <input id="searchBX" type="submit">
        </form>
    </div>
</div>
<div class="header-area shop-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 menu-col">
                <div class="left-menu">
                    <style>
                        .dropdown-menu .show {
                            width: 200px !important;
                        }
                    </style>
                    <nav>
                        <ul>
                            <li><a href="{{ route('home') }}" class="@if(\Request::is('/')) active-menu @endif">home</a></li>
                            <li><a href="{{ route('shop') }}" class="@if(\Request::is('Shop')) active-menu @endif">shop</a></li>
                            <li><a href="{{ route('shop') }}" class="@if(\Request::is('Categories')) active-menu @endif">categories</a></li>
                            <li><a href="{{ route('gallery') }}" class="@if(\Request::is('Gallery')) active-menu @endif">gallery</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 text-center logo-col">
                <div class="logo-area">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('front-end/assets/img/mainLogo.svg')}}" alt="">
                        <img class="text-logo" src="{{ asset('front-end/assets/img/bottomLogo.svg')}}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 text-right">
                <div class="right-menu">
                    <nav>
                        <ul>
                            <div class="MENU">
                                <li><a href="{{ route('faqs') }}" class="@if(\Request::is('FAQs')) active-menu @endif">FAQs</a></li>
                                <li><a href="{{ route('about') }}" class="@if(\Request::is('About')) active-menu @endif">ABOUT</a></li>
                                <li><a href="{{ route('contact-us') }}"  class="@if(\Request::is('Contact')) active-menu @endif">CONTACT</a></li>
                            </div>
                            <li><a href="#" class="searchBTN"><img src="{{ asset('front-end/assets/img/search.svg')}}" alt=""> </a></li>
                            <li>

                                <a href="" class="shop-icon" id="shopCart-Icon"><img src="{{ asset('front-end/assets/img/Shop.svg')}}" alt="">
                                    <span class="custom-badge-cart-primary">0</span>
                                </a>

                            </li>
                            <li>
                                {{-- dropdown-toggle id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"--}}
                                <a href="#" dropdown-toggle id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                    <img src="{{ asset('front-end/assets/img/user.svg')}}" alt="">
                                    <div class="dropdown-menu dropdown-menu-right" style="right: 120px;left: 0;">
                                        @if(!Auth::user())
                                            <a class="dropdown-item" href="{{route('login')}}#Section">Login</a>
                                            <a class="dropdown-item" href="{{route('register')}}">Signup</a>
                                        @else
                                            <a class="dropdown-item" href="#_" disable="disabled">
                                                {{ ucfirst(Auth::user()->name) }}
                                            </a>

                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="{{route('dashboard')}}">Dashboard</a>
                                            <a class="dropdown-item" href="{{route('wishlist.list')}}">Your Wishlist</a>
                                            <a class="dropdown-item" href="{{route('login')}}"
                                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}#Section" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        @endif
                                    </div>
                                </a>
                            </li>
                            <li class="humbargar-manuBAR">
                                <span></span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- header-area END -->

<!-- sidebar START-->
<div class="siteBAR">
    <div class="sidbarLogo">
        <div class="logo-area">
            <a href="{{ route('home') }}">
                <img src="{{ asset('front-end/assets/img/mainLogo.svg')}}" alt="">
                <img class="text-logos" src="{{ asset('front-end/assets/img/bottomLogo.svg') }}" alt="">
            </a>
        </div>

    </div>
    <nav>
        <ul>
            <li><a href="{{ route('home') }}">home</a></li>
            <li><a href="{{ route('shop') }}">shop</a></li>
            <li><a href="{{ route('category') }}">categories</a></li>
            <li><a href="{{ route('gallery') }}">gallery</a></li>
            <li><a href="{{ route('faqs') }}">FAQs</a></li>
            <li><a href="{{ route('about') }}">ABOUT</a></li>
            <li><a href="{{ route('contact-us') }}">CONTACT</a></li>
        </ul>
    </nav>
</div>

<!-- sidebar END-->


<!-- main -->
<!-- 404-area START -->
<div class="about-area">
    <div class="container">

        <div class="row height-control">
            <div class="col-md-12 text-center">
                <div class="my-5 height">
                    <h1 class="not-found-font-heading">
                        404
                    </h1>
                    <span class="sub-heading-404">
                            Page Not Found!
                    </span>
                </div>

            </div>
        </div>

    </div>
</div>
<!-- 404-area END -->

<!-- footer-area START -->
<footer>
    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="far fa-angle-up"></i></button>

    <script>
        //Get the button:
        mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    </script>

    <div class="footer-area">
        <div class="container">
            <div class="row news-latter">  <!-- news-latter -->
                <div class="col-lg-6">
                    <h4>SIGN UP FOR <b>OUR NEWSLETTER</b></h4>
                </div>
                <div class="col-lg-6">
                    <div class="news-letter-form">
                        <form action="">
                            <input type="text" placeholder="Enter your E-mail">
                            <img src=" {{ asset('front-end/assets/img/Message.svg')}}" alt="">
                            <button type="button">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="payment-logo">
                        <ul>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/payPal.svg')}}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/masterCard.svg')}}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/vissa.svg')}}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/PamentCard.svg')}}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/Sofort.svg')}}" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 nINlg">
                    <div class="footer-logo-area text-center">
                        <a href="#">
                            <img src="{{ asset('front-end/assets/img/mainLogo.svg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 offset-md-1">
                    <div class="footer-link-left  text-center">
                        <ul>
                            <li><a href="{{ route('shop') }}">SHOP</a></li>
                            <li><a href="{{ route('category') }}">CATEGORIES</a></li>
                            <li><a href="{{ route('gallery') }}">GALLERY</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 main-Flogo">
                    <div class="footer-logo-area text-center">
                        <a href="#">
                            <img src="{{ asset('front-end/assets/img/mainLogo.svg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-right-link">
                        <ul>
                            <li><a href="{{ route('faqs') }}">FAQs</a></li>
                            <li><a href="{{ route('contact-us') }}">CONTACT</a></li>
                            <li><a href="{{ route('about') }}">ABOUT</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copy-right">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <p>©2019  Torben Goldmund Art-N-Prints All Rights Reserved</p>
                </div>
                <div class="col-lg-6">
                    <ul>
                        <li><a href="#">ACCOUNTS</a></li>
                        <li><a href="#">PRIVACY</a></li>
                        <li><a href="#">TERMS & CONDITIONS</a></li>
                        <li><a href="#">LEGAL NOTICE</a></li>
                    </ul>
                </div>
                <div class="col-lg-2">
                    <div class="footer-social text-right">
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer-area END -->

@include('front-layout.script')


</body>
</html>
