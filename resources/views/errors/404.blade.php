@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <div class="container-fluid">



            </div><!-- /.container-fluid -->

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="error-page" style="margin-top: 20vh;">
                <h2 class="headline text-warning" style="text-align: center;float: none"> 404</h2>
                <div class="error-content" style="text-align: center;margin: 0px 0px;">
                    <h3><i class="fas fa-exclamation-triangle text-warning" ></i> Oops! Page not found.</h3>
                    <p>
                        We could not find the page you were looking for.
                    </p>
                </div>
                <!-- /.error-content -->
            </div>
            <!-- /.error-page -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

@endsection
