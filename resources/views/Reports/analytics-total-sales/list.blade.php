@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Total Sales")}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Total")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('reports.each-sales.list')}}">{{__("routes.Sales")}}</a></li>
                        </ol>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.List")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-4">
                                        <form action="" method="get">
                                            @csrf
                                            <div class="form-group">
                                                <label>Date range:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                                    </div>
                                                    <input type="text" class="form-control float-right" id="reservation" name="selected_range">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                            <div class="form-group">
                                                <input type="submit" value="Search" class="btn btn-primary btn-sm">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <hr>
                                <table id="example1" class="table table-bordered table-striped nowrap">
                                    <thead>
                                    <tr>
                                        <th>No:</th>
                                        <th>{{__("routes.Product")}}:</th>
                                        <th>{{__("routes.Quantity")}}:</th>
                                        <th>{{__("routes.Price")}}:</th>
                                        <th>{{__("routes.Created At")}}:</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php $num = 1; @endphp
                                    @forelse($prod as $k => $v)
                                        <tr>
                                            <td>{{$num}}</td>
                                            <td>{{ucfirst($v['title'])}}</td>
                                            <td>{{ucfirst($v['quantity'])}}</td>
                                            <td>{{$v['price']}}</td>
                                            <td>{{\Carbon\Carbon::parse($v['created_at'])->format('d-m-Y g:i a')}}</td>
                                        </tr>

                                        @php $num++; @endphp
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>

                                <hr>


                                @if( isset($total_quantity) && isset($total_price))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>Details</h5>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Total Quantity Of Product : {{$total_quantity}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Total Price : {{"$".number_format($total_price,2)}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-left">
                                            <div class="form-group">
                                                <a href="{{route('reports.total-sales.export')}}" class="btn btn-success btn-sm">Export Data</a>
                                            </div>
                                        </div>

                                    </div>
                                @endif



                            </div>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 7,
            "autoWidth": false,
        });

        //=======================================================================================================

        //Date range picker
        $('#reservation').daterangepicker();

        //=======================================================================================================


        var rr = $("#product_id option:selected").val();
        console.log(rr);

        //=======================================================================================================

    </script>

@endsection
