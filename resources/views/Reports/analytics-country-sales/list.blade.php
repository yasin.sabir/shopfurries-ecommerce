@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <div class="container-fluid">

                <div class="row mb-2">

                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Country")}}</h1>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Each")}}</li>
                            <li class="breadcrumb-item active"><a
                                    href="{{route('reports.country-sales.list')}}">{{__("routes.Sales")}}</a></li>
                        </ol>
                    </div>

                </div>

            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.List")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-3">
                                        <label>{{__("routes.Select Product")}}:</label>
                                    </div>
                                </div>

                                <div class="row mb-5">
                                    <div class="col-md-3">
                                        <form action="{{route('reports.country-sales.list')}}" method="get">
                                            @csrf
                                            <div class="form-group">
                                                <select class="form-control" name="country" id="">
                                                    <option value="_0">Default All</option>
                                                    @forelse($dropdown_country as $key => $val)
                                                        <option
                                                            value="{{trim(strtolower($val))}}" {{ !empty($_GET['country']) &&  trim(strtolower($_GET['country'])) == trim(strtolower($val)) ? "selected" : ""   }}>{{ucwords($val)}}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary" value="Search">
                                            </div>

                                        </form>
                                    </div>
                                </div>

                                <table id="example1" class="table table-bordered table-striped nowrap">
                                    <thead>
                                    <tr>
                                        <th>No:</th>
                                        <th>{{__("routes.Product")}}:</th>
                                        <th>{{__("routes.Quantity")}}:</th>
                                        <th>{{__("routes.Price")}}:</th>
                                        <th>{{__("routes.Country")}}:</th>
                                        <th>{{__("routes.Created At")}}:</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        @php $num = 1; @endphp
                                        @forelse($prod as $k => $v)
                                            <tr>
                                                <td>{{$num}}</td>
                                                <td>{{ucfirst($v['title'])}}</td>
                                                <td>{{ucfirst($v['quantity'])}}</td>
                                                <td>{{$v['price']}}</td>
                                                <td>{{$v['country']}}</td>
                                                <td>{{\Carbon\Carbon::parse($v['created_at'])->format('d-m-Y g:i a')}}</td>
                                            </tr>

                                            @php $num++; @endphp
                                        @empty

                                        @endforelse
                                    </tbody>
                                </table>

                                <hr>


                                    @if( isset($total_quantity) && isset($total_price))
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5>Details</h5>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    @php

                                                        if( !empty($_GET['country']) && $_GET['country'] != "_0" ){
                                                            $country = $_GET['country'];
                                                        }else{
                                                            $country = "All";
                                                        }

                                                    @endphp
                                                    <label for="">Country : {{$country}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Total Quanity Of Product : {{$total_quantity}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Total Price : {{"$".number_format($total_price,2)}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-left">
                                                <div class="form-group">
                                                    @php
                                                        $name = isset($_GET['country']) ? trim(strtolower($_GET['country'])) : "_0"
                                                    @endphp
                                                    <a href="{{route('reports.country-sales.export' , $name)}}" class="btn btn-success btn-sm">Export Data</a>
                                                </div>
                                            </div>

                                        </div>
                                    @endif

                            </div>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->


@endsection

@section('page-script')

    <script type="text/javascript">

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 7,
            "autoWidth": false,
        });

        //=======================================================================================================

        $("input[name='checkAll']").click(function () {
            $("input[name='faq']").not(this).prop('checked', this.checked);
        });

        // =======================================================================================================


        $("#delete-all-btn").on('click', function () {
            var ids = [];

            var route = '{{ route('event.delete-all') }}';

            $("#delete-all-modal").modal('show');
            $("#delete-all-modal-form").attr("action", route);

            $.each($("input[name='faq']:checked"), function () {
                ids.push($(this).val());
            });
            console.log(ids);
            $("#delete_ids").attr("value", JSON.stringify(ids));

            $('.delete_all_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-all-modal-form").submit();
            });

        });

        //=======================================================================================================
        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("event_id");
            var route = '{{ route('event.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            });

        });

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

        var rr = $("#product_id option:selected").val();
        console.log(rr);

        //=======================================================================================================

    </script>

@endsection
