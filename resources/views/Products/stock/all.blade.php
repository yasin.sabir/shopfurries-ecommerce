@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">
                            <h1> {{__("routes.Stock")}} </h1>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Stock")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('product.stock-list')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.List")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <table id="example1" class="table table-bordered table-striped nowrap">
                                    <thead>
                                    <tr>
                                        <th>No:</th>
                                        <th>{{__("routes.Product ID")}}:</th>
                                        <th>{{__("routes.Title")}}:</th>
                                        <th>{{__("routes.Stock")}}:</th>
                                        <th>{{__("routes.Low Threshold")}}:</th>
                                        <th>{{__("routes.Status")}}:</th>
                                        <th>{{__("routes.Action")}}:</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @forelse($products as $key => $product)
                                        @php
                                            $pp = \App\products_meta::where(['product_id' => $product->id])->get();

                                        @endphp

                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$product->id}}</td>
                                            <td><a href="">{{ucfirst($product->title)}}</a></td>

                                            @foreach ($pp as $k => $v)
                                                @if($v['product_meta'] == "stock")
                                                    <td class="product_stock">
                                                        <span class="right badge badge-primary">{{$v['product_meta_value']}}</span>
                                                    </td>
                                                @endif

                                                @if($v['product_meta'] == "stock_threshold")
                                                    <td class="stock_threshold">
                                                        <span class="right badge badge-primary">{{$v['product_meta_value']}}</span>
                                                    </td>
                                                @endif

                                            @endforeach

                                            <td>
                                                @if($product->status == "1")
                                                    {{__("routes.Enable")}}
                                                @else
                                                    {{__("routes.Disable")}}
                                                @endif
                                            </td>

                                            <td>
                                                <a href="#"
                                                   stock=""
                                                   stock_threshold=""
                                                   product_id="{{$product->id}}"
                                                   class="btn btn-info btn-sm mr-3 custom-edit-btn"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>

                                    @empty

                                    @endforelse


                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->


    <!-- Delete single once-->
    <div class="modal fade" id="edit-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="edit-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Edit Stock Details")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-4">
                                <label for="stock quantity" class="form-check-label">{{__("routes.Stock Quantity")}}
                                </label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control form-control-sm @error('stock_quantity') is-invalid @enderror"
                                       type="number"
                                       value=""
                                       id="stock_quantity"
                                       name="stock_quantity">
                                @error('stock_quantity')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="stock quantity"
                                       class="form-check-label">{{__("routes.Low Threshold")}}
                                </label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control form-control-sm"
                                       type="number"
                                       value=""
                                       id="stock_threshold"
                                       name="stock_threshold">
                                <span style="font-size: 12px;font-style: italic">{{__("routes.When product stock reaches this amount you will be notified by email")}}</span>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger save_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>


@endsection

@section('page-script')

    <script type="text/javascript">

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "autoWidth": false,
        });

        //=======================================================================================================

        $("input[name='checkAll']").click(function(){
            $("input[name='faq']").not(this).prop('checked', this.checked);
        });

        //=======================================================================================================




        //=======================================================================================================

        $(document).on('click', '.custom-edit-btn', function () {

            var product_stock           = $(this).parent().parent().children('.product_stock').children('span').text();
            var product_stock_threshold = $(this).parent().parent().children('.stock_threshold').children('span').text();

            var id = $(this).attr("product_id");
            var route = '{{ route('product.stock-detail-edit', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#edit-modal").modal('show');
            $("#edit-modal-form").attr("action", route);
            $("#stock_quantity").val(product_stock);
            $("#stock_threshold").val(product_stock_threshold);

            $('.save_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#edit-modal-form").submit();
            });

        });

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

    </script>

@endsection
