@extends('layouts.backend.app')

@section('page-css')
    <style>

        .is-invalid .select2-selection--multiple{
            border-color: #dc3545 !important;
            padding-right: 2.25rem !important;
            background-repeat: no-repeat !important;
            background-position: center right calc(.375em + .1875rem) !important;
            background-size: calc(.75em + .375rem) calc(.75em + .375rem) !important;
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23dc3545' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23dc3545' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E") !important;
        }

    </style>
@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Add Bundles")}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Product")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('product.bundle.add')}}">{{__("routes.Add Bundles")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>



        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Bundle")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>

                            <form action="{{route('product.bundle.create')}}" id="productBundle_process_form" method="post" enctype="multipart/form-data">
                                @csrf
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5 pr-5 mb-3">
                                            <label>Select Products:</label>
                                            <div class="form-group">
                                                <div class="select2-blue @error('selected_products') is-invalid @enderror">
                                                    <select class="select2" name="selected_products[]" multiple="multiple" data-placeholder="" data-dropdown-css-class="select2-blue" style="width: 100%;">
                                                        <option value>Choose Products</option>
                                                        @foreach($products as $key => $product)
                                                            <option value="{{$product->id}}">{{$product->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('selected_products')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row">

                                        <div class="col-md-6 px-4 mt-3">

                                            <div class="form-group">
                                                <label for="">Bundle Image:</label>
                                                <img class="d-block" src="{{asset('images/placeholders/default-placeholder-600x600.png')}}" id="product-img"
                                                     style="width: 270px;"/>
                                                <div class="custom-file mt-3">
                                                    <input type="file" class="custom-file-input" name="bundle_featured_img"
                                                           id="bundle_featured_img">
                                                    <label class="custom-file-label" for="customFile">{{__("routes.Choose Image")}}</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Bundle Title:</label>
                                                <input type="text" name="bundle_title" value="{{old('bundle_title')}}" class="form-control @error('bundle_title') is-invalid @enderror">
                                                @error('bundle_title')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="">Bundle Description:</label>
                                                <textarea id="textarea1" class="textarea" rows="30" placeholder="Place some text here"
                                                          name="bundle_description"
                                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('bundle_description')}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Bundle Price($):</label>
                                                <input type="text" name="bundle_price" value="{{old('bundle_price')}}" class="form-control @error('bundle_price') is-invalid @enderror">
                                                @error('bundle_price')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="">Bundle Stock:</label>
                                                <input type="text" name="bundle_stock" value="{{old('bundle_stock')}}"  class="form-control @error('bundle_stock') is-invalid @enderror">
                                                @error('bundle_price')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>


                                            <div class="form-group">

                                                <label for="exampleInputEmail1">{{__("routes.Add Bundle Expiry")}}:</label>
                                                <div class="custom-control custom-switch">
                                                    {{--                                                    @if(site_config('site_maintenance_status') == "on" ) checked @else uncheckec @endif--}}
                                                    <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                                    <input type="hidden" class="custom-control-input" name="bundle_start_end_date_check" value="0">
                                                    <label class="custom-control-label" for="customSwitch1"></label>
                                                </div>


                                                <div class="input-group" id="bundle_expiry_div">
                                                    <label for="exampleInputEmail1" class="mr-2">{{__("routes.Start & Expire Date")}}:</label>

                                                    <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="far fa-calendar-alt"></i>
                                                            </span>
                                                    </div>
                                                    <input type="text" value="{{old('startandexpire')}}" class="form-control float-right"
                                                           name="startandexpire"
                                                           id="startandexpire">
                                                    @error('bundle_startandexpire')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                           <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror

                                                </div>
                                            </div>


                                        </div>

                                        <div class="col-md-6 px-4 mt-3">

                                            <div class="form-group">
                                                <label for="">Status:</label>
                                                <select class="form-control @error('status') is-invalid @enderror" name="status">
                                                    <option value>Select Status</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>
                                                @error('status')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="">Bundle Category:</label>
                                                <div class="select2-blue">
                                                    <select class="select2 @error('bundle_category') is-invalid @enderror" name="bundle_category[]" multiple="multiple" data-placeholder="Select categories" data-dropdown-css-class="select2-blue" style="width: 100%;">

                                                        @if ($getCategoryTop->count())

                                                            @foreach($getCategoryTop as $key => $category)

                                                                @if (!empty($getCategoryAll[$category->id]))

                                                                    <option  value="{{$category->id}}">{{$category->Name}}</option>

                                                                    @foreach($getCategoryAll[$category->id] as $childCategory)
                                                                        <option class="pl-2" value="{{$childCategory->id}}">
                                                                            {{$childCategory->Name}}</option>

                                                                        @if(!empty($getCategoryAll[$childCategory->id]))

                                                                            @foreach( $getCategoryAll[$childCategory->id] as $childCategory2 )
                                                                                <option class="pl-4" value="{{$childCategory2->id}}">
                                                                                    {{$childCategory2->Name}}</option>
                                                                            @endforeach

                                                                        @endif

                                                                    @endforeach
                                                                @else
                                                                    <option value="{{$category->id}}">{{$category->Name}}</option>
                                                                @endif
                                                            @endforeach

                                                        @endif

                                                    </select>
                                                </div>


                                                @error('bundle_category')
                                                <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="">Bundle Tags:</label>
                                                <div class="select2-blue">
                                                    <select class="select2 @error('bundle_tags') is-invalid @enderror" name="bundle_tags[]" multiple="multiple" data-placeholder="Select tags" data-dropdown-css-class="select2-blue" style="width: 100%;">
                                                        @foreach($tags as $tag)
                                                            <option value="{{$tag->id}}">{{$tag->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('bundle_tags')
                                                <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="">Bundle Weight:</label>
                                                <input type="text" value="{{old('bundle_Weight')}}" name="bundle_Weight" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Bundle Video Link/URL:</label>
                                                <input type="text" value="{{old('bundle_video_link')}}" class="form-control @error('bundle_video_link') is-invalid @enderror" name="bundle_video_link" id="bundle_video_link" placeholder="Enter Video link here youtube,vimeo">
                                                @error('bundle_video_link')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="">Bundle Video Description:</label>
                                                <textarea id="textarea2" class="textarea" rows="30" placeholder="Place some text here"
                                                          name="bundle_video_description"
                                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('bundle_video_description')}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <label for="product_weight">{{__("routes.Shipping time")}}</label><br>

                                                <div class="d-flex">
                                                    <input type="text" value="{{old('bundle_shipping_time1')}}" class="form-control" name="bundle_shipping_time1" id="bundle_shipping_time1">
                                                    <label class="mt-1 mx-3">to</label>
                                                    <input type="text" value="{{old('bundle_shipping_time2')}}" class="form-control" name="bundle_shipping_time2" id="bundle_shipping_time2">
                                                </div>
                                                <small>
                                                    <cite>
                                                        {{__("routes.Showing estimated shipping time directly under each product")}} , <strong>like 2 to 5</strong>
                                                    </cite>
                                                </small>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-success" id="create_bundle" value="{{__("routes.Create Bundle")}}"
                                                           tabindex="2">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Order</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete this order</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection

@section('page-script')

    <script type="text/javascript">

        $('.select2').select2();

        // Summernote
        $('#textarea1').summernote({
            height: 150,
        });

        $('#textarea2').summernote({
            height: 150,
        });

        //=======================================================================================================

        $("#add_products").click(function () {

            $("#product_process_form").submit();

        });

        $('#type').on('change', function () {
            if (this.value === "percent") {
                $('#discount').attr('maxlength', '2');
                if ($('#discount').val() > 100) {
                    $('#discount').val('100');
                }
            } else {
                $('#discount').removeAttr('maxlength');
            }
        });

        $('.select2bs4').select2()
        $('input[name="startandexpire"]').daterangepicker();

        //=======================================================================================================

        // $("input[name='checkAll']").click(function(){
        //     $("input[name='faq']").not(this).prop('checked', this.checked);
        // });

        // =======================================================================================================

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("coupon_id");
            var route = '{{ route('coupon.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            })
        });

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#product-img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#bundle_featured_img").change(function () {
            readURL(this);
            console.log(readURL(this));
        });

        //=======================================================================================================

        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        //=======================================================================================================

        $("#bundle_expiry_div").hide();
        $('#customSwitch1').click(function() {

            var customSwitch1 = $(this);
            var bundle_start_expiry_div = $("#bundle_expiry_div");
            var bundle_start_expiry_input = $("#startandexpire");



            if ($(this).prop("checked") === true) {
                $("[name=bundle_start_end_date_check]").attr('value',1);
                bundle_start_expiry_input.attr('disabled',false);
                bundle_start_expiry_div.show();
            } else if ($(this).prop("checked") === false) {
                $("[name=bundle_start_end_date_check]").attr('value',0);
                bundle_start_expiry_input.attr('disabled',true);
                bundle_start_expiry_div.hide();
            }
        });


        //=======================================================================================================


    </script>

@endsection
