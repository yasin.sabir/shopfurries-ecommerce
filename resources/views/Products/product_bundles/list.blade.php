@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                        <h1>Product Bundles</h1>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Products</li>
                            <li class="breadcrumb-item active"><a href="{{route('product.bundle.list')}}">Bundle</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">


                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Product Bundle List</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <button class="btn btn-danger btn-sm mb-3" id="delete-all-btn">Trash All</button>

                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" name="checkAll" id="customCheckbox001">
                                                <label for="customCheckbox001" class="custom-control-label"></label>
                                            </div>
                                        </th>
                                        <th>S.No</th>
                                        <th>Product ID</th>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Start / Expiry</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $num=1; @endphp
                                    @forelse($products as $key => $product)
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox" name="faq" id="customCheckbox{{$key}}" value="{{$product->id}}">
                                                    <label for="customCheckbox{{$key}}" class="custom-control-label"></label>
                                                </div>
                                            </td>
                                            <td>{{$num}}</td>
                                            <td>{{$product->id}}</td>
                                            <td>
                                                @php
                                                    $image = "";
                                                    $ff = @unserialize($product->image);
                                                    if(!is_array($ff)){
                                                        $image = $ff;
                                                    }else{
                                                        $image = $product->image;
                                                    }
                                                    $default_size = \App\SiteSetting::where(['key' => 'product_image_size'])->first();
                                                @endphp

                                                @if(!is_array($ff))
                                                    @if(!empty($product->image))
                                                        <img  width="80" height="50" ff="af" src="{{ asset('storage/'.$product->image)}}">
                                                    @else
                                                        <img  width="80" height="50" src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"/>
                                                    @endif
                                                @else
                                                    <img  width="80" height="50" src="{{ asset('storage/'.$ff[$default_size->value])}}">
                                                @endif

                                            </td>
                                            <td>${{number_format($product->price,2)}}</td>
                                            <td>
                                                {{get_product_meta($product->id , 'bundle_start_date')}} -
                                                {{get_product_meta($product->id , 'bundle_end_date')}}
                                            </td>
                                                <td>
                                                    <div class="custom-control custom-switch">
                                                        <input type="hidden" class="review_id" value="{{$product->id}}">
                                                        <input type="checkbox" class="custom-control-input review-customSwitch" value="{{$product->id}}" id="customSwitch_{{$product->id}}" @if(isset($product->status) && $product->status == "1" ) checked @else uncheckeck @endif>
                                                        <label class="custom-control-label" for="customSwitch_{{$product->id}}"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="{{route('product.bundle.edit',encrypt($product->id))}}" class="btn btn-info btn-sm mr-3"><i class="fa fa-edit"></i></a>
                                                    <a href="#" class="custom-delete-btn btn btn-danger btn-sm" product_id="{{$product->id}}" product_name="{{ $product->title }}"><i class="fa fa-trash"></i></a>
                                                </td>
                                        </tr>
                                        @php $num++; @endphp
                                    @empty
                                        <tr>
                                            <td colspan="7">No Product Found</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>

                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Delete single once-->
    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Product</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete this Product!</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

    <!-- Delete more than once -->
    <div class="modal fade" id="delete-all-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-all-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="delete_ids" id="delete_ids" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Products</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete all the selected products!</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_all_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>


@endsection

@section('page-script')

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================

        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });

        var status = "";

        $('.review-customSwitch').click(function(){

            var customSwitch1 = $(this);
            var id = customSwitch1.val();
            console.log(id);
            var route = "{{route('product.status-update',['id' => 'id'])}}";
            route = route.replace('id',id);

            if($(this).prop("checked") === true){

                console.log("checked");
                status = "1";

                $.ajax({
                    context: this,
                    url: route,
                    type: 'post',
                    data: {"status":status},
                    success:function(response){
                        //console.log(response+" - Mode is on");
                        toastr.success('Product is Enabled <strong>On</strong> !', 'Product Alert!');
                    }
                });
            }
            else if($(this).prop("checked") === false){

                console.log("Unchecked");
                status = "0";
                $.ajax({
                    context: this,
                    url:route,
                    type: 'post',
                    data: {"status":status},
                    success:function(response){
                        //console.log(response+" - Mode is off");
                        toastr.error('Product is disabled <strong>Off</strong> !', 'Product Alert!');

                    }
                });
            }
        });


        //=======================================================================================================

        $("input[name='checkAll']").click(function(){
            $("input[name='faq']").not(this).prop('checked', this.checked);
        });

        // =======================================================================================================


        $("#delete-all-btn").on('click' , function () {
            var ids = [];

            var route = '{{ route('product.delete-all') }}';

            $("#delete-all-modal").modal('show');
            $("#delete-all-modal-form").attr("action", route);

            $.each($("input[name='faq']:checked"), function(){
                ids.push($(this).val());
            });
            console.log(ids);
            $("#delete_ids").attr("value",JSON.stringify(ids));

            $('.delete_all_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-all-modal-form").submit();
            });

        });

        //=======================================================================================================

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("product_id");
            var route = '{{ route('product.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            });

        });

        //=======================================================================================================




    </script>

@endsection
