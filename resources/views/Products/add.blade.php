@extends('layouts.backend.app') @section('page-css')

    <style>
        .note-editable.card-block {
            height: 300px;
        }
    </style>

@endsection
@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> {{__("routes.Add Product")}} </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Product")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('product.add')}}">{{__("routes.Add")}}</a></li>
                        </ol>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">


                        @if ($errors->any())
                            <div class="custom-alert-box">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

{{--                    @if (session('noti'))--}}
{{--                        <div class="custom-alert-box">--}}
{{--                            <ul>--}}
{{--                                <li>{{ session('noti') }}</li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    @endif--}}

                    <!-- form start -->
                        <form method="post" action="{{route('product.create')}}" class="form-horizontal"
                              enctype="multipart/form-data" enctype="multipart/form-data">
                            @csrf

                            <div class="row">
                                <div class="col-md-9">

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="card card-default">
                                                <div class="card-body pad">
                                                    <div class="form-group">
                                                        <label for="product_nae" class="mt-1">{{__("routes.Product Name")}} <span class="req-label">*</span> </label>
                                                        <input type="text"
                                                               class="form-control @error('product_name') is-invalid @enderror"
                                                               id="product_nae" value="{{old('product_name')}}" name="product_name"
                                                               placeholder="Product Names">
                                                        @error('product_name')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                               <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <hr>
                                                    <div class="form-group">
                                                        <label for="desciption" class="mt-1">{{__("routes.Description")}}</label>
                                                        <div class="mt-1">
                                                <textarea id="textarea1" class="textarea" rows="30" placeholder="Place some text here"
                                                          name="description"
                                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('description')}}</textarea>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    {{--                                            <div class="form-group">--}}
                                                    {{--                                                <label for="desciption" class="mt-1">Product Features</label>--}}
                                                    {{--                                                <div class="mt-1">--}}
                                                    {{--                                                <textarea id="textarea2" class="textarea" rows="30" placeholder="Place some text here"--}}
                                                    {{--                                                          name="product_features_description"--}}
                                                    {{--                                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">--}}
                                                    {{--                                                 </textarea>--}}
                                                    {{--                                                </div>--}}
                                                    {{--                                            </div>--}}



                                                </div>
                                                <!-- /.card-body -->
                                            </div>

                                            <!-- Product Data Section Start-->
                                            <div class="card card-default">
                                                <div class="card-header">
                                                    <h3 class="card-title">{{__("routes.Product Gallery")}}</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                            <i class="fas fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="card-body pad">
                                                    <div class="form-group">
                                                        <div class="file-loading">
                                                            <input id="file-1" type="file" name="file[]" multiple class="file">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>

                                            <!-- Product Data Section Start-->
                                            <div class="card card-default">
                                                <div class="card-header">
                                                    <h3 class="card-title">{{__("routes.Product Data")}}</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                            <i class="fas fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="card-body pad">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-5 col-sm-3">
                                                                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab"
                                                                     role="tablist" aria-orientation="vertical">
                                                                    <a class="nav-link active" id="vert-tabs-general-tab"
                                                                       data-toggle="pill" href="#vert-tabs-general" role="tab"
                                                                       aria-controls="vert-tabs-settings" aria-selected="true">{{__("routes.General")}}</a>
                                                                    <a class="nav-link" id="vert-tabs-inventory-tab"
                                                                       data-toggle="pill" href="#vert-tabs-inventory" role="tab"
                                                                       aria-controls="vert-tabs-messages" aria-selected="false">{{__("routes.Inventory")}}</a>
                                                                    <a class="nav-link" id="vert-tabs-upsell-tab"
                                                                       data-toggle="pill" href="#vert-tabs-upsell" role="tab"
                                                                       aria-controls="vert-tabs-messages" aria-selected="false">Upsell</a>
                                                                </div>
                                                            </div>
                                                            <div class="col-7 col-sm-9">
                                                                <div class="tab-content" id="vert-tabs-tabContent">
                                                                    <div class="tab-pane text-left fade show active"
                                                                         id="vert-tabs-general" role="tabpanel"
                                                                         aria-labelledby="vert-tabs-general-tab">

                                                                        <div class="form-group">

                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label for="Regular price"
                                                                                           class="form-check-label">{{__("routes.Regular Price")}} ($)  <span class="req-label">*</span></label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <input class="form-control form-control-sm @error('regular_price') is-invalid @enderror"
                                                                                           value="{{old('regular_price')}}" type="text"
                                                                                           name="regular_price"
                                                                                           placeholder=" number validation ">
                                                                                    @error('regular_price')
                                                                                    <span class="invalid-feedback d-block" role="alert">
                                                                               <strong>{{ $message }}</strong>
                                                                             </span>
                                                                                    @enderror
                                                                                </div>
                                                                            </div>

                                                                            <hr>

                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label for="Regular price"
                                                                                           class="form-check-label">{{__("routes.Sale Price")}} ($)  <span class="req-label">*</span></label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <input class="form-control form-control-sm"
                                                                                           type="text" value="{{old('sale_prices')}}"
                                                                                           name="sale_prices"
                                                                                           placeholder=" number validation ">

                                                                                    @if (session('noti'))
                                                                                        <span class="invalid-feedback d-block" role="alert">
                                                                                    <strong>
                                                                                      {{ session('noti') }}
                                                                                    </strong>
                                                                                </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="text-right">
                                                                                <a href="#_" id="sales_schedule"
                                                                                   style="font-size: 12px;font-style: italic;">{{__("routes.Sales Schedule")}}</a>
                                                                            </div>

                                                                            <hr>

                                                                            <div id="custom-sales-schedule">

                                                                                <div class="row">
                                                                                    <div class="col-md-4">
                                                                                        <label for="Regular price"
                                                                                               class="form-check-label">{{__("routes.Sales Price Date")}}:
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-md-8">
                                                                                        <div class="input-group">
                                                                                            <div class="input-group-prepend">
                                                                                    <span class="input-group-text">
                                                                                        <i class="far fa-calendar-alt"></i>
                                                                                    </span>
                                                                                            </div>
                                                                                            <input type="text"
                                                                                                   class="form-control float-right"
                                                                                                   value="{{old('sales_price_schd')}}"
                                                                                                   name="sales_price_schd"
                                                                                                   id="reservation">
                                                                                        </div>
                                                                                        <div class="text-right">
                                                                                            <a href="#_"
                                                                                               id="cancel_sales_schedule"
                                                                                               style="font-size: 12px;font-style: italic;">{{__("routes.Cancel")}}</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                    <div class="tab-pane fade" id="vert-tabs-inventory"
                                                                         role="tabpanel"
                                                                         aria-labelledby="vert-tabs-inventory-tab">

                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="Regular price"
                                                                                       class="form-check-label">{{__("routes.Item number")}}(sku):</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <input class="form-control form-control-sm"
                                                                                       type="text" value="{{old('product_sku')}}" name="product_sku"
                                                                                       placeholder="number validation">
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="Regular price"
                                                                                       class="form-check-label">{{__("routes.Inventory")}}:
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <select class="form-control form-control-sm">
                                                                                    <option value="in-stock">In Stock</option>
                                                                                    <option value="out-of-stock">Out of Stock
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <hr>

                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="Regular price"
                                                                                       class="form-check-label">{{__("routes.manage")}} {{__("routes.Inventory")}}
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <input class="form-check-input" type="checkbox"
                                                                                       id="manage_stock_checked"
                                                                                       name="manage_stock_checked"
                                                                                       value="checked"/>
                                                                                <span style="font-style: italic">{{__("routes.Enable stock management at product level")}}</span>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div id="custom-manage-stock-section">
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label for="stock quantity"
                                                                                           class="form-check-label">{{__("routes.Stock Quantity")}}
                                                                                    </label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <input class="form-control form-control-sm"
                                                                                           type="number" value="{{old('stock_quantity')}}"
                                                                                           name="stock_quantity">
                                                                                    <span
                                                                                        style="font-size: 12px;font-style: italic">{{__("routes.If product stock not define so the default value will be add.")}}<a
                                                                                            href="{{route("setting.default_product_stock")}}"><em>Change default stock value</em></a></span>
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label for="stock quantity"
                                                                                           class="form-check-label">{{__("routes.Low Threshold")}}
                                                                                    </label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <input class="form-control form-control-sm"
                                                                                           type="number" value="{{old('low_stock_threshold')}}"
                                                                                           name="low_stock_threshold">
                                                                                    <span
                                                                                        style="font-size: 12px;font-style: italic">{{__("routes.When product stock reaches this amount you will be notified by email")}}</span>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                    <div class="tab-pane fade" id="vert-tabs-upsell"
                                                                         role="tabpanel"
                                                                         aria-labelledby="vert-tabs-upsell-tab">

                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="stock quantity"
                                                                                       class="form-check-label">Upsell Product
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <select class="form-control select2" style="width: 100%;" name="up_sell">
                                                                                    <option value>Select Product</option>
                                                                                    @foreach($products as $data)
                                                                                        <option value="{{$data->id}}">{{$data->title}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>


                                                                        <hr>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="stock quantity"
                                                                                       class="form-check-label">Upsell Discount
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <input class="form-control form-control-sm"
                                                                                       type="text" value="{{old('upsell_amount')}}"
                                                                                       name="upsell_amount">
                                                                            </div>
                                                                        </div>

                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <!-- Product Data Section End-->

                                            <!-- Product Others Section Start-->
                                            <div class="card card-default">
                                                <div class="card-header">
                                                    <h3 class="card-title">{{__("routes.Others")}}</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                            <i class="fas fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="card-body pad">
                                                    <div class="form-group">
                                                        <label for="ProdcutMaterial" class="form-check-label">{{__("routes.Material")}}  <span class="req-label">*</span> </label>
                                                        <input type="text" class="form-control @error('product_material') is-invalid @enderror" value="{{old('product_material')}}" name="product_material" id="product_material" placeholder="Each value separate by comma like Basic , Hard ">
                                                        <small>
                                                            <cite>
                                                                Use comma for multiple values
                                                            </cite>
                                                        </small>

                                                        @error('product_material')
                                                        <span class="invalid-feedback d-block" role="alert">
                                                   <strong>{{ $message }}</strong>
                                                 </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProductSizes" class="form-check-label">{{__("routes.Size in cm")}}  <span class="req-label">*</span> </label>
                                                        <input type="text" class="form-control @error('product_sizes') is-invalid @enderror" value="{{old('product_sizes')}}" name="product_sizes" id="product_sizes" placeholder="Each value separate by comma like Basic,Hard ">
                                                        <small>
                                                            <cite>
                                                                Use comma for multiple values
                                                            </cite>
                                                        </small>

                                                        @error('product_sizes')
                                                        <span class="invalid-feedback d-block" role="alert">
                                                   <strong>{{ $message }}</strong>
                                                 </span>
                                                        @enderror
                                                    </div>
                                                    <hr>


                                                    <div class="form-group">
                                                        <label for="VideoDescription" class="form-check-label">{{__("routes.Video Link")}}</label><br>
                                                        <input type="text" value="{{old('product_video')}}" class="form-control @error('product_video') is-invalid @enderror" name="product_video" id="product_video" placeholder="Enter Video link here youtube,vimeo">
                                                        @error('product_video')
                                                        <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="VideoDescription" class="form-check-label">{{__("routes.Video Description")}}</label><br>
                                                        <textarea id="textarea3" class="textarea" rows="30" placeholder="Place some text here"
                                                                  name="product_video_description">{{old('product_video_description')}}</textarea>

                                                        {{--                                                <div class="card card-default">--}}
                                                        {{--                                                    <div class="card-header">--}}
                                                        {{--                                                        <h3 class="card-title">Product Video</h3>--}}
                                                        {{--                                                        <div class="card-tools">--}}
                                                        {{--                                                            <button type="button" class="btn btn-tool" data-card-widget="collapse">--}}
                                                        {{--                                                                <i class="fas fa-minus"></i></button>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                    </div>--}}
                                                        {{--                                                    <div class="card-body pad">--}}
                                                        {{--                                                        <div class="form-group">--}}
                                                        {{--                                                            <div class="file-loading">--}}
                                                        {{--                                                                <input id="video" type="file" name="product_video" multiple class="file">--}}
                                                        {{--                                                            </div>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                    </div>--}}
                                                        {{--                                                </div>--}}
                                                    </div>

                                                    <hr>
                                                    <div class="form-group">
                                                        <label for="product_weight" class="form-check-label">{{__("routes.Weight")}} (kg)</label><br>
                                                        <input type="text" value="{{old('product_weight')}}" class="form-control" name="product_weight" id="product_weight">
                                                        <small>
                                                            <cite>
                                                                {{__("routes.Weight in decimal form")}}
                                                            </cite>
                                                        </small>
                                                    </div>

                                                    <hr>

                                                    <div class="form-group">
                                                        <label for="product_weight" class="form-check-label">{{__("routes.Shipping time")}}</label><br>

                                                        <div class="d-flex">
                                                            <input type="text" value="{{old('product_shipping_time1')}}" class="form-control" name="product_shipping_time1" id="product_shipping_time1">
                                                            <label class="mt-1 mx-3">to</label>
                                                            <input type="text" value="{{old('product_shipping_time2')}}" class="form-control" name="product_shipping_time2" id="product_shipping_time2">
                                                        </div>
                                                        <small>
                                                            <cite>
                                                                {{__("routes.Showing estimated shipping time directly under each product")}} , <strong>like 2 to 5</strong>
                                                            </cite>
                                                        </small>
                                                    </div>


                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <!-- Product Others Section End-->

                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-3">

                                    <!-- Product Info Section Start-->
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Action")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                        class="fas fa-minus"></i></button>
                                            </div>
                                        </div>

                                        <div class="card-body pad">

                                            @hasrole('admin')
                                            <div class="form-group text-left">
                                                <label for="attr_name" class="form-check-label">{{__("routes.Status")}}:</label>
                                                <select class="form-control" name="status">
                                                    <option value="yes">{{__("routes.Publish")}}</option>
                                                    <option value="no">{{__("routes.Draft")}}</option>
                                                </select>
                                            </div>
                                            @endhasrole

                                            <div class="form-group text-center">
                                                <input type="submit" class="btn btn-primary btn-sm w-75" value="{{__("routes.Publish")}}">
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- Product Info Section End-->

                                    <!-- Category Section Start-->
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Category")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                        class="fas fa-minus"></i></button>
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="select2-blue">
                                                    <select class="select2"  name="categories[]" multiple="multiple" data-placeholder="Select categories" data-dropdown-css-class="select2-blue" style="width: 100%;">

                                                        @if ($getCategoryTop->count())

                                                            @foreach($getCategoryTop as $key => $category)

                                                                @if (!empty($getCategoryAll[$category->id]))

                                                                    <option {{ (collect(old('categories'))->contains($category->id)) ? 'selected':'' }} value="{{$category->id}}">{{$category->Name}}</option>

                                                                    @foreach($getCategoryAll[$category->id] as $childCategory)
                                                                        <option class="pl-2"
                                                                                {{ (collect(old('categories'))->contains($childCategory->id)) ? 'selected':'' }}
                                                                                value="{{$childCategory->id}}">
                                                                            {{$childCategory->Name}}</option>

                                                                        @if(!empty($getCategoryAll[$childCategory->id]))

                                                                            @foreach( $getCategoryAll[$childCategory->id] as $childCategory2 )
                                                                                <option class="pl-4" {{ (collect(old('categories'))->contains($childCategory2->id)) ? 'selected':'' }} value="{{$childCategory2->id}}">
                                                                                    {{$childCategory2->Name}}</option>
                                                                            @endforeach

                                                                        @endif

                                                                    @endforeach
                                                                @else
                                                                    <option {{ (collect(old('categories'))->contains($category->id)) ? 'selected':'' }} value="{{$category->id}}">{{$category->Name}}</option>
                                                                @endif
                                                            @endforeach

                                                        @endif

                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>

                                    <!-- Tags Section Start-->
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Tags")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                        class="fas fa-minus"></i></button>
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="select2-blue">
                                                    <select class="select2" name="tags[]" multiple="multiple" data-placeholder="Select tags" data-dropdown-css-class="select2-blue" style="width: 100%;">
                                                        @foreach($tags as $tag)
                                                            <option {{ (collect(old('tags'))->contains($tag->id)) ? 'selected':'' }} value="{{$tag->id}}">{{$tag->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>

                                    <!-- Artist Section Start -->
                                    @hasrole('admin')
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Artists")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                        class="fas fa-minus"></i></button>
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="form-group">
                                                <select class="form-control" name="artists" id="artists">
                                                    <option value="">Select Option</option>
                                                    <option {{ old('artists') == Auth::user()->id ? "selected" : "" }} value="{{Auth::user()->id}}">Your Self</option>
                                                    @forelse($artists as $key => $val)
                                                        <option {{ old('artists') == $val->id ? "selected" : "" }} value="{{$val->id}}">{{ucfirst($val->name)}}</option>
                                                    @empty

                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    @endhasrole
                                    <!-- Artist Section End -->

                                    <!-- Category Section End-->
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Product Image")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                        class="fas fa-minus"></i></button>
                                            </div>
                                        </div>

                                        <div class="card-body pad">
                                            <div class="form-group">
                                                <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}" id="product-img"
                                                     style="width: 100%;height: 100%;"/>

                                                <div class="custom-file mt-3">
                                                    <input type="file" class="custom-file-input" name="featured_img"
                                                           id="featured_img">
                                                    <label class="custom-file-label" for="customFile">{{__("routes.Choose Image")}}</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>

                            </div>

                            <!-- /.row -->
                        </form>

                    </div>
                </div>


            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection @section('page-script')

    <script>

        $("#product_shipping_time1 , #product_shipping_time2").keydown(function (event) {


            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {

            } else {
                event.preventDefault();
            }

            if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
                event.preventDefault();

        });

        //======================================================================================================

        $("input[id*='product_weight']").keydown(function (event) {


            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {

            } else {
                event.preventDefault();
            }

            if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
                event.preventDefault();

        });

        //======================================================================================================

        // Summernote
        $('#textarea1').summernote({
            height: 200,
        });

        $('#textarea2').summernote({
            height: 200,
        });

        $('#textarea3').summernote({
            height: 200,
        });

        //======================================================================================================

        $(function(){

            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            // $('.select2bs4').select2({
            //     theme: 'bootstrap4'
            // })

            //Date range picker
            $('#reservation').daterangepicker();
            var date = $("#reservation").val();
            $("#reservation").val(date);



            $("#custom-sales-schedule").css("display", "none");
            $("#custom-manage-stock-section").css("display", "none");
            $("#custom-attribute-section").css("display", "none");

            $("#sales_schedule").on('click', function () {
                $("#custom-sales-schedule").css("display", "block");
            });

            $("#cancel_sales_schedule").on('click', function () {
                $("#custom-sales-schedule").css("display", "none");
            });

            $("#manage_stock_checked").click(function () {

                if ($(this).is(":checked")) {
                    $("#custom-manage-stock-section").css("display", "block");
                } else {
                    $("#custom-manage-stock-section").css("display", "none");
                }

            });

            $("#add_attribute").on('click', function () {
                $("#custom-attribute-section").css("display", "block");
            });

            $("#cancel-add-attribute-section").on('click', function () {
                $("#custom-attribute-section").css("display", "none");
            });

        });

        //======================================================================================================

        $("#file-1").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'gif','mp4'],
            overwriteInitial: false,
            maxFileSize: 100000,
            maxFilesNum: 10,
            showClose: false,
            showUpload: false,
            showCancel: false,
            showZoom: true,

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        //======================================================================================================

        $("#video").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['mp3','.webm','mp4'],
            overwriteInitial: false,
            maxFileSize: 100000,
            maxFilesNum: 1,
            showClose: false,
            showUpload: false,
            showCancel: false,
            showZoom: true,

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        //======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#product-img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#featured_img").change(function () {
            readURL(this);
            console.log(readURL(this));
        });

        //======================================================================================================

        $("[name=sale_prices]").val(0);

    </script>

@endsection
