@extends('layouts.backend.app') @section('page-css')

    <style>
        .note-editable.card-block {
            height: 300px;
        }
    </style>

@endsection @section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> {{__("routes.Edit")}} {{$product->title}} </h1>
                        <a href="{{url('Shop/Product/'.$product->title.'/'.$product->id)}}">View Product</a>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Product")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('product.edit',$product->id)}}">{{__("routes.Edit")}}</a></li>
                        </ol>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        @if ($errors->any())
                            <div class="custom-alert-box">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

{{--                        @if (session('noti'))--}}
{{--                            <div class="custom-alert-box">--}}
{{--                                <ul>--}}
{{--                                    <li>{{ session('noti') }}</li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        @endif--}}

                    <!-- form start -->
                        <form method="post" action="{{route('product.update', $product->id)}}" class="form-horizontal"
                              enctype="multipart/form-data" enctype="multipart/form-data">
                            @csrf

                            <div class="row">
                                <div class="col-md-9">

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="card card-default">
                                                <div class="card-body pad">
                                                    <div class="form-group">
                                                        <label for="product_nae" class="mt-1">{{__("routes.Product Name")}}</label>
                                                        <input type="text" class="form-control"
                                                               class="form-control  @error('product_name') is-invalid @enderror"
                                                               id="product_nae" value="{{$product->title}}" name="product_name"
                                                               placeholder="Product Names">
                                                        @error('product_name')
                                                        <span class="invalid-feedback" role="alert">
                                                   <strong>{{ $message }}</strong>
                                                 </span>
                                                        @enderror
                                                    </div>
                                                    <hr>
                                                    <div class="form-group">
                                                        <label for="desciption" class="mt-1">{{__("routes.Description")}}</label>
                                                        <div class="mt-1">
                                                <textarea class="textarea" rows="30" placeholder="Place some text here"
                                                          name="description"
                                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                {{ isset($product->description) ? $product->description :"" }}
                                                </textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- /.card-body -->
                                            </div>

                                            <!-- Product Data Section Start-->
                                            <div class="card card-default">
                                                <div class="card-header">
                                                    <h3 class="card-title">{{__("routes.Product Gallery")}}</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                            <i class="fas fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="card-body pad">
                                                    <div class="form-group">
                                                        <div class="file-loading">
                                                            <input id="file-1" type="file" name="file[]" multiple class="file"
                                                                   data-overwrite-initial="false">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>

                                            <!-- Product Data Section Start-->
                                            <div class="card card-default">
                                                <div class="card-header">
                                                    <h3 class="card-title">{{__("routes.Product Data")}}</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                            <i class="fas fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="card-body pad">
                                                    <div class="form-group">

                                                        <div class="row">
                                                            <div class="col-5 col-sm-3">
                                                                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab"
                                                                     role="tablist" aria-orientation="vertical">
                                                                    <a class="nav-link active" id="vert-tabs-general-tab"
                                                                       data-toggle="pill" href="#vert-tabs-general" role="tab"
                                                                       aria-controls="vert-tabs-settings" aria-selected="true">{{__("routes.General")}}</a>
                                                                    <a class="nav-link" id="vert-tabs-inventory-tab"
                                                                       data-toggle="pill" href="#vert-tabs-inventory" role="tab"
                                                                       aria-controls="vert-tabs-messages" aria-selected="false">{{__("routes.Inventory")}}</a>
                                                                    <a class="nav-link" id="vert-tabs-upsell-tab"
                                                                       data-toggle="pill" href="#vert-tabs-upsell" role="tab"
                                                                       aria-controls="vert-tabs-messages" aria-selected="false">upsell</a>
                                                                </div>
                                                            </div>
                                                            <div class="col-7 col-sm-9">
                                                                <div class="tab-content" id="vert-tabs-tabContent">
                                                                    <div class="tab-pane text-left fade show active"
                                                                         id="vert-tabs-general" role="tabpanel"
                                                                         aria-labelledby="vert-tabs-general-tab">

                                                                        <div class="form-group">

                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label for="Regular price"
                                                                                           class="form-check-label">{{__("routes.Regular Price")}} ($)</label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <input
                                                                                        class="form-control form-control-sm @error('regular_price') is-invalid @enderror"
                                                                                        value="{{(isset($product_meta['price']) ) ? $product_meta['price'] : '' }}"
                                                                                        type="text" name="regular_price"
                                                                                        placeholder=" number validation ">
                                                                                    @error('regular_price')
                                                                                    <span class="invalid-feedback d-block"
                                                                                          role="alert">
                                                                               <strong>{{ $message }}</strong>
                                                                             </span>
                                                                                    @enderror
                                                                                </div>
                                                                            </div>

                                                                            <hr>

                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label for="Regular price"
                                                                                           class="form-check-label">{{__("routes.Sale Price")}}
                                                                                        ($)</label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <input class="form-control form-control-sm"
                                                                                           type="text"
                                                                                           value="{{(isset($product_meta['sale_price']) ) ? $product_meta['sale_price'] : '' }}"
                                                                                           name="sale_prices"
                                                                                           placeholder=" number validation ">

                                                                                    @if (session('noti'))
                                                                                        <span class="invalid-feedback d-block"
                                                                                              role="alert">
                                                                                <strong>
                                                                                  {{ session('noti') }}
                                                                                </strong>
                                                                            </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="text-right">
                                                                                <a href="#_" id="sales_schedule"
                                                                                   style="font-size: 12px;font-style: italic;">{{__("routes.Sales Schedule")}}</a>
                                                                            </div>

                                                                            <hr>

                                                                            <div id="custom-sales-schedule">

                                                                                <div class="row">
                                                                                    <div class="col-md-4">
                                                                                        <label for="Regular price"
                                                                                               class="form-check-label">{{__("routes.Sales Price Date")}}:
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-md-8">
                                                                                        <div class="input-group">
                                                                                            <div class="input-group-prepend">
                                                                                    <span class="input-group-text">
                                                                                                <i class="far fa-calendar-alt"></i>
                                                                                    </span>
                                                                                            </div>
                                                                                            <input type="text"
                                                                                                   name="sales_price_schd"
                                                                                                   value="{{$product_meta['sale_start_date']}} - {{$product_meta['sale_end_date']}}"
                                                                                                   class="form-control float-right"
                                                                                                   id="reservation">
                                                                                        </div>
                                                                                        <div class="text-right">
                                                                                            <a href="#_"
                                                                                               id="cancel_sales_schedule"
                                                                                               style="font-size: 12px;font-style: italic;">{{__("routes.Cancel")}}</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                    <div class="tab-pane fade" id="vert-tabs-inventory"
                                                                         role="tabpanel"
                                                                         aria-labelledby="vert-tabs-inventory-tab">

                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="Regular price"
                                                                                       class="form-check-label">{{__("routes.Item number")}} (sku):</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <input class="form-control form-control-sm"
                                                                                       type="text"
                                                                                       value="{{ (isset($product_meta['sku']) ) ? $product_meta['sku'] : '' }}"
                                                                                       name="product_sku"
                                                                                       placeholder="number validation">
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="Regular price"
                                                                                       class="form-check-label">{{__("routes.Inventory")}}:
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <select class="form-control form-control-sm">
                                                                                    <option value="in-stock">In Stock</option>
                                                                                    <option value="out-of-stock">Out of Stock
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <hr>

                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="Regular price"
                                                                                       class="form-check-label">{{__("routes.Manage Inventory")}}
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <input class="form-check-input" type="checkbox"
                                                                                       id="manage_stock_checked"
                                                                                       name="manage_stock_checked"
                                                                                       value="" @if(isset($product_meta['stock_manage_chk']) && $product_meta['stock_manage_chk'] == "checked" ) checked @else unchecked @endif>
                                                                                <span style="font-style: italic">{{__("routes.Enable stock management at product level")}}</span>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div id="custom-manage-stock-section">
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label for="stock quantity"
                                                                                           class="form-check-label">{{__("routes.Stock Quantity")}}
                                                                                    </label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <input class="form-control form-control-sm"
                                                                                           type="number"
                                                                                           value="{{(isset($product_meta['stock']) ) ? $product_meta['stock'] : '' }}"
                                                                                           name="stock_quantity">
                                                                                    <span
                                                                                        style="font-size: 12px;font-style: italic">{{__("routes.If product stock not define so the default value will be add.")}}<a
                                                                                            href="{{route("setting.default_product_stock")}}"><em>Change default stock value</em></a></span>
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label for="stock quantity"
                                                                                           class="form-check-label">{{__("routes.Low Threshold")}}
                                                                                    </label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <input class="form-control form-control-sm"
                                                                                           type="number"
                                                                                           value="{{(isset($product_meta['stock_threshold']) ) ? $product_meta['stock_threshold'] : '' }}"
                                                                                           name="low_stock_threshold">
                                                                                    <span
                                                                                        style="font-size: 12px;font-style: italic">{{__("routes.When product stock reaches this amount you will be notified by email")}}</span>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                        </div>

                                                                    <div class="tab-pane fade" id="vert-tabs-upsell"
                                                                         role="tabpanel"
                                                                         aria-labelledby="vert-tabs-upsell-tab">

                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="stock quantity"
                                                                                       upsell_amount class="form-check-label">Upsell
                                                                                    Product
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <select class="form-control select2"
                                                                                        style="width: 100%;" name="up_sell">
                                                                                    <option value>Select Product</option>
                                                                                    @foreach($all_products as $data)
                                                                                        <option value="{{$data->id}}"{{ $product['up_sell'] == $data->id ? 'selected="selected"' : '' }}>{{$data->title}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>


                                                                        <hr>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label for="stock quantity"
                                                                                       class="form-check-label">Upsell Discount
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <input class="form-control form-control-sm"
                                                                                       type="text"
                                                                                       value="{{ isset($product_meta['upsell_amount']) ? strip_tags($product_meta['upsell_amount']) : "" }}"
                                                                                       name="upsell_amount">

                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <!-- Product Data Section End-->

                                            <!-- Product Others Section Start-->
                                            <div class="card card-default">
                                                <div class="card-header">
                                                    <h3 class="card-title">{{__("routes.Others")}}</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                            <i class="fas fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="card-body pad">

                                                    @php

                                                        if(isset($product_meta['material'])){

                                                            $material = unserialize($product_meta['material']);

                                                            $material = array_map(function ($i){
                                                                return str_replace("_"," ",$i);
                                                            },$material);

                                                            $material = implode(",",$material);
                                                        }

                                                        if(isset($product_meta['sizes'])){

                                                            $sizes = unserialize($product_meta['sizes']);

                                                            $sizes = array_map(function ($i){
                                                                return str_replace("_"," ",$i);
                                                            },$sizes);

                                                            $sizes = implode(",",$sizes);
                                                        }

                                                    @endphp

                                                    <div class="form-group">
                                                        <label for="ProdcutMaterial" class="form-check-label">{{__("routes.Material")}}</label>
                                                        <input type="text"
                                                               class="form-control @error('product_material') is-invalid @enderror"
                                                               name="product_material" id="product_material"
                                                               value="{{$material}}"
                                                               placeholder="Each value separate by comma , like Basic , Hard ">
                                                        <small>
                                                            <cite>
                                                                Use comma for multiple values
                                                            </cite>
                                                        </small>

                                                        @error('product_material')
                                                        <span class="invalid-feedback d-block" role="alert">
                                                   <strong>{{ $message }}</strong>
                                                 </span>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="ProductSizes" class="form-check-label">{{__("routes.Size in cm")}}</label>
                                                        <input type="text"
                                                               class="form-control @error('product_sizes') is-invalid @enderror"
                                                               name="product_sizes" id="product_sizes"
                                                               value="{{$sizes}}"
                                                               placeholder="Each value separate by comma , like Basic , Hard ">
                                                        <small>
                                                            <cite>
                                                                Use comma for multiple values
                                                            </cite>
                                                        </small>

                                                        @error('product_sizes')
                                                        <span class="invalid-feedback d-block" role="alert">
                                                   <strong>{{ $message }}</strong>
                                                 </span>
                                                        @enderror

                                                    </div>

                                                    <hr>

                                                    <div class="form-group">
                                                        <label for="VideoDescription" class="form-check-label">{{__("routes.Video Link")}}</label><br>
                                                        <input type="text" value="{{$product->video}}"
                                                               class="form-control @error('product_video') is-invalid @enderror"
                                                               name="product_video" id="product_video"
                                                               placeholder="Enter Video link here youtube,vimeo">
                                                        @error('product_video')
                                                        <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="VideoDescription" class="form-check-label">{{__("routes.Video Description")}}</label><br>
                                                        <textarea id="textarea3" class="textarea" rows="30"
                                                                  placeholder="Place some text here"
                                                                  name="product_video_description">
                                                {{(isset($product_meta['product_video_description']) ) ? $product_meta['product_video_description'] : '' }}
                                            </textarea>
                                                    </div>

                                                    <hr>
                                                    <div class="form-group">
                                                        <label for="product_weight" class="form-check-label">{{__("routes.Weight")}}(kg)</label><br>
                                                        <input type="text" value="{{(isset($product_meta['weight']) ) ? $product_meta['weight'] : '' }}" class="form-control" name="product_weight" id="product_weight">
                                                        <small>
                                                            <cite>
                                                                {{__("routes.Weight in decimal form")}}
                                                            </cite>
                                                        </small>
                                                    </div>

                                                    <hr>

                                                    <div class="form-group">
                                                        @php
                                                            $estimate_time = isset($product_meta['estimate_shipping']) ? $product_meta['estimate_shipping'] : null;

                                                            $prod_estimate_data = "";

                                                            if($estimate_time != null){
                                                               $prod_estimate_data = explode("-",$estimate_time);
                                                            }

                                                        @endphp

                                                        <label for="product_weight" class="form-check-label">{{__("routes.Shipping time")}}</label><br>

                                                        <div class="d-flex">
                                                            <input type="text"  value="{{isset($prod_estimate_data[0]) ? $prod_estimate_data[0] :null}}" class="form-control" name="product_shipping_time1" id="product_shipping_time1">
                                                            <label class="mt-1 mx-3">to</label>
                                                            <input type="text" value="{{isset($prod_estimate_data[1]) ? $prod_estimate_data[1] : null}}" class="form-control" name="product_shipping_time2" id="product_shipping_time2">
                                                        </div>
                                                        <small>
                                                            <cite>
                                                                {{__("routes.Showing estimated shipping time directly under each product")}} , <strong>like 2 to 5</strong>
                                                            </cite>
                                                        </small>
                                                    </div>


                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <!-- Product Others Section End-->

                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-3">

                                    <!-- Product Info Section Start-->
                                    <div class="card card-default">

                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Action")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                        class="fas fa-minus"></i></button>
                                            </div>
                                        </div>

                                        <div class="card-body pad">
                                            <div class="form-group text-left">
                                                <label for="attr_name" class="form-check-label">{{__("routes.Status")}}:</label>
                                                <select class="form-control" name="status">
                                                    <option
                                                        value="yes" {{ $product['status'] == 1 ? 'selected="selected"' : '' }}>
                                                        {{__("routes.Publish")}}
                                                    </option>
                                                    <option
                                                        value="no" {{ $product['status'] == 0 ? 'selected="selected"' : '' }}>
                                                        {{__("routes.Draft")}}
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group text-center">
                                                <input type="submit" class="btn btn-primary btn-sm w-75" value="{{__("routes.Update")}}">
                                            </div>
                                        </div>

                                        <!-- /.card-body -->
                                    </div>
                                    <!-- Product Info Section End-->

                                    <!-- Category Section Start-->
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Category")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                        class="fas fa-minus"></i></button>
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="select2-blue">

                                                    <select class="select2" name="categories[]" multiple="multiple"
                                                            data-placeholder="Select categories"
                                                            data-dropdown-css-class="select2-blue" style="width: 100%;">

                                                        @foreach($categories as $k => $category)
                                                            <option
                                                                value="{{ $category->id }}" {{ in_array($category->id  , array_values($product->categories->pluck('id')->toArray())) ? 'selected' : '' }} >
                                                                {{$category->Name}}
                                                            </option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- Category Section End-->

                                    <!-- Tags Section Start-->
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Tags")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                        class="fas fa-minus"></i></button>
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="select2-blue">
                                                    <select class="select2" name="tags[]" multiple="multiple"
                                                            data-placeholder="Select tags"
                                                            data-dropdown-css-class="select2-blue" style="width: 100%;">

                                                        @foreach($tags as $i => $tag)
                                                            <option
                                                                value="{{ $tag->id }}" {{ in_array($tag->id  , array_values($product->tags->pluck('id')->toArray())) ? 'selected' : '' }} > {{ $tag->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>


                                @php
                                    $user_id = $product->user_id;

                                    $role = \DB::table('model_has_roles')
                                            ->where('model_has_roles.model_id','=',$user_id )
                                            ->join('roles' , 'model_has_roles.role_id' , '=' , 'roles.id')
                                            ->select('roles.name')
                                            ->first();
                                @endphp

                                @if($role->name != "user")
                                    <!-- Artist Section Start -->
                                        <div class="card card-default">
                                            <div class="card-header">
                                                <h3 class="card-title">{{__("routes.Artists")}}</h3>
                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                            class="fas fa-minus"></i></button>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="form-group">
                                                    <select class="form-control @error('artists') is-invalid @enderror"
                                                            name="artists" id="artists">
                                                        <option value="">Select Option</option>
                                                        <option value="{{Auth::user()->id}}">Your Self</option>

                                                        @php
                                                            $metas = isset($product_meta['artist']) ? $product_meta['artist'] : "";
                                                            $prevs_artist ="";
                                                        @endphp

                                                        @forelse($artists as $key => $val)

                                                            @if(  $val->id == $metas )
                                                                <option value="{{ $val->id }}" selected> {{ $val->name }}</option>
                                                                @php
                                                                    $prevs_artist = $val->id;
                                                                @endphp
                                                            @else
                                                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                            @endif

                                                        @empty

                                                        @endforelse
                                                    </select>
                                                    <input type="hidden" name="prevs_artist" value="{{$prevs_artist}}">
                                                </div>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- Artist Section End -->
                                    @endif

                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Product Image")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                        class="fas fa-minus"></i></button>
                                            </div>
                                        </div>

                                        <div class="card-body pad">
                                            <div class="form-group">

                                                @if($product['image'] != 'null')

                                                    @php
                                                        $image = "";

                                                        $ff = @unserialize($product['image']);

                                                        if(!is_array($ff)){
                                                            $image = $ff;
                                                        }else{
                                                            $image = $product['image'];
                                                        }

                                                        $default_size = \App\SiteSetting::where(['key' => 'product_image_size'])->first();
                                                    @endphp


                                                    @if(!is_array($ff))

                                                        @if(!empty($product['image']))
                                                            <img ff="af" src="{{asset('storage/'.$product['image'])}}" id="product-img"
                                                                 style="width: 100%;height: 100%;"/>
                                                        @else
                                                            <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}" id="product-img" style="width: 100%;height: 100%;" />
                                                        @endif

                                                    @else
                                                        <img src="{{ asset('storage/'.$ff[$default_size->value])}}" style="width: 100%;height: 100%;">
                                                    @endif


                                                @else
                                                    <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"
                                                         id="product-img" style="width: 100%;height: 100%;"/>
                                                @endif

                                                <div class="custom-file mt-3">
                                                    <input type="file" class="custom-file-input" name="featured_img"
                                                           id="featured_img">
                                                    <label class="custom-file-label" for="customFile">{{__("routes.Choose Image")}}</label>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- /.card-body -->
                                    </div>

                                </div>
                            </div>
                            <!-- /.row -->
                        </form>

                        <div class="row">
                            <div class="col-md-9">

                                <!-- Product Variation -->
                                <div class="card card-default">
                                    <div class="card-header">
                                        <h3 class="card-title">
                                            {{__("routes.Variable Product Section")}}
                                        </h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                    class="fas fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">

                                        <div class="form-group">
                                            <label for="SelectProduct" class="form-check-label">{{__("routes.Name")}}:</label>
                                            <input type="text" name="attr_name" id="attr_name"
                                                   class="form-control @error('attr_name') is-invalid @enderror">
                                            <input type="hidden" name="attr_product_id" id="attr_product_id"
                                                   value="{{$product->id}}">
                                            @error('attr_name')
                                            <span class="invalid-feedback d-block" role="alert">
                                                          <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="SelectProduct" class="form-check-label">{{__("routes.Values")}}:</label>
                                            <input type="text"
                                                   name="attr_value"
                                                   id="attr_value"
                                                   class="form-control @error('attr_value') is-invalid @enderror"
                                                   placeholder="Enter some text, or some attributes by '|' separating values."
                                            />
                                            @error('attr_value')
                                            <span class="invalid-feedback d-block" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>

                                        <input type="button" value="Save Attribute" class="btn btn-primary btn-sm"
                                               id="create_attr_btn">
                                        <div class="form-group">
                                            <small>
                                                <cite>
                                                    {{__("routes.After saving attribute page will be refresh automatically")}}
                                                </cite>
                                            </small>
                                        </div>

                                        @if(!empty($attr))
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">

                                                    @forelse($attr as $key => $val)

                                                        <div id="accordion">
                                                            <div class="card">
                                                                <div class="card-header border-bottom-0">
                                                                    <h4 class="card-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion"
                                                                           href="#collapse_{{$key}}" class="collapsed"
                                                                           aria-expanded="false">
                                                                            {{ucfirst($val->attribute)}}
                                                                        </a>
                                                                    </h4>

                                                                    <div class="card-tools">
                                                                <span class="attr_remove text-red attr_btn_remove"
                                                                      attr_id="{{$val->id}}">{{__("routes.Remove")}}</span>
                                                                    </div>
                                                                </div>
                                                                <div id="collapse_{{$key}}" class="panel-collapse in collapse"
                                                                     style="border-top: 1px solid rgba(0,0,0,.125);">
                                                                    <div class="card-body">

                                                                        <div class="form-group">
                                                                            <label for="" class="form-check-label">{{__("routes.Name")}}:</label>
                                                                            <input type="text" name="edit_attr_name"
                                                                                   id="edit_attr_name"
                                                                                   class="form-control form-control-sm"
                                                                                   value="{{$val->attribute}}">
                                                                        </div>

                                                                        @php
                                                                            $attr_value = unserialize($val->value);
                                                                            $attr_value = implode("|" , $attr_value);
                                                                        @endphp

                                                                        <div class="form-group">
                                                                            <label for=""
                                                                                   class="form-check-label">{{__("routes.Values")}}:</label>
                                                                            <input type="text"
                                                                                   name="edit_attr_value"
                                                                                   id="edit_attr_value"
                                                                                   class="form-control form-control-sm"
                                                                                   placeholder="Enter some text, or some attributes by '|' separating values."
                                                                                   value="{{$attr_value}}"
                                                                            />
                                                                        </div>

                                                                        <input type="button"
                                                                               value="Update"
                                                                               class="btn btn-primary btn-sm edit_attr_btn"
                                                                               edit_attr_name=""
                                                                               edit_attr_value=""
                                                                               edit_attr_id="{{$val->id}}"
                                                                        >

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <hr>
                                                    @empty

                                                    @endforelse

                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-inline">
                                                        @forelse($attr as $key => $val)
                                                            <li class="list-inline-item">
                                                                <div class="form-group">
                                                                    @php
                                                                        $attr_value = unserialize($val->value);
                                                                    @endphp
                                                                    <label for=""
                                                                           class="form-check-label">{{ucfirst($val->attribute)}}
                                                                        :</label>
                                                                    <input type="hidden" class="variation_values"
                                                                           value="{{strtolower($val->attribute)}}">
                                                                    <select class="form-control-sm"
                                                                            name="variation_{{strtolower($val->attribute)}}"
                                                                            id="variation_{{strtolower($val->attribute)}}">
                                                                        @foreach($attr_value as $key => $val)
                                                                            <option
                                                                                value="{{strtolower($val)}}">{{ucfirst($val)}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </li>
                                                        @empty

                                                        @endforelse
                                                    </ul>
                                                </div>


                                                @if(!isset($variations))
                                                    <div class="col-md-12">
                                                        <input type="button" value="{{__("routes.Generate Variation")}}" id="generate-variation"
                                                               class="btn btn-primary btn-sm">
                                                    </div>

                                                    <hr>
                                                @endif

                                            </div>
                                        @endif


                                        @if(!empty($variations))

                                            @forelse($variations as $key => $value)

                                                @php
                                                    $attr_vari = unserialize($value->variation);
                                                @endphp

                                                <div id="accordion">
                                                    <div class="card">
                                                        <div class="card-header border-bottom-0">

                                                            <div class="custom-control custom-switch float-left">
                                                                <input type="hidden" class="variation_id" value="{{$value->id}}">
                                                                <input type="checkbox"
                                                                       class="custom-control-input variation-per-customSwitch"
                                                                       value="{{$value->id}}" id="customSwitch_{{$value->id}}"
                                                                       @if(isset($value->status) && $value->status == "on" ) checked
                                                                       @else uncheckeck @endif>
                                                                <label class="custom-control-label"
                                                                       for="customSwitch_{{$value->id}}"></label>
                                                            </div>

                                                            <h4 class="card-title">
                                                                <a data-toggle="collapse" data-parent="#accordion"
                                                                   href="#collapse_{{$value->id+0001222}}" class="collapsed"
                                                                   aria-expanded="false">
                                                                    #variation - {{$key+1}} --

                                                                    @foreach($attr_vari as $key => $v)
                                                                        <label for="attribute_{{ucfirst($key)}}"
                                                                               class="form-check-label text-black-50">
                                                                            {{ucfirst($v)}}
                                                                        </label>,
                                                                    @endforeach

                                                                </a>
                                                            </h4>

                                                            <div class="card-tools">
                                                        <span class="attr_remove text-red variation_btn_remove"
                                                              variation_id="{{$value->id}}">{{__("routes.Remove")}}</span>
                                                            </div>
                                                        </div>

                                                        <div id="collapse_{{$value->id+0001222}}"
                                                             class="panel-collapse in collapse"
                                                             style="border-top: 1px solid rgba(0,0,0,.125);">

                                                            <form id="form_10{{$value->id}}" method="post"
                                                                  enctype="multipart/form-data">
                                                                @csrf
                                                                <div class="card-body">

                                                                    <div class="form-group">
                                                                        @if($value->image != null)
                                                                            <img src="{{asset('storage/'.$value->image)}}" id="product_variation_thumbnail"
                                                                                 width="100" height="100"/>
                                                                        @else
                                                                            <img src="{{asset('images/placeholders/default-placeholder-600x600.png')}}"
                                                                                 id="product_variation_thumbnail"
                                                                                 width="100" height="100"/>
                                                                        @endif


                                                                        <div class="custom-file mt-3">
                                                                            <input type="file" name="variation_thumbnail"
                                                                                   id="variation_thumbnail"
                                                                                   class="variation_thumbnail custom-file-input"
                                                                            />
                                                                            <input type="hidden" name="prev_variation_thumbnail" value="{{$value->image}}">
                                                                            <label class="custom-file-label" for="customFile">{{__("routes.Choose Image")}}</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="" class="form-check-label">{{__("routes.Price")}}:</label>
                                                                        <input type="text"
                                                                               name="update_vari_price"
                                                                               id="update_vari_price"
                                                                               class="form-control form-control-sm"
                                                                               value="{{$value->price}}"
                                                                        />
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="" class="form-check-label">{{__("routes.Sale Price")}}:</label>
                                                                        <input type="number"
                                                                               name="update_vari_sale_price"
                                                                               id="update_vari_sale_price"
                                                                               class="form-control form-control-sm"
                                                                               value="{{$value->sale_price}}"
                                                                        />
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="" class="form-check-label">{{__("routes.Stock")}}:</label>
                                                                        <input type="number"
                                                                               name="update_vari_stock"
                                                                               id="update_vari_stock"
                                                                               class="form-control form-control-sm"
                                                                               value="{{$value->stock}}"
                                                                        />
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="" class="form-check-label">{{__("routes.Item number")}}(sku):</label>
                                                                        <input type="text"
                                                                               name="update_vari_sku"
                                                                               id="update_vari_sku"
                                                                               class="form-control form-control-sm"
                                                                               value="{{$value->sku}}"
                                                                        />
                                                                        <input type="hidden" value="{{$product->id}}" name="product_id">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for=""
                                                                               class="form-check-label">{{__("routes.Description")}}:</label>
                                                                        <textarea class="form-control form-control-sm"
                                                                                  name="update_vari_description"
                                                                                  id="update_vari_description" cols="30"
                                                                                  rows="5">{{$value->description}}</textarea>

                                                                    </div>

                                                                    <div class="form-group">

                                                                        <input type="submit"
                                                                               value="Update Variation"
                                                                               class="btn btn-primary btn-sm update_variation_btn"
                                                                               update_image=""
                                                                               update_price=""
                                                                               update_sale_price=""
                                                                               update_stock=""
                                                                               update_sku=""
                                                                               update_description=""
                                                                               update_variation_id="{{$value->id}}"
                                                                        />

                                                                    </div>

                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>

                                            @empty

                                            @endforelse
                                        @endif


                                    </div>

                                </div>
                                <!-- Product Variation End -->

                            </div>
                            <div class="col-md-3"></div>
                        </div>


                        @if(isset($reviews) && count($reviews) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Product Review-->
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">{{__("routes.Product Reviews")}}</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                    <i class="fas fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <button class="btn btn-danger btn-sm mb-3" id="delete-all-btn">{{__("routes.Trash All")}}</button>

                                            <table id="example1" class="table table-bordered table-striped product-review">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>S.No</th>
                                                    <th>{{__("routes.Users")}}</th>
                                                    <th>{{__("routes.Reviews")}}</th>
                                                    <th>{{__("routes.Rating")}}</th>
                                                    <th>{{__("routes.Date Time")}}</th>
                                                    @hasrole('admin')
                                                    <th>{{__("routes.Action")}}</th>
                                                    @endhasrole
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php $num = 1; @endphp
                                                @forelse($reviews as $key => $val)
                                                    @php
                                                        $user = App\User::find($val->user_id);
                                                        $photos = unserialize($val->image);
                                                    @endphp
                                                    <tr>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input class="custom-control-input" type="checkbox" name="faq"
                                                                       id="customCheckbox{{$key}}" value="{{$val->id}}">
                                                                <label for="customCheckbox{{$key}}"
                                                                       class="custom-control-label"></label>
                                                            </div>
                                                            {{--                                                <input type="checkbox" name="faq" id="checkbox" value="{{$faq->id}}">--}}
                                                        </td>
                                                        <td><p class="review-description">{{$num}}</p></td>
                                                        <td><p class="review-description">{{ucfirst($user->name)}}</p></td>
                                                        <td>
                                                            <p class="review-description">
                                                                {{$val->description}}
                                                            </p>
                                                            @forelse($photos as $key1 => $v)
                                                                <a href="{{asset('storage/'.$v)}}" data-lightbox="image-098">
                                                                    <img src="{{asset('storage/'.$v)}}" alt="review_image"
                                                                         class="custom-review-image ml-2 mr-2" width="30"
                                                                         height="30">
                                                                </a>
                                                            @empty

                                                            @endforelse
                                                        </td>
                                                        <td>
                                                            @switch($val->flag)

                                                                @case('poor')
                                                                <p class="review-description">1.0</p>

                                                                @break

                                                                @case('moderate')
                                                                <p class="review-description">2.0</p>

                                                                @break

                                                                @case('average')
                                                                <p class="review-description">3.0</p>

                                                                @break

                                                                @case('good')
                                                                <p class="review-description">4.0</p>

                                                                @break

                                                                @case('perfect')
                                                                <p class="review-description">5.0</p>

                                                                @break

                                                                @default
                                                                <li>0.0</li>

                                                        @endswitch
                                                        <td>
                                                            <p class="review-description">
                                                                {{\Carbon\Carbon::parse($val->created_at)->format('d/m/Y g:i a')}}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-switch">
                                                                <input type="hidden" class="review_id" value="{{$val->id}}">
                                                                <input type="checkbox"
                                                                       class="custom-control-input review-customSwitch"
                                                                       value="{{$val->id}}" id="customSwitch_{{$val->id}}"
                                                                       @if(isset($val->status) && $val->status == "on" ) checked
                                                                       @else uncheckeck @endif>
                                                                <label class="custom-control-label"
                                                                       for="customSwitch_{{$val->id}}"></label>
                                                            </div>
                                                            <a href="#_" class="custom-delete-btn btn btn-danger btn-sm"
                                                               review_id="{{$val->id}}"
                                                            ><i class="fa fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="5">{{__("routes.No Review Found")}}!</td>
                                                    </tr>
                                                @endforelse

                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- Product Review Section-->
                                </div>
                            </div>
                        @endif



                    </div>
                </div>


            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Delete single once-->
    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Review Confirmation")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("routes.You want to sure to delete this Review from your site")}}!</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>


    <!-- Delete more than once -->
    <div class="modal fade" id="delete-all-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-all-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="delete_ids" id="delete_ids" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Customer Review")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("route.You want to sure to delete all the selected reviews")}}!</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_all_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}
                        </button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection @section('page-script')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================


        $("#product_shipping_time1 , #product_shipping_time2").keydown(function (event) {


            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {

            } else {
                event.preventDefault();
            }

            if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
                event.preventDefault();

        });


        //=======================================================================================================

        $("input[id*='product_weight']").keydown(function (event) {


            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {

            } else {
                event.preventDefault();
            }

            if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
                event.preventDefault();

        });


        //=======================================================================================================
        var status = "";

        $('.review-customSwitch').click(function () {

            var customSwitch1 = $(this);
            var id = customSwitch1.val();
            console.log(id);
            var route = "{{route('product.review-status-update',['id' => 'id'])}}";
            route = route.replace('id', id);

            if ($(this).prop("checked") === true) {

                console.log("checked");
                status = "on";

                $.ajax({
                    context: this,
                    url: route,
                    type: 'post',
                    data: {"status": status},
                    success: function (response) {
                        //console.log(response+" - Mode is on");
                        toastr.success('Review is Enabled <strong>On</strong> !', 'Product Review Alert!');
                    }
                });
            } else if ($(this).prop("checked") === false) {

                console.log("Unchecked");
                status = "off";
                $.ajax({
                    context: this,
                    url: route,
                    type: 'post',
                    data: {"status": status},
                    success: function (response) {
                        //console.log(response+" - Mode is off");
                        toastr.error('Review is disabled <strong>Off</strong> !', 'Product Review Alert!');

                    }
                });
            }
        });

        //=======================================================================================================

        //Create product attribute
        $("#create_attr_btn").click(function () {

            var attr_name = $("#attr_name").val();
            var attr_value = $("#attr_value").val();
            var product_id = $("#attr_product_id").val();
            var route_url = "{{route('product.variations.create-attr')}}";

            if (attr_name !== "" && attr_value !== "") {

                $.ajax({
                    context: this,
                    url: route_url,
                    type: 'post',
                    data: {"attr_name": attr_name, "attr_value": attr_value, "product_id": product_id},
                    success: function (response) {
                        //console.log(response+" - Mode is on");
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                });

            } else {
                toastr.error("Attribute fields are required!", "Alert!");
            }

        });

        // =======================================================================================================

        //Edit product attribute

        $("input#edit_attr_name").each(function () {
            var value = $(this).val();
            $(this).parent().parent().find('input.edit_attr_btn').attr("edit_attr_name", value);
        });

        $("input#edit_attr_name").keyup(function (e) {
            var val = $(this).val();
            $(this).parent().parent().find('input.edit_attr_btn').attr("edit_attr_name", val);
            // val = val.replace(/[^\w]+/g, "");
            // $(".edit_attr_btn").val(val);
        });

        $("input#edit_attr_value").each(function () {
            var value = $(this).val();
            $(this).parent().parent().find('input.edit_attr_btn').attr("edit_attr_value", value);
        });

        $("input#edit_attr_value").keyup(function (e) {
            var val = $(this).val();
            $(this).parent().parent().find('input.edit_attr_btn').attr("edit_attr_value", val);
        });

        $(".edit_attr_btn").click(function () {
            var edit_attr_name = $(this).attr("edit_attr_name");
            var edit_attr_value = $(this).attr("edit_attr_value");
            var attr_ID = $(this).attr("edit_attr_id");
            var route_url = "{{route('product.variations.update-attr',['id' , 'id'])}}";
            route_url = route_url.replace('id', attr_ID);

            $.ajax({
                context: this,
                url: route_url,
                type: 'post',
                data: {attr_name: edit_attr_name, attr_value: edit_attr_value},
                success: function (response) {
                    toastr.success("Attribute updated successfully!", "Alert!");
                    setTimeout(function () {
                        location.reload();
                    }, 500);
                },
                error: function (data) {
                    if (edit_attr_name == "") {
                        toastr.error(data.responseJSON.errors.attr_name, "Attribute Name Alert!");
                    }
                    if (edit_attr_value == "") {
                        toastr.error(data.responseJSON.errors.attr_value, "Attribute Value Alert!");
                    }
                }
            });


        });


        // =======================================================================================================

        //Delete product attribute
        $(".attr_btn_remove").click(function () {

            var attr_id = $(this).attr("attr_id");
            var route_url = "{{route('product.variations.delete-attr',['id' , 'id'])}}";
            route_url = route_url.replace('id', attr_id);

            $.ajax({
                context: this,
                url: route_url,
                type: 'post',
                data: {attr_id: attr_id},
                success: function (response) {
                    toastr.success("Attribute successfully deleted!", "Attribute Alert");
                    setTimeout(function () {
                        location.reload();
                    }, 500);
                }
            });

        });

        // =======================================================================================================

        //get variation
        var attr_name = [];
        $(".variation_values").each(function () {
            attr_name.push({name: this.value})
        });

        //Create Variations
        $("#generate-variation").click(function () {

            var product_id = $("#attr_product_id").val();
            var single_variation = [];

            $.each(attr_name, function (index, val) {
                single_variation.push({
                    attribute: val.name,
                    variation: $("#variation_" + val.name + " option:selected").val()
                });
            });

            console.log(single_variation);
            var route_url = "{{route('product.variations.create')}}";

            $.ajax({
                context: this,
                url: route_url,
                type: 'post',
                data: {singleVariation: single_variation, product_id: product_id},
                success: function (response) {
                    toastr.success("Variation created successfully!", "Alert!");
                    // setTimeout(function(){
                    //     location.reload();
                    // },500);
                },
                error:function(err){
                    toastr.error(err.responseJSON[0].msg, "Alert!");
                }
            });

        });

        // =======================================================================================================

        //Edit Varitaions
        $("input#variation_thumbnail").each(function () {
            var value = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_image", value);
        });

        $("input#update_vari_price").each(function () {
            var value = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_price", value);
        });

        $("input#update_vari_price").keyup(function (e) {
            var val = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_price", val);
        });

        $("input#update_vari_sale_price").each(function () {
            var value = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_sale_price", value);
        });

        $("input#update_vari_sale_price").keyup(function (e) {
            var val = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_sale_price", val);
        });

        $("input#update_vari_stock").each(function () {
            var value = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_stock", value);
        });

        $("input#update_vari_stock").keyup(function (e) {
            var val = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_stock", val);
        });

        $("input#update_vari_sku").each(function () {
            var value = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_sku", value);
        });

        $("input#update_vari_sku").keyup(function (e) {
            var val = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_sku", val);
        });

        $("textarea#update_vari_description").each(function () {
            var value = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_description", value);
        });

        $("textarea#update_vari_description").keyup(function (e) {
            var val = $(this).val();
            $(this).parent().parent().find('input.update_variation_btn').attr("update_description", val);
        });

        $(".update_variation_btn").click(function (e) {
            e.preventDefault();

            var price = $(this).attr("update_price");
            var id = $(this).attr("update_variation_id");
            var route_url = "{{route('product.variations.update',['id' , 'id'])}}";
            route_url = route_url.replace('id', id);
            //alert(image +" , "+ price +" , "+ sale_price +" , "+ stock +" , "+ sku +" , "+ description);

            var formData = new FormData($(this).parent().parent().parent()[0]);
            console.log(formData);

            $.ajax({
                url: route_url,
                type: 'POST',
                data: formData,
                dataType: 'JSON',
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    console.log(result);
                    toastr.success("Variation updated successfully!","Alert!");
                    location.reload();
                },
                error: function (data) {

                    console.log(data.responseJSON.errors.price);
                    if(price === ""){
                        toastr.error(data.responseJSON.errors.update_vari_price, "Variation Alert!");
                    }

                }
            });


        });

        // =======================================================================================================

        //enable or disable variation
        var variation_status = "";

        $('.variation-per-customSwitch').click(function () {

            var customSwitch1 = $(this);
            var id            = customSwitch1.val();
            var route         = "{{route('product.variations.status-update',['id' => 'id'])}}";
            route             = route.replace('id', id);

            if ($(this).prop("checked") === true) {

                //console.log("checked");
                variation_status = "on";

                $.ajax({
                    context: this,
                    url: route,
                    type: 'post',
                    data: {"status": variation_status},
                    success: function (response) {
                        //console.log(response+" - Mode is on");
                        toastr.success('Variation is enabled <strong>On</strong> !', 'Product Variation Alert!');
                    }
                });
            } else if ($(this).prop("checked") === false) {

                //console.log("Unchecked");
                variation_status = "off";
                $.ajax({
                    context: this,
                    url: route,
                    type: 'post',
                    data: {"status": variation_status},
                    success: function (response) {
                        //console.log(response+" - Mode is off");
                        toastr.error('Variation is disabled <strong>Off</strong> !', 'Product Variation Alert!');

                    }
                });
            }
        });


        // =======================================================================================================

        //Delete variation
        $(".variation_btn_remove").click(function () {

            var vari_id = $(this).attr("variation_id");
            var route_url = "{{route('product.variations.delete',['id' , 'id'])}}";
            route_url = route_url.replace('id', vari_id);

            $.ajax({
                context: this,
                url: route_url,
                type: 'post',
                data: {vari_id: vari_id},
                success: function (response) {
                    toastr.success("Variation successfully deleted!", "Variation Alert");
                    setTimeout(function () {
                        location.reload();
                    }, 500);
                }
            });

        });

        // =======================================================================================================

        // product variation thumbnail
        $(".variation_thumbnail").change(function () {
            var cc = $(this);
            if (this.files && this.files[0]) {

                var reader = new FileReader();
                var file = "";
                reader.onload = function (e) {
                    cc.parent().parent().find('#product_variation_thumbnail').attr('src', e.target.result);
                    file = e.target.result;
                }

                console.log(this.files[0].name);
                reader.readAsDataURL(this.files[0]);
            }
        });


        // =======================================================================================================

        $("#delete-all-btn").on('click', function () {
            var ids = [];

            var route = '{{ route('product.review-delete-all') }}';

            $("#delete-all-modal").modal('show');
            $("#delete-all-modal-form").attr("action", route);

            $.each($("input[name='faq']:checked"), function () {
                ids.push($(this).val());
            });
            console.log(ids);
            $("#delete_ids").attr("value", JSON.stringify(ids));

            $('.delete_all_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-all-modal-form").submit();
            });

        });


        //=======================================================================================================

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("review_id");
            var route = '{{ route('product.review-delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            });

        });

        //=======================================================================================================


        $(function () {

            $('#example1').DataTable({
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "pageLength": 7,
                "autoWidth": false,
            });


            //Initialize Select2 Elements
            $('.select2').select2()

            //Date range picker
            $('#reservation').daterangepicker();
            var date = $("#reservation").val();

            // Summernote
            $('.textarea').summernote();



            $("#custom-sales-schedule").css("display", "none");

            if($("#manage_stock_checked").props('checked')){
                $("#custom-manage-stock-section").css("display", "block");
            }else if($("#manage_stock_checked").props('unchecked')){
                $("#custom-manage-stock-section").css("display", "none");
            }else{

            }

            $("#custom-attribute-section").css("display", "none");

            $("#sales_schedule").on('click', function () {
                $("#custom-sales-schedule").css("display", "block");
            });

            $("#cancel_sales_schedule").on('click', function () {
                $("#custom-sales-schedule").css("display", "none");
            });



            $("#manage_stock_checked").click(function () {

                if ($(this).is(":checked")) {
                    $("#custom-manage-stock-section").css("display", "block");
                } else {
                    $("#custom-manage-stock-section").css("display", "none");
                }

            });

            $("#add_attribute").on('click', function () {
                $("#custom-attribute-section").css("display", "block");
            });

            $("#cancel-add-attribute-section").on('click', function () {
                $("#custom-attribute-section").css("display", "none");
            });

        })

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#profile-img").change(function () {
            readURL(this);
        });

        // $('[data-toggle=confirmation]').confirmation({
        //     rootSelector: '[data-toggle=confirmation]',
        //     // other options
        // });

        $("#file-1").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'gif', 'mp4'],
            overwriteInitial: false,
            maxFileSize: 100000,
            maxFilesNum: 10,
            showClose: false,
            showUpload: false,
            showCancel: false,
            showZoom: true,

            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            @isset($product_gallery)
            initialPreviewConfig: [
                    @foreach($product_gallery as $k=>$v)
                {
                    url: "{{route('product.photodelete')}}",
                    _token: '{{csrf_token()}}',
                    key: '{{$v->id}}'
                },
                @endforeach
            ],
            deleteExtraData: {
                '_token': '{{csrf_token()}}',
            },
            initialPreview: [
                @foreach($product_gallery as $k=>$v)
                    '<img src="{{asset('storage/'.$v->image)}}" class="kv-preview-data file-preview-image">',
                @endforeach
            ],
            @endisset

            // 'showUpload':false,
            // 'previewFileType':'image'}).on('filebeforedelete', function() {
            // return new Promise(function (resolve, reject) {
            //     $.confirm({
            //         title: 'Confirmation!',
            //         content: 'Are you sure you want to delete this file?',
            //         type: 'red',
            //         buttons: {
            //             ok: {
            //                 btnClass: 'btn-primary text-white',
            //                 keys: ['enter'],
            //                 action: function () {
            //                     resolve();
            //                 }
            //             },
            //             cancel: function () {
            //
            //             }
            //         }
            //     });
            // });


//        });

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        $("#video").fileinput({
            theme: 'fas',
            allowedFileTypes: ["video"],
            allowedFileExtensions: ['mp3', '.webm', 'mp4'],
            overwriteInitial: false,
            maxFileSize: 100000,
            maxFilesNum: 1,
            showClose: false,
            showUpload: false,
            showCancel: false,
            showZoom: true,
            @isset($product_meta['product_video'])
            initialPreview: [
                '<a href="{{url('/storage/'.$product_meta['product_video'])}}" class="playBTN"><img src="{{ asset('front-end/assets/img/youTube.png')}}" alt=""></a>'
            ],
            @endisset

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#product-img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#featured_img").change(function () {
            readURL(this);
            console.log(readURL(this));
        });


    </script>

@endsection
