@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Customers List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Users</a></li>
                            <li class="breadcrumb-item active">Customers</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="card">

                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-striped table-bordered nowrap ">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Phone No</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    <th>Created At</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($customers as $key => $customer)
                                        @php
                                           $meta = getAllUserMetas($customer->id);
                                        @endphp

                                        <tr>
                                            <td>{{ $key+1}}</td>
                                            <td>{{ $customer->id}}</td>
                                            <td>{{ ucfirst($customer->name)  }}</td>
                                            <td>{{ $customer->email }}</td>
                                            <td>{{ isset($meta['telno']) ? $meta['telno'] : "Not Found"  }}</td>
                                            <td>{{ isset($meta['city']) ? $meta['city'] : "Not Found" }}</td>
                                            <td>{{ isset($meta['country']) ? $meta['country'] : "Not Found" }}</td>
                                            <td>{{ \Carbon\Carbon::parse($customer->created_at)->format("Y-m-d g:ia") }}</td>
                                            <td>
                                                <a href="#" class="btn btn-info btn-sm mr-3"><i class="fa fa-edit"></i></a>
                                                <a href="#" class="custom-delete-btn btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')
    <script>
        $(function () {
            // $("#example1").DataTable();
            $('#example1').DataTable({
                "paging": true,
                "responsive": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,

            });
        });
    </script>

@endsection
