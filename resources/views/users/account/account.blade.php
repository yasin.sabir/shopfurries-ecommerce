@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__("routes.Account")}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.User")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('user.account')}}">{{__("routes.Account")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Account Details")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-12 mt-2 mb-3">
                                        <h4>{{__("routes.My Profile")}}</h4>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="{{asset(profile_pic(Auth::user()->id))}}" id="thumbnail-tag"
                                             width="100" height="100"/>
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4"></div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-4">
                                        <label for="CategoryThumbnail">{{__("routes.Full name")}}:</label>
                                        <span>{{ isset($user->name) ? $user->name : ""}}</span>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="CategoryThumbnail">{{__("routes.Email")}}:</label>
                                        <span>{{ (  strpos($user->email,'sample-deviantart.com') !== false )? '' : $user->email}}</span>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="CategoryThumbnail">{{__("routes.Phone No")}}:</label>
                                        <span>{{ isset($user_meta['telno']) ? $user_meta['telno'] : ""}}</span>
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-4">
                                        <label for="CategoryThumbnail">{{__("routes.Country")}}:</label>
                                        <span>{{ isset($user_meta['country']) ? $user_meta['country'] : ""}}</span>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="CategoryThumbnail">{{__("routes.City")}}:</label>
                                        <span>{{ isset($user_meta['city']) ? $user_meta['city'] : ""}}</span>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="CategoryThumbnail">{{__("routes.Street")}}:</label>
                                        @if( isset($user_meta['street_name']) ||  isset($user_meta['street_no']))
                                            <span>{{$user_meta['street_name']}} - {{$user_meta['street_no']}}</span>
                                        @else
                                            <span> </span>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 mt-2 mb-3">
                                        <h4>{{__("routes.My Address Book")}}</h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="CategoryThumbnail">{{__("routes.Shipping Address")}}:</label>

                                    </div>

                                    <div class="col-md-6">
                                        <label for="CategoryThumbnail">{{__("routes.Billing Address")}}:</label>

                                    </div>
                                </div>

                                <hr>
                                <div class="row mt-4">
                                    <div class="col-md-4">
                                        <a href="{{route('user.edit-account',encrypt($user->id))}}" class="btn btn-primary">{{__("routes.Edit Details")}}</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });
    </script>

@endsection
