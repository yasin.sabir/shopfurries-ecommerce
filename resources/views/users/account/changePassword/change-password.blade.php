@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__("routes.Account")}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.User")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('user.changePassword')}}">{{__("routes.Change Password")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Change Password")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{route('user.update-password',Auth::user()->id)}}" method="post">
                                    @csrf

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="">{{__("routes.New Password")}}:</label>
                                            <input type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" id="new_password">
                                                @error('new_password')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-6">
                                            <label for="">{{__("routes.Confirm Password")}}:</label>
                                            <input type="password" class="form-control @error('confirm_passwords') is-invalid @enderror" name="confirm_password" id="confirm_password">
                                                @error('confirm_passwords')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row mt-4">
                                        <div class="col-md-4">
                                            <input type="submit" class="btn btn-primary" value="{{__("routes.Update Password")}}">
                                        </div>
                                    </div>

                                </form>



                            </div>
                        </div>

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script>

    </script>

@endsection
