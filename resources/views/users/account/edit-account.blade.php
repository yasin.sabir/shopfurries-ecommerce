@extends('layouts.backend.app')

@section('page-css')
    <style>
        button.kv-file-remove.btn.btn-sm.btn-kv.btn-default.btn-outline-secondary {
            display: none;
        }
    </style>
@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__("routes.Edit")}} {{__("routes.Account")}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.User")}}</li>
                            <li class="breadcrumb-item">{{__("routes.Account")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('user.edit-account',$user->id)}}">{{__("routes.Edit")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Account Details")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <form action="{{route('user.update-account' , $user->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12 mt-2 mb-3">
                                            <h4>{{__("routes.My Profile")}}</h4>
                                        </div>

                                        <div class="col-md-6 pr-lg-5">
                                            <div class="form-group">
                                                <label for="full_name">{{__("routes.Full name")}}:</label>
                                                <input type="text" value="{{ isset($user->name) ? $user->name : ""}}" class="form-control" name="user_fullname" id="user_fullname">
                                                @error('user_fullname')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="email">{{__("routes.Email")}}:</label>
                                                <input type="text" value="{{(strpos($user->email,'sample-deviantart.com') !== false )? '' : $user->email }}" class="form-control" name="user_email" id="user_email">
                                                @error('user_email')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="tel_no">{{__("routes.Phone No")}}:</label>
                                                <input type="text" value="{{ isset($user_meta['telno']) ? $user_meta['telno'] : ""}}" class="form-control" name="user_telno" id="user_telno">
                                                @error('user_telno')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="country">{{__("routes.Country")}}:</label>
                                                <select class="form-control" name="user_country">
                                                    <option value>Select Country</option>

                                                    @php
                                                       if(isset($user_meta['country'])){
                                                            $user_meta['country'] = $user_meta['country'];
                                                       }else{
                                                            $user_meta['country'] = "";
                                                       }
                                                    @endphp


                                                    <option value="Afghanistan" @if($user_meta['country'] == "Afghanistan" )selected @endif>Afghanistan</option>
                                                    <option value="Åland Islands" @if($user_meta['country'] == "Åland Islands" )selected @endif>Åland Islands</option>
                                                    <option value="Albania" @if($user_meta['country'] == "Albania" )selected @endif>Albania</option>
                                                    <option value="Algeria" @if($user_meta['country'] == "Algeria" )selected @endif>Algeria</option>
                                                    <option value="American Samoa" @if($user_meta['country'] == "American Samoa" )selected @endif>American Samoa</option>
                                                    <option value="Andorra" @if($user_meta['country'] == "Andorra" )selected @endif>Andorra</option>
                                                    <option value="Angola" @if($user_meta['country'] == "Angola" )selected @endif>Angola</option>
                                                    <option value="Anguilla" @if($user_meta['country'] == "Anguilla" )selected @endif>Anguilla</option>
                                                    <option value="Antarctica" @if($user_meta['country'] == "Antarctica" )selected @endif>Antarctica</option>
                                                    <option value="Antigua and Barbuda" @if($user_meta['country'] == "Antigua and Barbuda" )selected @endif>Antigua and Barbuda</option>
                                                    <option value="Argentina" @if($user_meta['country'] == "Argentina" )selected @endif>Argentina</option>
                                                    <option value="Armenia" @if($user_meta['country'] == "Armenia" )selected @endif>Armenia</option>
                                                    <option value="Aruba" @if($user_meta['country'] == "Aruba" )selected @endif>Aruba</option>
                                                    <option value="Australia" @if($user_meta['country'] == "Australia" )selected @endif>Australia</option>
                                                    <option value="Austria" @if($user_meta['country'] == "Austria" )selected @endif>Austria</option>
                                                    <option value="Azerbaijan" @if($user_meta['country'] == "Azerbaijan" )selected @endif>Azerbaijan</option>
                                                    <option value="Bahamas" @if($user_meta['country'] == "Bahamas" )selected @endif>Bahamas</option>
                                                    <option value="Bahrain" @if($user_meta['country'] == "Bahrain" )selected @endif>Bahrain</option>
                                                    <option value="Bangladesh" @if($user_meta['country'] == "Bangladesh" )selected @endif>Bangladesh</option>
                                                    <option value="Barbados" @if($user_meta['country'] == "Barbados" )selected @endif>Barbados</option>
                                                    <option value="Belarus" @if($user_meta['country'] == "Belarus" )selected @endif>Belarus</option>
                                                    <option value="Belgium" @if($user_meta['country'] == "Belgium" )selected @endif>Belgium</option>
                                                    <option value="Belize" @if($user_meta['country'] == "Belize" )selected @endif>Belize</option>
                                                    <option value="Benin" @if($user_meta['country'] == "Benin" )selected @endif>Benin</option>
                                                    <option value="Bermuda" @if($user_meta['country'] == "Bermuda" )selected @endif>Bermuda</option>
                                                    <option value="Bhutan" @if($user_meta['country'] == "Bhutan" )selected @endif>Bhutan</option>
                                                    <option value="Bolivia" @if($user_meta['country'] == "Bolivia" )selected @endif>Bolivia</option>
                                                    <option value="Bosnia and Herzegovina" @if($user_meta['country'] == "Bosnia and Herzegovina" )selected @endif>Bosnia and Herzegovina</option>
                                                    <option value="Botswana" @if($user_meta['country'] == "Botswana" )selected @endif>Botswana</option>
                                                    <option value="Bouvet Island" @if($user_meta['country'] == "Bouvet Island" )selected @endif>Bouvet Island</option>
                                                    <option value="Brazil" @if($user_meta['country'] == "Brazil" )selected @endif>Brazil</option>
                                                    <option value="British Indian Ocean Territory" @if($user_meta['country'] == "British Indian Ocean Territory" )selected @endif>British Indian Ocean Territory</option>
                                                    <option value="Brunei Darussalam" @if($user_meta['country'] == "Brunei Darussalam" )selected @endif>Brunei Darussalam</option>
                                                    <option value="Bulgaria" @if($user_meta['country'] == "Bulgaria" )selected @endif>Bulgaria</option>
                                                    <option value="Burkina Faso" @if($user_meta['country'] == "Burkina Faso" )selected @endif>Burkina Faso</option>
                                                    <option value="Burundi" @if($user_meta['country'] == "Burundi" )selected @endif>Burundi</option>
                                                    <option value="Cambodia" @if($user_meta['country'] == "Cambodia" )selected @endif>Cambodia</option>
                                                    <option value="Cameroon" @if($user_meta['country'] == "Cameroon" )selected @endif>Cameroon</option>
                                                    <option value="Canada" @if($user_meta['country'] == "Canada" )selected @endif>Canada</option>
                                                    <option value="Cape Verde" @if($user_meta['country'] == "Cape Verde" )selected @endif>Cape Verde</option>
                                                    <option value="Cayman Islands" @if($user_meta['country'] == "Cayman Islands" )selected @endif>Cayman Islands</option>
                                                    <option value="Central African Republic" @if($user_meta['country'] == "Central African Republic" )selected @endif>Central African Republic</option>
                                                    <option value="Chad" @if($user_meta['country'] == "Chad" )selected @endif>Chad</option>
                                                    <option value="Chile" @if($user_meta['country'] == "Chile" )selected @endif>Chile</option>
                                                    <option value="China" @if($user_meta['country'] == "China" )selected @endif>China</option>
                                                    <option value="Christmas Island" @if($user_meta['country'] == "Christmas Island" )selected @endif>Christmas Island</option>
                                                    <option value="Cocos (Keeling) Islands" @if($user_meta['country'] == "Cocos (Keeling) Islands" )selected @endif>Cocos (Keeling) Islands</option>
                                                    <option value="Colombia" @if($user_meta['country'] == "Colombia" )selected @endif>Colombia</option>
                                                    <option value="Comoros" @if($user_meta['country'] == "Comoros" )selected @endif>Comoros</option>
                                                    <option value="Congo" @if($user_meta['country'] == "Congo" )selected @endif>Congo</option>
                                                    <option value="Congo, The Democratic Republic of The" @if($user_meta['country'] == "Congo, The Democratic Republic of The" )selected @endif>Congo, The Democratic Republic of The</option>
                                                    <option value="Cook Islands" @if($user_meta['country'] == "Cook Islands" )selected @endif>Cook Islands</option>
                                                    <option value="Costa Rica" @if($user_meta['country'] == "Costa Rica" )selected @endif>Costa Rica</option>
                                                    <option value="Cote D'ivoire" @if($user_meta['country'] == "Cote D'ivoire" )selected @endif>Cote D'ivoire</option>
                                                    <option value="Croatia" @if($user_meta['country'] == "Croatia" )selected @endif>Croatia</option>
                                                    <option value="Cuba" @if($user_meta['country'] == "Cuba" )selected @endif>Cuba</option>
                                                    <option value="Cyprus" @if($user_meta['country'] == "Cyprus" )selected @endif>Cyprus</option>
                                                    <option value="Czech Republic" @if($user_meta['country'] == "Czech Republic" )selected @endif>Czech Republic</option>
                                                    <option value="Denmark" @if($user_meta['country'] == "Denmark" )selected @endif>Denmark</option>
                                                    <option value="Djibouti" @if($user_meta['country'] == "Djibouti" )selected @endif>Djibouti</option>
                                                    <option value="Dominica" @if($user_meta['country'] == "Dominica" )selected @endif>Dominica</option>
                                                    <option value="Dominican Republic" @if($user_meta['country'] == "Dominican Republic" )selected @endif>Dominican Republic</option>
                                                    <option value="Ecuador" @if($user_meta['country'] == "Ecuador" )selected @endif>Ecuador</option>
                                                    <option value="Egypt" @if($user_meta['country'] == "Egypt" )selected @endif>Egypt</option>
                                                    <option value="El Salvador" @if($user_meta['country'] == "El Salvador" )selected @endif>El Salvador</option>
                                                    <option value="Equatorial Guinea" @if($user_meta['country'] == "Equatorial Guinea" )selected @endif>Equatorial Guinea</option>
                                                    <option value="Eritrea" @if($user_meta['country'] == "Eritrea" )selected @endif>Eritrea</option>
                                                    <option value="Estonia" @if($user_meta['country'] == "Estonia" )selected @endif>Estonia</option>
                                                    <option value="Ethiopia" @if($user_meta['country'] == "Ethiopia" )selected @endif>Ethiopia</option>
                                                    <option value="Falkland Islands (Malvinas)" @if($user_meta['country'] == "Falkland Islands (Malvinas)" )selected @endif>Falkland Islands (Malvinas)</option>
                                                    <option value="Faroe Islands" @if($user_meta['country'] == "Faroe Islands" )selected @endif>Faroe Islands</option>
                                                    <option value="Fiji" @if($user_meta['country'] == "Fiji" )selected @endif>Fiji</option>
                                                    <option value="Finland" @if($user_meta['country'] == "Finland" )selected @endif>Finland</option>
                                                    <option value="France" @if($user_meta['country'] == "France" )selected @endif>France</option>
                                                    <option value="French Guiana" @if($user_meta['country'] == "French Guiana" )selected @endif>French Guiana</option>
                                                    <option value="French Polynesia" @if($user_meta['country'] == "French Polynesia" )selected @endif>French Polynesia</option>
                                                    <option value="French Southern Territories" @if($user_meta['country'] == "French Southern Territories" )selected @endif>French Southern Territories</option>
                                                    <option value="Gabon" @if($user_meta['country'] == "Gabon" )selected @endif>Gabon</option>
                                                    <option value="Gambia" @if($user_meta['country'] == "Gambia" )selected @endif>Gambia</option>
                                                    <option value="Georgia" @if($user_meta['country'] == "Georgia" )selected @endif>Georgia</option>
                                                    <option value="Germany" @if($user_meta['country'] == "Germany" )selected @endif>Germany</option>
                                                    <option value="Ghana" @if($user_meta['country'] == "Ghana" )selected @endif>Ghana</option>
                                                    <option value="Gibraltar" @if($user_meta['country'] == "Gibraltar" )selected @endif>Gibraltar</option>
                                                    <option value="Greece" @if($user_meta['country'] == "Greece" )selected @endif>Greece</option>
                                                    <option value="Greenland" @if($user_meta['country'] == "Greenland" )selected @endif>Greenland</option>
                                                    <option value="Grenada" @if($user_meta['country'] == "Grenada" )selected @endif>Grenada</option>
                                                    <option value="Guadeloupe" @if($user_meta['country'] == "Guadeloupe" )selected @endif>Guadeloupe</option>
                                                    <option value="Guam" @if($user_meta['country'] == "Guam" )selected @endif>Guam</option>
                                                    <option value="Guatemala" @if($user_meta['country'] == "Guatemala" )selected @endif>Guatemala</option>
                                                    <option value="Guernsey" @if($user_meta['country'] == "Guernsey" )selected @endif>Guernsey</option>
                                                    <option value="Guinea" @if($user_meta['country'] == "Guinea" )selected @endif>Guinea</option>
                                                    <option value="Guinea-bissau" @if($user_meta['country'] == "Guinea-bissau" )selected @endif>Guinea-bissau</option>
                                                    <option value="Guyana" @if($user_meta['country'] == "Guyana" )selected @endif>Guyana</option>
                                                    <option value="Haiti" @if($user_meta['country'] == "Haiti" )selected @endif>Haiti</option>
                                                    <option value="Heard Island and Mcdonald Islands" @if($user_meta['country'] == "Heard Island and Mcdonald Islands" )selected @endif>Heard Island and Mcdonald Islands</option>
                                                    <option value="Holy See (Vatican City State)" @if($user_meta['country'] == "Holy See (Vatican City State)" )selected @endif>Holy See (Vatican City State)</option>
                                                    <option value="Honduras" @if($user_meta['country'] == "Honduras" )selected @endif>Honduras</option>
                                                    <option value="Hong Kong" @if($user_meta['country'] == "Hong Kong" )selected @endif>Hong Kong</option>
                                                    <option value="Hungary" @if($user_meta['country'] == "Hungary" )selected @endif>Hungary</option>
                                                    <option value="Iceland" @if($user_meta['country'] == "Iceland" )selected @endif>Iceland</option>
                                                    <option value="India" @if($user_meta['country'] == "India" )selected @endif>India</option>
                                                    <option value="Indonesia" @if($user_meta['country'] == "Indonesia" )selected @endif>Indonesia</option>
                                                    <option value="Iran" @if($user_meta['country'] == "Iran" )selected @endif>Iran, Islamic Republic of</option>
                                                    <option value="Iraq" @if($user_meta['country'] == "Iraq" )selected @endif>Iraq</option>
                                                    <option value="Ireland" @if($user_meta['country'] == "Ireland" )selected @endif>Ireland</option>
                                                    <option value="Isle of Man" @if($user_meta['country'] == "Isle of Man" )selected @endif>Isle of Man</option>
                                                    <option value="Israel" @if($user_meta['country'] == "Israel" )selected @endif>Israel</option>
                                                    <option value="Italy" @if($user_meta['country'] == "Italy" )selected @endif>Italy</option>
                                                    <option value="Jamaica" @if($user_meta['country'] == "Jamaica" )selected @endif>Jamaica</option>
                                                    <option value="Japan" @if($user_meta['country'] == "Japan" )selected @endif>Japan</option>
                                                    <option value="Jersey" @if($user_meta['country'] == "Jersey" )selected @endif>Jersey</option>
                                                    <option value="Jordan" @if($user_meta['country'] == "Jordan" )selected @endif>Jordan</option>
                                                    <option value="Kazakhstan" @if($user_meta['country'] == "Kazakhstan" )selected @endif>Kazakhstan</option>
                                                    <option value="Kenya" @if($user_meta['country'] == "Kenya" )selected @endif>Kenya</option>
                                                    <option value="Kiribati" @if($user_meta['country'] == "Kiribati" )selected @endif>Kiribati</option>
                                                    <option value="Korea" @if($user_meta['country'] == "Korea" )selected @endif>Korea</option>
                                                    <option value="Kuwait" @if($user_meta['country'] == "Kuwait" )selected @endif>Kuwait</option>
                                                    <option value="Kyrgyzstan" @if($user_meta['country'] == "Kyrgyzstan" )selected @endif>Kyrgyzstan</option>
                                                    <option value="Lao People's Democratic Republic" @if($user_meta['country'] == "Lao People's Democratic Republic" )selected @endif>Lao People's Democratic Republic</option>
                                                    <option value="Latvia" @if($user_meta['country'] == "Latvia" )selected @endif>Latvia</option>
                                                    <option value="Lebanon" @if($user_meta['country'] == "Lebanon" )selected @endif>Lebanon</option>
                                                    <option value="Lesotho" @if($user_meta['country'] == "Lesotho" )selected @endif>Lesotho</option>
                                                    <option value="Liberia" @if($user_meta['country'] == "Liberia" )selected @endif>Liberia</option>
                                                    <option value="Libyan Arab Jamahiriya" @if($user_meta['country'] == "Libyan Arab Jamahiriya" )selected @endif>Libyan Arab Jamahiriya</option>
                                                    <option value="Liechtenstein" @if($user_meta['country'] == "Liechtenstein" )selected @endif>Liechtenstein</option>
                                                    <option value="Lithuania" @if($user_meta['country'] == "Lithuania" )selected @endif>Lithuania</option>
                                                    <option value="Luxembourg" @if($user_meta['country'] == "Luxembourg" )selected @endif>Luxembourg</option>
                                                    <option value="Macao" @if($user_meta['country'] == "Macao" )selected @endif>Macao</option>
                                                    <option value="Macedonia" @if($user_meta['country'] == "Macedonia" )selected @endif>Macedonia</option>
                                                    <option value="Madagascar" @if($user_meta['country'] == "Madagascar" )selected @endif>Madagascar</option>
                                                    <option value="Malawi" @if($user_meta['country'] == "Malawi" )selected @endif>Malawi</option>
                                                    <option value="Malaysia" @if($user_meta['country'] == "Malaysia" )selected @endif>Malaysia</option>
                                                    <option value="Maldives" @if($user_meta['country'] == "Maldives" )selected @endif>Maldives</option>
                                                    <option value="Mali" @if($user_meta['country'] == "Mali" )selected @endif>Mali</option>
                                                    <option value="Malta" @if($user_meta['country'] == "Malta" )selected @endif>Malta</option>
                                                    <option value="Marshall Islands" @if($user_meta['country'] == "Marshall Islands" )selected @endif>Marshall Islands</option>
                                                    <option value="Martinique" @if($user_meta['country'] == "Martinique" )selected @endif>Martinique</option>
                                                    <option value="Mauritania" @if($user_meta['country'] == "Mauritania" )selected @endif>Mauritania</option>
                                                    <option value="Mauritius" @if($user_meta['country'] == "Mauritius" )selected @endif>Mauritius</option>
                                                    <option value="Mayotte" @if($user_meta['country'] == "Mayotte" )selected @endif>Mayotte</option>
                                                    <option value="Mexico" @if($user_meta['country'] == "Mexico" )selected @endif>Mexico</option>
                                                    <option value="Micronesia" @if($user_meta['country'] == "Micronesia" )selected @endif>Micronesia</option>
                                                    <option value="Moldova" @if($user_meta['country'] == "Moldova" )selected @endif>Moldova, Republic of</option>
                                                    <option value="Monaco" @if($user_meta['country'] == "Monaco" )selected @endif>Monaco</option>
                                                    <option value="Mongolia" @if($user_meta['country'] == "Mongolia" )selected @endif>Mongolia</option>
                                                    <option value="Montenegro" @if($user_meta['country'] == "Montenegro" )selected @endif>Montenegro</option>
                                                    <option value="Montserrat" @if($user_meta['country'] == "Montserrat" )selected @endif>Montserrat</option>
                                                    <option value="Morocco" @if($user_meta['country'] == "Morocco" )selected @endif>Morocco</option>
                                                    <option value="Mozambique" @if($user_meta['country'] == "Mozambique" )selected @endif>Mozambique</option>
                                                    <option value="Myanmar" @if($user_meta['country'] == "Myanmar" )selected @endif>Myanmar</option>
                                                    <option value="Namibia" @if($user_meta['country'] == "Namibia" )selected @endif>Namibia</option>
                                                    <option value="Nauru" @if($user_meta['country'] == "Nauru" )selected @endif>Nauru</option>
                                                    <option value="Nepal" @if($user_meta['country'] == "Nepal" )selected @endif>Nepal</option>
                                                    <option value="Netherlands" @if($user_meta['country'] == "Netherlands" )selected @endif>Netherlands</option>
                                                    <option value="Netherlands Antilles" @if($user_meta['country'] == "Netherlands Antilles" )selected @endif>Netherlands Antilles</option>
                                                    <option value="New Caledonia" @if($user_meta['country'] == "New Caledonia" )selected @endif>New Caledonia</option>
                                                    <option value="New Zealand" @if($user_meta['country'] == "New Zealand" )selected @endif>New Zealand</option>
                                                    <option value="Nicaragua" @if($user_meta['country'] == "Nicaragua" )selected @endif>Nicaragua</option>
                                                    <option value="Niger" @if($user_meta['country'] == "Niger" )selected @endif>Niger</option>
                                                    <option value="Nigeria" @if($user_meta['country'] == "Nigeria" )selected @endif>Nigeria</option>
                                                    <option value="Niue" @if($user_meta['country'] == "Niue" )selected @endif>Niue</option>
                                                    <option value="Norfolk Island" @if($user_meta['country'] == "Norfolk Island" )selected @endif>Norfolk Island</option>
                                                    <option value="Northern Mariana Islands" @if($user_meta['country'] == "Northern Mariana Islands" )selected @endif>Northern Mariana Islands</option>
                                                    <option value="Norway" @if($user_meta['country'] == "Norway" )selected @endif>Norway</option>
                                                    <option value="Oman" @if($user_meta['country'] == "Oman" )selected @endif>Oman</option>
                                                    <option value="Pakistan" @if($user_meta['country'] == "Pakistan" )selected @endif>Pakistan</option>
                                                    <option value="Palau" @if($user_meta['country'] == "Palau" )selected @endif>Palau</option>
                                                    <option value="Palestinian" @if($user_meta['country'] == "Palestinian" )selected @endif>Palestinian Territory, Occupied</option>
                                                    <option value="Panama" @if($user_meta['country'] == "Panama" )selected @endif>Panama</option>
                                                    <option value="Papua New Guinea" @if($user_meta['country'] == "Papua New Guinea" )selected @endif>Papua New Guinea</option>
                                                    <option value="Paraguay" @if($user_meta['country'] == "Paraguay" )selected @endif>Paraguay</option>
                                                    <option value="Peru" @if($user_meta['country'] == "Peru" )selected @endif>Peru</option>
                                                    <option value="Philippines" @if($user_meta['country'] == "Philippines" )selected @endif>Philippines</option>
                                                    <option value="Pitcairn" @if($user_meta['country'] == "Pitcairn" )selected @endif>Pitcairn</option>
                                                    <option value="Poland" @if($user_meta['country'] == "Poland" )selected @endif>Poland</option>
                                                    <option value="Portugal" @if($user_meta['country'] == "Portugal" )selected @endif>Portugal</option>
                                                    <option value="Puerto Rico" @if($user_meta['country'] == "Puerto Rico" )selected @endif>Puerto Rico</option>
                                                    <option value="Qatar" @if($user_meta['country'] == "Qatar" )selected @endif>Qatar</option>
                                                    <option value="Reunion" @if($user_meta['country'] == "Reunion" )selected @endif>Reunion</option>
                                                    <option value="Romania" @if($user_meta['country'] == "Romania" )selected @endif>Romania</option>
                                                    <option value="Russian Federation" @if($user_meta['country'] == "Russian Federation" )selected @endif>Russian Federation</option>
                                                    <option value="Rwanda" @if($user_meta['country'] == "Rwanda" )selected @endif>Rwanda</option>
                                                    <option value="Saint Helena" @if($user_meta['country'] == "Saint Helena" )selected @endif>Saint Helena</option>
                                                    <option value="Saint Kitts and Nevis" @if($user_meta['country'] == "Saint Kitts and Nevis" )selected @endif>Saint Kitts and Nevis</option>
                                                    <option value="Saint Lucia" @if($user_meta['country'] == "Saint Lucia" )selected @endif>Saint Lucia</option>
                                                    <option value="Saint Pierre and Miquelon" @if($user_meta['country'] == "Saint Pierre and Miquelon" )selected @endif>Saint Pierre and Miquelon</option>
                                                    <option value="Saint Vincent and The Grenadines" @if($user_meta['country'] == "Saint Vincent and The Grenadines" )selected @endif>Saint Vincent and The Grenadines</option>
                                                    <option value="Samoa" @if($user_meta['country'] == "Samoa" )selected @endif>Samoa</option>
                                                    <option value="San Marino" @if($user_meta['country'] == "San Marino" )selected @endif>San Marino</option>
                                                    <option value="Sao Tome and Principe" @if($user_meta['country'] == "Sao Tome and Principe" )selected @endif>Sao Tome and Principe</option>
                                                    <option value="Saudi Arabia" @if($user_meta['country'] == "Saudi Arabia" )selected @endif>Saudi Arabia</option>
                                                    <option value="Senegal" @if($user_meta['country'] == "Senegal" )selected @endif>Senegal</option>
                                                    <option value="Serbia" @if($user_meta['country'] == "Serbia" )selected @endif>Serbia</option>
                                                    <option value="Seychelles" @if($user_meta['country'] == "Seychelles" )selected @endif>Seychelles</option>
                                                    <option value="Sierra Leone" @if($user_meta['country'] == "Sierra Leone" )selected @endif>Sierra Leone</option>
                                                    <option value="Singapore" @if($user_meta['country'] == "Singapore" )selected @endif>Singapore</option>
                                                    <option value="Slovakia" @if($user_meta['country'] == "Slovakia" )selected @endif>Slovakia</option>
                                                    <option value="Slovenia" @if($user_meta['country'] == "Slovenia" )selected @endif>Slovenia</option>
                                                    <option value="Solomon Islands" @if($user_meta['country'] == "Solomon Islands" )selected @endif>Solomon Islands</option>
                                                    <option value="Somalia" @if($user_meta['country'] == "Somalia" )selected @endif>Somalia</option>
                                                    <option value="South Africa" @if($user_meta['country'] == "South Africa" )selected @endif>South Africa</option>
                                                    <option value="South Georgia and The South Sandwich Islands" @if($user_meta['country'] == "South Georgia and The South Sandwich Islands" )selected @endif>South Georgia and The South Sandwich
                                                        Islands</option>
                                                    <option value="Spain" @if($user_meta['country'] == "Spain" )selected @endif>Spain</option>
                                                    <option value="Sri Lanka" @if($user_meta['country'] == "Sri Lanka" )selected @endif>Sri Lanka</option>
                                                    <option value="Sudan" @if($user_meta['country'] == "Sudan" )selected @endif>Sudan</option>
                                                    <option value="Suriname" @if($user_meta['country'] == "Suriname" )selected @endif>Suriname</option>
                                                    <option value="Svalbard and Jan Mayen" @if($user_meta['country'] == "Svalbard and Jan Mayen" )selected @endif>Svalbard and Jan Mayen</option>
                                                    <option value="Swaziland" @if($user_meta['country'] == "Swaziland" )selected @endif>Swaziland</option>
                                                    <option value="Sweden" @if($user_meta['country'] == "Sweden" )selected @endif>Sweden</option>
                                                    <option value="Switzerland" @if($user_meta['country'] == "Switzerland" )selected @endif>Switzerland</option>
                                                    <option value="Syria" @if($user_meta['country'] == "Syria" )selected @endif>Syrian Arab Republic</option>
                                                    <option value="Taiwan" @if($user_meta['country'] == "Taiwan" )selected @endif>Taiwan, Province of China</option>
                                                    <option value="Tajikistan" @if($user_meta['country'] == "Tajikistan" )selected @endif>Tajikistan</option>
                                                    <option value="Tanzania" @if($user_meta['country'] == "Tanzania" )selected @endif>Tanzania, United Republic of</option>
                                                    <option value="Thailand" @if($user_meta['country'] == "Thailand" )selected @endif>Thailand</option>
                                                    <option value="Timor-leste" @if($user_meta['country'] == "Timor-leste" )selected @endif>Timor-leste</option>
                                                    <option value="Togo" @if($user_meta['country'] == "Togo" )selected @endif>Togo</option>
                                                    <option value="Tokelau" @if($user_meta['country'] == "Tokelau" )selected @endif>Tokelau</option>
                                                    <option value="Tonga" @if($user_meta['country'] == "Tonga" )selected @endif>Tonga</option>
                                                    <option value="Trinidad and Tobago" @if($user_meta['country'] == "Trinidad and Tobago" )selected @endif>Trinidad and Tobago</option>
                                                    <option value="Tunisia" @if($user_meta['country'] == "Tunisia" )selected @endif>Tunisia</option>
                                                    <option value="Turkey" @if($user_meta['country'] == "Turkey" )selected @endif>Turkey</option>
                                                    <option value="Turkmenistan" @if($user_meta['country'] == "Turkmenistan" )selected @endif>Turkmenistan</option>
                                                    <option value="Turks and Caicos Islands" @if($user_meta['country'] == "Turks and Caicos Islands" )selected @endif>Turks and Caicos Islands</option>
                                                    <option value="Tuvalu" @if($user_meta['country'] == "Tuvalu" )selected @endif>Tuvalu</option>
                                                    <option value="Uganda" @if($user_meta['country'] == "Uganda" )selected @endif>Uganda</option>
                                                    <option value="Ukraine" @if($user_meta['country'] == "Ukraine" )selected @endif>Ukraine</option>
                                                    <option value="United Arab Emirates" @if($user_meta['country'] == "United Arab Emirates" )selected @endif>United Arab Emirates</option>
                                                    <option value="United Kingdom" @if($user_meta['country'] == "United Kingdom" )selected @endif>United Kingdom</option>
                                                    <option value="United States" @if($user_meta['country'] === "United States" )selected @endif>United States</option>
                                                    <option value="United States Minor Outlying Islands" @if($user_meta['country'] == "United States Minor Outlying Islands" )selected @endif>United States Minor Outlying Islands</option>
                                                    <option value="Uruguay" @if($user_meta['country'] == "Uruguay" )selected @endif>Uruguay</option>
                                                    <option value="Uzbekistan" @if($user_meta['country'] == "Uzbekistan" )selected @endif>Uzbekistan</option>
                                                    <option value="Vanuatu" @if($user_meta['country'] == "Vanuatu" )selected @endif>Vanuatu</option>
                                                    <option value="Venezuela" @if($user_meta['country'] == "Venezuela" )selected @endif>Venezuela</option>
                                                    <option value="Vietnam" @if($user_meta['country'] == "Vietnam" )selected @endif>Viet Nam</option>
                                                    <option value="Virgin Islands, British" @if($user_meta['country'] == "Virgin Islands, British" )selected @endif>Virgin Islands, British</option>
                                                    <option value="Virgin Islands, U.S." @if($user_meta['country'] == "Virgin Islands, U.S." )selected @endif>Virgin Islands, U.S.</option>
                                                    <option value="Wallis and Futuna" @if($user_meta['country'] == "Wallis and Futuna" )selected @endif>Wallis and Futuna</option>
                                                    <option value="Western Sahara" @if($user_meta['country'] == "Western Sahara" )selected @endif>Western Sahara</option>
                                                    <option value="Yemen" @if($user_meta['country'] == "Western Sahara" )selected @endif>Yemen</option>
                                                    <option value="Zambia" @if($user_meta['country'] == "Zambia" )selected @endif>Zambia</option>
                                                    <option value="Zimbabwe" @if($user_meta['country'] == "Zimbabwe") selected @endif>Zimbabwe</option>
                                                </select>
                                                @error('user_country')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="city">{{__("routes.City")}}:</label>
                                                <input type="text" value="{{ isset($user_meta['city']) ? $user_meta['city'] : ""}}" class="form-control" name="user_city" id="user_city">
                                                @error('user_city')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="CategoryThumbnail">{{__("routes.Street")}}:</label>
                                                <input type="text" value="{{ isset($user_meta['street_name']) ? $user_meta['street_name'] : ""}}" class="form-control" name="user_streetname" id="user_streetname">
                                                @error('user_streetname')
                                                <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="CategoryThumbnail">{{__("routes.Street")}} #No:</label>
                                                <input type="text" value="{{ isset($user_meta['street_no']) ? $user_meta['street_no'] : ""}}" class="form-control" name="user_streetno" id="user_streetno">
                                                @error('user_streetno')
                                                <span class="invalid-feedback d-block" role="alert">
                                                       <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="CategoryThumbnail">{{__("routes.Profile Picture")}}:</label>
                                                <small class="d-block mb-2">
                                                    <cite title="Source Title">
                                                        {{__("routes.Man Width")}} 400px ,
                                                        {{__("routes.Max routes.Max Height")}} 400px
                                                    </cite>
                                                </small>
                                                <input id="file-1" type="file" name="profile_pic"  class="file">
                                            </div>
                                        </div>

                                    </div>

                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12 mt-2 mb-3">
                                            <h4>{{__("routes.My Address Book")}}</h4>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 pr-lg-5">
                                            <label for="shipping_address">{{__("routes.Shipping Address")}}:</label>
                                            <textarea name="user_shipping_add" class="form-control" id="" cols="30" rows="5">{{ isset($user_meta['shipping_address']) ? $user_meta['shipping_address'] : ""}}</textarea>
                                        </div>

                                        <div class="col-md-6 pr-lg-5">
                                            <label for="billing_address">{{__("routes.Billing Address")}}:</label>
                                            <textarea name="user_billing_add" class="form-control" id="" cols="30" rows="5">{{ isset($user_meta['billing_address']) ? $user_meta['billing_address'] : ""}}</textarea>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row mt-4">
                                        <div class="col-md-4">
                                            <input type="submit" class="btn btn-primary" value="{{__("routes.Update Details")}}">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script>

        //==================================================================================

        $("#file-1").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 3000,
            maxFilesNum: 1,
            showClose: true,
            showUpload: false,
            showCancel: true,
            showZoom: true,
            maxImageWidth:  400,
            maxImageHeight: 400,
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            @isset($user->profile_pic)
            initialPreview: [
                '<img src="{{asset('storage/'.$user->profile_pic.'')}}" class="kv-preview-data file-preview-image">',
            ],
            @endisset

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        //==================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });
    </script>

@endsection
