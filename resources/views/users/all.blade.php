@extends('layouts.backend.app')

@section('page-css')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__("routes.Users")}} {{__("routes.List")}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Users")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('user.list')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">

                            <!-- /.card-header -->
                            <div class="card-body">
                                {{--                            <button class="btn btn-danger btn-sm mb-3" id="delete-all-btn">Trash All</button>--}}

                                <table id="example1" class="table table-striped table-bordered nowrap ">
                                    <thead>
                                    <tr>
                                        {{--                                        <th>#</th>--}}
                                        <th>No</th>
                                        <th>ID</th>
                                        <th>{{__("routes.Full name")}}</th>
                                        <th>{{__("routes.Email")}}</th>
                                        <th>{{__("routes.Phone No")}}</th>
                                        <th>{{__("routes.City")}}</th>
                                        <th>{{__("routes.Country")}}</th>
                                        <th>{{__("routes.Created At")}}</th>
                                        <th>{{__("routes.Action")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key => $user)
                                        @php
                                            $meta = getAllUserMetas($user->id);
                                        @endphp

                                        <tr>
                                            {{--                                        <td>--}}
                                            {{--                                            <div class="custom-control custom-checkbox">--}}
                                            {{--                                                <input class="custom-control-input" type="checkbox" name="faq" id="customCheckbox{{$key}}" value="{{$user->id}}">--}}
                                            {{--                                                <label for="customCheckbox{{$key}}" class="custom-control-label"></label>--}}
                                            {{--                                            </div>--}}
                                            {{--                                        </td>--}}
                                            <td>{{ $key+1}}</td>
                                            <td>{{ $user->id}}</td>
                                            <td>{{ ucfirst($user->name)  }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ isset($meta['telno']) ? $meta['telno'] : "Not Found"  }}</td>
                                            <td>{{ isset($meta['city']) ? $meta['city'] : "Not Found" }}</td>
                                            <td>{{ isset($meta['country']) ? $meta['country'] : "Not Found" }}</td>
                                            <td>{{ \Carbon\Carbon::parse($user->created_at)->format("Y-m-d g:ia") }}</td>
                                            <td>
                                                {{--                                            @if(site_config('site_maintenance_status') == "on" ) checked @else uncheckec @endif--}}
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input user_status" id="user_status_{{$user->id}}"  value="{{$user->id}}" @if(($user->status)== 1 ) checked @else uncheckec @endif>
                                                    <label class="custom-control-label" for="user_status_{{$user->id}}"></label>
                                                </div>
                                                {{--                                            <a href="#" class="btn btn-info btn-sm mr-3"><i class="fa fa-edit"></i></a>--}}
                                                {{--                                            <a href="#" class="custom-delete-btn btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Delete more than once -->
    <div class="modal fade" id="delete-all-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-all-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="delete_ids" id="delete_ids" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Category</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete all the selected users!</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_all_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>



@endsection

@section('page-script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================


        $("#delete-all-btn").on('click' , function () {
            var ids = [];

            var route = '{{ route('user.delete-all') }}';

            $("#delete-all-modal").modal('show');
            $("#delete-all-modal-form").attr("action", route);

            $.each($("input[name='faq']:checked"), function(){
                ids.push($(this).val());
            });
            console.log(ids);
            $("#delete_ids").attr("value",JSON.stringify(ids));

            $('.delete_all_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-all-modal-form").submit();
            });

        });

        //=======================================================================================================


        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "autoWidth": false,
        });

        //=======================================================================================================

         var status = "";

        // input[type="checkbox"]
        $('.user_status').click(function(){
            var id= $(this).val();
            console.log(id)
            var customSwitch1 = $(this);
            if($(this).prop("checked") === true){
                console.log("checked");
                status = "on";

                $.ajax({
                    url:"{{route('user.status')}}",
                    type: 'post',
                    data: {status : status,user_id:id},
                    success:function(response){
                        toastr.success('User is <strong>Activate</strong> !', 'User Status');

                    }
                });
            }
            else if($(this).prop("checked") === false){
                console.log("Unchecked");
                status = "off";
                $.ajax({
                    url:"{{route('user.status')}}",
                    type: 'post',
                    data: {status : status, user_id: id},
                    success:function(response){
                        toastr.error('User is <strong>Deactivate</strong> !', 'User Status');
                    }
                });
            }
        });


        //=======================================================================================================



    </script>

@endsection
