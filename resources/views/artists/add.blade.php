@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> {{__("routes.Artists")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Artists")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('artist.add')}}">{{__("routes.Add")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Add Section")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <form action="{{route('artist.create')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-3 pl-5">
                                            <label for="CategoryThumbnail">{{__("routes.Avatar")}}:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <img src="{{asset('images/placeholders/Profile_avatar_placeholder_lg.png')}}" id="avatar-tag"
                                                     width="100" height="100"/>

                                                <div class="custom-file mt-3">
                                                    <input type="file" name="artist_avatar" class="custom-file-input"
                                                           id="avatar">
                                                    <label class="custom-file-label" for="customFile">{{__("routes.Choose Image")}}</label>
                                                </div>
                                                {{--<input type="file" name="file" id="profile-img">--}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistName">{{__("routes.Artist Name")}} <span class="text-danger">*</span> :</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('artist_name') is-invalid @enderror" name="artist_name" id="artist_name">
                                                @error('artist_name')
                                                <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">{{__("routes.Artist Email: (Optional)")}}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control @error('artist_email') is-invalid @enderror" value="{{old('artist_email')}}" name="artist_email" id="artist_email">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail"><i class="fab fa-facebook-square fa-2x mr-3"></i>(Optional)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="social_link_facebook" id="social_link_facebook">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail"><i class="fab fa-twitter fa-2x mr-3"></i>(Optional)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="social_link_twitter" id="social_link_twitter">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail"><i class="fab fa-instagram fa-2x mr-3"></i>(Optional)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="social_link_instagam" id="social_link_instagam">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail"><i class="fab fa-google-plus-g fa-2x mr-3"></i>(Optional)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="social_link_google_plus" id="social_link_google_plus">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3 pl-5">
                                            <label for="ArtistEmail">Other:   (Optional)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="social_link_other" id="social_link_other">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mt-4">
                                        <div class="col-md-3 text-left">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary" value="{{__("routes.Add New")}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#avatar-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#avatar").change(function () {
            readURL(this);
        });

        //=======================================================================================================

    </script>

@endsection
