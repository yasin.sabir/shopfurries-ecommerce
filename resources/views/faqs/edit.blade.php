@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1>{{{__("routes.Edit Details")}}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__('routes.FAQs')}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('admin.faqs.edit',encrypt($faq->id))}}"> {{__("routes.Edit")}} </a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Edit Section")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{route('admin.faqs.update',$faq->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="question_label">{{__("routes.Question")}}:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                     <textarea id="textarea1"
                                               class="@error('question') is-invalid @enderror"
                                               name="question"
                                               style="width: 100%;  font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$faq->question}}</textarea>
                                                @error('question')
                                                <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                                <small>
                                                    <cite title="Source Title">
                                                        {{__("routes.The question is how it appears on your FAQ page.")}}
                                                    </cite>
                                                </small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-3">
                                            <label for="question_label">{{__("routes.Answers")}}:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                    <textarea id="textarea2"
                                              class="@error('answer') is-invalid @enderror"
                                              name="answer"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$faq->answer}}</textarea>
                                                @error('answer')
                                                <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                                <small>
                                                    <cite title="Source Title">
                                                        {{__("routes.The answers is how it appears on your FAQ page.")}}
                                                    </cite>
                                                </small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="question_label">{{__("routes.Active")}}:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="checkbox" name="status" @if($faq->status == 1) checked @endif data-bootstrap-switch>
                                                <br>
                                                <small>
                                                    <cite title="Source Title">
                                                        {{__("routes.Question will be appear on faqs page or not ?")}}
                                                    </cite>
                                                </small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary btn-sm" value="{{__("routes.Update FAQ")}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </section>


    </div>

@endsection


@section('page-script')

    <script>
        // // Summernote
        // $('#textarea1').summernote({
        //     height: 200,
        //     toolbar: false,
        // });
        //
        // // Summernote
        // $('#textarea2').summernote({
        //     height: 200,
        //     toolbar: false,
        // });

        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });

    </script>

@endsection
