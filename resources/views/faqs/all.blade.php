@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__("routes.Frequently Asked Questions")}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.FAQs")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('admin.faqs.viewAll')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.FAQ List")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>

                            <!-- /.card-header -->
                            <div class="card-body">
                                <button class="btn btn-danger btn-sm mb-3" id="delete-all-btn">{{__("routes.Trash All")}}</button>
                                <table id="example1" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" name="checkAll" id="customCheckbox001">
                                                <label for="customCheckbox001" class="custom-control-label"></label>
                                            </div>
                                        </th>
                                        <th>No</th>
                                        <th>{{__("routes.Question")}}</th>
                                        <th>{{__("routes.Created At")}}</th>
                                        <th>{{__("routes.Action")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($faqs as $key => $faq)
                                        @php

                                            @endphp
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox" name="faq" id="customCheckbox{{$key}}" value="{{$faq->id}}">
                                                    <label for="customCheckbox{{$key}}" class="custom-control-label"></label>
                                                </div>
                                                {{--                                                <input type="checkbox" name="faq" id="checkbox" value="{{$faq->id}}">--}}
                                            </td>
                                            <td>{{$key+1}}</td>
                                            <td><a href="#">{{substr(strip_tags($faq->question),0,30)}}...</a></td>
                                            <td>{{ \Carbon\Carbon::parse($faq->created_at)->format("Y-m-d g:ia")  }}</td>
                                            <td>
                                                <a href="{{route('admin.faqs.edit',encrypt($faq->id))}}" class="btn btn-info btn-sm mr-3"><i class="fa fa-edit"></i></a>
                                                <a href="#_" class="custom-delete-btn btn btn-danger btn-sm"
                                                   faq_id="{{$faq->id}}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Single Delete modal  -->
    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Faq")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("routes.You want to sure to delete this")}} {{__("routes.FAQs")}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

    <!-- Delete more than once -->
    <div class="modal fade" id="delete-all-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-all-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="delete_ids" id="delete_ids" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Faq")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("routes.You want to sure to delete this")}} {{__("routes.FAQs")}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_all_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection

@section('page-script')
    <script>
        $(function () {
            // $("#example1").DataTable();
            $('#example1').DataTable({
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 7,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });

            // =======================================================================================================

            $("input[name='checkAll']").click(function(){
                $("input[name='faq']").not(this).prop('checked', this.checked);
            });

            // =======================================================================================================


            $("#delete-all-btn").on('click' , function () {
                var ids = [];

                var route = '{{ route('admin.faqs.delete-all') }}';

                $("#delete-all-modal").modal('show');
                $("#delete-all-modal-form").attr("action", route);

                $.each($("input[name='faq']:checked"), function(){
                    ids.push($(this).val());
                });
                console.log(ids);
                $("#delete_ids").attr("value",JSON.stringify(ids));

                $('.delete_all_modal_btn').on('click', function (e) {
                    e.preventDefault();
                    // alert("ds");
                    $("#delete-all-modal-form").submit();
                });

            });

        });

        // =======================================================================================================

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("faq_id");
            var route = '{{ route('admin.faqs.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            });

        });

        // =======================================================================================================

    </script>

@endsection
