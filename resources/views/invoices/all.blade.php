@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Invoices")}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Invoices")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('invoice.list')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.List")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                @if(Auth::user()->hasRole(1))
{{--                                    <button class="btn btn-danger btn-sm mb-3" id="delete-all-btn">{{__("routes.Trash All")}}</button>--}}
                                @endif

                                <table id="example1" class="table table-bordered table-striped nowrap">
                                    <thead>
                                    <tr>
                                        @if(Auth::user()->hasRole(1))
{{--                                            <th>--}}
{{--                                                <div class="custom-control custom-checkbox">--}}
{{--                                                    <input class="custom-control-input" type="checkbox" name="checkAll" id="customCheckbox001">--}}
{{--                                                    <label for="customCheckbox001" class="custom-control-label"></label>--}}
{{--                                                </div>--}}
{{--                                            </th>--}}
                                        @endif
                                        <th>No:</th>
                                        <th>{{__("routes.Invoice No")}}:</th>
                                        <th>{{__("routes.Order No")}}:</th>
                                        <th>{{__("routes.Created At")}}:</th>
                                        <th>{{__("routes.Action")}}:</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $num=1;
                                    @endphp

                                    @forelse($order as $key => $data)
                                        <tr>

                                            @if(Auth::user()->hasRole(1))
{{--                                                <td>--}}
{{--                                                    <div class="custom-control custom-checkbox">--}}
{{--                                                        <input class="custom-control-input" type="checkbox" name="faq"--}}
{{--                                                               id="customCheckbox{{$key}}" value="{{$data->id}}">--}}
{{--                                                        <label for="customCheckbox{{$key}}"--}}
{{--                                                               class="custom-control-label"></label>--}}
{{--                                                    </div>--}}
                                                    {{-- <input type="checkbox" name="faq" id="checkbox" value="{{$faq->id}}">--}}
{{--                                                </td>--}}
                                            @endif

                                            <td>{{$num}}</td>
                                            <td>{{"SH_invoice_".$data->id}}</td>
                                            <td>{{$data->order_id}}</td>
                                            <td>{{date("D d M Y", strtotime($data->created_at))}}</td>
                                            <td class="d-flex justify-content-center">
                                                <form action="{{route('order.pdfgenerate')}}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <input type="hidden" name="order_id" value="{{$data->id}}"/>
                                                    <button type="submit" class="btn btn-info btn-sm mr-3">
                                                        <i class="fa fa-file-pdf"></i>
                                                    </button>
                                                </form>

{{--                                                <a href="#_" class="custom-delete-btn btn btn-danger btn-sm"--}}
{{--                                                   order_id="{{$data->id}}"--}}
{{--                                                   order_name="{{ $data->code }}"><i class="fa fa-trash"></i></a>--}}
                                            </td>
                                        </tr>
                                        @php
                                            $num++;
                                        @endphp
                                    @empty
                                        <tr>
                                            <td colspan="7" class="text-center">{{__("routes.No Invoice Found")}}</td>
                                        </tr>
                                    @endforelse


                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->


    <!-- Delete single once-->
    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Invoice")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("routes.You want to sure to delete this Invoice from your site")}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>


    <!-- Delete more than once -->
    <div class="modal fade" id="delete-all-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-all-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="delete_ids" id="delete_ids" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Invoices")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("routes.You want to sure to delete all the selected Invoices")}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_all_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>


@endsection

@section('page-script')

    <script type="text/javascript">

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 7,
            "autoWidth": false,
        });

        //=======================================================================================================

        $(document).on('click' , '.custom-show-btn' , function () {

                {{--            @if( $artist->image != null)--}}
                {{--        <img src="{{asset('storage/'.$artist->image)}}" id="avatar-tag"--}}
                {{--            width="100" height="100"/>--}}
                {{--                @else--}}
                {{--                <img src="{{asset('images/placeholders/Profile_avatar_placeholder_lg.png')}}" id="avatar-tag"--}}
                {{--            width="100" height="100"/>--}}
                {{--                @endif--}}


            var id = $(this).attr("artist_id");
            var name = $(this).attr("artist_name");
            var email = $(this).attr("artist_email");
            var image = $(this).attr("artist_image");
            var fb = $(this).attr("facebook_link");
            var twit = $(this).attr("twitter_link");
            var insta = $(this).attr("instagram_link");
            var gplus = $(this).attr("googleplus_link");
            var other = $(this).attr("other_link");


            $("#show-model-title").text(name)
            $("#artist_lab_name").text(name);
            $("#artist_lab_email").text(email);
            $("#artist_lab_avatar").attr("src",image);
            $("#artist_lab_facebook").text(fb);
            $("#artist_lab_twitter").text(twit);
            $("#artist_lab_instagram").text(insta);
            $("#artist_lab_googleplus").text(gplus);
            $("#artist_lab_other").text(other);

            $("#show-modal").modal("show");
        });


        //=======================================================================================================

        $("input[name='checkAll']").click(function(){
            $("input[name='faq']").not(this).prop('checked', this.checked);
        });

        // =======================================================================================================


        $("#delete-all-btn").on('click' , function () {
            var ids = [];

            var route = '{{ route('event.delete-all') }}';

            $("#delete-all-modal").modal('show');
            $("#delete-all-modal-form").attr("action", route);

            $.each($("input[name='faq']:checked"), function(){
                ids.push($(this).val());
            });
            console.log(ids);
            $("#delete_ids").attr("value",JSON.stringify(ids));

            $('.delete_all_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-all-modal-form").submit();
            });

        });

        //=======================================================================================================
        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("event_id");
            var route = '{{ route('event.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            });

        });

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

    </script>

@endsection
