@extends('layouts.backend.app') @section('page-css')

    <style>
        .note-editable.card-block {
            height: 300px;
        }
    </style>

@endsection @section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> {{__("routes.Multiple Products")}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Products")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('product.bulk.add')}}">{{__("routes.Add Multiple")}}</a></li>
                        </ol>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Product Data Section Start-->
                        <div class="card card-default">
                            <div class="card-body pad">

                                <div class="form-group">
                                    <label for="product_nae"
                                           class="mt-1">{{__("routes.Add Multiple Products")}}</label>
                                </div>

                                <div class="form-group">
                                    <div class="file-loading">
                                        @php $i=1; @endphp
                                        <input id="file-1" type="file" name="products[]" multiple
                                               class="file" data-overwrite-initial="false" data-min-file-count="1">
                                        {{ $errors->has('file.',$i)? "has-error" :"" }}
                                        @if($errors->has('file.',$i))
                                            <span class="invalid-feedback d-block" role="alert">
                                                   <strong>{{ $errors->first('file.',$i) }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="category_name">{{__("routes.Category")}}
                                        <span class="text-danger">*</span> :</label>
                                    <select
                                        class="form-control @error('category_name') is-invalid @enderror"
                                        name="category_name" id="category_name">
                                        <option value="">Select Option</option>
                                        @foreach($categories as $key => $val)
                                            @if($val->Additional_Req == 1)
                                                <option value="{{$val->id}}">{{$val->Name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @error('category_name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <!-- form start -->
                                <form method="post" action="{{route('product.bulk.list')}}" class="form-horizontal" enctype="multipart/form-data">
                                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group text-center">
                                        <input type="submit" class="btn btn-lg btn-primary  btn-block"
                                               value="{{__("routes.Submit")}}">
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection @section('page-script')

    <script>
        $("#file-1").fileinput({
            theme: 'fas',
            showZoom: true,
            showClose: true,
            showUpload: true,
            uploadAsync: false,
            showCancel: false,
            maxFileSize: 153600,
            overwriteInitial: false,
            // enableResumableUpload:false,
            allowedFileExtensions: ['jpg', 'png', 'gif'],
            uploadUrl: '{{route('product.bulk.temp-upload')}}',
            //deleteUrl: '',
            uploadExtraData: function() {
                return {
                    _token: $('#csrf_token').val(),
                    category_name:$('#category_name option:selected').val()
                };
            },
            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            },
        })

        //     .on('fileuploaderror', function(event, data, msg) {
        //     console.log('File uploaded Err', msg , data , event);
        // });


        // CATCH RESPONSE
        $('#file-1').on('filebatchuploaderror', function(event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra,
                response = data.response, reader = data.reader;
        });

        $('#file-1').on('filebatchuploadsuccess', function(event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra,
                response = data.response, reader = data.reader;
            alert (extra.bdInteli + " " +  response.uploaded);
        });


    </script>

@endsection
