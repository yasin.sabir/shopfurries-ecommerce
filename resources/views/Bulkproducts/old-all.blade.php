@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="float-left">Products</h1>
                        <p><a href="{{route('product.list')}}" class="btn btn-info btn-sm ml-3 mr-3 ">Product List</a></p>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item active">List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Add Multiple Products</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                <form method="post" action="{{route('product.bulk.create-all')}}" enctype="multipart/form-data">
                                    @csrf
                                    @php $num=1; @endphp
                                    @forelse($images as $i => $images)

                                        <div class="row" id="row_{{$i}}">

                                            <div class="col-md-6 text-left">
                                                <h5>Product-{{$num}}</h5>
                                                <div class="form-group">
                                                    <img class="shadow custom-img-style" width="100" height="100" src="{{asset('storage/'.$images->name)}}" alt=""srcset="">
                                                    <input type="hidden" value="{{$images->name}}" name="image_name[{{$i}}]"/>
                                                    <input type="hidden" value="{{$images->id}}" name="image_row_id[{{$i}}]"/>
                                                </div>
                                            </div>

                                            <div class="col-md-6 text-right">
                                                <input type="hidden" name="temp_img_id" id="temp_img_id"  value="{{$images->id}}">
                                                <button type="submit" id="temp_prod_delete_btn"
                                                        image_id="{{$images->id}}"
                                                        class="custom-delete-btn btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <input type="text" value="{{old('product_name.'.$i)}}" class="form-control product_name {{ $errors->has('product_name.'.$i) ? 'has-error':""  }}" name="product_name[{{$i}}]" placeholder="Product Title">

                                                    @if($errors->has('product_name.'.$i))
                                                        <span class="invalid-feedback d-block" role="alert">
                                                           <strong>{{ $errors->first('product_name.'.$i) }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <select class="select2 {{ $errors->has('categories.'.$i) ? 'has-error':""  }}" name="categories[categories_{{$i}}][]" multiple="multiple" data-placeholder="Select categories" data-dropdown-css-class="select2-blue" style="width: 100%;">
                                                                @foreach($category as $categorys)
                                                                    <option value="{{ $categorys->id }}"  >
                                                                        {{$categorys->Name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">

                                                            <select class="select2" name="tags[{{$i}}][]" multiple="multiple" data-placeholder="Select tags" data-dropdown-css-class="select2-blue" style="width: 100%;">
                                                                @foreach($tags as $tagss)
                                                                    <option value="{{ $tagss->id }}"  {{ old('tag.'.$i) == $tagss->id ? 'selected' : '' }} > {{ $tagss->name }}</option>
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <input type="text"  value="{{old('sku.'.$i)}}" class="form-control {{ $errors->has('sku.'.$i) ? 'has-error':""  }}" name="sku[{{$i}}]" placeholder="SKU">

                                                            @if($errors->has('sku.'.$i))
                                                                <span class="invalid-feedback d-block" role="alert">
                                                                   <strong>{{ $errors->first('sku.'.$i) }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text"  value="{{old('stock.'.$i)}}" class="form-control {{ $errors->has('stock.'.$i) ? 'has-error':""  }}" name="stock[{{$i}}]" placeholder="Stock">

                                                            @if($errors->has('stock.'.$i))
                                                                <span class="invalid-feedback d-block" role="alert">
                                                                   <strong>{{ $errors->first('stock.'.$i) }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <input type="text"  value="{{old('price.'.$i)}}" class="form-control price {{ $errors->has('price.'.$i) ? 'has-error':""  }}" name="price[{{$i}}]" placeholder="Price">

                                                            @if($errors->has('price.'.$i))
                                                                <span class="invalid-feedback d-block" role="alert">
                                                                   <strong>{{ $errors->first('price.'.$i) }}</strong>
                                                                </span>
                                                            @endif

                                                        </div>
                                                        <div class="form-group">
                                                            <select class="form-control {{ $errors->has('status.'.$i) ? 'has-error':""  }}" name="status[{{$i}}]">
                                                                <option value="">Product Status</option>
                                                                <option value="1">yes</option>
                                                                <option value="0">no</option>
                                                            </select>

                                                            @if($errors->has('status.'.$i))
                                                                <span class="invalid-feedback d-block" role="alert">
                                                                   <strong>{{ $errors->first('status.'.$i) }}</strong>
                                                                </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <hr>
                                            </div>

                                        </div>

                                        @php $num++; @endphp
                                    @empty
                                        <div class="col-md-12 text-center">
                                            <h4>No Products Found</h4>
                                        </div>
                                    @endforelse

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary mt-3" value="Submit All Products" name="all_upload" id="all_upload">
                                    </div>

                                </form>
                            </div>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "pageLength": 50,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });

        $(function () {
            $('.select2').select2();
        });


        $(document).on('click', '.custom-delete-btn', function (e) {
            e.preventDefault();

            var temp_img = $(this).attr("image_id");
            var route = '{{ route('product.bulk.delete', ['id' => 'id']) }}';
            route = route.replace('id', temp_img);

            $.ajax({
                url:route,
                type:'post',
                data:{temp_img : temp_img},
                success:function(response){
                    //console.log(response+" - Mode is on");

                    toastr.success('Temporary Product has successfully deleted', 'Temporary Item');
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
            });

        });


    </script>

@endsection
