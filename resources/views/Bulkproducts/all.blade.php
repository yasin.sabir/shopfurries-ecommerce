@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="float-left">{{__("routes.Products")}}</h1>
                        <p><a href="{{route('product.list')}}" class="btn btn-primary btn-sm ml-3 mr-3 ">{{__("routes.Product List")}}</a></p>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Products")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('product.bulk.show')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">{{__("routes.Add Multiple Products")}}</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                <form method="post" action="{{route('product.bulk.create-all')}}" enctype="multipart/form-data">
                                    @csrf
                                    @php $num=1; @endphp
                                    <div class="row" id="row_">
                                    @forelse($images as $i => $image)

                                            <div class="col-md-3 pl-4 pr-4 text-center">
                                                <h5>Product-{{$num}}</h5>
                                                <div class="form-group">
                                                    <div class="custom-img-section shadow custom-img-style"  style="background-image: url({{asset('storage/'.$image['image'])}});">
                                                    </div>
                                                    <input type="hidden" value="{{$image['name']}}" name="product_name[{{$i}}]"/>
                                                    <input type="hidden" value="{{$image['image']}}" name="image_name[{{$i}}]"/>
                                                    <input type="hidden" value="{{$image['id']}}" name="image_row_id[{{$i}}]"/>
                                                    <input type="hidden" value="{{$image['category_id']}}" name="categories[{{$i}}]"/>
                                                    <input type="hidden" value="{{$image['price']}}" name="price[{{$i}}]"/>
                                                    <input type="hidden" value="{{$image['tags']}}" name="tags[{{$i}}]"/>
                                                    <input type="hidden" value="{{$image['material']}}" name="material[{{$i}}]"/>
                                                    <input type="hidden" value="{{$image['sizes']}}" name="sizes[{{$i}}]"/>
                                                    <input type="hidden" value="{{$image['artist']}}" name="artist[{{$i}}]"/>


                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="multiuploader-prod-name mt-2 d-block">{{ucfirst($image['name'])}} </label>
                                                </div>

                                                <div class="form-group">
                                                    <?php
                                                       $tags_id = unserialize($image['tags']);

                                                       if(!empty( $tags_id )){
                                                          foreach ($tags_id as $key => $val){
                                                              $tag = \App\Tag::find($val);
                                                            ?>
                                                                <span class="badge bg-primary">{{$tag->name}}</span>
                                                            <?php
                                                          }
                                                       }
                                                    ?>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="mt-2 d-block">{{isset($image['price']) ? '$'.number_format($image['price'],2) :'' }} </label>
                                                </div>

                                                <div class="form-group">
                                                    <input type="hidden" name="temp_img_id" id="temp_img_id"  value="{{$image['id']}}">
                                                    <button type="submit" id="temp_prod_delete_btn"
                                                            image_id="{{$image['id']}}"
                                                            class="custom-delete-btn btn btn-danger btn-sm"><i class="fa fa-trash"></i>
                                                    </button>
                                                </div>



                                            </div>


{{--                                            <div class="col-md-6">--}}
{{--                                                <input type="hidden" name="temp_img_id" id="temp_img_id"  value="{{$images->id}}">--}}
{{--                                                <button type="submit" id="temp_prod_delete_btn"--}}
{{--                                                        image_id="{{$images->id}}"--}}
{{--                                                        class="custom-delete-btn btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>--}}
{{--                                            </div>--}}

{{--                                            <div class="col-md-6">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <input type="text" value="{{old('product_name.'.$i)}}" class="form-control product_name {{ $errors->has('product_name.'.$i) ? 'has-error':""  }}" name="product_name[{{$i}}]" placeholder="Product Title">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}


                                        @php $num++; @endphp
                                    @empty
                                        <div class="col-md-12 text-center">
                                            <h4>{{__("routes.No Products Found")}}</h4>
                                        </div>
                                    @endforelse
                                    </div>


                                    @if(!empty($images))
                                        <div class="text-center">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary mt-3" value="{{__("routes.Submit All Products")}}" name="all_upload" id="all_upload">
                                            </div>
                                        </div>
                                    @endif


                                </form>
                            </div>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "pageLength": 50,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });

        $(function () {
            $('.select2').select2();
        });


        $(document).on('click', '.custom-delete-btn', function (e) {
            e.preventDefault();

            var temp_img = $(this).attr("image_id");
            var route = '{{ route('product.bulk.delete', ['id' => 'id']) }}';
            route = route.replace('id', temp_img);

            $.ajax({
                url:route,
                type:'post',
                data:{temp_img : temp_img},
                success:function(response){
                    //console.log(response+" - Mode is on");

                    toastr.success('Temporary Product has successfully deleted', 'Temporary Item');
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
            });

        });


    </script>

@endsection
