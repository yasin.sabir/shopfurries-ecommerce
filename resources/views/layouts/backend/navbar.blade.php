<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-primary elevation-4">
{{--    sidebar-light-primary--}}

    @php
        $getLocal = "";
        $getLocal = app()->getLocale() == "en" ? "" : "de/";
    @endphp



    <!-- Brand Logo -->
    <a href="{{url( $getLocal.'Dashboard' )}}" class="brand-link">
        <img src="{{asset('images/logo/ShopFurries-Logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="border-radius: 0px ;box-shadow: 0px 0px 0px 0px !important;opacity: .8">
        <span class="brand-text font-weight-light">Shop Furries</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset(profile_pic(Auth::user()->id))}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ ucfirst(Auth::user()->name)  }}</a>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{url( $getLocal.'Dashboard' )}}" class="nav-link @if(\Request::is($getLocal.'Dashboard')) active @endif">
                        <i class="nav-icon far fa-tachometer-alt"></i>
                        <p>{{__('msgs.Dashboard')}}</p>
                    </a>
                </li>

                <!-- User Section-->
                <li class="nav-item has-treeview @if(\Request::is('Users/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is('Users/*')) menu-open active @endif">
                        <i class="nav-icon far fa-users"></i>
                        <p>{{__("routes.Users")}}<i class="fas fa-angle-left right"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('user.list')}}" class="nav-link @if(\Request::is('Users/List')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getUserCount() }}</span></p>
                            </a>
                        </li>
                        {{--                        <li class="nav-item">--}}
                        {{--                            <a href="{{route('user.add')}}" class="nav-link @if(\Request::is('Users/Add')) active @endif">--}}
                        {{--                                <i class="far fa-circle nav-icon"></i>--}}
                        {{--                                <p>Add Users</p>--}}
                        {{--                            </a>--}}
                        {{--                        </li>--}}
                    </ul>
                </li>

                <!-- User Account Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'User/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'User/*')) menu-open active @endif">
                        <i class="nav-icon far fa-user"></i>
                        <p>{{__("routes.My Account")}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview @if(\Request::is($getLocal.'User/Account')) active @endif">
                        <li class="nav-item">
                            <a href="{{route('user.account')}}" class="nav-link  @if(\Request::is($getLocal.'User/Account')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Profile")}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('user.changePassword')}}" class="nav-link @if(\Request::is($getLocal.'User/ChangePassword')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Change Password")}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Artist Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'Artist/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'Artist/*')) menu-open active @endif">
                        <i class="nav-icon far fa-user-tie"></i>
                        <p>{{__("routes.Artists")}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('artist.list')}}" class="nav-link @if(\Request::is($getLocal.'Artist/List')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getArtistCount() }}</span></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('artist.add')}}" class="nav-link @if(\Request::is($getLocal.'Artist/Add')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Add Artist")}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Product Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'Product/*') || \Request::is($getLocal.'Product/Bundle/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'Product/*') || \Request::is($getLocal.'Product/Bundle/*')) menu-open active @endif">
                        <i class="nav-icon far fa-box-open"></i>
                        <p>{{__("routes.Products")}} <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('product.list') }}" class="nav-link @if(\Request::is($getLocal.'Product/List'))  active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getProductCount() }}</span></p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('product.add') }}" class="nav-link @if(\Request::is($getLocal.'Product/Add')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Add Product")}}</p>
                            </a>
                        </li>

                        <li class="nav-item has-treeview @if(\Request::is($getLocal.'Product/Bundle/*')) menu-open @endif">
                            <a href="#" class="nav-link @if(\Request::is($getLocal.'Product/Bundle/*')) menu-open active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>
                                    {{__("routes.Bundles")}}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('product.bundle.list')}}" class="nav-link @if(\Request::is($getLocal.'Product/Bundle/List')) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.View All")}}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('product.bundle.add')}}" class="nav-link @if(\Request::is($getLocal.'Product/Bundle/Add')) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.Add")}}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('product.bulk.add') }}" class="nav-link @if(\Request::is($getLocal.'Product/BulkProduct/Add')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Add Multiple Products")}}</p>
                            </a>
                        </li>

                        <li class="nav-item has-treeview @if(\Request::is($getLocal.'Product/Stock/*')) menu-open @endif">
                            <a href="#" class="nav-link @if(\Request::is($getLocal.'Product/Stock')) menu-open active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>
                                    {{__("routes.Stock Management")}}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('product.stock-list')}}" class="nav-link @if(\Request::is($getLocal.'Product/Stock/List')) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.View All")}}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('product.tags.list') }}" class="nav-link @if(\Request::is($getLocal.'Product/Tags/List')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Tags")}} <span class="right badge badge-danger">{{ getTagCount() }}</span></p>
                            </a>
                        </li>

                    </ul>
                </li>

                <!-- Category Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'Category/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'Category/*')) menu-open active @endif">
                        <i class="nav-icon far fa-cubes"></i>
                        <p>{{__("routes.Categories")}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('category.list')}}" class="nav-link  @if(\Request::is($getLocal.'Category/List')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getCategoryCount() }}</span></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('category.add')}}" class="nav-link @if(\Request::is($getLocal.'Category/Add')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Add Category")}}</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{route('category.setting')}}" class="nav-link  @if(\Request::is($getLocal.'Category/Setting')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Category Setting")}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Shop Commerce Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'Order/*')  || \Request::is($getLocal.'Shipping/*') || \Request::is($getLocal.'Invoice/*') || \Request::is($getLocal.'Coupon/*') || \Request::is($getLocal.'Abandoned/*')  || \Request::is($getLocal.'Reports/*') ) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'Order/*')  || \Request::is($getLocal.'Shipping/*') || \Request::is($getLocal.'Invoice/*') || \Request::is($getLocal.'Coupon/*') || \Request::is($getLocal.'Abandoned/*') || \Request::is($getLocal.'Reports/*')  ) menu-open active @endif">
                        <i class="nav-icon far fa-store"></i>
                        <p>
                            {{__("routes.Shop Commerce")}}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item has-treeview @if(\Request::is($getLocal.'Order/*')) menu-open @endif">
                            <a href="#" class="nav-link @if(\Request::is($getLocal.'Order/*')) menu-open active @endif ">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>
                                    {{__("routes.Orders")}}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="{{route('order.alllist')}}" class="nav-link @if(\Request::is($getLocal.'Order/AllList')) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.View All")}}</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('order.parts.list')}}" class="nav-link @if(\Request::is($getLocal.'Order/Parts/List')) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.Order Parts")}}</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('order.manual.add')}}" class="nav-link @if(\Request::is($getLocal.'Order/Manual/Create')) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.Create Order")}}</p>
                                    </a>
                                </li>

                            </ul>
                        </li>

                        <li class="nav-item has-treeview @if(\Request::is($getLocal.'Shipping/*')) menu-open @elseif( \Request::is($getLocal.'Shipping/Default/*')) menu-open  @endif  ">
                            <a href="#" class="nav-link @if(\Request::is($getLocal.'Shipping/*')) menu-open active  @elseif( \Request::is($getLocal.'Shipping/Default/*')) menu-open active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>
                                    {{__("routes.Shipping")}}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('shipping.alllist')}}" class="nav-link @if(\Request::is($getLocal.'Shipping/AllList')) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.Shipping List")}}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('shipping.add')}}" class="nav-link @if(\Request::is($getLocal.'Shipping/add')) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.Add Shippings")}}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('shipping.default-add')}}" class="nav-link  @if(\Request::is($getLocal.'Shipping/Default/Add')) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.Default Setting")}}</p>
                                    </a>
                                </li>
                            </ul>

                        </li>

                        <li class="nav-item has-treeview @if( \Request::is($getLocal.'Invoice/*') ) menu-open @endif  ">
                            <a href="#" class="nav-link @if(  \Request::is($getLocal.'Invoice/*') ) menu-open active @endif ">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>
                                    {{__("routes.Invoices")}}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('invoice.list')}}" class="nav-link @if( \Request::is($getLocal.'Invoice/*') ) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getInvoicesCount() }}</span></p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview @if( \Request::is($getLocal.'Coupon/*') ) menu-open @endif">
                            <a href="#" class="nav-link @if( \Request::is($getLocal.'Coupon/*') ) menu-open active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Coupons")}}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('coupon.list')}}" class="nav-link  @if( \Request::is($getLocal.'Coupon/List')) active @endif">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getCouponCount() }}</span></p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview @if( \Request::is($getLocal.'Abandoned/*') ) menu-open @endif  ">
                            <a href="#" class="nav-link @if(  \Request::is($getLocal.'Abandoned/*') ) menu-open active @endif ">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>
                                    {{__("routes.Abandoned Cart")}}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('abandoned.list')}}" class="nav-link @if( \Request::is($getLocal.'Abandoned/List') ) active @endif">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{getAbandonedCartCount()}}</span></p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview @if( \Request::is($getLocal.'Reports/*') ) menu-open @endif ">
                            <a href="#" class="nav-link @if( \Request::is($getLocal.'Reports/*') ) menu-open active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Reports")}}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>

                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{route('reports.each-sales.list')}}" class="nav-link @if( \Request::is($getLocal.'Reports/EachSales/List') ) active @endif">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>{{__("routes.Analytics Each Sale")}}</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('reports.total-sales.list')}}" class="nav-link @if( \Request::is($getLocal.'Reports/TotalSales/List') ) active @endif">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>{{__("routes.Analytics Total Sales")}}</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('reports.country-sales.list')}}" class="nav-link @if( \Request::is($getLocal.'Reports/CountrySales/List') ) active @endif">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>{{__("routes.Analytics Country Sales")}}</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>{{__("routes.Analytics Card Report")}}</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>{{__("routes.Sales Comparison")}}</p>
                                        </a>
                                    </li>


                                </ul>

                        </li>
                    </ul>
                </li>

                <li class="nav-header">{{__("routes.Others")}}</li>

                <!-- ArtWorkGallery Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'ArtWorkGallery/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'ArtWorkGallery/*')) menu-open active @endif">
                        <i class="nav-icon far fa-th"></i>
                        <p>{{__("routes.Art Submission")}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('art-work-Gallery.list')}}" class="nav-link @if(\Request::is($getLocal.'ArtWorkGallery/List')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{get_pending_artWork()}}</span></p>
                            </a>
                        </li>
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('art-work-Gallery.approved')}}" class="nav-link @if(\Request::is('Event/Add')) active @endif">--}}
{{--                                <i class="fas fa-circle nav-icon"></i>--}}
{{--                                <p>{{__("routes.Approved")}}<span class="right badge badge-danger">2</span></p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('art-work-Gallery.pending')}}" class="nav-link @if(\Request::is('Event/Add')) active @endif">--}}
{{--                                <i class="fas fa-circle nav-icon"></i>--}}
{{--                                <p>{{__("routes.Pending")}}<span class="right badge badge-danger">2</span></p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
                    </ul>
                </li>

                <!-- Event Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'Event/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'Event/*')) menu-open active @endif">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>{{__("routes.Events")}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('event.list')}}" class="nav-link @if(\Request::is($getLocal.'Event/List')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getEventCount() }}</span></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('event.add')}}" class="nav-link @if(\Request::is($getLocal.'Event/Add')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Add")}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Custom Review Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'Customer/Reviews/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'Customer/Reviews/*')) menu-open active @endif">
                        <i class="nav-icon far fa-comment"></i>
                        <p>{{__("routes.Customer Reviews")}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('customer-reviews.list')}}" class="nav-link @if(\Request::is($getLocal.'Customer/Reviews/List')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getCustomerReviewCount() }}</span></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('customer-reviews.add')}}" class="nav-link @if(\Request::is($getLocal.'Customer/Reviews/Add')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Add")}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Contact / Inquiry Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'Contact/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'Contact/*')) menu-open active @endif">
                        <i class="nav-icon far fa-phone-square"></i>
                        <p>{{__("routes.Inquires")}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('contact.list')}}" class="nav-link @if(\Request::is($getLocal.'Contact/List')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getAllInquires() }}</span></p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Admin FAQs Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'Admin/FAQs/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'Admin/FAQs/*')) menu-open active   @endif">
                        <i class="nav-icon far fa-question-circle"></i>
                        <p>{{__("routes.FAQs")}} <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.faqs.viewAll')}}" class="nav-link @if(\Request::is($getLocal.'Admin/FAQs/ViewAll')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.View All")}}<span class="right badge badge-danger">{{ getFaqCount() }}</span></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.faqs.add')}}" class="nav-link @if(\Request::is($getLocal.'Admin/FAQs/Add')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Add")}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Settings Section-->
                <li class="nav-item has-treeview @if(\Request::is($getLocal.'Settings/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is($getLocal.'Settings/*')) menu-open  active @endif">
                        <i class="nav-icon far fa-cogs"></i>
                        <p>{{__("routes.Settings")}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('setting.general-config')}}" class="nav-link @if(\Request::is($getLocal.'Settings/GeneralConfig')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.General")}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('setting.media-config')}}" class="nav-link  @if(\Request::is($getLocal.'Settings/MediaConfig')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Media")}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('setting.display_export_emails')}}" class="nav-link  @if(\Request::is($getLocal.'Settings/Export/Emails')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>{{__("routes.Export Emails")}}</p>
                            </a>
                        </li>
                    </ul>
                </li>


            </ul>
        </nav>
        <!-- /.sidebar-menu -->

    </div>
    <!-- /.sidebar -->

</aside>
<!-- /. Main Sidebar Container End -->
