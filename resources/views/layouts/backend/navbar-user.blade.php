<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-primary elevation-4">
{{--    sidebar-light-primary--}}

<!-- Brand Logo -->
    <a href="{{route('dashboard')}}" class="brand-link">
        <img src="{{asset('images/logo/ShopFurries-Logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="border-radius: 0px ;box-shadow: 0px 0px 0px 0px !important;opacity: .8">
        <span class="brand-text font-weight-light">Shop Furries</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset(profile_pic(Auth::user()->id))}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ ucfirst(Auth::user()->name)  }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{route('dashboard')}}" class="nav-link @if(\Request::is('/Dashboard')) active @endif">
                        <i class="nav-icon far fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item has-treeview @if(\Request::is('User/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is('User/*')) menu-open active @endif">
                        <i class="nav-icon far fa-tags"></i>
                        <p>My Account
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview @if(\Request::is('User/Account')) active @endif">
                        <li class="nav-item">
                            <a href="{{route('user.account')}}" class="nav-link  @if(\Request::is('User/Account')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>Profile</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('user.changePassword')}}" class="nav-link @if(\Request::is('User/ChangePassword')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>Change Password</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview @if(\Request::is('Product/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is('Product/*')) menu-open active @endif">
                        <i class="nav-icon far fa-box-open"></i>
                        <p>Products <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        @hasrole('user')
                        <li class="nav-item">
                            <a href="{{ route('product.list') }}" class="nav-link @if(\Request::is('Product/List'))  active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>My Products</p>
                            </a>
                        </li>
                        @endhasrole

                        <li class="nav-item">
                            <a href="{{ route('product.add') }}" class="nav-link @if(\Request::is('Product/Add')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>Add Product</p>
                            </a>
                        </li>

                    </ul>
                </li>


                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-store"></i>
                        <p>
                            Shop Commerce
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>
                                    Orders
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="{{route('order.mylist')}}" class="nav-link">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>My Orders</p>
                                    </a>
                                </li>

                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>
                                    Invoices
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('invoice.list')}}" class="nav-link">
                                        <i class="far fa-circle nav-icon nav-icon"></i>
                                        <p>My Invoices</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </li>

                <li class="nav-item has-treeview @if(\Request::is('Gallery/*')) menu-open @endif">
                    <a href="#" class="nav-link @if(\Request::is('Gallery/*')) menu-open active @endif">
                        <i class="nav-icon far fa-th"></i>
                        <p>Art Gallery
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview @if(\Request::is('Gallery/List')) active @endif">
                        <li class="nav-item">
                            <a href="{{route('gallery.list')}}" class="nav-link  @if(\Request::is('Gallery/List')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>View All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('gallery.add')}}" class="nav-link @if(\Request::is('Gallery/Add')) active @endif">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>Upload Your Work</p>
                            </a>
                        </li>
                    </ul>
                </li>



            </ul>
        </nav>
        <!-- /.sidebar-menu -->

    </div>
    <!-- /.sidebar -->

</aside>
<!-- /. Main Sidebar Container End -->
<?php
