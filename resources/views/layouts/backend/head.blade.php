<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ShopFurries</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fontawesome-free/css/all.min.css')}}">

    <!-- Font Awesome 5 pro-->
{{--    <link rel="stylesheet" href="{{asset('other-assets/fontawesome-pro-5.12.0-web/css/fontawesome.css')}}">--}}
    <link rel="stylesheet" href="{{asset('other-assets/fontawesome-pro-5.12.0-web/css/all.css')}}">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Flags icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">


    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css')}}">

    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('theme-assets/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/summernote/summernote-bs4.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">

    <!-- jsGrid -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/jsgrid/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/jsgrid/jsgrid-theme.min.css')}}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/toastr/toastr.min.css')}}">
    <!-- fullCalendar -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fullcalendar/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fullcalendar-daygrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fullcalendar-timegrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fullcalendar-bootstrap/main.min.css')}}">


    <!-- Ekko Lightbox -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/ekko-lightbox/ekko-lightbox.css')}}">

    <!-- kartik-v-bootstrap-fileinput -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}">

    <!-- LightBox -->
    <link rel="stylesheet" href="{{asset('other-assets/lightbox/css/lightbox.css')}}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

{{--    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.2/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap 4 input tags plugin-->
    <link href="{{asset('other-assets/Bootstrap-4-Tag-Input-Plugin-jQuery/tagsinput.css')}}" rel="stylesheet" type="text/css">

    <!-- Custom StyleSheet -->
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

</head>
