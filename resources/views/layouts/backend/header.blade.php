<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
{{--    main-header navbar navbar-expand navbar-dark navbar-primary--}}
{{--    navbar-white navbar-light--}}

<!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>

        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{route('home')}}" target="_blank" class="nav-link"><i class="far fa-home mr-2"></i>

                {{__('msgs.Home')}}
                @php
                    /*
                         if(!Auth::user()->hasRole(1)){
                                    echo "User";
                                }else{
                                    echo "Admin";
                                }
                              if(Auth::user()->hasRole(1)){
                                    echo "tt";
                                }else{
                                    echo "ff";
                                }
                             $user = Auth::user();

                            if($user->hasRole(1)){
                                echo "T";
                            }else{
                                echo "F";
                            }
                        */
                @endphp
            </a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{route('shop')}}" target="_blank" class="nav-link"><i class="far fa-store mr-2"></i>{{ __('msgs.Shop') }}</a>
        </li>

        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{route('contact-us')}}" target="_blank" class="nav-link"><i class="far fa-address-book mr-2"></i>{{ __('routes.Contact') }}</a>
        </li>

        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{route('wishlist.list')}}" target="_blank" class="nav-link"><i class="far fa-heart-square mr-2"></i> {{ __('msgs.Your Wishlist') }}</a>
        </li>

    </ul>


    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

        <!-- Messages Dropdown Menu -->
    {{--        <li class="nav-item dropdown">--}}
    {{--            <a class="nav-link" data-toggle="dropdown" href="#">--}}
    {{--                <i class="far fa-comments"></i>--}}
    {{--                <span class="badge badge-danger navbar-badge">3</span>--}}
    {{--            </a>--}}
    {{--            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">--}}
    {{--                <a href="#" class="dropdown-item">--}}
    {{--                    <!-- Message Start -->--}}
    {{--                    <div class="media">--}}
    {{--                        <img src="{{asset('theme-assets/dist/img/user1-128x128.jpg')}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">--}}
    {{--                        <div class="media-body">--}}
    {{--                            <h3 class="dropdown-item-title">--}}
    {{--                                Brad Diesel--}}
    {{--                                <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>--}}
    {{--                            </h3>--}}
    {{--                            <p class="text-sm">Call me whenever you can...</p>--}}
    {{--                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <!-- Message End -->--}}
    {{--                </a>--}}
    {{--                <div class="dropdown-divider"></div>--}}
    {{--                <a href="#" class="dropdown-item">--}}
    {{--                    <!-- Message Start -->--}}
    {{--                    <div class="media">--}}
    {{--                        <img src="{{asset('theme-assets/dist/img/user8-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle mr-3">--}}
    {{--                        <div class="media-body">--}}
    {{--                            <h3 class="dropdown-item-title">--}}
    {{--                                John Pierce--}}
    {{--                                <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>--}}
    {{--                            </h3>--}}
    {{--                            <p class="text-sm">I got your message bro</p>--}}
    {{--                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <!-- Message End -->--}}
    {{--                </a>--}}
    {{--                <div class="dropdown-divider"></div>--}}
    {{--                <a href="#" class="dropdown-item">--}}
    {{--                    <!-- Message Start -->--}}
    {{--                    <div class="media">--}}
    {{--                        <img src="{{asset('theme-assets/dist/img/user3-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle mr-3">--}}
    {{--                        <div class="media-body">--}}
    {{--                            <h3 class="dropdown-item-title">--}}
    {{--                                Nora Silvester--}}
    {{--                                <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>--}}
    {{--                            </h3>--}}
    {{--                            <p class="text-sm">The subject goes here</p>--}}
    {{--                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <!-- Message End -->--}}
    {{--                </a>--}}
    {{--                <div class="dropdown-divider"></div>--}}
    {{--                <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>--}}
    {{--            </div>--}}
    {{--        </li>--}}

    <!-- Notifications Dropdown Menu -->
        {{--        <li class="nav-item dropdown">--}}
        {{--            <a class="nav-link" data-toggle="dropdown" href="#">--}}
        {{--                <i class="far fa-bell"></i>--}}
        {{--                <span class="badge badge-warning navbar-badge">15</span>--}}
        {{--            </a>--}}
        {{--            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">--}}
        {{--                <span class="dropdown-item dropdown-header">15 Notifications</span>--}}
        {{--                <div class="dropdown-divider"></div>--}}
        {{--                <a href="#" class="dropdown-item">--}}
        {{--                    <i class="fas fa-envelope mr-2"></i> 4 new messages--}}
        {{--                    <span class="float-right text-muted text-sm">3 mins</span>--}}
        {{--                </a>--}}
        {{--                <div class="dropdown-divider"></div>--}}
        {{--                <a href="#" class="dropdown-item">--}}
        {{--                    <i class="fas fa-users mr-2"></i> 8 friend requests--}}
        {{--                    <span class="float-right text-muted text-sm">12 hours</span>--}}
        {{--                </a>--}}
        {{--                <div class="dropdown-divider"></div>--}}
        {{--                <a href="#" class="dropdown-item">--}}
        {{--                    <i class="fas fa-file mr-2"></i> 3 new reports--}}
        {{--                    <span class="float-right text-muted text-sm">2 days</span>--}}
        {{--                </a>--}}
        {{--                <div class="dropdown-divider"></div>--}}
        {{--                <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>--}}
        {{--            </div>--}}
        {{--        </li>--}}

        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="flag-icon flag-icon-{{ ( LaravelLocalization::getCurrentLocale() == 'en' ) ? "us" : LaravelLocalization::getCurrentLocale() }}  "></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right p-0">

                @php $langs = ['us' => 'en' , 'de' => 'de'];

                @endphp

{{--                @foreach ($langs as $key => $locale)--}}
{{--                    <a class="dropdown-item flag active"--}}
{{--                       href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), $locale) }}"--}}
{{--                       @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>--}}
{{--                        <i class="flag-icon flag-icon-{{$key}} mr-2"></i>{{ strtoupper($locale) }}--}}
{{--                    </a>--}}
{{--                @endforeach--}}


                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)

                    @if($localeCode == "en")
                        <a class="dropdown-item flag active" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            <i class="flag-icon flag-icon-us mr-2"></i>  {{ $properties['native'] }}
                        </a>
                    @else
                        <a class="dropdown-item flag active" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            <i class="flag-icon flag-icon-{{$localeCode}} mr-2"></i>  {{ $properties['native'] }}
                        </a>
                    @endif


                @endforeach


            </div>
        </li>

        <li class="nav-item dropdown user-menu">
            @if(Auth::check())
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="{{asset(asset(profile_pic(Auth::user()->id)))}}" class="user-image img-circle elevation-2"
                         alt="User Image">
                    <span class="d-none d-md-inline">{{ ucfirst(Auth::user()->name)  }}</span>
                </a>
            @else
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="{{asset('images/placeholders/Profile_avatar_placeholder_lg.png')}}" class="img-circle elevation-2"
                         alt="User Image">
                    <span class="d-none d-md-inline">Shopfurries</span>
                </a>
            @endif

            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                @if(Auth::check())
                    <li class="user-header bg-gradient-primary">
                        <img src="{{asset(asset(profile_pic(Auth::user()->id)))}}" class="img-circle elevation-2"
                             alt="User Image">
                        <p>
                            {{ ucfirst(Auth::user()->name)  }}
                            <small>Member since Nov. 2012</small>
                        </p>
                    </li>
                @else
                    <li class="user-header bg-gradient-primary">
                        <img src="{{asset('images/placeholders/Profile_avatar_placeholder_lg.png')}}" class="img-circle elevation-2"
                             alt="User Image">
                        <p>
                            Member <small>Member since Nov. 2012</small>
                        </p>
                    </li>
                @endif

                <!-- Menu Footer-->
                <li class="user-footer">
                    <a href="{{url( app()->getLocale().'/Dashboard' )}}" class="btn btn-default btn-flat">{{__("routes.Profile")}}</a>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();"
                       class="btn btn-default btn-flat float-right">{{ __('routes.Logout') }}</a>

                    <form id="logout-form" action="{{ route('logout')}}#Section" method="POST" style="display: none;">
                        @csrf
                    </form>
                    </div>


                </li>
            </ul>


    </ul>
</nav>
<!-- /.navbar -->
