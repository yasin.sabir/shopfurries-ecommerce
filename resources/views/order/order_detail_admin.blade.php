@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> {{__("routes.Order Detail")}}</h1>
                            <form action="{{route('order.pdfgenerate')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="order_id" value="{{$id}}"/>
                                <input type="submit" class="ml-2 btn btn-primary btn-sm" value="{{__("routes.Generate PDF")}}">
                            </form>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Orders")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('order.edit' , $id)}}">{{__("routes.Detail")}}</a></li>
                        </ol>
                    </div>
                </div>





            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('order.update', $id)}}" method="post" enctype="multipart/form-data">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        {{__("routes.Order Detail")}}
                                    </h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i></button>
                                    </div>
                                </div>
                                    {{--<form action="{{route('order.update', $id)}}" method="post" enctype="multipart/form-data">--}}
                                    @csrf
                                    <input type="hidden" value="{{$id}}" name="id">
                                    <input type="hidden" value="" name="status">
                                    <input type="hidden" value="{{ isset($order_details['payment']) ? $order_details['payment'] :"" }}" name="payment">
                                    <input type="hidden" value="{{ isset($order_details['shipping']) ? $order_details['shipping'] :"" }}" name="shipping">
                                    <input type="hidden" value="{{ isset($order_details['other-country']) ? $order_details['other-country'] :"" }}" name="ocountry">

                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4 pr-5">
                                                <label for="exampleInputEmail1">{{__("routes.Name")}}:</label>
                                                <input type="text" class="form-control form-control-sm" id="name" name="name" value="{{ isset($order_details['name']) ? $order_details['name'] :"" }}" readonly>
                                            </div>
                                            <div class="col-md-4 pr-5">
                                                <label for="email">{{__("routes.Email")}}:</label>
                                                <input type="text" class="form-control form-control-sm" id="email" name="email"
                                                       value="{{ isset($order_details['email']) ? $order_details['email'] :"" }}" readonly>
                                            </div>
                                            <div class="col-md-4 pr-5">
                                                <label for="phone">{{__("routes.Phone No")}}:</label>
                                                <input type="text" class="form-control form-control-sm" id="phone" name="phone"
                                                       value="{{ isset($order_details['mobile']) ? $order_details['mobile'] :"" }}" readonly>
                                            </div>
                                            <div class="col-md-4 pr-5 mt-3">
                                                <label for="address">{{__("routes.Address")}}:</label>
                                                <input type="text" class="form-control form-control-sm" id="address" name="address"
                                                       value="{{ isset($order_details['address']) ? $order_details['address'] :"" }}" readonly>
                                            </div>
                                            <div class="col-md-4 pr-5 mt-3">
                                                <label for="pcode">{{__("routes.Postal Code")}}:</label>
                                                <input type="text" class="form-control form-control-sm" id="pcode" name="pcode" value="{{ isset($order_details['pcode']) ? $order_details['pcode'] :"" }}" readonly>
                                            </div>
                                            <div class="col-md-4 pr-5 mt-3">
                                                <label for="city">{{__("routes.City")}}:</label>
                                                <input type="text" class="form-control form-control-sm" id="city" name="city"
                                                       value="{{ isset($order_details['city']) ? $order_details['city'] :"" }}" readonly>
                                            </div>
                                            <div class="col-md-4 pr-5 mt-3">
                                                <label for="country">{{__("routes.Country")}}:</label>
                                                <input type="text" class="form-control form-control-sm" id="country" name="country"
                                                       value="{{ isset($order_details['d-country']) ? $order_details['d-country'] :"" }}" readonly>
                                            </div>
                                            <div class="col-md-4 pr-5 mt-3">
                                                <label for="order_id">{{__("routes.Order ID")}}:</label>
                                                <input type="text" class="form-control form-control-sm" id="order_id" name=""
                                                       value="{{$order_id}}" readonly>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        {{__("routes.Additional Details")}}
                                    </h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i></button>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-md-4 pr-5 ">
                                            <label for="exampleInputEmail1">{{__("routes.Other Address")}}:</label>
                                            <input type="text" class="form-control form-control-sm" id="oaddress" name="oaddress"
                                                   value="{{ isset($order_details['other-address']) ? $order_details['other-address'] :"" }}" disabled>
                                        </div>
                                        <div class="col-md-4 pr-5 ">
                                            <label for="exampleInputEmail1">{{__("routes.Other Postal Code")}}:</label>
                                            <input type="text" class="form-control form-control-sm" id="opcode" name="opcode"
                                                   value="{{ isset($order_details['other-pcode']) ? $order_details['other-pcode'] :"" }}" disabled>
                                        </div>
                                        <div class="col-md-4 pr-5 ">
                                            <label for="exampleInputEmail1">{{__("routes.Other City")}}:</label>
                                            <input type="text" class="form-control form-control-sm" id="ocity" name="ocity"
                                                   value="{{ isset($order_details['other-city']) ? $order_details['other-city'] :"" }}" disabled>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-6 pr-5 mt-4">
                                            <label for="country">{{__("routes.Order Status")}}:</label>
                                            <select id="country" name="status" class="form-control-sm">
                                                <option value="Processing" {{ ( $order['status'] == "Processing") ? 'selected' : '' }}/>{{__("routes.Processing")}}</option>
                                                <option value="Hold" {{ ( $order['status'] == "Hold") ? 'selected' : '' }}>{{__("routes.Hold")}}</option>
                                                <option value="Cancel" {{ ( $order['status'] == "Cancel") ? 'selected' : '' }}>{{__("routes.Canceled")}}</option>
                                                <option value="Confirm" {{ ( $order['status'] == "Confirm") ? 'selected' : '' }}>{{__("routes.Confirm")}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 pr-5 mt-4 ">
                                            <input type="submit" class="btn btn-primary btn-sm" value="{{__("routes.Update Status")}}">
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                </div>
                    {{-- </form>--}}

                <div class="row mt-5">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Product List")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>{{__("routes.Product Name")}}</th>
                                        <th>{{__("routes.Product Image")}}</th>
                                        <th>{{__("routes.Additional Details")}}</th>
                                        <th>{{__("routes.Price")}}</th>
                                        <th>{{__("routes.Quantity")}}</th>
                                        <th>{{__("routes.Subtotal")}}</th>
                                        <th>{{__("routes.Action")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php
                                        $num=1;
                                        $subtotal=[];
                                    @endphp
                                    @forelse($product_detail as $data)
                                        <tr>
                                            <td>{{$num}}</td>
                                            <td>{{($data['title'])}}</td>
                                            <td><img style="width:120px;height:60px;" src="{{asset('storage/'.$data['image'])}}"></td>
                                            <td></td>
                                            <td class="text-center">${{($data['price'])}}</td>
                                            <td></td>
                                            <td class="text-center">${{($data['price'])}}</td>
                                            <td></td>
                                        </tr>
                                        @php
                                            $num++;
                                            $subtotal[] =  $data['price'];
                                        @endphp

                                    @empty
                                        <tr>
                                            <td colspan="7" class="text-center">{{__("routes.No Coupon Found")}}</td>
                                        </tr>
                                    @endforelse
                                    @php
                                        $subtotal = array_sum($subtotal);
                                    @endphp
                                    <tr>
                                        <td colspan="6" class="text-center"></td>
                                        <td colspan="1" class="text-center">${{($subtotal)}}</td>
                                        <td colspan="1" class="text-center"></td>

                                    </tr>

                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                </div>

            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->


@endsection

@section('page-script')

    <script type="text/javascript">

        $('#example1').DataTable({
            "paging": true,
            "pageLength": 7,
            "lengthChange": false,
            "responsive": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });

    </script>
@endsection
