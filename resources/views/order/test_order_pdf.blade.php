<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <title>Product Info</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>

        .text-right {
            text-align: right;
        }

        .m-0 { margin:0!important; }
        .m-1 { margin:.25rem!important; }
        .m-2 { margin:.5rem!important; }
        .m-3 { margin:1rem!important; }
        .m-4 { margin:1.5rem!important; }
        .m-5 { margin:3rem!important; }

        .mt-0 { margin-top:0!important; }
        .mr-0 { margin-right:0!important; }
        .mb-0 { margin-bottom:0!important; }
        .ml-0 { margin-left:0!important; }
        .mx-0 { margin-left:0!immortant;margin-right:0!immortant; }
        .my-0 { margin-top:0!important;margin-bottom:0!important; }

        .mt-1 { margin-top:.25rem!important; }
        .mr-1 { margin-right:.25rem!important; }
        .mb-1 { margin-bottom:.25rem!important; }
        .ml-1 { margin-left:.25rem!important; }
        .mx-1 { margin-left:.25rem!important;margin-right:.25rem!important; }
        .my-1 { margin-top:.25rem!important;margin-bottom:.25rem!important; }

        .mt-2 { margin-top:.5rem!important; }
        .mr-2 { margin-right:.5rem!important; }
        .mb-2 { margin-bottom:.5rem!important; }
        .ml-2 { margin-left:.5rem!important; }
        .mx-2 { margin-right:.5rem!important;margin-left:.5rem!important; }
        .my-2 { margin-top:.5rem!important;margin-bottom:.5rem!important; }

        .mt-3 { margin-top:1rem!important; }
        .mr-3 { margin-right:1rem!important; }
        .mb-3 { margin-bottom:1rem!important; }
        .ml-3 { margin-left:1rem!important; }
        .mx-3 { margin-right:1rem!important;margin-left:1rem!important; }
        .my-3 { margin-bottom:1rem!important;margin-top:1rem!important; }

        .mt-4 { margin-top:1.5rem!important; }
        .mr-4 { margin-right:1.5rem!important; }
        .mb-4 { margin-bottom:1.5rem!important; }
        .ml-4 { margin-left:1.5rem!important; }
        .mx-4 { margin-right:1.5rem!important;margin-left:1.5rem!important; }
        .my-4 { margin-top:1.5rem!important;margin-bottom:1.5rem!important; }

        .mt-5 { margin-top:3rem!important; }
        .mr-5 { margin-right:3rem!important; }
        .mb-5 { margin-bottom:3rem!important; }
        .ml-5 { margin-left:3rem!important; }
        .mx-5 { margin-right:3rem!important;margin-left:3rem!important; }
        .my-5 { margin-top:3rem!important;margin-bottom:3rem!important; }

        .mt-auto { margin-top:auto!important; }
        .mr-auto { margin-right:auto!important; }
        .mb-auto { margin-bottom:auto!important; }
        .ml-auto { margin-left:auto!important; }
        .mx-auto { margin-right:auto!important;margin-left:auto!important; }
        .my-auto { margin-bottom:auto!important;margin-top:auto!important; }

        .p-0 { padding:0!important; }
        .p-1 { padding:.25rem!important; }
        .p-2 { padding:.5rem!important; }
        .p-3 { padding:1rem!important; }
        .p-4 { padding:1.5rem!important; }
        .p-5 { padding:3rem!important; }

        .pt-0 { padding-top:0!important; }
        .pr-0 { padding-right:0!important; }
        .pb-0 { padding-bottom:0!important; }
        .pl-0 { padding-left:0!important; }
        .px-0 { padding-left:0!important;padding-right:0!important; }
        .py-0 { padding-top:0!important;padding-bottom:0!important; }

        .pt-1 { padding-top:.25rem!important; }
        .pr-1 { padding-right:.25rem!important; }
        .pb-1 { padding-bottom:.25rem!important; }
        .pl-1 { padding-left:.25rem!important; }
        .px-1 { padding-left:.25rem!important;padding-right:.25rem!important; }
        .py-1 { padding-top:.25rem!important;padding-bottom:.25rem!important; }

        .pt-2 { padding-top:.5rem!important; }
        .pr-2 { padding-right:.5rem!important; }
        .pb-2 { padding-bottom:.5rem!important; }
        .pl-2 { padding-left:.5rem!important; }
        .px-2 { padding-right:.5rem!important;padding-left:.5rem!important; }
        .py-2 { padding-top:.5rem!important;padding-bottom:.5rem!important; }

        .pt-3 { padding-top:1rem!important; }
        .pr-3 { padding-right:1rem!important; }
        .pb-3 { padding-bottom:1rem!important; }
        .pl-3 { padding-left:1rem!important; }
        .py-3 { padding-bottom:1rem!important;padding-top:1rem!important; }
        .px-3 { padding-right:1rem!important;padding-left:1rem!important; }

        .pt-4 { padding-top:1.5rem!important; }
        .pr-4 { padding-right:1.5rem!important; }
        .pb-4 { padding-bottom:1.5rem!important; }
        .pl-4 { padding-left:1.5rem!important; }
        .px-4 { padding-right:1.5rem!important;padding-left:1.5rem!important; }
        .py-4 { padding-top:1.5rem!important;padding-bottom:1.5rem!important; }

        .pt-5 { padding-top:3rem!important; }
        .pr-5 { padding-right:3rem!important; }
        .pb-5 { padding-bottom:3rem!important; }
        .pl-5 { padding-left:3rem!important; }
        .px-5 { padding-right:3rem!important;padding-left:3rem!important; }
        .py-5 { padding-top:3rem!important;padding-bottom:3rem!important; }

        .d-block { display: block; }

        .custom-fonts-adds {
            font-size: 12px;
            line-height: 13px;
            text-align: left;
            text-transform: none;
        }

        .custom-product-desciption {
            font-size: 12px;
        }

        .contact-detail > ul {
            list-style: none;
        }

        ul#details-list {
            list-style: none;
        }

        ul#details-list {
            padding-left: 0;
        }

        ul#details-list span.product-price {
            float: right;
        }

    </style>

</head>

<body class="login-page" style="background: white">

<div class="row">
    <div class="col-xs-7 mt-5">
        <div class="custom-fonts-adds">
            <strong>ShopFurries</strong>
            <br> 123 Company Ave. <br>
            Toronto, Ontario <br>
            P: (416) 123-4567 <br>
            E: copmany@company.com <br>
            <br>
        </div>
    </div>

    <div class="col-xs-4 text-right">
        <div class="mt-0">
            <img style="width: 100px;" src="{{asset('front-end/assets/img/ShopFurries-LOGO.png')}}" alt="logo">
        </div>
    </div>
</div>

<div class="mb-5"></div>


<div class="row">
    <div class="col-xs-12" style="text-align: center;">
        <h3>Invoice</h3>
    </div>
</div>


<div class="row mb-4">
    <div class="col-xs-7">
       <div class="contact-detail">
           <ul class="pl-0">
               <li><strong>Order #No:</strong>{{" ".$order->order_id}} </li>
               <li><strong>Invoice #No:</strong>{{"  SH_".$id}}</li>
               <li><strong>Order Date:</strong>{{" ".\Carbon\Carbon::parse($order->created_at)->format('d/m/y g:i:a')}}</li>
               <li><strong>Name:</strong>{{" ".ucwords($order_details['name'])}}</li>
               <li><strong>Email:</strong>{{" ".$order_details['email']}}</li>
               <li><strong>Address:</strong>{{" ".$order_details['customerAddress']}}</li>
               <li><strong>Phone No:</strong>{{" ".$order_details['mobile']}}</li>
           </ul>
       </div>
    </div>
    <div class="col-xs-4 text-right">
        <div class="mt-5">
            <div class="contact-detail">
            </div>
        </div>
    </div>
</div>


<div class="row mt-4">
    <div class="col-xs-12">

        <div class="order-details">
            <table id="example1" class="table table-bordered table-striped" style="margin-top: 50px;width: 100%;border-collapse: collapse;">
                <thead>
                <tr>
                    <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Product</th>
                    <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Quantity</th>
                    <th style="text-align: center;border: 1px solid #ddd;padding: 10px;">Price</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $num=1;
                    $subtotal=[];
                @endphp

                @forelse($order_log as $data)
                    <tr style="background-color: #f2f2f2;border: 1px solid #f1f1f1;">
                        <td style="text-align: center;border: 1px solid #ddd;">{{$data['product_name']}}</td>
                        <td style="text-align: center;border: 1px solid #ddd;">{{$data['qty']}}</td>
                        <td style="text-align: center;border: 1px solid #ddd;">{{$data['price']}}</td>
                    </tr>
                @empty
                @endforelse

                </tbody>
            </table>

            <table style="width:100%;margin-top: 50px;">
                <tr style="width: 100%;text-align: left">
                    <th>Additional Details</th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="font-weight: 400">Selected Country:</th>
                    <th></th>
                    <th style="font-weight: 400">{{$orderCal['selected_country']}}</th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="font-weight: 400">Cost Per Country Weight (Kg):</th>
                    <th></th>
                    <th style="font-weight: 400">${{$orderCal['cost_per_country_weight']}}</th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="font-weight: 400">Total Products Weight (Kg):</th>
                    <th></th>
                    <th style="font-weight: 400">{{$orderCal['total_products_weight']."(Kg)"}}</th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="width: 79%;"></th>
                    <th><div style="width: 50px;"></div></th>
                    <th></th>
                </tr>
            </table>

            <table style="width:100%;margin-top: 50px;">
                <tr style="width: 100%;text-align: left">
                    <th>Order Details</th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="font-weight: 400">Total Weight Cost:</th>
                    <th></th>
                    <th style="font-weight: 400">${{number_format($orderCal['weight_cost'],2)}}</th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="font-weight: 400">Shipping Cost - {{ucfirst($orderCal['selected_country'])}}:</th>
                    <th></th>
                    <th style="font-weight: 400">${{$orderCal['shipping_cost']}}</th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="font-weight: 400">Subtotal of Products:</th>
                    <th></th>
                    <th style="font-weight: 400">${{$orderCal['subtotal_of_products']}}</th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="font-weight: 400">Subtotal:</th>
                    <th></th>
                    <th style="font-weight: 400">
                        @php
                            $subTotal = ( $orderCal['subtotal_of_products'] + $orderCal['shipping_cost'] + $orderCal['weight_cost'] );
                        @endphp
                        {{"$".number_format($subTotal,2)}}
                    </th>
                </tr>

                @if( !empty($orderCal['coupon_discount']) && !empty($orderCal['coupon_type']) )
                    <tr style="width: 100%;text-align: left">
                        <th style="font-weight: 400">Coupon Type:</th>
                        <th style="font-weight: 400">{{$orderCal['coupon_type']}}</th>
                    </tr>
                    <tr style="width: 100%;text-align: left">
                        <th style="font-weight: 400">Coupon Discount:</th>
                        <th style="font-weight: 400">{{$orderCal['coupon_discount']}}</th>
                    </tr>
                @endif

                <tr style="width: 100%;text-align: left">
                    <th colspan="3"><hr></th>
                </tr>
                <tr style="width: 100%;text-align: left">
                    <th style="width: 80%;">Total Price:</th>
                    <th><div style="width: 50px;"></div></th>
                    <th>${{$orderCal['subtotal']}}</th>
                </tr>
            </table>

        </div>

    </div>
</div>

<div style="margin-bottom: 0px">&nbsp;</div>

</body>
