@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">
                           <h1>{{__("routes.Order")}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Orders")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('order.manual.add')}}">{{__("routes.Add")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>



        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.New Order")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{route('order.manual.process')}}" id="product_process_form" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div class="row">
                                        <div class="col-md-3 mb-5">
                                            <a href="#_" class="btn btn-success btn-sm mr-2" id="add-item"><i class="fa fa-plus"></i></a>
                                            <a href="#_" class="btn btn-danger btn-sm" id="remove-item"><i class="fa fa-minus"></i></a>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5 pr-5 mb-5">
                                            <label>Select User:</label>
                                            <select class="form-control form-control-sm @error('user_id') is-invalid @enderror" name="user_id" id="user_id">
                                                <option value>Users...</option>
                                                @forelse($users as $key => $val)
                                                    <option value="{{$val->id}}">{{ucwords($val->name)}}</option>
                                                @empty @endforelse
                                            </select>
                                            @error('user_id')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6 pr-5">
                                            <label>Items</label>
                                        </div>
                                        <div class="col-md-3 text-left">
                                            <label>Quantity</label>
                                        </div>
                                    </div>

                                    <div class="add-items-fields">
                                        <div class="row" id="field-section-0">
                                            <div class="col-md-5 pr-5">
                                                <div class="form-group">
                                                    <select class="form-control form-control-sm" name="selected_products[]" id="selected_products" data-placeholder="Select a Products" style="width: 100%;">
                                                        @forelse($products as $key => $product)
                                                            <option value="{{$product->id}}">{{ucwords($product->title)}}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="number" class="form-control form-control-sm" min="1" id="quanity-manual-order" name="quantity[]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <input type="button" class="btn btn-success" id="add_products" value="{{__("routes.Add Product")}}"
                                                           tabindex="2">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

{{--                        @if(isset($selected_products) && count($selected_products) > 0)--}}
{{--                            <form action="{{route('order.manual.create')}}" method="post" enctype="multipart/form-data">--}}
{{--                                @csrf--}}

{{--                                <div class="card">--}}
{{--                                    <div class="card-header">--}}
{{--                                        <h3 class="card-title">--}}
{{--                                            {{__("routes.Order Detail")}}--}}
{{--                                        </h3>--}}
{{--                                        <div class="card-tools">--}}
{{--                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i--}}
{{--                                                    class="fas fa-minus"></i></button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <!-- /.card-header -->--}}
{{--                                    <div class="card-body">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-md-3"><label for="">Products </label></div>--}}
{{--                                            <div class="col-md-3"><label>Quantity</label></div>--}}
{{--                                            <div class="col-md-3"><label>Price</label></div>--}}
{{--                                        </div>--}}

{{--                                        <div class="row mt-4">--}}
{{--                                            @forelse($selected_products as $key => $val)--}}
{{--                                                <div class="col-md-3">--}}
{{--                                                    <span>{{$val['title']}}</span>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3">--}}
{{--                                                    <span>{{$val['quantity']}}</span>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3">--}}
{{--                                                    <span>${{number_format($val['price'],1)}}</span>--}}
{{--                                                </div>--}}
{{--                                            @empty--}}

{{--                                            @endforelse--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}


{{--                            </form>--}}


{{--                        @endif--}}


                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Order</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete this order</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection

@section('page-script')

    <script type="text/javascript">

    $('.select2').select2();
    //=======================================================================================================

    $("#add_products").click(function () {

        $("#product_process_form").submit();

        {{--var route_url = "{{route('order.manual.process')}}";--}}
        {{--var selected_products = "";--}}
        {{--var quantity          = "";--}}
        {{--var frm = $('#product_process_form');--}}

        // var ff = frm.serializeArray();
        //
        // $.ajax({
        //     context: this,
        //     url: route_url,
        //     type: 'post',
        //     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //     data: {data: frm.serializeArray()},
        //     success: function (response) {
        //         console.log(JSON.parse(response[0].selected_products));
        //     }
        // });


    });

    //=======================================================================================================

    var addItems        = $("#add-item");
    var removeItems     = $("#remove-item");
    var addItemsFields  = $(".add-items-fields");
    var clone           = $("#selected_products").clone();
    var maxItems        = 1000;
    var x               = 1;
    //var val             = "";

    $(addItems).click(function (e) { //on add input button click
        e.preventDefault();

        if (x < maxItems) { //max input box allowed
            var val = x;
            x++; //text box increment

            $(addItemsFields).append('<div class="row" id="field-section-'+val+'">' +
                '<div class="col-md-5 pr-5">\n' +
                '<div class="form-group">\n' +
                ' <select class="select2 form-control form-control-sm" name="selected_products[]" id="selected_products" data-placeholder="Select a Products" style="width: 100%;">\n' +
                '   @forelse($products as $key => $product)\n' +
                '     <option value="{{$product->id}}">{{ucwords($product->title)}}</option>\n' +
                '   @empty\n' +
                '   @endforelse\n' +
                '</select>\n' +
                '\n' +
                '</div>\n' +
                '</div>\n' +
                '<div class="col-md-3">\n' +
                '<div class="form-group">\n' +
                '<input type="number" min="1" class="form-control form-control-sm" name="quantity[]">\n' +
                '</div>\n' +
                '</div>'); //add input box
        }
    });

    $(removeItems).on("click", function (e) { //user click on remove fields
        e.preventDefault();
        if(x !== 1){
            $(" #field-section-"+(x-1)+" ").remove();
            x--;
        }
    });



    //=======================================================================================================



    $('#type').on('change', function () {
        if (this.value === "percent") {
            $('#discount').attr('maxlength', '2');
            if ($('#discount').val() > 100) {
                $('#discount').val('100');
            }
        } else {
            $('#discount').removeAttr('maxlength');
        }
    });

    $('.select2bs4').select2()
    $('input[name="startandexpire"]').daterangepicker();

    //=======================================================================================================

    // $("input[name='checkAll']").click(function(){
    //     $("input[name='faq']").not(this).prop('checked', this.checked);
    // });

    // =======================================================================================================

    $(document).on('click', '.custom-delete-btn', function () {

        var id = $(this).attr("coupon_id");
        var route = '{{ route('coupon.delete', ['id' => 'id']) }}';
        route = route.replace('id', id);

        $("#delete-modal").modal('show');
        $("#delete-modal-form").attr("action", route);

        $('.delete_modal_btn').on('click', function (e) {
            e.preventDefault();
            // alert("ds");
            $("#delete-modal-form").submit();
        })
    });

    //=======================================================================================================

    $('#example1').DataTable({
        "paging": true,
        "responsive": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 7,
        "autoWidth": false,
    });

    //=======================================================================================================

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#thumbnail-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#thumbnail").change(function () {
        readURL(this);
    });

    //=======================================================================================================

    function randomPassword(length) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }

    //=======================================================================================================

    function generate() {
        jQuery('[name="coupon"]').val(randomPassword(20))
    }

    //=======================================================================================================

    </script>

@endsection
