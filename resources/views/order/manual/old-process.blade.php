@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">

                            <h1>{{__("routes.Order")}}</h1>
                            <form method="post" action="{{route('order.manual.delete-temp-order')}}">
                                @csrf
                                <input type="submit" class="ml-3 btn btn-primary btn-sm" value="Return Back">
                                @forelse($order_logs as $key => $val)
                                    <input type="hidden" name="order_log[]" value="{{$val}}">
                                @empty
                                @endforelse
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Orders")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('order.manual.process')}}">{{__("routes.Processing")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>



        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-12">
                    @if(isset($selected_products) && count($selected_products) > 0)

                        <!-- Product Details -->
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        {{__("routes.Order Detail")}}
                                    </h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i></button>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-3"><label for="">Products </label></div>
                                        <div class="col-md-3"><label>Quantity</label></div>
                                        <div class="col-md-3"><label>Price</label></div>
                                    </div>
                                    @forelse($selected_products as $key => $val)
                                        <div class="row mt-4">
                                            <div class="col-md-3">
                                                <span>{{ucwords($val['title'])}}</span>
                                            </div>
                                            <div class="col-md-3">
                                                <span>{{$val['quantity']."x"}}</span>
                                            </div>
                                            <div class="col-md-3">
                                                <span>${{number_format($val['price'],1)}}</span>
                                            </div>
                                        </div>

                                    @empty
                                    @endforelse
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <form method="post" id="coupon-apply" class="couponapply">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-5 text-left">
                                                        <div class="form-group">
                                                            <input type="text" name="coupon" id="coupon" class="form-control " placeholder="Enter Coupon">
                                                            @error('coupon')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 text-left">
                                                        <div class="form-group">
                                                            <input type="submit" id="coupon-btn" class="btn btn-primary" value="Apply Coupon">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif
                    </div>

                    <div class="col-md-12">

                    @if(isset($selected_products) && count($selected_products) > 0)
                        <!-- create order form  -->
                            <form action="{{route('order.manual.order-create')}}" method="post" enctype="multipart/form-data">

                                <!-- Shipping Details -->
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">
                                            {{__("routes.Shipping Address")}}
                                        </h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                    class="fas fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="dalivery-addres">
                                                    <h4>Billing Address:</h4>
                                                    <input type="hidden" name="_token" value="DZqOYr13Ntf4foYo6KpirDCSQZTYJ2zMBeY0w3Sl">                            <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>Name: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" id="name" name="name" class="@error('name') is-invalid @enderror form-control form-control-sm name">
                                                            @error('name')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>Email: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" id="email" name="email" class="@error('email') is-invalid @enderror form-control form-control-sm  address  ">
                                                            @error('email')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>Address: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" id="address" name="address" class="@error('address') is-invalid @enderror form-control form-control-sm address  ">
                                                            @error('address')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>Mobile No: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" id="mobile" name="mobile" class=" @error('mobile') is-invalid @enderror form-control form-control-sm address  ">
                                                            @error('mobile')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>Post Code: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" id="pcode" name="pcode" class="@error('pcode') is-invalid @enderror form-control form-control-sm pcode  ">
                                                            @error('pcode')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>City: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" id="city" name="city" class="@error('city') is-invalid @enderror form-control form-control-sm city  ">
                                                            @error('city')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>State: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" id="state" name="state" class="@error('state') is-invalid @enderror form-control form-control-sm city">
                                                            @error('state')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>Country: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <select id="country" name="d-country" class="@error('d-country') is-invalid @enderror form-control form-control-sm">
                                                                <option value="">Select Country</option>
                                                                <option value="Afghanistan">Afghanistan</option>
                                                                <option value="Åland Islands">Åland Islands</option>
                                                                <option value="Albania">Albania</option>
                                                                <option value="Algeria">Algeria</option>
                                                                <option value="American Samoa">American Samoa</option>
                                                                <option value="Andorra">Andorra</option>
                                                                <option value="Angola">Angola</option>
                                                                <option value="Anguilla">Anguilla</option>
                                                                <option value="Antarctica">Antarctica</option>
                                                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                <option value="Argentina">Argentina</option>
                                                                <option value="Armenia">Armenia</option>
                                                                <option value="Aruba">Aruba</option>
                                                                <option value="Australia">Australia</option>
                                                                <option value="Austria">Austria</option>
                                                                <option value="Azerbaijan">Azerbaijan</option>
                                                                <option value="Bahamas">Bahamas</option>
                                                                <option value="Bahrain">Bahrain</option>
                                                                <option value="Bangladesh">Bangladesh</option>
                                                                <option value="Barbados">Barbados</option>
                                                                <option value="Belarus">Belarus</option>
                                                                <option value="Belgium">Belgium</option>
                                                                <option value="Belize">Belize</option>
                                                                <option value="Benin">Benin</option>
                                                                <option value="Bermuda">Bermuda</option>
                                                                <option value="Bhutan">Bhutan</option>
                                                                <option value="Bolivia">Bolivia</option>
                                                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                                <option value="Botswana">Botswana</option>
                                                                <option value="Bouvet Island">Bouvet Island</option>
                                                                <option value="Brazil">Brazil</option>
                                                                <option value="British Indian Ocean Territory">British Indian Ocean Territory
                                                                </option>
                                                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                <option value="Bulgaria">Bulgaria</option>
                                                                <option value="Burkina Faso">Burkina Faso</option>
                                                                <option value="Burundi">Burundi</option>
                                                                <option value="Cambodia">Cambodia</option>
                                                                <option value="Cameroon">Cameroon</option>
                                                                <option value="Canada">Canada</option>
                                                                <option value="Cape Verde">Cape Verde</option>
                                                                <option value="Cayman Islands">Cayman Islands</option>
                                                                <option value="Central African Republic">Central African Republic</option>
                                                                <option value="Chad">Chad</option>
                                                                <option value="Chile">Chile</option>
                                                                <option value="China">China</option>
                                                                <option value="Christmas Island">Christmas Island</option>
                                                                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                                <option value="Colombia">Colombia</option>
                                                                <option value="Comoros">Comoros</option>
                                                                <option value="Congo">Congo</option>
                                                                <option value="Congo, The Democratic Republic of The">Congo, The Democratic
                                                                    Republic of The
                                                                </option>
                                                                <option value="Cook Islands">Cook Islands</option>
                                                                <option value="Costa Rica">Costa Rica</option>
                                                                <option value="Cote D'ivoire">Cote D'ivoire</option>
                                                                <option value="Croatia">Croatia</option>
                                                                <option value="Cuba">Cuba</option>
                                                                <option value="Cyprus">Cyprus</option>
                                                                <option value="Czech Republic">Czech Republic</option>
                                                                <option value="Denmark">Denmark</option>
                                                                <option value="Djibouti">Djibouti</option>
                                                                <option value="Dominica">Dominica</option>
                                                                <option value="Dominican Republic">Dominican Republic</option>
                                                                <option value="Ecuador">Ecuador</option>
                                                                <option value="Egypt">Egypt</option>
                                                                <option value="El Salvador">El Salvador</option>
                                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                <option value="Eritrea">Eritrea</option>
                                                                <option value="Estonia">Estonia</option>
                                                                <option value="Ethiopia">Ethiopia</option>
                                                                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                                <option value="Faroe Islands">Faroe Islands</option>
                                                                <option value="Fiji">Fiji</option>
                                                                <option value="Finland">Finland</option>
                                                                <option value="France">France</option>
                                                                <option value="French Guiana">French Guiana</option>
                                                                <option value="French Polynesia">French Polynesia</option>
                                                                <option value="French Southern Territories">French Southern Territories</option>
                                                                <option value="Gabon">Gabon</option>
                                                                <option value="Gambia">Gambia</option>
                                                                <option value="Georgia">Georgia</option>
                                                                <option value="Germany">Germany</option>
                                                                <option value="Ghana">Ghana</option>
                                                                <option value="Gibraltar">Gibraltar</option>
                                                                <option value="Greece">Greece</option>
                                                                <option value="Greenland">Greenland</option>
                                                                <option value="Grenada">Grenada</option>
                                                                <option value="Guadeloupe">Guadeloupe</option>
                                                                <option value="Guam">Guam</option>
                                                                <option value="Guatemala">Guatemala</option>
                                                                <option value="Guernsey">Guernsey</option>
                                                                <option value="Guinea">Guinea</option>
                                                                <option value="Guinea-bissau">Guinea-bissau</option>
                                                                <option value="Guyana">Guyana</option>
                                                                <option value="Haiti">Haiti</option>
                                                                <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald
                                                                    Islands
                                                                </option>
                                                                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)
                                                                </option>
                                                                <option value="Honduras">Honduras</option>
                                                                <option value="Hong Kong">Hong Kong</option>
                                                                <option value="Hungary">Hungary</option>
                                                                <option value="Iceland">Iceland</option>
                                                                <option value="India">India</option>
                                                                <option value="Indonesia">Indonesia</option>
                                                                <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                                <option value="Iraq">Iraq</option>
                                                                <option value="Ireland">Ireland</option>
                                                                <option value="Isle of Man">Isle of Man</option>
                                                                <option value="Israel">Israel</option>
                                                                <option value="Italy">Italy</option>
                                                                <option value="Jamaica">Jamaica</option>
                                                                <option value="Japan">Japan</option>
                                                                <option value="Jersey">Jersey</option>
                                                                <option value="Jordan">Jordan</option>
                                                                <option value="Kazakhstan">Kazakhstan</option>
                                                                <option value="Kenya">Kenya</option>
                                                                <option value="Kiribati">Kiribati</option>




                                                                <option value="Kuwait">Kuwait</option>
                                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                <option value="Lao People's Democratic Republic">Lao People's Democratic
                                                                    Republic
                                                                </option>
                                                                <option value="Latvia">Latvia</option>
                                                                <option value="Lebanon">Lebanon</option>
                                                                <option value="Lesotho">Lesotho</option>
                                                                <option value="Liberia">Liberia</option>
                                                                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                <option value="Liechtenstein">Liechtenstein</option>
                                                                <option value="Lithuania">Lithuania</option>
                                                                <option value="Luxembourg">Luxembourg</option>
                                                                <option value="Macao">Macao</option>
                                                                <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former
                                                                    Yugoslav Republic of
                                                                </option>
                                                                <option value="Madagascar">Madagascar</option>
                                                                <option value="Malawi">Malawi</option>
                                                                <option value="Malaysia">Malaysia</option>
                                                                <option value="Maldives">Maldives</option>
                                                                <option value="Mali">Mali</option>
                                                                <option value="Malta">Malta</option>
                                                                <option value="Marshall Islands">Marshall Islands</option>
                                                                <option value="Martinique">Martinique</option>
                                                                <option value="Mauritania">Mauritania</option>
                                                                <option value="Mauritius">Mauritius</option>
                                                                <option value="Mayotte">Mayotte</option>
                                                                <option value="Mexico">Mexico</option>
                                                                <option value="Micronesia, Federated States of">Micronesia, Federated States
                                                                    of
                                                                </option>
                                                                <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                                <option value="Monaco">Monaco</option>
                                                                <option value="Mongolia">Mongolia</option>
                                                                <option value="Montenegro">Montenegro</option>
                                                                <option value="Montserrat">Montserrat</option>
                                                                <option value="Morocco">Morocco</option>
                                                                <option value="Mozambique">Mozambique</option>
                                                                <option value="Myanmar">Myanmar</option>
                                                                <option value="Namibia">Namibia</option>
                                                                <option value="Nauru">Nauru</option>
                                                                <option value="Nepal">Nepal</option>
                                                                <option value="Netherlands">Netherlands</option>
                                                                <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                <option value="New Caledonia">New Caledonia</option>
                                                                <option value="New Zealand">New Zealand</option>
                                                                <option value="Nicaragua">Nicaragua</option>
                                                                <option value="Niger">Niger</option>
                                                                <option value="Nigeria">Nigeria</option>
                                                                <option value="Niue">Niue</option>
                                                                <option value="Norfolk Island">Norfolk Island</option>
                                                                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                <option value="Norway">Norway</option>
                                                                <option value="Oman">Oman</option>
                                                                <option value="Pakistan">Pakistan</option>
                                                                <option value="Palau">Palau</option>
                                                                <option value="Palestinian Territory, Occupied">Palestinian Territory,
                                                                    Occupied
                                                                </option>
                                                                <option value="Panama">Panama</option>
                                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                                <option value="Paraguay">Paraguay</option>
                                                                <option value="Peru">Peru</option>
                                                                <option value="Philippines">Philippines</option>
                                                                <option value="Pitcairn">Pitcairn</option>
                                                                <option value="Poland">Poland</option>
                                                                <option value="Portugal">Portugal</option>
                                                                <option value="Puerto Rico">Puerto Rico</option>
                                                                <option value="Qatar">Qatar</option>
                                                                <option value="Reunion">Reunion</option>
                                                                <option value="Romania">Romania</option>
                                                                <option value="Russian Federation">Russian Federation</option>
                                                                <option value="Rwanda">Rwanda</option>
                                                                <option value="Saint Helena">Saint Helena</option>
                                                                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                <option value="Saint Lucia">Saint Lucia</option>
                                                                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                                <option value="Saint Vincent and The Grenadines">Saint Vincent and The
                                                                    Grenadines
                                                                </option>
                                                                <option value="Samoa">Samoa</option>
                                                                <option value="San Marino">San Marino</option>
                                                                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                                <option value="Senegal">Senegal</option>
                                                                <option value="Serbia">Serbia</option>
                                                                <option value="Seychelles">Seychelles</option>
                                                                <option value="Sierra Leone">Sierra Leone</option>
                                                                <option value="Singapore">Singapore</option>
                                                                <option value="Slovakia">Slovakia</option>
                                                                <option value="Slovenia">Slovenia</option>
                                                                <option value="Solomon Islands">Solomon Islands</option>
                                                                <option value="Somalia">Somalia</option>
                                                                <option value="South Africa">South Africa</option>
                                                                <option value="South Georgia and The South Sandwich Islands">South Georgia and
                                                                    The South Sandwich
                                                                    Islands
                                                                </option>
                                                                <option value="Spain">Spain</option>
                                                                <option value="Sri Lanka">Sri Lanka</option>
                                                                <option value="Sudan">Sudan</option>
                                                                <option value="Suriname">Suriname</option>
                                                                <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                                <option value="Swaziland">Swaziland</option>
                                                                <option value="Sweden">Sweden</option>
                                                                <option value="Switzerland">Switzerland</option>
                                                                <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                                <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                                                <option value="Tajikistan">Tajikistan</option>
                                                                <option value="Tanzania, United Republic of">Tanzania, United Republic of
                                                                </option>
                                                                <option value="Thailand">Thailand</option>
                                                                <option value="Timor-leste">Timor-leste</option>
                                                                <option value="Togo">Togo</option>
                                                                <option value="Tokelau">Tokelau</option>
                                                                <option value="Tonga">Tonga</option>
                                                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                <option value="Tunisia">Tunisia</option>
                                                                <option value="Turkey">Turkey</option>
                                                                <option value="Turkmenistan">Turkmenistan</option>
                                                                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                                <option value="Tuvalu">Tuvalu</option>
                                                                <option value="Uganda">Uganda</option>
                                                                <option value="Ukraine">Ukraine</option>
                                                                <option value="United Arab Emirates">United Arab Emirates</option>
                                                                <option value="United Kingdom">United Kingdom</option>
                                                                <option value="United States">United States</option>
                                                                <option value="United States Minor Outlying Islands">United States Minor
                                                                    Outlying Islands
                                                                </option>
                                                                <option value="Uruguay">Uruguay</option>
                                                                <option value="Uzbekistan">Uzbekistan</option>
                                                                <option value="Vanuatu">Vanuatu</option>
                                                                <option value="Venezuela">Venezuela</option>
                                                                <option value="Viet Nam">Viet Nam</option>
                                                                <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                                <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                                <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                                <option value="Western Sahara">Western Sahara</option>
                                                                <option value="Yemen">Yemen</option>
                                                                <option value="Zambia">Zambia</option>
                                                                <option value="Zimbabwe">Zimbabwe</option>
                                                            </select>
                                                            @error('d-country')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        @forelse($shipping as $key => $val)
                                                            <input type="hidden" name="country_{{$key}}" class="shipping_country" shipping_country="{{$val->country}}" weight="{{$val->weight}}" shipping_cost_country="{{$val->shipping_cost_country}}"  value="{{$val->price}}" >
                                                        @empty

                                                        @endforelse
                                                        <input type="hidden" name="default_weight_price" id="default_weight_price" value="{{$Default_Shipping_Setting ['default_weight_price']}}">
                                                        <input type="hidden" name="default_shipping_cost" id="default_shipping_cost" value="{{$Default_Shipping_Setting ['default_shipping_cost']}}">

                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-7">
                                                            <label>Mailing address same as billing address: </label>
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <div class="form-group">
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="same-as-billingAddress" required="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="dalivery-addres mt-sm-5 mt-md-0">
                                                    <h4>Mailing Address:(Optional)</h4>

                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>Address: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="other-address" id="other-address" class="form-control form-control-sm name">
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>Post Code: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="other-pcode" id="other-pcode" class="form-control form-control-sm pcode">
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>City: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="other-city" id="other-city" class="form-control form-control-sm city">
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>State: </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="other-state" id="other-state" class="form-control form-control-sm state">
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-3">
                                                            <label>Country: </label>
                                                        </div>
                                                        <div class="col-sm-8">

                                                            <select id="other-country" class="form-control form-control-sm" name="other-country">
                                                                <option value="">Select Country</option>
                                                                <option value="Afghanistan">Afghanistan</option>
                                                                <option value="Åland Islands">Åland Islands</option>
                                                                <option value="Albania">Albania</option>
                                                                <option value="Algeria">Algeria</option>
                                                                <option value="American Samoa">American Samoa</option>
                                                                <option value="Andorra">Andorra</option>
                                                                <option value="Angola">Angola</option>
                                                                <option value="Anguilla">Anguilla</option>
                                                                <option value="Antarctica">Antarctica</option>
                                                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                <option value="Argentina">Argentina</option>
                                                                <option value="Armenia">Armenia</option>
                                                                <option value="Aruba">Aruba</option>
                                                                <option value="Australia">Australia</option>
                                                                <option value="Austria">Austria</option>
                                                                <option value="Azerbaijan">Azerbaijan</option>
                                                                <option value="Bahamas">Bahamas</option>
                                                                <option value="Bahrain">Bahrain</option>
                                                                <option value="Bangladesh">Bangladesh</option>
                                                                <option value="Barbados">Barbados</option>
                                                                <option value="Belarus">Belarus</option>
                                                                <option value="Belgium">Belgium</option>
                                                                <option value="Belize">Belize</option>
                                                                <option value="Benin">Benin</option>
                                                                <option value="Bermuda">Bermuda</option>
                                                                <option value="Bhutan">Bhutan</option>
                                                                <option value="Bolivia">Bolivia</option>
                                                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                                <option value="Botswana">Botswana</option>
                                                                <option value="Bouvet Island">Bouvet Island</option>
                                                                <option value="Brazil">Brazil</option>
                                                                <option value="British Indian Ocean Territory">British Indian Ocean Territory
                                                                </option>
                                                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                <option value="Bulgaria">Bulgaria</option>
                                                                <option value="Burkina Faso">Burkina Faso</option>
                                                                <option value="Burundi">Burundi</option>
                                                                <option value="Cambodia">Cambodia</option>
                                                                <option value="Cameroon">Cameroon</option>
                                                                <option value="Canada">Canada</option>
                                                                <option value="Cape Verde">Cape Verde</option>
                                                                <option value="Cayman Islands">Cayman Islands</option>
                                                                <option value="Central African Republic">Central African Republic</option>
                                                                <option value="Chad">Chad</option>
                                                                <option value="Chile">Chile</option>
                                                                <option value="China">China</option>
                                                                <option value="Christmas Island">Christmas Island</option>
                                                                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                                <option value="Colombia">Colombia</option>
                                                                <option value="Comoros">Comoros</option>
                                                                <option value="Congo">Congo</option>
                                                                <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic
                                                                    of The
                                                                </option>
                                                                <option value="Cook Islands">Cook Islands</option>
                                                                <option value="Costa Rica">Costa Rica</option>
                                                                <option value="Cote D'ivoire">Cote D'ivoire</option>
                                                                <option value="Croatia">Croatia</option>
                                                                <option value="Cuba">Cuba</option>
                                                                <option value="Cyprus">Cyprus</option>
                                                                <option value="Czech Republic">Czech Republic</option>
                                                                <option value="Denmark">Denmark</option>
                                                                <option value="Djibouti">Djibouti</option>
                                                                <option value="Dominica">Dominica</option>
                                                                <option value="Dominican Republic">Dominican Republic</option>
                                                                <option value="Ecuador">Ecuador</option>
                                                                <option value="Egypt">Egypt</option>
                                                                <option value="El Salvador">El Salvador</option>
                                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                <option value="Eritrea">Eritrea</option>
                                                                <option value="Estonia">Estonia</option>
                                                                <option value="Ethiopia">Ethiopia</option>
                                                                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                                <option value="Faroe Islands">Faroe Islands</option>
                                                                <option value="Fiji">Fiji</option>
                                                                <option value="Finland">Finland</option>
                                                                <option value="France">France</option>
                                                                <option value="French Guiana">French Guiana</option>
                                                                <option value="French Polynesia">French Polynesia</option>
                                                                <option value="French Southern Territories">French Southern Territories</option>
                                                                <option value="Gabon">Gabon</option>
                                                                <option value="Gambia">Gambia</option>
                                                                <option value="Georgia">Georgia</option>
                                                                <option value="Germany">Germany</option>
                                                                <option value="Ghana">Ghana</option>
                                                                <option value="Gibraltar">Gibraltar</option>
                                                                <option value="Greece">Greece</option>
                                                                <option value="Greenland">Greenland</option>
                                                                <option value="Grenada">Grenada</option>
                                                                <option value="Guadeloupe">Guadeloupe</option>
                                                                <option value="Guam">Guam</option>
                                                                <option value="Guatemala">Guatemala</option>
                                                                <option value="Guernsey">Guernsey</option>
                                                                <option value="Guinea">Guinea</option>
                                                                <option value="Guinea-bissau">Guinea-bissau</option>
                                                                <option value="Guyana">Guyana</option>
                                                                <option value="Haiti">Haiti</option>
                                                                <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald
                                                                    Islands
                                                                </option>
                                                                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                                <option value="Honduras">Honduras</option>
                                                                <option value="Hong Kong">Hong Kong</option>
                                                                <option value="Hungary">Hungary</option>
                                                                <option value="Iceland">Iceland</option>
                                                                <option value="India">India</option>
                                                                <option value="Indonesia">Indonesia</option>
                                                                <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                                <option value="Iraq">Iraq</option>
                                                                <option value="Ireland">Ireland</option>
                                                                <option value="Isle of Man">Isle of Man</option>
                                                                <option value="Israel">Israel</option>
                                                                <option value="Italy">Italy</option>
                                                                <option value="Jamaica">Jamaica</option>
                                                                <option value="Japan">Japan</option>
                                                                <option value="Jersey">Jersey</option>
                                                                <option value="Jordan">Jordan</option>
                                                                <option value="Kazakhstan">Kazakhstan</option>
                                                                <option value="Kenya">Kenya</option>
                                                                <option value="Kiribati">Kiribati</option>




                                                                <option value="Kuwait">Kuwait</option>
                                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic
                                                                </option>
                                                                <option value="Latvia">Latvia</option>
                                                                <option value="Lebanon">Lebanon</option>
                                                                <option value="Lesotho">Lesotho</option>
                                                                <option value="Liberia">Liberia</option>
                                                                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                <option value="Liechtenstein">Liechtenstein</option>
                                                                <option value="Lithuania">Lithuania</option>
                                                                <option value="Luxembourg">Luxembourg</option>
                                                                <option value="Macao">Macao</option>
                                                                <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former
                                                                    Yugoslav Republic of
                                                                </option>
                                                                <option value="Madagascar">Madagascar</option>
                                                                <option value="Malawi">Malawi</option>
                                                                <option value="Malaysia">Malaysia</option>
                                                                <option value="Maldives">Maldives</option>
                                                                <option value="Mali">Mali</option>
                                                                <option value="Malta">Malta</option>
                                                                <option value="Marshall Islands">Marshall Islands</option>
                                                                <option value="Martinique">Martinique</option>
                                                                <option value="Mauritania">Mauritania</option>
                                                                <option value="Mauritius">Mauritius</option>
                                                                <option value="Mayotte">Mayotte</option>
                                                                <option value="Mexico">Mexico</option>
                                                                <option value="Micronesia, Federated States of">Micronesia, Federated States of
                                                                </option>
                                                                <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                                <option value="Monaco">Monaco</option>
                                                                <option value="Mongolia">Mongolia</option>
                                                                <option value="Montenegro">Montenegro</option>
                                                                <option value="Montserrat">Montserrat</option>
                                                                <option value="Morocco">Morocco</option>
                                                                <option value="Mozambique">Mozambique</option>
                                                                <option value="Myanmar">Myanmar</option>
                                                                <option value="Namibia">Namibia</option>
                                                                <option value="Nauru">Nauru</option>
                                                                <option value="Nepal">Nepal</option>
                                                                <option value="Netherlands">Netherlands</option>
                                                                <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                <option value="New Caledonia">New Caledonia</option>
                                                                <option value="New Zealand">New Zealand</option>
                                                                <option value="Nicaragua">Nicaragua</option>
                                                                <option value="Niger">Niger</option>
                                                                <option value="Nigeria">Nigeria</option>
                                                                <option value="Niue">Niue</option>
                                                                <option value="Norfolk Island">Norfolk Island</option>
                                                                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                <option value="Norway">Norway</option>
                                                                <option value="Oman">Oman</option>
                                                                <option value="Pakistan">Pakistan</option>
                                                                <option value="Palau">Palau</option>
                                                                <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied
                                                                </option>
                                                                <option value="Panama">Panama</option>
                                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                                <option value="Paraguay">Paraguay</option>
                                                                <option value="Peru">Peru</option>
                                                                <option value="Philippines">Philippines</option>
                                                                <option value="Pitcairn">Pitcairn</option>
                                                                <option value="Poland">Poland</option>
                                                                <option value="Portugal">Portugal</option>
                                                                <option value="Puerto Rico">Puerto Rico</option>
                                                                <option value="Qatar">Qatar</option>
                                                                <option value="Reunion">Reunion</option>
                                                                <option value="Romania">Romania</option>
                                                                <option value="Russian Federation">Russian Federation</option>
                                                                <option value="Rwanda">Rwanda</option>
                                                                <option value="Saint Helena">Saint Helena</option>
                                                                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                <option value="Saint Lucia">Saint Lucia</option>
                                                                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                                <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines
                                                                </option>
                                                                <option value="Samoa">Samoa</option>
                                                                <option value="San Marino">San Marino</option>
                                                                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                                <option value="Senegal">Senegal</option>
                                                                <option value="Serbia">Serbia</option>
                                                                <option value="Seychelles">Seychelles</option>
                                                                <option value="Sierra Leone">Sierra Leone</option>
                                                                <option value="Singapore">Singapore</option>
                                                                <option value="Slovakia">Slovakia</option>
                                                                <option value="Slovenia">Slovenia</option>
                                                                <option value="Solomon Islands">Solomon Islands</option>
                                                                <option value="Somalia">Somalia</option>
                                                                <option value="South Africa">South Africa</option>
                                                                <option value="South Georgia and The South Sandwich Islands">South Georgia and The
                                                                    South Sandwich
                                                                    Islands
                                                                </option>
                                                                <option value="Spain">Spain</option>
                                                                <option value="Sri Lanka">Sri Lanka</option>
                                                                <option value="Sudan">Sudan</option>
                                                                <option value="Suriname">Suriname</option>
                                                                <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                                <option value="Swaziland">Swaziland</option>
                                                                <option value="Sweden">Sweden</option>
                                                                <option value="Switzerland">Switzerland</option>
                                                                <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                                <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                                                <option value="Tajikistan">Tajikistan</option>
                                                                <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                                <option value="Thailand">Thailand</option>
                                                                <option value="Timor-leste">Timor-leste</option>
                                                                <option value="Togo">Togo</option>
                                                                <option value="Tokelau">Tokelau</option>
                                                                <option value="Tonga">Tonga</option>
                                                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                <option value="Tunisia">Tunisia</option>
                                                                <option value="Turkey">Turkey</option>
                                                                <option value="Turkmenistan">Turkmenistan</option>
                                                                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                                <option value="Tuvalu">Tuvalu</option>
                                                                <option value="Uganda">Uganda</option>
                                                                <option value="Ukraine">Ukraine</option>
                                                                <option value="United Arab Emirates">United Arab Emirates</option>
                                                                <option value="United Kingdom">United Kingdom</option>
                                                                <option value="United States">United States</option>
                                                                <option value="United States Minor Outlying Islands">United States Minor Outlying
                                                                    Islands
                                                                </option>
                                                                <option value="Uruguay">Uruguay</option>
                                                                <option value="Uzbekistan">Uzbekistan</option>
                                                                <option value="Vanuatu">Vanuatu</option>
                                                                <option value="Venezuela">Venezuela</option>
                                                                <option value="Viet Nam">Viet Nam</option>
                                                                <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                                <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                                <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                                <option value="Western Sahara">Western Sahara</option>
                                                                <option value="Yemen">Yemen</option>
                                                                <option value="Zambia">Zambia</option>
                                                                <option value="Zimbabwe">Zimbabwe</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Product Details -->
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">
                                            {{__("routes.Total Amount")}}
                                        </h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                    class="fas fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <!-- Products group by with one category -->
                                                @forelse($product_groupBy_category as $key => $val)

                                                    @foreach($val as $k => $v)
                                                        @php
                                                            $product            = \App\product::find($k);
                                                            $product_price []   = $val[$k];
                                                            //$order_log_product  = \App\order_log::where('user_id',Auth::user()->id)
                                                                                      //->where(['product_id' => $product->id])->first();

                                                            $order_log_product  = \App\order_log::where('user_id',$userID)
                                                                                      ->where(['product_id' => $product->id])->first();
                                                        @endphp
                                                        <div class="checkout-per-product-detail">
                                                            {{--                                                            <p class="product-check-out-details">--}}
                                                            {{--                                                            </p>--}}
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <strong>{{ucwords($product->title)}} </strong>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <span class="ml-3">{{"$".number_format($val[$k],2)}} - {{"x".$order_log_product->qty}}</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    @endforeach
                                                    @php
                                                        $category = \App\Category::find($key);
                                                        $category_SalesTax = \App\Category_Meta::where(['category_id' => $key])
                                                                 ->where(['meta_key' => 'cate_sale_tax'])
                                                                 ->first();
                                                    @endphp
                                                    <p class="mt-2"><strong><em>Category Note</em></strong>: "{{ucwords($category->Name)}}" VAT Sales Tax Applied : {{$category_SalesTax->meta_value}}%</p>
                                                    <div class="Total-VAT">
                                                        <div class="row">
                                                            <div class="col-lg-4"><p> Total with VAT: </p></div>
                                                            <div class="col-lg-4"> <span class="ml-3"> <strong><em>{{"=/ $".number_format($product_groupBy_category_with_salesTax[$key][0],2)}}</em></strong> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                @empty

                                                @endforelse


                                            <!-- Products that have morethan 2 categories apply default sales tax VAT -->
                                                @if(isset($product_groupBy_moreThan_two_categories) && count($product_groupBy_moreThan_two_categories) >= 1)
                                                    <span class="check-out-note-default">Note: Default VAT applied when product contain more than two catgories</span>

                                                    @forelse($product_groupBy_moreThan_two_categories as $key => $val)
                                                        @php
                                                            $product = \App\product::find($key);
                                                            //$order_log_product  = \App\order_log::where('user_id',Auth::user()->id)
                                                                                      //->where(['product_id' => $product->id])->first();

                                                            $order_log_product  = \App\order_log::where('user_id',$userID)
                                                                                      ->where(['product_id' => $product->id])->first();
                                                        @endphp

                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <strong>{{ucwords($product->title)}} </strong>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <span class="ml-3">{{"$".number_format($val[$k],2)}} - {{"x".$order_log_product->qty}}</span>
                                                            </div>
                                                        </div>

                                                        {{--                                                        <div class="checkout-per-product-detail">--}}
                                                        {{--                                                            <p class="product-check-out-details">{{ucwords($product->title)}} <span class="ml-3">{{"$".number_format($val,2)}} - {{"x".$order_log_product->qty}}</span></p>--}}
                                                        {{--                                                        </div>--}}
                                                    @empty

                                                    @endforelse

                                                    @php
                                                        $dst        = \App\SiteSetting::where(['key' => 'default_sales_tax'])->first();
                                                    @endphp
                                                    <p><strong><em>Default</em></strong> Sales Tax VAT Applied: {{$dst->value}}%</p>
                                                    <div class="Total-VAT">
                                                        <div class="row">
                                                            <div class="col-lg-4"><p> Total with VAT: </p></div>
                                                            <div class="col-lg-4"> <span class="ml-3"> <strong><em>{{"=/ $".number_format($defaultSaleTax,2)}}</em></strong> </span>
                                                            </div>
                                                        </div>
                                                        {{--                                                        <p>Total with VAT: <span class="ml-3"> <strong><em>{{"=/ $".number_format($defaultSaleTax,2)}}</em></strong> </span> </p>--}}
                                                    </div>

                                                @endif

                                                @if(isset($total_weight))
                                                    <span class="check-out-note-default">
                                                           Note: Weight price applied according to country wise:
                                                    </span>

                                                    <div class="weight-section">
                                                        <div class="Total-VAT">
                                                            <div class="row">
                                                                <div class="col-lg-4"> <p class="mb-0">Selected Country: </p></div>
                                                                <div class="col-lg-4"><span class="ml-3 product-check-out-price selected_country_by_shipping_address"></span></div>
                                                            </div>
                                                            {{--                                                            <p class="mb-0">Selected Country: <span class="ml-3 product-check-out-price selected_country_by_shipping_address"></span></p>--}}
                                                        </div>
                                                        <div class="Total-VAT">
                                                            <div class="row">
                                                                <div class="col-lg-4"><p class="mb-0">Cost Per Country:</p></div>
                                                                <div class="col-lg-4"><span class="ml-3 product-check-out-price cost_per_country"></span></div>
                                                            </div>
                                                            {{--                                                            <p class="mb-0">Cost Per Country: <span class="ml-3 product-check-out-price cost_per_country"></span></p>--}}
                                                        </div>
                                                        <div class="Total-VAT">
                                                            <div class="row">
                                                                <div class="col-lg-4"><p class="mb-0">Cost Per Weight(kg):</p></div>
                                                                <div class="col-lg-4"><span class="ml-3 product-check-out-price cost_per_weight"></span></div>
                                                            </div>
                                                            {{--                                                            <p class="mb-0">Cost Per Weight(kg): <span class="ml-3 product-check-out-price cost_per_weight"></span> </p>--}}
                                                        </div>
                                                        <div class="Total-VAT">
                                                            <div class="row">
                                                                <div class="col-lg-4"><p class="mb-0">Total Products Weights:</p></div>
                                                                <div class="col-lg-4"> <span class="ml-3"><strong><em>{{$total_weight." (Kg)"}}</em></strong></span></div>
                                                            </div>
                                                            {{--                                                            <p class="mb-0">Total Products Weights: <span class="ml-3"><strong><em>{{$total_weight." (Kg)"}}</em></strong></span>--}}
                                                            {{--                                                            </p>  <p class="product-check-out-price"></p>--}}
                                                            <input type="hidden" id="total_product_weight" value="{{$total_weight}}">
                                                        </div>

                                                    </div>
                                                    <hr>
                                                @endif

                                                <div class="Total-VAT">
                                                    <div class="row">
                                                        <div class="col-lg-4"><p class="mb-0">User:</p></div>
                                                        @php
                                                            //$user = \App\User::find($userID);
                                                        @endphp
                                                        <div class="col-lg-4"> <span class="product-check-out-price user">{{ucwords($UserDetail->name)}}</span></div>
                                                        <input type="hidden" name="userID" value="{{$UserDetail->id}}">
                                                    </div>
                                                </div>
                                                <div class="Total-VAT">
                                                    <div class="row">
                                                        <div class="col-lg-4"><p class="mb-0">Weight Cost:</p></div>
                                                        <div class="col-lg-4"> <span class="product-check-out-price weight_cost">$0.00</span></div>
                                                    </div>
                                                    {{--                                                    <p class="mb-0">Weight Cost: <span class="product-check-out-price weight_cost">$0.00</span></p>--}}
                                                </div>
                                                <div class="Total-VAT">
                                                    <div class="row">
                                                        <div class="col-lg-4"><p class="mb-0">Shipping Cost:</p></div>
                                                        <div class="col-lg-4"> <span class="product-check-out-price shipping_cost">$0.00</span></div>
                                                    </div>
                                                    {{--                                                    <p class="mb-0">Shipping Cost:<span class="product-check-out-price shipping_cost">$0.00</span></p>--}}
                                                </div>
                                                <div class="Total-VAT">
                                                    <div class="row">
                                                        <div class="col-lg-4"><p class="mb-0">Cart Subtotal:</p></div>
                                                        <div class="col-lg-4"> <span class="product-check-out-price carttotal" data-total="{{number_format($total_sum,2)}}">${{number_format($total_sum,2)}}</span></div>
                                                    </div>
                                                    {{--                                                    <p class="mb-0">Cart Subtotal:<span class="product-check-out-price carttotal" data-total="{{number_format($total_sum,2)}}">${{number_format($total_sum,2)}}</span></p>--}}
                                                </div>
                                                <div class="Total-VAT">
                                                    <div class="row">
                                                        <div class="col-lg-4"><p class="mb-0">Coupon Discount:</p></div>
                                                        <div class="col-lg-4"> <span class="product-check-out-price coupon_discount">${{number_format(0,2)}}</span></div>
                                                    </div>
                                                    {{--                                                    <p class="mb-0">Coupon Discount: <span class="product-check-out-price coupon_discount">${{number_format(0,2)}}</span></p>--}}
                                                </div>
                                                <div class="Total-VAT">
                                                    <div class="row">
                                                        <div class="col-lg-4"><p class="mb-0">Total:</p></div>
                                                        <div class="col-lg-4"> <span class="product-check-out-price total"><strong>${{number_format($total_sum,2)}}</strong></span></div>
                                                    </div>
                                                    {{--                                                    <p class="mb-0"><strong>Total:</strong>  <span class="product-check-out-price total"><strong>${{number_format($total_sum,2)}}</strong></span></p>--}}
                                                    <input type="hidden" id="subtotal-number-formatted" name="subtotal-number-formatted" value="{{number_format($total_sum,2)}}" >
                                                    <input type="hidden" id="subtotal" name="subtotal" value="{{$total_sum}}" >
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    @csrf
                                    <input type="submit" value="Create Order" class="btn btn-primary">
                                </div>
                            </form>
                        @endif


                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Order</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete this order</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection

@section('page-script')

    <script type="text/javascript">

        $('.select2').select2();
        //=======================================================================================================

        $("#add_products").click(function () {

            $("#product_process_form").submit();

            {{--var route_url = "{{route('order.manual.process')}}";--}}
            {{--var selected_products = "";--}}
            {{--var quantity          = "";--}}
            {{--var frm = $('#product_process_form');--}}

            // var ff = frm.serializeArray();
            //
            // $.ajax({
            //     context: this,
            //     url: route_url,
            //     type: 'post',
            //     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            //     data: {data: frm.serializeArray()},
            //     success: function (response) {
            //         console.log(JSON.parse(response[0].selected_products));
            //     }
            // });


        });

        //=======================================================================================================

        var carttotal = $('.carttotal').attr("data-total");
        var print_type =  localStorage.getItem('type');
        var print_coupon_discount =  localStorage.getItem('coupon_discount');
        // $('.btn-checkout').click(function(){
        //
        // })

        if(print_type == 'fixed'){
            $('.dollar').html(' $ ');
            var total = carttotal - print_coupon_discount;
            $('.total').html('$ '+ total )
            // $('#coupon-btn').css('display','none');
            // $('#coupon').css('display','none');
        }
        if(print_type == 'percent'){
            $('.percent').html(' % ');
            var calculation = ((carttotal/100)*print_coupon_discount);
            var total = carttotal - calculation;
            $('.total').html('$ '+ total );
            // $('#coupon-btn').css('display','none');
            // $('#coupon').css('display','none');
        }
        $('.discount').html(print_coupon_discount);

        if(total){
            $('#subtotal').val(total);
        }


        $("#coupon-apply").on('submit' , function (e) {
            e.preventDefault();

            var coupon = $('#coupon').val();
            var carttotal = $('.carttotal').attr("data-total");
            var route   = "{{route('couponapply')}}";
            $.ajax({
                url : route,
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {coupon : coupon},
                success:function(response){
                    var type = response.type ;
                    var coupon_discount = response.coupon_discount ;
                    localStorage.setItem('type', type);
                    localStorage.setItem('coupon_discount', coupon_discount);

                    var noti = response.noti;
                    //alert(noti);
                    var print_type =  localStorage.getItem('type');
                    var print_coupon_discount =  localStorage.getItem('coupon_discount');
                    //$('#coupon-btn').css('display','none');
                    //$('#coupon').css('display','none');
                    if(print_type == 'fixed'){
                        $('.dollar').html(' $ ');
                        var total = carttotal - print_coupon_discount;
                        $('.total').html('$ '+ total )
                    }
                    if(print_type == 'percent'){
                        $('.percent').html(' % ');
                        var calculation = ((carttotal/100)*print_coupon_discount);
                        var total = carttotal - calculation;
                        $('.total').html('$ '+ total )
                    }
                    $('.discount').html(print_coupon_discount);

                    if(total){
                        $('#subtotal').val(total);
                    }

                    if(noti == "Coupon Applied Successfully!"){
                        toastr.success('Coupon Applied Successfully', 'Temporary Item');
                    }else if(noti == "Coupon use limit cross!"){
                        toastr.warning('Coupon use limit cross!', 'Temporary Item');
                    }else if(noti == "Coupon Expired!"){
                        toastr.warning('Coupon Expire!', 'Temporary Item');
                    }else if(noti == "Coupon Not Found!"){
                        toastr.warning('Coupon Not Found!', 'Temporary Item');
                    }
                    else if(noti == "Coupon Already Used By User!"){
                        toastr.warning('Coupon Already Used By User!', 'Temporary Item');
                    }

                },
                error:function(response){
                    console.log(response);
                    toastr.success('Coupon Applied Failed', 'Temporary Item');
                }
            });

        });

        //=======================================================================================================



        var total_product_weight = $("#total_product_weight").val();
        //selected_country_by_shipping_address
        var total_price                    =  parseFloat($("#subtotal").val());
        var default_weight_price           =  parseFloat($("#default_weight_price").val());
        var default_shipping_cost          =  parseFloat($("#default_shipping_cost").val());
        // var shipping_country            = "";
        var shipping_cost_country          = "";
        var shipping_country_weight_price  = "";
        var shipping_countries             = [];


        $(".shipping_country").each(function(i){
            shipping_countries.push($(this).attr("shipping_country"));
        });

        $("select#country").change(function() {
            var selectedCountry = $(this).children("option:selected").val();

            $(".shipping_country").each(function(){
                //shipping_country = $(this).attr("shipping_country");
                //console.log($(this).val());
                shipping_country_weight_price = $(this).val();

                if(selectedCountry ===  $(this).attr("shipping_country")){

                    shipping_cost_country   = $(this).attr("shipping_cost_country");
                    if(shipping_cost_country === "0" || shipping_cost_country === ""){
                        shipping_cost_country = default_shipping_cost;
                    }

                    if(shipping_country_weight_price === "0" || shipping_country_weight_price === ""){
                        shipping_country_weight_price = default_weight_price;
                    }

                    var applied_weight      = (total_product_weight * shipping_country_weight_price);

                    var sub_total           = ( parseFloat(applied_weight) + parseFloat(total_price) + parseFloat(shipping_cost_country) ).toFixed(1);

                    $("span.cost_per_country").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                    $("span.cost_per_weight").html("<strong><em>"+"$"+$.number(shipping_country_weight_price , 2)+"</em></strong>");
                    $("span.weight_cost").html("<strong><em>"+"$"+$.number(applied_weight , 2)+"</em></strong>");
                    $("span.shipping_cost").html("<strong><em>"+"$"+$.number(shipping_cost_country , 2)+"</em></strong>");
                    $("span.total").html("$"+$.number(sub_total,2));

                    $("#subtotal").val(sub_total);
                    $("#subtotal-number-formatted").val(sub_total);
                }

                //If Country not exist in shipping table
                if(jQuery.inArray(selectedCountry , shipping_countries) < 0){

                    var default_applied_weight      = (total_product_weight * default_weight_price);
                    var sub_total2                  = (parseFloat(default_applied_weight) + (total_price) + parseFloat(default_shipping_cost));

                    $("span.cost_per_country").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");
                    $("span.cost_per_weight").html("<strong><em>"+"$"+$.number(default_weight_price , 2)+"</em></strong>");
                    $("span.weight_cost").html("<strong><em>"+"$"+$.number(default_applied_weight , 2)+"</em></strong>");
                    $("span.shipping_cost").html("<strong><em>"+"$"+$.number(default_shipping_cost , 2)+"</em></strong>");

                    $("span.total").html("$"+$.number(sub_total2,2));

                    $("#subtotal").val(sub_total2);

                }

            });

            $("span.selected_country_by_shipping_address").html("<strong><em>"+selectedCountry+"</em></strong>")

        });


        //console.log(shipping_countries[0]+"DD");

        //=======================================================================================================

        $("#same-as-billingAddress").click(function(){
            if($(this).prop("checked") === true){

                var address = $("#address").val();
                var pcode   = $("#pcode").val();
                var city    = $("#city").val();
                var state   = $("#state").val();
                var country = $("#country option:selected").val();

                if(address !== "" || pcode !== "" || city !== "" || state !== "" || country !== "" ){
                    $("#other-address").val(address);
                    $("#other-pcode").val(pcode);
                    $("#other-city").val(city);
                    $("#other-state").val(state);
                    $("[name=other-country]").val(country);
                    $('[name=other-country]').niceSelect('update');
                }

            }else if($(this).prop("checked") === false){

                $("#other-address").val("");
                $("#other-pcode").val("");
                $("#other-city").val("");
                $("#other-state").val("");
                $('#other-country').val("");
                $('#other-country').niceSelect('update');

            }
        });

        //=======================================================================================================

        $('#type').on('change', function () {
            if (this.value === "percent") {
                $('#discount').attr('maxlength', '2');
                if ($('#discount').val() > 100) {
                    $('#discount').val('100');
                }
            } else {
                $('#discount').removeAttr('maxlength');
            }
        });

        $('.select2bs4').select2()
        $('input[name="startandexpire"]').daterangepicker();

        //=======================================================================================================

        // $("input[name='checkAll']").click(function(){
        //     $("input[name='faq']").not(this).prop('checked', this.checked);
        // });

        // =======================================================================================================

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("coupon_id");
            var route = '{{ route('coupon.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            })
        });

        //=======================================================================================================

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 7,
            "autoWidth": false,
        });

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        //=======================================================================================================

        function generate() {
            jQuery('[name="coupon"]').val(randomPassword(20))
        }

        //=======================================================================================================

    </script>

@endsection
