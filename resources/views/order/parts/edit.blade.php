@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> Edit Order Part</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__('routes.Order Parts')}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('order.parts.edit',encrypt($orderPart->id) )}}">{{__('routes.Edit')}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <form action="{{route('order.parts.update', $orderPart->id)}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-md-3">
                            <label for="exampleInputEmail1">Order Part Title:</label>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Order Part Title:</label>
                                <input type="text"
                                       name="orderPart_title"
                                       class="form-control form-control-sm @error('orderPart_title') is-invalid @enderror"
                                       id="orderPart_title"
                                       placeholder="Order Part Title"
                                       value="{{isset($orderPart->title) ? $orderPart->title : null}}"
                                >
                                <small>
                                    <cite title="Source Title">
                                        The name is how it appears on checkout page.
                                    </cite>
                                </small>
                                @error('orderPart_title')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Order Parts:</label>
                                <input type="text"
                                       name="orderParts"
                                       class="form-control form-control-sm @error('orderParts') is-invalid @enderror"
                                       id="orderParts"
                                       placeholder="Order Parts"
                                       value="{{isset($orderPart->parts) ? $orderPart->parts : null}}"
                                >
                                <small>
                                    <cite title="Source Title">
                                        Set Order Part into numbers like.. 2 , 3
                                    </cite>
                                </small>
                                @error('orderParts')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="per_orderPart_days">Order Part Per Days:</label>
                                <input type="text"
                                       name="per_orderPart_days"
                                       class="form-control form-control-sm @error('per_orderPart_days') is-invalid @enderror"
                                       id="per_orderPart_days"
                                       placeholder="Per OrderPart Days"
                                       value="{{isset($orderPart->days) ? $orderPart->days : null}}"
                                >
                                <small>
                                    <cite title="Source Title">
                                        Set how much days in each order part
                                    </cite>
                                </small>
                                @error('per_orderPart_days')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>



                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-sm" value="Update">
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>

                </form>
            </div>
        </section>


    </div>


@endsection

@section('page-script')


@endsection
