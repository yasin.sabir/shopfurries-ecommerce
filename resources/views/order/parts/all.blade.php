@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> Order Parts </h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__('routes.Order Parts')}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('order.parts.add')}}">{{__('routes.List')}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-4">
                        <form role="form" action="{{route('order.parts.create')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Order Part Title:</label>
                                <input type="text" name="orderPart_title" class="form-control form-control-sm @error('orderPart_title') is-invalid @enderror" id="orderPart_title" placeholder="Order Part Title">
                                <small>
                                    <cite title="Source Title">
                                        The name is how it appears on checkout page.
                                    </cite>
                                </small>
                                @error('orderPart_title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Order Parts:</label>
                                <input type="text" name="orderParts" class="form-control form-control-sm @error('orderParts') is-invalid @enderror" id="orderParts" placeholder="Order Parts">
                                <small>
                                    <cite title="Source Title">
                                        Set Order Part into numbers like.. 2 , 3
                                    </cite>
                                </small>
                                @error('orderParts')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="per_orderPart_days">Order Part Per Days:</label>
                                <input type="text" name="per_orderPart_days" class="form-control form-control-sm @error('per_orderPart_days') is-invalid @enderror" id="per_orderPart_days" placeholder="Per OrderPart Days">
                                <small>
                                    <cite title="Source Title">
                                        Set how much days in each order part
                                    </cite>
                                </small>
                                @error('per_orderPart_days')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-sm" value="Add New Order Part">
                            </div>


                            <!-- /.card-body -->
                        </form>
                    </div>
                    <div class="col-md-8">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Order Parts List
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <button class="btn btn-danger btn-sm mb-3" id="delete-all-btn">Trash All</button>

                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" name="checkAll" id="customCheckbox001">
                                                <label for="customCheckbox001" class="custom-control-label"></label>
                                            </div>
                                        </th>
                                        <th>No:</th>
                                        <th>Title:</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($orderParts as $key => $part)
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox" name="faq" id="customCheckbox{{$key}}" value="{{$part->id}}">
                                                    <label for="customCheckbox{{$key}}" class="custom-control-label"></label>
                                                </div>
                                            </td>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $part->title }}</td>
                                            <td class="d-flex">
                                                <a href="{{route('order.parts.edit',encrypt($part->id))}}"
                                                   class="btn btn-info btn-sm mr-3"><i class="fa fa-edit"></i></a>

                                                <a href="#_" class="custom-delete-btn btn btn-danger btn-sm"
                                                   inst_id="{{$part->id}}"
                                                   inst_name="{{ $part->title }}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                </div>




            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->


    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Order Part</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete this Part</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>



    <!-- Delete more than once -->
    <div class="modal fade" id="delete-all-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-all-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="delete_ids" id="delete_ids" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Order Parts</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete all the selected Parts!</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_all_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection

@section('page-script')

    <script>


        //=======================================================================================================

        $("input[name='checkAll']").click(function(){
            $("input[name='faq']").not(this).prop('checked', this.checked);
        });

        // =======================================================================================================

        $("#delete-all-btn").on('click' , function () {
            var ids = [];

            var route = '{{ route('order.parts.delete-all') }}';

            $("#delete-all-modal").modal('show');
            $("#delete-all-modal-form").attr("action", route);

            $.each($("input[name='faq']:checked"), function(){
                ids.push($(this).val());
            });
            console.log(ids);
            $("#delete_ids").attr("value",JSON.stringify(ids));

            $('.delete_all_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-all-modal-form").submit();
            });

        });

        //=======================================================================================================

        $(document).on('click', '.custom-delete-btn', function () {

            var id      = $(this).attr("inst_id");
            var route   = '{{ route('order.parts.delete', ['id' => 'id']) }}';
            route       = route.replace('id',id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action",route);

            $('.delete_modal_btn').on('click' , function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            })
        });


        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "pageLength": 5,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>

@endsection
