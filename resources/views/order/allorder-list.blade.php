@extends('layouts.backend.app')

@section('page-css')
    @php
        $num=1;
    @endphp

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6 d-inline-flex">
                        <h1>{{__("routes.Orders List")}}</h1>
                        <a href="{{route('order.export-excel')}}" class="ml-3 btn btn-success btn-sm">Export All Orders</a>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Orders")}}</li>
                            <li class="breadcrumb-item active"><a
                                    href="{{route('order.alllist')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{getOrderByStatus('Processing')}}</h3>
                                <p>{{__("routes.Processing")}}</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-cyan">
                            <div class="inner">
                                <h3>{{getOrderByStatus('Hold')}}</h3>
                                <p>{{__("routes.Hold")}}</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{getOrderByStatus('Cancel')}}</h3>
                                <p>{{__("routes.Canceled")}}</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{getOrderByStatus('Confirm')}}</h3>
                                <p>{{__("routes.Confirm")}}</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">

                                @if(Auth::user()->hasRole(1))
                                    <button class="btn btn-danger btn-sm mb-3" id="delete-all-btn">Trash All</button>
                                @endif

                                <table id="example1" class="table table-striped table-bordered nowrap ">
                                    <thead>
                                    <tr>
                                        @if(Auth::user()->hasRole(1))
                                            <th>
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox" name="checkAll"
                                                           id="customCheckbox001">
                                                    <label for="customCheckbox001" class="custom-control-label"></label>
                                                </div>
                                            </th>
                                        @endif
                                        <th>No</th>
                                        <th>{{__("routes.Order ID")}}</th>
                                        <th>{{__("routes.User Email")}}</th>
                                        <th>{{__("routes.Order Status")}}</th>
                                        <th>{{__("routes.Total Amount")}}</th>
                                        <th>{{__("routes.Created At")}}</th>
                                        <th>{{__("routes.Operation")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($order as $key => $data)
                                        <tr>

                                            @if(Auth::user()->hasRole(1))
                                                <td>
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="faq"
                                                               id="customCheckbox{{$key}}" value="{{$data->id}}">
                                                        <label for="customCheckbox{{$key}}"
                                                               class="custom-control-label"></label>
                                                    </div>
                                                    {{-- <input type="checkbox" name="faq" id="checkbox" value="{{$faq->id}}">--}}
                                                </td>
                                            @endif

                                            <td>{{$num}}</td>
                                            <td>{{$data->order_id}}</td>
                                            <td>{{$data->user_email}}</td>
                                            <td>{{$data->status}}</td>
                                            <td>${{number_format($data->subtotal,2)}}</td>
                                            <td>{{date("D d M Y", strtotime($data->created_at))}}</td>
                                            <td>
                                                <a href="{{route('order.edit',encrypt($data->id))}}"
                                                   class="btn btn-info btn-sm mr-3"><i class="fa fa-edit"></i></a>
                                                <a href="#_" class="custom-delete-btn btn btn-danger btn-sm"
                                                   order_id="{{$data->id}}"
                                                   order_name="{{ $data->code }}"><i class="fa fa-trash"></i></a>

                                            </td>
                                        </tr>
                                        @php
                                            $num++;
                                        @endphp
                                    @empty
                                        <tr>
                                            <td colspan="7" class="text-center">{{__("routes.No Order Found")}}</td>
                                        </tr>

                                    @endforelse
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Order")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("routes.You want to sure to delete this order")}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn"
                                data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>


    <!-- Delete more than once -->
    <div class="modal fade" id="delete-all-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-all-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="delete_ids" id="delete_ids" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Order</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>You want to sure to delete all the selected Orders!</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger delete_all_modal_btn" data-dismiss="modal">Yes</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

@endsection

@section('page-script')

    <script type="text/javascript">

        $('#example1').DataTable({
            "paging": true,
            "pageLength": 7,
            "lengthChange": false,
            "responsive": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });

        // =======================================================================================================

        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("order_id");
            var route = '{{ route('order.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            })
        });


        // =======================================================================================================

        $("input[name='checkAll']").click(function(){
            $("input[name='faq']").not(this).prop('checked', this.checked);
        });

        $("#delete-all-btn").on('click', function () {
            var ids = [];

            var route = '{{ route('order.delete-all') }}';

            $("#delete-all-modal").modal('show');
            $("#delete-all-modal-form").attr("action", route);

            $.each($("input[name='faq']:checked"), function () {
                ids.push($(this).val());
            });
            console.log(ids);
            $("#delete_ids").attr("value", JSON.stringify(ids));

            $('.delete_all_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-all-modal-form").submit();
            });

        });
    </script>

@endsection
