<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <title>Invoice</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        .text-right {
            text-align: right;
        }
    </style>

</head>
@php
    $subtotal=[];
@endphp
<body class="login-page" style="background: white">

<div>
    <div class="row">
        <div class="col-xs-7">
            <h4>From:</h4>
            <strong>ShopFurries.</strong><br>
            123 Company Ave. <br>
            Toronto, Ontario <br>
            P: (416) 123-4567 <br>
            E: copmany@company.com <br>

            <br>
        </div>

        <div class="col-xs-4">
            <img style="width:160px" src="{{asset('images/logo/ShopFurries-Logo.png')}}" alt="logo">
        </div>
    </div>

    <div style="margin-bottom: 0px">&nbsp;</div>

    <div class="row">
        <div class="col-xs-6">
            <h4>To:</h4>
            <address>
                <strong>{{$order_details['name']}}</strong><br>
                <span>{{$order_details['email']}}</span> <br>
                <span>{{$order_details['address']}}</span>
            </address>
        </div>

        <div class="col-xs-5">
            <table style="width: 100%">
                <tbody>
                <tr>
                    <th>Invoice Num:</th>
                    <td class="text-right">{{$order_id}}</td>
                </tr>
                <tr>
                    <th> Invoice Date: </th>
                    <td class="text-right">Oct 1, 2018</td>
                </tr>
                </tbody>
            </table>

            <div style="margin-bottom: 0px">&nbsp;</div>

        </div>
    </div>

    <table class="table">
        <thead style="background: #F5F5F5;">
        <tr>
            <th>Item List</th>
            <th></th>
            <th class="text-right">Price</th>
        </tr>
        </thead>
        <tbody>
        @foreach($product_detail as $data)
            <tr>
                <td><div><strong>{{($data['title'])}}</strong></div>
                    <p></p></td>
                <td></td>
                <td class="text-right">${{($data['price'])}}</td>
            </tr>
            @php
                $subtotal[] =  $data['price'];
            @endphp
        @endforeach
        </tbody>
    </table>

    @php
        $subtotal = array_sum($subtotal);
    @endphp

    <div class="row">
        <div class="col-xs-6"></div>
        <div class="col-xs-5">
            <table style="width: 100%">
                <tbody>
                <tr class="well" style="padding: 5px">
                    <th style="padding: 5px"><div> Total  </div></th>
                    <td style="padding: 5px" class="text-right"><strong> ${{$subtotal}} </strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div style="margin-bottom: 0px">&nbsp;</div>

    <div class="row">
        <div class="col-xs-8 invbody-terms">
            Thank you for your business. <br>
            <br>
            <h4>Payment Terms</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eius quia, aut doloremque, voluptatibus quam ipsa sit sed enim nam dicta. Soluta eaque rem necessitatibus commodi, autem facilis iusto impedit!</p>
        </div>
    </div>
</div>

</body>
