@extends('layouts.backend.app')

@section('page-css')
    @php
        $num=1;
    @endphp

@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__("routes.Orders List")}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Orders")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('order.mylist')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="container-fluid">
              <div class="row">
                  <div class="col-12">

                      <div class="card">

                          <!-- /.card-header -->
                          <div class="card-body">
                              <table id="example1" class="table table-striped table-bordered nowrap ">
                                  <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>{{__("routes.Orders List")}}</th>
                                      <th>{{__("routes.User Email")}}</th>
                                      <th>{{__("routes.Status")}}</th>
                                      <th>{{__("routes.Total Amount")}}</th>
                                      <th>{{__("routes.Created At")}}</th>
                                      <th>{{__("routes.Operation")}}</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  @forelse($order as $data)
                                      <tr>
                                          <td>{{$num}}</td>
                                          <td>{{$data->order_id}}</td>
                                          <td>{{$data->user_email}}</td>
                                          <td>{{$data->status}}</td>
                                          <td>${{$data->subtotal}}</td>
                                          <td>{{date("D d M Y", strtotime($data->created_at))}}</td>
                                          <td>
                                              <a href="{{route('order.edit',$data->id)}}" class="btn btn-info btn-sm mr-3"><i class="fa fa-edit"></i></a>
                                          </td>
                                      </tr>
                                      @php
                                          $num++;
                                      @endphp
                                  @empty
                                      <tr>
                                          <td colspan="7" class="text-center">{{__("routes.No Order Found")}}</td>
                                      </tr>

                                  @endforelse
                                  </tbody>

                              </table>
                          </div>
                          <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                  </div>
                  <!-- /.col -->
              </div>
              <!-- /.row -->
          </div>



        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">

    $('#example1').DataTable({
    "paging": true,
    "pageLength": 7,
    "lengthChange": false,
    "responsive": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    });

    </script>

@endsection
