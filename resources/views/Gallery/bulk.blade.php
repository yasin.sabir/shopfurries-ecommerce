@extends('layouts.backend.app')

@section('page-css')
    <style>
        .badge {
            margin: 3px 3px !important;
        }

    </style>
@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="float-left">{{__("routes.Products")}}</h1>
                        <p><a href="{{route('product.list')}}" class="btn btn-primary btn-sm ml-3 mr-3 ">{{__("routes.Product List")}}</a></p>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Products")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('gallery.bulk-list')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">{{__("routes.Add Multiple Products")}}</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                <form method="post" action="{{route('gallery.create')}}" enctype="multipart/form-data">
                                    @csrf
                                    @php $num=1; @endphp
                                    <div class="row" id="row_">
                                        @forelse($temp_data as $i => $data)

                                            <div class="col-md-4 pl-4 pr-4 text-center">
                                                <h5>{{__("routes.Entry")}} -{{$num}}</h5>
                                                <div class="form-group">
                                                    <div class="custom-img-section shadow custom-img-style"  style="background-image: url({{asset('storage/'.$data->image)}});">
                                                    </div>
                                                    <input type="hidden" value="{{Auth::user()->id}}" name="user_id"/>
                                                    <input type="hidden" value="{{$data->id}}" name="gallery_entry_[{{$i}}]"/>
                                                </div>

                                            </div>

                                            <div class="col-md-8">

                                                <div class="form-group">
                                                    <input type="text" placeholder="Enter Title" class="form-control" name="title_[{{$data->id}}]" value="">
                                                </div>

                                                <div class="form-group">
                                                    <textarea class="form-control" placeholder="Add Description" name="description_[{{$data->id}}]" id="description_[{{$data->id}}]" cols="15" rows="5"></textarea>
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" placeholder="Add Tags" class="tags" name="entry_tag[{{$data->id}}]" data-role="tagsinput" value="">
                                                </div>

                                                <div class="form-group">
                                                    <input type="hidden" name="temp_img_id" id="temp_img_id"  value="">
                                                    <button type="submit" id="temp_prod_delete_btn"
                                                            gallery_entry_id="{{$data->id}}"
                                                            class="custom-delete-btn btn btn-danger btn-sm"><i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                            </div>

                                            <hr>
                                            @php $num++; @endphp
                                        @empty
                                            <div class="col-md-12 text-center">
                                                <h4>{{__("routes.No Products Found")}}</h4>
                                            </div>
                                        @endforelse
                                    </div>


                                    @if(!empty($temp_data))
                                        <div class="text-center">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary mt-3" value="{{__("routes.Submit")}}" name="all_upload" id="all_upload">
                                            </div>
                                        </div>
                                    @endif


                                </form>
                            </div>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "pageLength": 50,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });

        $(function () {
            $('.select2').select2();
        });


        $(document).on('click', '.custom-delete-btn', function (e) {
            e.preventDefault();

            var temp_img = $(this).attr("gallery_entry_id");
            var route    = '{{ route('gallery.delete', ['id' => 'id']) }}';
            route        = route.replace('id', temp_img);

            $.ajax({
                url:route,
                type:'post',
                data:{temp_img : temp_img},
                success:function(response){
                    //console.log(response+" - Mode is on");
                    toastr.success('Temporary Art Work has successfully deleted', 'Temporary Item');
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
            });

        });


    </script>

@endsection
