@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Art Submission")}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Gallery")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('art-work-Gallery.list')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.List")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <button class="btn btn-danger btn-sm mb-3" id="delete-all-btn">{{__("routes.Trash All")}}</button>

                                <table id="example1" class="table table-bordered table-striped nowrap">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" name="checkAll" id="customCheckbox001">
                                                <label for="customCheckbox001" class="custom-control-label"></label>
                                            </div>
                                        </th>
                                        <th>No:</th>
                                        <th>{{__("routes.Artists")}}</th>
                                        <th>{{__("routes.Title")}}:</th>
                                        <th>{{__("routes.Image")}}:</th>
                                        <th>{{__("routes.Status")}}:</th>
                                        <th>{{__("routes.Created At")}}:</th>
                                        <th>{{__("routes.Action")}}:</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @forelse($artWorks as $key => $art_work)
                                        @php
                                            @endphp
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox" name="faq" id="customCheckbox{{$key}}" value="{{$art_work->id}}">
                                                    <label for="customCheckbox{{$key}}" class="custom-control-label"></label>
                                                </div>
                                            </td>
                                            <td>{{$key+1}}</td>
                                            <td>
                                                @php
                                                    $user = \App\User::find($art_work['artist_id']);
                                                @endphp
                                                {{ucfirst($user->name)}}
                                            </td>
                                            <td>{{ucfirst($art_work->title)}}</td>
                                            <td><img src="{{asset('storage/'.$art_work->image)}}" width="100" height="70"></td>
                                            <td>
                                                @if($art_work->status != 0)
                                                    <span class="right badge badge-success">{{_("Approved")}}</span>
                                                @else
                                                    <span class="right badge badge-warning">{{_("Pending")}}</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{\Carbon\Carbon::parse($art_work->created_at)->format('d-m-Y g:i a')}}
                                            </td>
                                            <td>
                                                <div class="custom-control custom-switch">
                                                    <input type="hidden" class="artSubmission_id" value="{{$art_work->id}}">
                                                    <input type="checkbox"
                                                           class="custom-control-input submission-customSwitch"
                                                           value="{{$art_work->id}}" id="customSwitch_{{$art_work->id}}"
                                                           @if(isset($art_work->status) && $art_work->status == 1 ) checked
                                                           @else uncheckeck @endif>
                                                    <label class="custom-control-label"
                                                           for="customSwitch_{{$art_work->id}}"></label>
                                                </div>

                                                <a href="#_" class="custom-delete-btn btn btn-danger btn-sm"
                                                   artSubmission_id="{{$art_work->id}}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>

                                    @empty

                                    @endforelse


                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->


    <!-- Delete single once-->
    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Art Work")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("routes.You want to sure to delete this art work from your site")}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

    <!-- Delete more than once -->
    <div class="modal fade" id="delete-all-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-all-modal-form">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="delete_ids" id="delete_ids" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__("routes.Delete Art Work")}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>{{__("routes.You want to sure to delete all the selected art works")}}</p>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__("routes.Close")}}</button>
                        <button type="submit" class="btn btn-danger delete_all_modal_btn" data-dismiss="modal">{{__("routes.Yes")}}</button>
                    </div>
                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>


@endsection

@section('page-script')
    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 7,
            "autoWidth": false,
        });

        //=======================================================================================================

        $("input[name='checkAll']").click(function(){
            $("input[name='faq']").not(this).prop('checked', this.checked);
        });

        // =======================================================================================================


        $("#delete-all-btn").on('click' , function () {
            var ids = [];

            var route = '{{ route('art-work-Gallery.delete-all') }}';

            $("#delete-all-modal").modal('show');
            $("#delete-all-modal-form").attr("action", route);

            $.each($("input[name='faq']:checked"), function(){
                ids.push($(this).val());
            });
            console.log(ids);
            $("#delete_ids").attr("value",JSON.stringify(ids));

            $('.delete_all_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-all-modal-form").submit();
            });

        });

        //=======================================================================================================
        $(document).on('click', '.custom-delete-btn', function () {

            var id = $(this).attr("artSubmission_id");
            var route = '{{ route('art-work-Gallery.delete', ['id' => 'id']) }}';
            route = route.replace('id', id);

            $("#delete-modal").modal('show');
            $("#delete-modal-form").attr("action", route);

            $('.delete_modal_btn').on('click', function (e) {
                e.preventDefault();
                // alert("ds");
                $("#delete-modal-form").submit();
            });

        });

        //=======================================================================================================

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });


        //=======================================================================================================
        var status = "";

        $('.submission-customSwitch').click(function () {

            var customSwitch1 = $(this);
            var id = customSwitch1.val();
            var route = "{{route('art-work-Gallery.update',['id' => 'id'])}}";
            route = route.replace('id', id);

            if ($(this).prop("checked") === true) {

                console.log("checked");
                status = "approved";

                $.ajax({
                    context: this,
                    url: route,
                    type: 'post',
                    data: {"status": status},
                    success: function (response) {
                        //console.log(response+" - Mode is on");
                        toastr.success('Art Work is <strong>approved</strong> !', 'Art Work Alert!');
                        window.location.reload();
                    }
                });
            } else if ($(this).prop("checked") === false) {

                console.log("Unchecked");
                status = "pending";
                $.ajax({
                    context: this,
                    url: route,
                    type: 'post',
                    data: {"status": status},
                    success: function (response) {
                        //console.log(response+" - Mode is off");
                        toastr.error('Art work is <strong>pending</strong> !', 'Art Work Alert!');
                        window.location.reload();
                    }
                });
            }
        });

        //=======================================================================================================

    </script>

@endsection
