@extends('layouts.backend.app') @section('page-css')

    <style>
        .note-editable.card-block {
            height: 300px;
        }

        .badge {
            margin: 3px 3px !important;
        }

    </style>

@endsection @section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> {{__("routes.Multiple Products")}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Products")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('gallery.add')}}">{{__("routes.Add Multiple")}}</a></li>
                        </ol>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <!-- form start -->
                <form method="post" action="{{route('gallery.bulk-list')}}" class="form-horizontal"
                      enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Product Data Section Start-->
                                    <div class="card card-default">
                                        <div class="card-body pad">
                                            <div class="form-group">
{{--                                                <input type="text" id="tags" data-role="tagsinput" value="jQuery,Script,Net">--}}
                                                <label for="product_nae"
                                                       class="mt-1">{{__("routes.Add Multiple Art Work")}}</label>
                                            </div>

                                            <div class="form-group">
                                                <div class="file-loading">
                                                    @php $i=1; @endphp
                                                    <input id="file-1" type="file" name="file[]" multiple
                                                           class="file {{ $errors->has('file.',$i)? "has-error" :"" }}">
                                                    @if($errors->has('file.',$i))
                                                        <span class="invalid-feedback d-block" role="alert">
                                                           <strong>{{ $errors->first('file.',$i) }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                            </div>

                                            <div class="form-group text-center pt-5">
                                                <input type="submit" class="btn btn-lg btn-primary  btn-block"
                                                       value="{{__("routes.Upload")}}">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>

                            </div>
                        </div>

                    </div>

                    <!-- /.row -->
                </form>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection @section('page-script')

    <script>
        $("#file-1").fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'gif'],
            overwriteInitial: false,
            maxFileSize: 5000,
            maxFilesNum: 10,
            showClose: false,
            showUpload: false,
            showCancel: false,
            showZoom: true,

            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

    </script>

@endsection
