{{--@if(site_config('site_maintenance_status') == 'on')--}}
{{--    @include('front-views.maintenance')--}}
{{--    @php--}}
{{--        exit();--}}
{{--    @endphp--}}
{{--@endif--}}

<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        @yield('title')
    </title>


    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('front-layout.head')

    @yield('custom-front-css')
</head>
<body>

    <!-- header-area START -->
    @include('front-layout.header')
    <!-- header-area END -->

    <!-- sidbar -->
    @include('front-layout.sidebar')
    <!-- sidbar -->
    <!-- main -->
    @yield('Main')

    <!-- footer-area START -->
    @include('front-layout.footer')
    <!-- footer-area END -->

    @include('front-layout.script')

    <script>

        $(document).ready(function() {
           getWithExpiry("guestDetail");
        });


        @if(Session::has('message'))
        var type = "{{ Session::get('alert-type','info')  }}";
        switch (type) {

            case 'info':
                toastr.info("{{Session::get('message')}}");
                break;
            case 'success':
                toastr.success("{{Session::get("message")}}");
                break;
            case 'warning':
                toastr.warning("{{Session::get('message')}}");
                break;
            case 'error':
                toastr.error("{{Session::get("message")}}");
                break;
        }
        @endif

        //=================================================================================

        function getWithExpiry(key) {

            const itemStr = localStorage.getItem(key);
            // if the item doesn't exist, return null;
            if (!itemStr) {
                return null
            }

            const item = JSON.parse(itemStr);
            const now  = new Date();

            // compare the expiry time of the item with the current time
            if (now.getTime() > item.expiry) {
                // If the item is expired, delete the item from storage

                var route2 = '{{ route('guest.destroy-id')}}';

                $.ajax({
                    type: 'POST',
                    url: route2,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        guest_id: item.ID,
                    },
                    success: function (res) {
                        console.log(res.message);
                        localStorage.setItem('count', 0);
                        $('.custom-badge-cart-primary').html(0);
                    }
                });
                localStorage.removeItem(key);
                //return null
            }
            return item.value
        }

        //=================================================================================

        var gusestDetail         = "";
        var localStorageHasValue = false;

        setInterval(function(){

            if(localStorage.hasOwnProperty("guestDetail")){
                gusestDetail = JSON.parse(localStorage.getItem("guestDetail"));
            }

        },1000);



         @if(auth()->guest())

             setInterval(function(){
                 if(localStorage.hasOwnProperty("guestDetail")){
                     var id = gusestDetail.ID;
                     var route = '{{ route('checkout', ['id' => 'id']) }}';
                     route = route.replace('id', id);
                     $("#shopCart-Icon").attr("href", route);
                 }
             },1000);

            $("a#shopCart-Icon").click(function (e) {
                e.preventDefault();
                //alert("Cart is empty. So add item into cart!");

                if(!$(this).attr("href")){
                    $("#emptyCart-modal").modal('show');
                }else{
                    window.location.href = $(this).attr("href");
                }

            });

        @else

            var Auth_user_id = '{{ Auth::user()->id }}';
            var route2        = '{{ route('checkout', ['id' => 'id']) }}';
            route2            =  route2.replace('id',Auth_user_id.trim());
            $("#shopCart-Icon").attr("href",route2);

        @endif


        //==========================================================================================================


    </script>

    @yield('custom-front-script')


</body>
</html>
