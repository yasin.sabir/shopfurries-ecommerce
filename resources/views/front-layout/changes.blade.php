<div class="col-lg-12">
    <ul class="filters text-center">
        <li class="active" data-filter="*"><a href="#!">ALLE</a></li>
        <li data-filter=".2020"><a href="#!">2020</a></li>
        <li data-filter=".2019"><a href="#!">2019</a></li>
        <li data-filter=".2018"><a href="#!">2018</a></li>
        <li data-filter=".2017"><a href="#!">2017</a></li>
        <li data-filter=".2016"><a href="#!">2016</a></li>
        <li data-filter=".2015"><a href="#!">2015</a></li>
        <li data-filter=".2014"><a href="#!">2014</a></li>
        <li data-filter=".later"><a href="#!">FRÜHER</a></li>
    </ul>
</div>

<div class="projects" >
    <!-- content 2020 section start -->
    <div class="col-sm-6 col-md-3 item 2020" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-greaterzuricharea.jpg" alt="greaterzuricharea">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Greater Zurich Area<br>
                            30.03.2020</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare lanciert Gutscheinaktion für KMU</h3>
                <div class="separator"></div>
                <p> Der Crowdlending-Anbieter Cashare unterstützt krisengeplagte KMU mit einer kostenlosen Gutscheinplattform. Sie soll die Folgen der Corona-Krise für jene Unternehmen abfedern, die durch das Netz des nationalen Hilfsprogramms fallen</p>
                <a href="https://www.greaterzuricharea.com/de/news/cashare-lanciert-gutscheinaktion-fuer-kmu" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2020" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-swissfinancestartups.jpg" alt="iTReseller">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Swiss Finance Startups<br>
                            31.01.2020</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Ausbildung fair finanziert mit Cashare</h3>
                <div class="separator"></div>
                <p>Nach dem erfolgreichen Start der Partnerschaft von Cashare und der Bénédict Education Group wird nun ein weiterer Meilenstein erklommen.</p>
                <a href="https://www.swissfinancestartups.com/post/ausbildung-fair-finanziert-mit-cashare" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2020" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-finews.jpg" alt="Finews">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finews <br>
                            17.02.2020</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Harald Schnabel: Vom Fintech zur Berner Börse und zurück</h3>
                <div class="separator"></div>
                <p>Der deutsche Börsenspezialist Harald Schnabel kehrt als Berater zum Schweizer Schwarmfinanzierer Cashare zurück.</p>
                <a href="https://www.finews.ch/news/banken/39973-cashare-harald-schnabel-bx-swiss" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2020" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/itreseller.png" alt="iTReseller">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>iT Reseller<br>
                            17.02.2020</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Harald Schnabel wechselt zurück in den Beirat von Cashare</h3>
                <div class="separator"></div>
                <p>Harald Schnabel ist zurück im Advisory Board von Cashare. Davor war er während seit etwas mehr als zwei Jahren bei BXSwiss als CEO tätig.</p>
                <a href="https://www.itreseller.ch/Artikel/90927/Harald_Schnabel_wechselt_zurueck_in_den_Beirat_von_Cashare.html" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2020" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-cash.jpg" alt="cash">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>cash<br>
                            17.02.2020</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Harald Schnabel zurück bei Cashare</h3>
                <div class="separator"></div>
                <p>Harald Schnabel ist wieder Beirat von Cashare. Davor war er während rund zwei Jahren bei BX Swiss als CEO tätig.</p>
                <a href="https://www.cash.ch/fonds-news/partner-news/harald-schnabel-zurueck-bei-cashare-1481229" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2020" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-investtrends.jpg" alt="investrends.ch ">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>investrends.ch<br>
                            17.02.2020</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Harald Schnabel zurück bei Cashare</h3>
                <div class="separator"></div>
                <p>Harald Schnabel ist wieder Beirat von Cashare. Davor war er während rund zwei Jahren bei BX Swiss als CEO tätig.</p>
                <a href="https://investrends.ch/aktuell/people/harald-schnabel-zuruck-bei-cashare/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2020" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fuw.jpg" alt="Finanz und Wirtschaft">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finanz und Wirtschaft<br>
                            03.02.2020</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Privatkredite sind Oase in der Renditewüste</h3>
                <div class="separator"></div>
                <p>Plattformen bringen Kreditnehmer und Geldgeber zusammen. In der Schweiz sind sie noch klein, in den USA sind die Ergebnisse ernüchternd.</p>
                <a href="https://www.fuw.ch/article/privatkredite-sind-oase-in-der-renditewueste/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <!-- content 2019 section start -->
    <div class="col-sm-6 col-md-3 item 2019"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-srf.jpg" alt="SRF1 Espresso">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>SRF1 Espresso<br>
                            26.11.2019</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdlending: Wenn Private Bank spielen</h3>
                <div class="separator"></div>
                <p>Wer auf Crowdlending-Plattformen Geld investiert, erhält dafür viel Zins. Man kann das Geld aber auch verlieren.</p>
                <a href="https://www.srf.ch/news/schweiz/investitionen-crowdlending-wenn-private-bank-spielen" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2019"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-nzz.jpg" alt="Neue Zürcher Zeitung">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zürcher Zeitung<br>
                            10.05.2019</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Das Kreditgeschäft im Schwarm wächst rasant und entfernt sich von der Grundidee</h3>
                <div class="separator"></div>
                <p>Das Volumen im Crowdlending ist 2018 um 40% gewachsen. Die Kredit-Vermittlungsplattformen haben vermehrt institutionelle Geldgeber und KMU im Visier.</p>
                <a href="https://www.nzz.ch/finanzen/schwarm-kreditgeschaeft-waechst-und-entfernt-sich-von-der-grundidee-ld.1480572" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2019"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-1010.jpg" alt="10×10">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>10×10<br>
                            03.05.2019</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Anwendungen für Krypto-Assets</h3>
                <div class="separator"></div>
                <p>Krypto-Assets sollen Mainstream werden, so die Forderung vieler! Die ersten Schritte werden getan. Wer ein wenig stöbert, findet schon interessante Anwendungsfälle.</p>
                <a href="https://www.10x10.ch/anwendungen-fuer-krypto-assets/" target="_blank" class="btn btn-default btn-sm  btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2019"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-cash.jpg" alt="cash">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>cash<br>
                            11.04.2019</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Gesetzesänderung hilft dem Crowdlending-Geschäft</h3>
                <div class="separator"></div>
                <p>Seit Monatsbeginn gilt die sogenannte «20er-Regel» für Privatkredite in der Schweiz nicht mehr. Das erleichtert vor allem dem Crowdlending das Geschäft - obwohl dieses auch in Zukunft ein Nischenmarkt bleiben wird.</p>
                <a href="https://www.cash.ch/news/top-news/kredite-ueber-das-internet-gesetzesaenderung-hilft-dem-crowdlending-geschaeft-1314917" target="_blank" class="btn btn-default btn-sm  btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2019"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fintechnews.jpg" alt="FinTech News">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>FinTech News<br>
                            05.03.2019</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>P2P Lending Cashare-Team mit neuem CFO</h3>
                <div class="separator"></div>
                <p>Der Zuger Crowdlending Pionier Cashare hat einen neuen CFO.</p>
                <a href="http://fintechnews.ch/p2plending/p2p-lending-cashare-team-mit-neuem-cfo-michael-boge/26208/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2019"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-finews.jpg" alt="Finews">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finews <br>
                            04.03.2019</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Notenstein-Banker verstärkt Fintech</h3>
                <div class="separator"></div>
                <p>Ein weiterer ehemaliger Notenstein-Banker tritt einen neuen Job an. Der erfahrene Private Banker übernimmt bei einem Zuger Fintech als Finanzchef. </p>
                <a href="https://www.finews.ch/news/banken/35533-fintech-cfo-cashare-michael-boge-notenstein" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2019"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-cash.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>cash<br>
                            15.01.2019</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Wie die Digitalisierung beim Kostensparen hilft</h3>
                <div class="separator"></div>
                <p>Der Fintech-Boom geht weiter: Auch 2019 kommt eine Reihe neuer Angebote auf den Markt, die Finanzangelegenheiten einfacher und vor allem günstiger machen sollen. Für Bankkunden ist dies unter dem Strich positiv.</p>
                <a href="https://www.cash.ch/news/top-news/apps-fintechs-start-ups-wie-die-digitalisierung-beim-kostensparen-hilft-1263616" target="_blank" class="btn btn-default btn-sm  btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <!-- content 2018 section start -->
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/ktipp.jpg" alt="Ktipp">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Ktipp <br>
                            13.11.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Tausende Kunden zahlen zu viel</h3>
                <div class="separator"></div>
                <p>Der Höchstzinssatz für Kleinkredite liegt heute bei 10 Prozent. Kunden können aus einem teureren ­Vertrag jederzeit ­aussteigen.</p>
                <a href="https://www.ktipp.ch/artikel/d/tausende-kunden-zahlen-zu-viel/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/moneytoday.jpg" alt="MoneyToday.ch">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>MoneyToday.ch <br>
                            12.10.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare vermittelt Kredite gegen Kryptowährungen als Sicherheit</h3>
                <div class="separator"></div>
                <p>Cashare lässt Kredite in Schweizerfranken durch die Crowd finanzieren und sichert Investoren durch hinterlegte Kryptowährungen ab.</p>
                <a href="https://www.moneytoday.ch/news/cashare-vermittelt-kredite-gegen-kryptowaehrungen-als-sicherheit/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/investory.png" alt="Investory">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Asset Manager Magazin Nr. 9 - 2018 <br>
                            08.10.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdlending Markt wächst rasant</h3>
                <div class="separator"></div>
                <p>Mit einem vermittelten Kreditvolumen von 186,7 Millionen Franken (im Vorjahr waren es 55,1 Millionen Franken) und einem Wachstum von 240 Prozent) konnte sich die Crowdlending-Branche stark steigern.</p>
                <a href="http://online.flipbuilder.com/ifov/hxdc/mobile/index.html#p=14" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fintechnews.jpg" alt="FinTech News">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>FinTech News<br>
                            04.10.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Kooperation von Crowdlending-Pionier Cashare und AXA Versicherung</h3>
                <div class="separator"></div>
                <p>Tech company Aave, which specializes in the development of decentralized and hybrid blockchain applications, today announced the launch of Aave Lending, an efficient SaaS solution for the collateralization of loans.</p>
                <a href="http://fintechnews.ch/p2plending/switzerlands-crowdlending-pioneer-cashare-partners-with-aave-to-enter-digital-asset-backed-lending-industry/22617/ " target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/finanznachrichten.jpg" alt="finanznachrichten">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>finanznachrichten.de <br>
                            04.10.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare, der Pionier im Crowdlending in der Schweiz, schließt Partnerschaft mit Aave, um die Kredit-Branche mit Sicherung durch digitale Vermögenswerte zu erschließen</h3>
                <div class="separator"></div>
                <p>Neue Software zur Kreditvergabe ermöglicht Unternehmen und Privatpersonen, die aufstrebende Digital-Asset-Klasse zu nutzen</p>
                <a href="https://www.finanznachrichten.de/nachrichten-2018-10/44935713-cashare-der-pionier-im-crowdlending-in-der-schweiz-schliesst-partnerschaft-mit-aave-um-die-kredit-branche-mit-sicherung-durch-digitale-vermoegenswert-006.htm" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/wallstreet-online.jpg" alt="wallstreet-online">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>wallstreet-online.de <br>
                            04.10.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare, der Pionier im Crowdlending in der Schweiz, schließt Partnerschaft mit Aave, um die Kredit-Branche mit Sicherung durch digitale Vermögenswerte zu erschließen</h3>
                <div class="separator"></div>
                <p>Neue Software zur Kreditvergabe ermöglicht Unternehmen und Privatpersonen, die aufstrebende Digital-Asset-Klasse zu nutzen</p>
                <a href="https://www.wallstreet-online.de/nachricht/10907010-cashare-pionier-crowdlending-schweiz-schliesst-partnerschaft-aave-kredit-branche-sicherung-digitale-vermoegenswerte-erschliessen" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/zentralplus.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>zentralplus.ch <br>
                            11.08.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Wie eine Menstruations-Schokolade finanziert wurde</h3>
                <div class="separator"></div>
                <p>Crowdfunding-Plattformen werden immer beliebter, wenn es darum geht, Projekte oder Herzenswünsche zu realisieren.</p>
                <a href="https://www.zentralplus.ch/de/news/wirtschaft/5573728/Wie-eine-Menstruations-Schokolade-finanziert-wurde.htm?utm_source=zentralplus&utm_campaign=0b291588e6-Daily_Newsletter&utm_medium=email&utm_term=0_72e9367cee-0b291588e6-96946469" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-tagesanzeiger.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Tages-Anzeiger<br>
                            30.07.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Was Anleger beim Crowdfunding beachten sollten</h3>
                <div class="separator"></div>
                <p>Schwarmfinanzierung kann auch für Investoren interessant sein. Diese sollten sich aber nicht nur von der guten Rendite beeindrucken lassen.</p>
                <a href="https://www.tagesanzeiger.ch/wirtschaft/sozial-und-sicher/was-anleger-beim-crowdfunding-beachten-sollten/story/15606499" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/zentralplus.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>zentralplus.ch <br>
                            17.07.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Das Crowdfunding-Business brummt – und Zug bereitete den Nährboden</h3>
                <div class="separator"></div>
                <p>Die Crowdfunding-Szene wächst und wächst. Alleine im letzten Jahr lag das vermittelte Volumen bei über 350 Millionen Franken. Zuger Firmen haben gleich in mehreren Bereichen Pionierarbeit geleistet</p>
                <a href="https://www.zentralplus.ch/de/news/wirtschaft/5572905/Das-Crowdfunding-Business-brummt-–-und-Zug-bereitete-den-Nährboden.htm" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/itreseller.png" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>IT Reseller<br>
                            13.06.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare geht Kooperation mit Axa Versicherungen ein</h3>
                <div class="separator"></div>
                <p>Die Crowdlending-Plattform Cashare kann eine Zusammenarbeit mit Axa im Bereich Kreditschutzversicherungen vermelden.</p>
                <a href="https://www.itreseller.ch/Artikel/87005/Cashare_geht_Kooperation_mit_Axa_Versicherungen_ein.html" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fintechnews.jpg" alt="FinTech News">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>FinTech News<br>
                            12.06.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Kooperation von Crowdlending-Pionier Cashare und AXA Versicherung</h3>
                <div class="separator"></div>
                <p>Die Schweizer Peer to Peer Lending Platform Cashare gibt bekannt, dass seit Mai 2018 die AXA die neue Versicherungspartnerin von Cashare für Kreditschutzversicherungen ist.</p>
                <a href="http://fintechnews.ch/p2plending/kooperation-von-crowdlending-pionier-cashare-und-axa-versicherungen/19053/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-financeblog.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Hochschule Luzern<br>
                            28.05.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Rekord im Schweizer Crowdfunding: 375 Millionen Franken vermittelt</h3>
                <div class="separator"></div>
                <p>Der Schweizer Crowdfunding-Markt verzeichnet Rekordwerte: 374.5 Mio. Franken wurden 2017 über Crowdfunding-Plattformen vermittelt, fast dreimal so viel wie im Vorjahr. Dies zeigt das neuste Crowdfunding Monitoring der Hochschule Luzern.</p>
                <a href="https://blog.hslu.ch/retailbanking/2018/05/28/rekord-im-schweizer-crowdfunding-375-millionen-franken-vermittelt/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fuw.jpg" alt="Finanz und Wirtschaft">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finanz und Wirtschaft <br>
                            07.03.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Geld sparen bei Bankgeschäften</h3>
                <div class="separator"></div>
                <p>Wertschriften kaufen und verkaufen, Kredite aufnehmen, Geld überweisen. Was dank Digitalisierung alles billiger wird.</p>
                <a href="https://www.fuw.ch/article/geld-sparen-bei-bankgeschaeften/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fintechnews.jpg" alt="FinTech News">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>FinTech News <br>
                            23.02.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>FinTech News: 10 Jahre Cashare – Start der
                    Fintech-Zeitrechnung
                </h3>
                <div class="separator"></div>
                <p>Über die Aufhebung der 20er Regel bei KMU-Darlehen bis zu CHF
                    1 Mio. hat Cashare erst im Juli berichtet und das System auf
                    den 1. August angepasst. Es wurde deutlich angemerkt, dass die
                    Aufhebung dieser Regel nicht für Privatpersonen gilt.
                </p>
                <a href="http://fintechnews.ch/p2plending/10-jahre-cashare-start-der-fintech-zeitrechnung/16242/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2018"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-nzz.jpg" alt="Finews">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zürcher Zeitung<br>
                            25.01.2018</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Der Bund vermurkst die Schweizer Fintech-Regulierung</h3>
                <div class="separator"></div>
                <p>Auf die Euphorie folgt die Ernüchterung. Der Bundesrat wollte
                    den Startups im Finanzsektor entgegenkommen – und patzte auf
                    den letzten Metern.
                </p>
                <a href="https://www.nzz.ch/wirtschaft/der-bund-vermurkst-die-schweizer-fintech-regulierung-ld.1350925" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <!-- content 2017 section start -->
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-finews.jpg" alt="Finews">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finews <br>
                            12.10.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare: Sensationelle Rendite mit Grundpfand als Sicherheit</h3>
                <div class="separator"></div>
                <p>Cashare, der Pionier im Crowdlending-Bereich, präsentiert ein
                    in seiner Art neues Projekt. Es handelt sich um ein
                    KMU-Darlehen mit einem Grundpfand als zusätzliche Sicherheit.
                    Interessiert?
                </p>
                <a href="https://www.finews.ch/service/advertorial/29212-cashare-crowlending-plattform-parcelsus-clinica-al-ronc-peer-to-peer" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fintechnews.jpg" alt="FinTech News">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>FinTech News <br>
                            04.09.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Verfrühter Jubel über die neue Fintech-Regulierung / 20er
                    Regel nur für KMU Darlehen
                </h3>
                <div class="separator"></div>
                <p>Über die Aufhebung der 20er Regel bei KMU-Darlehen bis zu CHF
                    1 Mio. hat Cashare erst im Juli berichtet und das System auf
                    den 1. August angepasst. Es wurde deutlich angemerkt, dass die
                    Aufhebung dieser Regel nicht für Privatpersonen gilt.
                </p>
                <a href="http://fintechnews.ch/p2plending/verfruhter-jubel-uber-die-neue-fintech-regulierung/12086/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/beaobachter.jpg" alt="Beobachter">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Beobachter <br>
                            17.08.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Das Geld kommt aus dem Netz</h3>
                <div class="separator"></div>
                <p>Immer mehr Firmen wenden sich für eine Finanzspritze an
                    Crowdlending-Plattformen. Dort finanzieren mehrere Investoren
                    einen Kredit. Ein interessantes Modell für beide Seiten.
                </p>
                <a href="https://www.beobachter.ch/wirtschaft/crowdlending-das-geld-kommt-aus-dem-netz" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-startupticker.jpg" alt="Startupticker">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>startupticker.ch <br>
                            16.08.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Halbherzige neue Fintech-Regeln</h3>
                <div class="separator"></div>
                <p>Die am 1. August in Kraft getretenen Änderungen der
                    Bankenverordnung sollen auch das Crowdfunding vereinfachen.
                    Eine Nachfrage des Crowdlending-Anbieters Cashare beim
                    Eidgenössischen Finanzdepartement führte nun allerdings zu der
                    Auskunft, dass sich bei Crowd-Krediten für Privatpersonen gar
                    nichts ändert. Kredite für Privatpersonen dürfen nach wie vor
                    von maximal 20 Personen finanziert werden, sonst braucht es
                    eine Banklizenz.
                </p>
                <a href="http://www.startupticker.ch/en/news/august-2017/halbherzige-neue-fintech-regeln" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-inside-it.jpg" alt="Inside-it">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>inside-it.ch <br>
                            14.08.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Schweizer Crowdlending-Fintechs loben neuen Spielraum
                    (Update)
                </h3>
                <div class="separator"></div>
                <p>Letzten Juli schaffte der Bundesrat die "20er Regel" ab. In
                    Konsequenz benötigen Kreditnehmer nun keine Bewilligung mehr,
                    wenn sie Publikumseinlagen bis zu einer Million Franken von
                    mehr als 20 Investoren entgegennehmen. Wenn sich früher mehr
                    als 20 Investoren für ein Darlehen zusammen tun wollten, so
                    wurde ein Kreditnehmer wie eine Art Bank behandelt und
                    unterstand einer Bewilligungspflicht.
                </p>
                <a href="http://www.inside-it.ch/articles/48348" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-finews.jpg" alt="Finews">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finews <br>
                            20.06.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdlending: Bewährungsprobe steht noch aus</h3>
                <div class="separator"></div>
                <p>Das klassische Kreditgeschäft der Banken gerät unter Druck.
                    Digitale Disruptoren erorbern den Markt. Was ist davon zu
                    halten?
                </p>
                <a href="http://www.finews.ch/news/banken/27791-crowdlending-die-bewährungsprobe-steht-noch-aus" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/nzzas.jpg" alt="NZZ am Sonntag">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>NZZ am Sonntag <br>
                            16.07.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Ein Feiertag für Anleger, die in Kredite investieren (nur
                    mit Online-Zugang abrufbar)
                </h3>
                <div class="separator"></div>
                <p>Es lohnt sich, in einer ruhigen Stunde die Websites von
                    Anbietern wie Cashare, Creditgate24, Creditworld, Lend,
                    Lendico und Swisspeers anzuschauen. Wer bei diesen Schweizer
                    Lending-Plattformen Geld anlegt, kann mit Zinsen zwischen 2%
                    und 7% rechnen.
                </p>
                <a href="https://epaper.nzz.ch/#article/8/NZZ am Sonntag/2017-07-16/26/223973705 " target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-peersociallending.jpg" alt="PEER & SOCIAL LENDING">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>PEER & SOCIAL LENDING <br>
                            04.07.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Peer to Peer Lending Sites -> 24 of the World’s Best</h3>
                <div class="separator"></div>
                <p>Todays post is by Chris Grundy of Bitbond. Chris is a Bitcoin
                    obsessive and avid p2p lending fan who has written for a
                    variety of Online publications as well as regularly
                    contributing to the Bitbond blog.
                </p>
                <a href="http://peersociallending.com/investing/peer-to-peer-lending-sites-16-of-the-worlds-best/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/investory.png" alt="Investory">>
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Asset Manager Magazin Nr. 5
                            - 2017 <br>
                            01.07.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Online-Kredite als Anlageklasse</h3>
                <div class="separator"></div>
                <p>«Crowdlending» ist heute in aller Munde und konnte im
                    vergangen Jahre ein markantes Wachstum verzeichnen.
                </p>
                <a href="https://www.investory.ch/de/news/magazin/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-finews.jpg" alt="Finews">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finews <br>
                            06.06.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Frischer Wind im Kreditgeschäft: Peer-to-Peer Lending in der
                    Schweiz
                </h3>
                <div class="separator"></div>
                <p>In der Schweiz haben sich aufstrebende Anbieter aus der
                    Fintech-Szene etabliert, die das traditionelle Kreditangebot
                    ergänzen und teilweise ersetzen. Werden die traditionellen
                    Banken dadurch verdrängt?
                </p>
                <a href="http://www.finews.ch/service/advertorial/27716-frischer-wind-im-kreditgeschäft-peer-to-peer-lending-in-der-schweiz" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-nzz.jpg" alt="NZZ">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zürcher Zeitung <br>
                            17.05.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Schweizer setzen vermehrt auf Crowdfunding</h3>
                <div class="separator"></div>
                <p>Von Zahlen wie in China können Schweizer Crowdfunding-Firmen
                    derzeit nur träumen. Die Vermittlung von Geldern über das
                    Internet steckt hierzulande noch in den Kinderschuhen. Die
                    jungen Fintech-Firmen wachsen aber rasant.
                </p>
                <a href="https://www.nzz.ch/wirtschaft/fintech-sektor-mit-schwung-schweizer-setzen-vermehrt-auf-crowdfunding-ld.1293990?reduced=true" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/kmuadmin.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>KMU-Portal <br>
                            26.05.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdlending: Kredite ohne Banken</h3>
                <div class="separator"></div>
                <p>Bei diesem Crowdfunding-Modell leihen die Unterstützer
                    Unternehmen oder Privatpersonen Geld und erhalten dieses
                    später mit Zinsen zurück.
                </p>
                <a href="https://www.kmu.admin.ch/kmu/de/home/praktisches-wissen/finanzielles/finanzierung/beteiligungsfinanzierung/crowdfunding/crowdlending-kredite-ohne-banken.html" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/itreseller.png" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>IT Reseller<br>
                            04.05.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare erweitert Advisory Board</h3>
                <div class="separator"></div>
                <p>Der Zuger Crowdlending-Spezialist Cashare erweitert sein
                    Advisory Board mit Dominik Witz und Harald Schnabel.
                </p>
                <a href="http://www.itreseller.ch/Artikel/84433/Cashare_erweitert_Advisory_Board.html" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-startupticker.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Startupticker.ch<br>
                            02.05.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdlending Pionier schliesst Finanzierungsrunde ab</h3>
                <div class="separator"></div>
                <p>Der Zuger Crowdlending Pionier Cashare hat die dritte
                    Finanzierungsrunde abgeschlossen. Zudem unterstützen neu ein
                    ausgewiesener Finanzrechts-Experte und eine Person mit grosser
                    Erfahrung im Börsenwesen und Aufbau von innovativen
                    Handelsplattformen das Unternehmen.
                </p>
                <a href="http://startupticker.ch/en/news/may-2017/crowdlending-pionier-schliesst-finanzierungsrunde-ab" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/basler_zeitung.png" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Basler Zeitung <br>
                            15.04.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Firmen holen sich Finanz-Spritze im Internet</h3>
                <div class="separator"></div>
                <p>KMU kommen kaum noch an Bankkredite. Geld liefern nun
                    Privatanleger. Diese Online-Plattformen wachsen rasant.
                </p>
                <a href="http://bazonline.ch/wirtschaft/statt-bei-der-bank-holen-sich-firmen-kredite-im-internet/story/25988300" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-tagesanzeiger.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Tages-Anzeiger<br>
                            14.04.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Firmen holen sich Finanz-Spritze im Internet</h3>
                <div class="separator"></div>
                <p>KMU kommen kaum noch an Bankkredite. Geld liefern nun
                    Privatanleger. Diese Online-Plattformen wachsen rasant.
                </p>
                <a href="http://www.tagesanzeiger.ch/wirtschaft/standard/Firmen-holen-sich-FinanzSpritze-im-Internet/story/25988300" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read more</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/zentralplus.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>zentralplus.ch <br>
                            31.03.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Den Kredit aus dem Internet statt von der Bank</h3>
                <div class="separator"></div>
                <p>Durch Crowdlending-Plattformen kommt manch einer einfacher
                    oder günstiger zu einem Kredit als bei einer Bank. Der
                    Schweizer Marktführer sitzt in Hünenberg. Das Unternehmen
                    vermittelt jährlich Millionen an Private und KMU.
                </p>
                <a href="https://www.zentralplus.ch/de/news/wirtschaft/5528899/Den-Kredit-aus-dem-Internet-statt-von-der-Bank.htm" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-cash.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>cash <br>
                            16.03.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>«Die Finanzkrise bescherte uns mehr Glaubwürdigkeit»</h3>
                <div class="separator"></div>
                <p>Crowdlending-Plattformen wollen etablierten
                    Konsumkredit-Banken den Markt abgraben. Im cash-Interview gibt
                    Michael Borter, CEO der ersten Schweizer
                    Crowdlending-Plattform Cashare, einen Überblick über das
                    Business.
                </p>
                <a href="https://www.cash.ch/news/top-news/kredite-ohne-bankbeteiligung-die-finanzkrise-bescherte-uns-mehr-glaubwuerdigkeit-1051584" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2017"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/20min.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>20 Minuten <br>
                            02.03.2017</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Kredit-Plattform macht Private zu Mini-Banken</h3>
                <div class="separator"></div>
                <p>Kredite statt Taxifahrten: Schweizer Plattformen verknüpfen
                    private Geldgeber mit Kreditsuchenden. Ist das riskant?
                </p>
                <a href="http://www.20min.ch/finance/news/story/Kredit-Plattform-macht-Private-zu-Mini-Banken-12930384" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <!-- content 2016 section start -->
    <div class="col-sm-6 col-md-3 item 2016"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/Swiss_Credit_Management_Forum.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>1st Swiss Credit Management
                            Forum <br>
                            30.11.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowd lending and what it means for funding</h3>
                <div class="separator"></div>
                <p>The 1st Swiss Credit Management Forum ook place at November
                    30, 2016 in the Technopark in Zurich.
                </p>
                <a href="http://www.creditmanager.ch/tl_files/vfcms/pdf/First Swiss Credit Mgt Forum Invitation.pdf" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/bilanz.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Bilanz <br>
                            28.11.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdlending – die neue Art der Finanzierung</h3>
                <div class="separator"></div>
                <p>Bank, adieu! Auf Online-Plattformen vergeben private Anleger
                    direkt Kredite. Das Risiko ist geringer, als man denkt, und
                    die Renditen lassen sich sehen.
                </p>
                <a href="http://www.bilanz.ch/invest/crowdlending-die-neue-art-der-finanzierung-749951" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-finews.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>finews.ch <br>
                            25.11.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>News ganz kurz – November 2016</h3>
                <div class="separator"></div>
                <p>Die Crowdlending-Plattform Cashare ist mit dem Anbieter von
                    erneuerbaren Energien Fairpower eine Kooperation zur
                    Finanzierung von Solaranlagen eingegangen.
                </p>
                <a href="http://www.finews.ch/news/news-ganz-kurz/24967-news-ganz-kurz-november-2016" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-moneycab.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>moneycab <br>
                            25.11.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cleantech trifft Fintech – Megatrends ohne Atomstrom</h3>
                <div class="separator"></div>
                <p>Die Abstimmung über die Atomausstiegsinitiative steht
                    unmittelbar bevor und wirft unter anderem die Frage auf, wie
                    nachhaltige Energie bestmöglich finanziert werden kann.
                </p>
                <a href="http://www.moneycab.com/2016/11/25/fairpower-cleantech-trifft-fintech-megatrends-ohne-atomstrom/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-inside-it.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>inside-it.ch <br>
                            25.11.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Fintech im Einsatz für erneuerbare Energien</h3>
                <div class="separator"></div>
                <p>Das Fintech-Unternehmen Cashare und der Energieanbieter
                    Fairpower wollen mit einer neuen Partnerschaft eine Lösung zur
                    Finanzierung von Solarstromanlagen bieten.
                </p>
                <a href="http://www.inside-it.ch/articles/45779" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/beaobachter.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Beobachter <br>
                            25.11.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Für den Kredit nicht mehr zur Bank</h3>
                <div class="separator"></div>
                <p>«Disruptiv» ist ein Lieblingswort von Michael Borter.
                    «Disruptiv» sind Innovationen, wenn sie bestehende
                    Dienstleistungen möglicherweise vollständig verdrängen.
                </p>
                <a href="http://www.beobachter.ch/wirtschaft/artikel/sharing-economy_eine-hippe-geldmaschine/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fuw.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finanz und Wirtschaft <br>
                            24.11.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Fintech – (R)evolution der Schweizer Finanzindustrie</h3>
                <div class="separator"></div>
                <p>Alle reden über Fintech. Doch woher kommt das Wort, was
                    steckt dahinter, und welche Geschichte nahm die
                    Digitalisierung der Schweizer Finanzbranche bis heute?
                </p>
                <a href="http://www.fuw.ch/article/fintech-revolution-der-schweizer-finanzindustrie/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/bilanz.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Bilanz <br>
                            21.10.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Kredite für Rendite</h3>
                <div class="separator"></div>
                <p>Bank, adieu! Auf Online-Plattformen vergeben private Anleger
                    direkt Kredite. Das Risiko ist geringer, als man denkt und die
                    Renditen lassen sich sehen.
                </p>
                <a href="/assets/static/images/pdfs/2016-10-21_Bilanz.pdf" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/rsila1.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>RSI LA 1 <br>
                            14.10.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Quando la tecnologia sostituisce il banchiere</h3>
                <div class="separator"></div>
                <p>La digitalizzazione arriva anche nella finanza, è il
                    cosiddetto fintech, o tecnofinanza
                </p>
                <a href="http://www.rsi.ch/la1/programmi/informazione/tempi-moderni/in-prima/Quando-la-tecnologia-sostituisce-il-banchiere-8173957.html" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/AssetManager.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Asset Manager 2016 <br>
                            01.10.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Aus dem Tagebuch eines Schweizer Fintech-Pioniers (S. 12-13)</h3>
                <div class="separator"></div>
                <p>Von der Idee zum FinTech - Die Geschichte von Cashare beginnt
                    bereits einige Zeit vor der Gründung im Januar 2008.
                </p>
                <a href="https://indd.adobe.com/view/762f3fac-2f3c-4024-bf8b-9f736e550596" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/erfolg.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Erfolg <br>
                            01.09.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdlending finanziert Kinderfahrrad Projekt</h3>
                <div class="separator"></div>
                <p>UrbanRider produziert Fahrräder seit 2012, welche die Kunden
                    online oder im Shop selber gestalten können.
                </p>
                <a href="/assets/static/images/pdfs/2016-09-01_Erfolg_Crowdlending_finanziert_Kinderfahrrad_Projekt.pdf" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/Aargauer_Zeitung.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Aargauer Zeitung <br>
                            15.08.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>CD aufnehmen, Buch drucken, Konzert veranstalten: Kreative
                    flirten mit Schwarm-Geld
                </h3>
                <div class="separator"></div>
                <p>Das Finanzieren von Projekten oder Firmen durch eine Vielzahl
                    von Menschen ist im Trend, besonders in der Kreativwirtschaft.
                    Schwarm-Finanzierung ist dennoch nichts für Schwärmer, denn
                    auch hier wird gerechnet.
                </p>
                <a href="http://www.aargauerzeitung.ch/wirtschaft/cd-aufnehmen-buch-drucken-konzert-veranstalten-kreative-flirten-mit-schwarm-geld-130489549" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/nzzas.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>NZZ am Sonntag <br>
                            14.08.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Sieben Fragen an Michael Borter</h3>
                <div class="separator"></div>
                <p>«Die Banker sind nachdenklicher geworden»</p>
                <a href="/assets/static/images/pdfs/2016-08-14_NZZaS_7_Fragen_an.pdf" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-nzz.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zürcher Zeitung <br>
                            29.07.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Welche Crowdlending-Anbieter prägen den Schweizer Markt?</h3>
                <div class="separator"></div>
                <p>2015 wurden in der Schweiz knapp 8 Mio. Fr. über
                    Kreditplattformen vermittelt. Die Crowdlending-Branche in der
                    Schweiz ist derzeit noch übersichtlich und wird von jungen
                    Unternehmen geprägt.
                </p>
                <a href="http://www.nzz.ch/wirtschaft/wirtschaftspolitik/neue-kreditplattformen-welche-crowdlending-anbieter-praegen-den-schweizer-markt-ld.108168" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-youtube.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Youtube<br>
                            25.07.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>“FinTech Made in Switzerland”<br>
                    Interview Michael Borter, Cashare
                </h3>
                <div class="separator"></div>
                <p>Der Film bildet das FinTech Ökosystem in der Schweiz ab in
                    Konversationen mit Startup-Gründern, Vordenkern im
                    Finanzsektor, Banken, Regulatoren, Politikern, und
                    Finanzexperten.
                </p>
                <a href="https://www.youtube.com/watch?v=8f_CRChcUIo" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-startupticker.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Startupticker.ch<br>
                            20.07.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>The era of Crowdlending opens new market for startups</h3>
                <div class="separator"></div>
                <p>Crowdlending in Switzerland is still in the early stage yet
                    significant growth results have been recorded. Last year, a
                    growth rate of +126% was recorded with CHF 7.9 million
                    invested in 266 projects.
                </p>
                <a href="http://www.startupticker.ch/en/news/july-2016/the-era-of-crowlending-opens-new-market-for-startups" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-the-business-times.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>The Business Times<br>
                            12.07.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Swiss business association keen to play a bigger role</h3>
                <div class="separator"></div>
                <p>AS both Singapore and Switzerland place great value on human
                    capital and local talent, developing them further offers an
                    avenue for fruitful mutual cooperation, says Tom Ludescher....
                </p>
                <a href="http://www.businesstimes.com.sg/hub/swiss-state-visit/swiss-business-association-keen-to-play-a-bigger-role" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-12er.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>12-App<br>
                            19.05.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Nun kann jeder zur Bank werden</h3>
                <div class="separator"></div>
                <p>CindyUH sitzt ungeduldig vor dem Computer. Noch zwanzig
                    Minuten dauert’s, bis die Konditionen für die Finanzierung
                    ihres Autos feststehen. Derzeit müsste sie für die 13'000
                    Franken. drei Jahre lang 7 Prozent Zins bezahlen – ausser...
                </p>
                <a href="http://desktop.12app.ch/articles/25675128" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-tagesanzeiger.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Tagesanzeiger<br>
                            19.05.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Lieber ins Netz statt zur Bank</h3>
                <div class="separator"></div>
                <p>Längst werden nicht mehr nur Musik-CDs oder Theaterstücke per
                    Crowdfunding finanziert. Auch Kleinfirmen holen sich zunehmend
                    so Geld.<br>
                    Die drei Unternehmensberater sind gut unterwegs. In ihrem
                    dritten...
                </p>
                <a href="http://www.tagesanzeiger.ch/wirtschaft/konjunktur/lieber-ins-netz-statt-zur-bank/story/27165343" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fuw.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finanz und Wirtschaft<br>
                            19.05.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Der Anleger wird zur Bank</h3>
                <div class="separator"></div>
                <p>Im Konsumkreditmarkt werden die Spielregeln geändert. Dank
                    Internetplattformen können alle zum Geldgeber werden.
                </p>
                <a href="http://www.fuw.ch/article/der-anleger-wird-zur-bank/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-moneycab.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>moneycab<br>
                            18.05.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Immer mehr Crowdfunding-Plattformen drängen auf den Markt</h3>
                <div class="separator"></div>
                <p>Immer mehr Crowdfunding-Plattformen drängen auf den Markt</p>
                <a href="http://www.moneycab.com/2016/05/18/immer-mehr-crowdfunding-plattformen-draengen-auf-den-markt/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-financeblog.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Hochschule Luzern<br>
                            18.05.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdfunding in der Schweiz mit hohem Wachstum</h3>
                <div class="separator"></div>
                <p>Crowdfunding in der Schweiz mit hohem Wachstum</p>
                <a href="https://blog.hslu.ch/retailbanking/2016/05/18/crowdfunding-in-der-schweiz-mit-hohem-wachstum/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-startupticker.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>startupticker<br>
                            08.04.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare vermittelt Kredite von über einer Million Franken
                    pro Monat
                </h3>
                <div class="separator"></div>
                <p>Die Crowdlending-Plattform Cashare hat im ersten Quartal 2016
                    eine Schallmauer durchbrochen. Zum ersten Mal wurde mehr als
                    eine Million Franken pro Monat vermittelt. Zum Erfolg trugen
                    Privatdarlehen sowie das neue Produkt KMU-Kredit bei.
                </p>
                <a href="http://www.startupticker.ch/en/news/april-2016/cashare-vermittelt-kredite-von-uber-einer-million-franken-pro-monat?utm_source=newsletter193&utm_medium=email&utm_campaign=newsletter193/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-zuger_zeitung.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zuger Zeitung<br>
                            05.04.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare bricht Millionenmarke</h3>
                <div class="separator"></div>
                <p>Die Crowdlending-Plattform Cashare hat im ersten Quartal 2016
                    eine Schallmauer durchbrochen. Zum ersten Mal wurde mehr als
                    eine Million Franken pro Monat vermittelt. Zum Erfolg trugen
                    Privatdarlehen sowie das neue Produkt KMU-Kredit bei.
                </p>
                <a href="https://www.zugerzeitung.ch/importe/fupep/neue_lz/lz_wirtschaft/Cashare-bricht-Millionenmarke;art128773,711209" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-kgeld.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>KGeld<br>
                            30.04.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Woher soll ich einen Kredit nehmen?</h3>
                <div class="separator"></div>
                <p>Ich werde im Mai 65 – und zum ersten Mal in meinem ­Leben
                    brauche ich einen Kredit von 15000 Franken. Was raten Sie mir?
                </p>
                <a href="https://www.kgeld.ch/artikel/d/woher-soll-ich-einen-kredit-nehmen/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-swissquotemagazin.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Swissquote Magazin<br>
                            01.03.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Peer-to-Peer- Geschäfte abseits der Banken</h3>
                <div class="separator"></div>
                <p>In ein Start-up investieren, einen Kredit aufnehmen, Geld
                    transferieren oder Vermögen verwalten: Solche Aktivitäten
                    finden mittlerweile (auch) zwischen Privatpersonen statt. Wir
                    stellen vier Beispiele vor
                </p>
                <a href="https://www.cashare.ch/file/web/company/press/2016-03-01_Swissquote_Magazine_FinancePtoP-de.pdf" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-lending-school.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Lending School<br>
                            25.02.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Schweizer Cashare wird Teil von Deutsche Börse Venture
                    Network
                </h3>
                <div class="separator"></div>
                <p>Das Deutsche Börse Venture Network nimmt den Kreditmarktplatz
                    Cashare in sein Programm auf. Das innovative Netzwerk will
                    führende Wachstumsunternehmen mit internationalen Investoren
                    zusammenbringen.
                </p>
                <a href="http://www.lending-school.de/blog/schweizer-cashare-wird-teil-von-deutsche-boerse-venture-network/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-3.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>startupticker<br>
                            23.02.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Cashare wird Teil des Deutsche Börse Venture Network</h3>
                <div class="separator"></div>
                <p>Das Deutsche Börse Venture Network nimmt den Schweizer
                    Crowdlending-Pionier Cashare ab sofort in sein innovatives
                    Programm mit führenden Wachstumsunternehmen und nationalen
                    sowie internationalen Investoren auf.
                </p>
                <a href="http://startupticker.ch/en/news/february-2016/cashare-wird-teil-des-deutsche-borse-venture-network#.VswV8fdxCpM.twitter" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-c36daily.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>C36daily<br>
                            22.02.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>ICT-Startups</h3>
                <div class="separator"></div>
                <p>Einer der Schweizer Fintech-Pioniere, die seit 2008 in
                    Hünenberg ansässige Cashare, ist in das «Venture Network» der
                    Deutschen Börse aufgenommen worden. Dort hat man nun Zugang zu
                    Investitionsmitteln von nationalen und internationalen
                    Investoren.
                </p>
                <a href="http://www.c36daily.ch/2016/02/22/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-crowdstreet.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Crowdstreet<br>
                            19.02.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdlending-Plattform Cashare wird Teil des Deutsche
                    BörseVenture Network
                </h3>
                <div class="separator"></div>
                <p>Das Deutsche Börse Venture Network nimmt Cashare ab sofort in
                    sein innovatives Programm aus führenden Wachstumsunternehmen
                    und nationalen sowie internationalen Investoren auf.
                </p>
                <a href="http://crowdstreet.de/2016/02/19/crowdlending-plattform-cashare-wird-teil-des-deutsche-boerse-venture-network/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-inside-it.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>inside-it<br>
                            19.02.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Zuger Fintech Cashare schafft es an die Quelle</h3>
                <div class="separator"></div>
                <p>Das Deutsche Börse Venture Network nimmt Cashare ab sofort in
                    sein innovatives Programm aus führenden Wachstumsunternehmen
                    und nationalen sowie internationalen Investoren auf.
                </p>
                <a href="http://www.inside-it.ch/articles/43008" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-p2p-kredite.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>p2p-kredite<br>
                            19.02.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3> Schweizer P2P Kreditmarktplatz Cashare wird Teil des
                    Deutsche Börse Venture Networks
                </h3>
                <div class="separator"></div>
                <p>Die Gruppe Deutsche Börse hilft mit dem Venture Netzwerk die
                    Finanzierungssituation von jungen und wachstumsstarken
                    Unternehmen zu verbessern.
                </p>
                <a href="http://www.p2p-kredite.com/schweizer-p2p-kreditmarktplatz-cashare-wird-teil-des-deutsche-boerse-venture-networks_2016.html" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-finews.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finews<br>
                            19.02.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Das Schweizer Fintech-Unternehmen Cashare ist in das
                    Deutsche Börse Venture Network aufgenommen worden
                </h3>
                <div class="separator"></div>
                <p>Cashare erhält damit Zugang zu einem internationalen Netzwerk
                    von Partnern und Investoren. Cashare betreibt eine Plattform
                    für die Vermittlung von Krediten.
                </p>
                <a href="http://www.finews.ch/news/news-ganz-kurz/21819-news-ganz-kurz-februar-2016" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-cash.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>cash<br>
                            17.02.2016</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Die hohen Renditen der Direkt-Kredite</h3>
                <div class="separator"></div>
                <p>Immer mehr Online-Plattformen offerieren
                    Peer-to-Peer-Kredite, also Darlehen ausserhalb des
                    Bankensystems. Für Anleger können dabei erstaunlich hohe
                    Renditen rausspringen. Aber wie sicher sind solche
                    Investments?
                </p>
                <a href="http://www.cash.ch/news/alle/die_hohen_renditen_der_direktkredite-3419607-448" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2016"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-nzz.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>NZZ<br>
                            11.02.2016 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3> Klick, Klick, Kredit</h3>
                <div class="separator"></div>
                <p>Die Zahl der neuen Anbieter von Krediten mit effizienten
                    technologischen Plattformen steigt rasch an. Sie machen
                    traditionellen Banken vermehrt die Kundschaft abspenstig.
                </p>
                <a href="http://www.nzz.ch/finanzen/aktien/klick-klick-kredit-1.18675042" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <!-- // content 2016 section start -->
    <!-- content 2015 section start -->
    <div class="col-sm-6 col-md-3 item 2015"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-zuger_zeitung.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zuger Zeitung<br>
                            19.12.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Sharing Economy in der Zentralschweiz</h3>
                <div class="separator"></div>
                <p>Wie können Städte von der Sharing Economy profitieren? Dieser
                    Frage gehen Forscher der Hochschule Luzern um Professor
                    Dominik Georgi in einem Forschungsprojekt nach.
                </p>
                <a href="https://www.zugerzeitung.ch/importe/fupep/neue_lz/lz_markt/Sharing-Economy-in-der-Zentralschweiz;art128774,651223" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015">
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-financeblog.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>IFZ Retail Banking Blog<br>
                            09.11.2015</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>KMU-Kredite über Crowdfunding – der Schweizer Markt bewegt
                    sich
                </h3>
                <div class="separator"></div>
                <p>Crowdlending wurde in der Schweiz lange Zeit nur für private
                    Kreditnehmer angeboten. Als Schweizer Pionier in diesem
                    Bereich vermittelt Cashare seit 2008 Darlehen (Konsumkredite)
                    zwischen Privatpersonen.
                </p>
                <a href="https://blog.hslu.ch/retailbanking/2015/11/09/kmu-kredite-ueber-crowdfunding-der-schweizer-markt-bewegt-sich/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015">
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-cash.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Cash<br>
                            07.09.2015</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdsourcing ist Alternative geworden</h3>
                <div class="separator"></div>
                <p>Crowdfunding und Crowdsourcing - ein Problem für
                    traditionelle Unternehmen? Jan Marco Leimeister von der
                    Universität St. Gallen sieht eher die Chancen. Und er erklärt,
                    was Crowdsourcing mit Gummibärchen zu tun hat.
                </p>
                <a href="https://www.cash.ch/news/politik/crowdsourcing-ist-alternative-geworden-364721" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-tagesanzeiger.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Tagesanzeiger<br>
                            17.08.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Private Banking ohne Bankberater</h3>
                <div class="separator"></div>
                <p>Fintech ist das neue Schlagwort in der Finanzbranche. Wer die
                    Newcomer sind, was es konkret für Kunden bedeutet und wie sich
                    Banken vorsehen.
                </p>
                <a href="http://www.tagesanzeiger.ch/wirtschaft/geld/Private-Banking-ohne-Bankberater/story/18235475" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-bilan.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Bilan<br>
                            10.08.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Comment investir dans le crowdfunding</h3>
                <div class="separator"></div>
                <p>Le monde du financement participatif est peu réglementé. Il
                    faut donc bien examiner les conditions de la plateforme sur
                    laquelle vous investissez. Le rendement peut en valoir la
                    peine.
                </p>
                <a href="http://www.bilan.ch/argent-finances-plus-de-redaction/comment-investir-crowdfunding" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015">
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-letemps.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Le Temps<br>
                            09.08.2015</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Le nombre de plateformes va diminuer</h3>
                <div class="separator"></div>
                <p>Au sein du financement participatif, le «crowdlending»,
                    l’octroi de crédits entre particuliers, est le segment qui a
                    le plus progressé en Suisse en 2014, avec un volume de prêts
                    qui a doublé à 3,5 millions de francs l’an dernier.
                </p>
                <a href="https://www.letemps.ch/economie/2015/08/09/nombre-plateformes-va-diminuer" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015">
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-financeblog.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>IFZ Retail Banking Blog<br>
                            22.06.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Wer möchte eigentlich einen Kredit über Crowdfunding?</h3>
                <div class="separator"></div>
                <p>Crowdlending (auch P2P-Lending) war in der Schweiz bisher nur
                    für private Kreditnehmer möglich. Entsprechend steht der Markt
                    in direkter Konkurrenz mit dem herkömmlichen
                    Konsumkreditmarkt.
                </p>
                <a href="https://blog.hslu.ch/retailbanking/2015/06/22/wer-moechte-eigentlich-einen-kredit-ueber-crowdfunding/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-financeblog.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>IFZ Retail Banking Blog<br>
                            22.05.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Crowdfunding Monitoring 2015 – Studie zum Schweizer
                    Crowdfunding Markt
                </h3>
                <div class="separator"></div>
                <p>2014 wurden in der Schweiz 15.8 Millionen Franken durch
                    Crowdfunding vermittelt – im Vorjahr waren es noch 11.6
                    Millionen Franken. Das zeigt das «Crowdfunding Monitoring
                    2015» der Hochschule Luzern und Swisscom.
                </p>
                <a href="https://blog.hslu.ch/retailbanking/2015/05/22/crowdfunding-monitoring-2015-studie-zum-schweizer-crowdfunding-markt/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-srf10vor10.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>SRF 10vor10<br>
                            05.05.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Fintech auf dem Vormarsch</h3>
                <div class="separator"></div>
                <p>Die UBS hat heute das beste Quartalsergebnis seit fast fünf
                    Jahren vorgelegt. UBS-Konzernchef Sergio Ermotti ist sehr
                    zufrieden, warnt aber vor überzogenen Erwartungen. Denn es
                    warten Herausforderungen. Die junge Finanz-Tech-Branche macht
                    sich auf, die Banken bei klassischen Dienstleistungen
                    auszustechen.
                </p>
                <a href="http://www.srf.ch/play/tv/10vor10/video/fintech-auf-dem-vormarsch?id=110cf4e3-b882-447c-8ba0-545558eadd96" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-srf.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>SRF3 Input<br>
                            26.04.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Wozu brauchen wir noch Banken?</h3>
                <div class="separator"></div>
                <p>Zinsen gibts keine mehr, dafür Gebühren. Kredite erhalten wir
                    auch von der Crowd - nicht mehr nur von der Sparkasse. Wozu
                    brauchen wir noch Banken - wozu brauchen die Banken uns
                    Kunden?
                </p>
                <a href="http://www.srf.ch/sendungen/input/wozu-brauchen-wir-noch-banken" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015">
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-fuw.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Finanz und Wirtschaft<br>
                            08.04.2015</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Finanz und Wirtschaft Forum Fintech 2015</h3>
                <div class="separator"></div>
                <p>FuW-Forum "FinTech 2015 - Innovation in Finance" vom 8. April
                    2015 im The Dolder Grand, Zürich
                </p>
                <a href="https://zuercher-bankenverband.ch/tages-anzeiger-forum-algorithmus-big-data-strategien-fuer-das-unternehmen-von-morgen/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-nzz.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zürcher Zeitung<br>
                            30.03.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Anleger entdecken das Social Lending</h3>
                <div class="separator"></div>
                <p>Private Anleger entdecken die Kreditvergabe über
                    Internet-Plattformen. Die Auswahl für hiesige Investoren ist
                    überschaubar, die Unterschiede zwischen den Angeboten sind
                    gross. Das sollte man beachten.
                </p>
                <a href="http://www.nzz.ch/finanzen/anleger-entdecken-das-social-lending-1.18512670" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-handelszeitung.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Handelszeitung<br>
                            07.03.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Neue Anbieter locken mit Krediten zu Tiefstzinsen</h3>
                <div class="separator"></div>
                <p>Trotz tiefen Zinsen bleibt das Geschäft mit Krediten
                    profitabel. Ein neuer Online-Vermittler will nun den Markt
                    aufmischen.
                </p>
                <a href="http://www.handelszeitung.ch/unternehmen/neue-anbieter-locken-mit-krediten-zu-tiefstzinsen-749313" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2015" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-kgeld.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>KGeld<br>
                            04.02.2015 </em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Bank spielen und Geld verdienen</h3>
                <div class="separator"></div>
                <p>Nicht nur Banken vergeben Kredite. Auch Privatleute können
                    anderen Konsumenten ein Darlehen geben und so Zinsen kassieren
                    – und zwar per Internet. So funktioniert das sogenannte
                    Crowdlending.
                </p>
                <a href="https://www.cashare.ch/file/pdf/2015-02-04_KGeld_Bank_spielen_und_Geld_verdienen.pdf" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <!-- // content 2015 section end -->
    <!-- // content 2014 section start -->
    <div class="col-sm-6 col-md-3 item 2014">
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-rsi.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>RSI.ch Radiotelevisione
                            Svizzera<br>
                            17.10.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Fenomeno crowdlending</h3>
                <div class="separator"></div>
                <p>I prestiti da privati sono una realtà in Gran Bretagna, in
                    Svizzera siamo ai primordi
                </p>
                <a href="http://www.rsi.ch/news/economia/Fenomeno-crowdlending-2749300.html" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-nzz.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zürcher Zeitung<br>
                            30.06.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Neue Konkurrenz für Banken</h3>
                <div class="separator"></div>
                <p>Hypothekenvermittler sowie Finanz- und
                    Crowdfunding-Plattformen kämpfen zunehmend mit Banken um
                    Marktanteile.
                </p>
                <a href="http://www.nzz.ch/wirtschaft/neue-konkurrenz-fuer-banken-1.18331468" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-financeblog.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>IFZ Retail Banking Blog<br>
                            30.06.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Rückblick auf die Konferenz “Innovative Angebote im Retail
                    Banking”
                </h3>
                <div class="separator"></div>
                <p>Die Konferenz „Innovative Angebote im Retail Banking“ fand am
                    Donnerstag, 26 Juni 2014 am Institut für
                    Finanzdienstleistungen Zug IFZ statt und wurde von knapp 120
                    Personen besucht.
                </p>
                <a href="https://blog.hslu.ch/retailbanking/2014/06/30/rueckblick-auf-die-konferenz-innovative-angebote-im-retail-banking/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-handelszeitung.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Handelszeitung<br>
                            15.05.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Von der Nische zur Masse</h3>
                <div class="separator"></div>
                <p>Der Schweizer Markt steckt noch in den Kinderschuhen.
                    Anbieter und Experten rechnen mit hohem Wachstumspotenzial.
                </p>
                <a href="https://www.cashare.ch/file/pdf/2014-05-15_Handelszeitung.pdf" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014" >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-tagesanzeiger.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Tages Anzeiger<br>
                            14.05.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Der Investorenschwarm formiert sich</h3>
                <div class="separator"></div>
                <p>Der Schweizer Markt steckt noch in den Kinderschuhen.
                    Anbieter und Experten rechnen mit hohem Wachstumspotenzial.
                </p>
                <a href="http://www.tagesanzeiger.ch/wirtschaft/geld/Der-Investorenschwarm-formiert-sich/story/29430756" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-zuger_zeitung.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zuger Zeitung<br>
                            14.05.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Der alternative Weg zum Kredit</h3>
                <div class="separator"></div>
                <p>Immer öfter werden Kredite im Internet vergeben. Doch eine
                    Bank ist nicht im Spiel. Beim Crowdfunding kann jeder Geld
                    geben, der ein Projekt unterstützen will.
                </p>
                <a href="https://www.zugerzeitung.ch/importe/fupep/neue_lz/lz_markt/Der-alternative-Weg-zum-Kredit;art128774,372307" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-zuger_zeitung.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zuger Zeitung<br>
                            14.05.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Was ist der Vorteil an Crowdfunding?</h3>
                <div class="separator"></div>
                <p>Interview mit Andreas Dietrich: Mitglied der Leitung des
                    Instituts für Finanzdienstleistungen Zug (IFZ)
                </p>
                <a href="https://www.zugerzeitung.ch/importe/fupep/neue_lz/lz_markt/Was-ist-der-Vorteil-an-Crowdfunding;art128774,372301" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014"   >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-financeblog.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>IFZ Retail Banking Blog<br>
                            14.05.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Erste Studie zum Schweizer Crowdfunding Markt</h3>
                <div class="separator"></div>
                <p>Crowdfunding Monitoring 2014 – erste Studie zum Schweizer
                    Crowdfunding Markt
                </p>
                <a href="https://blog.hslu.ch/retailbanking/2014/05/14/crowdfunding-monitoring-2014-erste-studie-zum-schweizer-crowdfunding-markt/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014"  >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-finance20.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Wirtschaftsmagazin PUNKT,
                            Nr. 2-2014 und auf Blog Finance 2.0: Kredite von der Crowd<br>
                            16.04.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Peer-to-Peer Lending</h3>
                <div class="separator"></div>
                <p>Privatpersonen investieren in Privatpersonen. Investoren
                    stellen Geld für Privatkredite zur Verfügung. So einfach ist
                    das Modell für Peer-to-Peer Lending. Handelt es sich um ein
                    Nischenangebot oder müssen die Banken nun zittern?
                </p>
                <a href="http://www.finance20.ch/kredite-von-der-crowd-peer-to-peer/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-aargauerzeitung.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Aargauer Zeitung<br>
                            11.04.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Free Banking: Hat die klassische Bank bald ausgedient?</h3>
                <div class="separator"></div>
                <p>Datenlecks sind die Begleiterscheinung einer grossen
                    digitalen Umwälzung im Bankgeschäft. Die Neuerungen bringen
                    aber auch Vorteile. Dienstleistungen werden transparenter,
                    günstiger und besser. Der Gang zum Bankschalter wird zunehmend
                    überflüssig.
                </p>
                <a href="http://www.aargauerzeitung.ch/wirtschaft/free-banking-hat-die-klassische-bank-bald-ausgedient-127872875" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 item 2014"    >
        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
            <div class="overlay-container">
                <img class="img-fluid" src="/public_pages/wp-content/uploads/2020/07/portfolio-nzz.jpg" alt="">
                <div class="overlay-to-top">
                    <p class="small margin-clear"><em>Neue Zürcher Zeitung<br>
                            24.03.2014</em>
                    </p>
                </div>
            </div>
            <div class="body">
                <h3>Leihe deinem Nächsten</h3>
                <div class="separator"></div>
                <p>Privatpersonen investieren in Privatpersonen. Investoren
                    stellen Geld für Privatkredite zur Verfügung. So einfach ist
                    das Modell für Peer-to-Peer Lending. Handelt es sich um ein
                    Nischenangebot oder müssen die Banken nun zittern?
                </p>
                <a href="http://www.nzz.ch/finanzen/leihe-deinem-naechsten-1.18268886" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read
                    More</a>
            </div>
        </div>
    </div>

</div>
