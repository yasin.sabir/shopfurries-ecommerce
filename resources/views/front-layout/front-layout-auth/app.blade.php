<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        @yield('title')
    </title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('front-layout.head')
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/my-custom.css') }}">


    @yield('custom-front-css')
</head>
<body>

<!-- main -->
@yield('Main')

@include('front-layout.script')
@yield('custom-front-script')


</body>
</html>
