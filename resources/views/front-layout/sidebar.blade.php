
<div class="siteBAR">
    <div class="sidbarLogo">
        <div class="logo-area">
            <a href="{{ route('home') }}">
                <img src="{{ asset('front-end/assets/img/mainLogo.svg')}}" alt="">
                <img class="text-logos" src="{{ asset('front-end/assets/img/bottomLogo.svg') }}" alt="">
            </a>
        </div>

    </div>
    <nav>
        <ul>
            <li><a href="{{ route('home') }}">home</a></li>
            <li><a href="{{ route('shop') }}">shop</a></li>
            <li><a href="{{ route('categories') }}">categories</a></li>
            <li><a href="{{ route('gallery') }}">gallery</a></li>
            <li><a href="{{ route('faqs') }}">FAQs</a></li>
            <li><a href="{{ route('about') }}">ABOUT</a></li>
            <li><a href="{{ route('contact-us') }}">CONTACT</a></li>
        </ul>
    </nav>
</div>

