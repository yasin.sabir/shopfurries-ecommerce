<style>
    #myBtn {
        display: none; /* Hidden by default */
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        border: none;
        outline: none;
        background-color: #016FB9;
        color: white;
        cursor: pointer;
        padding: 12px;
        border-radius: 100%;
        font-size: 18px;
        height: 50px;
        width: 50px;
        box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
    }

    #myBtn:hover {
        background-color: #555; /* Add a dark-grey background on hover */
    }
</style>
<footer>
    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="far fa-angle-up"></i></button>

    <script>
        //Get the button:
        mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    </script>

    <div class="footer-area">
        <div class="container">
            <div class="row news-latter">  <!-- news-latter -->
                <div class="col-lg-6">
                    <h4>SIGN UP FOR <b>OUR NEWSLETTER</b></h4>
                </div>
                <div class="col-lg-6">
                    <div class="news-letter-form">
                        <form action="">
                            <input type="text" placeholder="Enter your E-mail">
                            <img src=" {{ asset('front-end/assets/img/Message.svg')}}" alt="">
                            <button type="button">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="payment-logo">
                        <ul>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/payPal.svg')}}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/masterCard.svg')}}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/vissa.svg')}}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/PamentCard.svg')}}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('front-end/assets/img/Sofort.svg')}}" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 nINlg">
                    <div class="footer-logo-area text-center">
                        <a href="#">
                            <img src="{{ asset('front-end/assets/img/mainLogo.svg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 offset-md-1">
                    <div class="footer-link-left  text-center">
                        <ul>
                            <li><a href="{{ route('shop') }}">SHOP</a></li>
                            <li><a href="{{ route('categories') }}">CATEGORIES</a></li>
                            <li><a href="{{ route('gallery') }}">GALLERY</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 main-Flogo">
                    <div class="footer-logo-area text-center">
                        <a href="#">
                            <img src="{{ asset('front-end/assets/img/mainLogo.svg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-right-link">
                        <ul>
                            <li><a href="{{ route('faqs') }}">FAQs</a></li>
                            <li><a href="{{ route('contact-us') }}">CONTACT</a></li>
                            <li><a href="{{ route('about') }}">ABOUT</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copy-right">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <p>©2019  Torben Goldmund Art-N-Prints All Rights Reserved</p>
                </div>
                <div class="col-lg-6">
                    <ul>
                        <li><a href="#">ACCOUNTS</a></li>
                        <li><a href="#">PRIVACY</a></li>
                        <li><a href="#">TERMS & CONDITIONS</a></li>
                        <li><a href="#">LEGAL NOTICE</a></li>
                    </ul>
                </div>
                <div class="col-lg-2">
                    <div class="footer-social text-right">
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

