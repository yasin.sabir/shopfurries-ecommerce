{{--<link rel="manifest" href="site.webmanifest">--}}
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
<!-- Place favicon.ico in the root directory -->
<!-- CSS here -->
<link rel="stylesheet" href="{{ asset('front-end/assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/magnific-popup.css') }}">

<link rel="stylesheet" href="{{asset('theme-assets/dist/css/adminlte.min.css')}}">

<link rel="stylesheet" href="{{ asset('front-end/assets/css/fontawesome-all.min.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/themify-icons.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/meanmenu.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/slick.css') }}">
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/gallery.css')}}" type="text/css">
{{--<link rel="stylesheet" href="https://swiperjs.com/package/css/swiper.min.css">--}}
<link rel="stylesheet" href="{{ asset('front-end/assets/css/swiper.min.css')}}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/nice-select.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/jquery.nice-number.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/responsive.css') }}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/jquery.nice-number.css') }}">
<link rel="stylesheet" href="{{asset('theme-assets/plugins/toastr/toastr.min.css')}}">
<link rel="stylesheet" href="{{asset('other-assets/lightbox/css/lightbox.css')}}">
<link rel="stylesheet" href="{{asset('front-end/assets/css/custom-stripe.css')}}">
<link rel="stylesheet" href="{{ asset('front-end/assets/css/custom-css.css') }}">
<script src="https://js.stripe.com/v3/"></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />
