<div class="search-area">
    <div class="container">
        <form id="product_search_form" method="get" action="{{route('searchResult')}}">
            <div class="inp">
                <input id="searchBX" name="search_product" type="text" placeholder="Search here..."
                    value="{{ (isset($search) ? $search : '' ) }}"
                >
                <span class="search_ICON">
                    <i class="far fa-search"></i>
                </span>
                <select class="header-select-box" name="search_type">
                    <option value>Select Type</option>
                    <option value="1" @if(isset($type) && $type == 1) selected @endif>Category Search</option>
                    <option value="2" @if(isset($type) && $type == 2) selected @endif>Product Search</option>
                </select>

            </div>
            <input id="searchBX" type="submit">
        </form>
    </div>
</div>
<div class="header-area shop-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 menu-col">
                <div class="left-menu">
                    <style>
                        .dropdown-menu .show {
                            width: 200px !important;
                        }

                    </style>
                    <nav>
                        <ul>
                            <li><a href="{{ route('home') }}" class="@if(\Request::is('/')) active-menu @endif">home</a></li>
                            <li><a href="{{ route('shop') }}" class="@if(\Request::is('Shop')) active-menu @endif"
                                   >shop
{{--                                        <div class="dropdown-menu dropdown-menu-right-shop-menu">--}}
{{--                                            <a class="dropdown-item" href="{{ route('shop') }}">Products</a>--}}
{{--                                            <a class="dropdown-item" href="#">Bundle Products</a>--}}
{{--                                        </div>--}}
                                </a>
                            </li>
                            {{--<li><a href="{{ route('category') }}" class="@if(\Request::is('Categories')) active-menu @endif">categories</a></li>--}}
                            <li><a href="{{ route('categories') }}" class="@if(\Request::is('Categories')) active-menu @endif">categories</a></li>
                            <li><a href="{{ route('gallery') }}" class="@if(\Request::is('Gallery')) active-menu @endif">gallery</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 text-center logo-col">
                <div class="logo-area">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('front-end/assets/img/mainLogo.svg')}}" alt="">
                        <img class="text-logo" src="{{ asset('front-end/assets/img/bottomLogo.svg')}}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 text-right">
                <div class="right-menu">
                    <nav>
                        <ul>
                            <div class="MENU">
                                <li><a href="{{ route('faqs') }}" class="@if(\Request::is('FAQs')) active-menu @endif">FAQs</a></li>
                                <li><a href="{{ route('about') }}" class="@if(\Request::is('About')) active-menu @endif">ABOUT</a></li>
                                <li><a href="{{ route('contact-us') }}"  class="@if(\Request::is('Contact')) active-menu @endif">CONTACT</a></li>
                            </div>
                            <li><a href="#" class="searchBTN"><img src="{{ asset('front-end/assets/img/search.svg')}}" alt=""> </a></li>
                            <li>

                                <a href="" class="shop-icon" id="shopCart-Icon"><img src="{{ asset('front-end/assets/img/Shop.svg')}}" alt="">
                                    <span class="custom-badge-cart-primary">0</span>
                                </a>

                            </li>
                            <li>
{{--                                dropdown-toggle id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"--}}
                                <a href="#" dropdown-toggle id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                    <img src="{{ asset('front-end/assets/img/user.svg')}}" alt="">
                                    <div class="dropdown-menu dropdown-menu-right" style="right: 120px;left: 0;">
                                        @if(!Auth::user())
                                            <a class="dropdown-item" href="{{route('login')}}#Section">Login</a>
                                            <a class="dropdown-item" href="{{route('register')}}">Signup</a>
                                        @else
                                            <a class="dropdown-item" href="#_" disable="disabled">
                                                {{ ucfirst(Auth::user()->name) }}
                                            </a>

                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="{{route('dashboard')}}">Dashboard</a>
                                            <a class="dropdown-item" href="{{route('wishlist.list')}}">Favorites</a>
                                            <a class="dropdown-item" href="{{route('login')}}"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                           </a>
                                            <form id="logout-form" action="{{ route('logout') }}#Section" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        @endif
                                    </div>
                                </a>
                            </li>
                            <li class="humbargar-manuBAR">
                                <span></span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>



{{--<div class="header-area">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-lg-5 col-md-5 menu-col">--}}
{{--                <div class="left-menu">--}}
{{--                    <nav>--}}
{{--                        <ul>--}}
{{--                            <li><a href="{{ route('home') }}" class="active-menu">home</a></li>--}}
{{--                            <li><a href="{{ route('shop') }}">shop</a></li>--}}
{{--                            <li><a href="{{ route('category') }}">categories</a></li>--}}
{{--                            <li><a href="{{ route('gallery') }}">gallery</a></li>--}}
{{--                        </ul>--}}
{{--                    </nav>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-2 col-md-6 text-center logo-col">--}}
{{--                <div class="logo-area">--}}
{{--                    <a href="{{ route('home') }}">--}}
{{--                        <img src="{{ asset('front-end/assets/img/mainLogo.svg')}} " alt="">--}}
{{--                        <img class="text-logo" src="{{ asset('front-end/assets/img/bottomLogo.svg')}}" alt="">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-5 col-md-6 text-right">--}}
{{--                <div class="right-menu">--}}
{{--                    <nav>--}}
{{--                        <ul>--}}
{{--                            <div class="MENU">--}}
{{--                                <li><a href="{{ route('faqs') }}">FAQs</a></li>--}}
{{--                                <li><a href="{{ route('about') }}">ABOUT</a></li>--}}
{{--                                <li><a href="{{ route('contact') }}">CONTACT</a></li>--}}
{{--                            </div>--}}
{{--                            <li><a href="#"><img src="{{ asset('front-end/assets/img/search.svg')}}" alt=""></a></li>--}}
{{--                            <li><a href="#" class="shop-icon"><img src="{{ asset('front-end/assets/img/Shop.svg')}}" alt=""></a></li>--}}
{{--                            <li><a href="#"><img src="{{ asset('front-end/assets/img/user.svg')}}" alt=""></a></li>--}}
{{--                            <li><a href="#" dropdown-toggle id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('front-end/assets/img/user.svg')}}" alt="">--}}
{{--                                    <div class="dropdown-menu dropdown-menu-right">--}}
{{--                                        <a class="dropdown-item" href="#">Shop</a>--}}
{{--                                        <a class="dropdown-item" href="#">Shop action</a>--}}
{{--                                        <a class="dropdown-item" href="#">Shop</a>--}}
{{--                                    </div>--}}
{{--                                </a></li>--}}
{{--                            <li class="humbargar-manuBAR">--}}
{{--                                <span></span>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </nav>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}


<!-- Modal For Empty Cart -->
<div class="modal fade" id="emptyCart-modal" tabindex="-1" role="dialog" aria-labelledby="emptyCart-modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="emptyCart-modalLabel">Alert! - Add to Cart</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Cart is empty. So add item into cart!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
