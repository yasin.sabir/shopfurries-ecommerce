<!-- JS here -->
<script src="{{ asset('front-end/assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/popper.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/isotope.pkgd.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/one-page-nav-min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/slick.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/jquery.meanmenu.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/ajax-form.js')}}"></script>
<script src="{{ asset('front-end/assets/js/wow.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/gallery.js')}}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="{{ asset('front-end/assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/1351.js')}}"></script>
<script src="{{ asset('front-end/assets/js/gmap3.min.js')}}"></script>
{{--<script src="https://swiperjs.com/package/js/swiper.min.js"></script>--}}
<script src="{{ asset('front-end/assets/js/swiper.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/jquery.nice-number.js')}}"></script>
<script src="{{ asset('front-end/assets/js/jquery.nice-select.min.js')}}"></script>
<script src="{{ asset('front-end/assets/js/plugins.js')}}"></script>
<script src="{{ asset('front-end/assets/js/main.js')}}"></script>
<script src="{{asset('theme-assets/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{asset('other-assets/lightbox/js/lightbox.js')}}"></script>
<script src="{{asset('other-assets/jquery-number-master/jquery.number.js')}}"></script>
<script src="{{asset('other-assets/jquery.lazy-master/jquery.lazy.js')}}"></script>
<script src="{{asset('other-assets/jquery.lazy-master/jquery.lazy.plugins.js')}}"></script>

<script>

    $(function () {

        $(".header-select-box").niceSelect("destroy");

        $('[data-toggle="tooltip"]').tooltip();

        if(localStorage.getItem("count") !=null  ) {
            $('.custom-badge-cart-primary').html(localStorage.getItem('count'));
            localStorage.removeItem('coupon_discount');
            localStorage.removeItem('type');
        }else{
            $('.custom-badge-cart-primary').html(0);
        }

    });

    if(localStorage.getItem("count") !=null  ) {
        var count = localStorage.getItem('count');
        $('.custom-badge-cart-primary').html(count);
        localStorage.removeItem('coupon_discount');
        localStorage.removeItem('type');
    }else{
        $('.custom-badge-cart-primary').html(0);

    }

    $('.container-gallery').gallery({
        height: 650,
        items: 6,
        480: {
            items: 2,
            height: 400,
            thmbHeight: 100
        },
        768: {

            items: 3,
            height: 500,
            thmbHeight: 120
        },
        600: {

            items: 4
        },
        992: {

            items: 5,
            height: 350
        }

    });



</script>
