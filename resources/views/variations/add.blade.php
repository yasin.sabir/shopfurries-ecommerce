@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')

    <?php
    use Illuminate\Support\Facades\Input;
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1> Variations</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Product</a></li>
                            <li class="breadcrumb-item active">Variations</li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Add Section
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-3 pl-5">
                                <label for="SelectProduct">Select Product:</label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="product_id_variation" class="form-control product_id_variation" id="product_id_variation">
                                    <option value="">Select Product</option>
                                    @forelse($products as $key => $val)
                                            <option value="{{$val->id}}" {{ (Input::old("product_id_variation") == $key ? "selected":"") }}>{{ucfirst($val->title)}}</option>
                                        @empty

                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-left pl-5">
                                <input type="button" value="Add Attribute" id="get_product_btn" class="btn btn-primary btn-sm" />
                            </div>
                        </div>


                        <form action="{{route('product.variations.create-attr')}}" method="post" enctype="multipart/form-data" class="mt-5" id="attr_form">
                            <hr>
                            @csrf

                            <div class="row">
                                <div class="col-md-3 pl-5">
                                    <label for="SelectProduct">Name:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="attr_name" id="attr_name" class="form-control @error('attr_name') is-invalid @enderror">
                                        @error('attr_name')
                                        <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <input type="hidden" name="product_id" id="product_id" value="" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 pl-5">
                                    <label for="SelectProduct">Values:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text"
                                               name="attr_value"
                                               id="attr_value"
                                               class="form-control @error('attr_value') is-invalid @enderror"
                                               placeholder="Enter some text, or some attributes by '|' separating values."
                                        />
                                        @error('attr_value')
                                            <span class="invalid-feedback d-block" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 pl-5">
                                    <input type="submit" value="Save Attributes" class="btn btn-primary btn-sm">
                                </div>
                            </div>
                        </form>



                        @if(!empty($attributes))
                            <div class="row">
                                <div class="col-md-3 pl-5">
                                </div>
                                <div class="col-md-6">

                                    @forelse($attributes as $key => $val)

                                        <div id="accordion">
                                            <div class="card">
                                                <div class="card-header border-bottom-0">
                                                    <h4 class="card-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed" aria-expanded="false">
                                                            {{ucfirst($val->attribute)}}
                                                        </a>
                                                    </h4>

                                                    <div class="card-tools">
                                                       <span class="attr_remove text-red">Remove</span>
                                                    </div>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse in collapse" style="border-top: 1px solid rgba(0,0,0,.125);">
                                                    <div class="card-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                                                        3
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @empty

                                    @endforelse

                                </div>
                            </div>

                        @endif


                    </div>

                </div>

            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">
        //=======================================================================================================

        $("#attr_form").hide();

        $("#get_product_btn").click(function(){

            var selectedproduct = $( "#product_id_variation" ).val();

            if(selectedproduct !== ""){
                $("#attr_form").show();
                $("#product_id").val(selectedproduct);
            }else{
                toastr.error('Select product first!!', 'Alert!');
            }



        });



        $("select.product_id_variation").change(function(){
            var selected = $(this).children("option:selected").val();
            $("#product_id").val(selected);
            if(selected === ""){
                $("#attr_form").hide();
            }
        });


        //=======================================================================================================


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#thumbnail-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function () {
            readURL(this);
        });

        //=======================================================================================================

        $('#textarea2').summernote({
            height: 200,
            toolbar: false,
        });


        //=======================================================================================================


        //=======================================================================================================

    </script>

@endsection
