@extends('layouts.backend.app')

@section('page-css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Media")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Settings")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('setting.media-config')}}">{{__("routes.Media")}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">

                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Configuration")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="question_label">{{__("routes.Product Images Sizes")}}:</label>
                                        <small class="d-block">
                                            <cite title="Source Title" for="customSwitch1">
                                                {{__("routes.By default image size is 600x600")}}
                                            </cite>
                                        </small>
                                    </div>
                                    <div class="col-md-6 my-auto">
                                        <input type="number" value="{{isset($site_settings["product_image_size"]) ? $site_settings["product_image_size"] : "" }}" class="form-control" name="image_size" id="image_size">
                                        <small class="d-block">
                                            <cite title="Source Title" for="customSwitch1">
                                                {{__("routes.Thumbnail - 300x300 , Medium - 600x600 , Full - 1024x1024")}}
                                            </cite>
                                        </small>

                                        <div class="form-group mt-3">
                                            <button class="btn btn-primary btn-sm" id="imageSize_btn">{{__("routes.Update")}}</button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </section>


    </div>

@endsection


@section('page-script')

    <script>

//        $.ajaxSetup({
//            headers: {
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            }
//        });

        //=======================================================================================================

        $("#imageSize_btn").click(function () {

            var image_size = $("#image_size").val();

            $.ajax({
                url: "{{route('setting.media-config-update')}}",
                type : 'post',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : {image_size:image_size},
                success: function(response){
                    toastr.success(response[0].msg,'General Config Alert');
                    location.reload();
                },
                error: function(data){
                    toastr.error(data.responseJSON[0].msg,'General Config Alert');
                }

            });

        });


    </script>

@endsection
