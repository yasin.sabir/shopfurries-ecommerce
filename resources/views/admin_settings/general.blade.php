@extends('layouts.backend.app')

@section('page-css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('section')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">

                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.General Configuration")}}</h1>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Settings")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('setting.general-config')}}">{{__("routes.General Configuration")}}</a></li>
                        </ol>
                    </div>
                </div>


            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.Add Section")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="question_label">{{__("routes.Maintenance Mode")}}:</label>
                                    </div>
                                    <div class="col-md-6 my-auto">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch1" @if(site_config('site_maintenance_status') == "on" ) checked @else uncheckec @endif >
                                            <label class="custom-control-label" for="customSwitch1"></label>
                                            <small>
                                                <cite title="Source Title" for="customSwitch1">
                                                    {{__("routes.Website will go on maintenance mode")}}
                                                </cite>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="question_label">{{__("routes.Default Sales Tax")}}:</label>
                                    </div>
                                    <div class="col-md-6 my-auto">
                                        <input type="number" value="{{isset($site_settings["default_sales_tax"]) ? $site_settings["default_sales_tax"] : null }}" class="form-control" name="default_sales_tax" id="default_sales_tax">
                                        <small>
                                            <cite title="Source Title" for="customSwitch1">
                                                {{__("routes.Default sales tax apply on products when product contain more than one category.")}}
                                            </cite>
                                        </small>
                                        <div class="form-group">
                                            <button class="btn btn-primary btn-sm" id="default_sales_tax_btn">{{__("routes.Update sales tax")}}</button>
                                        </div>

                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="question_label">{{__("routes.Default Product Stock")}}:</label>
                                    </div>
                                    <div class="col-md-6 my-auto">
                                        <input type="number" value="{{isset($site_settings["default_product_stock"]) ? $site_settings["default_product_stock"] : null }}" class="form-control" name="default_product_stock" id="default_product_stock">
                                        <small>
                                            <cite title="Source Title" for="customSwitch1">
                                                {{__("routes.If product stock not define so the default value will be add.")}}
                                            </cite>
                                        </small>
                                        <div class="form-group">
                                            <button class="btn btn-primary btn-sm" id="default_product_stock_btn">{{__("routes.Update Product Stock")}}</button>
                                        </div>

                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="question_label">{{__("routes.Trending Products")}}:</label>
                                        <small class="d-block">
                                            <cite title="Source Title" for="customSwitch1">
                                                {{__("routes.Show products those reviews upto...")}}
                                            </cite>
                                        </small>
                                    </div>
                                    <div class="col-md-6 my-auto">
                                        <input type="number" value="{{isset($site_settings["upto_review"]) ? $site_settings["upto_review"] : "" }}" class="form-control" name="upto_review" id="upto_review">
                                        <div class="form-group mt-3">
                                            <button class="btn btn-primary btn-sm" id="upto_review_btn">{{__("routes.Update")}}</button>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="question_label">{{__("routes.Abandoned Cart")}}:</label>
                                        <small class="d-block">
                                            <cite title="Source Title" for="customSwitch1">
                                                {{__("routes.Show abandoned cart product days...")}}
                                            </cite>
                                        </small>
                                    </div>
                                    <div class="col-md-6 my-auto">
                                        <input type="number" value="{{isset($site_settings["abandoned_cart_days"]) ? $site_settings["abandoned_cart_days"] : "" }}" class="form-control" name="abandoned_cart_days" id="abandoned_cart_days">
                                        <div class="form-group mt-3">
                                            <button class="btn btn-primary btn-sm" id="abandoned_cart_days_btn">{{__("routes.Update")}}</button>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>



            </div>

        </section>


    </div>

@endsection


@section('page-script')

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //=======================================================================================================

        var status = "";

        // input[type="checkbox"]
        $('#customSwitch1').click(function(){
            var customSwitch1 = $(this);
            if($(this).prop("checked") === true){
                console.log("checked");
                status = "on";

                $.ajax({
                    url:"{{route('setting.general-config-maintenance-mode')}}",
                    type: 'post',
                    data: {status : status},
                    success:function(response){
                        //console.log(response+" - Mode is on");
                        toastr.error('Maintenance mode is <strong>On</strong> !', 'General Config Alert');
                    }
                });
            }
            else if($(this).prop("checked") === false){
                console.log("Unchecked");
                status = "off";
                $.ajax({
                    url:"{{route('setting.general-config-maintenance-mode')}}",
                    type: 'post',
                    data: {status : status},
                    success:function(response){
                        //console.log(response+" - Mode is off");
                        toastr.success('Maintenance mode is <strong>Off</strong> !', 'General Config Alert');
                    }
                });
            }
        });

        //=======================================================================================================

        $("#default_sales_tax_btn").click(function () {

            var default_sales_tax = $("#default_sales_tax").val();

            $.ajax({
                url: "{{route('setting.default_sales_tax')}}",
                type : 'post',
                data : {default_sales_tax:default_sales_tax},
                success: function(response){
                    toastr.success(response[0].msg,'General Config Alert');
                    location.reload();
                },
                error: function(data){
                    toastr.error(data.responseJSON[0].msg,'General Config Alert');
                }

            });

        });


        //=======================================================================================================

        $("#default_product_stock_btn").click(function () {

            var default_product_stock = $("#default_product_stock").val();

            $.ajax({
                url: "{{route('setting.default_product_stock')}}",
                type : 'post',
                data : {default_product_stock:default_product_stock},
                success: function(response){
                    toastr.success(response[0].msg,'General Config Alert');
                    location.reload();
                },
                error: function(data){
                    toastr.error(data.responseJSON[0].msg,'General Config Alert');
                }

            });

        });


        //=======================================================================================================

        $("#upto_review_btn").click(function () {

            var upto_review = $("#upto_review").val();
            $.ajax({
                url: "{{route('setting.upto_reviews')}}",
                type : 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : {upto_review:upto_review},
                success: function(response){
                    toastr.success(response[0].msg,'General Config Alert');
                    location.reload();
                },
                error: function(data){
                    toastr.error(data.responseJSON[0].msg,'General Config Alert');
                }

            });

        });


        //=======================================================================================================

        $("#abandoned_cart_days_btn").click(function () {

            var abandoned_cart_days = $("#abandoned_cart_days").val();

            $.ajax({
                url: "{{route('setting.abandoned_cart_days')}}",
                type : 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : {abandoned_cart_days:abandoned_cart_days},
                success: function(response){
                    toastr.success(response[0].msg,'General Config Alert');
                    location.reload();
                },
                error: function(data){
                    toastr.error(data.responseJSON[0].msg,'General Config Alert');
                }

            });

        });



    </script>

@endsection
