<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
    <title>Shop Furries</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Flags icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">


    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css')}}">

    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('theme-assets/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/summernote/summernote-bs4.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">

    <!-- jsGrid -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/jsgrid/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/jsgrid/jsgrid-theme.min.css')}}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/toastr/toastr.min.css')}}">
    <!-- fullCalendar -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fullcalendar/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fullcalendar-daygrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fullcalendar-timegrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/fullcalendar-bootstrap/main.min.css')}}">

    <!-- Ekko Lightbox -->
    <link rel="stylesheet" href="{{asset('theme-assets/plugins/ekko-lightbox/ekko-lightbox.css')}}">

    <!-- Custom StyleSheet -->
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>

    <meta name="csrf-token" content="{{ csrf_token() }}" />

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-12 mt-5">

            <div class="error-page">
                <h2 class="headline text-warning">
                    <img src="{{asset('front-end/assets/img/ShopFurries-LOGO.png')}}" width="200" alt="logo" srcset="">
                </h2>

                <div class="error-content pt-4">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> User Deactivate Mode</h3>
                    <p>User is currently deactivate.</p>
                    <p>For Further Detail Contact Administrator</p>

                    <form id="logout-form" action="{{ route('logout')}}#Section" method="POST" style="display: none;">
                        @csrf
                    </form>
                    <p style="display: flex;">Meanwhile, You may &nbsp; &nbsp;  <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();"
                                               class="btn btn-default btn-flat float-right">{{ __('Logout') }}</a></p>
                </div>
                <!-- /.error-content -->
            </div>
            <!-- /.error-page -->
        </div>
    </div>
</div>




<!-- jQuery -->
<script src="{{asset('theme-assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('theme-assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('theme-assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('theme-assets/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('theme-assets/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('theme-assets/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('theme-assets/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('theme-assets/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('theme-assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('theme-assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('theme-assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('theme-assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('theme-assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('theme-assets/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('theme-assets/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('theme-assets/dist/js/demo.js')}}"></script>

<!-- FLOT CHARTS -->
<script src="{{asset('theme-assets/plugins/flot/jquery.flot.js')}}"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="{{asset('theme-assets/plugins/flot-old/jquery.flot.resize.min.js')}}"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="{{asset('theme-assets/plugins/flot-old/jquery.flot.pie.min.js')}}"></script>

<!-- Select2 -->
<script src="{{asset('theme-assets/plugins/select2/js/select2.full.min.js')}}"></script>

<!-- Bootstrap4 Duallistbox -->
<script src="{{asset('theme-assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>

<!-- InputMask -->
<script src="{{asset('theme-assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('theme-assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>

<!-- bootstrap color picker -->
<script src="{{asset('theme-assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>

<!-- Bootstrap Switch -->
<script src="{{asset('theme-assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>

<!-- bs-custom-file-input -->
<script src="{{asset('theme-assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>

<!-- jquery-validation -->
<script src="{{asset('theme-assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('theme-assets/plugins/jquery-validation/additional-methods.min.js')}}"></script>

<!-- DataTables -->
<script src="{{asset('theme-assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('theme-assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>


<!-- jsGrid -->
<script src="{{asset('theme-assets/plugins/jsgrid/demos/db.js')}}"></script>
<script src="{{asset('theme-assets/plugins/jsgrid/jsgrid.min.js')}}"></script>

<!-- SweetAlert2 -->
<script src="{{asset('theme-assets/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- Toastr -->
<script src="{{asset('theme-assets/plugins/toastr/toastr.min.js')}}"></script>

<!-- Ion Slider -->
<script src="{{asset('theme-assets/plugins/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>

<!-- Bootstrap slider -->
<script src="{{asset('theme-assets/plugins/bootstrap-slider/bootstrap-slider.min.js')}}"></script>

<!-- fullCalendar 2.2.5 -->
<script src="{{asset('theme-assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('theme-assets/plugins/fullcalendar/main.min.js')}}"></script>
<script src="{{asset('theme-assets/plugins/fullcalendar-daygrid/main.min.js')}}"></script>
<script src="{{asset('theme-assets/plugins/fullcalendar-timegrid/main.min.js')}}"></script>
<script src="{{asset('theme-assets/plugins/fullcalendar-interaction/main.min.js')}}"></script>
<script src="{{asset('theme-assets/plugins/fullcalendar-bootstrap/main.min.js')}}"></script>


<!-- Ekko Lightbox -->
<script src="{{asset('theme-assets/plugins/ekko-lightbox/ekko-lightbox.min.js')}}"></script>
<!-- Filterizr-->
<script src="{{asset('theme-assets/plugins/filterizr/jquery.filterizr.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/themes/fas/theme.min.js"></script>
<script src="{{ asset('js/script.js')}}"></script>


</body>
</html>
