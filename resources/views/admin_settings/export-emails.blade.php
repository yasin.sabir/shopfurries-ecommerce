@extends('layouts.backend.app')

@section('page-css')

@endsection

@section('section')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="" style="display: inline-flex;">
                            <h1>{{__("routes.Export Customer Emails")}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">{{__("routes.Export")}}</li>
                            <li class="breadcrumb-item active"><a href="{{route('setting.display_export_emails')}}">{{__("routes.List")}}</a></li>
                        </ol>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    {{__("routes.List")}}
                                </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <table id="example1" class="table table-bordered table-striped nowrap">
                                    <thead>
                                        <tr>
                                            <th>{{__("routes.Email")}}:</th>
                                            <th>{{__("routes.Account Type")}}:</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($arr as $key => $v)
                                            <tr>
                                                <td>{{$v['email']}}</td>
                                                <td>{{$v['type']}}</td>
                                            </tr>
                                        @empty
                                        @endforelse
                                    </tbody>
                                </table>

                                <div class="col-md-12 text-left mt-5">
                                    <form action="{{route('setting.export_emails')}}" method="post">
                                        <div class="form-group">
                                            @csrf
                                            <input type="hidden" value="{{serialize($arr)}}" name="export_data">
                                            <input type="submit" class="btn btn-success" value="{{__("routes.Export Data")}}">
                                        </div>
                                    </form>
                                </div>

                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection

@section('page-script')

    <script type="text/javascript">

        $('#example1').DataTable({
            "paging": true,
            "responsive": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 7,
            "autoWidth": false,
        });

        //=======================================================================================================

        $("input[name='checkAll']").click(function(){
            $("input[name='faq']").not(this).prop('checked', this.checked);
        });

        //=======================================================================================================

    </script>

@endsection
