<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;


class OrdersExport implements WithColumnWidths , WithStyles , WithMapping , FromCollection , WithHeadings
{
    private $orders;

     public function __construct($data)
     {
        $this->orders = $data;
     }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 20,
            'C' => 35,
            'D' => 35,
            'E' => 70,
            'F' => 20,
            'G' => 80,
            'H' => 20,
            'I' => 20,
            'J' => 20,
            'K' => 20,
            'L' => 30,
            'M' => 20,
            'N' => 30,
            'O' => 30,
            'P' => 20,
            'Q' => 20,
            'R' => 20,
            'S' => 20,
            'T' => 20,
            'U' => 20,
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'Status',
            'Name',
            'Email',
            'Items',
            'Total Items',
            'Address',
            'City',
            'State',
            'Country',
            'Postal Code',
            'Phone',
            'Payment Type',
            'Discount Type',
            'Discount',
            'Total Products Weight',
            'Cost Per Country Weight',
            'Weight Cost',
            'Shipping Cost',
            'Subtotal of Products',
            'Total Cost',
        ];
    }

    public function collection()
    {
        return $this->orders;
    }

    public function map($orders_): array
    {
        $orderDetails = (array) unserialize( $orders_->order_details );
        $arr [] = [
            'ID'                      => $orders_->id,
            'Status'                  => $orders_->status,
            'Name'                    => $orderDetails['name'] ?? '',
            'Email'                   => $orderDetails['email'] ?? '',
            'Items'                   => $orders_->items,
            'Total Items'             => $orders_->total_items,
            'Address'                 => $orderDetails['address'] ?? '',
            'City'                    => $orderDetails['city'] ?? '',
            'State'                   => $orderDetails['state'] ?? '',
            'Country'                 => $orderDetails['d-country'] ?? '',
            'Postal Code'             => $orderDetails['pcode'] ?? '',
            'Phone'                   => $orderDetails['mobile'] ?? '',
            'Payment Type'            => $orderDetails['payment'] ?? '',
            'Discount Type'           => $orders_->discount_type ?? '',
            'Discount'                => $orders_->discount ?? '',
            'Total Products Weight'   => $orders_->total_products_weight ?? '',
            'Cost Per Country Weight' => $orders_->cost_per_country_weight ?? '',
            'Weight Cost'             => $orders_->total_shipping ?? '',
            'Shipping Cost'           => $orders_->total_weight ?? '',
            'Subtotal of Products'    => $orders_->subtotal_of_products ?? '',
            'Total Cost'              => number_format( $orders_->subtotal, 2 ) ?? '',
        ];
        return $arr;


//        $arr [] = [
//            'ID'                      => $orders_->id,
//            'Status'                  => $orders_->status,
//            'Name'                    => $orderDetails['name'] ?? '',
//            'Email'                   => $orderDetails['email'] ?? '',
//            'Items'                   => $orders_->items,
//            'Total Items'             => $orders_->total_items,
//            'Address'                 => $orderDetails['address'] ?? '',
//            'City'                    => $orderDetails['city'] ?? '',
//            'State'                   => $orderDetails['state'] ?? '',
//            'Country'                 => $orderDetails['d-country'] ?? '',
//            'Postal Code'             => $orderDetails['pcode'] ?? '',
//            'Phone'                   => $orderDetails['mobile'] ?? '',
//            'Payment Type'            => $orderDetails['payment'] ?? '',
//            'Total Products Weight'   => $orderDetails['total_product_weight'] ?? '',
//            'Cost Per Country Weight' => $orderDetails['cost_per_country_weight'] ?? '',
//            'Weight Cost'             => $orderDetails['shipping_cost'] ?? '',
//            'Shipping Cost'           => $orderDetails['weight_cost'] ?? '',
//            'Subtotal of Products'    => $orderDetails['subtotal_of_products'] ?? '',
//            'Total Cost'              => number_format( $orders_->subtotal, 2 ) ?? '',
//        ];

    }

}
