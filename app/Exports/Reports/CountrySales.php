<?php


namespace App\Exports\Reports;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;


class CountrySales implements FromArray ,WithHeadings , WithColumnWidths , WithStyles
{

    /**
     * @return \Illuminate\Support\Collection
     */

    protected $order;

    public function __construct(array $orders)
    {
        $this->order = $orders;
    }

    public function array(): array
    {
        return $this->order;
    }


    public function headings():array{
        return [
            [   'ID',
                'Products',
                'Quantity',
                'Price',
                'Country',
                'Created At'
            ]
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 10,
            'B' => 30,
            'C' => 20,
            'D' => 20,
            'E' => 20,
            'F' => 20,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1  => ['font' => ['bold' => true]], // For Rows..
            'C'  => ['font' => ['bold' => true]], // For Cols..
            'D'  => ['font' => ['bold' => true]],
        ];
    }

}
