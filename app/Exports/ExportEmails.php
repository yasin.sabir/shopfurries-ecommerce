<?php


namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExportEmails implements FromArray ,WithHeadings , WithColumnWidths , WithStyles
{

    /**
     * @return array
     */
    protected $data;

    public function __construct(array $data){
        $this->data = $data;
    }


    public function array(): array
    {
       return $this->data;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 30,
            'B' => 10,
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            [   'Emails',
                'Type',
            ]
        ];

    }

    public function styles(Worksheet $sheet)
    {
        return [
            1  => ['font' => ['bold' => true]], // For Rows..
        ];
    }
}
