<?php

namespace App\Exports;

use App\Http\Controllers\ProductController;
use App\Product;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;


class ProductsExport implements WithColumnWidths , WithStyles , WithMapping , FromCollection , WithHeadings
{
	private $product;

	public function __construct($data)
	{
		$this->product = $data;
	}

	public function styles(Worksheet $sheet)
	{
		return [
			// Style the first row as bold text.
			1    => ['font' => ['bold' => true]],
		];
	}

	public function columnWidths(): array
	{
		return [
			'A' => 20,
			'B' => 20,
			'C' => 75,
			'D' => 35,
			'E' => 45,
			'F' => 20,
			'G' => 20,
			'H' => 20,
			'I' => 20,
			'J' => 20,
			'K' => 20,
			'L' => 30,
			'M' => 20,
			'N' => 30,
			'O' => 30,
			'P' => 20,
			'Q' => 20,
			'R' => 20,
			'S' => 20,
			'T' => 20,
			'U' => 20,
		];
	}

	public function headings(): array
	{
		return [
			'ID',
			'User ID',
			'Title',
			'Slug',
			'Categories',
			'Description',
			'Sku',
			'Image',
			'Video',
			'Price',
			'Stock',
			'Status',
			'Weight',
			'Shipping',
			'Material',
			'Sizes',
			'Product Type',
			'Created At',
		];
	}

	public function collection()
	{
		return $this->product;
	}

	public function map($products): array
	{
		$meta                = ProductController::getProductMetas($products->id);
		$arr [] = [
			'ID'             => $products->id,
			'User ID'        => $products->user_id ?? '',
			'Title'          => $products->title ?? '',
			'Slug'           => $products->slug ?? '',
			'Categories'     => implode(' , ', ProductController::getProductCategoriesById($products->id) ) ?? '',
			'Description'    => strip_tags($products->description) ?? '',
			'Sku'            => $products->sku ?? '',
			'Image'          => $products->image ?? '',
			'Video'          => $products->video ?? '',
			'Price'          => $products->price ?? '',
			'Stock'          => $products->stock ?? '',
			'Status'         => $products->status ?? '',
			'Weight'         => $meta['weight'] ?? '',
			'Shipping'       => $meta['estimate_shipping'] ?? '',
			'Material'       => '',
			'Sizes'          => '',
			'Product Type'   => $products->product_type ?? '',
			'Created At'     => $products->created_at ?? '',
		];
		return $arr;

	}

}
