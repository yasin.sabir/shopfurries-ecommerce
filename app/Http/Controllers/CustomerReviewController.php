<?php

namespace App\Http\Controllers;

use App\CustomerReviews;
use App\cr;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CustomerReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cr = CustomerReviews::orderBy('id','DESC')->get();
        return view('customer_reviews.all', ['cr' => $cr]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer_reviews.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $custom_msg = [
            'cr_name.required'           => "Name is required!",
            'cr_rating.numeric'          => "Rating must be a number!",
        ];

        $this->validate($request,[
            'cr_name'           => "required",
            'cr_rating'         => "numeric|min:1|max:5",
        ],$custom_msg);

        $cr = new CustomerReviews();
        $cr->name            = $request->cr_name;
        $cr->status          = "on";
        $cr->rating          = $request->cr_rating;
        $cr->detail          = (!empty($request->cr_detail) ? $request->cr_detail : null );
        $cr->save();

        $default_path  = "images/placeholders/Profile_avatar_placeholder_lg.png";
        $cr_img     = $request->file('cr_img') ? $request->file('cr_img')->store('upload/customer-reviews/customer-review_'.$cr->id, 'public') : $default_path;

        CustomerReviews::where(['id' => $cr->id])->update(['image' => $cr_img]);

        if($request->hasFile('cr_img')){
            $sizes = ['150','350'];
            $type  = "customer-reviews";
            $slug  = "customer-review_";
            $file_key = 'cr_img';
            $this->imageResizing( $cr->id, $type, $slug , $file_key , $sizes , $request);
        }

        $noti = array("message" => "Customer Review created successfully!", "alert-type" => "success");
        return redirect()->route('customer-reviews.list')->with($noti);
    }


    function imageResizing($id, $type , $slug , $file_key , $sizes , $request){

        if(!empty($sizes) && count($sizes) > 0){

            foreach ($sizes as $key => $val){
                $folderName   = $val."x".$val;
                $eventFolder  = $slug."_".$id;
                $directory    = "public/upload/".$type."/".$eventFolder."/".$folderName."/";
                Storage::makeDirectory($directory);
            }

            if($request->hasFile($file_key)){

                $image     = $request->file($file_key);
                $filename  = time() . '.' . $image->getClientOriginalExtension();

                foreach ($sizes as $key => $val){
                    $folderName     = $val."x"."$val";
                    $eventFolder    = $slug."_".$id;
                    $path           = storage_path('app/public/upload/'.$type.'/'.$eventFolder.'/'.$folderName.'/').$filename;
                    $img            = Image::make($image->getRealPath());
                    $img->resize($val, $val, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $img->save($path);
                }

            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {

            $cr = CustomerReviews::find(decrypt( $id ));
            if ( ! empty( $cr ) ) {
                return view('customer_reviews.edit',['cr' => $cr]);
            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }

        return view( 'errors.404' );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $custom_msg = [
            'cr_name.required'           => "Name is required!",
            'cr_rating.required'         => "Rating is required!",
        ];

        $this->validate($request,[
            'cr_name'           => "required",
            'cr_rating'         => "required|min:1|max:5",
        ],$custom_msg);

        $prev_cr_image = $request->prev_cr_image;
        $cr_img     = $request->file('cr_img') ? $request->file('cr_img')->store('upload/customer-reviews/customer-review_'.$id, 'public') : $prev_cr_image;

        CustomerReviews::where(['id' => $id])->update([
            'name'    => $request->cr_name,
            'rating'  => $request->cr_rating,
            'image'   => $cr_img,
            'detail' => (!empty($request->cr_detail) ? $request->cr_detail : null )
        ]);

        if($request->hasFile('cr_img')){
            $sizes = ['150','350'];
            $type  = "customer-reviews";
            $slug  = "customer-review_";
            $file_key = 'cr_img';
            $this->imageResizing( $id, $type, $slug , $file_key , $sizes , $request);
        }

        $noti = array("message" => "Customer Review updated successfully!", "alert-type" => "success");
        return redirect()->route('customer-reviews.list')->with($noti);
    }


    public function updateStatus(Request $request , $id){

        if( isset($request->status) && $request->status == "on" ){

            CustomerReviews::where(['id' => $id])->update(['status' => $request->status]);
            $msg = "Status is on";
            return response()->json(array(['msg'=> $msg , 'val' => 'checked']), 200);

        }else if(isset($request->status) && $request->status == "off"){

            CustomerReviews::where(['id' => $id])->update(['status' => $request->status]);
            $msg = "Status is off";
            return response()->json(array(['msg'=> $msg , 'val' => 'unchecked']), 200);

        }else{

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cr = CustomerReviews::findOrFail($id);
        $cr->delete();

        $noti = array("message" => "Customer Review deleted successfully!", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }

    public function destroyAll(Request $request){

        $ids = json_decode($request->delete_ids);
        customVarDump_die($ids);

        foreach ($ids as $key => $id){
            $cr = CustomerReviews::findOrFail($id);
            $cr->delete();
        }

        $noti = array("message" => "Selected Customer Reviews deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }


}
