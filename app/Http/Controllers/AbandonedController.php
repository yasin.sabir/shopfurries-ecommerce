<?php

namespace App\Http\Controllers;

use App\Mail\Abandoned\Abandoned_Cart_Email;
use App\order_log;
use App\SiteSetting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class AbandonedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

	    try{
		    $abandoned_orders = DB::table('order_log')->select(DB::raw('count(*) as items , user_id '))->groupBy('user_id')->get();
		    $abandonedCart    = [];
		    $data             = SiteSetting::where( [ 'key' => 'abandoned_cart_days' ] )->first();
		    $abandoned_days   = ( isset($data->value) ) ? $data->value  : 2;

		    foreach ($abandoned_orders as $key => $val){

			    //$log_time       = strtotime($val->created_at);
			    //$current_time   = strtotime(date('Y-m-d g:i a'));
			    //$day            = intval((($current_time - $log_time)/86400)) ;
			    //            if($day > $abandoned_days && $user_exit ){
			    //                $abandonedCart [] = $val;
			    //            }

			    $user_exit = User::find($val->user_id);

			    if( $user_exit ){
				    $abandonedCart [] = $val;
			    }

		    }

		    return view('abandoned-cart.list')->with([ 'abandonedCart' => $abandonedCart ]);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }


    }

    public function readable_timeFormat($time)
    {

	    try{

		    //date_default_timezone_set(get_option('timezone_string'));
		    //$seconds_ago = (time() - strtotime('2014-01-06 15:25:08'));

		    if (!empty($time)) {

			    $seconds_ago = (time() - strtotime($time));

			    if ($seconds_ago >= 31536000) {
				    echo "" . intval($seconds_ago / 31536000) . " years ago";
			    } elseif ($seconds_ago >= 2419200) {
				    echo "" . intval($seconds_ago / 2419200) . " months ago";
			    } elseif ($seconds_ago >= 86400) {
				    echo "" . intval($seconds_ago / 86400) . " days ago";
			    } elseif ($seconds_ago >= 3600) {
				    echo "" . intval($seconds_ago / 3600) . " hours ago";
			    } elseif ($seconds_ago >= 60) {
				    echo "" . intval($seconds_ago / 60) . " minutes ago";
			    } else {
				    echo "less than a minute ago";
			    }

		    } else {
			    echo "No Last Visit";
		    }

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }


    public function sendAbandonedEmail(Request $request){

	    try{

		    $user = User::find($request->user_id);
		    Mail::to($user->email)->send(new Abandoned_Cart_Email( $user));

		    $noti = array("message" => "Email send", "alert-type" => "success");
		    return redirect()->back()->with($noti);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

	    try{

	    	$log = order_log::findOrFail($id);
		    $log->delete();

		    $noti = array("message" => "Log deleted successfully!", "alert-type" => "success");
		    return redirect()->back()->with($noti);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }


    public function destroyAll(Request $request){

	    try{

		    $ids = json_decode($request->delete_ids);

		    foreach ($ids as $key => $id){
			    $log = order_log::findOrFail($id);
			    $log->delete();
		    }

		    $noti = array("message" => "Selected logs deleted successfully", "alert-type" => "success");
		    return redirect()->back()->with($noti);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }
}
