<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Paypal;
use Session;
use App\order;
use App\Guest;
use App\Product;
use App\order_log;
use PayPal\Api\Item;
use PayPal\Api\Payer;
use Stripe\Error\Card;
use PayPal\Api\Amount;
use App\products_meta;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\ItemList;
use App\Mail\OrderStatus;
use http\Client\Curl\User;
use PayPal\Rest\ApiContext;
use PayPal\Api\Transaction;
use Illuminate\Support\Str;
use PayPal\Api\RedirectUrls;
use Illuminate\Http\Request;
use PayPal\Api\ExecutePayment;
use PayPal\Api\ShippingAddress;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Input;
use PayPal\Auth\OAuthTokenCredential;
use Illuminate\Support\Facades\Redirect;
use Cartalyst\Stripe\Laravel\Facades\Stripe;


class CheckoutController extends Controller
{
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        //dd($paypal_conf);
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );

        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function paymentselect(Request $request)
    {
        $custom_message = [
            'name.required'     => 'Name is required!',
            'email.required'    => 'Email is required!',
            'address.required'  => 'Address is required!',
            'pcode.required'    => 'Postal Code is required!',
            'city.required'     => 'City is required!',
            'mobile.required'   => 'Mobile is required!',
        ];

        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required',
            'address'   => 'required',
            'pcode'     => 'required',
            'city'      => 'required',
            'mobile'    => 'required',
        ], $custom_message);

        $checkout_arr = [];
        $checkout_arr = $request->all();
        //dd($checkout_arr);
        $payment_method = $request->payment;
        if($payment_method == 'paypal'){
            return $this->paypal($checkout_arr);
        }
        else if($payment_method == 'stripe'){
            return $this->stripe($checkout_arr);
        }

    }

    public function paypal($arr){
        //dd($arr);

        //$user             = Auth::user();
        $order_log        = order_log::where('user_id',$arr['currentUserID'])->get();

        $payer            = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_detail      = [];
        $totall           = [];
        $abc              = [];
        $subtotal         = $arr['subtotal'];
        $check_out_detail = [];

        foreach( $arr as $key => $value){
          $check_out_detail[$key] = $value;
        }

        foreach($order_log as $data) {
            $totall[]       = (($data->qty) * ($data->price));
            $abc[]          =  $data;
            $item_1         = new Item();
            $item_detail[]  = $item_1->setName($data['product_name'])
            ->setCurrency('USD')
                ->setQuantity($data['qty'])
                ->setPrice($data['price']);
        }
        $total           = array_sum($totall);
        $shippingAddress = new ShippingAddress();
        //        $shippingAddress->setRecipientName("Brian Robinson")
        //            ->setLine1("4th Floor")
        //            ->setLine2("Unit #34")
        //            ->setCity("San Jose")
        //            ->setCountryCode("US")
        //            ->setPostalCode("95131")
        //            ->setPhone("2025550110")
        //            ->setState("CA");

        $item_list      = new ItemList();
        $item_list->setItems(array());
        //$item_list->setShippingAddress($shippingAddress);

        $amount         = new Amount();
        $amount->setCurrency('USD')
                ->setTotal(floor($subtotal));

        $transaction    = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Shop Furries');

        $redirect_urls  = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('checkout.createorder',['arra'=>$check_out_detail]))
        ->setCancelUrl(route('checkout.cancel_paypal_url'));
//        $redirect_urls->setReturnUrl(route('checkout.createorder', $check_out_detail ))/** Specify return URL **/


        $payment        = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            dd($ex);
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::route('checkout.paywithpaypal');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('checkout.paywithpaypal');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
        return Redirect::route('paywithpaypal');
    }

    public function stripe($arr){

       // dd($arr);
        $totall                 = [];
        $product_detail         = [];
        $product_extra_detail   = [];
        $item_detail            = [];
        $subtotal               = $arr['subtotal'];
        $totalItems             = 0;
        $items                  = [];

        //$user                   = Auth::user();
        $order_log              = order_log::where('user_id',$arr['currentUserID'])->get();

        foreach($order_log as $data) {
            $totall[]               = (($data->qty) * ($data->price));
            $item_detail[]          = $data;
            $product_detail[]       = $data->product_meta;
            $product_extra_detail[] = $data->extra_detail;
            $totalItems             = $totalItems + $data->qty;
            $items[]                = $data->product_name;
        }
        //dd($product_extra_detail);

        $total                  = array_sum($totall);
        $product_detail         = serialize($product_detail);
        $product_extra_detail   = serialize($product_extra_detail);
        $order_detail           = $arr;
        $serialize_order_detail =  serialize($order_detail);

        $user                   = \App\User::where(['id' => $arr['currentUserID']])->exists();

        if(!$user){
            $user               = Guest::where(['guestID' => $arr['currentUserID'] ])->first();
        }else{
            $user               = \App\User::find($arr['currentUserID']);
        }

        $stripe                 = Stripe::make('sk_test_5HWhZTehFH5DvCehsZd3oGCb');
        $stripeCustomer         = $user->stripe_customer_id;

        if($stripeCustomer == null) {

            $customer = $stripe->customers()->create([
                'name'    => $arr['name'],
                'email'   => $arr['email'],
                'address' => [
                    'line1'         => $arr['address'],
                    'city'          => $arr['city'],
                    'country'       => $arr['d-country'],
                    'postal_code'   => $arr['pcode'],
                    'state'         => $arr['state']
                ],
                 //'source'  => $arr['stripeToken'],
                'card'    => $arr['stripeToken'],
            ]);

            $charge = $stripe->charges()->create([
                'currency'          => 'USD',
                'amount'            => $subtotal,
                'receipt_email'     => $arr['email'],
                'description'       => 'Shop Furries Product Purchased',
                'customer'          => $customer['id']
            ]);

            if(auth()->guest()){
                $user->name               = $arr['name'];
                $user->email              = $arr['email'];
                $user->stripe_customer_id = $customer['id'];
                $user->place_order_status = 1;
                $user->save();
            }else{
                $user->stripe_customer_id = $customer['id'];
                $user->save();
            }

        }else{
            $charge = $stripe->charges()->create([
                'currency'          => 'USD',
                'amount'            => $subtotal,
                'receipt_email'     =>  $arr['email'],
                'description'       => 'Shop Furries Product Purchased',
                'customer'          => $stripeCustomer
            ]);
        }

        if($charge['status'] == 'succeeded') {
            $order                            = new order();
            $order->user_id                   = $arr['currentUserID'];
            $order->order_id                  = uniqid("orderID-");
            $order->items                     = implode('/',$items);
            $order->total_items               = $totalItems;
            $order->user_email                = $order_detail['email'];
            $order->order_details             = $serialize_order_detail;
            $order->user_meta                 = $user; // issue for guest user
            $order->product_detail            = $product_detail;
            $order->product_extra_detail      = $product_extra_detail;
            $order->subtotal                  = $subtotal;
            $order->subtotal_of_products      = $arr['subtotal_of_products'];
            $order->total_shipping            = $arr['shipping_cost'];
            $order->discount                  = $arr['coupon_discount'];
            $order->discount_type             = $arr['coupon_type'];
            $order->selected_country          = $arr['d-country'];
            $order->cost_per_country_weight   = $arr['cost_per_country_weight'];
            $order->total_weight              = $arr['weight_cost'];
            $order->total_products_weight     = $arr['total_product_weight'];
            $order->tax                       = "";
            $order->status                    = 'Confirm';
            $order->selected_order_part_id    = isset( $arr['orderParts'] ) ? $arr['orderParts'] : null;
            $order->order_part_done           = isset( $arr['orderParts'] ) ? 1 : null;
            $order->order_log                 = $arr['order_log'];
            $order->save();

            foreach($order_log as $data) {
                $prod = product::find($data->product_id);

                if($prod->product_type == "bundle_product"){

                    $meta_value = products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->first();

                    //Reduce stock from inventory
                    if(!empty($prod->stock)){
                        $updated_stock_ = ( $prod->stock - $data->qty );
                        product::where(['id' => $data->product_id])->update(['stock' => $updated_stock_]);
                    }

                    if(!empty($meta_value['product_meta_value'])){
                        $updated_stock = ( $meta_value['product_meta_value'] - $data->qty );
                        products_meta::where(['product_id' => $data->product_id])
                                     ->where(['product_meta' => 'stock'])
                                     ->update(['product_meta_value' => $updated_stock]);
                    }


                    //Reduce each stock of inventory
                    $selected_products = unserialize(get_product_meta($data->product_id , 'bundle_product'));

                    foreach ($selected_products as $key => $val){

                        $selected_prod = product::find($val);
                        $selected_prod_meta = get_product_meta($val , 'stock');

                        if(!empty($selected_prod->stock)){
                            $upd_stock_ = ( $selected_prod->stock - $data->qty );
                            product::where(['id' => $val])->update(['stock' => $upd_stock_]);
                        }

                        if(!empty($selected_prod_meta['product_meta_value'])){
                            $upd_stock = ( $selected_prod_meta['product_meta_value'] - $data->qty );
                            products_meta::where(['product_id' => $val ])
                                         ->where(['product_meta' => 'stock'])
                                         ->update(['product_meta_value' => $upd_stock]);
                        }

                    }


                }else{

                    $meta_value = products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->first();

                    if(!empty($prod->stock)){
                        $updated_stock_ = ( $prod->stock - $data->qty );
                        product::where(['id' => $data->product_id])->update(['stock' => $updated_stock_]);
                    }

                    if(!empty($meta_value['product_meta_value'])){
                        $updated_stock = ( $meta_value['product_meta_value'] - $data->qty );
                        products_meta::where(['product_id' => $data->product_id])
                                     ->where(['product_meta' => 'stock'])
                                     ->update(['product_meta_value' => $updated_stock]);
                    }

                }

            }


            Mail::to($order_detail['email'])->send(new OrderStatus($order));

            order_log::where('user_id', $arr['currentUserID'])->delete();

            \Session::put('success', 'Payment success');
            $noti = array("message" => "Shop Successfully!", "alert-type" => "success");

            return redirect()->route('thankyouView')->with($noti);
            //return redirect()->route('home')->with($noti);
        }
         //   dd($charge);

    }

    public function cancel_paypal_url(){
        $noti = array("message" => "Shop Failed!", "alert-type" => "danger");
        return redirect()->route('checkout')->with($noti);
    }

    public function paywithpaypal(){
        $noti = array("message" => "Shop Failed!", "alert-type" => "danger");
        return redirect()->route('checkout')->with($noti);
    }

    //for paypal
    public function createorder(Request $request)
    {
        $totall                     = [];
        $product_detail             = [];
        $product_extra_detail       = [];
        $item_detail                = [];
        $totalItems                 = 0;
        $items                      = [];
        $order_detail               = $request->get('arra');
        //$user                     = Auth::user();

        if(auth()->guest())
            $user                   = Guest::where(['guestID' => $order_detail['currentUserID'] ])->first();
        else
            $user                   = Auth::user();


        $order_log                  = order_log::where('user_id',$order_detail['currentUserID'])->get();

        foreach($order_log as $data) {
            $totall[]               = (($data->qty) * ($data->price));
            $item_detail[]          = $data;
            $product_detail[]       = $data->product_meta;
            $product_extra_detail[] = $data->extra_detail;
            $totalItems             = $totalItems + $data->qty;
            $items[]                = $data->product_name;
        }

        $total                      = array_sum($totall);
        $product_detail             = serialize($product_detail);
        $product_extra_detail       = serialize($product_extra_detail);

        $serialize_order_detail     = serialize($order_detail);
        $subtotal                   = $order_detail['subtotal'];

        //dd($lat);
        //$user = Auth::user();

        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');

        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            \Session::put('error','Payment failed');

             return Redirect::route('addmoney.paywithpaypal');
        }

        $payment = Payment::get($payment_id, $this->_api_context);

        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/


        if ($result->getState() == 'approved') {

            $order                            = new order();
            $order->user_id                   = $order_detail['currentUserID'];
            $order->order_id                  = uniqid("orderID-");
            $order->user_email                = $order_detail['email'];
            $order->order_details             = $serialize_order_detail;
            $order->user_meta                 = $user;
            $order->product_detail            = $product_detail;
            $order->product_extra_detail      = $product_extra_detail;
            $order->subtotal                  = $subtotal;
            $order->subtotal_of_products      = $order_detail['subtotal_of_products'];
            $order->total_shipping            = $order_detail['shipping_cost'];
            $order->discount                  = $order_detail['coupon_discount'];
            $order->discount_type             = $order_detail['coupon_type'];
            $order->selected_country          = $order_detail['selected_country'];
            $order->cost_per_country_weight   = $order_detail['cost_per_country_weight'];
            $order->total_weight              = $order_detail['weight_cost'];
            $order->total_products_weight     = $order_detail['total_product_weight'];
            $order->tax                       = "";
            $order->status                    = "Confirm";
            $order->selected_order_part_id    = isset( $order_detail['orderParts'] ) ? $order_detail['orderParts'] : null;
            $order->order_part_done           = isset( $order_detail['orderParts'] ) ? 1 : null;
            $order->order_log                 = $order_detail['order_log'];

            foreach($order_log as $data) {
                $prod = product::find($data->product_id);

                if($prod->product_type == "bundle_product"){
                    $meta_value = products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->first();

                    //Reduce stock from inventory
                    if(!empty($prod->stock)){
                        $updated_stock_ = ( $prod->stock - $data->qty );
                        product::where(['id' => $data->product_id])->update(['stock' => $updated_stock_]);
                    }

                    if(!empty($meta_value['product_meta_value'])){
                        $updated_stock = ( $meta_value['product_meta_value'] - $data->qty );
                        products_meta::where(['product_id' => $data->product_id])
                                     ->where(['product_meta' => 'stock'])
                                     ->update(['product_meta_value' => $updated_stock]);
                    }


                    //Reduce each stock of inventory
                    $selected_products = unserialize(get_product_meta($data->product_id , 'bundle_product'));

                    foreach ($selected_products as $key => $val){

                        $selected_prod = product::find($val);
                        $selected_prod_meta = get_product_meta($val , 'stock');

                        if(!empty($selected_prod->stock)){
                            $upd_stock_ = ( $selected_prod->stock - $data->qty );
                            product::where(['id' => $val])->update(['stock' => $upd_stock_]);
                        }

                        if(!empty($selected_prod_meta['product_meta_value'])){
                            $upd_stock = ( $selected_prod_meta['product_meta_value'] - $data->qty );
                            products_meta::where(['product_id' => $val ])
                                         ->where(['product_meta' => 'stock'])
                                         ->update(['product_meta_value' => $upd_stock]);
                        }

                    }


                }else{

                    $meta_value = products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->first();

                    if(!empty($prod->stock)){
                        $updated_stock_ = ( $prod->stock - $data->qty );
                        product::where(['id' => $data->product_id])->update(['stock' => $updated_stock_]);
                    }

                    if(!empty($meta_value['product_meta_value'])){
                        $updated_stock = ( $meta_value['product_meta_value'] - $data->qty );
                        products_meta::where(['product_id' => $data->product_id])
                                     ->where(['product_meta' => 'stock'])
                                     ->update(['product_meta_value' => $updated_stock]);
                    }

                }


//                $prod = product::find($data->product_id);
//                $meta_value = products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->first();
//
//                if(!empty($prod->stock)){
//                    $updated_stock_ = ( $prod->stock - $data->qty );
//                    product::where(['id' => $data->product_id])->update(['stock' => $updated_stock_]);
//                }
//
//                if(!empty($meta_value['product_meta_value'])){
//                    $updated_stock = ( $meta_value['product_meta_value'] - $data->qty );
//                    products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->update([
//                        'product_meta_value' => $updated_stock
//                    ]);
//                }
            }

            $order->save();

            if(auth()->guest()){
                $user->name                 = $order_detail['name'];
                $user->email                = $order_detail['email'];
                $user->place_order_status   = 1;
                $user->save();
            }

             Mail::to($order_detail['email'])->send(new OrderStatus($order));

             order_log::where('user_id',$order_detail['currentUserID'])->delete();

             \Session::put('success','Payment success');
             $noti = array("message" => "Shop Successfully!", "alert-type" => "success");

             return redirect()->route('thankyouView')->with($noti);
             //return redirect()->route('home')->with($noti);
        }

        \Session::put('error','Payment failed');
         $noti = array("message" => "Shop Failed!", "alert-type" => "danger");

         return redirect()->route('checkout')->with($noti);
    }

}

