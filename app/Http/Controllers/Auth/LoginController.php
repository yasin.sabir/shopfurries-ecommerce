<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Contracts\Factory as SocialiteFactory;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/Dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //    public function logout()
    //    {
    //        Auth::logout();
    //        return redirect()->to('/login');
    //    }


    public function redirectToProvider_github()
    {
        return Socialite::driver('github')->redirect();
    }

    public function redirectToProvider_google()
    {
        return Socialite::driver('google')->redirect();
    }

    public function redirectToProvider_twitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    public function redirectToProvider_deviantart()
    {
       return Socialite::driver('deviantart')->redirect();
    }


    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback_github()
    {
        $github_user = Socialite::driver('github')->user();
        $user = User::where(['provider_id'=> $github_user->getId()])->first();
        if(!$user){
            $user = User::create([
                'name' => $github_user->getNickname(),
                'email' => $github_user->getEmail(),
                'provider_id' => $github_user->getId(),
                'provider' => 'github',
                'status' => 1,
            ]);
            $user->assignRole('user');
        }
        Auth::login($user,true);
        return redirect($this->redirectTo);
        // $user->token;
    }

    public function handleProviderCallback_google()
    {
        $google_user = Socialite::driver('google')->user();
        $user = User::where(['provider_id'=> $google_user->getId()])->first();
        if(!$user){
            $user = User::create([
                'name' => $google_user->getName(),
                'email' => $google_user->getEmail(),
                'provider_id' => $google_user->getId(),
                'provider' => 'google',
                'status' => 1,
            ]);
            $user->assignRole('user');
        }
        Auth::login($user,true);
        return redirect($this->redirectTo);
        // $user->token;
    }

    public function handleProviderCallback_twitter()
    {
        $twitter_user = Socialite::driver('twitter')->user();
        $user = User::where(['provider_id'=> $twitter_user->getId()])->first();

        if(!$user){
            $user = User::create([
                'name' => $twitter_user->getName(),
                'email' => $twitter_user->getEmail(),
                'provider_id' => $twitter_user->getId(),
                'provider' => 'twitter',
                'status' => 1,
            ]);
            $user->assignRole('user');
        }
        Auth::login($user,true);
        return redirect($this->redirectTo);
        // $user->token;
    }

    public function handleProviderCallback_deviantart()
    {
        $deviantart_user = Socialite::driver('deviantart')->user();

        $user = User::where(['provider_id'=> $deviantart_user->getId()])->first();

        if(!$user){
            $user = User::create([
                'name' => $deviantart_user->getNickname(),
                'email' => $deviantart_user->getNickname().'@sample-deviantart.com',
                'provider_id' => $deviantart_user->getId(),
                'provider' => 'deviantart',
                'status' => 1,
            ]);
            $user->assignRole('user');
        }
        Auth::login($user,true);

        $checkUser = User::find($user->id);

        return redirect($this->redirectTo);

    }


    //    public function authenticated(Request $request, $user)
    //    {
    //        if (!$user->verified) {
    //            auth()->logout();
    //            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
    //        }
    //
    //        return redirect()->intended($this->redirectPath());
    //    }


}
