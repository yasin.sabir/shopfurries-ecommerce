<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserMeta;
use App\VerifyUser;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;


class   RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
      //protected $redirectTo = '/home';
      protected $redirectTo = '/Dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
            'streetName' => ['required'],
            'streetNo' => ['required'],
            'country' => ['required'],
            'city' => ['required'],
            'telno' => ['required' ,'numeric'],

            //            'role' => [
            //                'required',
            //                Rule::in(['user','vendor','admin']),
            //            ],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status'   => 1
        ]);

        $user->assignRole("user");

        $user_meta = [
            'street_name'   => $data['streetName'],
            'street_no'     => $data['streetNo'],
            'country'       => $data['country'],
            'city'          => $data['city'],
            'telno'         => $data['telno'],
        ];

        foreach ($user_meta as $key => $value){
            $user_meta = new UserMeta();
            $user_meta->user_id     = $user->id;
            $user_meta->meta_key    = $key;
            $user_meta->meta_value  = $value;
            $user_meta->save();
        }

        //            $verifyUser = VerifyUser::create([
        //              'user_id' => $user->id,
        //              'token' => str_random(40)
        //            ]);

        //Mail::to($user->email)->send(new VerifyMail($user));

        return $user;

    }

    function adminRegisterView()
    {
        return view('auth.registerAdmin');
    }


    //    public function verifyUser($token)
    //    {
    //        $verifyUser = VerifyUser::where('token', $token)->first();
    //        if (isset($verifyUser)) {
    //            $user = $verifyUser->user;
    //            if (!$user->verified) {
    //                $verifyUser->user->verified = 1;
    //                $verifyUser->user->save();
    //                $status = "Your e-mail is verified. You can now login.";
    //            } else {
    //                $status = "Your e-mail is already verified. You can now login.";
    //            }
    //        } else {
    //            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
    //        }
    //
    //        return redirect('/login')->with('status', $status);
    //    }


    //    protected function registered(Request $request, $user)
    //    {
    //        $this->guard()->logout();
    //        return redirect('/login')->with('status', 'We sent you an activation link. Check your email and click on the link to verify.');
    //    }



    //     protected function create(Request $request)
    //     {
    //         $user = new User;
    //         $user->name = $request->name;
    //         $user->email = $request->email;
    //         $user->password =  Hash::make($request->password);
    //         $user->save();
    //         $user->assignRole($request->role);
    //     }
}
