<?php

namespace App\Http\Controllers;

use App\Guest;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_id(Request $request){

        if($request->guest_id){

            $guest = new Guest();
            $guest->guestID = $request->guest_id;
            $guest->save();

            return response()->json(['status' => 'success','message' => 'ID Stored']);

        }else{
            return response()->json(['status' => 'error','message' => 'ID not set']);
        }

    }


    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroy_id(Request $request){
        echo $request->guest_id;
        if($request->guest_id){

            $guest       = Guest::where(['guestID' => $request->guest_id])->first();
            $guestDetail = Guest::where(['id' => $guest->id])->where(['place_order_status' => 0]);
            $guestDetail->delete();

            return response()->json(['status' => 'success','message' => 'ID Delete']);

        }else{
            return response()->json(['status' => 'error','message' => 'ID not found!']);
        }

    }

}
