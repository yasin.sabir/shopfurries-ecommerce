<?php

namespace App\Http\Controllers;

use App\Exports\ExportEmails;
use App\Guest;
use App\SiteSetting;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function s_gc_maintenance()
    {

        $get_settings  = SiteSetting::all();
        $site_settings = [];

        foreach ( $get_settings as $key => $val ) {
            $site_settings[ $val["key"] ] = $val["value"];
        }


        return view( 'admin_settings.general', [ 'site_settings' => $site_settings ] );
    }


    public function s_gc_maintenance_Update( Request $request )
    {

        if ( isset( $request->status ) && $request->status == "on" ) {

            $ss_maintenance = SiteSetting::where( [ 'key' => 'site_maintenance_status' ] )->first();
            SiteSetting::where( [ 'id' => $ss_maintenance->id ] )->update( [ 'value' => $request->status ] );

            $msg = "Status is on";

            return response()->json( array( [ 'msg' => $msg, 'val' => 'checked' ] ), 200 );

        } else if ( isset( $request->status ) && $request->status == "off" ) {

            $ss_maintenance = SiteSetting::where( [ 'key' => 'site_maintenance_status' ] )->first();
            SiteSetting::where( [ 'id' => $ss_maintenance->id ] )->update( [ 'value' => $request->status ] );

            $msg = "Status is off";

            return response()->json( array( [ 'msg' => $msg, 'val' => 'unchecked' ] ), 200 );

        } else {

        }

    }


    public function s_gc_maintenanceView()
    {
        return view( 'admin_settings.maintenance' );
    }


    public function user_deactivate_view( Request $request )
    {
        return view( 'admin_settings.user_deactivate' );
    }


    public function defaultSaleTax( Request $request )
    {

        if ( isset( $request->default_sales_tax ) ) {

            $data = SiteSetting::where( [ 'key' => 'default_sales_tax' ] )->first();

            if ( isset( $data ) ) {
                SiteSetting::where( [ 'key' => 'default_sales_tax' ] )->update( [
                                                                                    'value' => $request->default_sales_tax
                                                                                ] );
            } else {
                SiteSetting::updateOrInsert( [ 'key' => 'default_sales_tax', 'value' => $request->default_sales_tax ] );
            }

            $msg = "Default sales tax is updated!";

            return response()->json( array( [ 'msg' => $msg ] ), 200 );

        } else {

            //$msg = "Something went wrong with default sales tax!";
            $msg = "Default sales tax value is empty!";

            return response()->json( array( [ 'msg' => $msg ] ), 422 );

        }

    }


    public function defaultProductStock( Request $request )
    {

        if ( isset( $request->default_product_stock ) ) {

            $data = SiteSetting::where( [ 'key' => 'default_product_stock' ] )->first();

            if ( isset( $data ) ) {
                SiteSetting::where( [ 'key' => 'default_product_stock' ] )->update( [
                                                                                        'value' => $request->default_product_stock
                                                                                    ] );
            } else {
                SiteSetting::updateOrInsert( [ 'key'   => 'default_product_stock',
                                               'value' => $request->default_product_stock
                                             ] );
            }

            $msg = "Default product stock is updated!";

            return response()->json( array( [ 'msg' => $msg ] ), 200 );

        } else {

            //$msg = "Something went wrong with default sales tax!";
            $msg = "Default product stock value is empty!";

            return response()->json( array( [ 'msg' => $msg ] ), 422 );

        }
    }


    function uptoReviews( Request $request )
    {

        if ( isset( $request->upto_review ) ) {

            $data = SiteSetting::where( [ 'key' => 'upto_review' ] )->first();

            if ( isset( $data ) ) {
                SiteSetting::where( [ 'key' => 'upto_review' ] )->update( [
                                                                              'value' => $request->upto_review
                                                                          ] );
            } else {
                SiteSetting::updateOrInsert( [ 'key' => 'upto_review', 'value' => $request->upto_review ] );
            }

            $msg = "Upto Reviews is updated!";

            return response()->json( array( [ 'msg' => $msg ] ), 200 );

        } else {

            //$msg = "Something went wrong with default sales tax!";
            $msg = "Reviews field is empty!";

            return response()->json( array( [ 'msg' => $msg ] ), 422 );

        }

    }


    public function abandoned_cart_days( Request $request )
    {

        if ( isset( $request->abandoned_cart_days ) ) {

            $data = SiteSetting::where( [ 'key' => 'abandoned_cart_days' ] )->first();

            if ( isset( $data ) ) {
                SiteSetting::where( [ 'key' => 'abandoned_cart_days' ] )->update( [
                                                                                      'value' => $request->abandoned_cart_days
                                                                                  ] );
            } else {
                SiteSetting::updateOrInsert( [ 'key'   => 'abandoned_cart_days',
                                               'value' => $request->abandoned_cart_days
                                             ] );
            }

            $msg = "Abandoned cart days is updated!";

            return response()->json( array( [ 'msg' => $msg ] ), 200 );

        } else {

            //$msg = "Something went wrong with default sales tax!";
            $msg = "field is empty!";

            return response()->json( array( [ 'msg' => $msg ] ), 422 );

        }
    }


    public function mediaConfig_view()
    {

        $get_settings  = SiteSetting::all();
        $site_settings = [];

        foreach ( $get_settings as $key => $val ) {
            $site_settings[ $val["key"] ] = $val["value"];
        }


        return view( 'admin_settings.media', [ 'site_settings' => $site_settings ] );
    }


    public function mediaConfig_update( Request $request )
    {

        $data = SiteSetting::where( [ 'key' => 'product_image_size' ] )->first();

        if ( isset( $request->image_size ) && $request->image_size == '300' ) {

            if ( isset( $data ) ) {
                SiteSetting::where( [ 'key' => 'product_image_size' ] )->update( [ 'value' => $request->image_size ] );
            } else {
                SiteSetting::updateOrInsert( [ 'key' => 'product_image_size', 'value' => $request->image_size ] );
            }

            $msg = "Image Size is updated!";

            return response()->json( array( [ 'msg' => $msg ] ), 200 );

        } elseif ( isset( $request->image_size ) && $request->image_size == '600' ) {

            if ( isset( $data ) ) {
                SiteSetting::where( [ 'key' => 'product_image_size' ] )->update( [ 'value' => $request->image_size ] );
            } else {
                SiteSetting::updateOrInsert( [ 'key' => 'product_image_size', 'value' => $request->image_size ] );
            }

            $msg = "Image Size is updated!";

            return response()->json( array( [ 'msg' => $msg ] ), 200 );

        } elseif ( isset( $request->image_size ) && $request->image_size == '1024' ) {

            if ( isset( $data ) ) {
                SiteSetting::where( [ 'key' => 'product_image_size' ] )->update( [ 'value' => $request->image_size ] );
            } else {
                SiteSetting::updateOrInsert( [ 'key' => 'product_image_size', 'value' => $request->image_size ] );
            }

            $msg = "Image Size is updated!";

            return response()->json( array( [ 'msg' => $msg ] ), 200 );

        } else {
            $msg = "Not Valid Input Size!";

            return response()->json( array( [ 'msg' => $msg ] ), 422 );
        }

    }


    public function display_export_emails(){

        // for users have accounts in system
        $members_details = [];
        $guests_details  = [];
        $users           = User::role('user')->get();

        foreach ($users as $key => $val){
            $members_details [$key]['email'] = $val->email;
            $members_details [$key]['type'] = 'member';
        }

        // for guest users
        $guests = Guest::all();
        foreach ($guests as $key => $val){

            if(!empty($val->email)){
                if(!in_array($val->email ,$members_details[$key] )){
                  $guests_details[$key]['email'] = $val->email;
                  $guests_details[$key]['type'] = 'guest';
                }
            }
        }

        $arr = array_merge($members_details , $guests_details);

        return view('admin_settings.export-emails')->with(['arr' => $arr]);
    }

    public function exportEmails(Request $request){

        $export = new ExportEmails( [ unserialize($request->export_data) ] );
        return Excel::download( $export, 'emailExport-' . now() . '.xlsx' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        //
    }
}
