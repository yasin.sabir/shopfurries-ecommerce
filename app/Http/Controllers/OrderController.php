<?php

namespace App\Http\Controllers;


use App;
use App\Mail\CustomThrowExceptionEmail;
use Auth;
use App\User;
use App\order;
use App\Product;
use App\Category;
use App\Shipping;
use App\order_log;
use Carbon\Carbon;
use App\SiteSetting;
use App\products_meta;
use App\Category_Meta;
use App\Mail\OrderStatus;
use App\productVariation;
use Cartalyst\Stripe\Config;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Exports\OrdersExport;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Rap2hpoutre\FastExcel\FastExcel;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;
use Box\Spout\Writer\Style\StyleBuilder;
use Illuminate\Support\Facades\Redirect;




class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function Buynowupsell(Request $request)
    {
        $detail = $request->all();
        $product_id = $detail['product_id'];
        $user_id = $detail['user_id'];
        $upsell_product_id = $detail['upsell_product_id'];
        $material = $detail['material'];
        $size = $detail['size'];
        $extra = $detail['extra'];
        $save =  $detail['save'];
        $product_weight = $detail['product_weight'];

        try {
            if ($user_id != null) {

                $user = User::find($user_id);

                if($user !== null){
                    //for existing users
                    $user_serialize = $user->toJson();
                }else{
                    //for guest users
                    $user           = App\Guest::find($user_id);
                    $user_serialize = serialize($user);
                }

                $product = product::find($product_id);
                $product_serialize = [
                    'id'            => $product->id,
                    'user_id'       => $product->user_id,
                    'title'         => $product->title,
                    'slug'          => $product->slug,
                    'description'   => $product->description,
                    'sku'           => $product->sku,
                    'image'         => $product->image,
                    'video'         => $product->video,
                    'price'         => $product->price,
                    'stock'         => $product->stock,
                    'status'        => $product->status,
                ];

                $upsell_product = product::find($upsell_product_id);

                $upsell_product_serialize = [
                    'id'            => $upsell_product->id,
                    'user_id'       => $upsell_product->user_id,
                    'title'         => $upsell_product->title,
                    'slug'          => $upsell_product->slug,
                    'description'   => $upsell_product->description,
                    'sku'           => $upsell_product->sku,
                    'image'         => $upsell_product->image,
                    'video'         => $upsell_product->video,
                    'price'         => $upsell_product->price,
                    'stock'         => $upsell_product->stock,
                    'status'        => $upsell_product->status,
                ];

                $discount_price =  $product->price +  $upsell_product->price;
                $discount_price = $discount_price - $save;
               //$product_sale_price       = products_meta::where(['product_id' => $product_id])->where(['product_meta' => 'sale_price'])->first();
                //$upsell_product_sale_price       = products_meta::where(['product_id' => $upsell_product_id])->where(['product_meta' => 'sale_price'])->first();
                //dd($product_sale_price);

                $product_serialize = serialize($product_serialize);
                $upsell_productserialize = serialize($upsell_product_serialize);

                $extra_detail = [
                    'material' => $material,
                    'size'     => $size,
                    'extra'    => $extra,
                ];

                $order_log                        = new order_log();
                $order_log->user_id               = $user_id;
                $order_log->product_id            = $product_id;
                $order_log->product_name          = "Bunch Of ".$product->title." & ".$upsell_product->title;
                $order_log->product_meta          = $product_serialize;
                $order_log->upsell_product_id     = $upsell_product_id;
                $order_log->upsell_product_meta   = $upsell_productserialize;
                $order_log->price                 = $discount_price;
                $order_log->user_meta             = (($user_id != null) ? $user_serialize : 'null');
                $order_log->material              = $material;
                $order_log->size                  = $size;
                $order_log->extra                 = $extra;
                $order_log->weight                = $product_weight;
                $order_log->qty                   = 1;
                $order_log->extra_detail          = serialize($extra_detail);
                $order_log->image                 = $product->image;
                $order_log->save();

            }



            $order = order_log::where(['user_id' => $user_id])->get();
            $count = count($order);

            $noti  = array("message" => "Product added in cart successfully " , "alert-type" => "success");
            return response()->json(['ok' => 'Product added in cart successfully ','order_log' => $order_log ,'count' => $count]);


        } catch (\Exception $ex) {
            $noti = array( "message" => $ex->getMessage() , "alert-type" => "error");
            return response()->json(["message" => $ex->getMessage()]);
        }

    }

    public function Buynow(Request $request)
    {
        //customVarDump($request->user_id);
        //customVarDump_die($request->all());


        $size           = $request->size;
        $extra          = $request->extra;
        $weight         = $request->weight;
        $material       = $request->material;
        $quantity       = $request->quantity;
        $product_id     = $request->product_id;
        $user_id        = $request->user_id;
        $variation_id   = $request->vari_id;
        $order_type     = $request->order_type;
        $user_serialize = "";

        try {

            if ($user_id != null) {
                $user = User::find($user_id);

                if($user !== null){
                   //for existing users
                    $user_serialize = $user->toJson();
                }else{
                    //for guest users
                    $user           = App\Guest::find($user_id);
                    $user_serialize = serialize($user);
                }
            }


            $extra_detail = [
                'material' => $material,
                'size'     => $size,
                'extra'    => $extra,
                'quantity' => $quantity,
                'weight'   => ($quantity*$weight),
                'type'     => $order_type,
            ];

            $product_cart = order_log::where([
                'product_id'           => $product_id ,
                'product_variation_id' => $variation_id ,
                'user_id'              => $user_id ,
                'material'             => $material ,
                'size'                 => $size ,
                'extra'                => $extra,
                'weight'               => ($quantity*$weight)
            ])->get();

            $product_in_cart = count($product_cart);

            if (($product_in_cart == 0)){

                $product = product::find($product_id);
                $product_serialize = [
                    'id'            => $product->id,
                    'user_id'       => $product->user_id,
                    'title'         => $product->title,
                    'slug'          => $product->slug,
                    'description'   => $product->description,
                    'sku'           => $product->sku,
                    'image'         => $product->image,
                    'video'         => $product->video,
                    'price'         => $product->price,
                    'stock'         => $product->stock,
                    'status'        => $product->status,
                    'quantity'      => $quantity,
                    'variation_id'  => $variation_id,
                ];

               $product_serialize = serialize($product_serialize);

                if(!empty($variation_id)){

                    $product_variation                = productVariation::where(['id' => $variation_id])->first();

                    $order_log                        = new order_log();
                    $order_log->user_id               = $user_id;
                    $order_log->product_id            = $product_id;
                    $order_log->product_name          = $product->title;
                    $order_log->product_meta          = $product_serialize;
                    $order_log->product_variation_id  = $variation_id;
                    $order_log->price                 = $product_variation->price;
                    $order_log->user_meta             = (($user_id != null) ? $user_serialize : 'null');
                    $order_log->material              = $material;
                    $order_log->size                  = $size;
                    $order_log->extra                 = $extra;
                    $order_log->extra_detail          = serialize($extra_detail);
                    $order_log->image                 = $product->image;
                    $order_log->qty                   = $quantity;
                    $order_log->weight                = ($quantity*$weight);
                    $order_log->order_type            = $order_type;
                    $order_log->save();

                }else{

                    $order_log                        = new order_log();
                    $order_log->user_id               = $user_id;
                    $order_log->product_id            = $product_id;
                    $order_log->product_name          = $product->title;
                    $order_log->product_meta          = $product_serialize;
                    $order_log->product_variation_id  = $variation_id;
                    $order_log->price                 = $product->price;
                    $order_log->user_meta             = ( !empty($user) ? $user_serialize : null);
                    $order_log->material              = $material;
                    $order_log->size                  = $size;
                    $order_log->extra                 = $extra;
                    $order_log->weight                = $weight;
                    $order_log->extra_detail          = serialize($extra_detail);
                    $order_log->image                 = $product->image;
                    $order_log->qty                   = $quantity;
                    $order_log->weight                = ($quantity*$weight);
                    $order_log->order_type            = $order_type;
                    $order_log->save();
                }


            }else if($product_in_cart > 0 ){

                $extra_detail = [
                    'material'       => $material,
                    'size'           => $size,
                    'extra'          => $extra,
                    'quantity'       => $quantity,
                    'weight'         => ($quantity*$weight),
                ];

                $product           = product::find($product_id);
                $product_serialize = $product->toJson();

                $order_log     = order_log::where([
                    'user_id'  => $user_id ,
                    'material' => $material ,
                    'size'     => $size ,
                    'extra'    => $extra
                ])->first();

                $old_extra_detail          = unserialize($order_log->extra_detail);
                $old_qty                   = $order_log->qty;
                $old_weight                = $order_log->weight;
                $old_extra_detail          = array($old_extra_detail);
                $new_extra_detail          = array($extra_detail);
                $update_extra_detail       = array_merge($old_extra_detail,$new_extra_detail);
                $update_extra_detail       = serialize($update_extra_detail);
                $order_log->extra_detail   = $update_extra_detail;
                $order_log->qty            = $old_qty + $quantity;
                $order_log->weight         = ($order_log->qty * $weight);
                $order_log->order_type     = $order_type;
                $order_log->save();

            }

            $order = order_log::where(['user_id' => $user_id])->get();
            $count = count($order);

            $noti  = array("message" => "Product added in cart successfully " , "alert-type" => "success");
            return response()->json(['ok' => 'Product added in cart successfully ','order_log' => $order_log ,'count' => $count]);


        } catch (\Exception $ex) {
              $noti = array( "message" => $ex->getMessage() , "alert-type" => "error");
              return response()->json(["message" => $ex->getMessage()]);
        }

    }

    public function order_BuynowBackend(Request $request)
    {
        //customVarDump($request->user_id);

        $size           = $request->size;
        $extra          = $request->extra;
        $weight         = $request->weight;
        $material       = $request->material;
        $quantity       = $request->quantity;
        $product_id     = $request->product_id;
        $user_id        = $request->user_id;
        $variation_id   = $request->vari_id;
        $user_serialize = "";

        try {


            if ($user_id != null) {
                $user = User::find($user_id);
                if($user !== null){
                    //for existing users
                    $user_serialize = $user->toJson();
                }
            }

            $extra_detail = [
                'material' => $material,
                'size'     => $size,
                'extra'    => $extra,
                'quantity' => $quantity,
                'weight'   => ($quantity*$weight),
            ];

            $product_in_cart = order_log::where([
                'product_id'           => $product_id ,
                'product_variation_id' => $variation_id ,
                'user_id'              => $user_id ,
                'material'             => $material ,
                'size'                 => $size ,
                'extra'                => $extra,
                'weight'               => ($quantity*$weight)
            ])->get();

            $product_in_cartt = count($product_in_cart);
            //            if($product_in_cartt > 0){
            //                foreach($product_in_cart as $check){
            //                    $qty = $check->qty;
            //                    $material = $check->material;
            //                    $size = $check->size;
            //                    $extra_detail_check = ($extra_detail_check === $extra_detail);
            //                }
            //
            //           }

            // dd($product_in_cart);

            if (($product_in_cartt == 0)){
                $product = product::find($product_id);
                $product_serialize = [
                    'id'            => $product->id,
                    'user_id'       => $product->user_id,
                    'title'         => $product->title,
                    'slug'          => $product->slug,
                    'description'   => $product->description,
                    'sku'           => $product->sku,
                    'image'         => $product->image,
                    'video'         => $product->video,
                    'price'         => $product->price,
                    'stock'         => $product->stock,
                    'status'        => $product->status,
                    'variation_id'  => $variation_id,
                ];

                $product_serialize = serialize($product_serialize);

                if(!empty($variation_id)){

                    $product_variation                = productVariation::where(['id' => $variation_id])->first();

                    $order_log                        = new order_log();
                    $order_log->user_id               = $user_id;
                    $order_log->product_id            = $product_id;
                    $order_log->product_name          = $product->title;
                    $order_log->product_meta          = $product_serialize;
                    $order_log->product_variation_id  = $variation_id;
                    $order_log->price                 = $product_variation->price;
                    $order_log->user_meta             = (($user_id != null) ? $user_serialize : 'null');
                    $order_log->material              = $material;
                    $order_log->size                  = $size;
                    $order_log->extra                 = $extra;
                    $order_log->extra_detail          = serialize($extra_detail);
                    $order_log->image                 = $product->image;
                    $order_log->qty                   = $quantity;
                    $order_log->weight                = ($quantity*$weight);
                    $order_log->order_type            = "front-order";
                    $order_log->save();

                }else{

                    $order_log                        = new order_log();
                    $order_log->user_id               = $user_id;
                    $order_log->product_id            = $product_id;
                    $order_log->product_name          = $product->title;
                    $order_log->product_meta          = $product_serialize;
                    $order_log->product_variation_id  = $variation_id;
                    $order_log->price                 = $product->price;
                    $order_log->user_meta             = (($user_id != null) ? $user_serialize : 'null');
                    $order_log->material              = $material;
                    $order_log->size                  = $size;
                    $order_log->extra                 = $extra;
                    $order_log->weight                = $weight;
                    $order_log->extra_detail          = serialize($extra_detail);
                    $order_log->image                 = $product->image;
                    $order_log->qty                   = $quantity;
                    $order_log->weight                = ($quantity*$weight);
                    $order_log->order_type            = "front-order";
                    $order_log->save();
                }


            }else if($product_in_cartt > 0 ){

                //dd('abc');
                $extra_detail = [
                    'material'       => $material,
                    'size'           => $size,
                    'extra'          => $extra,
                    'quantity'       => $quantity,
                    'weight'         => ($quantity*$weight),
                ];

                $product           = product::find($product_id);
                $product_serialize = $product->toJson();

                $order_log     = order_log::where([
                    'user_id'  => $user_id ,
                    'material' => $material ,
                    'size'     => $size ,
                    'extra'    => $extra
                ])->first();

                $old_extra_detail          = unserialize($order_log->extra_detail);
                $old_qty                   = $order_log->qty;
                $old_weight                = $order_log->weight;
                $old_extra_detail          = array($old_extra_detail);
                $new_extra_detail          = array($extra_detail);
                $update_extra_detail       = array_merge($old_extra_detail,$new_extra_detail);
                $update_extra_detail       = serialize($update_extra_detail);
                $order_log->extra_detail   = $update_extra_detail;
                $order_log->qty            = $old_qty + $quantity;
                $order_log->weight         = ($order_log->qty * $weight);
                $order_log->order_type     = "front-order";
                $order_log->save();

            }

            $order = order_log::where(['user_id' => $user_id])->get();
            $count = count($order);

            $noti  = array("message" => "Product added in cart successfully " , "alert-type" => "success");
            return response()->json(['ok' => 'Product added in cart successfully ','order_log' => $order_log ,'count' => $count]);


        } catch (\Exception $ex) {
            $noti = array( "message" => $ex->getMessage() , "alert-type" => "error");
            return response()->json(["message" => $ex->getMessage()]);
        }

    }

    public function allorderlist()
    {
        try{

            $order = order::all();
            return view('order.allorder-list',['order'=>$order]);

        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }
    }

    public function myorderlist()
    {
        try{

            $user = Auth::user();
            $order = order::where('user_id',$user->id)->get();
            return view('order.myorder-list',['order'=>$order]);

        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }
    }

    public function exportOrder_excel(order $order){

        try{
            return Excel::download(new OrdersExport($order), 'orders-'.now().'.xlsx');
        }catch (\Exception $ex){
            $noti = array("message" => "Error -".$ex->getMessage(), "alert-type" => "error");
            return redirect()->back()->with($noti);
        }
    }

    public function exportSpecificOrderExcel($order_id){

     //   try{
            $orderID = $order_id;

            $orderDetail = order::find($order_id);
            $details = unserialize($orderDetail->order_details);
            $productDetail = unserialize($orderDetail->product_detail);
            $productExtraDetail = unserialize($orderDetail->product_extra_detail);
            $userMeta = (array)json_decode($orderDetail->user_meta);


            $phpWord = new \PhpOffice\PhpWord\PhpWord();

            $section = $phpWord->addSection();

            $description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

            $section->addImage("http://itsolutionstuff.com/frontTheme/images/logo.png");
            $section->addText($description);

            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        try {
            $objWriter->save(storage_path('helloWorld.docx'));
        } catch (Exception $e) {
        }


        return response()->download(storage_path('helloWorld.docx'));





//            customVarDump( $orderDetail->toArray() );
//            customVarDump( $details );
//            customVarDump( $productDetail );
//            customVarDump( $productExtraDetail );
//            customVarDump( $userMeta );
//            die;


//        }catch (\Exception $ex){
//            $noti = array( "message" => "Exception : " . $ex->getMessage() , "alert-type" => "error" );
//            return redirect()->back()->with(  $noti );
//        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {

        try {

            $order                    = order::find(decrypt( $id ));

            if ( ! empty( $order ) ) {


                $order_id              = $order->id;
                $order_custom_id       = $order->order_id;
                $product_extra_details = [];
                $product_details       = [];
                $user_detail           = $order->user_meta;
                $order_details         = unserialize($order->order_details);
                $product_detail        = unserialize($order->product_detail);
                $product_extra_detail  =  unserialize($order->product_extra_detail);

                foreach($product_extra_detail as $data){
                    $product_extra_details[] = unserialize($data);
                }
                foreach($product_detail as $data){
                    $product_details[]       = unserialize($data);
                }

                if(Auth::user()->isAdmin()) {
                    return view('order.order_detail_admin',
                        [
                            'order'                 => $order,
                            'order_details'         => $order_details,
                            'product_detail'        => $product_details,
                            'product_extra_detail'  => $product_extra_details,
                            'id'                    => $order_id,
                            'order_id'              => $order_custom_id
                        ]);
                }else {
                    return view('order.order_detail_user',
                        [
                            'order'                 => $order,
                            'order_details'         => $order_details,
                            'product_detail'        => $product_details,
                            'product_extra_detail'  => $product_extra_details,
                            'id'                    => $order_id,
                            'order_id'              => $order_custom_id
                        ]);
                }

            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }
        return view( 'errors.404' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status          = $request->status;
        $email           = $request->email;
        $order           = order::find($id);
        $oldstatus       = $order->status;
        $previous_detail = unserialize($order->order_details);

        if($status == null){
            $status = $oldstatus;
        }

        //dd($request->all());

        $order_detail = [
              "_token"          => $request->_token,
              "name"            => $request->name,
              "email"           => $request->email,
              "address"         => $request->address,
              "mobile"          => $request->phone,
              "pcode"           => $request->pcode,
              "city"            => $request->city,
              "d-country"       => $request->ocountry,
              "other-address"   => $request->oaddress,
              "other-city"      => $request->ocity,
              "other-pcode"     => $request->opcode,
              "shipping"        => $request->shipping,
              "payment"         => $request->payment,
              "other-country"   =>$request->ocountry
        ];

        $order_detail           = serialize($order_detail);
        $order->order_details   = $order_detail;
        $order->status          = $status;
        $order->save();

        Mail::to($email)->send(new OrderStatus($order));

        $noti = array("message" => "Order updated successfully", "alert-type" => "success");

        if(Auth::user()->isAdmin()){
            return redirect()->route('order.alllist')->with($noti);
        }
        return redirect()->route('order.mylist')->with($noti);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{

            order::where('id',$id)->delete();
            $noti = array("message" => "Order deleted successfully", "alert-type" => "success");
            if(Auth::user()->isAdmin()){
                return redirect()->route('order.alllist')->with($noti);
            }
            return redirect()->route('order.mylist')->with($noti);


        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }

    }

    public function destroyAllOrders(Request $request){

        try{

            $ids = json_decode($request->delete_ids);
            customVarDump_die($ids);

            foreach ($ids as $key => $id){
                $order = order::findOrFail($id);
                $order->delete();
            }

            $noti = array("message" => "Selected orders are deleted successfully", "alert-type" => "success");
            return redirect()->back()->with($noti);

        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }



    }

    public function productdestroy(Request $request,$id){

        try{


            $order_id = $request->order_id;
            $order = order::findOrFail($order_id);
            $product_detail = unserialize($order->product_detail);

            return redirect()->back();

        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }
    }

    public function pdfgenerate(Request $request){

        //Ahsan code without checkout calculation....

        //        $id                     = $request->order_id;
        //        $order                  = order::find($id);
        //        $order_id               = $order->id;
        //        $order_custom_id        = $order->order_id;
        //        $product_extra_details  = [];
        //        $product_details        = [];
        //        $user_detail            = $order->user_meta;
        //        $order_details          = unserialize($order->order_details);
        //        $product_detail         = unserialize($order->product_detail);
        //        $product_extra_detail   = unserialize($order->product_extra_detail);

        //        foreach($product_extra_detail as $data){
        //            $product_extra_details[] = unserialize($data);
        //        }
        //        foreach($product_detail as $data){
        //            $product_details[] = unserialize($data);
        //        }

        //===============================================================================================
        //Yasin code with calculation.....

        $id                                      = $request->order_id;
        $order                                   = order::find($id);
        $order_id                                = $order->id;
        $order_log                               = unserialize($order->order_log);
        $userID_in_order_log                     = $order_log[0]['user_id'];
        $user_detail                             = $order->user_meta;
        $order_details                           = unserialize($order->order_details);


        $totall                                  = [];
        $dst                                     = SiteSetting::where(['key' => 'default_sales_tax'])->first();
        $product_meta                            = [];
        $category_sales_tax                      = [];
        $product_cate_ids                        = [];
        $after_category_SalesTax_added           = "";
        $after_defaultSalesTax_added             = "";
        $product_groupBy_category_with_salesTax  = [];
        $product_groupBy_moreThan_two_categories = [];

        foreach($order_log as $key => $total){

            $totall[] = (($total['qty']) * ($total['price']));

            $pr = products_meta::where(['product_id' => $total["product_id"] ])->get();
            $product_meta [] = $pr->toArray();

            foreach($product_meta[$key] as $k => $val){
                if($val["product_meta"] == "categories"){

                    $cate = unserialize($val["product_meta_value"]);

                    //customVarDump($cate);
                    if(count($cate) <= 1){

                        $cate_id   = $cate[0];
                        $cate_meta = Category_Meta::where(['category_id' => $cate_id])
                                                  ->where(['meta_key' => 'cate_sale_tax'])
                                                  ->get();

                        foreach ($cate_meta as $i => $data){
                            if($data["category_id"] == $cate_id){
                                $product_cate_ids  [] = $data['category_id'];
                                $product_ids       [] = $total['product_id'];
                                $pp                   = ($total['qty']*$total["price"]);
                            }
                        }

                    }else{
                        $product_groupBy_moreThan_two_categories [$total["product_id"]] = $total['qty'] *  $total["price"];
                        if(isset($dst)){
                            $Default_sale_tax                     = ( ( $dst->value / 100 ) * array_sum($product_groupBy_moreThan_two_categories) ) ;
                            $after_defaultSalesTax_added          = $Default_sale_tax + array_sum($product_groupBy_moreThan_two_categories);
                        }else{
                            $without_DefaultSalesTax              = array_sum($product_groupBy_moreThan_two_categories);
                        }

                    }

                }
            }

        }

        $product_groupBy_category = [];
        $product_cate_id         = array_unique($product_cate_ids);

        foreach ($product_cate_id as $k => $v){
            $cc = Category::find($v);
            foreach ($cc->products as $kk => $vv){

                if(in_array($vv->id,$product_ids)){

                    foreach ($order_log as $i => $val){
                        if($val['product_id'] == $vv->id){
                            $product_groupBy_category[$cc->id][$vv->id] =  ($val['qty'] * $val['price']);
                        }
                    }
                    //$order_log_product                   = order_log::where('user_id',$userID_in_order_log[0]['user_id'])
                    //                                              ->where(['product_id' => $vv->id])->first();
                    //$product_groupBy_category[$cc->id][$vv->id] =  ($order_log_product['qty'] * $order_log_product['price']);
                }

            }
        }


        //Products group by categories
        //customVarDump($product_groupBy_category);

        //Products group by more than 2 categories
        //customVarDump($product_groupBy_moreThan_two_categories);

        foreach ($product_groupBy_category as $i => $val){

            $category_SalesTax = Category_Meta::where(['category_id' => $i])
                                              ->where(['meta_key' => 'cate_sale_tax'])
                                              ->first();

            $category_SalesTax_value         = ( $category_SalesTax->meta_value / 100 );
            $after_category_SalesTax_added   = ( $category_SalesTax_value * array_sum($val));
            $product_groupBy_category_with_salesTax[$i][] = ( $after_category_SalesTax_added + array_sum($val));
        }


        if(isset($dst)){
            $defaultSaleTax = $after_defaultSalesTax_added;
        }else{
            $defaultSaleTax = $without_DefaultSalesTax;
        }

        // Generate sub total of all product including default or category sales tax
        $addSubTotal [] = $defaultSaleTax;

        foreach ($product_groupBy_category_with_salesTax as $key => $val){
            $addSubTotal [] = $val[0];
        }
        $addSubTotal_ = array_sum($addSubTotal);

        //$total_sum  = array_sum($totall);
        $count        = count($order_log);

        $total_weight = 0;
        foreach ($order_log as $key => $val){
            $total_weight = $total_weight + $val['weight'];
        }

        $shipping = Shipping::all();

        //Default Shipping & Weight costs
        $Default_Shipping_Setting = [];
        $default_weight_price     = SiteSetting::where(['key' => 'default_weight_price'])->first();
        $default_shipping_cost    = SiteSetting::where(['key' => 'default_shipping_cost'])->first();

        $Default_Shipping_Setting ['default_weight_price']  = $default_weight_price->value;
        $Default_Shipping_Setting ['default_shipping_cost'] = $default_shipping_cost->value;

        //        customVarDump($product_groupBy_category);
        //        customVarDump($product_groupBy_category_with_salesTax);
        //        customVarDump($product_groupBy_moreThan_two_categories);
        //        customVarDump($defaultSaleTax);
        //        customVarDump($count);
        //        customVarDump($addSubTotal_);
        //        customVarDump($total_weight);
        //        customVarDump($shipping);
        //        customVarDump($Default_Shipping_Setting);
        //        die;

        $orderCal['selected_country']          = $order->selected_country;
        $orderCal['shipping_cost']             = $order->total_shipping;
        $orderCal['cost_per_country_weight']   = $order->cost_per_country_weight;
        $orderCal['weight_cost']               = $order->total_weight;
        $orderCal['total_products_weight']     = $order->total_products_weight;  //in (kg)
        $orderCal['coupon_discount']           = $order->discount;
        $orderCal['coupon_type']               = $order->discount_type;
        $orderCal['subtotal_of_products']      = $order->subtotal_of_products;
        $orderCal['subtotal']                  = $order->subtotal;

        $customerAddress = '';
        $customerAddress .= isset( $order_details['address'] ) ? $order_details['address'] : '';
        $customerAddress .= isset( $order_details['pcode'] ) ? $order_details['pcode'].", " : '';
        $customerAddress .= isset( $order_details['city'] ) ? $order_details['city']." " : '';
        $customerAddress .= isset( $order_details['state'] ) ? $order_details['state']." " : '';
        $customerAddress .= isset( $order_details['d-country'] ) ? $order_details['d-country'] : '';

        $order_details['customerAddress'] = $customerAddress;

        $pdf = PDF::loadView('order.pdfinvoice',[
            'id'                       => $order_id,
            'order'                    => $order,
            'order_details'            => $order_details,
            'orderCal'                 => $orderCal,
            'status'                   => $order->status,
            'order_log'                => $order_log,
        ]);
        return $pdf->download('SH_invoice_'.$request->order_id.'.pdf');

		//          return view('order.pdfinvoice')->with([
		//            'id'                       => $order_id,
		//            'order'                    => $order,
		//            'order_details'            => $order_details,
		//            'orderCal'                 => $orderCal,
		//            'status'                   => $order->status,
		//            'order_log'                => $order_log,
		//          ]);


        //        if(Auth::user()->isAdmin()) {
        //            $pdf = PDF::loadView('order.pdfinvoice',[
        //                'order'                    => $order,
        //                'order_details'            => $order_details,
        //                'product_detail'           => $product_details,
        //                'product_extra_detail'     => $product_extra_details,
        //                'id'                       => $order_id,
        //                'order_id'                 => $order_custom_id,
        //                'subtotal'                 => $order->subtotal,
        //                'subtotal_of_products'     => $order->subtotal_of_products,
        //                'total_shipping'           => $order->total_shipping,
        //                'discount'                 => $order->discount,
        //                'discount_type'            => $order->discount_type,
        //                'selected_country'         => $order->selected_country,
        //                'cost_per_country_weight'  => $order->cost_per_country_weight,
        //                'total_product_weight_kg'  => $order->total_products_weight,
        //                'total_weight'             => $order->total_weight,
        //                'status'                   => $order->status,
        //
        //            ]);
        //            return $pdf->download('invoice_'.$request->order_id.'.pdf');
        //
        //        }else {
        //            $pdf = PDF::loadView('order.pdfinvoice',[
        //                'order'                    => $order,
        //                'order_details'            => $order_details,
        //                'product_detail'           => $product_details,
        //                'product_extra_detail'     => $product_extra_details,
        //                'id'                       => $order_id,
        //                'order_id'                 => $order_custom_id,
        //                'subtotal'                 => $order->subtotal,
        //                'subtotal_of_products'     => $order->subtotal_of_products,
        //                'total_shipping'           => $order->total_shipping,
        //                'discount'                 => $order->discount,
        //                'discount_type'            => $order->discount_type,
        //                'selected_country'         => $order->selected_country,
        //                'cost_per_country_weight'  => $order->cost_per_country_weight,
        //                'total_product_weight_kg'  => $order->total_products_weight,
        //                'total_weight'             => $order->total_weight,
        //                'status'                   => $order->status,
        //            ]);
        //            return $pdf->download('invoice_'.$request->order_id.'.pdf');
        //        }

    }


    //Below functions for create order from dashboard panel
    public function manual_create(){

        try{

            $products_ = product::where('stock','>' ,0)->get();

            $products  = [];

            foreach ($products_ as $key => $product){

                $metas = products_meta::where(['product_id' => $product->id])->get();

                foreach ($metas as $k => $meta){
                    if(($meta['product_meta'] == 'stock') && ( $meta['product_meta_value'] >= 1 )){
                        $products [] = $product;
                    }
                }
            }

            $users = User::all();

            return view('order.manual.add' , ['products' => $products , 'users' => $users]);


        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }

    }

    public function manual_process(Request $request){

        try{

            $custom_msg = [
                'user_id.required' => 'User is required',
                //'quantity.required' => 'Quantity not less than 1'
            ];

            $this->validate($request ,[
                'user_id' => 'required',
            ],$custom_msg);


            if($request->selected_products){

                $selected_products = [];
                $order_logs        = [];

                foreach ($request->selected_products as $key => $id){
                    $p = product::where(['id' => $id])->first();

                    $selected_products [$key]['id']           = $p->id;
                    $selected_products [$key]['user_id']      = $p->user_id;
                    $selected_products [$key]['title']        = $p->title;
                    $selected_products [$key]['slug']         = $p->slug;
                    $selected_products [$key]['description']  = $p->description;
                    $selected_products [$key]['sku']          = $p->sku;
                    $selected_products [$key]['image']        = $p->image;
                    $selected_products [$key]['video']        = $p->video;
                    $selected_products [$key]['price']        = ( $p->price * $request->quantity[$key] );
                    $selected_products [$key]['stock']        = $p->stock;
                    $selected_products [$key]['status']       = $p->status;
                    $selected_products [$key]['quantity']     = $request->quantity[$key];


                    $metas = products_meta::where([ 'product_id' => $id ])->get();
                    foreach ($metas as $k => $meta){
                        $selected_products [$key]['product_meta'][$meta->product_meta] = $meta->product_meta_value;

                        if( ($meta->product_meta == "weight") && (!empty($meta->product_meta_value)) ){
                            $selected_products [$key]['weight'] = $meta->product_meta_value;
                        }else{

                            //Default Shipping & Weight costs
                            $Default_Shipping_Setting = [];
                            $default_weight_price     = SiteSetting::where(['key' => 'default_weight_price'])->first();
                            $default_shipping_cost    = SiteSetting::where(['key' => 'default_shipping_cost'])->first();

                            $Default_Shipping_Setting ['default_weight_price']  = $default_weight_price->value;
                            $Default_Shipping_Setting ['default_shipping_cost'] = $default_shipping_cost->value;

                            $selected_products [$key]['weight'] = $default_weight_price->value;
                        }
                    }

                    $log = order_log::where(['user_id'=> $request->user_id])
                                    ->where(['product_id'=>$p->id])
                                    ->where(['back_order_status'=>1])->first();

                    if( empty($log) ){
                        $order_log                        = new order_log();
                        $user_details                     = User::find($request->user_id);
                        $user_serialize                   = $user_details->toJson();
                        $product_serialize = [
                            'id'            => $p->id,
                            'user_id'       => $p->user_id,
                            'title'         => $p->title,
                            'slug'          => $p->slug,
                            'description'   => $p->description,
                            'sku'           => $p->sku,
                            'image'         => $p->image,
                            'video'         => $p->video,
                            'price'         => $p->price,
                            'stock'         => $p->stock,
                            'status'        => $p->status,
                            'quantity'      => $request->quantity[$key],
                            'variation_id'  => "",
                        ];
                        $order_log->user_id               = $request->user_id;
                        $order_log->product_id            = $p->id;
                        $order_log->product_name          = $p->title;
                        $order_log->product_meta          = serialize($product_serialize);
                        $order_log->price                 = $p->price;
                        $order_log->user_meta             = (($request->user_id != null) ? $user_serialize : 'null');
                        $order_log->material              = $selected_products [$key]['product_meta']['material'];
                        $order_log->size                  = $selected_products [$key]['product_meta']['sizes'];
                        $order_log->extra                 = "Yes";
                        $order_log->image                 = $p->image;
                        $order_log->qty                   = $request->quantity[$key];
                        $order_log->weight                = ( $request->quantity[$key] * $selected_products [$key]['weight'] );
                        $order_log->order_type            = "back-order";
                        $order_log->back_order_status     = 1;
                        $order_log->save();
                        $order_logs[]                     = $order_log->id;
                    }


                }

            }else{
                $selected_products = [];
            }

            $userID = $request->user_id;
            //customVarDump_die($order_logs);


            //=========================================================================================
            //below code is for calculation of products with weight and shipping rates and default rate
            //=========================================================================================
            $user_id       = $id;
            $order_log     = order_log::where(['user_id' => $userID])->where(['order_type' => 'back-order'])->get();
            $totall        = [];
            $dst           = SiteSetting::where(['key' => 'default_sales_tax'])->first();

            $product_meta                            = [];
            $category_sales_tax                      = [];
            $product_cate_ids                        = [];
            $after_category_SalesTax_added           = "";
            $after_defaultSalesTax_added             = "";
            $product_groupBy_category_with_salesTax  = [];
            $product_groupBy_moreThan_two_categories = [];

            foreach($order_log as $key => $total){
                $totall[] = (($total->qty) * ($total->price));

                //--customVarDump_die($total->product_id);
                $pr = products_meta::where(['product_id' => $total->product_id ])->get();
                //$pr = products_meta::where(['product_id' => $total["product_id"] ])->get();
                $product_meta [] = $pr->toArray();

                foreach($product_meta[$key] as $k => $val){
                    if($val["product_meta"] == "categories"){

                        $cate = unserialize($val["product_meta_value"]);

                        if(count($cate) <= 1){
                            $cate_id   = $cate[0];
                            $cate_meta = Category_Meta::where(['category_id' => $cate_id])
                                                      ->where(['meta_key' => 'cate_sale_tax'])
                                                      ->get();

                            foreach ($cate_meta as $i => $data){
                                if($data["category_id"] == $cate_id){
                                    $product_cate_ids  [] = $data['category_id'];
                                    $product_ids       [] = $total['product_id'];
                                    $pp                   = ($total->qty * $total->price);
                                }
                            }

                        }else{
                            $product_groupBy_moreThan_two_categories [$total["product_id"]] = ($total->qty *  $total->price);
                            if(isset($dst)){
                                $Default_sale_tax                     = ( ( $dst->value / 100 ) * array_sum($product_groupBy_moreThan_two_categories) ) ;
                                $after_defaultSalesTax_added          = $Default_sale_tax + array_sum($product_groupBy_moreThan_two_categories);
                            }else{
                                $without_DefaultSalesTax              = array_sum($product_groupBy_moreThan_two_categories);
                            }

                        }

                    }
                }

            }


            $product_groupBy_category = [];
            $product_cate_id         = array_unique($product_cate_ids);

            foreach ($product_cate_id as $k => $v){
                $cc = Category::find($v);
                foreach ($cc->products as $kk => $vv){

                    if(in_array($vv->id,$product_ids)){
                        $order_log_product                   = order_log::where(['user_id' => $userID])
                                                                        ->where(['product_id' => $vv->id])->first();
                        $product_groupBy_category[$cc->id][$vv->id] =  ($order_log_product->qty * $order_log_product->price);
                    }

                }
            }


            //Products group by categories
            //customVarDump($product_groupBy_category);

            //Products group by more than 2 categories
            //customVarDump($product_groupBy_moreThan_two_categories);

            foreach ($product_groupBy_category as $i => $val){

                $category_SalesTax = Category_Meta::where(['category_id' => $i])
                                                  ->where(['meta_key' => 'cate_sale_tax'])
                                                  ->first();

                $category_SalesTax_value         = ( $category_SalesTax->meta_value / 100 );
                $after_category_SalesTax_added   = ( $category_SalesTax_value * array_sum($val));
                $product_groupBy_category_with_salesTax[$i][] = ( $after_category_SalesTax_added + array_sum($val));
            }

            if(isset($dst)){
                $defaultSaleTax = $after_defaultSalesTax_added;
            }else{
                $defaultSaleTax = $without_DefaultSalesTax;
            }

            // Generate sub total of all product including default or category sales tax
            $addSubTotal [] = $defaultSaleTax;

            foreach ($product_groupBy_category_with_salesTax as $key => $val){
                $addSubTotal [] = $val[0];
            }
            $addSubTotal_ = array_sum($addSubTotal);

            //$total_sum  = array_sum($totall);
            $count        = count($order_log);

            $total_weight = 0;
            foreach ($order_log as $key => $val){
                $total_weight = $total_weight + $val->weight;
            }

            $shipping = Shipping::all();

            //Default Shipping & Weight costs
            $Default_Shipping_Setting = [];
            $default_weight_price     = SiteSetting::where(['key' => 'default_weight_price'])->first();
            $default_shipping_cost    = SiteSetting::where(['key' => 'default_shipping_cost'])->first();

            $Default_Shipping_Setting ['default_weight_price']  = $default_weight_price->value;
            $Default_Shipping_Setting ['default_shipping_cost'] = $default_shipping_cost->value;

            //===============================================================================================
            //Ended below code is for calculation of products with weight and shipping rates and default rate
            //===============================================================================================

            //return view('order.manual.process' , ['selected_products' => $selected_products , 'order_logs' => $order_logs ]);

            //customVarDump_die($Default_Shipping_Setting);
            $UserDetail = \App\User::find($userID);

            return view('order.manual.process',[
                'userID'                                    => $userID,
                'UserDetail'                                => $UserDetail,
                'product_groupBy_category'                  => $product_groupBy_category,
                'product_groupBy_category_with_salesTax'    => $product_groupBy_category_with_salesTax,
                'product_groupBy_moreThan_two_categories'   => $product_groupBy_moreThan_two_categories,
                'defaultSaleTax'                            => $defaultSaleTax,
                'order_log'                                 => $order_log,
                'count'                                     => $count,
                'total_sum'                                 => $addSubTotal_,
                'total_weight'                              => $total_weight,
                'shipping'                                  => $shipping,
                'Default_Shipping_Setting'                  => $Default_Shipping_Setting,
                'selected_products'                         => $selected_products ,
                'order_logs'                                => $order_logs
            ]);

        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }

    }

    public function manual_order_creation(Request $request){

        try{

            //dd($request->all());

            $custom_message = [
                'name.required'      => 'Name is required!',
                'email.required'     => 'Email is required!',
                'address.required'   => 'Address is required!',
                'pcode.required'     => 'Postal Code is required!',
                'city.required'      => 'City is required!',
                'mobile.required'    => 'Mobile is required!',
                'state.required'     => 'State is required!',
                'd-country.required' => 'Country is required!',
            ];

            $this->validate($request, [
                'name'      => 'required',
                'email'     => 'required',
                'address'   => 'required',
                'pcode'     => 'required',
                'city'      => 'required',
                'mobile'    => 'required',
                'state'     => 'required',
                'd-country' => 'required',
            ], $custom_message);


            $totall                        = [];
            $product_detail                = [];
            $product_extra_detail          = [];
            $item_detail                   = [];

            $order_log                     = order_log::where('user_id',$request->userID)->where(['back_order_status'=>1])->get();

            foreach($order_log as $data) {

                $totall[]                  = (($data->qty) * ($data->price));
                $item_detail[]             = $data;
                $product_detail[]          = $data->product_meta;
                $product_extra_detail[]    = $data->extra_detail;

                //when order is placing detect product stock qty from product_meta
                $prod = product::find($data->product_id);

                if($prod->product_type == "bundle_product"){
                    $meta_value = products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->first();

                    //Reduce stock from inventory
                    if(!empty($prod->stock)){
                        $updated_stock_ = ( $prod->stock - $data->qty );
                        product::where(['id' => $data->product_id])->update(['stock' => $updated_stock_]);
                    }

                    if(!empty($meta_value['product_meta_value'])){
                        $updated_stock = ( $meta_value['product_meta_value'] - $data->qty );
                        products_meta::where(['product_id' => $data->product_id])
                                     ->where(['product_meta' => 'stock'])
                                     ->update(['product_meta_value' => $updated_stock]);
                    }


                    //Reduce each stock of inventory
                    $selected_products = unserialize(get_product_meta($data->product_id , 'bundle_product'));

                    foreach ($selected_products as $key => $val){

                        $selected_prod = product::find($val);
                        $selected_prod_meta = get_product_meta($val , 'stock');

                        if(!empty($selected_prod->stock)){
                            $upd_stock_ = ( $selected_prod->stock - $data->qty );
                            product::where(['id' => $val])->update(['stock' => $upd_stock_]);
                        }

                        if(!empty($selected_prod_meta['product_meta_value'])){
                            $upd_stock = ( $selected_prod_meta['product_meta_value'] - $data->qty );
                            products_meta::where(['product_id' => $val ])
                                         ->where(['product_meta' => 'stock'])
                                         ->update(['product_meta_value' => $upd_stock]);
                        }

                    }


                }else{
                    $meta_value = products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->first();

                    if(!empty($prod->stock)){
                        $updated_stock_ = ( $prod->stock - $data->qty );
                        product::where(['id' => $data->product_id])->update(['stock' => $updated_stock_]);
                    }
                    if(!empty($meta_value['product_meta_value'])){
                        $updated_stock = ( $meta_value['product_meta_value'] - $data->qty );
                        products_meta::where(['product_id' => $data->product_id])
                                     ->where(['product_meta' => 'stock'])
                                     ->update(['product_meta_value' => $updated_stock]);
                    }
                }


//            $meta_value = products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->first();
//
//            if(!empty($prod->stock)){
//                $updated_stock_ = ( $prod->stock - $data->qty );
//                product::where(['id' => $data->product_id])->update(['stock' => $updated_stock_]);
//            }
//
//            if(!empty($meta_value['product_meta_value'])){
//                $updated_stock = ( $meta_value['product_meta_value'] - $data->qty );
//                products_meta::where(['product_id' =>  $data->product_id])->where(['product_meta' => 'stock'])->update([
//                    'product_meta_value' => $updated_stock
//                ]);
//            }

            }
            //$product_detail['quantity']         =

            $order_detail                       = $request->all();
            $serialize_order_detail             = serialize($order_detail);
            $user                               = \App\User::find($request->userID);

            $order                              = new order();
            $order->user_id                     = $request->userID;
            $order->order_id                    = uniqid("orderID-");
            $order->user_email                  = $request->email;
            $order->order_details               = $serialize_order_detail;
            $order->user_meta                   = $user; // issue for guest user
            $order->product_detail              = serialize($product_detail);
            $order->product_extra_detail        = serialize($product_extra_detail);
            $order->subtotal                    = $request->subtotal;
            $order->subtotal_of_products        = $request->subtotal_of_products;
            $order->total_shipping              = $request->shipping_cost;
            $order->discount                    = $request->coupon_discount;
            $order->discount_type               = $request->coupon_type;
            $order->selected_country            = $request->selected_country;
            $order->cost_per_country_weight     = $request->cost_per_country_weight;
            $order->total_weight                = $request->weight_cost;
            $order->total_products_weight       = $request->total_product_weight;
            $order->tax                         = '';
            $order->status                      = 'Confirm';
            $order->selected_order_part_id      = null;
            $order->order_part_done             = null;
            $order->save();


            //when order is placing detect product stock qty from product_meta
            foreach($order_log as $data) {
                $meta_value = products_meta::where(['product_id' => $data->product_id])->where(['product_meta' => 'stock'])->first();
                if(!empty($meta_value['product_meta_value'])){
                    $updated_stock = ( $meta_value['product_meta_value'] - $data->qty );
                    products_meta::where(['product_id' => 185])->where(['product_meta' => 'stock'])->update([
                                                                                                                'product_meta_value' => $updated_stock
                                                                                                            ]);
                }
            }
            order_log::where('user_id', $request->userID)->delete();

            Mail::to($request->email)->send(new OrderStatus($order));



            $noti = array("message" => "Order created successfully!", "alert-type" => "success");
            return redirect()->route('order.manual.add')->with($noti);

        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }

    }

    public function manual_orderCheckoutDetails($id , $selected_products , $order_logs ){

        try{
            $user_id       = $id;
            $order_log     = order_log::where('user_id',$user_id)->where(['order_type' => 'back-order'])->get();
            $totall        = [];
            $dst           = SiteSetting::where(['key' => 'default_sales_tax'])->first();

            $product_meta                            = [];
            $category_sales_tax                      = [];
            $product_cate_ids                        = [];
            $after_category_SalesTax_added           = "";
            $after_defaultSalesTax_added             = "";
            $product_groupBy_category_with_salesTax  = [];
            $product_groupBy_moreThan_two_categories = [];

            foreach($order_log as $key => $total){
                $totall[] = (($total->qty) * ($total->price));

                $pr = products_meta::where(['product_id' => $total["product_id"] ])->get();
                $product_meta [] = $pr->toArray();

                foreach($product_meta[$key] as $k => $val){
                    if($val["product_meta"] == "categories"){

                        $cate = unserialize($val["product_meta_value"]);

                        //customVarDump($cate);
                        if(count($cate) <= 1){

                            $cate_id   = $cate[0];
                            $cate_meta = Category_Meta::where(['category_id' => $cate_id])
                                                      ->where(['meta_key' => 'cate_sale_tax'])
                                                      ->get();

                            foreach ($cate_meta as $i => $data){
                                if($data["category_id"] == $cate_id){
                                    $product_cate_ids  [] = $data['category_id'];
                                    $product_ids       [] = $total['product_id'];
                                    $pp                   = ($total['qty']*$total["price"]);
                                }
                            }

                        }else{
                            $product_groupBy_moreThan_two_categories [$total["product_id"]] = $total['qty'] *  $total["price"];
                            if(isset($dst)){
                                $Default_sale_tax                     = ( ( $dst->value / 100 ) * array_sum($product_groupBy_moreThan_two_categories) ) ;
                                $after_defaultSalesTax_added          = $Default_sale_tax + array_sum($product_groupBy_moreThan_two_categories);
                            }else{
                                $without_DefaultSalesTax              = array_sum($product_groupBy_moreThan_two_categories);
                            }

                        }

                    }
                }

            }


            $product_groupBy_category = [];
            $product_cate_id         = array_unique($product_cate_ids);

            foreach ($product_cate_id as $k => $v){
                $cc = Category::find($v);
                foreach ($cc->products as $kk => $vv){

                    if(in_array($vv->id,$product_ids)){
                        $order_log_product                   = order_log::where('user_id',$user_id)
                                                                        ->where(['product_id' => $vv->id])->first();
                        $product_groupBy_category[$cc->id][$vv->id] =  ($order_log_product->qty * $order_log_product->price);
                    }

                }
            }


            //Products group by categories
            //customVarDump($product_groupBy_category);

            //Products group by more than 2 categories
            //customVarDump($product_groupBy_moreThan_two_categories);

            foreach ($product_groupBy_category as $i => $val){

                $category_SalesTax = Category_Meta::where(['category_id' => $i])
                                                  ->where(['meta_key' => 'cate_sale_tax'])
                                                  ->first();

                $category_SalesTax_value         = ( $category_SalesTax->meta_value / 100 );
                $after_category_SalesTax_added   = ( $category_SalesTax_value * array_sum($val));
                $product_groupBy_category_with_salesTax[$i][] = ( $after_category_SalesTax_added + array_sum($val));
            }


            if(isset($dst)){
                $defaultSaleTax = $after_defaultSalesTax_added;
            }else{
                $defaultSaleTax = $without_DefaultSalesTax;
            }


            // Generate sub total of all product including default or category sales tax
            $addSubTotal [] = $defaultSaleTax;

            foreach ($product_groupBy_category_with_salesTax as $key => $val){
                $addSubTotal [] = $val[0];
            }
            $addSubTotal_ = array_sum($addSubTotal);

            //$total_sum  = array_sum($totall);
            $count        = count($order_log);



            $total_weight = 0;
            foreach ($order_log as $key => $val){
                $total_weight = $total_weight + $val->weight;
            }

            $shipping = Shipping::all();


            //Default Shipping & Weight costs
            $Default_Shipping_Setting = [];
            $default_weight_price     = SiteSetting::where(['key' => 'default_weight_price'])->first();
            $default_shipping_cost    = SiteSetting::where(['key' => 'default_shipping_cost'])->first();

            $Default_Shipping_Setting ['default_weight_price']  = $default_weight_price->value;
            $Default_Shipping_Setting ['default_shipping_cost'] = $default_shipping_cost->value;


            //return view('front-views.checkout',[
            return view('order.manual.process',[
                'userID'                                    => $user_id,
                'product_groupBy_category'                  => $product_groupBy_category,
                'product_groupBy_category_with_salesTax'    => $product_groupBy_category_with_salesTax,
                'product_groupBy_moreThan_two_categories'   => $product_groupBy_moreThan_two_categories,
                'defaultSaleTax'                            => $defaultSaleTax,
                'order_log'                                 => $order_log,
                'count'                                     => $count,
                'total_sum'                                 => $addSubTotal_,
                'total_weight'                              => $total_weight,
                'shipping'                                  => $shipping,
                'Default_Shipping_Setting'                  => $Default_Shipping_Setting,
                'selected_products'                         => $selected_products ,
                'order_logs'                                => $order_logs
            ]);

        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }

    }

    public function manual_destroyTempOrder(Request $request){

        try{

            $orderLog_ids = $request->order_log;

            foreach ($orderLog_ids as $key => $val){
                $Temp_order = order_log::findOrFail($val);
                $Temp_order->delete();
            }

            $products_ = product::all();
            $products  = [];

            foreach ($products_ as $key => $product){
                $metas = products_meta::where(['product_id' => $product->id])->get();
                foreach ($metas as $k => $meta){
                    if(($meta['product_meta'] == 'stock') && ( $meta['product_meta_value'] >= 1 )){
                        $products [] = $product;
                    }
                }
            }

            $users = User::all();

            return view('order.manual.add' , ['products' => $products , 'users' => $users]);

        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }
    }

}
