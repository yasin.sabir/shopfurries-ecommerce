<?php

namespace App\Http\Controllers;

use App\Category;
use App\Category_Meta;
use App\order;
use App\products_meta;
use App\Shipping;
use App\SiteSetting;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if(Auth::user()->hasRole(1)){
            $order = order::get();
        }else{
            $current_user = Auth::user()->id;
            $order = order::where(['user_id' => $current_user])->get();
        }

        return view('invoices.all',['order'=>$order]);
    }

    public function generateInvoice(Request $request){

        $id                                      = $request->order_id;
        $order                                   = order::find($id);
        $order_id                                = $order->id;
        $order_log                               = unserialize($order->order_log);
        $userID_in_order_log                     = $order_log[0]['user_id'];
        $user_detail                             = $order->user_meta;
        $order_details                           = unserialize($order->order_details);


        $totall                                  = [];
        $dst                                     = SiteSetting::where(['key' => 'default_sales_tax'])->first();
        $product_meta                            = [];
        $category_sales_tax                      = [];
        $product_cate_ids                        = [];
        $after_category_SalesTax_added           = "";
        $after_defaultSalesTax_added             = "";
        $product_groupBy_category_with_salesTax  = [];
        $product_groupBy_moreThan_two_categories = [];

        foreach($order_log as $key => $total){

            $totall[] = (($total['qty']) * ($total['price']));

            $pr = products_meta::where(['product_id' => $total["product_id"] ])->get();
            $product_meta [] = $pr->toArray();

            foreach($product_meta[$key] as $k => $val){
                if($val["product_meta"] == "categories"){

                    $cate = unserialize($val["product_meta_value"]);

                    //customVarDump($cate);
                    if(count($cate) <= 1){

                        $cate_id   = $cate[0];
                        $cate_meta = Category_Meta::where(['category_id' => $cate_id])
                                                  ->where(['meta_key' => 'cate_sale_tax'])
                                                  ->get();

                        foreach ($cate_meta as $i => $data){
                            if($data["category_id"] == $cate_id){
                                $product_cate_ids  [] = $data['category_id'];
                                $product_ids       [] = $total['product_id'];
                                $pp                   = ($total['qty']*$total["price"]);
                            }
                        }

                    }else{
                        $product_groupBy_moreThan_two_categories [$total["product_id"]] = $total['qty'] *  $total["price"];
                        if(isset($dst)){
                            $Default_sale_tax                     = ( ( $dst->value / 100 ) * array_sum($product_groupBy_moreThan_two_categories) ) ;
                            $after_defaultSalesTax_added          = $Default_sale_tax + array_sum($product_groupBy_moreThan_two_categories);
                        }else{
                            $without_DefaultSalesTax              = array_sum($product_groupBy_moreThan_two_categories);
                        }
                    }
                }
            }
        }

        $product_groupBy_category = [];
        $product_cate_id         = array_unique($product_cate_ids);

        foreach ($product_cate_id as $k => $v){
            $cc = Category::find($v);
            foreach ($cc->products as $kk => $vv){

                if(in_array($vv->id,$product_ids)){

                    foreach ($order_log as $i => $val){
                        if($val['product_id'] == $vv->id){
                            $product_groupBy_category[$cc->id][$vv->id] =  ($val['qty'] * $val['price']);
                        }
                    }
                    //$order_log_product                   = order_log::where('user_id',$userID_in_order_log[0]['user_id'])
                    //                                              ->where(['product_id' => $vv->id])->first();
                    //$product_groupBy_category[$cc->id][$vv->id] =  ($order_log_product['qty'] * $order_log_product['price']);
                }
            }
        }

        //Products group by categories
        //customVarDump($product_groupBy_category);

        //Products group by more than 2 categories
        //customVarDump($product_groupBy_moreThan_two_categories);

        foreach ($product_groupBy_category as $i => $val){

            $category_SalesTax = Category_Meta::where(['category_id' => $i])
                                              ->where(['meta_key' => 'cate_sale_tax'])
                                              ->first();

            $category_SalesTax_value         = ( $category_SalesTax->meta_value / 100 );
            $after_category_SalesTax_added   = ( $category_SalesTax_value * array_sum($val));
            $product_groupBy_category_with_salesTax[$i][] = ( $after_category_SalesTax_added + array_sum($val));
        }


        if(isset($dst)){
            $defaultSaleTax = $after_defaultSalesTax_added;
        }else{
            $defaultSaleTax = $without_DefaultSalesTax;
        }

        // Generate sub total of all product including default or category sales tax
        $addSubTotal [] = $defaultSaleTax;

        foreach ($product_groupBy_category_with_salesTax as $key => $val){
            $addSubTotal [] = $val[0];
        }
        $addSubTotal_ = array_sum($addSubTotal);

        //$total_sum  = array_sum($totall);
        $count        = count($order_log);

        $total_weight = 0;
        foreach ($order_log as $key => $val){
            $total_weight = $total_weight + $val['weight'];
        }

        $shipping = Shipping::all();

        //Default Shipping & Weight costs
        $Default_Shipping_Setting = [];
        $default_weight_price     = SiteSetting::where(['key' => 'default_weight_price'])->first();
        $default_shipping_cost    = SiteSetting::where(['key' => 'default_shipping_cost'])->first();

        $Default_Shipping_Setting ['default_weight_price']  = $default_weight_price->value;
        $Default_Shipping_Setting ['default_shipping_cost'] = $default_shipping_cost->value;

        //        customVarDump($product_groupBy_category);
        //        customVarDump($product_groupBy_category_with_salesTax);
        //        customVarDump($product_groupBy_moreThan_two_categories);
        //        customVarDump($defaultSaleTax);
        //        customVarDump($count);
        //        customVarDump($addSubTotal_);
        //        customVarDump($total_weight);
        //        customVarDump($shipping);
        //        customVarDump($Default_Shipping_Setting);
        //        die;

        $orderCal['selected_country']          = $order->selected_country;
        $orderCal['shipping_cost']             = $order->total_shipping;
        $orderCal['cost_per_country_weight']   = $order->cost_per_country_weight;
        $orderCal['weight_cost']               = $order->total_weight;
        $orderCal['total_products_weight']     = $order->total_products_weight;  //in (kg)
        $orderCal['coupon_discount']           = $order->discount;
        $orderCal['coupon_type']               = $order->discount_type;
        $orderCal['subtotal_of_products']      = $order->subtotal_of_products;
        $orderCal['subtotal']                  = $order->subtotal;

        $pdf = PDF::loadView('order.pdfinvoice',[
            'id'                       => $order_id,
            'order'                    => $order,
            'order_details'            => $order_details,
            'orderCal'                 => $orderCal,
            'status'                   => $order->status,
            'order_log'                => $order_log,
        ]);
        return $pdf->download('SH_invoice_'.$request->order_id.'.pdf');

        //          return view('order.test_order_pdf')->with([
        //            'id'                       => $order_id,
        //            'order'                    => $order,
        //            'order_details'            => $order_details,
        //            'orderCal'                 => $orderCal,
        //            'status'                   => $order->status,
        //            'order_log'                => $order_log,
        //          ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
