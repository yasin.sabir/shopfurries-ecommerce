<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Category;
use App\Category_Meta;
use App\category_setting;
use App\Faqs;
use App\Product;
use App\SiteSetting;
use App\Tag;
use App\UserMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


//        $rr = "Soft Quality";
//
//        $categoryProductMaterial_ = explode(",",$rr);
//
//        $categoryProductMaterial_ = array_map(function ($i){
//            return str_replace(" " ,"_", trim($i , " "));
//        },$categoryProductMaterial_);
//
//
//        customVarDump_die(serialize($categoryProductMaterial_));


        $categories = Category::orderBy('id','DESC')->get();
        $cateIds    = [];

        foreach ($categories as $key => $val){
            $cateIds[$key] = $val->id;
        }

        $getCategoryTop = Category::where('status', 1)->where('top', 1)->get();

        $getCategoryAll = Category::where('status' ,1)
                ->orderby('id','asc')
                ->get()
                ->groupBy('Parent');
        //customVarDump_die($getCategoryAll->toArray());

        $getTags = Tag::all();

        return view('categories.all', ['categories' => $categories , 'cateIDs' => $cateIds , 'getCategoryTop' => $getCategoryTop, 'getCategoryAll' => $getCategoryAll , 'tags' => $getTags]);
    }


    public function get_category_products(Request $request, $name, $cat_id){

        $category_id = custom_base64_decode($cat_id);
        $cat_ = Category::find($category_id);
        $products_ = [];
        $prod_ids = [];

        foreach ($cat_->products as $key => $val){
            $products_  [] = $val['id'];
            $prod_ids   [] = $val['id'];
        }

        $filter_option = ( !empty($request->select_product_feature) ? $request->select_product_feature : '' );

        if ($filter_option == 'sort') {
            $products = product::whereIn('id' , $products_)
                               -> where(['status' => 1])
                               ->where('stock' , '>' , 0)
                               ->where(['product_type' => 'product'])
                               ->orderBy('title' , 'asc')
                               ->orderBy('price' ,'asc')->paginate(12);

        } else if ($filter_option == 'l-h') {
            $products = product::whereIn('id' , $products_)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('price', 'asc')->paginate(12);

        } else if ($filter_option == 'h-l') {
            $products = product::whereIn('id' , $products_)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('price', 'desc')->paginate(12);

        } else if ($filter_option == 'a-z') {
            $products = product::whereIn('id' , $products_)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('title', 'asc')->paginate(12);

        } else if ($filter_option == 'z-a') {
            $products = product::whereIn('id' , $products_)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('title', 'desc')->paginate(12);

        } else if ($filter_option == 'new') {
            $products = product::whereIn('id' , $products_)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->latest()->paginate(12)->OnEachSide(3);

        } else {
            $products = product::whereIn('id' , $products_)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->latest()->paginate(12)->OnEachSide(3);
        }



        return view('front-views.category_products', ['products' => $products , 'ids' => $prod_ids ,'category_id' => custom_base64_encode($category_id) , 'category_name' => strtolower( str_replace(" " , "_" , $cat_->Name ) ) ]);

    }


    public function get_category_products_filter(Request $request , $name ,$id ){

        $ids            = unserialize($request->filter_product_ids);
        $category_id    = custom_base64_decode($id);
        $cat_           = Category::find($category_id);

        // Display All Products With Pagination
        $filter_option = ( !empty($request->select_product_feature) ? $request->select_product_feature : '' );

        if ($filter_option == 'sort') {
            $products = product::whereIn('id' , $ids)
                               -> where(['status' => 1])
                               ->where('stock' , '>' , 0)
                               ->where(['product_type' => 'product'])
                               ->orderBy('title' , 'asc')
                               ->orderBy('price' ,'asc')->paginate(12);

        } else if ($filter_option == 'l-h') {
            $products = product::whereIn('id' , $ids)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('price', 'asc')->paginate(12);

        } else if ($filter_option == 'h-l') {
            $products = product::whereIn('id' , $ids)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('price', 'desc')->paginate(12);

        } else if ($filter_option == 'a-z') {
            $products = product::whereIn('id' , $ids)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('title', 'asc')->paginate(12);

        } else if ($filter_option == 'z-a') {
            $products = product::whereIn('id' , $ids)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('title', 'desc')->paginate(12);

        } else if ($filter_option == 'new') {
            $products = product::whereIn('id' , $ids)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->latest()->paginate(12)->OnEachSide(3);

        } else {
            $products = product::whereIn('id' , $ids)->where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->latest()->paginate(12)->OnEachSide(3);
        }

        $prod = isset($products) && count($products) > 0 ? $products : product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->latest()->paginate(12)->OnEachSide(3);

        return view('front-views.category_products')->with(['products' => $prod , 'ids' => $ids , 'category_id' => $id , 'category_name' => strtolower( str_replace(" " , "_" , $cat_->Name ) ) ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = Category::all();
        $cateIds    = [];

        foreach ($categories as $key => $val){
            $cateIds[$key] = $val->id;
        }

        $getCategoryTop = Category::where('status', 1)->where('top', 1)->get();

        $getCategoryAll = Category::where('status' ,1)
            ->orderby('id','asc')
            ->get()
            ->groupBy('Parent');

        $getTags = Tag::all();
        $artists = Artist::all();

        return view('categories.add' , [
            'categories'      => $categories ,
            'cateIDs'         => $cateIds ,
            'getCategoryTop'  => $getCategoryTop ,
            'getCategoryAll'  => $getCategoryAll ,
            'tags'            => $getTags,
            'artists'         => $artists
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $Cate_Detail = Category::where(['Name' => $request->category ])
            ->where(['Parent' => $request->parent_category])
            ->get();

        $custom_message = [
            'category.required'                 => 'Category name is required!',
            'artists.required'                  => 'Artist is required!',
            'additional_req.required'           => 'Additional requirement are required!',
            'category_prod_material.required'   => "Material field is required!",
            'category_prod_size.required'       => 'Size field is required!',
            'category_price.required'           => 'Price is required!',
            //'cate_sale_tax.regex'             => 'Format is invalid!',
            //'cate_sale_tax.required'          => 'Category sales tax is required!'
        ];

        $this->validate($request, [
            'category'                          => 'required',
            'artists'                           => 'required',
            'additional_req'                    => 'required',
            'category_prod_material'            => 'required',
            'category_prod_size'                => 'required',
            'category_price'                    => 'required',
            //'cate_sale_tax'                   => 'regex:/^(\d*\.)?\d+%$/i'
        ], $custom_message);

        try{

            if(count($Cate_Detail) == 0){

                $category = new Category();
                $category->Alias    = strtolower(str_replace(" ", "-", $request->category));
                $category->Name     = $request->category;
                $category->Parent   = isset($request->parent_category) ? $request->parent_category : 'root';

                if(!$request->parent_category){
                    $category->Top      = intval(1);
                }else{
                    $category->Top      = intval(0);
                }

                $category->Status         = $request->status;
                $category->Additional_Req = $request->additional_req;
                $category->save();

                $directory = "public/upload/categories/category_".$category->id;
                Storage::makeDirectory($directory);

                Category::where(['id' =>$category->id ])->update([
                    'Image' => $request->file('thumbnail_image') ? $request->file('thumbnail_image')->store('upload/categories/category_'.$category->id.'/thumbnail', 'public') : 'null',
                ]);


                $category_poster_images = [];
                $poster_images          = $request->file('category_poster_pics');

                $category_material_pics = [];
                $actual_material_images = $request->file('category_actual_material_pics');

                $category_reason_titles  = [];
                $reason_titles           = $request->category_reason_title;

                $category_reason_details = [];
                $reason_details          = $request->category_reason_detail;

                $category_feature_titles = [];
                $feature_titles          = $request->category_feature_title;

                $category_feature_details = [];
                $feature_details          = $request->category_feature_detail;

                if($poster_images){
                    foreach ($poster_images as $key => $val){
                        $category_poster_images[$key] = $val->store('upload/categories/category_'.$category->id.'/posters-pics/', 'public');
                        $val->store('upload/categories/category_'.$category->id.'/posters-pics/', 'public');
                    }
                }

                if($actual_material_images){
                    foreach ($actual_material_images as $key => $val){
                        $category_material_pics[$key] =  $val->store('upload/categories/category_'.$category->id.'/actual-material-pics/', 'public');
                        $val->store('upload/categories/category_'.$category->id.'/actual-material-pics/', 'public');
                    }
                }

                if($reason_titles){
                    foreach ($reason_titles as $key => $val){
                        $category_reason_titles[$key] = (!empty($val) ? $val : null );
                    }
                }

                if($reason_details){
                    foreach ($reason_details as $key => $val){
                        $category_reason_details[$key] = (!empty($val) ? $val : null );
                    }
                }

                if($feature_titles){
                    foreach ($feature_titles as $key => $val){
                        $category_feature_titles[$key] = (!empty($val) ? $val : null );
                    }
                }

                if($feature_details){
                    foreach ($feature_details as $key => $val){
                        $category_feature_details[$key] = (!empty($val) ? $val : null );
                    }
                }


                $categoryProductMaterial_ = explode(",",$request->category_prod_material);
                $categoryProductSizes_    = explode(",",$request->category_prod_size);

                $categoryProductMaterial_ = array_map(function ($i){
                    return str_replace(" " ,"_", trim($i , " "));
                },$categoryProductMaterial_);

                $categoryProductSizes_ = array_map(function ($i){
                    return str_replace(" " ,"_", trim($i , " "));
                },$categoryProductSizes_);

                $category_metas = [
                    ['category_id' => $category->id , 'meta_key' => 'artist'                        , 'meta_value' => ( !empty($request->artists) ? $request->artists : null )],
                    ['category_id' => $category->id , 'meta_key' => 'price'                         , 'meta_value' => ( !empty($request->category_price) ? $request->category_price : null )],
                    ['category_id' => $category->id , 'meta_key' => 'tags'                          , 'meta_value' => ( !empty($request->category_tags) ? serialize($request->category_tags) : null ) ],
                    ['category_id' => $category->id , 'meta_key' => 'material'                      , 'meta_value' => ( !empty($categoryProductMaterial_) ? serialize($categoryProductMaterial_) : null )],
                    ['category_id' => $category->id , 'meta_key' => 'sizes'                         , 'meta_value' => ( !empty($categoryProductSizes_) ? serialize($categoryProductSizes_) : null) ],
                    ['category_id' => $category->id , 'meta_key' => 'cate_sale_tax'                 , 'meta_value' => ( !empty($request->cate_sale_tax) ? $request->cate_sale_tax : null )],
                    ['category_id' => $category->id , 'meta_key' => 'poster_images'                 , 'meta_value' => ( $category_poster_images ? serialize($category_poster_images) : serialize(array()) )],
                    ['category_id' => $category->id , 'meta_key' => 'actual_material_images'        , 'meta_value' => ( $category_material_pics ? serialize($category_material_pics) : serialize(array()) )],
                    ['category_id' => $category->id , 'meta_key' => 'category_reason_title'         , 'meta_value' => ( !empty($category_reason_titles) ?  serialize($category_reason_titles) : null )],
                    ['category_id' => $category->id , 'meta_key' => 'category_reason_detail'        , 'meta_value' => ( !empty($category_reason_details) ? serialize($category_reason_details) : null )],
                    ['category_id' => $category->id , 'meta_key' => 'category_prod_feature_title'   , 'meta_value' => ( !empty($category_feature_titles) ? serialize($category_feature_titles) : null )],
                    ['category_id' => $category->id , 'meta_key' => 'category_prod_feature_detail'  , 'meta_value' => ( !empty($category_feature_details) ? serialize($category_feature_details) : null )],
                ];

                foreach ($category_metas as $k => $v) {
                    Category_Meta::updateOrInsert(['category_id' => $v['category_id'], 'meta_key' => $v['meta_key']], $v);
                }


                //                $category_metas = [
                //                    "price"                  => ( !empty($request->category_price) ? $request->category_price : null ),
                //                    "tags"                   => ( !empty($request->category_tags) ? serialize($request->category_tags) : null ) ,
                //                    "poster_images"          => ( $request->file('category_poster_pics') ? serialize($category_poster_images) : serialize(array()) ) ,
                //                    "actual_material_images" => ( $request->file('category_actual_material_pics') ? serialize($category_material_pics) : serialize(array()) ) ,
                //                    "category_reason_title"  => ( !empty($category_reason_titles) ?  serialize($category_reason_titles) : null ),
                //                    "category_reason_detail" => ( !empty($category_reason_details) ? serialize($category_reason_details) : null ),
                //                    "category_prod_features" => ( !empty($request->category_prod_features) ? $request->category_prod_features : null )
                //                ];
                //
                //                foreach ($category_metas as $key => $val){
                //                    $category_meta = new Category_Meta();
                //                    $category_meta->category_id = $category->id;
                //                    $category_meta->meta_key    = $key;
                //                    $category_meta->meta_value  = $val;
                //                    $category_meta->save();
                //                }


                $noti = array("message" => "Category created successfully!", "alert-type" => "success");
                return redirect()->route('category.list')->with($noti);
            }

            $noti = array("message" => "Category {$request->category} already exist!!", "alert-type" => "error");
            return redirect()->route('category.list')->with($noti);

        }catch(\Exception $ex){
            $noti = array("message" =>$ex->getMessage(), "alert-type" => "danger");
            return redirect()->route('category.list')->with($noti);
        }


    }

    function imageResizing($id, $type , $slug , $file_key , $sizes , $other_folders = false , $request){

        $savePath = [];

        if(!empty($sizes) && count($sizes) > 0){

            foreach ($sizes as $key => $val){

                $folderName   = $val."x".$val;
                $slugFolder  = $slug."_".$id;

                if($other_folders){
                    $otherFolders = $other_folders;
                    $directory    = "public/upload/".$type."/".$slugFolder."/".$otherFolders."/".$folderName."/";
                }else{
                    $directory    = "public/upload/".$type."/".$slugFolder."/".$folderName."/";
                }
                Storage::makeDirectory($directory);
            }

            if($request->hasFile($file_key)){

                //For Multiple Images
                if(is_array($request->file($file_key))){

                    $images     = $request->file($file_key);
                    foreach ($images as $k => $v){

                        $filename  = time() . '.' . $v->getClientOriginalExtension();

                        foreach ($sizes as $key => $val){
                            $folderName     = $val."x"."$val";
                            $folderSlug    = $slug."_".$id;

                            if($other_folders){
                                $otherFolders   = $other_folders;
                                $path           = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/').$filename;
                                $savePath []    = 'upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/'.$filename;
                            }else{
                                $path           = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$folderName.'/').$filename;
                                $savePath []    = 'upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/'.$filename;
                            }

                            $img            = \Intervention\Image\Facades\Image::make( $v->getRealPath());
                            $img->resize($val, $val, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
                            $img->save($path);
                        }
                    }


                }else{

                    //For Single Image
                    $image     = $request->file($file_key);
                    $filename  = time() . '.' . $image->getClientOriginalExtension();

                    foreach ($sizes as $key => $val){
                        $folderName     = $val."x"."$val";
                        $folderSlug    = $slug."_".$id;

                        if($other_folders){
                            $otherFolders       = $other_folders;
                            $path               = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/').$filename;
                            $savePath [$val]    = 'upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/'.$filename;
                        }else{
                            $path               = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$folderName.'/').$filename;
                            $savePath [$val]    = 'upload/'.$type.'/'.$folderSlug.'/'.$folderName.'/'.$filename;
                        }
                        $img                = \Intervention\Image\Facades\Image::make( $image->getRealPath());
                        $img->resize($val, $val, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        $img->save($path);

                    }
                }

            }


        }
        return $savePath;
    }

    function old_imageResizing($id, $type , $slug , $file_key , $sizes , $other_folders , $request ){
        if(!empty($sizes) && count($sizes) > 0){
            foreach ($sizes as $key => $val){
                $folderName     = $val."x".$val;
                $eventFolder    = $slug."_".$id;
                $otherFolders   = $other_folders;
                $directory      = "public/upload/".$type."/".$eventFolder."/".$otherFolders."/".$folderName."/";
                Storage::makeDirectory($directory);
            }
            if($request->hasFile($file_key)){
                $image     = $request->file($file_key);
                $filename  = time() . '.' . $image->getClientOriginalExtension();
                foreach ($sizes as $key => $val){
                    $folderName     = $val."x"."$val";
                    $eventFolder    = $slug."_".$id;
                    $otherFolders   = $other_folders;
                    $path           = storage_path('app/public/upload/'.$type.'/'.$eventFolder.'/'.$otherFolders.'/'.$folderName.'/').$filename;
                    $img            = Image::make($image->getRealPath());
                    $img->resize($val, $val, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $img->save($path);
                    Category::where(['id' => $id ])->update([ 'Image' => $path ]);
                }

            }
        }

    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$categories = Category::all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $category_details = Category::where(['id' => decrypt( $id )])->first();

            if( ! empty($category_details) ){

                $cate_metas    = Category_Meta::where(['category_id' => $category_details->id])->get();

                $category_metas = [];
                foreach ($cate_metas as $key => $val){
                    $category_metas[$val->meta_key] = $val->meta_value;
                }

                //        foreach(unserialize($category_metas['actual_material_images']) as $key => $val){
                //            customVarDump(json_encode($val));
                //        }
                //        die;
                //        customVarDump_die($category_metas);
                //        customVarDump_die(json_encode(unserialize($category_metas['actual_material_images'])));

                //        customVarDump(unserialize($category_metas['poster_images']));
                //        $images = unserialize($category_metas['poster_images']);
                //        $img = "upload/category/men-coats_61/posters-pics//aSaonf5afUqWUuPc6Q5YzqKZUnY2ha2wFlK5VnNb.png";
                //        $data_remove = array_search($img,$images);
                //        unset($images[$data_remove]);
                //        customVarDump_die(serialize($images));

                $categories = Category::all();
                $tags       = Tag::all();
                $artists    = Artist::all();

                return view('categories.edit', [
                    'categories'            => $categories ,
                    'category_details'      => $category_details,
                    'category_metas'        => $category_metas,
                    'category_id'           => decrypt( $id ),
                    'tags'                  => $tags,
                    'artists'               => $artists
                ] );

            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }

        return view( 'errors.404' );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {

//        if(!empty($request->category_tags)){
//            customVarDump_die(serialize($request->category_tags));
//        }else{
//            var_dump("null");
//        }
//
//        die;

        $Category   = Category::where(['id' => $id ])->first();
        $Cate_Metas = Category_Meta::where(['category_id' => $id])->get();

        $category_prev_image_path = $Category->Image;

        $custom_message = [
            'category.required'         => 'Category name is required!',
            'artists.required'          => 'Artist is required!',
            'additional_req.required'   => 'Additional requirement are required!',
             'category_price.required'  => 'Price is required!',
        ];

        $this->validate($request, [
            'category'          => 'required',
            'artists'           => 'required',
            'category_price'    => 'required',
            'additional_req' => 'required'
        ], $custom_message);

        try{

            $metas_array = [];
            foreach ($Cate_Metas as $key => $val){
                $metas_array[$val->meta_key] = $val->meta_value;
            }

            $category = Category::where(['id' => $id])->update([
                'Name'      => $request->category,
                'Alias'     => strtolower(str_replace(" ", "-", $request->category)),
                'Image'     => $request->file('thumbnail_image') ? $request->file('thumbnail_image')->store('upload/category/'.strtolower(str_replace(" ","-",$request->category))."_".$id.'/thumbnail', 'public') : $category_prev_image_path,
                'Parent'    => $request->parent_category,
                'Top'       => 1,
                'Status'    => $request->status,
            ]);


            $category_poster_images = [];
            $poster_images          = $request->file('category_poster_pics');

            $category_material_pics = [];
            $actual_material_images = $request->file('category_actual_material_pics');

            $category_reason_titles  = [];
            $reason_titles           = $request->category_reason_title;

            $category_reason_details = [];
            $reason_details          = $request->category_reason_detail;

            $category_feature_titles = [];
            $feature_titles          = $request->category_feature_title;

            $category_feature_details = [];
            $feature_details          = $request->category_feature_detail;

            if($poster_images){
                foreach ($poster_images as $key => $val){
                    $category_poster_images[$key] = $val->store('upload/category/'.strtolower(str_replace(" ","-",$request->category))."_".$id.'/posters-pics/', 'public');
                }
            }
            $category_poster_images = array_merge($category_poster_images , unserialize($metas_array['poster_images']) );
            //customVarDump_die($category_poster_images);

            if($actual_material_images){
                foreach ($actual_material_images as $key => $val){
                    $category_material_pics[$key] =  $val->store('upload/category/'.strtolower(str_replace(" ","-",$request->category))."_".$id.'/actual-material-pics/', 'public');
                }
            }
            $category_material_pics = array_merge($category_material_pics , unserialize($metas_array['actual_material_images']) );


            if($reason_titles){
                foreach ($reason_titles as $key => $val){
                    $category_reason_titles[$key] = (!empty($val) ? $val : null );
                }
            }

            if($reason_details){
                foreach ($reason_details as $key => $val){
                    $category_reason_details[$key] = (!empty($val) ? $val : null );
                }
            }

            if($feature_titles){
                foreach ($feature_titles as $key => $val){
                    $category_feature_titles[$key] = (!empty($val) ? $val : null );
                }
            }

            if($feature_details){
                foreach ($feature_details as $key => $val){
                    $category_feature_details[$key] = (!empty($val) ? $val : null );
                }
            }

            $categoryProductMaterial_ = explode(",",$request->category_prod_material);
            $categoryProductSizes_    = explode(",",$request->category_prod_size);

            $categoryProductMaterial_ = array_map(function ($i){
                return str_replace(" " ,"_", trim($i , " "));
            },$categoryProductMaterial_);

            $categoryProductSizes_ = array_map(function ($i){
                return str_replace(" " ,"_", trim($i , " "));
            },$categoryProductSizes_);


            $category_metas = [
                ['category_id' => $id , 'meta_key' => 'artist'                      , 'meta_value' => ( !empty($request->artists) ? $request->artists : null )],
                ['category_id' => $id , 'meta_key' => 'price'                       , 'meta_value' => ( !empty($request->category_price) ? $request->category_price : null )],
                ['category_id' => $id , 'meta_key' => 'tags'                        , 'meta_value' => ( !empty($request->category_tags) ? serialize($request->category_tags) : null ) ],
                ['category_id' => $id , 'meta_key' => 'material'                    , 'meta_value' => ( !empty($categoryProductMaterial_) ? serialize($categoryProductMaterial_) : null )],
                ['category_id' => $id , 'meta_key' => 'sizes'                       , 'meta_value' => ( !empty($categoryProductSizes_) ? serialize($categoryProductSizes_) : null) ],
                ['category_id' => $id , 'meta_key' => 'cate_sale_tax'               , 'meta_value' => ( !empty($request->cate_sale_tax) ? $request->cate_sale_tax : null )],
                ['category_id' => $id , 'meta_key' => 'poster_images'               , 'meta_value' => ( $category_poster_images ? serialize($category_poster_images) : serialize(array()) )],
                ['category_id' => $id , 'meta_key' => 'actual_material_images'      , 'meta_value' => ( $category_material_pics ? serialize($category_material_pics) : serialize(array()) )],
                ['category_id' => $id , 'meta_key' => 'category_reason_title'       , 'meta_value' => ( !empty($category_reason_titles) ?  serialize($category_reason_titles) : null )],
                ['category_id' => $id , 'meta_key' => 'category_reason_detail'      , 'meta_value' => ( !empty($category_reason_details) ? serialize($category_reason_details) : null )],
                ['category_id' => $id , 'meta_key' => 'category_prod_feature_title' , 'meta_value' => ( !empty($category_feature_titles) ? serialize($category_feature_titles) : null )],
                ['category_id' => $id , 'meta_key' => 'category_prod_feature_detail' , 'meta_value' => ( !empty($category_feature_details) ? serialize($category_feature_details) : null )],
            ];

            foreach ($category_metas as $k => $v) {
                Category_Meta::updateOrInsert(['category_id' => $v['category_id'], 'meta_key' => $v['meta_key']], $v);
            }


            //            $category_metas = [
            //                "price"                  => ( !empty($request->category_price) ? $request->category_price : null ),
            //                "tags"                   => ( !empty($request->category_tags) ? serialize($request->category_tags) : null ) ,
            //                "poster_images"          => ( $category_poster_images ? serialize($category_poster_images) : serialize(array()) ) ,
            //                "actual_material_images" => ( $category_material_pics ? serialize($category_material_pics) : serialize(array()) ) ,
            //                "category_reason_title"  => ( !empty($category_reason_titles) ?  serialize($category_reason_titles) : null ),
            //                "category_reason_detail" => ( !empty($category_reason_details) ? serialize($category_reason_details) : null ),
            //                "category_prod_features" => ( !empty($request->category_prod_features) ? $request->category_prod_features : null )
            //            ];

            //            foreach ($category_metas as $key => $val){
            //
            //                $cate_ = DB::table('category_meta')->select('id')
            //                            ->where(['category_id' => $id])
            //                            ->where(['meta_key' => $key])->first();
            //
            //                Category_Meta::where(['id' => $cate_->id])->update([
            //                    'meta_key' => $key ,
            //                    'meta_value' => $val
            //                ]);
            //
            //            }

            $noti = array("message" => "Category updated successfully!", "alert-type" => "success");
            return Redirect::back()->with($noti);


        }catch(\Exception $ex){
            $noti = array("message" =>$ex->getMessage(), "alert-type" => "error");
            return redirect()->route('category.list')->with($noti);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function delete_poster_images(Request $request , $id){

        $meta_key = "poster_images";
        $category_poster_images = $request->key;

        $result = Category_Meta::where(['category_id' => $id])->where(['meta_key' => $meta_key])->first();
        $images = unserialize($result->meta_value);
        $data_remove = array_search($category_poster_images,$images);
        unset($images[$data_remove]);

        Category_Meta::where(['id' => $result->id])->update(['meta_value' => serialize($images)]);

        return "true";
    }

    public function delete_actual_material_images(Request $request , $id){

        $meta_key = "actual_material_images";
        $category_actual_material_image = $request->key;

        $result = Category_Meta::where(['category_id' => $id])->where(['meta_key' => $meta_key])->first();
        $images = unserialize($result->meta_value);
        $data_remove = array_search($category_actual_material_image,$images);
        unset($images[$data_remove]);

        Category_Meta::where(['id' => $result->id])->update(['meta_value' => serialize($images)]);

        return "true";
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        Category_Meta::where(['category_id'=>$id])->delete();

        $noti = array("message" => "Category deleted successfully", "alert-type" => "success");
        return redirect()->route('category.list')->with($noti);
    }


    public function destroyAllCategories(Request $request){

        $ids = json_decode($request->delete_ids);

        foreach ($ids as $key => $id){
            $category = Category::findOrFail($id);
            $category->delete();
            Category_Meta::where(['category_id'=>$id])->delete();
        }

        $noti = array("message" => "Selected categories deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }



    public function SettingView(){

        $cst = DB::table('category_setting')->latest('id')->first();

        $reasons = [];
        $features = [];

        if(!empty($cst)){

            $r_title  = unserialize($cst->reason_titles);
            $r_detail = unserialize($cst->reason_descriptions);
            if( !empty($r_title) ){
                foreach ($r_title as $key => $val){
                    $reasons[$key]['title_'.$key] = $val;
                    $reasons[$key]['detail_'.$key] = $r_detail[$key];
                }
            }


            $f_title  = unserialize($cst->feature_titles);
            $f_detail = unserialize($cst->feature_descriptions);
            if( !empty($f_title) ){
                foreach ($f_title as $key => $val){
                    $features[$key]['title_'.$key] = $val;
                    $features[$key]['detail_'.$key] = $f_detail[$key];
                }
            }

        }

        return view('categories.setting.view-setting' , [
            'cst' => $cst ,
            'reasons'  => $reasons,
            'features' => $features
        ]);
    }

    public function SettingView_create(){
        return view('categories.setting.add-setting');
    }

    public function SettingView_store(Request $request){

        $category_setting = new category_setting();

        $category_poster_images = [];
        $poster_images          = $request->file('category_poster_pics');

        $category_material_pics = [];
        $actual_material_images = $request->file('category_actual_material_pics');

        $category_reason_titles  = [];
        $reason_titles           = $request->category_reason_title;

        $category_reason_details = [];
        $reason_details          = $request->category_reason_detail;

        $category_feature_titles = [];
        $feature_titles          = $request->category_feature_title;

        $category_feature_details = [];
        $feature_details          = $request->category_feature_detail;

        if($poster_images){
            foreach ($poster_images as $key => $val){
                $category_poster_images[$key] = $val->store('upload/category_setting/posters-pics', 'public');
                $val->store('upload/category_setting/posters-pics', 'public');
            }
        }

        if($actual_material_images){
            foreach ($actual_material_images as $key => $val){
                $category_material_pics[$key] =  $val->store('upload/category_setting/actual-material-pics', 'public');
                $val->store('upload/category_setting/actual-material-pics', 'public');
            }
        }

        if($reason_titles){
            foreach ($reason_titles as $key => $val){
                $category_reason_titles[$key] = (!empty($val) ? $val : null );
            }
        }

        if($reason_details){
            foreach ($reason_details as $key => $val){
                $category_reason_details[$key] = (!empty($val) ? $val : null );
            }
        }

        if($feature_titles){
            foreach ($feature_titles as $key => $val){
                $category_feature_titles[$key] = (!empty($val) ? $val : null );
            }
        }

        if($feature_details){
            foreach ($feature_details as $key => $val){
                $category_feature_details[$key] = (!empty($val) ? $val : null );
            }
        }


        $category_setting->reason_titles             = ( !empty($category_reason_titles) ? serialize($category_reason_titles) : null );
        $category_setting->reason_descriptions       = ( !empty($category_reason_details) ? serialize($category_reason_details) : null );
        $category_setting->feature_titles            = ( !empty($category_feature_titles) ? serialize($category_feature_titles) : null );
        $category_setting->feature_descriptions      = ( !empty($category_feature_titles) ? serialize($category_feature_titles) : null );
        $category_setting->posters                   = ( $category_poster_images ? serialize($category_poster_images) : serialize(array()) );
        $category_setting->actual_material_pics      = ( $category_material_pics ? serialize($category_material_pics) : serialize(array()) );
        $category_setting->save();

        $noti = array("message" => "Category Setting created successfully!", "alert-type" => "success");
        return redirect()->route('category.setting')->with($noti);

    }

    public function SettingView_edit($id){

        try {

            $cst = category_setting::find( decrypt( $id ));

            if ( ! empty( $cst ) ) {
                return view('categories.setting.edit-setting' , ['cst' => $cst]);
            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }
        return view( 'errors.404' );
    }

    public function SettingView_Update(Request $request , $id){

        $cst = category_setting::find($id);

        $prev_poster_img = $cst->posters;
        $prev_actual_material_img = $cst->actual_material_pics;


        $category_poster_images = [];
        $poster_images          = $request->file('category_poster_pics');

        $category_material_pics = [];
        $actual_material_images = $request->file('category_actual_material_pics');

        $category_reason_titles  = [];
        $reason_titles           = $request->category_reason_title;

        $category_reason_details = [];
        $reason_details          = $request->category_reason_detail;

        $category_feature_titles = [];
        $feature_titles          = $request->category_feature_title;

        $category_feature_details = [];
        $feature_details          = $request->category_feature_detail;

        if($poster_images){
            foreach ($poster_images as $key => $val){
                $category_poster_images[$key] = $val->store('upload/category_setting/posters-pics/', 'public');
                $val->store('upload/category_setting/posters-pics/', 'public');
            }
        }

        if($actual_material_images){
            foreach ($actual_material_images as $key => $val){
                $category_material_pics[$key] =  $val->store('upload/category_setting/actual-material-pics/', 'public');
                $val->store('upload/category_setting/actual-material-pics/', 'public');
            }
        }

        if($reason_titles){
            foreach ($reason_titles as $key => $val){
                $category_reason_titles[$key] = (!empty($val) ? $val : null );
            }
        }

        $category_poster_images = array_merge($category_poster_images , unserialize($prev_poster_img) );

        if($reason_details){
            foreach ($reason_details as $key => $val){
                $category_reason_details[$key] = (!empty($val) ? $val : null );
            }
        }
        $category_material_pics = array_merge($category_material_pics , unserialize($prev_actual_material_img) );


        if($feature_titles){
            foreach ($feature_titles as $key => $val){
                $category_feature_titles[$key] = (!empty($val) ? $val : null );
            }
        }

        if($feature_details){
            foreach ($feature_details as $key => $val){
                $category_feature_details[$key] = (!empty($val) ? $val : null );
            }
        }


        $category_setting = category_setting::where(['id' => $id])->update([
            'reason_titles'                 => ( !empty($category_reason_titles) ? serialize($category_reason_titles) : null ),
            'reason_descriptions'           => ( !empty($category_reason_details) ? serialize($category_reason_details) : null ),
            'feature_titles'                => ( !empty($category_feature_titles) ? serialize($category_feature_titles) : null ) ,
            'feature_descriptions'          => ( !empty($category_feature_titles) ? serialize($category_feature_titles) : null ),
            'posters'                       => ( $category_poster_images ? serialize($category_poster_images) : serialize(array()) ),
            'actual_material_pics'          => ( $category_material_pics ? serialize($category_material_pics) : serialize(array()) ),
        ]);


        $noti = array("message" => "Category Setting updated successfully!", "alert-type" => "success");
        return redirect()->route('category.setting')->with($noti);

    }

    public function SettingStatus_update(Request $request , $id){

        if( isset($request->status) && $request->status == "on" ){

            category_setting::find($id)->update(['status' =>  $request->status ]);
            $msg = "Status is on";
            return response()->json(array(['msg'=> $msg , 'val' => 'checked']), 200);

        }else if(isset($request->status) && $request->status == "off"){

            category_setting::find($id)->update(['status' =>  $request->status ]);
            $msg = "Status is off";
            return response()->json(array(['msg'=> $msg , 'val' => 'unchecked']), 200);

        }else{

        }

    }

    public function delete_cateST_poster_images(Request $request , $id){

        $category_poster_images = $request->key;
        $result             = category_setting::find($id);
        $images             = unserialize($result->posters);
        $data_remove        = array_search($category_poster_images,$images);
        unset($images[$data_remove]);
        category_setting::where(['id' => $result->id])->update(['posters' => serialize($images)]);
        return "true";
    }

    public function delete_cateST_actual_material_images(Request $request , $id){

        $category_actual_material_image = $request->key;
        $result             = category_setting::find($id);
        $images             = unserialize($result->actual_material_pics);

        $data_remove        = array_search($category_actual_material_image,$images);
        unset($images[$data_remove]);
        category_setting::where(['id' => $result->id])->update(['actual_material_pics' => serialize($images)]);
        return "true";
    }


}
