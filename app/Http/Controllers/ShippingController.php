<?php

namespace App\Http\Controllers;

use App\Shipping;
use App\SiteSetting;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    public function allshippinglist()
    {
        $shipping = Shipping::get();
        return view('shipping.all-shipping-price',['shipping'=>$shipping]);
    }

    public function addshipping()
    {
        return view('shipping.add');
    }

    public function store(Request $request)
    {
        $custom_msg = [
            'shipping_country.required' => 'Shipping Country is required!',
            'shipping_price.required' => 'Shipping Price  is required!'
        ];

        $this->validate($request,[
             'shipping_country' => 'required',
             'shipping_price' => 'required'
        ],$custom_msg);

        $shipping = new Shipping();

        $shipping->country                  = $request->shipping_country;
        $shipping->price                    = $request->shipping_price;
        $shipping->weight                   = $request->shipping_weight;
        $shipping->shipping_cost_country    = $request->shipping_cost_country;

        $shipping->save();

        $noti = array("message" => "Shipping created successfully!", "alert-type" => "success");
        return redirect()->route('shipping.alllist')->with($noti);
    }
    public function destroy($id)
    {

        shipping::where('id',$id)->delete();
        $noti = array("message" => "Shipping Country deleted successfully", "alert-type" => "success");

        return redirect()->route('shipping.alllist')->with($noti);

    }

    public function edit($id)
    {
        try {

            $shipping = Shipping::find( decrypt( $id ));

            if ( ! empty( $shipping ) ) {
                return view('shipping.edit',['shipping' => $shipping ]);
            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }

        return view( 'errors.404' );

    }
    public function update(Request $request, $id)
    {
        $custom_msg = [
            'shipping_country.required' => 'Shipping Country is required!',
            'shipping_price.required' => 'Shipping Price  is required!'
        ];

        $this->validate($request,[
            'shipping_country' => 'required',
            'shipping_price' => 'required'
        ],$custom_msg);

        Shipping::where(['id' => $id])->update([
            'country'                  => $request->shipping_country,
            'weight'                   => $request->shipping_weight,
            'price'                    => $request->shipping_price,
            'shipping_cost_country'    => $request->shipping_cost_country,
        ]);

        $noti = array("message" => "Shipping updated successfully!", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }




    public function defaultAddShipping(){
        return view('shipping.default.add');
    }

    public function defaultStoreShipping(Request $request){

        $custom_msg = [
            'default_weight_price.required'  => 'Default weight is required!',
            'default_shipping_cost.required' => 'Default shipping is required!',
            'default_weight_price.regex'     => 'Format is invalid!',
            'default_shipping_cost.regex'    => 'Format is invalid!',
        ];

        $this->validate($request,[
            'default_weight_price' =>  'required|regex:/^[\w]+(.[\w]+)*$/i',
            'default_shipping_cost' => 'required|regex:/^[\w]+(.[\w]+)*$/i'
        ],$custom_msg);


        $data1 = SiteSetting::where(['key' => 'default_weight_price'])->first();
        $data2 = SiteSetting::where(['key' => 'default_shipping_cost'])->first();

        if(isset($data1)){
            SiteSetting::where(['key' => 'default_weight_price'])->update([
                'value' => $request->default_weight_price
            ]);
        }else{
            SiteSetting::updateOrInsert(['key' => 'default_weight_price' , 'value' => $request->default_weight_price]);
        }

        if(isset($data2)){
            SiteSetting::where(['key' => 'default_shipping_cost'])->update([
                'value' => $request->default_shipping_cost
            ]);
        }else{
            SiteSetting::updateOrInsert(['key' => 'default_shipping_cost' , 'value' => $request->default_shipping_cost]);
        }

        //        if(isset($request->default_weight_price) && isset($request->default_shipping_cost)){
        //
        //        }

        $noti = array("message" => "Default Shipping Setting add successfully!", "alert-type" => "success");
        return redirect()->back()->with($noti);

    }

}
