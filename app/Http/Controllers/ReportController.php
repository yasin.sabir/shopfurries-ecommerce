<?php

namespace App\Http\Controllers;

use App\Exports\ExportOrders;
use App\Exports\Reports\CountrySales;
use App\Exports\Reports\EachSales;
use App\Exports\Reports\TotalSales;
use App\order;
use App\Product;
use Box\Spout\Writer\Style\StyleBuilder;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use function MongoDB\BSON\toJSON;
use Rap2hpoutre\FastExcel\FastExcel;

class ReportController extends Controller
{
    //===== Analytics Each Sales
    public function eachSales_list( Request $request )
    {

        $all_product       = product::all();
        $orders            = order::all();
        $product_ids       = [];
        $product           = [];
        $avail_product     = [];
        $products          = [];
        $selected_country  = [];
        $dropdown_products = [];
        $total_quantity    = 0;
        $total_price       = 0;


        foreach ( $all_product as $key => $val ) {
            $product_ids [] = $val->id;
        }

        if ( isset( $request->product_id ) && $request->product_id != "_0" ) {

            $product_id = $request->product_id;
            $num        = 1;

            foreach ( $orders as $key => $val ) {

                $product_details = unserialize( $val->product_detail );

                foreach ( $product_details as $k => $v ) {

                    $rr = unserialize( $v );
                    if ( $product_id == $rr['id'] ) {
                        $product [ $num ]['id']         = $rr['id'];
                        $product [ $num ]['title']      = $rr['title'];
                        $product [ $num ]['quantity']   = $rr['quantity'];
                        $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                        $product [ $num ]['created_at'] = $val->created_at;
                        $total_price                    = $total_price + ( $rr['quantity'] * $rr['price'] );
                        $total_quantity                 = $total_quantity + $rr['quantity'];
                        $num ++;
                    }

                    if ( in_array( $rr['id'], $product_ids ) ) {
                        $avail_product [ $num ]['id']    = $rr['id'];
                        $avail_product [ $num ]['title'] = $rr['title'];
                        $num ++;
                    }
                }
            }

            $dropdown_products = array_unique( $avail_product, SORT_REGULAR );

        } else {

            $num = 1;
            foreach ( $orders as $key => $val ) {
                $product_details = unserialize( $val->product_detail );
                foreach ( $product_details as $k => $v ) {

                    $rr = unserialize( $v );
                    if ( in_array( $rr['id'], $product_ids ) ) {
                        $product [ $num ]['id']         = $rr['id'];
                        $product [ $num ]['title']      = $rr['title'];
                        $product [ $num ]['quantity']   = $rr['quantity'];
                        $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                        $product [ $num ]['created_at'] = $val->created_at;
                        $total_price                    = $total_price + ( $rr['quantity'] * $rr['price'] );
                        $total_quantity                 = $total_quantity + $rr['quantity'];
                        $products [ $num ]['id']        = $rr['id'];
                        $products [ $num ]['title']     = $rr['title'];
                        $num ++;
                    }
                }
            }

            $dropdown_products = array_unique( $products, SORT_REGULAR );


        }


        return view( 'Reports.analytics-each-sale.list' )->with( [
                                                                     'dropdown_products' => $dropdown_products,
                                                                     'prod'              => $product,
                                                                     'total_price'       => $total_price,
                                                                     'total_quantity'    => $total_quantity
                                                                 ] );
    }

    public function eachSales_productExport( Request $request, $id )
    {
        $all_product    = product::all();
        $orders         = order::all();
        $product_ids    = [];
        $product        = [];
        $total_quantity = 0;
        $total_price    = 0;
        $id             = isset( $id ) ? $id : "_0";


        foreach ( $all_product as $key => $val ) {
            $product_ids [] = $val->id;
        }

        if ( isset( $id ) && $id != "_0" ) {

            $product_id = $id;
            $num        = 1;

            foreach ( $orders as $key => $val ) {

                $product_details = unserialize( $val->product_detail );

                foreach ( $product_details as $k => $v ) {

                    $rr = unserialize( $v );
                    if ( $product_id == $rr['id'] ) {
                        $product [ $num ]['id']         = $rr['id'];
                        $product [ $num ]['title']      = $rr['title'];
                        $product [ $num ]['quantity']   = $rr['quantity'];
                        $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                        $product [ $num ]['created_at'] = \Carbon\Carbon::parse( $val->created_at )->format( 'd-m-Y g:i a' );
                        $total_price                    = $total_price + ( $rr['quantity'] * $rr['price'] );
                        $total_quantity                 = $total_quantity + $rr['quantity'];
                        $num ++;
                    }

                    if ( in_array( $rr['id'], $product_ids ) ) {
                        $avail_product [ $num ]['id']    = $rr['id'];
                        $avail_product [ $num ]['title'] = $rr['title'];
                        $num ++;
                    }
                }
            }


        } else {

            $num = 1;
            foreach ( $orders as $key => $val ) {
                $product_details = unserialize( $val->product_detail );
                foreach ( $product_details as $k => $v ) {

                    $rr = unserialize( $v );
                    if ( in_array( $rr['id'], $product_ids ) ) {
                        $product [ $num ]['id']         = $rr['id'];
                        $product [ $num ]['title']      = $rr['title'];
                        $product [ $num ]['quantity']   = $rr['quantity'];
                        $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                        $product [ $num ]['created_at'] = \Carbon\Carbon::parse( $val->created_at )->format( 'd-m-Y g:i a' );
                        $total_price                    = $total_price + ( $rr['quantity'] * $rr['price'] );
                        $total_quantity                 = $total_quantity + $rr['quantity'];
                        $products [ $num ]['id']        = $rr['id'];
                        $products [ $num ]['title']     = $rr['title'];
                        $num ++;
                    }
                }
            }


        }

        // customVarDump_die($products);

        $total_heading [] = [
            'data1'          => '',
            'data2'          => '',
            'Total Quantity' => 'Total Quantity',
            'Total Price'    => 'Total Price',
        ];

        $totals [] = [
            'data1'          => '',
            'data2'          => '',
            'Total Quantity' => $total_quantity,
            'Total Price'    => $total_price,
        ];

        $export = new EachSales( [ $product, $total_heading, $totals ] );

        return Excel::download( $export, 'eachSales_Report-' . now() . '.xlsx' );

    }

    public function eachSales_product( Request $request )
    {

        $product_id     = $request->product_id;
        $orders         = order::all();
        $all_products   = product::all();
        $product_ids    = [];
        $product        = [];
        $num            = 1;
        $total_quantity = 0;
        $total_price    = 0;


        foreach ( $all_products as $key => $val ) {
            $product_ids [] = $val->id;
        }

        foreach ( $orders as $key => $val ) {

            $product_details = unserialize( $val->product_detail );

            foreach ( $product_details as $k => $v ) {

                $rr = unserialize( $v );

                if ( $product_id == $rr['id'] ) {
                    $product [ $num ]['id']         = $rr['id'];
                    $product [ $num ]['title']      = $rr['title'];
                    $product [ $num ]['quantity']   = $rr['quantity'];
                    $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                    $product [ $num ]['created_at'] = $val->created_at;

                    $total_price    = $total_price + $rr['price'];
                    $total_quantity = $total_quantity + $rr['quantity'];

                    $num ++;
                }

                if ( in_array( $rr['id'], $product_ids ) ) {
                    $avail_product [ $num ]['id']    = $rr['id'];
                    $avail_product [ $num ]['title'] = $rr['title'];
                    $num ++;
                }


            }

        }


        $avail_product = array_unique( $avail_product, SORT_REGULAR );

        return view( 'Reports.analytics-each-sale.list' )->with( [
                                                                     'prod'           => $product,
                                                                     'products'       => $avail_product,
                                                                     'total_price'    => $total_price,
                                                                     'total_quantity' => $total_quantity
                                                                 ] );
    }


    //===== Analytics Total Sales
    public function totalSales_list( Request $request )
    {

        $all_product    = product::all();
        $orders         = order::all();
        $product_ids    = [];
        $product        = [];
        $products       = [];
        $total_quantity = 0;
        $total_price    = 0;
        $filter_orders  = [];
        //$dropdown_products  = [];

        foreach ( $all_product as $key => $val ) {
            $product_ids [] = $val->id;
        }

        if ( $request->selected_range ) {

            $date = explode( "-", $request->selected_range );
            $date = array_map( function ( $i ) {
                $dd = date_format( date_create( $i ), "d-m-Y" );

                return trim( $dd );
            }, $date );

            $start_date = strtotime( $date[0] );
            $end_date   = strtotime( $date[1] );

            foreach ( $orders as $key => $val ) {
                $order_date = strtotime( date_format( date_create( $val->created_at ), "d-m-Y" ) );
                if ( $start_date <= $order_date && $end_date >= $order_date ) {
                    $filter_orders [] = $val;
                }
            }

            $orders = $filter_orders;
        }


        $num = 1;
        foreach ( $orders as $key => $val ) {

            $product_details = unserialize( $val->product_detail );
            foreach ( $product_details as $k => $v ) {

                $rr = unserialize( $v );
                if ( in_array( $rr['id'], $product_ids ) ) {
                    $product [ $num ]['id']         = $rr['id'];
                    $product [ $num ]['title']      = $rr['title'];
                    $product [ $num ]['quantity']   = $rr['quantity'];
                    $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                    $product [ $num ]['created_at'] = $val->created_at;
                    $total_price                    = $total_price + ( $rr['quantity'] * $rr['price'] );
                    $total_quantity                 = $total_quantity + $rr['quantity'];
                    $products [ $num ]['id']        = $rr['id'];
                    $products [ $num ]['title']     = $rr['title'];
                    $num ++;
                }
            }
        }

        $dropdown_products = array_unique( $products, SORT_REGULAR );

        return view( 'Reports.analytics-total-sales.list' )
            ->with( [
                        'dropdown_products' => $dropdown_products,
                        'prod'              => $product,
                        'total_price'       => $total_price,
                        'total_quantity'    => $total_quantity
                    ] );

    }

    public function totalSales_productExport()
    {

        $all_product    = product::all();
        $orders         = order::all();
        $product_ids    = [];
        $product        = [];
        $total_quantity = 0;
        $total_price    = 0;

        foreach ( $all_product as $key => $val ) {
            $product_ids [] = $val->id;
        }

        $num = 1;
        foreach ( $orders as $key => $val ) {
            $product_details = unserialize( $val->product_detail );
            foreach ( $product_details as $k => $v ) {

                $rr = unserialize( $v );
                if ( in_array( $rr['id'], $product_ids ) ) {
                    $product [ $num ]['id']         = $rr['id'];
                    $product [ $num ]['title']      = $rr['title'];
                    $product [ $num ]['quantity']   = $rr['quantity'];
                    $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                    $product [ $num ]['created_at'] = \Carbon\Carbon::parse( $val->created_at )->format( 'd-m-Y g:i a' );
                    $total_price                    = $total_price + $rr['price'];
                    $total_quantity                 = $total_quantity + $rr['quantity'];
                    $products [ $num ]['id']        = $rr['id'];
                    $products [ $num ]['title']     = $rr['title'];
                    $num ++;
                }
            }
        }

        $total_heading [] = [
            'data1'          => '',
            'data2'          => '',
            'Total Quantity' => 'Total Quantity',
            'Total Price'    => 'Total Price',
        ];

        $totals [] = [
            'data1'          => '',
            'data2'          => '',
            'Total Quantity' => $total_quantity,
            'Total Price'    => $total_price,
        ];

        $export = new TotalSales( [ $product, $total_heading, $totals ] );

        return Excel::download( $export, 'totalSales_Report-' . now() . '.xlsx' );


        //        $header_style = ( new StyleBuilder() )->setFontBold()->build();
        //        $rows_style   = ( new StyleBuilder() )
        //            // ->setShouldWrapText()
        //            ->build();
        //
        //        return ( new FastExcel( $product ) )
        //            ->headerStyle( $header_style )
        //            ->rowsStyle( $rows_style )
        //            ->download( 'totalSale_Report-' . now() . '.xlsx', function ( $order ) {
        //                return [
        //                    'ID'         => $order['id'],
        //                    'Product'    => $order['title'],
        //                    'Quantity'   => $order['quantity'],
        //                    'Price'      => ( $order['quantity'] * $order['price'] ),
        //                    'Created_at' => \Carbon\Carbon::parse( $order['created_at'] )->format( 'd-m-Y g:i a' ),
        //                ];
        //            } );

    }

    //===== Analytics Country Sales
    public function countrySales_list( Request $request )
    {

        $all_product       = product::all();
        $orders            = order::all();
        $product_ids       = [];
        $product           = [];
        $avail_product     = [];
        $products          = [];
        $selected_country  = [];
        $dropdown_products = [];

        $total_quantity = 0;
        $total_price    = 0;


        foreach ( $orders as $key => $val ) {
            if ( ! empty( $val->selected_country ) ) {
                $selected_country [] = $val->selected_country;
            }
        }

        $selected_country = array_unique( $selected_country, SORT_REGULAR );


        foreach ( $all_product as $key => $val ) {
            $product_ids [] = $val->id;
        }

        if ( isset( $request->country ) && $request->country != "_0" ) {

            $num = 1;
            foreach ( $orders as $key => $val ) {

                if ( $request->country == trim( strtolower( $val->selected_country ) ) ) {

                    $product_details = unserialize( $val->product_detail );

                    foreach ( $product_details as $k => $v ) {

                        $rr                             = unserialize( $v );
                        $product [ $num ]['id']         = $rr['id'];
                        $product [ $num ]['title']      = $rr['title'];
                        $product [ $num ]['quantity']   = $rr['quantity'];
                        $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                        $product [ $num ]['country']    = $val->selected_country;
                        $product [ $num ]['created_at'] = $val->created_at;
                        $total_price                    = $total_price + ( $rr['quantity'] * $rr['price'] );
                        $total_quantity                 = $total_quantity + $rr['quantity'];
                        $num ++;

                        if ( in_array( $rr['id'], $product_ids ) ) {
                            $avail_product [ $num ]['id']    = $rr['id'];
                            $avail_product [ $num ]['title'] = $rr['title'];
                            $num ++;
                        }
                    }

                }
            }

            $dropdown_products = array_unique( $avail_product, SORT_REGULAR );

        } else {

            $num = 1;
            foreach ( $orders as $key => $val ) {

                if ( ! empty( $val->selected_country ) ) {
                    $product_details = unserialize( $val->product_detail );
                    foreach ( $product_details as $k => $v ) {

                        $rr                             = unserialize( $v );
                        $product [ $num ]['id']         = $rr['id'];
                        $product [ $num ]['title']      = $rr['title'];
                        $product [ $num ]['quantity']   = $rr['quantity'];
                        $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                        $product [ $num ]['country']    = $val->selected_country;
                        $product [ $num ]['created_at'] = $val->created_at;
                        $total_price                    = $total_price + ( $rr['quantity'] * $rr['price'] );
                        $total_quantity                 = $total_quantity + $rr['quantity'];

                        $products [ $num ]['id']    = $rr['id'];
                        $products [ $num ]['title'] = $rr['title'];
                        $num ++;
                    }
                }

            }

            if ( ! empty( $products ) ) {
                $dropdown_products = array_unique( $products, SORT_REGULAR );
            }

        }


        return view( 'Reports.analytics-country-sales.list' )
            ->with( [
                        'dropdown_products' => $dropdown_products,
                        'dropdown_country'  => $selected_country,
                        'prod'              => $product,
                        'total_price'       => $total_price,
                        'total_quantity'    => $total_quantity
                    ] );

    }

    public function countrySales_productExport( Request $request , $name )
    {

        $all_product      = product::all();
        $orders           = order::all();
        $product_ids      = [];
        $product          = [];
        $avail_product    = [];
        $products         = [];
        $selected_country = [];
        $country          = isset( $name ) ? $name : "_0";
        $total_quantity = 0;
        $total_price    = 0;


        foreach ( $orders as $key => $val ) {
            if ( ! empty( $val->selected_country ) ) {
                $selected_country [] = $val->selected_country;
            }
        }

        foreach ( $all_product as $key => $val ) {
            $product_ids [] = $val->id;
        }

        if ( isset( $country ) && $country != "_0" ) {

            $num = 1;
            foreach ( $orders as $key => $val ) {

                if ( $country == trim( strtolower( $val->selected_country ) ) ) {

                    $product_details = unserialize( $val->product_detail );

                    foreach ( $product_details as $k => $v ) {

                        $rr                             = unserialize( $v );
                        $product [ $num ]['id']         = $rr['id'];
                        $product [ $num ]['title']      = $rr['title'];
                        $product [ $num ]['quantity']   = $rr['quantity'];
                        $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                        $product [ $num ]['country']    = $val->selected_country;
                        $product [ $num ]['created_at'] = $val->created_at;
                        $total_price                    = $total_price + ( $rr['quantity'] * $rr['price'] );
                        $total_quantity                 = $total_quantity + $rr['quantity'];
                        $num ++;

                        if ( in_array( $rr['id'], $product_ids ) ) {
                            $avail_product [ $num ]['id']    = $rr['id'];
                            $avail_product [ $num ]['title'] = $rr['title'];
                            $num ++;
                        }
                    }

                }
            }

        } else {

            $num = 1;
            foreach ( $orders as $key => $val ) {

                if ( ! empty( $val->selected_country ) ) {
                    $product_details = unserialize( $val->product_detail );
                    foreach ( $product_details as $k => $v ) {

                        $rr                             = unserialize( $v );
                        $product [ $num ]['id']         = $rr['id'];
                        $product [ $num ]['title']      = $rr['title'];
                        $product [ $num ]['quantity']   = $rr['quantity'];
                        $product [ $num ]['price']      = ( $rr['quantity'] * $rr['price'] );
                        $product [ $num ]['country']    = $val->selected_country;
                        $product [ $num ]['created_at'] = $val->created_at;
                        $total_price                    = $total_price + ( $rr['quantity'] * $rr['price'] );
                        $total_quantity                 = $total_quantity + $rr['quantity'];

                        $products [ $num ]['id']    = $rr['id'];
                        $products [ $num ]['title'] = $rr['title'];
                        $num ++;
                    }
                }
            }

        }



        $total_heading [] = [
            'data1'          => '',
            'data2'          => '',
            'Total Quantity' => 'Total Quantity',
            'Total Price'    => 'Total Price',
        ];

        $totals [] = [
            'data1'          => '',
            'data2'          => '',
            'Total Quantity' => $total_quantity,
            'Total Price'    => $total_price,
        ];

        $export = new CountrySales( [ $product, $total_heading, $totals ] );

        return Excel::download( $export, 'countrySales_Report-' . now() . '.xlsx' );

    }


}
