<?php

namespace App\Http\Controllers;


use App\Artist;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::orderBy('id','DESC')->get();
        return view('tags.all' , ['tags' => $tags ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Tags_Detail = Tag::where(['name' => $request->tag ])->first();


        $custom_message = [
            'tag.required' => 'Tag name is required!'
        ];

        $this->validate($request, [
            'tag' => 'required'
        ], $custom_message);


        try{


            if(!$Tags_Detail){

                $tag = new Tag();
                $tag->name     = $request->tag;
                $tag->alias    = strtolower(str_replace(" ", "-", $request->tag));
                $tag->save();

                $noti = array("message" => "Tag created successfully!", "alert-type" => "success");
                return redirect()->route('product.tags.list')->with($noti);
            }

            $noti = array("message" => "Tag {$request->tag} already exist!!", "alert-type" => "error");
            return redirect()->route('product.tags.list')->with($noti);

        }catch(\Exception $ex){
            $noti = array("message" =>$ex->getMessage(), "alert-type" => "danger");
            return redirect()->route('product.tags.list')->with($noti);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            //$tag_details = Tag::where(['id' => $tag_id])->first();
            $tag_details = Tag::find(decrypt( $id ));

            if ( ! empty( $tag_details ) ) {
               $tag_id = decrypt( $id );
            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }


        return view('tags.edit', [ 'tag_details' => $tag_details , 'tag_id' => $tag_id] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $Tag = Tag::where(['name' => $request->name ])->get();

        $custom_message = [
            'tag.required' => 'Tag name is required!'
        ];

        $this->validate($request, [
            'tag' => 'required',
        ], $custom_message);

        try{

            if(count($Tag) == 0){

                $tag = Tag::where(['id' => $id])->update([
                    'name'      => $request->tag,
                    'alias'     => strtolower(str_replace(" ", "-", $request->tag)),
                ]);

                $noti = array("message" => "Tag updated successfully!", "alert-type" => "success");
                return Redirect::back()->with($noti);
            }

            $noti = array("message" => "Tag {$request->tag} already exist!!", "alert-type" => "error");
            return Redirect::back()->with($noti);



        }catch(\Exception $ex){
            $noti = array("message" =>$ex->getMessage(), "alert-type" => "error");
            return redirect()->route('product.tags.list')->with($noti);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();

        $noti = array("message" => "Tag delete successfully", "alert-type" => "success");
        return redirect()->route('product.tags.list')->with($noti);
    }

    public function destroyAllTags(Request $request){

        $ids = json_decode($request->delete_ids);

        foreach ($ids as $key => $id){
            $tag = Tag::findOrFail($id);
            $tag->delete();
        }

        $noti = array("message" => "Selected tags are deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }


}
