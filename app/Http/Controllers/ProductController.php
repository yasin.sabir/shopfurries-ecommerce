<?php

namespace App\Http\Controllers;

use Image;
use App\Tag;
use App\User;
use App\Artist;
use App\Product;
use App\UserMeta;
use App\Category;
use App\SiteSetting;
use App\Mail\Testing;
use App\products_meta;
use App\Product_Review;
use App\Mail\ContactUs;
use App\CustomerReviews;
use App\products_gallery;
use App\category_setting;
use Illuminate\Http\Request;
use App\Exports\ProductsExport;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use App\Mail\Product\ProductSubmitted;
use Illuminate\Support\Facades\Storage;
use App\Mail\Product\ProductPublished_User;
use App\Mail\Product\ProductSubmitted_Admin;

class ProductController extends Controller
{

    public function __construct(products_gallery $products_gallery)
    {
        $this->products_gallery = $products_gallery;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$pp = product::find(38)->categories()->get();
        //customVarDump_die($pp->toArray());

	    try{
		    if(Auth::user()->hasRole(1)){
			    $product = product::where(['product_type' => 'product'])->orderBy('id','DESC')->get();
			    return view('Products.all', ['products' => $product]);
		    }else{
			    $product = product::where(['user_id'=> Auth::user()->id ])
			                      ->where(['product_type' => 'product'])
			                      ->orderBy('id','DESC')->get();
			    return view('Products.all', ['products' => $product]);
		    }
	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }


    public function exportProductInfo(Request $request){

	    try{

		    $product     = product::find( $request->prod_id );
		    $metas       = products_meta::where( [ 'product_id' => $request->prod_id ] )->get();
		    $productMeta = [];

		    foreach ($metas as $key => $val){
			    $productMeta [$val['product_meta']] = $val['product_meta_value'];
		    }

		    $image = "";
		    $ff    = @unserialize( $product->image );

		    if ( ! is_array( $ff ) ) {
			    $image = $ff;
		    } else {
			    $image = $product->image;
		    }

		    $default_size = \App\SiteSetting::where( [ 'key' => 'product_image_size' ] )->first();

		    if ( ! is_array( $ff ) ){
			    if ( ! empty( $product->image ) ) {
				    $feature_img = "storage/".$image;
			    }else{
				    $feature_img = "images/default-placeholder-600x600.png";
			    }
		    }else{
			    $feature_img = "storage/".$ff[$default_size->value];
		    }

		    $default_weight_price = SiteSetting::where(['key' => 'default_weight_price'])->first();


		    $cat_ = unserialize($productMeta['categories']);
		    $cat_ = array_map(function ($i){
			    $cc = Category::find($i);
			    return $cc->Name;
		    },$cat_);
		    $cat_ = implode(" , ",$cat_);


		    $tags_   = unserialize($productMeta['tags']);
		    $tags_   = array_map(function ($i){
			    $tt  = Tag::find($i);
			    return $tt->name;
		    },$tags_);
		    $tags_   = implode(" , ",$tags_);

		    $materials_ = implode(" , ",unserialize($productMeta['material']));
		    $sizes_     = implode(" , ",unserialize($productMeta['sizes']));


		    $pdf = PDF::loadView('Products.product-pdf.export-info',[
			    'product'            => $product,
			    'product_meta'       => $productMeta,
			    'feature_img'        => $feature_img,
			    'default_weight'     => $default_weight_price,
			    'categories'         => $cat_,
			    'tags'               => $tags_,
			    'materials'          => $materials_,
			    'sizes'              => $sizes_,
		    ]);
		    return $pdf->download('product_'.$request->prod_id.'_info.pdf');
		    //return view('Products.product-pdf.export-info');

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }


	public static function getProductCategoriesById($product_id){

		$data       = Product::find($product_id);
    	$categories = [];

		foreach ($data->categories as $key => $val){
			$categories [] = $val->Name;
		}

		return $categories;

    }

    public static function getProductMetas($product_id){

    	$data       = Product::find($product_id);
	    $metas      = [];

	    foreach ($data->product_metas as $key => $val){
		    $metas [$val->product_meta] = $val->product_meta_value;
	    }

	    return $metas;

    }

    public function exportProductExcel(Product $product){
	    try{
			//	    	dd($this->getProductCategoriesById(200));
			//	    	$rr = Product::find(118);
			//	    	dd($rr->product_metas->toArray());
	    	return Excel::download(new ProductsExport($product), 'products-'.now().'.xlsx');
	    }catch (\Exception $ex){
		    $noti = array("message" => "Error -".$ex->getMessage(), "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    try{
		    //Categories
		    $getCategoryTop = Category::where('status', 1)->where('top', 1)->get();

		    $getCategoryAll = Category::where('status', 1)
		                              ->orderby('id', 'asc')
		                              ->get()
		                              ->groupBy('Parent');
		    //customVarDump_die($getCategoryAll->toArray());

		    $getTags = Tag::all();
		    $artists = Artist::all();
		    $products = product::all();

		    return view('Products.add', [
			    'getCategoryTop' => $getCategoryTop,
			    'getCategoryAll' => $getCategoryAll,
			    'tags'           => $getTags,
			    'artists'        => $artists,
			    'products'       => $products
		    ]);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

	    $sale_prices = (!isset($request->sale_prices) ? 0 : $request->sale_prices);

	    $custom_message = [
		    'product_name.required'             => 'Product name is required!',
		    'product_sizes.required'            => 'Size field is required!',
		    'product_material.required'         => "Material field is required!",
		    'lt'                                => "Sale price should not be greater than Regular price",
	    ];

	    $this->validate($request, [
		    'product_name'     => 'required',
		    'product_sizes'    => 'required',
		    'regular_price'    => 'required',
		    'sale_prices'      => 'required|lt:regular_price',
		    'product_material' => 'required',
		    //'product_video'    => array('product_video' => 'regex:/^\b(?:(?:https?|http|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]$/i'),
	    ], $custom_message);

	    $product_meta_serialize = $request->except(['file', 'featured_img','product_video']);
	    $product_meta_serialize = serialize($product_meta_serialize);
	    $productID_MailUse      = "";

	    try {
		    DB::transaction(function () use ($request, $product_meta_serialize,$sale_prices,$productID_MailUse) {

			    $product = new product();
			    $product->user_id           = Auth::user()->id;
			    $product->title             = $request->product_name;
			    $product->description       = $request->description;
			    $slug                       = str_slug($request->product_name, "-");
			    $product->slug              = $slug;
			    $product->price             = $request->regular_price;
			    $auto_gen_sku               = uniqid("sku-");
			    $product->sku               = $auto_gen_sku;
			    $product->product_type      = "product";

			    //$product->video             = $request->file('product_video') ? $request->file('product_video')->store('upload/product/' . $slug . '/video', 'public') : 'null';
			    $product->video             = $request->product_video;

			    if(Auth::user()->hasRole(1)){
				    $product->status        = ($request->has('status') && $request->status == "yes" ) ? 1 : 0;
			    }else{
				    $product->status        = 0;
			    }

			    $default_ = SiteSetting::where(['key' => 'default_product_stock'])->first();
			    $default_stock = ( !empty($default_) ? $default_->value : 'no' );

			    $product->stock             = $request->has('stock_quantity') ? $request->stock_quantity : $default_stock;
			    $product->save();


			    //Multi sizes of featured Image
			    //$img600 = storage_path('app/public');
			    //$img900 = storage_path('app/public');

			    //                    $fileExt = ".jpg";
			    //                    Image::configure(array('driver' => 'gd'));
			    //                    $imgName = sha1(time()) . $fileExt;
			    //
			    //                    $img300 = storage_path('app/public/upload/product/product_'.$product->id.'/feature/300');
			    //                    $savePath = $img300 . $imgName;
			    //                    $img = Image::make($request->file('featured_img'))
			    //                            ->resize(300,300)
			    //                            ->save($savePath);
			    //                    $img->save();


			    //                    product::where(['id' => $product->id])->update([
			    //                       'image' => $request->file('featured_img') ? $request->file('featured_img')->store('upload/product/product_'.$product->id.'/feature', 'public') : 'null'
			    //                    ]);

			    if($request->hasFile('featured_img')){
				    $sizes        = ['300','600','1024'];
				    $type         = "products";
				    $slug         = "product";
				    $otherFolders = "feature";
				    $file_key     = 'featured_img';
				    $path         = $this->imageResizing($request ,$product->id , $type, $slug , $file_key , $sizes , $otherFolders , $request);
				    //customVarDump_die(serialize($path));
				    product::where(['id' => $product->id])->update([ 'image' => serialize($path) ]);
			    }

			    $product_id         = $product->id;
			    $this->productID_MailUse  = $product->id;

			    $product->categories()->attach($request->categories);
			    $product->tags()->attach($request->tags);

			    $sales_price_schd = explode('-', $request->sales_price_schd);

			    $prod_shipping_1 = isset($request->product_shipping_time1) ? $request->product_shipping_time1 : null;
			    $prod_shipping_2 = isset($request->product_shipping_time2) ? $request->product_shipping_time2 : null;

			    $estimate_shipping = $prod_shipping_1."-".$prod_shipping_2;

			    $up_sell = $request->up_sell;
			    $upsell_amount = $request->upsell_amount;

			    $productMaterial_ = explode(",",$request->product_material);
			    $productSizes_    = explode(",",$request->product_sizes);

			    $productMaterial_ = array_map(function ($i){
				    return str_replace(" " ,"_", trim($i , " "));
			    },$productMaterial_);

			    $productSizes_ = array_map(function ($i){
				    return str_replace(" " ,"_", trim($i , " "));
			    },$productSizes_);

			    $product_metas = [
				    ['product_id'=> $product_id , "product_meta" => "slug"                        , "product_meta_value" => $slug],
				    ['product_id'=> $product_id , "product_meta" => "artist"                      , "product_meta_value" => ( !Auth::user()->hasRole(1) ? Auth::user()->id :  ( !empty($request->artists) ? $request->artists : null )  )],
				    ['product_id'=> $product_id , "product_meta" => "artist_updated_separately"   , "product_meta_value" => ( !Auth::user()->hasRole(1) ? Auth::user()->id :  ( !empty($request->artists) ? "yes" : "no" )  )  ],
				    ['product_id'=> $product_id , "product_meta" => "price"                       , "product_meta_value" => $request->regular_price],
				    ['product_id'=> $product_id , "product_meta" => "sale_price"                  , "product_meta_value" => $sale_prices],
				    ['product_id'=> $product_id , "product_meta" => "sale_start_date"             , "product_meta_value" => $sales_price_schd[0]],
				    ['product_id'=> $product_id , "product_meta" => "sale_end_date"               , "product_meta_value" => $sales_price_schd[1]],
				    ['product_id'=> $product_id , "product_meta" => "sku"                         , "product_meta_value" => $auto_gen_sku],
				    ['product_id'=> $product_id , "product_meta" => "tags"                        , "product_meta_value" => serialize($request->tags)],
				    ['product_id'=> $product_id , "product_meta" => "categories"                  , "product_meta_value" => serialize($request->categories)],
				    ['product_id'=> $product_id , "product_meta" => "featured_image"              , "product_meta_value" => $request->file('featured_img') ? $request->file('featured_img')->store('upload/product/product_' . $product_id . '/feature', 'public') : "null"],
				    ['product_id'=> $product_id , "product_meta" => "stock"                       , "product_meta_value" => ($request->has('stock_quantity') ? $request->stock_quantity : $default_stock)],
				    ['product_id'=> $product_id , "product_meta" => "material"                    , "product_meta_value" => serialize($productMaterial_)],
				    ['product_id'=> $product_id , "product_meta" => "sizes"                       , "product_meta_value" => serialize($productSizes_)],
				    ['product_id'=> $product_id , "product_meta" => "weight"                      , "product_meta_value" => ( !empty($request->product_weight) ? $request->product_weight : null ) ],
				    ['product_id'=> $product_id , "product_meta" => "stock_manage_chk"            , "product_meta_value" => ($request->has('manage_stock_checked') ? "checked" : "unchecked" )],
				    ['product_id'=> $product_id , "product_meta" => "stock_threshold"             , "product_meta_value" => $request->has('low_stock_threshold') ? $request->low_stock_threshold : ''],
				    ['product_id'=> $product_id , "product_meta" => "estimate_shipping"           , "product_meta_value" => $estimate_shipping],
				    ['product_id'=> $product_id , "product_meta" => "serialize_data"              , "product_meta_value" => $product_meta_serialize],
				    ['product_id'=> $product_id , "product_meta" => "product_video_description"   , "product_meta_value" => $request->product_video_description],
				    ['product_id'=> $product_id , "product_meta" => "product_video"               , "product_meta_value" => $request->product_video],
				    ['product_id'=> $product_id , "product_meta" => "up_sell"                     , "product_meta_value" => $up_sell],
				    ['product_id'=> $product_id , "product_meta" => "upsell_amount"               , "product_meta_value" => $upsell_amount],
			    ];

			    foreach ($product_metas as $k => $v) {
				    products_meta::updateOrInsert(['product_id' => $v['product_id'], 'product_meta' => $v['product_meta']], $v);
			    }


			    if($request->hasFile('file')){
				    $sizes          = ['600'];
				    $type           = "products";
				    $slug           = "product";
				    $otherFolders   = "gallery";
				    $file_key       = 'file';
				    $path           = $this->imageResizing( $request ,$product->id , $type, $slug , $file_key , $sizes , $otherFolders);

				    if(count($path) > 0){
					    foreach ($path as $key => $val){
						    $products_gallery = new products_gallery();
						    $products_gallery->product_id = $product_id;
						    $products_gallery->image      = $val;
						    $products_gallery->save();
					    }
				    }
			    }

			    //                      if ($request->file('file')) {
			    //                        $file = $request->file;
			    //                        foreach ($file as $image) {
			    //                            $products_gallery = new products_gallery();
			    //                            $products_gallery->product_id = $product_id;
			    //                            $products_gallery->image = $image->store('upload/product/' . $slug . '/gallery', 'public');
			    //                            $products_gallery->save();
			    //                        }
			    //                    }


		    });

		    //send mail to user and admin when user add product into system
		    if(!Auth::user()->hasRole(1)){

			    $user                 = User::find(Auth::user()->id);
			    $fetch_productDetails = product::find($this->productID_MailUse);

			    $user_details [0] = $user;
			    $user_details [1] = $fetch_productDetails;

			    Mail::to($user->email)->send(new ProductSubmitted($user_details));

			    $get_admin_ids = [];

			    foreach (get_AllAdmin() as $key => $val){
				    $get_admin_ids [] = $val->user_id;
			    }

			    $adminDetails = get_admin_detail();

			    $detail_send_to_Admin [0] = $adminDetails;
			    $detail_send_to_Admin [1] = $user;
			    $detail_send_to_Admin [2] = $fetch_productDetails;

			    //$test_adminEmail1 = "yasin@hztech.biz";
			    //$test_adminEmail2 = "yasin.100@hotmail.com";
			    $admin_email = get_admin_email();

			    //Inform to admin when user submit their product in furries
			    Mail::to($admin_email)->send(new ProductSubmitted_Admin($detail_send_to_Admin));

			    //This below code for multiple admins
//                    foreach ($get_admin_ids as $key => $val){
//
//                        $adminDetails = User::find($val);
//
//                        $detail_send_to_Admin [0] = $adminDetails;
//                        $detail_send_to_Admin [1] = $user;
//                        $detail_send_to_Admin [2] = $fetch_productDetails;
//
//                        $test_adminEmail1 = "yasin@hztech.biz";
//                        //$test_adminEmail2 = "yasin.100@hotmail.com";
//
//                        //Inform to admin when user submit their product in furries
//                        Mail::to($test_adminEmail1)->send(new ProductSubmitted_Admin($detail_send_to_Admin));
//                        //Mail::to($test_adminEmail2)->send(new ProductSubmitted_Admin($detail_send_to_Admin));
//                    }
		    }

		    $noti = array("message" => "Product created successfully!", "alert-type" => "success");
		    return redirect()->route('product.list')->with($noti);

	    } catch (\Exception $ex) {
		    $noti = array("message" => $ex->getMessage(), "alert-type" => "error");
		    return redirect()->route('product.add')->with($noti);
	    }

    }

    function imageResizing(Request $request,$id, $type , $slug , $file_key , $sizes , $other_folders = false){

	    try{

		    $savePath = [];

		    if(!empty($sizes) && count($sizes) > 0){

			    foreach ($sizes as $key => $val){

				    $folderName   = $val."x".$val;
				    $slugFolder  = $slug."_".$id;

				    if($other_folders){
					    $otherFolders = $other_folders;
					    $directory    = "public/upload/".$type."/".$slugFolder."/".$otherFolders."/".$folderName."/";
				    }else{
					    $directory    = "public/upload/".$type."/".$slugFolder."/".$folderName."/";
				    }
				    Storage::makeDirectory($directory);
			    }

			    if($request->hasFile($file_key)){

				    //For Multiple Images
				    if(is_array($request->file($file_key))){
//                if(is_array(Input::file($file_key))){

					    $images = $request->file($file_key);
					    foreach ($images as $k => $v){

						    $filename  = time() . '.' . $v->getClientOriginalExtension();

						    foreach ($sizes as $key => $val){
							    $folderName     = $val."x"."$val";
							    $folderSlug    = $slug."_".$id;

							    if($other_folders){
								    $otherFolders   = $other_folders;
								    $path           = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/').$filename;
								    $savePath []    = 'upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/'.$filename;
							    }else{
								    $path           = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$folderName.'/').$filename;
								    $savePath []    = 'upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/'.$filename;
							    }

							    $img            = \Intervention\Image\Facades\Image::make( $v->getRealPath());
							    $img->resize($val, $val, function ($constraint) {
								    $constraint->aspectRatio();
								    $constraint->upsize();
							    });
							    $img->save($path);
						    }
					    }


				    }else{

					    //For Single Image
					    $image     = $request->file($file_key);
//                    $image     = Input::file($file_key);
					    $filename  = time() . '.' . $image->getClientOriginalExtension();

					    foreach ($sizes as $key => $val){
						    $folderName     = $val."x"."$val";
						    $folderSlug    = $slug."_".$id;

						    if($other_folders){
							    $otherFolders       = $other_folders;
							    $path               = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/').$filename;
							    $savePath [$val]    = 'upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/'.$filename;
						    }else{
							    $path               = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$folderName.'/').$filename;
							    $savePath [$val]    = 'upload/'.$type.'/'.$folderSlug.'/'.$folderName.'/'.$filename;
						    }
						    $img                = \Intervention\Image\Facades\Image::make( $image->getRealPath());
						    $img->resize($val, $val, function ($constraint) {
							    $constraint->aspectRatio();
							    $constraint->upsize();
						    });
						    $img->save($path);

					    }
				    }

			    }


		    }
		    return $savePath;

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {

            $product = product::find( decrypt( $id ) );
            if ( ! empty( $product ) ) {

                $all_products = product::all();

                $p_metas = products_meta::where(['product_id' => decrypt( $id )])->get();
                $tags = Tag::all();
                $categories = Category::all();

                $meta_data = [];
                foreach ($p_metas as $key => $val) {
                    $meta_data[$val['product_meta']] = $val['product_meta_value'];
                }

                //Review Section
                $reviews         = Product_Review::where(['product_id'=>decrypt( $id )])->get();
                $product_gallery = products_gallery::where('product_id', decrypt( $id ))->get();
                $artists         = Artist::all();
                $attr            = product::find(decrypt( $id ));
                $variations      = product::find(decrypt( $id ));

            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }


        return view('Products.edit',
        [
            'product'           => $product,
            'all_products'      => $all_products,
            'product_meta'      => $meta_data,
            'product_gallery'   => $product_gallery,
            'categories'        => $categories,
            'tags'              => $tags,
            'reviews'           => $reviews,
            'artists'           => $artists,
            'attr'              => $attr->attributes,
            'variations'        => $variations->variations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    try{

		    // Previous Featured Image
		    $Product                   = Product::find($id);
		    $product_prev_featured_img = $Product->image;
		    $product_video             = $Product->video;

		    //Previous Gallery Images
		    $Gallery = products_gallery::select('image')->where(['product_id' => $id])->get();

		    $product_prev_gallery = [];
		    foreach ($Gallery as $key => $val) {
			    $product_prev_gallery[$key] = $val->image;
		    }


		    //Get user role
		    $user_id                   = $Product->user_id;

		    $UserRole = \DB::table('model_has_roles')
		                   ->where('model_has_roles.model_id','=',$user_id )
		                   ->join('roles' , 'model_has_roles.role_id' , '=' , 'roles.id')
		                   ->select('roles.name')
		                   ->first();


		    $sale_prices = (!isset($request->sale_prices) ? 0 : $request->sale_prices);

		    $custom_message = [
			    'product_name.required'             => 'Product name is required!',
			    'product_material.required'         => "Material field is required!",
			    'product_sizes.required'            => 'Size field is required!',
			    'lt'                                => "Sale price should not be greater than Regular price",
		    ];

		    $this->validate($request, [
			    'product_name'     => 'required',
			    'product_material' => 'required',
			    'product_sizes'    => 'required',
			    'regular_price'    => 'required',
			    'sale_prices'      => 'required|lt:regular_price',
			    //'product_video'    => array('product_video' => 'required','regex:/^\b(?:(?:https?|http|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]$/i'),
		    ], $custom_message);

		    $product_meta_serialize = $request->except(['file', 'featured_img','product_video']);
		    $product_meta_serialize = serialize($product_meta_serialize);

		    $slug = str_slug($request->product_name, "-");

		    $default_ = SiteSetting::where(['key' => 'default_product_stock'])->first();
		    $default_stock = ( !empty($default_) ? $default_->value : 'no' );

		    $product = Product::find($id)->update([
			                                          'user_id'           => Auth::user()->id,
			                                          'title'             => $request->product_name,
			                                          'description'       => $request->description,
			                                          'slug'              => $slug,
			                                          'price'             => $request->regular_price,
			                                          //'image'            => $request->file('featured_img') ? $request->file('featured_img')->store('upload/product/product_' . $id . '/feature', 'public') : $product_prev_featured_img,
			                                          //'video'            =>  $request->file('product_video') ? $request->file('product_video')->store('upload/product/' . $slug . '/video', 'public') : 'null',
			                                          'video'             =>  $request->product_video,
			                                          'status'            => ($request->has('status') && $request->status == "yes" ) ? 1 : 0,
			                                          'stock'             => ( !empty($request->stock_quantity) ? $request->stock_quantity : $default_stock ),
			                                          'product_type'      => 'product',
		                                          ]);

		    if( $request->hasFile('featured_img')){
			    $sizes              = ['300','600','1024'];
			    $type               = "products";
			    $slug               = "product";
			    $otherFolders       = "feature";
			    $file_key           = 'featured_img';
			    $path               = $this->imageResizing( $request , $id , $type, $slug , $file_key , $sizes , $otherFolders);
			    product::where(['id' => $id])->update([ 'image' => serialize($path) ]);
		    }else{
			    product::where(['id' => $id])->update([ 'image' => $product_prev_featured_img ]);
		    }


		    $prod = Product::find($id);
		    $prod->categories()->sync($request->categories);
		    $prod->tags()->sync($request->tags);

		    $sales_price_schd = explode('-', $request->sales_price_schd);

		    $prevs_artist = $request->prevs_artist;

		    $prod_shipping_1   = isset($request->product_shipping_time1) ? $request->product_shipping_time1 : null;
		    $prod_shipping_2   = isset($request->product_shipping_time2) ? $request->product_shipping_time2 : null;
		    $estimate_shipping = $prod_shipping_1."-".$prod_shipping_2;

		    $up_sell = $request->up_sell;
		    $upsell_amount = $request->upsell_amount;

		    $productMaterial_ = explode(",",$request->product_material);
		    $productSizes_    = explode(",",$request->product_sizes);

		    $productMaterial_ = array_map(function ($i){
			    return str_replace(" " ,"_", trim($i , " "));
		    },$productMaterial_);

		    $productSizes_ = array_map(function ($i){
			    return str_replace(" " ,"_", trim($i , " "));
		    },$productSizes_);

		    $product_metas = [
			    ['product_id'=> $id , "product_meta" => "slug"                                , "product_meta_value" => $slug],
			    ['product_id'=> $id , "product_meta" => "artist"                              , "product_meta_value" => ( ($UserRole->name == "user") ? $user_id : ( !empty($request->artists) ? $request->artists : null ) ) ],
			    ['product_id'=> $id , "product_meta" => "artist_updated_separately"           , "product_meta_value" => ( ($UserRole->name == "user") ? $user_id :  (  (!empty($request->artists) && $prevs_artist != $request->artists) ? "yes" : "no" )  )  ],
			    ['product_id'=> $id , "product_meta" => "price"                               , "product_meta_value" => $request->regular_price],
			    ['product_id'=> $id , "product_meta" => "sale_price"                          , "product_meta_value" => $sale_prices],
			    ['product_id'=> $id , "product_meta" => "sale_start_date"                     , "product_meta_value" => $sales_price_schd[0]],
			    ['product_id'=> $id , "product_meta" => "sale_end_date"                       , "product_meta_value" => $sales_price_schd[1]],
			    ['product_id'=> $id , "product_meta" => "tags"                                , "product_meta_value" => (isset($request->tags) ? serialize($request->tags) : null )],
			    ['product_id'=> $id , "product_meta" => "categories"                          , "product_meta_value" => (isset($request->categories) ? serialize($request->categories) : null ) ],
			    ['product_id'=> $id , "product_meta" => "featured_image"                      , "product_meta_value" => $request->file('featured_img') ? $request->file('featured_img')->store('upload/product/product_' . $id . '/feature', 'public') : $product_prev_featured_img],
			    ['product_id'=> $id , "product_meta" => "stock"                               , "product_meta_value" => ($request->has('stock_quantity') ? $request->stock_quantity : $default_stock )],
			    ['product_id'=> $id , "product_meta" => "material"                            , "product_meta_value" => serialize($productMaterial_)],
			    ['product_id'=> $id , "product_meta" => "sizes"                               , "product_meta_value" => serialize($productSizes_)],
			    ['product_id'=> $id , "product_meta" => "weight"                              , "product_meta_value" => ( !empty($request->product_weight) ? $request->product_weight : null )],
			    ['product_id'=> $id , "product_meta" => "stock_manage_chk"                    , "product_meta_value" => ( $request->has('manage_stock_checked') ? "checked" : "unchecked" ) ],
			    ['product_id'=> $id , "product_meta" => "stock_threshold"                     , "product_meta_value" => $request->has('low_stock_threshold') ? $request->low_stock_threshold : ''],
			    ['product_id'=> $id , "product_meta" => "estimate_shipping"                   , "product_meta_value" => $estimate_shipping],
			    ['product_id'=> $id , "product_meta" => "serialize_data"                      , "product_meta_value" => $product_meta_serialize],
			    ['product_id'=> $id , "product_meta" => "product_video_description"           , "product_meta_value" => $request->product_video_description],
			    ['product_id'=> $id , "product_meta" => "product_video"                       , "product_meta_value" => $request->product_video],
			    ['product_id'=> $id , "product_meta" => "up_sell"                             , "product_meta_value" => $up_sell],
			    ['product_id'=> $id , "product_meta" => "upsell_amount"                       , "product_meta_value" => $upsell_amount],
			    //['product_id'=> $id , "product_meta" => "user_product_status"                 , "product_meta_value" => ( ($UserRole->name == "user") ? "published_no" : ( ($request->has('status') && $request->status == "yes" ) ? 1 : 0 )  ) ]
		    ];

		    foreach ($product_metas as $k => $v) {
			    products_meta::updateOrInsert(['product_id' => $v['product_id'], 'product_meta' => $v['product_meta']], $v);
		    }

		    if ($request->file('file')) {

			    $file = $request->file('file');
			    foreach ($file as $image) {
				    products_gallery::updateOrInsert(['product_id' => $id, 'image' => $image->store('upload/product/' . $slug . '/gallery', 'public') ]);
			    }

		    } else {
			    foreach ($product_prev_gallery as $key => $val) {
				    products_gallery::where(['product_id' => $id])->update([
					                                                           'image' => $val
				                                                           ]);
			    }
		    }

		    //send mail to user when admin publish their product for first time.
		    $userProductMail_Details = [];
		    $userProductStatus       = products_meta::where(['product_id' => $id])->where(['product_meta' => 'user_product_status'])->first();


		    if($UserRole->name == "user" && $userProductStatus->product_meta_value == "published_no"){

			    $userProduct_meta            = products_meta::where(['product_id' => $id])->where(['product_meta' => 'artist'])->first();
			    $product_title               = $prod->title;
			    $userProductMail_Details[0]  = User::find($userProduct_meta->product_meta_value);
			    $userProductMail_Details[1]  = product::find($id);

			    //Send Email
			    Mail::to($userProductMail_Details[0]->email)->send(new ProductPublished_User($userProductMail_Details));

			    //                $product_userStatusMeta = [
			    //                    ['product_id'=> $id , "product_meta" => "user_product_status" , "product_meta_value" => ( ($UserRole->name == "user") ? "published_yes" : ( ($request->has('status') && $request->status == "yes" ) ? 1 : 0 )  ) ]
			    //                ];
			    //                foreach ($product_userStatusMeta as $key => $vv) {
			    //                    products_meta::updateOrInsert(['product_id' => $vv['product_id'], 'product_meta' => $vv['product_meta']], $vv);
			    //                }

		    }

		    $noti = array("message" => "Product Updated successfully!", "alert-type" => "success");
		    return redirect()->back()->with($noti);


	    }catch (\Exception $ex){
		    $noti = array("message" => $ex->getMessage(), "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    public function updateStatus(Request $request , $id){

	    try{

		    if( isset($request->status) && $request->status == "1" ){

			    $prod                        = product::find($id);
			    $prod_                       = product::where(['id' => $id])->update(['status' => $request->status]);
			    $userProduct_meta            = products_meta::where(['product_id' => $id])->where(['product_meta' => 'artist'])->first();
			    $UserRole                    = get_UserRole($userProduct_meta->product_meta_value);

			    product::where(['id' => $id])->update(['status' => $request->status]);

			    //            $product_userStatusMeta = [
			    //                ['product_id'=> $id , "product_meta" => "user_product_status" , "product_meta_value" => ( ($UserRole->name == "user") ? 0 : ( ($request->has('status') && $request->status == "yes" ) ? 1 : 0 )  ) ]
			    //            ];
			    //            foreach ($product_userStatusMeta as $key => $vv) {
			    //                products_meta::updateOrInsert(['product_id' => $vv['product_id'], 'product_meta' => $vv['product_meta']], $vv);
			    //            }
			    //            $userProductStatus       = products_meta::where(['product_id' => $id])->where(['product_meta' => 'user_product_status'])->first();
			    //            $Status_val              = floatval($userProductStatus->product_meta_value);
			    //            $val                     = floatval(1);


			    if( $UserRole->name == "user"){

				    $userProductMail_Details[0]  = User::find($userProduct_meta->product_meta_value);
				    $userProductMail_Details[1]  = $prod;

				    //Send Email
				    Mail::to($userProductMail_Details[0]->email)->send(new ProductPublished_User($userProductMail_Details));

				    //                $product_userStatusMeta = [
				    //                    ['product_id'=> $id , "product_meta" => "user_product_status" , "product_meta_value" => ( ($UserRole->name == "user") ? 1 : ( ($request->has('status') && $request->status == "yes" ) ? 1 : 0 )  ) ]
				    //                ];
				    //                foreach ($product_userStatusMeta as $key => $vv) {
				    //                    products_meta::updateOrInsert(['product_id' => $vv['product_id'], 'product_meta' => $vv['product_meta']], $vv);
				    //                }

				    $msg = "Status is on";
				    return response()->json(array(['msg'=> $msg , 'val' => 'checked']), 200);

			    }

		    }else if(isset($request->status) && $request->status == "0"){

			    product::where(['id' => $id])->update(['status' => $request->status]);
			    $msg = "Status is off";
			    return response()->json(array(['msg'=> $msg , 'val' => 'unchecked']), 200);

		    }else{

		    }

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

	    try{

	    	product::find($id)->categories()->detach();
		    product::find($id)->tags()->detach();

		    product::where('id', $id)->delete();
		    products_meta::where('product_id', $id)->delete();
		    products_gallery::where('product_id', $id)->delete();

		    $noti = array("message" => "Product deleted successfully", "alert-type" => "success");
		    return redirect()->route('product.list')->with($noti);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    public function destroyAllProducts(Request $request){

	    try{
		    $ids = json_decode($request->delete_ids);

		    foreach ($ids as $key => $id){

			    product::find($id)->categories()->detach();
			    product::find($id)->tags()->detach();

			    $prod = product::findOrFail($id);
			    $prod->delete();
			    products_meta::where('product_id', $id)->delete();
			    products_gallery::where('product_id', $id)->delete();
		    }

		    $noti = array("message" => "Selected products are deleted successfully", "alert-type" => "success");
		    return redirect()->back()->with($noti);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }


    }

    public function photo_delete(Request $request){

	    try{

		    $this->products_gallery->destroy($request->key);
		    return 1;

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }


    //Product Review Section

    public function indexReview(){

    }


    public function storeReview(Request $request){

	    try{

		    $custom_msg = [
			    'description.required' => 'Review description is required!',
			    'flag.required'        => 'Review is Required!'
		    ];

		    $this->validate($request,[
			    'description' => 'required',
			    'flag'        => 'required'
		    ],$custom_msg);

		    $pReview = new Product_Review();
		    $pReview->user_id           = $request->user_id;
		    $pReview->product_id        = $request->product_id;
		    $pReview->status            = 'on';
		    $pReview->flag              = $request->flag;
		    $pReview->description       = $request->description;
		    $pReview->video             = ( !isset($request->video) ? $request->video : null );
		    $pReview->parent_id         = null;
		    $pReview->voted             = null;
		    $pReview->unvoted           = null;
		    $photos                     = $request->file('photo');
		    $reviewsPhotos = [];
		    $pReview->save();

		    if($photos){
			    foreach ($photos as $key => $val){
				    $reviewsPhotos[$key] = $val->store('upload/product_review/product_'.$request->product_id.'/user_'.$request->user_id.'/review_'.$pReview->id.'', 'public');
			    }

			    Product_Review::where(['id' => $pReview->id])->update([
				                                                          'image' => serialize($reviewsPhotos)
			                                                          ]);

		    }else{
			    Product_Review::where(['id' => $pReview->id])->update([
				                                                          'image' => serialize(array())
			                                                          ]);
		    }

		    $noti = array("message" => "Review Added successfully!", "alert-type" => "success");
		    return redirect()->back()->with($noti);


	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }


    }

    public function editReview(Request $request , $id){

    }

    public function updateReview(Request $request , $id){

    }

    public function ReviewStatus_update(Request $request , $id){

	    try{

		    if( isset($request->status) && $request->status == "on" ){

			    Product_Review::find($id)->update(['status' =>  $request->status ]);
			    $msg = "Review is enable";
			    return response()->json(array(['msg'=> $msg , 'val' => 'checked']), 200);

		    }else if(isset($request->status) && $request->status == "off"){

			    Product_Review::find($id)->update(['status' =>  $request->status ]);
			    $msg = "Review is disable";
			    return response()->json(array(['msg'=> $msg , 'val' => 'unchecked']), 200);

		    }else{

		    }

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }


    }


    public function destoryReview($id){

	    try{

		    $prodReview = Product_Review::findOrFail($id);
		    $prodReview->delete();

		    $noti = array("message" => "Review deleted successfully!", "alert-type" => "success");
		    return redirect()->back()->with($noti);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    public function destroyAllReviews(Request $request){

	    try{

		    $ids = json_decode($request->delete_ids);
		    foreach ($ids as $key => $id){
			    $prodReview = Product_Review::findOrFail($id);
			    $prodReview->delete();
		    }

		    $noti = array("message" => "Selected Reviews deleted successfully", "alert-type" => "success");
		    return redirect()->back()->with($noti);
	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }



    public function indexStock(){

	    try{

		    $products = product::orderBy('id','DESC')->get();

		    //        foreach ($products as $key => $val){
		    //            $pp = products_meta::where(['product_id' => $val->id])->get();
		    //            foreach ($pp as $k => $v){
		    //                if($v['product_meta'] == "stock"){
		    //                    customVarDump($v['product_meta_value']);
		    //                }
		    //            }
		    //        }


		    return view('Products.stock.all',['products' => $products]);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }


    public function StockDetailEdit(Request $request ,$id){

	    try{

		    $metas = [
			    ['product_id'=> $id , "product_meta" => "stock_threshold"   , "product_meta_value" => $request->stock_threshold],
			    ['product_id'=> $id , "product_meta" => "stock"             , "product_meta_value" => $request->stock_quantity],
		    ];

		    foreach ($metas as $key => $val){
			    products_meta::updateOrInsert(['product_id' => $val['product_id'], 'product_meta' => $val['product_meta']], $val);
		    }

		    $noti = array("message" => "Stock of this product ".$id." details successfully updated", "alert-type" => "success");
		    return redirect()->back()->with($noti);


	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }



    //================ Product Bundles Functions Start

    public function pb_list(){

	    try{

		    $product = product::where(['product_type' => 'bundle_product'])->orderBy('id','DESC')->get();
		    return view('Products.product_bundles.list', ['products' => $product]);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }
    }

    public function pb_add(){

	    try{

		    $products_ = product::where('stock','>' ,0)->get();
		    $products  = [];

		    foreach ($products_ as $key => $product){
			    $metas = products_meta::where(['product_id' => $product->id])->get();
			    foreach ($metas as $k => $meta){
				    if(($meta['product_meta'] == 'stock') && ( $meta['product_meta_value'] >= 1 )){
					    $products [] = $product;
				    }
			    }
		    }

		    //=========================================

		    //Categories
		    $getCategoryTop = Category::where('status', 1)->where('top', 1)->get();
		    $getCategoryAll = Category::where('status', 1)
		                              ->orderby('id', 'asc')
		                              ->get()
		                              ->groupBy('Parent');

		    //=========================================

		    $getTags = Tag::all();
		    $artists = Artist::all();

		    return view('Products.product_bundles.add' , [
			    'products'       => $products,
			    'tags'           => $getTags,
			    'artists'        => $artists,
			    'getCategoryTop' => $getCategoryTop,
			    'getCategoryAll' => $getCategoryAll,
		    ]);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    public function pb_store(Request $request){

        $custom_message = [
            'bundle_title.required' => 'Bundle name is required!',
            'bundle_price.required' => 'Bundle price is required!',
            'bundle_stock.required' => 'Bundle stock is required!',
            'selected_products.required' => 'Products in bundle are required!',
        ];

        $this->validate( $request, [
            'bundle_title'      => 'required',
            'bundle_price'      => 'required',
            'bundle_stock'      => 'required',
            'selected_products' => 'required',
            'status'            => 'required',
        ], $custom_message );

        $selected_products      = serialize($request->selected_products);
        $product_meta_serialize = $request->except(['bundle_featured_img']);
        $product_meta_serialize = serialize($product_meta_serialize);
        $productID_MailUse      = "";

        try {

            DB::transaction(function () use ( $request , $product_meta_serialize , $selected_products ,$productID_MailUse ) {

                $path = "";
                $default_ = SiteSetting::where(['key' => 'default_product_stock'])->first();
                $default_stock = ( !empty($default_) ? $default_->value : 'no' );

                $product = new product();
                $product->user_id           = Auth::user()->id;
                $product->title             = $request->bundle_title;
                $product->description       = $request->bundle_description;
                $slug                       = str_slug($request->bundle_title, "-");
                $product->slug              = $slug;
                $product->price             = $request->bundle_price;
                $auto_gen_sku               = uniqid("sku-");
                $product->sku               = $auto_gen_sku;
                $product->product_type      = "bundle_product";
                $product->video             = !empty($request->bundle_video_link) ? $request->bundle_video_link : null ;
                $product->status            = ($request->has('status') && $request->status == "yes" ) ? 1 : 0;
                $product->stock             = $request->has('bundle_stock') ? $request->bundle_stock : $default_stock;
                $product->save();


                if($request->hasFile('bundle_featured_img')){
                    $sizes        = ['300','600','1024'];
                    $type         = "products";
                    $slug         = "bundle_product";
                    $otherFolders = "feature";
                    $file_key     = 'bundle_featured_img';
                    $path         = $this->imageResizing( $request , $product->id , $type, $slug , $file_key , $sizes , $otherFolders);
                    product::where(['id' => $product->id])->update([ 'image' => serialize($path) ]);
                }

                $product_id               = $product->id;
                $product->categories()->attach($request->bundle_category);
                $product->tags()->attach($request->bundle_tags);

                $bundle_prod_shipping_1 = isset($request->bundle_shipping_time1) ? $request->bundle_shipping_time1 : null;
                $bundle_prod_shipping_2 = isset($request->bundle_shipping_time2) ? $request->bundle_shipping_time2 : null;
                $estimate_shipping      = $bundle_prod_shipping_1."-".$bundle_prod_shipping_2;

                $bundle_date            = explode('-',$request->startandexpire);
                $bundle_start_date      = date("Y-m-d", strtotime($bundle_date[0]));
                $bundle_end_date        = date("Y-m-d", strtotime($bundle_date[1]));

                $product_metas = [
                    ['product_id'=> $product_id , "product_meta" => "bundle_start_date"           , "product_meta_value" => $bundle_start_date ?? null],
                    ['product_id'=> $product_id , "product_meta" => "bundle_end_date"             , "product_meta_value" => $bundle_end_date ?? null ],
                    ['product_id'=> $product_id , "product_meta" => "bundle_start_end"            , "product_meta_value" => $request->startandexpire ?? null],
                    ['product_id'=> $product_id , "product_meta" => "bundle_start_end_date_check" , "product_meta_value" => $request->bundle_start_end_date_check ?? null],
                    ['product_id'=> $product_id , "product_meta" => "bundle_product"              , "product_meta_value" => $selected_products],
                    ['product_id'=> $product_id , "product_meta" => "slug"                        , "product_meta_value" => $slug],
                    ['product_id'=> $product_id , "product_meta" => "artist"                      , "product_meta_value" => (  Auth::user()->id  ) ],
                    ['product_id'=> $product_id , "product_meta" => "artist_updated_separately"   , "product_meta_value" => (  Auth::user()->id  )  ],
                    ['product_id'=> $product_id , "product_meta" => "price"                       , "product_meta_value" => $request->bundle_price],
                    ['product_id'=> $product_id , "product_meta" => "sale_price"                  , "product_meta_value" => null ],
                    ['product_id'=> $product_id , "product_meta" => "sale_start_date"             , "product_meta_value" => null ],
                    ['product_id'=> $product_id , "product_meta" => "sale_end_date"               , "product_meta_value" => null ],
                    ['product_id'=> $product_id , "product_meta" => "sku"                         , "product_meta_value" => $auto_gen_sku],
                    ['product_id'=> $product_id , "product_meta" => "tags"                        , "product_meta_value" => serialize($request->bundle_tags)],
                    ['product_id'=> $product_id , "product_meta" => "categories"                  , "product_meta_value" => serialize($request->bundle_category)],
                    ['product_id'=> $product_id , "product_meta" => "featured_image"              , "product_meta_value" => serialize($path) ],
                    ['product_id'=> $product_id , "product_meta" => "stock"                       , "product_meta_value" => ( $request->has('bundle_stock') ? $request->bundle_stock : $default_stock ) ],
                    ['product_id'=> $product_id , "product_meta" => "material"                    , "product_meta_value" => null ],
                    ['product_id'=> $product_id , "product_meta" => "sizes"                       , "product_meta_value" => null ],
                    ['product_id'=> $product_id , "product_meta" => "weight"                      , "product_meta_value" => ( !empty($request->bundle_Weight) ? $request->bundle_Weight : null ) ],
                    ['product_id'=> $product_id , "product_meta" => "stock_manage_chk"            , "product_meta_value" => null ],
                    ['product_id'=> $product_id , "product_meta" => "stock_threshold"             , "product_meta_value" => null ],
                    ['product_id'=> $product_id , "product_meta" => "estimate_shipping"           , "product_meta_value" => $estimate_shipping],
                    ['product_id'=> $product_id , "product_meta" => "serialize_data"              , "product_meta_value" => $product_meta_serialize],
                    ['product_id'=> $product_id , "product_meta" => "product_video_description"   , "product_meta_value" => $request->bundle_video_description],
                    ['product_id'=> $product_id , "product_meta" => "product_video"               , "product_meta_value" => $request->bundle_video_link],
                    ['product_id'=> $product_id , "product_meta" => "up_sell"                     , "product_meta_value" => null ],
                    ['product_id'=> $product_id , "product_meta" => "upsell_amount"               , "product_meta_value" => null ],
                ];

                foreach ($product_metas as $k => $v) {
                    products_meta::updateOrInsert(['product_id' => $v['product_id'], 'product_meta' => $v['product_meta']], $v);
                }


            });

            $noti = array("message" => "Bundle Product created successfully!", "alert-type" => "success");
            return redirect()->route('product.bundle.list')->with($noti);

        } catch (\Exception $ex) {
            $noti = array("message" => $ex->getMessage(), "alert-type" => "error");
            return redirect()->route('product.bundle.add')->with($noti);
        }


    }

    public function pb_edit($id){

	    try{
		    //customVarDump_die($meta->product_meta_value);
		    $product = product::find(decrypt( $id ));

		    $meta = \App\products_meta::where(['product_id' => decrypt( $id )])->where(['product_meta' => 'bundle_product'])->first();

		    if ( ! empty( $product ) ) {

			    $all_products     = product::all();

			    $p_metas          = products_meta::where(['product_id' => decrypt( $id )])->get();
			    $tags             = Tag::all();
			    $categories       = Category::all();
			    $meta_data        = [];

			    foreach ($p_metas as $key => $val) {
				    $meta_data[$val['product_meta']] = $val['product_meta_value'];
			    }

			    //Review Section
			    $reviews         = Product_Review::where(['product_id' => decrypt( $id )])->get();

		    }


		    return view('Products.product_bundles.edit',
		                [
			                'productID'         => $product->id,
			                'product'           => $product,
			                'all_products'      => $all_products,
			                'product_meta'      => $meta_data,
			                'categories'        => $categories,
			                'tags'              => $tags,
			                'reviews'           => $reviews,
			                'meta'              => $meta->product_meta_value
		                ]);


	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return view( 'errors.404' );
	    }

    }

    public function pb_update(Request $request , $id){

	    try{

		    // dd($request->all());
		    // Previous Featured Image
		    $Product                         = Product::find($id);
		    $product_prev_featured_img       = $Product->image;
		    $bundle_prev_sku                 = $Product->sku;

		    $custom_message = [
			    'bundle_title.required'      => 'Bundle name is required!',
			    'bundle_price.required'      => 'Bundle price is required!',
			    'bundle_stock.required'      => 'Bundle stock is required!',
			    'selected_products.required' => 'Products in bundle are required!',
		    ];

		    $this->validate( $request, [
			    'bundle_title'               => 'required',
			    'bundle_price'               => 'required',
			    'bundle_stock'               => 'required',
			    'selected_products'          => 'required',
			    'status'                     => 'required',
		    ], $custom_message );


		    $selected_products               = serialize($request->selected_products);
		    $product_meta_serialize          = $request->except(['bundle_featured_img']);
		    $product_meta_serialize          = serialize($product_meta_serialize);
		    $slug                            = str_slug($request->bundle_title, "-");
		    $path                            = "";
		    $default_                        = SiteSetting::where(['key' => 'default_product_stock'])->first();
		    $default_stock                   = ( !empty($default_) ? $default_->value : 'no' );

		    $prod = Product::find($id)->update([
			                                       'user_id'                      => Auth::user()->id,
			                                       'title'                        => $request->bundle_title,
			                                       'description'                  => $request->bundle_description,
			                                       'slug'                         => $slug,
			                                       'price'                        => $request->bundle_price,
			                                       'sku'                          => $bundle_prev_sku,
			                                       'video'                        => (!empty($request->bundle_video_link) ? $request->bundle_video_link : null),
			                                       'status'                       => ($request->has('status') && $request->status == "yes" ) ? 1 : 0,
			                                       'stock'                        => (!empty($request->bundle_stock) ? $request->bundle_stock : $default_stock),
			                                       'product_type'                 => 'bundle_product'
		                                       ]);


		    if($request->hasFile('bundle_featured_img')){
			    $sizes                       = ['300','600','1024'];
			    $type                        = "products";
			    $slug                        = "bundle_product";
			    $otherFolders                = "feature";
			    $file_key                    = 'bundle_featured_img';
			    $path                        = $this->imageResizing( $request , $id , $type, $slug , $file_key , $sizes , $otherFolders);
			    product::where(['id' => $id])->update([ 'image' => serialize($path) ]);
		    }else{
			    product::where(['id' => $id])->update([ 'image' => $product_prev_featured_img ]);
		    }

		    $prod = Product::find($id);
		    $prod->categories()->sync($request->bundle_category);
		    $prod->tags()->sync($request->bundle_tags);

		    $bundle_prod_shipping_1          = isset($request->bundle_shipping_time1) ? $request->bundle_shipping_time1 : null;
		    $bundle_prod_shipping_2          = isset($request->bundle_shipping_time2) ? $request->bundle_shipping_time2 : null;
		    $estimate_shipping               = $bundle_prod_shipping_1."-".$bundle_prod_shipping_2;

		    if($request->bundle_start_end_date_check == '1'){
			    $bundle_date                 = explode('-',$request->startandexpire);
			    $bundle_start_date           = date("Y-m-d", strtotime($bundle_date[0]));
			    $bundle_end_date             = date("Y-m-d", strtotime($bundle_date[1]));
		    }

		    $product_metas = [
			    ['product_id'=> $id , "product_meta" => "bundle_start_date"           , "product_meta_value" => $bundle_start_date ?? null],
			    ['product_id'=> $id , "product_meta" => "bundle_end_date"             , "product_meta_value" => $bundle_end_date ?? null ],
			    ['product_id'=> $id , "product_meta" => "bundle_start_end"            , "product_meta_value" => $request->startandexpire ?? null],
			    ['product_id'=> $id , "product_meta" => "bundle_start_end_date_check" , "product_meta_value" => $request->bundle_start_end_date_check ?? null],
			    ['product_id'=> $id , "product_meta" => "bundle_product"              , "product_meta_value" => $selected_products],
			    ['product_id'=> $id , "product_meta" => "slug"                        , "product_meta_value" => $slug],
			    ['product_id'=> $id , "product_meta" => "artist"                      , "product_meta_value" => (Auth::user()->id)],
			    ['product_id'=> $id , "product_meta" => "artist_updated_separately"   , "product_meta_value" => (Auth::user()->id)],
			    ['product_id'=> $id , "product_meta" => "price"                       , "product_meta_value" => $request->bundle_price],
			    ['product_id'=> $id , "product_meta" => "sale_price"                  , "product_meta_value" => null ],
			    ['product_id'=> $id , "product_meta" => "sale_start_date"             , "product_meta_value" => null ],
			    ['product_id'=> $id , "product_meta" => "sale_end_date"               , "product_meta_value" => null ],
			    ['product_id'=> $id , "product_meta" => "sku"                         , "product_meta_value" => $bundle_prev_sku],
			    ['product_id'=> $id , "product_meta" => "tags"                        , "product_meta_value" => serialize($request->bundle_tags)],
			    ['product_id'=> $id , "product_meta" => "categories"                  , "product_meta_value" => serialize($request->bundle_category)],
			    ['product_id'=> $id , "product_meta" => "featured_image"              , "product_meta_value" => serialize($path) ],
			    ['product_id'=> $id , "product_meta" => "stock"                       , "product_meta_value" => ( $request->has('bundle_stock') ? $request->bundle_stock : $default_stock ) ],
			    ['product_id'=> $id , "product_meta" => "material"                    , "product_meta_value" => null ],
			    ['product_id'=> $id , "product_meta" => "sizes"                       , "product_meta_value" => null ],
			    ['product_id'=> $id , "product_meta" => "weight"                      , "product_meta_value" => ( !empty($request->bundle_Weight) ? $request->bundle_Weight : null ) ],
			    ['product_id'=> $id , "product_meta" => "stock_manage_chk"            , "product_meta_value" => null ],
			    ['product_id'=> $id , "product_meta" => "stock_threshold"             , "product_meta_value" => null ],
			    ['product_id'=> $id , "product_meta" => "estimate_shipping"           , "product_meta_value" => $estimate_shipping ?? null],
			    ['product_id'=> $id , "product_meta" => "serialize_data"              , "product_meta_value" => $product_meta_serialize],
			    ['product_id'=> $id , "product_meta" => "product_video_description"   , "product_meta_value" => $request->bundle_video_description],
			    ['product_id'=> $id , "product_meta" => "product_video"               , "product_meta_value" => $request->bundle_video_link],
			    ['product_id'=> $id , "product_meta" => "up_sell"                     , "product_meta_value" => null ],
			    ['product_id'=> $id , "product_meta" => "upsell_amount"               , "product_meta_value" => null ],
		    ];

		    foreach ($product_metas as $k => $v) {
			    products_meta::updateOrInsert(['product_id' => $v['product_id'], 'product_meta' => $v['product_meta']], $v);
		    }


		    $noti = array("message" => "Bundle Product Updated successfully!", "alert-type" => "success");
		    return redirect()->back()->with($noti);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    public function pb_destroy($id){

    }

    public function pb_destroyAll($id){

    }


    //================ Product Bundles Functions End


}
