<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;
use App\Product;
use App\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class CouponController extends Controller
{
    public function index()
    {
        $coupon         = Coupon::orderBy('id','DESC')->get();
        $category       = Category::get();
        $product        = Product::get();
        return view('coupon.all', ['coupon' => $coupon,'product' => $product,'category' => $category]);
    }

    public function store(Request $request)
    {
        $date               = $request->startandexpire;
        $coupon_date        = explode('-',$request->startandexpire);
        $exclude_products   = serialize($request->excludeproduct);
        $exclude_categories = serialize($request->excludecategory);

        $custom_message = [
            'coupon.required' => 'Coupon code is required!',
        ];

        $this->validate($request, [
            'coupon'   => 'required',
            'type'     => 'required',
            'discount' => 'required',
            'status'   => 'required'
        ], $custom_message);

        $start_date  = date("Y-m-d", strtotime($coupon_date[0]));
        $expire_date = date("Y-m-d", strtotime($coupon_date[1]));

        try{

            $coupon = new Coupon();
            $coupon->code            = $request->coupon;
            $coupon->type            = $request->type;
            $coupon->max_uses        = $request->usefrequency;
            $coupon->status          = $request->status;
            $coupon->discount        = $request->discount;
            $coupon->start_date      = $start_date;
            $coupon->expire_date	 = $expire_date;
            $coupon->startandexpire	 = $date;
            $coupon->save();
        //  $coupon->freedelivery	 = $request->delivery;
        //  $coupon->max_uses_user    = $request->maxusers;
        //  $coupon->exclude_product   = $exclude_products;
        //  $coupon->exclude_category  = $exclude_categories;

            $noti = array("message" => "Coupon created successfully!", "alert-type" => "success");
            return redirect()->route('coupon.list')->with($noti);

        }catch(\Exception $ex){
            $noti = array("message" =>$ex->getMessage(), "alert-type" => "danger");
            return redirect()->route('coupon.list')->with($noti);
        }
    }

    public function edit($id)
    {

        try {

            $coupon = Coupon::find(decrypt( $id ));

            if ( ! empty( $coupon ) ) {

                $category       = Category::get();
                $product        = Product::get();
                $start_date     =  date("d-m-Y", strtotime($coupon->start_date));
                $expire_date    = date("d-m-Y", strtotime($coupon->expire_date));
                $startandexpire = array($start_date,$expire_date);
                $startandexpire = (implode(" - ",$startandexpire));
                // $exclude_products   =  $coupon->exclude_product;
                // $exclude_category   =  $coupon->exclude_category;
                // $exclude_products   =  unserialize($exclude_products);
                // $exclude_categories =  unserialize($exclude_category);

                return view('coupon.edit',
                    [
                        'coupon'            => $coupon,
                        'startandexpire'    => $startandexpire,
                        // 'exclude_products'  => $exclude_products,
                        // 'exclude_categories'=> $exclude_categories,
                        // 'category'       => $category,
                        // 'product'        => $product
                    ]);

            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }

        return view( 'errors.404' );
    }

    public function update(Request $request , $id)
    {

        $date = $request->startandexpire;
        $start_date = explode('-',$request->startandexpire);
        // $exclude_products = serialize($request->excludeproduct);
        // $exclude_categories = serialize($request->excludecategory);

        $custom_message = [
            'coupon.required' => 'Coupon code is required!',
        ];

        $this->validate($request, [
            'coupon'     => 'required',
            'type'       => 'required',
            'discount'   => 'required',
            'status'     => 'required'
        ], $custom_message);

        $start_date[0] = str_replace("/","-", $start_date[0]);
        $start_date[0] = date("Y-m-d", strtotime($start_date[0]));

        $start_date[1] = str_replace("/","-", $start_date[1]);
        $start_date[1] = date("Y-m-d", strtotime($start_date[1]));


        try{

            $coupon = Coupon::find($id);
            $coupon->code            = $request->coupon;
            $coupon->type            = $request->type;
            $coupon->max_uses        = $request->usefrequency;
            // $coupon->freedelivery	 = $request->delivery;
            $coupon->status          = $request->status;
            $coupon->discount        = $request->discount;
            // $coupon->max_uses_user   = $request->maxusers;
            // $coupon->exclude_product = $exclude_products;
            // $coupon->exclude_category= $exclude_categories;
            $coupon->start_date      = $start_date[0];
            $coupon->expire_date	 = $start_date[1];
            $coupon->startandexpire	 = $date;
            $coupon->save();

            $noti = array("message" => "Coupon updated successfully!", "alert-type" => "success");
            return redirect()->route('coupon.list')->with($noti);

        }catch(\Exception $ex){
            $noti = array("message" =>$ex->getMessage(), "alert-type" => "danger");
            return redirect()->route('coupon.list')->with($noti);
        }
    }

    public function destroy($id)
    {
        coupon::where('id',$id)->delete();;
        $noti = array("message" => "Coupon deleted successfully", "alert-type" => "success");
        return redirect()->route('coupon.list')->with($noti);
    }

}
