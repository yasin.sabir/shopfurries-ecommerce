<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Event;
use App\SiteSetting;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){

        $type = "events";

    }


    public function index()
    {
        $events = Event::orderBy('id','DESC')->get();
        return view('events.list', ['events' => $events]);
    }


    public function get_single_event($name,$event_id ){

        $eventID = custom_base64_decode($event_id);
        $event = Event::find( $eventID );

        return view('front-views.event')->with(['event' => $event]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('events.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());

        $custom_msg = [
            'event_name.required'           => "Title is required!",
            'event_datetime.required'       => "Date & Time is required!",
            'event_address.required'        => "Address is required!",
            'event_city.required'           => "City is required!",
            'event_country.required'        => "Country is required!",
        ];

        $this->validate($request,[
            'event_name'           => "required",
            'event_datetime'       => "required",
            'event_address'        => "required",
            'event_city'           => "required",
            'event_country'        => "required",
        ],$custom_msg);

        $event = new Event();
        $event->name            = $request->event_name;
        $event->status          = "on";
        $event->address         = $request->event_address;
        $event->state           = (!empty($request->event_state) ? $request->event_state : null );
        $event->city            = $request->event_city;
        $event->country         = $request->event_country;
        $event->timing          = $request->event_datetime;
        $event->details         = (!empty($request->event_details) ? $request->event_details : null );
        $event->save();


        $default_path  = "images/event-places-img/map.jpg";
        $event_img     = $request->file('event_img') ? $request->file('event_img')->store('upload/events/event_'.$event->id, 'public') : $default_path;

        Event::where(['id' => $event->id])->update(['image' => $event_img]);

        if($request->hasFile('event_img')){
            $sizes = ['150','300','600','1024'];
            $type  = "events";
            $slug  = "event";
            $file_key = 'event_img';
            $this->imageResizing( $event->id, $type, $slug , $file_key , $sizes , $request);
        }

        $noti = array("message" => "Event created successfully!", "alert-type" => "success");
        return redirect()->route('event.list')->with($noti);

    }


    function imageResizing($id, $type , $slug , $file_key , $sizes , $request){

        if(!empty($sizes) && count($sizes) > 0){

            foreach ($sizes as $key => $val){
                $folderName   = $val."x".$val;
                $eventFolder  = $slug."_".$id;
                $directory    = "public/upload/".$type."/".$eventFolder."/".$folderName."/";
                Storage::makeDirectory($directory);
            }

            if($request->hasFile($file_key)){

                $image     = $request->file($file_key);
                $filename  = time() . '.' . $image->getClientOriginalExtension();

                foreach ($sizes as $key => $val){
                    $folderName     = $val."x"."$val";
                    $eventFolder    = $slug."_".$id;
                    $path           = storage_path('app/public/upload/'.$type.'/'.$eventFolder.'/'.$folderName.'/').$filename;
                    $img            = Image::make($image->getRealPath());
                    $img->resize($val, $val, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $img->save($path);
                }

            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $event = Event::find( decrypt( $id ));

            if ( ! empty( $event ) ) {
                return view('events.edit',['event' => $event]);
            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }
        return view( 'errors.404' );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $custom_msg = [
            'event_name.required'           => "Title is required!",
            'event_datetime.required'       => "Date & Time is required!",
            'event_address.required'        => "Address is required!",
            'event_city.required'           => "City is required!",
            'event_country.required'        => "Country is required!",
        ];

        $this->validate($request,[
            'event_name'           => "required",
            'event_datetime'       => "required",
            'event_address'        => "required",
            'event_city'           => "required",
            'event_country'        => "required",
        ],$custom_msg);


        $prev_event_image = $request->prev_event_image;
        $event_img        = $request->file('event_img') ? $request->file('event_img')->store('upload/events/event_'.$id, 'public') : $prev_event_image;

        Event::where(['id' => $id])->update([
            'name'              => $request->event_name,
            'address'           => $request->event_address ,
            'state'             => (!empty($request->event_state) ? $request->event_state : null ),
            'city'              => $request->event_city,
            'country'           => $request->event_country,
            'timing'            => $request->event_datetime,
            'image'             => $event_img,
            'details'           => (!empty($request->event_details) ? $request->event_details : null ),
        ]);

        if($request->hasFile('event_img')){
            $sizes = ['150','300','600','1024'];
            $type  = "events";
            $slug  = "event";
            $file_key = 'event_img';
            $this->imageResizing( $id, $type, $slug , $file_key , $sizes , $request);
        }

        $noti = array("message" => "Event updated successfully!", "alert-type" => "success");
        return redirect()->route('event.list')->with($noti);

    }


    public function updateStatus(Request $request, $id){

        if( isset($request->status) && $request->status == "on" ){

            Event::where(['id' => $id])->update(['status' => $request->status]);
            $msg = "Status is on";
            return response()->json(array(['msg'=> $msg , 'val' => 'checked']), 200);

        }else if(isset($request->status) && $request->status == "off"){

            Event::where(['id' => $id])->update(['status' => $request->status]);
            $msg = "Status is off";
            return response()->json(array(['msg'=> $msg , 'val' => 'unchecked']), 200);

        }else{

        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();

        $noti = array("message" => "Event deleted successfully!", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }


    public function destroyAll(Request $request){

        $ids = json_decode($request->delete_ids);

        //  customVarDump_die($ids);

        foreach ($ids as $key => $id){
            $event = Event::findOrFail($id);
            $event->delete();
        }

        $noti = array("message" => "Selected Events deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }
}
