<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Mail\VerifyMail;
use App\User;
use App\UserMeta;
use App\VerifyUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::role( 'user' )->orderBy( 'id', 'DESC' )->get();

        return view( 'users.all', [ 'users' => $users ] );
    }


    public function customersList()
    {
        $customers = User::role( 'customer' )->orderBy( 'id', 'DESC' )->get();

        return view( 'users.customers', [ 'customers' => $customers ] );
    }

    public function vendorsList()
    {
        $vendors = User::role( 'vendor' )->get();

        return view( 'users.vendors', [ 'vendors' => $vendors ] );
    }

    public function UserList()
    {
        $users = User::role( 'user' )->get();

        return view( 'users.all', [ 'users' => $users ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view( 'users.add' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        //
        return view( 'users.show' );
    }

    public function show_account()
    {
        $id   = Auth::user()->id;
        $user = User::find( $id );

        $user_meta = [];
        foreach ( $user->metas as $key => $val ) {
            $user_meta[ $val->meta_key ] = $val->meta_value;
        }

        return view( 'users.account.account', [ 'user' => $user, 'user_meta' => $user_meta ] );
    }

    public function edit_account( $id )
    {
        try {

            $user = User::find( decrypt( $id ) );

            if ( ! empty( $user ) ) {

                $user_meta = [];
                foreach ( $user->metas as $key => $val ) {
                    $user_meta[ $val->meta_key ] = $val->meta_value;
                }

            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }


        return view( 'users.account.edit-account', [ 'user' => $user, 'user_meta' => $user_meta ] );
    }

    public function update_account( Request $request, $id )
    {
        $user = User::find( $id );

        $custom_msg = [
            'user_fullname.required'   => 'Full name is required',
            'user_email.required'      => 'Email is required',
            'user_telno.required'      => 'Tel #No is required',
            'user_country.required'    => 'Country is required',
            'user_city.required'       => 'City is required',
            'user_streetname.required' => 'Street Name is required',
            'user_streetno.required'   => 'Street No# is required',
        ];

        $this->validate( $request, [
            'user_fullname'   => 'required',
            'user_email'      => 'required|max:255|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:users,email,' . $user->id,
            'user_telno'      => 'required|numeric',
            'user_country'    => 'required',
            'user_city'       => 'required',
            'user_streetname' => 'required',
            'user_streetno'   => 'required',
        ], $custom_msg );


        $user->name  = $request->user_fullname;
        $user->email = $request->user_email;

        if($user->provider == 'deviantart' && $user->email_verified_at == null){
            // Send verify email to user
            $token =  Str::random(40);
            $verifyUser = VerifyUser::create([ 'user_id' => $user->id, 'token' => $token]);
            Mail::to($request->user_email)->send(new VerifyMail($token));
        }


        if ( $request->hasFile( 'profile_pic' ) ) {
            $user->profile_pic = $request->file( 'profile_pic' )->store( 'upload/users/user_' . $user->id . '/profile_pic', 'public' );
        }

        if ( $user->save() ) {

            $meta_data = [
                [ 'user_id' => $user->id, 'meta_key' => 'street_name', 'meta_value' => $request->user_streetname ],
                [ 'user_id' => $user->id, 'meta_key' => 'street_no', 'meta_value' => $request->user_streetno ],
                [ 'user_id' => $user->id, 'meta_key' => 'country', 'meta_value' => $request->user_country ],
                [ 'user_id' => $user->id, 'meta_key' => 'city', 'meta_value' => $request->user_city ],
                [ 'user_id' => $user->id, 'meta_key' => 'telno', 'meta_value' => $request->user_telno ],
                [ 'user_id' => $user->id, 'meta_key' => 'shipping_address','meta_value' => $request->user_shipping_add ],
                [ 'user_id' => $user->id, 'meta_key' => 'billing_address', 'meta_value' => $request->user_billing_add ],
            ];

            foreach ( $meta_data as $k => $v ) {
                UserMeta::updateOrInsert( [ 'user_id' => $v['user_id'], 'meta_key' => $v['meta_key'] ], $v );
            }

            if ( $request->hasFile( 'profile_pic' ) ) {
                $sizes    = [ '150' ];
                $type     = "users";
                $slug     = "user";
                $file_key = 'profile_pic';
                $this->imageResizing( $user->id, $type, $slug, $file_key, $sizes , $request );
            }

            $noti = array( "message" => "Profile updated successfully!", "alert-type" => "success" );
            return Redirect::back()->with( $noti );

        }

    }

    public function verifyUser_socialAuth( Request $request , $token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();

        if (isset($verifyUser)) {
            $user = $verifyUser->user;
            if (!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->email_verified_at = Carbon::now()->toDateTimeString();
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }

        return redirect('/login')->with('status', $status);
    }


    function imageResizing( $id, $type, $slug, $file_key, $sizes , $request )
    {

        if ( ! empty( $sizes ) && count( $sizes ) > 0 ) {

            foreach ( $sizes as $key => $val ) {
                $folderName  = $val . "x" . $val;
                $eventFolder = $slug . "_" . $id;
                $directory   = "public/upload/" . $type . "/" . $eventFolder . "/" . $folderName . "/";
                Storage::makeDirectory( $directory );
            }

            if ( $request->hasFile( $file_key ) ) {

                $image    = $request->file( $file_key );
                $filename = time() . '.' . $image->getClientOriginalExtension();

                foreach ( $sizes as $key => $val ) {
                    $folderName  = $val . "x" . "$val";
                    $eventFolder = $slug . "_" . $id;
                    $path        = storage_path( 'app/public/upload/' . $type . '/' . $eventFolder . '/' . $folderName . '/' ) . $filename;
                    $img         = Image::make( $image->getRealPath() );
                    $img->resize( $val, $val, function ( $constraint ) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    } );
                    $img->save( $path );
                }

            }
        }

    }

    public function changePasswordView()
    {
        return view( 'users.account.changePassword.change-password' );
    }


    public function updatePassword( Request $request, $id )
    {

        $custom_msg = [
            'new_password.required' => 'Password Field is required',
        ];

        $this->validate( $request, [
            'new_password'     => 'required|required_with:confirm_password|same:confirm_password|min:5',
            'confirm_password' => 'min:5'
        ], $custom_msg );

        User::find( $id )->update( [ 'password' => Hash::make( $request->new_password ) ] );

        $noti = array( "message" => "Password updated successfully!", "alert-type" => "success" );

        return Redirect::back()->with( $noti );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        //
        return view( 'users.edit' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        //
    }

    public function destroyAllUsers( Request $request )
    {
        $ids = json_decode( $request->delete_ids );
        customVarDump_die( $ids );
        //        foreach ($ids as $key => $id){
        //            $user = User::findOrFail($id);
        //            $user->delete();
        //        }
        //
        //        $noti = array("message" => "Selected users are deleted successfully", "alert-type" => "success");
        //        return redirect()->back()->with($noti);
    }

    public function user_status( Request $request )
    {
        $status = $request->status;
        //dd($status);
        $user = User::find( $request->user_id );
        if ( $status == "on" ) {
            $user->status = 1;
        } else if ( $status == "off" ) {
            $user->status = 0;
        }
        $user->save();

    }

}
