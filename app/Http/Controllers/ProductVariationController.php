<?php

namespace App\Http\Controllers;

use App\Event;
use App\Product;
use App\productAttribute;
use App\productVariation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProductVariationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //get all variations
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = product::orderBy('id','DESC')->get();

        $attributes = productAttribute::all();

        return view('variations.add' , ['products' => $products , 'attributes' => $attributes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_attr(Request $request){

        $custom_msg = [
            'attr_name' => 'Attribute name is required!',
            'attr_value' => 'Attribute value is required',
        ];

        $this->validate($request, [
            'attr_name' => 'required',
            'attr_value' => 'required',
        ],$custom_msg);

        $values = array_map('trim', explode('|', $request->attr_value));

        $product_attr = new productAttribute();
        $product_attr->product_id = $request->product_id;
        $product_attr->attribute  = $request->attr_name;
        $product_attr->value      = serialize($values);
        $product_attr->save();


        $noti = array("message" => "Attribute created successfully!", "alert-type" => "success");
        return redirect()->back()->with($noti);

    }


    // store variation
    public function store(Request $request)
    {
        $prev_variation = [];

        $p_v = productVariation::where(['product_id' => $request->product_id])->get();
        foreach ($p_v as $k => $v){
            $prev_variation [] = unserialize($v->variation);
        }


        foreach ($request->singleVariation as $key => $val){
            $new_variation[$key] = strtolower($val['attribute'])."-".strtolower($val["variation"]);
        }

        $new_var_meta = $request->singleVariation ;

        if(in_array($new_variation , $prev_variation)){

            $msg = "Variation is already exist!";
            return response()->json(array(['msg'=> $msg , 'val' => 'Not Done']), 422);

        }else{
            $prod_variation                 = new productVariation();
            $prod_variation->product_id     = $request->product_id;
            $prod_variation->variation      = serialize($new_variation);
            $prod_variation->variation_meta = serialize($new_var_meta);
            $prod_variation->save();

            $msg = "Variation created successfully";
            return response()->json(array(['msg'=> $msg , 'val' => 'Done']), 200);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        $custom_msg = [
            'update_vari_price.required' => 'Price of variation is required!',
        ];
        $this->validate($request , [
            'update_vari_price' => 'required',
        ],$custom_msg);

        $prev_variation_thumbnail = $request->prev_variation_thumbnail;

        productVariation::where(['id' => $id])->update([
            'image'             => $request->file('variation_thumbnail') ? $request->file('variation_thumbnail')->store('upload/product_variation/product_'.$request->product_id.'/variation_'.$id.'/thumbnail', 'public') : $prev_variation_thumbnail,
            'price'             => $request->update_vari_price,
            'sale_price'        => $request->update_vari_sale_price,
            'stock'             => $request->update_vari_stock,
            'sku'               => $request->update_vari_sku,
            'description'       => $request->update_vari_description
        ]);

        $msg = "Variation updated Successfully";
        return response()->json(array(['msg'=> $msg , 'val' => 'Done']), 200);
    }

    // product variation status
    public function updateStatus(Request $request, $id){

        if( isset($request->status) && $request->status == "on" ){

            productVariation::where(['id' => $id])->update(['status' => $request->status]);
            $msg = "Status is on";
            return response()->json(array(['msg'=> $msg , 'val' => 'checked']), 200);

        }else if(isset($request->status) && $request->status == "off"){

            productVariation::where(['id' => $id])->update(['status' => $request->status]);
            $msg = "Status is off";
            return response()->json(array(['msg'=> $msg , 'val' => 'unchecked']), 200);

        }else{

        }
    }


    public function update_attr(Request $request , $id){

        $custom_msg = [
            'attr_name.required' => 'Attribute name is required!',
            'attr_value.required' => 'Attribute value is required!'
        ];
        $this->validate($request , [
            'attr_name' => 'required',
            'attr_value' => 'required'
        ],$custom_msg);


        $attr_values = explode("|",$request->attr_value);
        productAttribute::where(['id' => $id])->update([
            'attribute' => $request->attr_name,
            'value'     => serialize($attr_values)
        ]);

        $msg = "Attribute updated Successfully";
        return response()->json(array(['msg'=> $msg , 'val' => 'Done']), 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function destroy_attr($id)
    {
        $p_Attr = productAttribute::findOrFail($id);
        $p_Attr->delete();

        $msg = "Delete Successfully";
        return response()->json(array(['msg'=> $id , 'val' => 'Done']), 200);
    }

    public function destroy($id)
    {
        $p_Variation = productVariation::findOrFail($id);
        $p_Variation->delete();

        $msg = "Delete Successfully";
        return response()->json(array(['msg'=> $msg , 'val' => 'Done']), 200);
    }
}
