<?php

namespace App\Http\Controllers;

use App\Category_Meta;
use App\productimgtemp;
use App\Category;
use App\products_gallery;
use App\Tag;
use App\Product;
use App\products_meta;
use File;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BulkProductControler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //        $custom_msg = [
        //            'category_name' => 'Category Name is required!'
        //        ];
        //
        //        $this->validate($request,[
        //            'category_name' => 'required'
        //        ],$custom_msg);
        //
        //        if ($request->file('file')) {
        //            $file = $request->file;
        //            foreach ($file as $image) {
        //
        //                $productimgtemp = new productimgtemp();
        //                $productimgtemp->name = str_replace([".png" , ".jpg" , ".jpeg"],'',$image->getClientOriginalName());
        //                $productimgtemp->image = $image->store('upload/product_temperory_img', 'public');
        //                $productimgtemp->category_id  = $request->category_name;
        //                $productimgtemp->save();
        //            }
        //
        //        }

        return redirect()->route('product.bulk.show');

    }

    public function tempUpload(Request $request){

	    try{

		    //        $custom_msg = [
		    //            'category_name' => 'Category Name is required!'
		    //        ];
		    //
		    //        $this->validate($request,[
		    //            'category_name' => 'required'
		    //        ],$custom_msg);

		    $validator = Validator::make($request->all(), [
			    'category_name' => 'required'
		    ]);

		    if ($validator->fails()) {
			    return response()->json('Category is required!', 422);
		    }

		    if($file = $request->hasFile('products')) {

			    if ($request->file('products')) {
				    $file = $request->file('products');
				    foreach ($file as $image) {
					    $productImgTemp = new productimgtemp();
					    $productImgTemp->name = str_replace([".png" , ".jpg" , ".jpeg"],'',$image->getClientOriginalName());
					    $productImgTemp->image = $image->store('upload/product_temperory_img', 'public');
					    $productImgTemp->category_id  = $request->category_name;
					    $productImgTemp->save();

					    return response()->json(['uploaded' => $image->getClientOriginalName()]);
				    }
			    }
		    }
		    die;

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }


    public function tempDelete(Request $request){

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	try{

		    $category = Category::all();
		    return view('Bulkproducts.add' ,['categories' => $category]);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }


    public function multiUploader(Request $request){

	    try{
		    $product_meta_serialize = serialize($request->all());
		    $product_images_ids  = $request->image_row_id; // For delete data form producttempimg table
		    $product_titles      = $request->product_name;
		    $product_images      = $request->image_name;
		    $product_prices      = $request->price;
		    $product_materials   = $request->material;
		    $product_sizes       = $request->sizes;
		    $product_artist      = $request->artist;

		    $product_category    = implode(",",array_unique($request->categories));
		    $product_tags        = implode(",",array_unique($request->tags));
		    $product_tags        = unserialize($product_tags);

		    $i = 0;
		    foreach ($product_titles as $key => $val){
			    $product                = new product();
			    $product->user_id       = Auth::user()->id;
			    $product->title         = ( !empty($val) ? $val : 'Not Available' );
			    $product->description   = null;
			    $slug                   = str_slug($val, "-");
			    $product->slug          = $slug;
			    $product->price         = $product_prices[$i];
			    $product->sku           = null;
			    $image                  = explode('/',$request->image_name[$i]);
			    $old_path               = 'public/upload/product_temperory_img/'.$image[2];
			    $new_path               = 'public/upload/product/'.$slug.'/feature/'.$image[2];
			    $db_path                = 'upload/product/'.$slug.'/feature/'.$image[2];
			    $product->image         =  $db_path;
			    $product->video         = null;
			    $product->status        = 1;
			    $product->stock         = null;
			    $product->product_type  = 'product';
			    Storage::copy($old_path ,$new_path );
			    $product->save();

			    $product_id = $product->id;

			    if(isset($product_category) && !empty($product_category)){
				    $product->categories()->attach($product_category);
			    }

			    if(isset($product_tags) && !empty($product_tags)){
				    foreach ($product_tags as $key1 => $val1){
					    $product->tags()->attach($val1);
				    }
			    }


			    $product_metas = [
				    ['product_id'=> $product_id , "product_meta" => "slug"                                    , "product_meta_value" => $slug],
				    ['product_id'=> $product_id , "product_meta" => "artist"                                  , "product_meta_value" => ( !empty($product_artist[$i]) ? $product_artist[$i] : null )],
				    ['product_id'=> $product_id , "product_meta" => "artist_updated_separately"               , "product_meta_value" => ( !empty($product_artist[$i]) ? "no" : "no" )],
				    ['product_id'=> $product_id , "product_meta" => "price"                                   , "product_meta_value" => $product_prices[$i]],
				    ['product_id'=> $product_id , "product_meta" => "sale_price"                              , "product_meta_value" => ""],
				    ['product_id'=> $product_id , "product_meta" => "sale_start_date"                         , "product_meta_value" => ""],
				    ['product_id'=> $product_id , "product_meta" => "sale_end_date"                           , "product_meta_value" => ""],
				    ['product_id'=> $product_id , "product_meta" => "sku"                                     , "product_meta_value" => ""],
				    ['product_id'=> $product_id , "product_meta" => "tags"                                    , "product_meta_value" => serialize($request->tags)],
				    ['product_id'=> $product_id , "product_meta" => "categories"                              , "product_meta_value" => serialize($request->categories)],
				    ['product_id'=> $product_id , "product_meta" => "featured_image"                          , "product_meta_value" => $db_path],
				    ['product_id'=> $product_id , "product_meta" => "stock"                                   , "product_meta_value" => ""],
				    ['product_id'=> $product_id , "product_meta" => "material"                                , "product_meta_value" => $product_materials[$i]],
				    ['product_id'=> $product_id , "product_meta" => "sizes"                                   , "product_meta_value" => $product_sizes[$i]],
				    ['product_id'=> $product_id , "product_meta" => "product_features_description"            , "product_meta_value" => ""],
				    ['product_id'=> $product_id , "product_meta" => "stock_manage_chk"                        , "product_meta_value" => ""],
				    ['product_id'=> $product_id , "product_meta" => "stock_threshold"                         , "product_meta_value" => ""],
				    ['product_id'=> $product_id , "product_meta" => "serialize_data"                          , "product_meta_value" => $product_meta_serialize],
				    ['product_id'=> $product_id , "product_meta" => "product_video_description"               , "product_meta_value" => ""],
				    ['product_id'=> $product_id , "product_meta" => "product_video"                           , "product_meta_value" => ""],
			    ];

			    foreach ($product_metas as $key3 => $value) {
				    products_meta::updateOrInsert(['product_id' => $value['product_id'], 'product_meta' => $value['product_meta']], $value);
			    }

			    $i++;
		    }

		    foreach ($product_images_ids as $key => $id){
			    productimgtemp::where(['id' => $id])->delete();
		    }

		    $noti = array("message" => "Products are created successfully!", "alert-type" => "success");
		    return redirect()->route('product.list')->with($noti);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    public function createAll(Request $request){

	    try{

		    $custom_message = [
			    'product_name.*.required' => 'Product Name is required!',
			    'sku.*.required' => 'Product Sku is required!',
			    'price.*.required' => 'Product Price is required!',
			    'stock.*.required' => 'Product Stock is required!',
			    'status.*.required' => 'Product Status is required!',
		    ];

		    $this->validate($request, [
			    'product_name.*' => 'required',
			    'sku.*'          => 'required',
			    'price.*'        => 'required|numeric',
			    'status.*'       => 'required',
			    'stock.*'        => 'required|numeric',
		    ], $custom_message);

//        customVarDump_die($request->all());
//        die;

		    $product_meta_serialize = serialize($request->all());

		    $product_titles      = $request->product_name;
		    $product_images      = $request->image_name;
		    $product_images_ids  = $request->image_row_id;
		    $product_skus        = $request->sku;
		    $product_prices      = $request->price;
		    $product_status      = $request->status;
		    $product_stock       = $request->stock;
		    $product_categories  = $request->categories;
		    $product_tags        = $request->tags;

		    $categories = [];
		    $tags       = [];

		    $i = 0;
		    foreach ($product_titles as $key => $val){
			    //customVarDump($product_stock["stock_".$i]);
			    $product                = new product();
			    $product->user_id       = Auth::user()->id;
			    $product->title         = $val;
			    $product->description   = null;
			    $slug                   = str_slug($val, "-");
			    $product->slug          = $slug;
			    $product->price         = $product_prices[$i];
			    $product->sku           = $product_skus[$i];
			    $image                  = explode('/',$request->image_name[$i]);
			    $old_path               = 'public/upload/product_temperory_img/'.$image[2];
			    $new_path               = 'public/upload/product/'.$slug.'/feature/'.$image[2];
			    $db_path                = 'upload/product/'.$slug.'/feature/'.$image[2];
			    $product->image         =  $db_path;
			    $product->video         = 'null';
			    $product->status        = $product_status[$i];
			    $product->stock         = $product_stock[$i];
			    Storage::copy($old_path ,$new_path );
			    $product->save();

			    $product_id = $product->id;

			    if(isset($request->categories["categories_".$i]) && !empty($request->categories["categories_".$i])){
				    foreach ($request->categories["categories_".$i] as $key1 => $val1){
					    $product->categories()->attach($val1);
					    $categories[$key1] = $val1;
				    }
			    }

			    if(isset($request->tags["tags_".$i]) && !empty($request->tags["tags_".$i])){
				    foreach ($request->tags["tags_".$i] as $key2 => $val2){
					    $product->tags()->attach($val2);
					    $tags[$key2] = $val2;
				    }
			    }

			    $product_metas = [
				    'slug'                         => $slug,
				    'price'                        => $product_prices[$i],
				    'sale_price'                   => "",
				    'sale_start_date'              => "",
				    'sale_end_date'                => "",
				    'sku'                          => $product_skus[$i],
				    'tags'                         => serialize($tags),
				    'categories'                   => serialize($categories),
				    'featured_image'               => $db_path,
				    'stock'                        => $product_stock[$i],
				    'material'                     => "",
				    'sizes'                        => "",
				    'product_features_description' => "",
				    'stock_manage_chk'             => "",
				    'stock_threshold'              => "",
				    'serialize_data'               => $product_meta_serialize,
				    'product_video_description'    => "",
				    'product_video'                => "",
			    ];

			    foreach ($product_metas as $key3 => $value) {
				    $product_meta                      = new products_meta();
				    $product_meta->product_id          = $product_id;
				    $product_meta->product_meta        = $key3;
				    $product_meta->product_meta_value  = $value;
				    $product_meta->save();
			    }

			    $i++;
		    }


		    foreach ($product_images_ids as $key => $id){
			    productimgtemp::where(['id' => $id])->delete();
		    }

		    $noti = array("message" => "Products are created successfully!", "alert-type" => "success");
		    return redirect()->route('product.list')->with($noti);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }



    }

    public function insert(Request $request)
    {
        //dd($request->all());
        $image_name = $request->image_name;
        $row = $request->row;
        $product_meta_serialize = $request->all();
        $product_meta_serialize = serialize($product_meta_serialize);
        //dd($product_meta_serialize);

        try {

            DB::transaction(function () use ($request, $product_meta_serialize) {

                $product = new product();
                $product->title = $request->product_name;
                $product->description = $request->description;
                $slug = str_slug($request->product_name, "-");
                $product->slug = $slug;
                $product->price = $request->price;
                $product->sku = '';
                $product->image  = $request->image_name;
                $product->status = 1;
                $product->stock = 100;
                $product->save();

                $product_id = $product->id;

                $product->categories()->attach($request->categories);

                $product->tags()->attach($request->tags);

                $sales_price_schd = explode('-', $request->sales_price_schd);
                $product_metas = [
                    'slug' => $slug,
                    'price' => $request->price,
                    'sale_price' => '',
                    'sale_start_date' => '',
                    'sale_end_date' => '',
                    'sku' => '',
                    'tags' => serialize($request->tags),
                    'categories' => serialize($request->categories),
                    'featured_image' => $request->file('featured_img') ? $request->file('featured_img')->store('upload/product/' . $slug . '/feature', 'public') : 'null',
                    'stock' => 100,
                    'stock_manage_chk' => '',
                    'stock_threshold' => '',
                    'serialize_data' => $product_meta_serialize
                ];


                foreach ($product_metas as $key => $value) {
                    $product_meta = new products_meta();
                    $product_meta->product_id = $product_id;
                    $product_meta->product_meta = $key;
                    $product_meta->product_meta_value = $value;
                    $product_meta->save();
                }

                $productimgtemp = productimgtemp::find($request->image_row_id)->delete();
            });

            $noti = array("message" => "Product created successfully!", "alert-type" => "success");
            return response()->json(['ok' => 'Product Insert Successfully','row'=>$row]);

           // return redirect()->route('product.list')->with($noti);

        } catch (\Exception $ex) {
            $noti = array("message" => $ex->getMessage(), "alert-type" => "error");
            return response()->json(['ok' => 'Product Insert Failed','row'=>$row]);
            //return redirect()->route('product.add')->with($noti);
        }


    }

    function imageResizing($id, $type , $slug , $file_key , $sizes , $other_folders = false , $request){

	    try{


		    $savePath = [];

		    if(!empty($sizes) && count($sizes) > 0){

			    foreach ($sizes as $key => $val){

				    $folderName   = $val."x".$val;
				    $eventFolder  = $slug."_".$id;

				    if($other_folders){
					    $otherFolders = $other_folders;
					    $directory    = "public/upload/".$type."/".$eventFolder."/".$otherFolders."/".$folderName."/";
				    }else{
					    $directory    = "public/upload/".$type."/".$eventFolder."/".$folderName."/";
				    }
				    Storage::makeDirectory($directory);
			    }

			    if($request->hasFile($file_key)){

				    //For Multiple Images
				    if(is_array($request->file($file_key))){

					    $images     = $request->file($file_key);
					    foreach ($images as $k => $v){

						    $filename  = time() . '.' . $v->getClientOriginalExtension();

						    foreach ($sizes as $key => $val){
							    $folderName     = $val."x"."$val";
							    $folderSlug    = $slug."_".$id;

							    if($other_folders){
								    $otherFolders   = $other_folders;
								    $path           = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/').$filename;
								    $savePath []    = 'upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/'.$filename;
							    }else{
								    $path           = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$folderName.'/').$filename;
								    $savePath []    = 'upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/'.$filename;
							    }

							    $img            = \Intervention\Image\Facades\Image::make( $v->getRealPath());
							    $img->resize($val, $val, function ($constraint) {
								    $constraint->aspectRatio();
								    $constraint->upsize();
							    });
							    $img->save($path);
						    }
					    }


				    }else{

					    //For Single Image
					    $image     = $request->file($file_key);
					    $filename  = time() . '.' . $image->getClientOriginalExtension();

					    foreach ($sizes as $key => $val){
						    $folderName     = $val."x"."$val";
						    $folderSlug    = $slug."_".$id;

						    if($other_folders){
							    $otherFolders       = $other_folders;
							    $path               = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/').$filename;
							    $savePath [$val]    = 'upload/'.$type.'/'.$folderSlug.'/'.$otherFolders.'/'.$folderName.'/'.$filename;
						    }else{
							    $path               = storage_path('app/public/upload/'.$type.'/'.$folderSlug.'/'.$folderName.'/').$filename;
							    $savePath [$val]    = 'upload/'.$type.'/'.$folderSlug.'/'.$folderName.'/'.$filename;
						    }
						    $img                = \Intervention\Image\Facades\Image::make( $image->getRealPath());
						    $img->resize($val, $val, function ($constraint) {
							    $constraint->aspectRatio();
							    $constraint->upsize();
						    });
						    $img->save($path);

					    }
				    }

			    }


		    }
		    return $savePath;

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $noti = array("message" => "Product created successfully!", "alert-type" => "success");
            return redirect()->route('product.bulk.list')->with($noti);

        } catch (\Exception $ex) {
            $noti = array("message" => $ex->getMessage(), "alert-type" => "error");
            return redirect()->route('product.bulk.add')->with($noti);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
	    try{

		    $category       = Category::get();
		    $tag            = Tag::get();
		    $product        = product::get();
		    $products_meta  = products_meta::get();
		    $data           = productimgtemp::all();
		    $image_details  = [];

		    $cate_id        = "";
		    foreach ($data->toArray() as $key => $val){
			    $cate_id = $val['category_id'];
		    }


		    if(!empty($cate_id)) {
			    $cate = Category::find($cate_id);
			    $category = [
				    'category_id'             => $cate->id,
				    'category_name'           => $cate->Name,
				    'category_Alias'          => $cate->Alias,
				    'category_Image'          => $cate->Image,
				    'category_Parent'         => $cate->Parent,
				    'category_Top'            => $cate->Top,
				    'category_Status'         => $cate->Status,
				    'category_Additional_Req' => $cate->Additional_Req,
			    ];

			    $metas = Category_Meta::where(['category_id' => $cate_id])->get();

			    $category_meta = [];
			    foreach ($metas as $key => $val){
				    $category_meta[$val->meta_key] = $val->meta_value;
			    }

			    $final_merge =[];
			    foreach ($data->toArray() as $key => $val){
				    $merge1       = array_merge($val,$category);
				    $final_merge [] = array_merge($merge1,$category_meta);
				    //            customVarDump($final_merge);
				    //            $image_details [$key]['category'] = $val;
				    //            $image_details [$key]['category_meta']   = $category_meta;
			    }
			    //        customVarDump($final_merge[0]);
			    //        die;

			    return view('Bulkproducts.all',
			                [
				                'category'      => $category,
				                'tags'          => $tag,
				                'product'       => $product,
				                'products_meta' => $products_meta,
				                'images'        => $final_merge
			                ]);

		    }


		    return view('Bulkproducts.all',
		                [
			                'category'      => $category,
			                'tags'          => $tag,
			                'product'       => $product,
			                'products_meta' => $products_meta,
			                'images'        => array()
		                ]);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    try{
		    return view('Bulkroducts.edit');
	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    try{

		    $noti = array("message" => "Product Updated successfully!", "alert-type" => "success");
		    return redirect()->route('product.bulk.list')->with($noti);
	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    try{

		    $msg = "Deleted";
		    productimgtemp::where(['id' => $id])->delete();
		    return response()->json(array(['msg'=> $msg , 'val' => $id]), 200);

	    }catch (\Exception $ex){
		    $noti = array("message" => "Error", "alert-type" => "error");
		    return redirect()->back()->with($noti);
	    }

    }
}



