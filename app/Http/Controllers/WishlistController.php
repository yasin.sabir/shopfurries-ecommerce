<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Product;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Display products according to users
        $user_id = Auth::user()->id;

        //Wishlist::where(['user_id' => $user_id])->orderBy('id','DESC')->get();
        $wishlist_products1 = Wishlist::where(['user_id' => $user_id])->where(['type' => 'art_work'])->get();
        $wishlist_products2 = Wishlist::where(['user_id' => $user_id])->where(['type' => 'product'])->get();

        $art_works = [];
        $products = [];

        foreach ($wishlist_products1 as $key => $val){
            $prods = Gallery::where(['id' => $val->product_id])->first();
            $art_works[] = $prods;
        }

        foreach ($wishlist_products2 as $key => $val){
            $prods = product::where(['id' => $val->product_id])->first();
            $products[] = $prods;
        }

        return view('wishlist.all' , ['art_works' => $art_works , 'products' => $products ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , $id)
    {
        $current_user_id = Auth::user()->id;

        if( isset($request->status) && $request->status == "add" && isset($id) ){

            $wish = new Wishlist();
            $wish->user_id      = $current_user_id;
            $wish->product_id   = $id;
            $wish->type         = $request->type;
            $wish->save();

            $msg = "Status is added";
            return response()->json(array(['msg'=> $msg , 'val' => 'added']), 200);
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        $current_user_id = Auth::user()->id;

        if( isset($request->status) && $request->status == "remove" && isset($id) ){

            Wishlist::where(['user_id' => $current_user_id])->where(['product_id' => $id])->delete();
            $msg = "Status is removed";
            return response()->json(array(['msg'=> $msg , 'val' => 'removed']), 200);
        }

    }
}
