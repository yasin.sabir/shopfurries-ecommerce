<?php

namespace App\Http\Controllers\FrontViews;

use App\Gallery;
use App\Http\Controllers\Controller;

use App\Mail\ArtGallery\Submitted;
use App\Mail\ArtGallery\Submitted_to_Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallries = Gallery::where(['status' => 1])->get();
        return view('front-views.gallery')->with(['galleries' => $gallries , 'search_result' => array()]);
    }


    public function listView(){
        $gallery_arts = Gallery::where(['artist_id' => Auth::user()->id])->get();
        return view('Gallery.list')->with(['gallery_arts' => $gallery_arts]);
    }


    public function bulk(Request $request){

        $custom_msg = [
            'file.*.required' => 'Files are required!',
        ];

        $this->validate($request,[
            'file.*' => 'required',
        ],$custom_msg);

        if ($request->file('file')) {
            $file = $request->file;
            foreach ($file as $image) {

                $artist_gallery              = new Gallery();
                $artist_gallery->artist_id   = Auth::user()->id;
                $artist_gallery->image       = $image->store('upload/gallery_work_upload', 'public');
                $artist_gallery->status      = 0;
                $artist_gallery->submission  = "temporary";
                $artist_gallery->tags        = null;
                $artist_gallery->save();
            }

        }

        return redirect()->route('gallery.show');

    }

    public function insert(Request $request){

    }

    public function searchResult(Request $request){

        $result = Gallery::where('title','LIKE','%'.$request->find.'%')->get();
        //dd($result);

        return view('front-views.gallery')->with(['search_result' => $result]);

//        if(!empty($request->find)){
//            $result = Gallery::where('title','LIKE','%'.$request->find.'%')->get();
//            return view('front-views.gallery')->with(['result' => $request]);
//        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        return view('Gallery.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallery_entry_ = $request->gallery_entry_;
        $user_id        = $request->user_id;

        $i = 0;
        foreach ($gallery_entry_ as $key => $val){

            Gallery::where(['id' => $val])->update([
                'title'       =>  $request->title_[$val],
                'submission'  =>  'submitted',
                'description' =>  $request->description_[$val],
                'tags'        =>  $request->entry_tag[$val],
            ]);

          $i++;
        }

        $submitted_artWork = [];
        foreach ($gallery_entry_ as $key => $val){
            $detail = Gallery::where(['id' => $val])->first();
            $submitted_artWork [$val] = $detail->toArray();
        }



        $user_detail = User::where(['id'=> $user_id])->first();

        $data ['user_detail'] = $user_detail->toArray();
        $data ['submitted_artWork'] = $submitted_artWork;

        Mail::to($user_detail->email)->send(new Submitted($data));

        //$temp_admin_email = "yasin@hztech.biz";
        $admin_email = get_admin_email();
        Mail::to($admin_email)->send(new Submitted_to_Admin($data));

        $noti = array("message" => "You Art Work submitted successfully!", "alert-type" => "success");
        return redirect()->route('gallery.list')->with($noti);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $temp_data = Gallery::where(['artist_id' => Auth::user()->id])->where(['submission' => 'temporary'])->get();
        return view('Gallery.bulk')->with(['temp_data' => $temp_data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artWork_ = Gallery::findOrFail($id);
        $artWork_->delete();

        $noti = array("message" => "Art deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }


    public function destroyAll(Request $request){
        $ids = json_decode($request->delete_ids);

        foreach ($ids as $key => $id){
            $artWork_ = Gallery::findOrFail($id);
            $artWork_->delete();
        }

        $noti = array("message" => "Selected Art Works deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }



}
