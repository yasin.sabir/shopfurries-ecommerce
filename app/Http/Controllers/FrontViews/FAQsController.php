<?php

namespace App\Http\Controllers\FrontViews;

use App\Faqs;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class FAQsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //For Front View FAQs Page
        $faqs = Faqs::where(['status' => 1])->get();
        return view('front-views.faqs', ['faqs' => $faqs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function viewAll(){

        $faqs = Faqs::orderBy('id','DESC')->get();

        return view('faqs.all',['faqs' => $faqs]);

    }


    public function create()
    {
        //
        return view('faqs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $custom_msg = [
            'question.required' => 'Question is Required!',
            'answer.required'   => 'Answers is Required!',

        ];

        $this->validate($request,[
            'question' => 'required',
            'answer'   => 'required'
        ],$custom_msg);


        if($request->status == "on" ){
            $status = 1;
        }else{
            $status = 0;
        }

        $faqs = new Faqs();
        $faqs->question = preg_replace('/\s+/', ' ', $request->question);
        $faqs->answer   = preg_replace('/\s+/', ' ', $request->answer);
        $faqs->status   = $status;
        $faqs->save();

        $noti = array("message" => "FAQs created successfully!", "alert-type" => "success");
        return redirect()->route('admin.faqs.viewAll')->with($noti);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $faq = Faqs::find(decrypt( $id ));

            if ( ! empty( $faq ) ) {
                return view('faqs.edit',['faq' => $faq]);
            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }

        return view( 'errors.404' );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $custom_msg = [
            'question.required' => 'Question is Required!',
            'answer.required'   => 'Answers is Required!',

        ];

        $this->validate($request,[
            'question' => 'required',
            'answer'   => 'required'
        ],$custom_msg);


        if($request->status == "on" ){
            $status = 1;
        }else{
            $status = 0;
        }

        Faqs::where(['id' => $id])->update([
            'question' =>  preg_replace('/\s+/', ' ', $request->question),
            'answer'   => preg_replace('/\s+/', ' ', $request->answer),
            'status'   => $status
        ]);

        $noti = array("message" => "FAQs updated successfully!", "alert-type" => "success");
        return redirect()->route('admin.faqs.viewAll')->with($noti);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faqs::findOrFail($id);
        $faq->delete();

        $noti = array("message" => "Faq deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }


    public function destroyAll(Request $request){

        $ids = json_decode($request->delete_ids);

        foreach ($ids as $key => $id){
            $faq = Faqs::findOrFail($id);
            $faq->delete();
        }

        $noti = array("message" => "Selected Faqs deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }



}
