<?php

namespace App\Http\Controllers\FrontViews;

use App\Gallery;
use App\Mail\ArtGallery\Approved;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class AdminGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $art_works = Gallery::orderBy('id', 'DESC')->get();
        return view('Gallery.Admin_Gallery.list')->with(['artWorks' => $art_works]);
    }

    public function approvedList()
    {
        $art_works = Gallery::orderBy('id', 'DESC')->get();
        return view('Gallery.Admin_Gallery.list')->with(['artWorks_approved' => $art_works]);
    }

    public function pendingList()
    {
        $art_works = Gallery::orderBy('id', 'DESC')->get();
        return view('Gallery.Admin_Gallery.list')->with(['artWorks_pending' => $art_works]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!empty($id) && $request->status == "approved") {

            Gallery::where(['id' => $id])->update([
                'status' => 1,
                'submission' => 'approved'
            ]);

            $submitted_artWork = [];
            $detail = Gallery::where(['id' => $id])->first();
            $submitted_artWork = $detail->toArray();

            $user_detail                = User::where(['id' => $detail->artist_id])->first();
            $data['user_detail']        = $user_detail->toArray();
            $data ['submitted_artWork'] = $submitted_artWork;

            //Send Email to user'
            Mail::to($user_detail->email)->send(new Approved($data));

            $msg = "Status is Approved";
            return response()->json(array(['msg' => $msg, 'val' => 'checked']), 200);

        } elseif (!empty($id) && $request->status == "pending") {

            Gallery::where(['id' => $id])->update([
                'status' => 0,
                'submission' => 'submitted'
            ]);

            $msg = "Status is Pending";
            return response()->json(array(['msg' => $msg, 'val' => 'unchecked']), 200);
        } else {

            $msg = "Something went wrong!";
            return response()->json(array(['msg' => $msg, 'val' => 'unchecked']), 422);

        }

    }


    public function gallerySearch(Request $request)
    {


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artWork_ = Gallery::findOrFail($id);
        $artWork_->delete();


        $noti = array("message" => "Art deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }


    public function destroyAll(Request $request){
        $ids = json_decode($request->delete_ids);

        foreach ($ids as $key => $id){
            $artWork_ = Gallery::findOrFail($id);
            $artWork_->delete();
        }

        $noti = array("message" => "Selected Art Works deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }

}
