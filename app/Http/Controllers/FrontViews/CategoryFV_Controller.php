<?php

namespace App\Http\Controllers\FrontViews;

use App\Http\Controllers\Controller;

use App\Product;
use Illuminate\Http\Request;

class CategoryFV_Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Display All Products With Pagination
        $filter_option = ( !empty($request->select_product_feature) ? $request->select_product_feature : '' );

        if ($filter_option == 'sort') {
            $products = product::where(['status' => 1])
                               ->where('stock' , '>' , 0)
                               ->where(['product_type' => 'product'])
                               ->orderBy('title' , 'asc')
                               ->orderBy('price' ,'asc')->paginate(12);

        } else if ($filter_option == 'l-h') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('price', 'asc')->paginate(12);

        } else if ($filter_option == 'h-l') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('price', 'desc')->paginate(12);

        } else if ($filter_option == 'a-z') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('title', 'asc')->paginate(12);

        } else if ($filter_option == 'z-a') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('title', 'desc')->paginate(12);

        } else if ($filter_option == 'new') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->latest()->paginate(12)->OnEachSide(3);

        } else {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->latest()->paginate(12)->OnEachSide(3);
            //$products = product::where(['status' => 1])->get();

        }
        return view('front-views.category', ['products' => $products]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
