<?php

namespace App\Http\Controllers\FrontViews;

use App\Artist;
use App\Category;
use App\Category_Meta;
use App\category_setting;
use App\Http\Controllers\Controller;

use App\Product;
use App\order_log;
use App\Coupon;
use App\CouponUser;
use App\Product_Review;
use App\products_meta;
use App\Shipping;
use App\SiteSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Image;
//use Symfony\Component\HttpFoundation\Cookie;

//use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Support\Facades\Cookie;
//use Symfony\Component\HttpFoundation\Cookie;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Display All Products With Pagination
        $filter_option = ( !empty($request->select_product_feature) ? $request->select_product_feature : '' );

        if ($filter_option == 'sort') {
            $products = product::where(['status' => 1])
                                 ->where('stock' , '>' , 0)
                                 ->where(['product_type' => 'product'])
                                 ->orderBy('title' , 'asc')
                                 ->orderBy('price' ,'asc')->paginate(12);

        } else if ($filter_option == 'l-h') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('price', 'asc')->paginate(12);

        } else if ($filter_option == 'h-l') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('price', 'desc')->paginate(12);

        } else if ($filter_option == 'a-z') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('title', 'asc')->paginate(12);

        } else if ($filter_option == 'z-a') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->orderBy('title', 'desc')->paginate(12);

        } else if ($filter_option == 'new') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->latest()->paginate(12)->OnEachSide(3);

        } else {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'product'])->latest()->paginate(12)->OnEachSide(3);
            //$products = product::where(['status' => 1])->get();

        }
        return view('front-views.shop', ['products' => $products]);
    }

    public function bundleProduct_index(Request $request){

        // Display All Products With Pagination
        $filter_option = ( !empty($request->select_product_feature) ? $request->select_product_feature : '' );

        if ($filter_option == 'sort') {
            $products = product::where(['status' => 1])
                               ->where('stock' , '>' , 0)
                               ->where(['product_type' => 'bundle_product'])
                               ->orderBy('title' , 'asc')
                               ->orderBy('price' ,'asc')->paginate(12);

        } else if ($filter_option == 'l-h') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'bundle_product'])->orderBy('price', 'asc')->paginate(12);

        } else if ($filter_option == 'h-l') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'bundle_product'])->orderBy('price', 'desc')->paginate(12);

        } else if ($filter_option == 'a-z') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'bundle_product'])->orderBy('title', 'asc')->paginate(12);

        } else if ($filter_option == 'z-a') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'bundle_product'])->orderBy('title', 'desc')->paginate(12);

        } else if ($filter_option == 'new') {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'bundle_product'])->latest()->paginate(12)->OnEachSide(3);

        } else {
            $products = product::where(['status' => 1])->where('stock' , '>' , 0)->where(['product_type' => 'bundle_product'])->latest()->paginate(12)->OnEachSide(3);
        }
        return view('front-views.shopBundleProducts', ['products' => $products]);
    }


    function fetch_data(Request $request)
    {
        if($request->ajax())
        {
            $products =  product::latest()->paginate(4);
            return $products;
            // return view('front-views.shop', ['products' => $products]);
        }
    }

    function get_SingleProduct(Request $request ,$name){


        try {
        	$product                        = product::where(['slug' => $name])->first();

            if( ! empty($product) ){

                $metas                          = product::find($product->id)->product_metas;
                $galleries_images               = product::find($product->id)->product_galleries;
                $product_variation              = $product::findOrFail($product->id)->variations;
                $related_products               = [];
                $cookie_data                    = unserialize($request->cookie('viewed_products_items'));
                $cookie_recently_view_products  = !empty($cookie_data) ? $cookie_data : [] ;

                if(!empty($cookie_recently_view_products)){
                    if(!in_array($product->id , $cookie_recently_view_products)){
                        array_push($cookie_recently_view_products,$product->id);
                    }
                }else{
                    if(!in_array($product->id , $cookie_recently_view_products)){
                        array_push($cookie_recently_view_products,$product->id);
                    }
                }

                Cookie::queue('viewed_products_items', serialize($cookie_recently_view_products), 15);

                $product_metas = [];
                foreach ($metas as $key => $val){
                    $product_metas[$val->product_meta] = $val->product_meta_value;
                }

                $cate = "";
                $cst = [];
                $category_metas = [];
                $artist = [];

                //Apply Condition From this point
                $categories_ = unserialize($product_metas['categories']);

                if(!empty($categories_) && count($categories_) == 1){

                    // For only one category
                    $cate_id = implode("",unserialize($product_metas['categories']));
                    $cate    = Category::find($cate_id);

                    $rela_product = Category::find($cate_id);

                    foreach ($rela_product->products as $key => $val){
                        $related_products [] =  $val;
                    }

                    //If category doesn't have any meta data so it's run default setting of category
                    $cate_metas = Category::find($cate_id)->category_metas;


                    if(!empty($cate_metas)){

                        foreach ($cate_metas as $key => $val){
                            $category_metas[$val->meta_key] = $val->meta_value;
                        }


                        if(!empty($product_metas['artist_updated_separately']) && $product_metas['artist_updated_separately'] != "yes"){

                            if(isset($category_metas['artist'])){
                                $artist_id                    = $category_metas['artist'];
                                $artist_data                  = Artist::find($artist_id);
                                if(!empty($artist_data)){
                                    $artist['name']               = $artist_data->name;
                                    $artist['email']              = $artist_data->email;
                                    $artist['image']              = $artist_data->image;
                                    $artist['status']             = $artist_data->status;
                                    $artist['additional_details'] = isset($artist_data->additional_details) ? unserialize($artist_data->additional_details) : array() ;
                                }
                            }

                        }else{


                            $artist_id                    = !empty($product_metas['artist']) ? $product_metas['artist'] : null;
                            $artist_data                  = Artist::find($artist_id);
                            if(!empty($artist_data)){
                                $artist['name']               = $artist_data->name;
                                $artist['email']              = $artist_data->email;
                                $artist['image']              = $artist_data->image;
                                $artist['status']             = $artist_data->status;
                                $artist['additional_details'] = isset($artist_data->additional_details) ? unserialize($artist_data->additional_details) : array() ;
                            }

                        }

                        $reasons = [];
                        $r_title  = unserialize($category_metas['category_reason_title']);
                        $r_detail = unserialize($category_metas['category_reason_detail']);
                        if( !empty($r_title) ){
                            foreach ($r_title as $key => $val){
                                $reasons[$key]['title_'.$key]  = $val;
                                $reasons[$key]['detail_'.$key] = $r_detail[$key];
                            }
                        }

                        $features = [];
                        $f_title  = unserialize($category_metas['category_prod_feature_title']);
                        $f_detail = unserialize($category_metas['category_prod_feature_detail']);
                        if( !empty($f_title) ){
                            foreach ($f_title as $key => $val){
                                $features[$key]['title_'.$key]  = $val;
                                $features[$key]['detail_'.$key] = $f_detail[$key];
                            }
                        }

                    }else{

                        //Show Default setting of category
                        $cst = category_setting::all();
                        $cst_status = $cst->toArray();

                        foreach ($cst as $key => $val){
                            $category_metas["status"]                           = $val->status;
                            $category_metas["category_reason_title"]            = $val->reason_titles;
                            $category_metas["category_reason_detail"]           = $val->reason_descriptions;
                            $category_metas["category_prod_feature_title"]      = $val->feature_titles;
                            $category_metas["category_prod_feature_detail"]     = $val->feature_descriptions;
                            $category_metas["poster_images"]                    = $val->posters;
                            $category_metas["actual_material_images"]           = $val->actual_material_pics;
                        }

                        $reasons    = [];
                        $r_title    = unserialize($category_metas["category_reason_title"]);
                        $r_detail   = unserialize($category_metas["category_reason_detail"]);
                        if( !empty($r_title) ){
                            foreach ($r_title as $key => $val){
                                $reasons[$key]['title_'.$key]  = $val;
                                $reasons[$key]['detail_'.$key] = $r_detail[$key];
                            }
                        }

                        $features = [];
                        $f_title  = unserialize($category_metas["category_prod_feature_title"]);
                        $f_detail = unserialize($category_metas["category_prod_feature_detail"]);
                        if( !empty($f_title) ){
                            foreach ($f_title as $key => $val){
                                $features[$key]['title_'.$key]  = $val;
                                $features[$key]['detail_'.$key] = $f_detail[$key];
                            }
                        }

                    }

                }else{


                    $rel_product = product::orderBy('id','DESC')->take(10)->get();

                    foreach ($rel_product as $kk => $val){
                        $related_products [] = $val;
                    }

                    //Show Default setting of category
                    $cst = category_setting::all();

                    foreach ($cst as $key => $val){
                        $category_metas["status"]                           = $val->status;
                        $category_metas["category_reason_title"]            = $val->reason_titles;
                        $category_metas["category_reason_detail"]           = $val->reason_descriptions;
                        $category_metas["category_prod_feature_title"]      = $val->feature_titles;
                        $category_metas["category_prod_feature_detail"]     = $val->feature_descriptions;
                        $category_metas["poster_images"]                    = $val->posters;
                        $category_metas["actual_material_images"]           = $val->actual_material_pics;
                    }

                    $reasons    = [];
                    $r_title    = unserialize($category_metas["category_reason_title"]);
                    $r_detail   = unserialize($category_metas["category_reason_title"]);
                    if( !empty($r_title) ){
                        foreach ($r_title as $key => $val){
                            $reasons[$key]['title_'.$key]  = $val;
                            $reasons[$key]['detail_'.$key] = $r_detail[$key];
                        }
                    }

                    $features = [];
                    $f_title  = unserialize($category_metas["category_prod_feature_title"]);
                    $f_detail = unserialize($category_metas["category_prod_feature_detail"]);
                    if( !empty($f_title) ){
                        foreach ($f_title as $key => $val){
                            $features[$key]['title_'.$key]  = $val;
                            $features[$key]['detail_'.$key] = $f_detail[$key];
                        }
                    }

                }

                //Review Section
                $reviews = Product_Review::where(['product_id'=>$product->id])->where(['status' => 'on'])->get();
                //customVarDump_die($reviews);

                //        foreach ($product_variation as $key => $val){
                //            $ff = unserialize($val->variation_meta);
                //
                //            //            $rr = str_replace("-"," : ",$ff[$key]);
                //            //            echo ucwords($rr)." , ";
                //        }


                if(!empty($product_metas['up_sell'])){
                    $up_sell = product::find($product_metas['up_sell']);
                }else{
                    $up_sell = [];
                }

                return view('front-views.single-product.singleProduct',[
                    'product'            => $product ,
                    'product_variations' => $product_variation ,
                    'metas'              => $product_metas ,
                    'galleries'          => $galleries_images ,
                    'category'           => $cate,
                    'category_setting'   => $cst,
                    'category_meta'      => $category_metas ,
                    'reasons'            => $reasons ,
                    'features'           => $features ,
                    'artist'             => $artist,
                    'reviews'            => $reviews ,
                    'related_products'   => $related_products,
                    'up_sell'            => $up_sell,
                ]);

                //        return response(view('front-views.single-product.singleProduct',[
                //            'product'            => $product ,
                //            'product_variations' => $product_variation ,
                //            'metas'              => $product_metas ,
                //            'galleries'          => $galleries_images ,
                //            'category'           => $cate,
                //            'category_setting'   => $cst,
                //            'category_meta'      => $category_metas ,
                //            'reasons'            => $reasons ,
                //            'features'           => $features ,
                //            'artist'             => $artist,
                //            'reviews'            => $reviews ,
                //            'related_products'   => $related_products,
                //            'up_sell'            => $up_sell,
                //        ]))->cookie('viewed_products',$cookie_json_view_products,5);

            }


        } catch ( \Exception $e ) {
            return view( 'errors.front-end-errors.product-error-404' );
        }
        return view( 'errors.front-end-errors.product-error-404' );

    }


    public function getProductImage($image){

        $ff           = @unserialize($image);
        $default_size = \App\SiteSetting::where(['key' => 'product_image_size'])->first();

        if(!is_array($ff)){
            if(!empty($image)){
                return "storage/".$image;
            }else{
                return "images/placeholders/default-placeholder-600x600.png";
            }
        }else{
            return "storage/".$ff[$default_size->value];
        }
    }

    public function get_SingleBundleProduct($name,$id){

        $product            = product::find($id);
        $metas              = product::find($id)->product_metas;
        $galleries_images   = product::find($id)->product_galleries;
        $product_variation  = $product::findOrFail($id)->variations;
        $related_products   = [];

        $product_metas = [];
        foreach ($metas as $key => $val){
            $product_metas[$val->product_meta] = $val->product_meta_value;
        }

        $cate = "";
        $cst = [];
        $category_metas = [];
        $artist = [];

        //Apply Condition From this point
        $categories_ = unserialize($product_metas['categories']);

        if(!empty($categories_) && count($categories_) == 1){

            // For only one category
            $cate_id = implode("",unserialize($product_metas['categories']));
            $cate    = Category::find($cate_id);

            $rela_product = Category::find($cate_id);

            foreach ($rela_product->products as $key => $val){
                $related_products [] =  $val;
            }

            //If category doesn't have any meta data so it's run default setting of category
            $cate_metas = Category::find($cate_id)->category_metas;


            if(!empty($cate_metas)){

                foreach ($cate_metas as $key => $val){
                    $category_metas[$val->meta_key] = $val->meta_value;
                }


                if(!empty($product_metas['artist_updated_separately']) && $product_metas['artist_updated_separately'] != "yes"){

                    if(isset($category_metas['artist'])){
                        $artist_id                    = $category_metas['artist'];
                        $artist_data                  = Artist::find($artist_id);
                        if(!empty($artist_data)){
                            $artist['name']               = $artist_data->name;
                            $artist['email']              = $artist_data->email;
                            $artist['image']              = $artist_data->image;
                            $artist['status']             = $artist_data->status;
                            $artist['additional_details'] = isset($artist_data->additional_details) ? unserialize($artist_data->additional_details) : array() ;
                        }
                    }

                }else{

                    $artist_id                    = !empty($product_metas['artist']) ? $product_metas['artist'] : null;
                    $artist_data                  = Artist::find($artist_id);
                    if(!empty($artist_data)){
                        $artist['name']               = $artist_data->name;
                        $artist['email']              = $artist_data->email;
                        $artist['image']              = $artist_data->image;
                        $artist['status']             = $artist_data->status;
                        $artist['additional_details'] = isset($artist_data->additional_details) ? unserialize($artist_data->additional_details) : array() ;
                    }

                }

                $reasons = [];
                $r_title  = unserialize($category_metas['category_reason_title']);
                $r_detail = unserialize($category_metas['category_reason_detail']);
                if( !empty($r_title) ){
                    foreach ($r_title as $key => $val){
                        $reasons[$key]['title_'.$key]  = $val;
                        $reasons[$key]['detail_'.$key] = $r_detail[$key];
                    }
                }

                $features = [];
                $f_title  = unserialize($category_metas['category_prod_feature_title']);
                $f_detail = unserialize($category_metas['category_prod_feature_detail']);
                if( !empty($f_title) ){
                    foreach ($f_title as $key => $val){
                        $features[$key]['title_'.$key]  = $val;
                        $features[$key]['detail_'.$key] = $f_detail[$key];
                    }
                }

            }else{

                //Show Default setting of category
                $cst = category_setting::all();
                $cst_status = $cst->toArray();

                foreach ($cst as $key => $val){
                    $category_metas["status"]                           = $val->status;
                    $category_metas["category_reason_title"]            = $val->reason_titles;
                    $category_metas["category_reason_detail"]           = $val->reason_descriptions;
                    $category_metas["category_prod_feature_title"]      = $val->feature_titles;
                    $category_metas["category_prod_feature_detail"]     = $val->feature_descriptions;
                    $category_metas["poster_images"]                    = $val->posters;
                    $category_metas["actual_material_images"]           = $val->actual_material_pics;
                }

                $reasons    = [];
                $r_title    = unserialize($category_metas["category_reason_title"]);
                $r_detail   = unserialize($category_metas["category_reason_detail"]);
                if( !empty($r_title) ){
                    foreach ($r_title as $key => $val){
                        $reasons[$key]['title_'.$key]  = $val;
                        $reasons[$key]['detail_'.$key] = $r_detail[$key];
                    }
                }

                $features = [];
                $f_title  = unserialize($category_metas["category_prod_feature_title"]);
                $f_detail = unserialize($category_metas["category_prod_feature_detail"]);
                if( !empty($f_title) ){
                    foreach ($f_title as $key => $val){
                        $features[$key]['title_'.$key]  = $val;
                        $features[$key]['detail_'.$key] = $f_detail[$key];
                    }
                }

            }

        }else{


            $rel_product = product::orderBy('id','DESC')->take(10)->get();

            foreach ($rel_product as $kk => $val){
                $related_products [] = $val;
            }

            //Show Default setting of category
            $cst = category_setting::all();

            foreach ($cst as $key => $val){
                $category_metas["status"]                           = $val->status;
                $category_metas["category_reason_title"]            = $val->reason_titles;
                $category_metas["category_reason_detail"]           = $val->reason_descriptions;
                $category_metas["category_prod_feature_title"]      = $val->feature_titles;
                $category_metas["category_prod_feature_detail"]     = $val->feature_descriptions;
                $category_metas["poster_images"]                    = $val->posters;
                $category_metas["actual_material_images"]           = $val->actual_material_pics;
            }

            $reasons    = [];
            $r_title    = unserialize($category_metas["category_reason_title"]);
            $r_detail   = unserialize($category_metas["category_reason_title"]);
            if( !empty($r_title) ){
                foreach ($r_title as $key => $val){
                    $reasons[$key]['title_'.$key]  = $val;
                    $reasons[$key]['detail_'.$key] = $r_detail[$key];
                }
            }

            $features = [];
            $f_title  = unserialize($category_metas["category_prod_feature_title"]);
            $f_detail = unserialize($category_metas["category_prod_feature_detail"]);
            if( !empty($f_title) ){
                foreach ($f_title as $key => $val){
                    $features[$key]['title_'.$key]  = $val;
                    $features[$key]['detail_'.$key] = $f_detail[$key];
                }
            }

        }

        //Review Section
        $reviews = Product_Review::where(['product_id'=>$id])->where(['status' => 'on'])->get();
        //customVarDump_die($reviews);

        //        foreach ($product_variation as $key => $val){
        //            $ff = unserialize($val->variation_meta);
        //
        //            //            $rr = str_replace("-"," : ",$ff[$key]);
        //            //            echo ucwords($rr)." , ";
        //        }


        if(!empty($product_metas['up_sell'])){
            $up_sell = product::find($product_metas['up_sell']);
        }else{
            $up_sell = [];
        }

        //================================ Combine All Featured Image

        $image = "";
        $selectedProductImages            = [];
        $selectedProductImages [0]        = $this->getProductImage($product->image);
        $selected_product_featured_images = unserialize(get_product_meta( $product->id , 'bundle_product' ));

        foreach($selected_product_featured_images as $key => $val){
            $prod         = product::find($val);
            $ff           = @unserialize($prod->image);

            if(!is_array($ff)){
                if(!empty($prod->image)){
                    $selectedProductImages[$key+1] = "storage/".$prod->image;
                }else{
                    $selectedProductImages[$key+1] = "images/placeholders/default-placeholder-600x600.png";
                }
            }else{
                $selectedProductImages[$key+1] =  $this->getProductImage($ff);
            }
        }

        return view('front-views.single-product.bundleSingleProduct',[
            'product'               => $product ,
            'selectedProductImages' => $selectedProductImages,
            'product_variations'    => $product_variation ,
            'metas'                 => $product_metas ,
            'galleries'             => $galleries_images ,
            'category'              => $cate,
            'category_setting'      => $cst,
            'category_meta'         => $category_metas ,
            'reasons'               => $reasons ,
            'features'              => $features ,
            'artist'                => $artist,
            'reviews'               => $reviews ,
            'related_products'      => $related_products,
            'up_sell'               => $up_sell,
        ]);

    }

    public function checkout($id){

        $user_id       = $id;
        $order_log     = order_log::where('user_id',$user_id)->where(['order_type' => 'front'])->get();
        $totall        = [];
        $dst           = SiteSetting::where(['key' => 'default_sales_tax'])->first();


        $product_meta                            = [];
        $category_sales_tax                      = [];
        $product_cate_ids                        = [];
        $after_category_SalesTax_added           = "";
        $after_defaultSalesTax_added             = "";
        $product_groupBy_category_with_salesTax  = [];
        $product_groupBy_moreThan_two_categories = [];

        foreach($order_log as $key => $total){
            $totall[] = (($total->qty) * ($total->price));

            $pr = products_meta::where(['product_id' => $total["product_id"] ])->get();
            $product_meta [] = $pr->toArray();

            foreach($product_meta[$key] as $k => $val){
                if($val["product_meta"] == "categories"){

                    $cate = unserialize($val["product_meta_value"]);

                    //customVarDump($cate);
                    if(count($cate) <= 1){

                        $cate_id   = $cate[0];
                        $cate_meta = Category_Meta::where(['category_id' => $cate_id])
                                                    ->where(['meta_key' => 'cate_sale_tax'])
                                                    ->get();

                        foreach ($cate_meta as $i => $data){
                            if($data["category_id"] == $cate_id){
                                $product_cate_ids  [] = $data['category_id'];
                                $product_ids       [] = $total['product_id'];
                                $pp                   = ($total['qty']*$total["price"]);
                            }
                        }

                    }else{
                        $product_groupBy_moreThan_two_categories [$total["product_id"]] = $total['qty'] *  $total["price"];
                        if(isset($dst)){
                            $Default_sale_tax                     = ( ( $dst->value / 100 ) * array_sum($product_groupBy_moreThan_two_categories) ) ;
                            $after_defaultSalesTax_added          = $Default_sale_tax + array_sum($product_groupBy_moreThan_two_categories);
                        }else{
                            $without_DefaultSalesTax              = array_sum($product_groupBy_moreThan_two_categories);
                        }

                    }

                }
            }

        }


        $product_groupBy_category = [];
        $product_cate_id         = array_unique($product_cate_ids);

        foreach ($product_cate_id as $k => $v){
            $cc = Category::find($v);
            foreach ($cc->products as $kk => $vv){

                if(in_array($vv->id,$product_ids)){
                    $order_log_product                   = order_log::where('user_id',$user_id)
                                                                      ->where(['product_id' => $vv->id])->first();
                    $product_groupBy_category[$cc->id][$vv->id] =  ($order_log_product->qty * $order_log_product->price);
                }

            }
        }


        //Products group by categories
        //customVarDump($product_groupBy_category);

        //Products group by more than 2 categories
        //customVarDump($product_groupBy_moreThan_two_categories);

        foreach ($product_groupBy_category as $i => $val){

            $category_SalesTax = Category_Meta::where(['category_id' => $i])
                ->where(['meta_key' => 'cate_sale_tax'])
                ->first();

            $category_SalesTax_value         = ( $category_SalesTax->meta_value / 100 );
            $after_category_SalesTax_added   = ( $category_SalesTax_value * array_sum($val));
            $product_groupBy_category_with_salesTax[$i][] = ( $after_category_SalesTax_added + array_sum($val));
        }


        if(isset($dst)){
            $defaultSaleTax = $after_defaultSalesTax_added;
        }else{
            $defaultSaleTax = $without_DefaultSalesTax;
        }


        // Generate sub total of all product including default or category sales tax
        $addSubTotal [] = $defaultSaleTax;

        foreach ($product_groupBy_category_with_salesTax as $key => $val){
            $addSubTotal [] = $val[0];
        }
        $addSubTotal_ = array_sum($addSubTotal);

        //$total_sum  = array_sum($totall);
        $count        = count($order_log);



        $total_weight = 0;
        foreach ($order_log as $key => $val){
            $total_weight = $total_weight + $val->weight;
        }

        $shipping = Shipping::all();


        //Default Shipping & Weight costs
        $Default_Shipping_Setting = [];
        $default_weight_price     = SiteSetting::where(['key' => 'default_weight_price'])->first();
        $default_shipping_cost    = SiteSetting::where(['key' => 'default_shipping_cost'])->first();

        $Default_Shipping_Setting ['default_weight_price']  = $default_weight_price->value;
        $Default_Shipping_Setting ['default_shipping_cost'] = $default_shipping_cost->value;

        return view('front-views.checkout',[
            'userID'                                    => $user_id,
            'product_groupBy_category'                  => $product_groupBy_category,
            'product_groupBy_category_with_salesTax'    => $product_groupBy_category_with_salesTax,
            'product_groupBy_moreThan_two_categories'   => $product_groupBy_moreThan_two_categories,
            'defaultSaleTax'                            => $defaultSaleTax,
            'order_log'                                 => $order_log,
            'log_data'                                  => serialize($order_log->toArray()),
            'count'                                     => $count,
            'total_sum'                                 => $addSubTotal_,
            'total_weight'                              => $total_weight,
            'shipping'                                  => $shipping,
            'Default_Shipping_Setting'                  => $Default_Shipping_Setting,
        ]);

    }

    public function delete_temp_item($id){
        $order_log = order_log::find($id);
        $order_log->delete();
        return response()->json(['message' => 'success','Cart' => 'Item Removed!']);
        //return redirect()->route('checkout');
    }

    public function update_temp_item(Request $request){

        $cart_ids         = $request->id;
        $cart_product_qty = $request->qty;

        foreach ($cart_ids as $key => $val){
            order_log::where(['id' => $val])->update([
                'qty' => $cart_product_qty[$key]
            ]);
        }

        return redirect()->route('checkout');
    }

    public function couponapply(Request $request){

        $custom_msg = [
            'coupon.required' => "Coupon is Required!"
        ];

        $this->validate($request,[
            'coupon' => 'required'
        ],$custom_msg);

        $coupons = $request->coupon;
       // dd($coupons);

        $today = date("Y-m-d");

        $coupon = Coupon::where('code',$coupons)->get();
        $coupon_id = $coupon[0]['id'];
        $user_id = Auth::user()->id;
        $couponcheck = CouponUser::where('user_id',$user_id)->where('coupon_id',$coupon_id)->where('status',1)->get();

        if( ($coupon != null) || ($coupon[0]['status'] == 1) ){

            if( ($coupon[0]['expire_date']) > $today){

                //( > 0)
                if($coupon[0]['max_uses'] >= 0) {

                    // (==0)
                    if(count($couponcheck) >= 0) {

                        if ($coupon[0]['type'] == "fixed") {
                            $type = 'fixed';
                            $coupon_discount = $coupon[0]['discount'];
                            $coupon_q = $coupon[0]['max_uses'] - 1;
                            Coupon::where('code', $coupons)->update([
                                'max_uses' => $coupon_q
                            ]);
                            $couponuser = new CouponUser();
                            $couponuser->coupon_id = $coupon_id;
                            $couponuser->user_id = $user_id;
                            $couponuser->status = 1;
                            $couponuser->save();

                            return response()->json(['type' => $type, 'coupon_discount' => $coupon_discount, 'noti' => 'Coupon Applied Successfully!']);
                        }
                        else if ($coupon[0]['type'] == "percent") {
                            $type = 'percent';
                            $coupon_discount = $coupon[0]['discount'];
                            $coupon_q = $coupon[0]['max_uses'] - 1;
                            Coupon::where('code', $coupons)->update([
                                'max_uses' => $coupon_q
                            ]);
                            $couponuser = new CouponUser();
                            $couponuser->coupon_id = $coupon_id;
                            $couponuser->user_id = $user_id;
                            $couponuser->status = 1;
                            $couponuser->save();

                            return response()->json(['type' => $type, 'coupon_discount' => $coupon_discount, 'noti' => 'Coupon Applied Successfully!']);
                        }

                    }else{
                        return response()->json(['type' => 'fixed', 'coupon_discount' => 0, 'noti' => 'Coupon Already Used By User!']);
                    }

                } else{
                    return response()->json(['type' => 'fixed','coupon_discount'=>0,'noti'=>'Coupon use limit cross!']);
                }

            }else{
                return response()->json(['type' => 'fixed','coupon_discount'=>0,'noti'=>'Coupon Expired!']);
            }
        }else{
            return response()->json(['type' => 'fixed','coupon_discount'=>0,'noti'=>'Coupon Not Found!']);

        }
    }



    public function thankyouView(){
        return view('front-views.thankyou-shopping');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
