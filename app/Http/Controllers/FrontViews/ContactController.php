<?php

namespace App\Http\Controllers\FrontViews;

use App\Mail\ContactUs;
use App\Contact;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\Contact_to_Admin;
use http\Client\Curl\User;
use Illuminate\Support\Facades\Auth;
use App\Query;


use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('front-views.contact');
    }

    public function list(){

        $inquires = Contact::all();
        return view('contactus.list',[ 'inquires' => $inquires ]);
    }

    public function contact(Request $request)
    {

        //dd($contact = $request->all());
        //customVarDump_die($contact['email']);
        //customVarDump_die(hasAdmin());

        $custom_msg = [
            'name.required' => 'Name is required!',
            'email.required' => 'Email is required!',
            'message.required' => 'Message is required!',
        ];

        $this->validate($request,[
            'name'      => 'required',
            'email'     => 'required',
            'message'   => 'required'
        ],$custom_msg);

        $contactUs = new Contact();
        $contactUs->name = $request->name;
        $contactUs->email = $request->email;
        $contactUs->phone = $request->mobile;
        $contactUs->message = $request->message;
        $contactUs->save();

        $contact = $request->all();

        Mail::to($contact['email'])->send(new ContactUs($contact));

        foreach (get_AllAdmin() as $key => $val){
            $admin = \App\User::find($val->user_id);
            Mail::to($admin->email)->send(new Contact_to_Admin($contact));
        }

        $noti = array("message" => "Thank You For Contacting", "alert-type" => "success");
        return redirect()->back()->with($noti);

    }

    public function query_list(){
        //        $query = Query::all();
        //        return view('query.list', ['query' => $query]);
    }

    public function destroyAll_quries(Request $request){

        //        $ids = json_decode($request->delete_ids);
        //        //customVarDump_die($ids);
        //        foreach ($ids as $key => $id){
        //            $event = Query::findOrFail($id);
        //            $event->delete();
        //        }
        //        $noti = array("message" => "Selected Queries deleted successfully", "alert-type" => "success");
        //        return redirect()->back()->with($noti);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Contact::findOrFail($id);
        $event->delete();

        $noti = array("message" => "Inquiry deleted successfully!", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }


    public function destroyAll(Request $request){

        $ids = json_decode($request->delete_ids);
        //customVarDump_die($ids);

        foreach ($ids as $key => $id){
            $event = Contact::findOrFail($id);
            $event->delete();
        }

        $noti = array("message" => "Selected Inquires deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }
}
