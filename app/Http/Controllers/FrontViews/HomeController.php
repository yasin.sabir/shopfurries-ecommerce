<?php

namespace App\Http\Controllers\FrontViews;

use App\Http\Controllers\Controller;
use App\order_log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use app\User;
use App\Coupon;
use App\Product;
use App\order;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['auth' => 'verified']);
        //$this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId     = Auth::id();
        $count      = order_log::where('user_id',$userId)->get();
        $count_cart = count($count);
        return view('front-views.home',['count'=>$count_cart]);
    }

    public function dashboard()
    {
        $userId         = Auth::id();
        $users          = User::all();
        $products       = product::all();
        $coupons        = Coupon::all();
        $orders         = order::all();
        $orders_by_user = order::where('user_id',$userId)->get();

        return view('dashboard',['user'=>$users,'product'=>$products,'coupon'=>$coupons,'orders'=>$orders
        ,'orders_by_user'=>$orders_by_user]);
    }
}
