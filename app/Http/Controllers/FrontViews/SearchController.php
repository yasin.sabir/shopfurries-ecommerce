<?php

namespace App\Http\Controllers\FrontViews;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $product_ids    = [];
        $search         = $request->search_product;
        $type           = $request->search_type;

        if($type == 1){

	        $categories = Category::where('Name' , 'LIKE' , '%' .$search. '%' )->get();

	        foreach ($categories as $key => $val){

		        $rr = Category::find($val->id);

		        foreach ($rr->products as $k => $v){
			        $product_ids [] = $v->id;
		        }
	        }

	        $product_ids = array_unique($product_ids);
	        $products    = product::whereIn('id',$product_ids)->latest()->paginate( 12 )->OnEachSide( 3 );

        }else{
			$products = product::where( 'title', 'LIKE', '%' . $search . '%' )
			                   ->orWhere( 'slug', 'LIKE', '%' . $search . '%' )
			                   ->latest()->paginate( 12 )->OnEachSide( 3 );
        }

        return view( 'front-views.searchResult', [
        	'products' => $products,
	        'search'   => $search,
	        'type'     => $type
        ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        //
    }
}
