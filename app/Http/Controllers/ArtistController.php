<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $artists = Artist::orderBy('id','DESC')->get();
            return view('artists.all' , ['artists' => $artists]);
        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            return view('artists.add');
        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        try{
            $custom_msg = [
                'artist_name.required' => 'Artist name is required!'
            ];

            $this->validate($request,[
                'artist_name' => 'required'
            ],$custom_msg);

            $artist = new Artist();
            $data = [];

            $artist->name               = $request->artist_name;
            $artist->email              = $request->artist_email;
            $artist->status             = "on";
            $data["facebook"]           = isset($request->social_link_facebook) ? $request->social_link_facebook : null;
            $data["twitter"]            = isset($request->social_link_twitter) ? $request->social_link_twitter : null;
            $data["instagram"]          = isset($request->social_link_instagam) ? $request->social_link_instagam : null;
            $data["googleplus"]         = isset($request->social_link_google_plus) ? $request->social_link_google_plus : null;
            $data["other"]              = isset($request->social_link_other) ? $request->social_link_other : null;

            $artist->additional_details = serialize($data);
            $artist->save();

            $default_path  = "images/placeholders/Profile_avatar_placeholder_lg.png";
            $artist_avatar = $request->file('artist_avatar') ? $request->file('artist_avatar')->store('upload/artists/artist_'.$artist->id, 'public') : null;

            Artist::where(['id' => $artist->id])->update(['image' => $artist_avatar]);

            if($request->hasFile('artist_avatar')){
                $sizes = ['150'];
                $type  = "artists";
                $slug  = "artist";
                $file_key = 'artist_avatar';
                $this->imageResizing( $artist->id, $type, $slug , $file_key , $sizes , $request);
            }

            $noti = array("message" => "Artist created successfully!", "alert-type" => "success");
            return redirect()->route('artist.list')->with($noti);

//        }catch (\Exception $ex){
//            $noti = array("message" => "Error", "alert-type" => "error");
//            return redirect()->back()->with($noti);
//        }

    }

    function imageResizing($id, $type , $slug , $file_key , $sizes , $request){

        try{
            if(!empty($sizes) && count($sizes) > 0){

                foreach ($sizes as $key => $val){
                    $folderSizes   = $val."x".$val;
                    $folderName  = $slug."_".$id;
                    $directory    = "public/upload/".$type."/".$folderName."/".$folderSizes."/";
                    Storage::makeDirectory($directory);
                }

                if($request->hasFile($file_key)){

                    $image     = $request->file($file_key);
                    $filename  = time() . '.' . $image->getClientOriginalExtension();

                    foreach ($sizes as $key => $val){
                        $folderSizes    = $val."x".$val;
                        $folderName     = $slug."_".$id;
                        $path           = storage_path('app/public/upload/'.$type.'/'.$folderName.'/'.$folderSizes.'/').$filename;
                        $img            = Image::make($image->getRealPath());
                        $img->resize($val, $val, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        $img->save($path);
                    }

                }
            }
        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $artist = Artist::find(decrypt( $id ));

            if ( ! empty( $artist ) ) {
                $artist_social_links = unserialize($artist->additional_details);
            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }

        return view('artists.edit',['artist' => $artist , 'links' => $artist_social_links]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $custom_msg = [
                'artist_name.required' => 'Artist name is required!'
            ];

            $this->validate($request,[
                'artist_name' => 'required'
            ],$custom_msg);

            $prev_avatar   = $request->prev_artist_avatar;
            $artist_avatar = $request->file('artist_avatar') ? $request->file('artist_avatar')->store('upload/artists/artist_'.$id.'/', 'public') : $prev_avatar;

            $data = [];
            $data["facebook"]           = isset($request->social_link_facebook) ? $request->social_link_facebook : null;
            $data["twitter"]            = isset($request->social_link_twitter) ? $request->social_link_twitter : null;
            $data["instagram"]          = isset($request->social_link_instagam) ? $request->social_link_instagam : null;
            $data["googleplus"]         = isset($request->social_link_google_plus) ? $request->social_link_google_plus : null;
            $data["other"]              = isset($request->social_link_other) ? $request->social_link_other : null;

            Artist::where(['id' => $id])->update([
                                                     'name'   => $request->artist_name,
                                                     'email'  => $request->artist_email,
                                                     'image'  => $artist_avatar,
                                                     'additional_details' => serialize($data),
                                                 ]);

            if($request->hasFile('artist_avatar')){
                $sizes = ['150'];
                $type  = "artists";
                $slug  = "artist";
                $file_key = 'artist_avatar';
                $this->imageResizing( $id, $type, $slug , $file_key , $sizes , $request);
            }


            $noti = array("message" => "Artist updated successfully!", "alert-type" => "success");
            return redirect()->back()->with($noti);

        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $artist = Artist::findOrFail($id);
            $artist->delete();

            $noti = array("message" => "Artist deleted successfully!", "alert-type" => "success");
            return redirect()->back()->with($noti);
        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }
    }


    public function destroyAll(Request $request){

        try{

            $ids = json_decode($request->delete_ids);

            foreach ($ids as $key => $id){
                $artist = Artist::findOrFail($id);
                $artist->delete();
            }

            $noti = array("message" => "Selected Artists deleted successfully", "alert-type" => "success");
            return redirect()->back()->with($noti);
        }catch (\Exception $ex){
            $noti = array("message" => "Error", "alert-type" => "error");
            return redirect()->back()->with($noti);
        }

    }

}
