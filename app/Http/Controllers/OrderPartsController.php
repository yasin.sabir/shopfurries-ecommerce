<?php

namespace App\Http\Controllers;


use App\OrderParts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class OrderPartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderParts = OrderParts::orderBy('id','DESC')->get();
        return view('order.parts.all' , ['orderParts' => $orderParts ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $orderPart_Detail = OrderParts::where(['title' => $request->orderPart_title ])->first();

        $custom_msg = [
            'orderPart_title.required'    => 'Installment title is required!',
            'orderParts.numeric'          => 'Installment should be in numbers!',
            'per_orderPart_days.numeric' => 'Days should be in numbers!'
        ];

        $this->validate($request , [
            'orderPart_title'      => 'required',
            'orderParts'           => 'required|numeric',
            'per_orderPart_days'   => 'required|numeric'
        ],$custom_msg);


        if(!$orderPart_Detail){

            $orderPart  = new OrderParts();
            $orderPart->title = $request->orderPart_title;
            $orderPart->parts = $request->orderParts;
            $orderPart->days  = $request->per_orderPart_days;
            $orderPart->save();

            $noti = array("message" => "Order Part created successfully!", "alert-type" => "success");
            return redirect()->route('order.parts.list')->with($noti);

        }

        $noti = array("message" => "Part {$request->orderPart_title} already exist!!", "alert-type" => "error");
        return redirect()->route('order.parts.list')->with($noti);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $orderPart_details = Orderparts::find( decrypt( $id ) );

            if ( ! empty( $orderPart_details ) ) {

                return view('order.parts.edit', [ 'orderPart' => $orderPart_details , '$orderPart_id' =>  decrypt( $id ) ] );
            }

        } catch ( \Exception $e ) {
            return view( 'errors.404' );
        }
        return view( 'errors.404' );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $orderPart = OrderParts::where(['title' => $request->orderPart_title ])->get();

        $custom_msg = [
            'orderPart_title.required'    => 'Installment title is required!',
            'orderParts.numeric'          => 'Installment should be in numbers!',
            'per_orderPart_days.numeric' => 'Days should be in numbers!'
        ];

        $this->validate($request , [
            'orderPart_title'      => 'required',
            'orderParts'           => 'required|numeric',
            'per_orderPart_days'   => 'required|numeric'
        ],$custom_msg);

        try{

            if(count($orderPart) == 0){

                $order_part = OrderParts::where(['id' => $id])->update([
                    'title'      => $request->orderPart_title,
                    'parts'      => $request->orderParts,
                    'days'       => $request->per_orderPart_days,
                ]);

                $noti = array("message" => "Order Part updated successfully!", "alert-type" => "success");
                return Redirect::back()->with($noti);
            }

            $noti = array("message" => "Tag {$request->orderPart_title} already exist!!", "alert-type" => "error");
            return Redirect::back()->with($noti);



        }catch(\Exception $ex){
            $noti = array("message" =>$ex->getMessage(), "alert-type" => "error");
            return redirect()->route('order.parts.list')->with($noti);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inst = OrderParts::findOrFail($id);
        $inst->delete();

        $noti = array("message" => "Order Part delete successfully", "alert-type" => "success");
        return redirect()->route('order.parts.list')->with($noti);
    }


    public function destroyAllOrderparts(Request $request){

        $ids = json_decode($request->delete_ids);

        foreach ($ids as $key => $id){
            $inst = OrderParts::findOrFail($id);
            $inst->delete();
        }

        $noti = array("message" => "Selected Parts are deleted successfully", "alert-type" => "success");
        return redirect()->back()->with($noti);
    }

}
