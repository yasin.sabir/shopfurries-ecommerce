<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class Maintenance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next , $guard = null)
    {
        //This for non-authenticated users (!Auth::guard($guard)->check())
        //( Auth::check() && Auth::user()->hasRole([3]) ? 'user' : '')
        //( null !== Auth::user() && Auth::user()->hasRole([3]) ? 'user' : '' )

        if( (!Auth::guard($guard)->check()) && (site_config('site_maintenance_status') == 'on') ){
             return new Response(view('admin_settings.maintenance'));
        }
        return $next($request);
    }
}
