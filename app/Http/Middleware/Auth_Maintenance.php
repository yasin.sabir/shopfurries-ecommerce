<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class Auth_Maintenance
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {

        if ( ( Auth::guest() || Auth::user()->isAdmin() == null ) && site_config( 'site_maintenance_status' ) == 'on' ) {
            return new Response( view( 'admin_settings.maintenance' ) );
        }

        return $next( $request );
    }
}
