<?php

namespace App\Http\Middleware;

use Closure;

class ActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->status == 0) {
            $noti = array("message" => "Your account is blocked ", "alert-type" => "warning");
            return redirect()->route('setting.user_deactivate_view')->with($noti);

        }

        return $next($request);
    }
}
