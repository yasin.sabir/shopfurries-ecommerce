<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_log extends Model
{
    protected $table = "order_log";
    protected $fillable = [
        'user_id','product_id','product_name','product_meta','product_variation_id','user_meta','price','qty','back_order_status','order_type'
    ];
}
