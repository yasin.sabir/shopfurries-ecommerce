<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderparts extends Model
{
    //
    protected $table    = "orderparts";
    protected $fillable = ['title', 'parts', 'days'];
    public $timestamps  = true;
}
