<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category_Meta extends Model
{
    //
    protected $table = "category_meta";
    public $timestamps = false;
    protected $fillable = [ 'category_id', 'meta_key', 'meta_value'];




    public function category(){
        return $this->belongsTo('App\Category');
    }

}
