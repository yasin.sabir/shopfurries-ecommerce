<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    protected $fillable = [
            'user_id','title','description','sku','image','video','price','stock','status','product_type'
   ];

    //protected $with = ['product_metas'];

   public function products(){
       return $this->hasMany('App\products_gallery');
   }

   public function product_metas(){
       return $this->hasMany('App\products_meta', 'product_id', 'id');
   }

   public function product_galleries(){
       return $this->hasMany('App\products_gallery', 'product_id', 'id');
   }

   public function categories(){
       return $this->belongsToMany(Category::class);
   }

	public function productCategories(){
		return $this->hasMany(Category::class);
	}

   public function tags(){
      return $this->belongsToMany(Tag::class);
   }

    public function productReviews(){
        return $this->hasMany('App\Product_Review','product_id','id');
    }

    public function attributes(){
        return $this->hasMany('App\ProductAttribute' , 'product_id');
    }

    public function variations(){
        return $this->hasMany('App\ProductVariation','product_id');
    }

}
