<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table = "orders";
    protected $fillable = [
        'order_id',
        'user_email',
        'order_details',
        'product_detail',
        'product_extra_detail',
        'user_meta',
        'user_id',
        'subtotal',
        'total',
        'subtotal_of_products',
        'total_shipping',
        'discount',
        'discount_type',
        'selected_country',
        'cost_per_country_weight',
        'total_weight',
        'total_products_weight',
        'status',
        'selected_order_part_id',
        'order_part_done',
        'order_log'
    ];
}
