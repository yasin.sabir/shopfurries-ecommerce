<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productAttribute extends Model
{
    protected $table = "product_attributes";
    protected $fillable = [
        'product_id', 'attribute', 'value'
    ];
    public $timestamps = true;

    public function product(){
        return $this->belongsTo(Product::class ,'product_id' ,'id');
    }


}
