<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productimgtemp extends Model
{
    protected $table = "productimgtemp";
    protected $fillable = [
        'name','image','category_id'
    ];
}
