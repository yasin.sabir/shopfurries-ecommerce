<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products_gallery extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'image'
   ];

   public function products()
   {
       return $this->belongsTo('App\Product');
   }
}
