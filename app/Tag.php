<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $table = "tags";
    protected $fillable = [ 'name', 'alias'];

    public function products(){
        return $this->belongsToMany(Product::class);
    }

}
