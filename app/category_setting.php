<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category_setting extends Model
{
    //
    public $table       = "category_setting";
    public $timestamps  = true;
    protected $fillable = ['status', 'reason_titles', 'reason_descriptions', 'feature_titles', 'feature_descriptions', 'posters', 'actual_material_pics'];
}
