<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = "coupons";
    protected $fillable = [
        'code', 'type', 'max_uses', 'max_uses_user', 'discount', 'status', 'freedelivery',
        'exclude_category','exclude_product','start_date','expire_date'
   ];
   
   public function products()
   {
       return $this->hasMany('App\Product');
   }
   
   public function category()
   {
       return $this->hasMany('App\Category');
   }
}
