<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = "categories";
    protected $fillable = [ 'id', 'Name', 'Alias', 'Image', 'Parent', 'Top', 'Status', 'Additional_Req' ];


    public function products()
    {
        return $this->belongsToMany( Product::class );
    }


    public function category_metas()
    {
        return $this->hasMany( 'App\Category_Meta' );
    }


}
