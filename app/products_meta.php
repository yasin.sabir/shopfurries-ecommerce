<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products_meta extends Model
{
    public $timestamps = false;
    protected $table = "products_metas";
    protected $fillable = [
        'product_id','product_meta' , 'product_meta_value'
   ];


    public function product(){
        return $this->belongsTo('App\Product' ,'product_id', 'id');
    }

}
