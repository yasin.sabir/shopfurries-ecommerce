<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderStatus extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $product_extra_details= [];
        $product_details = [];
        $user_detail = $this->order->user_meta;
        $order_details = unserialize($this->order->order_details);
        $product_detail = unserialize($this->order->product_detail);
        $product_extra_detail =  unserialize($this->order->product_extra_detail);

        foreach($product_extra_detail as $data){
            $product_extra_details[] = unserialize($data);
        }

        foreach($product_detail as $data){
            $product_details[] = unserialize($data);
        }

        $orderCal['selected_country']          = $this->order->selected_country;
        $orderCal['shipping_cost']             = $this->order->total_shipping;
        $orderCal['cost_per_country_weight']   = $this->order->cost_per_country_weight;
        $orderCal['weight_cost']               = $this->order->total_weight;
        $orderCal['total_products_weight']     = $this->order->total_products_weight;  //in (kg)
        $orderCal['coupon_discount']           = $this->order->discount;
        $orderCal['coupon_type']               = $this->order->discount_type;
        $orderCal['subtotal_of_products']      = $this->order->subtotal_of_products;
        $orderCal['subtotal']                  = $this->order->subtotal;

        if($this->order['status'] != "Cancel") {

            return $this->from('admin@shopfurries.com')
                        ->subject('Order Confirmation')
                        ->markdown('emails.new_order',[
                                    'order'                => $this->order,
                                    'order_details'        => $order_details,
                                    'product_detail'       => $product_details,
                                    'product_extra_detail' => $product_extra_details,
                                    'orderCal'             => $orderCal
                                   ]);
        }else{

            return $this->from('admin@shopfurries.com')
                        ->subject('Order Cancelled')
                        ->markdown('emails.order_cancel',[
                                     'order'                => $this->order,
                                     'order_details'        => $order_details,
                                     'product_detail'       => $product_details,
                                     'product_extra_detail' => $product_extra_details,
                                     'orderCal'             => $orderCal
                                   ]);
        }

        //->with('order', $this->order,'order_details',$order_details,'product_detail' , $product_details,'product_extra_detail',$product_extra_details);
    }
}
