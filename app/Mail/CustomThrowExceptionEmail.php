<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomThrowExceptionEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The body of the message.
     *
     * @var string
     */
    public $content;
    private $developerEmail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content , $developerEmail = null)
    {
        $this->content = $content;
        $this->developerEmail = $developerEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Exception - '.now())->markdown('emails.Exceptions.exceptions')->with('content', $this->content);
    }
}
