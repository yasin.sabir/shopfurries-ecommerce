<?php

namespace App\Mail\Product;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductSubmitted_Admin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($detail_send_to_Admin)
    {
        //
        $this->detail_send_to_Admin = $detail_send_to_Admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        return $this->view('view.name');
        return $this->from('admin@shopfurries.com' ,"ShopFurries")->subject('Product Submission')->markdown('emails.Product.ProductSubmitted_Admin')->with(['detail_send_to_Admin' => $this->detail_send_to_Admin ]);
    }
}
