<?php

namespace App\Mail\Product;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductPublished_User extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userProductMail_Details)
    {
        //
        $this->userProductMail_Details = $userProductMail_Details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('admin@shopfurries.com' ,"ShopFurries")->subject('Congratulation! Product Published')->markdown('emails.Product.ProductPublished_User')->with(['userProductMail_Details' => $this->userProductMail_Details ]);
    }
}
