<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $table = "events";
    protected $fillable = ['name','status', 'address', 'state','city' ,'country', 'image', 'timing','details'];
    public $timestamps = true;
}
