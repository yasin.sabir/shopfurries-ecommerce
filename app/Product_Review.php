<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Review extends Model
{
    //
    protected $table = "product_reviews";
    protected $fillable = ['user_id', 'product_id', 'status', 'flag', 'description', 'image', 'video','parent_id','voted','unvoted'];
    public $timestamps = true;


    public function products()
    {
        return $this->belongsTo('App\Product');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }


}
