<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    //
    protected $table = 'users_meta';
    protected $fillable = ['user_id' , 'meta_key' , 'meta_value'];
    public $timestamps = true;


    public function user(){
        return $this->belongsTo('App\User');
    }

}
