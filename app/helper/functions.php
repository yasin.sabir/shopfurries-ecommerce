<?php

use App\SiteSetting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Product;

function customVarDump($val){
    echo "<pre>";
    var_dump($val);
}

function customVarDump_die($val){
    echo "<pre>";
    var_dump($val);
    die;
}

function getCategoryName($val){

    if($val !== 'root'){
        $cat_name = \App\Category::find($val);

        if($cat_name){
            return $cat_name['Name'];
        }else{
            return "Not Found!";
        }
    }else{
        return "Root";
    }
}


function getAllUserMetas($user_id){

    $users_metas = \App\User::find($user_id)->metas;

    $metas = [];

    foreach ($users_metas as $val){
        $metas[$val->meta_key] = $val->meta_value;
    }

    return $metas;
}


function getCustomerCount(){
    $customer = \App\User::role('customer')->get();
    return count($customer);
}

function getVendorCount(){
    $customer = \App\User::role('vendor')->get();
    return count($customer);
}


function getUserCount(){
    $customer = \App\User::role('user')->get();
    return count($customer);
}

function getCategoryCount(){
    $cate = \App\Category::all();
    return count($cate);
}

function getFaqCount(){
    $faq = \App\Faqs::all();
    return count($faq);
}

function getCouponCount(){
    $c = \App\Coupon::all();
    return count($c);
}

//function getInvoiceCount(){
//    $customer = \App\::role('user')->get();
//    return count($customer);
//}

function getOrderCount(){
    $order = \App\order::all();
    return count($order);
}

function getInvoicesCount(){
    $invoice = \App\order::all();
    return count($invoice);
}


function getArtistCount(){
    $artist = \App\Artist::all();
    return count($artist);
}

function getProductCount(){
    $product = product::all();
    return count($product);
}

function getTagCount(){
    $tags = \App\Tag::all();
    return count($tags);
}

function getEventCount(){
    $events = \App\Event::all();
    return count($events);
}

function getCustomerReviewCount(){
    $cr = \App\CustomerReviews::all();
    return count($cr);
}

function site_config($val){
    $data = \App\SiteSetting::where(['key' => $val])->first();
    return $data->value;
}

function getImage(){

    $default_size = ( !empty(site_config('product_image_size')) ? site_config('product_image_size') : 600  );

    $rr  = 'a:3:{i:300;s:58:"upload/products/product_215/feature/300x300/1620167896.jpg";i:600;s:58:"upload/products/product_215/feature/600x600/1620167896.jpg";i:1024;s:60:"upload/products/product_215/feature/1024x1024/1620167896.jpg";}';
    $img = unserialize($rr);

    if(isset($img[$default_size])) {
        return $img[$default_size];
    }

}

function getUserProductsCount(){

    $user_id = Auth::user()->id;
    $products = product::where(['user_id' => $user_id])->get();
    return count($products);
}

function getAllInquires(){

    $data = \App\Contact::all();
    return count($data);
}


function getAbandonedCartCount(){

    $log_data = DB::table('order_log')->select(DB::raw('count(*) as items , user_id'))->groupBy('user_id')->get();
    $abandonedCart    = [];
    $data             = SiteSetting::where( [ 'key' => 'abandoned_cart_days' ] )->first();
    $abandoned_days   = ( isset($data->value) ) ? $data->value  : 2;

    foreach ($log_data as $key => $val){
        $user_exit = User::find($val->user_id);
        if( $user_exit ){
            $abandonedCart [] = $val;
        }
    }



    return count($abandonedCart);
}


function hasAdmin(){

    $user = Auth::user();

    if($user->hasRole(1)){
        return true;
    }else{
        return false;
    }
}

function isAdmin(){
	$user = Auth::user();

	if($user->hasRole(1)){
		return true;
	}else{
		return false;
	}
}


function profile_pic($id){
    $user = \App\User::find($id);

    if($user->profile_pic !== null){
        $profile_picture = "storage/".$user->profile_pic;
        return $profile_picture;
    }else{
        $placeholder = "images/placeholders/Profile_avatar_placeholder_lg.png";
        return $placeholder;
    }

}

function get_env($string){
    return env($string);
}


function get_AllAdmin(){

    $val = DB::table('model_has_roles')->where('name' ,'=' ,'admin')
        ->join('roles' , 'model_has_roles.role_id' ,'=' , 'roles.id')
        ->select('model_has_roles.model_id as user_id')->get();

   return $val;
}


function get_UserRole($user_id){

   $role = $UserRole = DB::table('model_has_roles')
            ->where('model_has_roles.model_id','=',$user_id )
            ->join('roles' , 'model_has_roles.role_id' , '=' , 'roles.id')
            ->select('roles.name')
            ->first();

   return $role->name;

}


function get_pending_artWork(){
    $gallery = \App\Gallery::where(['status' => 0])->get();
    return count($gallery);
}


function get_admin_email(){
    $admin = Auth::user();
    if($admin->hasRole(1)){
        return $admin->email;
    }
}

function get_admin_detail(){
    $admin = Auth::user();
    if($admin->hasRole(1)){
        return $admin;
    }
}


function get_product_meta($id , $key){

    $meta = \App\products_meta::where(['product_id' => (int)$id])->where(['product_meta' => $key])->first();
    return $meta['product_meta_value'];
}


function getProductFeatureImage($image){

    $ff           = @unserialize($image);
    $default_size = \App\SiteSetting::where(['key' => 'product_image_size'])->first();

    if(!is_array($ff)){
        if(!empty($image)){
            return "storage/".$image;
        }else{
            return "images/placeholders/default-placeholder-600x600.png";
        }
    }else{
        return "storage/".$ff[$default_size->value];
    }
}


function get_viewed_product(Request $request){
    return unserialize($request->cookie('viewed_products_items'));
}

function isUserVerify_SocialLogin(){

    $user = User::find(Auth::user()->id);

    switch ($user->provider){
        case 'google':
            break;
        case 'facebook':
            break;
        case 'deviantart':

            if( strpos($user->email , 'sample-deviantart.com') !== false ){
                return 1;
            }else{
                return 0;
            }

            break;
    }

}


function custom_base64_encode( $data ){
    return base64_encode( time().'-'.'012345ABCDEF'.'-'.$data );
}

function custom_base64_decode( $data ){
    $dd = base64_decode($data);
    $real_str = explode('-', $dd);
    return end($real_str);
}

function getOrderByStatus($status){
    $orders = \App\order::where('status' , $status)->get();
    return count( $orders );
}




//function imageResizing($id, $type , $slug , $file_key , $sizes){
//
//    if(!empty($sizes) && count($sizes) > 0){
//
//        foreach ($sizes as $key => $val){
//            $folderSizes   = $val."x".$val;
//            $folderName  = $slug."_".$id;
//            $directory    = "public/upload/".$type."/".$folderName."/".$folderSizes."/";
//            Storage::makeDirectory($directory);
//        }
//
//        if(Input::hasFile($file_key)){
//
//            $image     = Input::file($file_key);
//            $filename  = time() . '.' . $image->getClientOriginalExtension();
//
//            foreach ($sizes as $key => $val){
//                $folderSizes   = $val."x".$val;
//                $folderName  = $slug."_".$id;
//                $path           = storage_path('app/public/upload/'.$type.'/'.$folderName.'/'.$folderSizes.'/').$filename;
//                $img            = Image::make($image->getRealPath());
//                $img->resize($val, $val, function ($constraint) {
//                    $constraint->aspectRatio();
//                    $constraint->upsize();
//                });
//                $img->save($path);
//            }
//
//        }
//    }
//
//}
