<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table = "guests";
    protected $fillable = [
        'guestID', 'name', 'email', 'guest_meta', 'stripe_customer_id' , 'place_order_status'
    ];
    public $timestamps = true;


}
