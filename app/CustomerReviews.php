<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerReviews extends Model
{
    protected $table = "customer_reviews";
    protected $fillable = ['name', 'image', 'rating', 'detail', 'status'];
    public $timestamps = true;
    
}
