<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
//'artist_id', 'title', 'image', 'status'
    protected $table    = "galleries";
    protected $fillable = ['artist_id', 'title', 'image', 'status', 'submission' ,' description', 'tags'];
    public $timestamps  = true;

    public function artists(){
        return $this->belongsTo('App\Artist');
    }


}




