<?php

namespace App\Console\Commands;

use App\Mail\ContactUs;
use App\Mail\Testing;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class everyMinute_OrderPart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'every_minute:orderPartCheck';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This command check users orders if user purchased something and didn't paid full amount of that order send mail to that user to notify the further cost to pay!";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = "DUMMY MAIL";
        Mail::to("yasin.100@hotmail.com")->send(new Testing($data));
    }
}
