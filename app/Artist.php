<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    //
    protected $table = "artists";
    protected $fillable = ['name', 'email' , 'image', 'status', 'additional_details'];
    public $timestamps = true;


    public function galleries(){
        return $this->hasMany('App\Gallery','artist_id','id');
    }



}
