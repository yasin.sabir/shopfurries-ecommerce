<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $table = "shipping";
    protected $fillable = [
        'country','weight','price','shipping_cost_country'
    ];
    public $timestamps = false;
}
