<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productVariation extends Model
{
    protected $table = "product_variations";
    protected $fillable = [
       'status' , 'product_id', 'variation' , 'variation_meta' , 'image', 'price', 'sale_price', 'stock', 'sku', 'description'
    ];
    public $timestamps = true;


    public function product(){
        return $this->belongsTo(Product::class ,'product_id' ,'id');
    }


}
