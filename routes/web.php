<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use Analytics;
//use Spatie\Analytics\Analytics;
use Spatie\Analytics\Period;

Route::get('analytics' , function(){
//fetch the most visited pages for today and the past week
    $dd = Analytics::fetchMostVisitedPages(Period::days(1));
    customVarDump_die($dd);
});

include_once( 'Default-AdminThemeRoutes/route.php' );

Auth::routes( [ 'verify' => true ] );
Route::get( '/logout', 'Auth\LoginController@logout' );

//For Mail Preview
include_once( 'Mail-Preview-Routes/routes.php' );

// For Checkout  Routes
include_once( 'Checkout-Routes/route.php' );


// For Gallery View Routes
include_once( 'Gallery-Routes/routes.php' );


//For Wishlist Product
include_once( 'Wishlist-Routes/route.php' );

// Specific Routes For Admin
//include_once ('Category-Routes/route.php');
Route::get( 'Auth/Admin/Registration/Page', 'Auth\RegisterController@adminRegisterView' )->name( 'admin_register_view' );

// For Role Routes
include_once( 'Roles-Routes/route.php' );

// Testing purpose Routes
include_once( 'Testing-Routes/route.php' );

// For Social Login Routes
include_once( 'SocialLogin-Routes/route.php' );

// For Front View Routes
include_once( 'Front-End/route.php' );

// For Order Routes
include_once( 'Order-Routes/route.php' );

// For Error Routes
include_once ('Errors-Routes/route.php');

Route::group( [
                  'prefix'     => LaravelLocalization::setLocale(),
                  'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
              ], function () {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/

    // For FAQS Admin Section Routes
    include_once( 'Faqs-Routes/route.php' );

    // For Users Routes
    include_once( 'Users-Routes/route.php' );

    //For Guest Routes
    include_once( 'Guest-Routes/route.php' );

    // For Product Routes
    include_once( 'Product-Routes/route.php' );

    // For Category Routes
    include_once( 'Category-Routes/route.php' );

    //For Artist Route
    include_once( 'Artist-Routes/route.php' );

    // For Events
    include_once( 'Event-Routes/route.php' );

    // For Customer Reviews for Homer Page
    include_once( 'Customer-Reviews-Routes/route.php' );


    // For Shipping Routes
    include_once( 'Shipping-Routes/route.php' );

    //For Invoice Route
    include_once ( 'Invoice-Routes/route.php' );

    //For Abandoned Cart Route
    include_once ( 'AbandonedCart-Routes/route.php' );

    //For Reports Routes
    include_once ( 'Reports-Routes/routes.php' );

    // For Coupon Routes
    include_once( 'Coupon-Routes/route.php' );

    //For Admin Setting Routes
    include_once( 'Admin-Routes/route.php' );

    //For ContactUs Routes
    include_once( 'ContactUs-Routes/route.php' );

    // For Admin Gallery View Routes
    include_once( 'Gallery-Routes/Admin-Gallery-Routes/routes.php' );

} );


//Route::fallback(function () {
//    return view('errors.front-end-errors.page-error-404');
//});


