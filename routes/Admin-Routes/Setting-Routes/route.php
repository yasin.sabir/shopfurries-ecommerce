<?php

//Route::group([
//    'prefix' => LaravelLocalization::setLocale() ,
//    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
//], function(){
//    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/


    Route::group(['prefix' => 'Settings' , 'as' => 'setting' , 'middleware' => ['auth']] , function(){

        Route::get('/GeneralConfig'                             ,['as' => '.general-config'                  ,'uses' => 'SettingsController@s_gc_maintenance']);
        Route::get('/MediaConfig'                               ,['as' => '.media-config'                    ,'uses' => 'SettingsController@mediaConfig_view']);
        Route::post('/MediaConfig/Update'                       ,['as' => '.media-config-update'             ,'uses' => 'SettingsController@mediaConfig_update']);
        Route::get('/UserDeactivate'                            ,['as' => '.user_deactivate_view'            ,'uses' => 'SettingsController@user_deactivate_view']);
        Route::post('/DefaultSalesTax'                          ,['as' => '.default_sales_tax'               ,'uses' => 'SettingsController@defaultSaleTax']);
        Route::post('/DefaultProductStock'                      ,['as' => '.default_product_stock'           ,'uses' => 'SettingsController@defaultProductStock']);
        Route::post('/UptoReviews'                              ,['as' => '.upto_reviews'                    ,'uses' => 'SettingsController@uptoReviews']);
        Route::post('/GeneralConfig/MaintenanceMode/Update/'    ,['as' => '.general-config-maintenance-mode' ,'uses' => 'SettingsController@s_gc_maintenance_Update']);
        Route::post('/AbandonedCartDays'                        ,['as' => '.abandoned_cart_days'             ,'uses' => 'SettingsController@abandoned_cart_days']);
        Route::get('/Export/Emails'                             ,['as' => '.display_export_emails'           ,'uses' => 'SettingsController@display_export_emails']);
        Route::post('/Export/Emails/Data'                       ,['as' => '.export_emails'                   ,'uses' => 'SettingsController@exportEmails']);
    });

//Route::get('/' , 'SettingsController@s_gc_maintenanceView')->name('maintenance_mode');

//});

