<?php
Route::group(['prefix' => 'Abandoned' , 'as' => 'abandoned' , 'middleware' => ['auth']], function () {

    Route::get('/List'                 ,['as' => '.list'                ,'uses' => 'AbandonedController@index']);
    Route::post('/Delete/{id}'         ,['as' => '.delete'              ,'uses' => 'AbandonedController@destroy' ]);
    Route::post('/DeleteAll'           ,['as' => '.delete-all'          ,'uses' => 'AbandonedController@destroyAll' ]);
    Route::post('/Email'               ,['as' => '.send-email'          ,'uses' => 'AbandonedController@sendAbandonedEmail' ]);

});

