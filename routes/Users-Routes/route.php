<?php


//Route::group([
//    'prefix' => LaravelLocalization::setLocale() ,
//    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
//], function(){
//    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
//
//    Route::get('/Dashboard','FrontViews\HomeController@dashboard')->middleware(['auth','active_user'])->name('dashboard');
//
//});



Route::group(['prefix' => 'Users' , 'as' => 'user' , 'middleware' =>['auth' ,'admin'] ], function () {

    Route::get('Customers/List' ,['as' => '.customers.list' ,'uses' => 'UsersController@customersList' ]);
    Route::get('Vendors/List' ,['as' => '.vendors.list' ,'uses' => 'UsersController@vendorsList' ]);


    Route::get('/List', ['as' => '.list' , 'uses' => 'UsersController@index' ] );
    Route::get('/Add' ,['as' => '.add' , 'uses' =>  'UsersController@create' ]);
    Route::get('/Create' ,['as' => '.create' , 'uses' =>  'UsersController@store' ]);
    Route::get('/Show/{id}' ,['as' => '.show' , 'uses' =>  'UsersController@show' ]);
    Route::get('/Edit/{id}' ,['as' => '.edit' , 'uses' =>  'UsersController@edit' ]);
    Route::post('/Update/{id}' ,['as' => '.update' , 'uses' =>  'UsersController@update' ]);
    Route::post('/Delete/{id}' ,['as' => '.delete' , 'uses' =>  'UsersController@destroy' ]);
    Route::post('/DeleteAll'  ,['as' => '.delete-all'   , 'uses' =>  'UsersController@destroyAllUsers' ]);
    Route::post('/Status' ,['as' => '.status' , 'uses' =>  'UsersController@user_status' ]);
});

Route::group(['prefix' => 'User' , 'as' => 'user' , 'middleware' =>['auth'] ], function () {
    Route::get('/Account' ,['as' => '.account' , 'uses' =>  'UsersController@show_account' ]);
    Route::get('/Account/Edit/{id}' ,['as' => '.edit-account' , 'uses' =>  'UsersController@edit_account' ]);
    Route::post('/Account/Update/{id}' ,['as' => '.update-account' , 'uses' =>  'UsersController@update_account' ]);
    Route::get('/ChangePassword' , ['as' => '.changePassword' , 'uses' => 'UsersController@changePasswordView']);
    Route::post('/updatePassword/{id}' , ['as' => '.update-password' , 'uses' => 'UsersController@updatePassword']);
});

Route::get('/verification/SocialAuth/Deviantart/{token}' , 'UsersController@verifyUser_socialAuth')->name('verifyUser_socialAuth');
