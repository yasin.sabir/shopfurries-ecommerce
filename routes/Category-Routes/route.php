<?php

Route::group(['prefix' => 'Category' , 'as' => 'category' , 'middleware' =>['auth']  ], function () {
    
    Route::get('/List' , ['as' => '.list' , 'uses' => 'CategoryController@index']);
    Route::get('/Add' ,['as' => '.add' , 'uses' =>  'CategoryController@create' ]);
    Route::post('/Create' ,['as' => '.create' , 'uses' =>  'CategoryController@store' ]);
    Route::get('/Show/{id}' ,['as' => '.show' , 'uses' =>  'CategoryController@show' ]);
    Route::get('/Edit/{id}' ,['as' => '.edit' , 'uses' =>  'CategoryController@edit' ]);
    Route::post('/Update/{id}' ,['as' => '.update' , 'uses' =>  'CategoryController@update' ]);
    Route::post('/Delete/{id}' ,['as' => '.delete' , 'uses' =>  'CategoryController@destroy' ]);
    Route::post('/DeleteAll'  ,['as' => '.delete-all'   , 'uses' =>  'CategoryController@destroyAllCategories' ]);
    Route::post('/Delete-Poster-Images/{id}',['as' => '.delete-poster-images' , 'uses' => 'CategoryController@delete_poster_images']);
    Route::post('/Delete-Actual-Material-Images/{id}',['as' => '.delete-actual-material-images' , 'uses' => 'CategoryController@delete_actual_material_images']);

    //Category Setting

    Route::post('/Setting/Status-Update/{id}' , ['as' => '.setting-status-update' , 'uses' => 'CategoryController@SettingStatus_update']);


    Route::get('/Setting/' , ['as' => '.setting' , 'uses' => 'CategoryController@SettingView']);
    Route::get('/Setting/Add/' , ['as' => '.setting-add' , 'uses' => 'CategoryController@SettingView_create']);
    Route::post('/Setting/Create/' , ['as' => '.setting-create' , 'uses' => 'CategoryController@SettingView_store']);
    Route::get('/Setting/Edit/{id}' , ['as' => '.setting-edit' , 'uses' => 'CategoryController@SettingView_edit']);
    Route::post('/Setting/Update/{id}' , ['as' => '.setting-update' , 'uses' => 'CategoryController@SettingView_Update']);

    Route::post('/Delete-CateST-Poster-Img/{id}',['as' => '.delete-cateST-poster-img' , 'uses' => 'CategoryController@delete_cateST_poster_images']);
    Route::post('/Delete-CateST-Actual-Material-Img/{id}',['as' => '.delete-cateST-actual-material-img' , 'uses' => 'CategoryController@delete_cateST_actual_material_images']);


});
