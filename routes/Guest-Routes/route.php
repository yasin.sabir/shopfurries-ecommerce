<?php


Route::group(['prefix' => 'Guest' , 'as' => 'guest'], function () {

    Route::put('/Guest-id'           ,['as' => '.store-id'      , 'uses' =>  'GuestController@store_id' ]);
    Route::post('/Delete/id'    ,['as' => '.destroy-id'    , 'uses' =>  'GuestController@destroy_id' ]);


//    Route::get('/List'          ,['as' => '.list'        , 'uses' =>  'GuestController@index' ] );
//    Route::get('/Add'           ,['as' => '.add'         , 'uses' =>  'GuestController@create' ]);
//    Route::get('/Create'        ,['as' => '.create'      , 'uses' =>  'GuestController@store' ]);
//    Route::get('/Show/{id}'     ,['as' => '.show'        , 'uses' =>  'GuestController@show' ]);
//    Route::get('/Edit/{id}'     ,['as' => '.edit'        , 'uses' =>  'GuestController@edit' ]);
//    Route::post('/Update/{id}'  ,['as' => '.update'      , 'uses' =>  'GuestController@update' ]);
//    Route::post('/Delete/{id}'  ,['as' => '.delete'      , 'uses' =>  'GuestController@destroy' ]);
//    Route::post('/DeleteAll'    ,['as' => '.delete-all'  , 'uses' =>  'GuestController@destroyAllUsers' ]);
//    Route::post('/Status'       ,['as' => '.status'      , 'uses' =>  'GuestController@user_status' ]);
});
