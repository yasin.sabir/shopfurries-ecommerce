<?php

Route::group(['prefix' => 'Event' , 'as' => 'event' , 'middleware' => ['auth']] , function() {

    Route::get('/List' , ['as' => '.list' , 'uses' => 'EventsController@index']);
    Route::get('/Add'  , ['as' => '.add' , 'uses'  => 'EventsController@create']);
    Route::get('/Show/{id}'  , ['as' => '.show' , 'uses'  => 'EventsController@show']);
    Route::post('/Create' , ['as' => '.create' , 'uses' => 'EventsController@store']);
    Route::get('/Edit/{id}' , ['as' => '.edit' , 'uses' => 'EventsController@edit']);
    Route::post('/Update/{id}' , ['as' => '.update' , 'uses' => 'EventsController@update']);
    Route::post('/Status/Update/{id}' , ['as' => '.status-update' , 'uses' => 'EventsController@updateStatus']);
    Route::post('/Delete/{id}' , ['as' => '.delete' , 'uses' => 'EventsController@destroy']);
    Route::post('/DeleteAll'  ,['as' => '.delete-all'   , 'uses' =>  'EventsController@destroyAll' ]);

});

