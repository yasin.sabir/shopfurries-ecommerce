<?php

Route::group(['prefix' => 'Customer/Reviews' , 'as' => 'customer-reviews' , 'middleware' => ['auth']] , function() {

    Route::get('/List' , ['as' => '.list' , 'uses' => 'CustomerReviewController@index']);
    Route::get('/Add'  , ['as' => '.add' , 'uses'  => 'CustomerReviewController@create']);
    Route::get('/Show/{id}'  , ['as' => '.show' , 'uses'  => 'CustomerReviewController@show']);
    Route::post('/Create' , ['as' => '.create' , 'uses' => 'CustomerReviewController@store']);
    Route::get('/Edit/{id}' , ['as' => '.edit' , 'uses' => 'CustomerReviewController@edit']);
    Route::post('/Update/{id}' , ['as' => '.update' , 'uses' => 'CustomerReviewController@update']);
    Route::post('/Status/Update/{id}' , ['as' => '.status-update' , 'uses' => 'CustomerReviewController@updateStatus']);
    Route::post('/Delete/{id}' , ['as' => '.delete' , 'uses' => 'CustomerReviewController@destroy']);
    Route::post('/DeleteAll'  ,['as' => '.delete-all'   , 'uses' =>  'CustomerReviewController@destroyAll' ]);

});

