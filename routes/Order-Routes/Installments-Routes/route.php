<?php


Route::group(['prefix' => 'Parts' , 'as' => '.parts' , 'middleware' => ['auth'] ] , function(){
    
    Route::get('/List'          ,['as' => '.list'       ,'uses' => 'OrderpartsController@index' ]);
    Route::get('/Add'           ,['as' => '.add'        ,'uses' => 'OrderpartsController@create' ]);
    Route::post('/Create'       ,['as' => '.create'     ,'uses' => 'OrderpartsController@store' ]);
    Route::get('/Show/{id}'     ,['as' => '.show'       ,'uses' => 'OrderpartsController@show' ]);
    Route::get('/Edit/{id}'     ,['as' => '.edit'       ,'uses' => 'OrderpartsController@edit' ]);
    Route::post('/Update/{id}'  ,['as' => '.update'     ,'uses' => 'OrderpartsController@update' ]);
    Route::post('/Delete/{id}'  ,['as' => '.delete'     ,'uses' => 'OrderpartsController@destroy' ]);
    Route::post('/DeleteAll'    ,['as' => '.delete-all' ,'uses' => 'OrderpartsController@destroyAllOrderparts' ]);

});
