<?php

Route::group(['prefix' => 'Order' ,  'as' => 'order' ] , function(){

    Route::get('/AllList'                    ,['as' => '.alllist'        ,'uses' => 'OrderController@allorderlist'              ,'middleware' => ['auth']]);
    Route::get('/MyList'                     ,['as' => '.mylist'         ,'uses' => 'OrderController@myorderlist'               ,'middleware' => ['auth']]);
    Route::post('/Create'                    ,['as' => '.create'         ,'uses' => 'OrderController@store'                     ,'middleware' => ['auth']]);
    Route::get('/Show/{id}'                  ,['as' => '.show'           ,'uses' => 'OrderController@show'                      ,'middleware' => ['auth']]);
    Route::get('/Edit/{id}'                  ,['as' => '.edit'           ,'uses' => 'OrderController@edit'                      ,'middleware' => ['auth']]);
    Route::post('/Update/{id}'               ,['as' => '.update'         ,'uses' => 'OrderController@update'                    ,'middleware' => ['auth']]);
    Route::post('/Delete/{id}'               ,['as' => '.delete'         ,'uses' => 'OrderController@destroy'                   ,'middleware' => ['auth']]);
    Route::post('/DeleteAll'                 ,['as' => '.delete-all'     ,'uses' => 'OrderController@destroyAllOrders' ]);
    Route::post('/ProductDelete/{id}'        ,['as' => '.productdelete'  ,'uses' => 'OrderController@productdestroy'            ,'middleware' => ['auth']]);
    Route::post('/Buy-Now'                   ,['as' => '.buynow'         ,'uses' => 'OrderController@Buynow' ]);
    Route::post('/Buy-Now-upsell'            ,['as' => '.buynowupsell',  'uses' =>  'OrderController@Buynowupsell' ]);
    Route::post('/Pdfgenerate'               ,['as' => '.pdfgenerate'    ,'uses' => 'OrderController@pdfgenerate'                ,'middleware' => ['auth'] ]);
    Route::get('/Order/Export/Excel'         ,['as' => '.export-excel'   ,'uses' => 'OrderController@exportOrder_excel'          ,'middleware' => ['auth']]);
    Route::get('/Order/Export/{order_id}'    ,['as' => '.export'         ,'uses' => 'OrderController@exportSpecificOrderExcel'   ,'middleware' => ['auth']]);


    //Create order from dashboard panel manually for testing purpose only.
   Route::group(['prefix' => 'Manual' , 'as' => '.manual' ] , function (){
       Route::get('/AllList'                    ,['as' => '.alllist'               ,'uses' => 'OrderController@manual_allorderlist'         ,'middleware' => ['auth']]);
       Route::get('/Create'                     ,['as' => '.add'                   ,'uses' => 'OrderController@manual_create'               ,'middleware' => ['auth']]);
       Route::post('/Process'                   ,['as' => '.process'               ,'uses' => 'OrderController@manual_process'              ,'middleware' => ['auth']]);
       Route::post('/Store'                     ,['as' => '.create'                ,'uses' => 'OrderController@manual_store'                ,'middleware' => ['auth']]);
       Route::get('/Show/{id}'                  ,['as' => '.show'                  ,'uses' => 'OrderController@manual_show'                 ,'middleware' => ['auth']]);
       Route::get('/Edit/{id}'                  ,['as' => '.edit'                  ,'uses' => 'OrderController@manual_edit'                 ,'middleware' => ['auth']]);
       Route::post('/Update/{id}'               ,['as' => '.update'                ,'uses' => 'OrderController@manual_update'               ,'middleware' => ['auth']]);
       Route::post('/Delete/{id}'               ,['as' => '.delete'                ,'uses' => 'OrderController@manual_destroy'              ,'middleware' => ['auth']]);
       Route::post('/Delete/Temp/Order/'        ,['as' => '.delete-temp-order'     ,'uses' => 'OrderController@manual_destroyTempOrder'     ,'middleware' => ['auth']]);
       Route::post('/Buy-Now'                   ,['as' => '.buynow'                ,'uses' => 'OrderController@manual_Buynow'               ,'middleware' => ['auth']]);
       Route::post('/Pdfgenerate'               ,['as' => '.pdfgenerate'           ,'uses' => 'OrderController@manual_pdfgenerate'          ,'middleware' => ['auth']]);
       Route::post('/Order-Create/'             ,['as' => '.order-create'          ,'uses' => 'OrderController@manual_order_creation'       ,'middleware' => ['auth']]);
   });

   include_once ("Installments-Routes/route.php");

});


