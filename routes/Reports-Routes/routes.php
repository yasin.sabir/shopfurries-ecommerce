<?php

use App\Exports\ExportOrders;
use Maatwebsite\Excel\Facades\Excel;


Route::group(['prefix' => 'Reports' , 'as' => 'reports' , 'middleware' => ['auth']] , function(){

    //Analytics Each Sales Routes ===============================================
    Route::group(['prefix' => 'EachSales' , 'as' => '.each-sales' ] , function(){

        Route::get('/List'           ,['as' => '.list'     ,'uses' => 'ReportController@eachSales_list']);
        Route::get('/Export/{id}'    ,['as' => '.export'   ,'uses' => 'ReportController@eachSales_productExport']);


    });

    //Analytics Total Sales Routes ===============================================
    Route::group(['prefix' => 'TotalSales' , 'as' => '.total-sales' ] , function(){
        Route::get('/List'           ,['as' => '.list'     ,'uses' => 'ReportController@totalSales_list']);
        Route::get('/Export'         ,['as' => '.export'   ,'uses' => 'ReportController@totalSales_productExport']);
    });


    //Analytics Country Sales Routes =============================================
    Route::group(['prefix' => 'CountrySales' , 'as' => '.country-sales' ] , function(){
        Route::get('/List'           ,['as' => '.list'     ,'uses' => 'ReportController@countrySales_list']);
        Route::get('/Export/{name}'  ,['as' => '.export'   ,'uses' => 'ReportController@countrySales_productExport']);
    });


});
