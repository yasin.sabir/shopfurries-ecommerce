<?php


Route::group(['prefix' => 'Shipping', 'as' => 'shipping', 'middleware' => ['auth']], function () {

    Route::get('/AllList',             ['as' => '.alllist',       'uses' => 'ShippingController@allshippinglist']);
    Route::get('/add',                 ['as' => '.add',           'uses' => 'ShippingController@addshipping']);
    Route::post('/Create',             ['as' => '.create',        'uses' => 'ShippingController@store']);
    Route::get('/Show/{id}',           ['as' => '.show',          'uses' => 'ShippingController@show']);
    Route::get('/Edit/{id}',           ['as' => '.edit',          'uses' => 'ShippingController@edit']);
    Route::post('/Update/{id}',        ['as' => '.update',        'uses' => 'ShippingController@update']);
    Route::post('/Delete/{id}',        ['as' => '.delete',        'uses' => 'ShippingController@destroy']);
    Route::post('/ProductDelete/{id}', ['as' => '.productdelete', 'uses' => 'ShippingController@productdestroy']);
    Route::post('/Buy-Now',            ['as' => '.buynow',        'uses' => 'ShippingController@Buynow']);


    //Default Setting of shipping weight and shipping cost
    Route::get('/Default/Add',          ['as' => '.default-add',    'uses' => 'ShippingController@defaultAddShipping']);
    Route::post('/Default/Create',      ['as' => '.default-create', 'uses' => 'ShippingController@defaultStoreShipping']);
    Route::get('/Default/Show/{id}',    ['as' => '.default-show',   'uses' => 'ShippingController@defaultShowShipping']);
    Route::get('/Default/Edit/{id}',    ['as' => '.default-edit',   'uses' => 'ShippingController@defaultEditShipping']);
    Route::post('/Default/Update/{id}', ['as' => '.default-update', 'uses' => 'ShippingController@defaultUpdateShipping']);

});


