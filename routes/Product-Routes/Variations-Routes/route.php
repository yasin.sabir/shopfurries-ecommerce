<?php

Route::group(['prefix' => 'Variation' , 'as' => '.variations' ] , function(){
    Route::get('/List' ,['as' => '.list' ,'uses' => 'ProductVariationController@index' ]);
    Route::get('/Add' ,['as' => '.add' , 'uses' =>  'ProductVariationController@create' ]);
    Route::post('/Create' ,['as' => '.create' , 'uses' =>  'ProductVariationController@store' ]);
    Route::get('/Show/{id}' ,['as' => '.show' , 'uses' =>  'ProductVariationController@show' ]);
    Route::get('/Edit/{id}' ,['as' => '.edit' , 'uses' =>  'ProductVariationController@edit' ]);
    Route::post('/Update/{id}' ,['as' => '.update' , 'uses' =>  'ProductVariationController@update' ]);
    Route::post('/Status/Update/{id}' , ['as' => '.status-update' , 'uses' => 'ProductVariationController@updateStatus']);
    Route::post('/Delete/{id}' ,['as' => '.delete' , 'uses' =>  'ProductVariationController@destroy' ]);
    Route::post('/DeleteAll'  ,['as' => '.delete-all'   , 'uses' =>  'ProductVariationController@destroyAllVariations' ]);


    //For Attributes
    Route::get('/List/Attributes' ,['as' => '.list-attr' ,'uses' => 'ProductVariationController@index_attr' ]);
    Route::get('/Add//Attribute' ,['as' => '.add-attr' , 'uses' =>  'ProductVariationController@create_attr' ]);
    Route::post('/Create/Attribute' ,['as' => '.create-attr' , 'uses' =>  'ProductVariationController@store_attr' ]);
    Route::get('/Show/Attributes/{id}' ,['as' => '.show-attr' , 'uses' =>  'ProductVariationController@show_attr' ]);
    Route::get('/Edit/Attribute/{id}' ,['as' => '.edit-attr' , 'uses' =>  'ProductVariationController@edit_attr' ]);
    Route::post('/Update/Attribute/{id}' ,['as' => '.update-attr' , 'uses' =>  'ProductVariationController@update_attr' ]);
    Route::post('/Delete/Attribute/{id}' ,['as' => '.delete-attr' , 'uses' =>  'ProductVariationController@destroy_attr' ]);
    Route::post('/DeleteAll/Attribute'  ,['as' => '.delete-all-attr'   , 'uses' =>  'ProductVariationController@destroyAllAttr' ]);

});
