<?php

    Route::group(['prefix' => 'Tags' , 'as' => '.tags' ] , function(){
        Route::get('/List' ,['as' => '.list' ,'uses' => 'TagsController@index' ]);
        Route::get('/Add' ,['as' => '.add' , 'uses' =>  'TagsController@create' ]);
        Route::post('/Create' ,['as' => '.create' , 'uses' =>  'TagsController@store' ]);
        Route::get('/Show/{id}' ,['as' => '.show' , 'uses' =>  'TagsController@show' ]);
        Route::get('/Edit/{id}' ,['as' => '.edit' , 'uses' =>  'TagsController@edit' ]);
        Route::post('/Update/{id}' ,['as' => '.update' , 'uses' =>  'TagsController@update' ]);
        Route::post('/Delete/{id}' ,['as' => '.delete' , 'uses' =>  'TagsController@destroy' ]);
        Route::post('/DeleteAll'  ,['as' => '.delete-all'   , 'uses' =>  'TagsController@destroyAllTags' ]);

    });
