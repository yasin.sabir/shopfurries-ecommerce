<?php

Route::group(['prefix' => 'Product' ,  'as' => 'product' , 'middleware' => ['auth'] ] , function(){

    Route::get('/List'                  ,['as' => '.list'                 ,'uses' => 'ProductController@index' ]);
    Route::get('/Add'                   ,['as' => '.add'                  ,'uses' => 'ProductController@create' ]);
    Route::post('/Create'               ,['as' => '.create'               ,'uses' => 'ProductController@store' ]);
    Route::get('/Show/{id}'             ,['as' => '.show'                 ,'uses' => 'ProductController@show' ]);
    Route::get('/Edit/{id}'             ,['as' => '.edit'                 ,'uses' => 'ProductController@edit' ]);
    Route::post('/Update/{id}'          ,['as' => '.update'               ,'uses' => 'ProductController@update' ]);
    Route::post('/Delete/{id}'          ,['as' => '.delete'               ,'uses' => 'ProductController@destroy' ]);
    Route::post('/DeleteAll'            ,['as' => '.delete-all'           ,'uses' => 'ProductController@destroyAllProducts' ]);
    Route::post('/Status/Update/{id}'   ,['as' => '.status-update'        ,'uses' => 'ProductController@updateStatus']);
    Route::post('/Export/Product/Info'  ,['as' => '.export-product-info'  ,'uses' => 'ProductController@exportProductInfo' , 'middleware' => ['auth'] ]);
    Route::get('/Export/Excel'          ,['as' => '.export-excel'         ,'uses' => 'ProductController@exportProductExcel' , 'middleware' => ['auth'] ]);

    Route::post('/PhotoDelete/'   ,['as' => '.photodelete' ,'uses' => 'ProductController@photo_delete' ]);

    //Product Review Routes
    Route::post('/Review/List'                  ,['as' => '.review-list'            ,'uses' => 'ProductController@indexReview' ]);
    Route::post('/Review/Create'                ,['as' => '.review-create'          ,'uses' => 'ProductController@storeReview' ]);
    Route::get('/Review/Edit/{id}'              ,['as' => '.review-edit'            ,'uses' => 'ProductController@editReview' ]);
    Route::get('/Review/Update/{id}'            ,['as' => '.review-update'          ,'uses' => 'ProductController@updateReview' ]);
    Route::post('/Review/Delete/{id}'           ,['as' => '.review-delete'          ,'uses' => 'ProductController@destoryReview' ]);
    Route::post('Review/DeleteAll'              ,['as' => '.review-delete-all'             ,'uses' =>  'ProductController@destroyAllReviews' ]);
    Route::post('/Review/Status-Update/{id}'    ,['as' => '.review-status-update'   ,'uses' => 'ProductController@ReviewStatus_update']);

    Route::get('/Stock/List'                    ,['as' => '.stock-list'             ,'uses' => 'ProductController@indexStock' ]);
    Route::post('/Stock/Details/Edit/{id}'       ,['as' => '.stock-detail-edit'      ,'uses' => 'ProductController@StockDetailEdit']);

    include_once ('Tags-Routes/route.php');
    include_once ('bulkproduct-Routes/route.php');
    include_once ('ProductBundles-Routes/route.php');
    include_once ('Variations-Routes/route.php');
});


