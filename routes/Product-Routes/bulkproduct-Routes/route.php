<?php

Route::group(['prefix' => 'BulkProduct' , 'as' => '.bulk' ,'middleware' =>['admin'] ] , function(){
    Route::post('/List'                 , ['as' => '.list'           , 'uses' => 'BulkProductControler@index' ]);
    Route::post('/Temporary/Upload'     , ['as' => '.temp-upload'    , 'uses' => 'BulkProductControler@tempUpload' ]);
    Route::post('/Temporary/Delete'     , ['as' => '.temp-delte'     , 'uses' => 'BulkProductControler@tempDelete' ]);
    Route::get('/Add'                   , ['as' => '.add'            , 'uses' => 'BulkProductControler@create' ]);
    Route::post('Show/Insert'           , ['as' => '.insert'         , 'uses' => 'BulkProductControler@insert' ]);
    Route::post('/Create'               , ['as' => '.create'         , 'uses' => 'BulkProductControler@store' ]);
    Route::get('/Show'                  , ['as' => '.show'           , 'uses' => 'BulkProductControler@show' ]);
    Route::get('/Edit/{id}'             , ['as' => '.edit'           , 'uses' => 'BulkProductControler@edit' ]);
    Route::post('/Update/{id}'          , ['as' => '.update'         , 'uses' => 'BulkProductControler@update' ]);
    Route::post('/Delete/{id}'          , ['as' => '.delete'         , 'uses' => 'BulkProductControler@destroy' ]);

    Route::post('/Create/All' , ['as' => '.create-all' , 'uses' => 'BulkProductControler@multiUploader']);

});
