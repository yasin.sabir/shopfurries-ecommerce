<?php

Route::group(['prefix' => 'Bundle' , 'as' => '.bundle' ,'middleware' =>['admin'] ] , function(){
    Route::get('/List'              , ['as' => '.list'        , 'uses' => 'ProductController@pb_list' ]);
    Route::get('/Add'               , ['as' => '.add'         , 'uses' => 'ProductController@pb_add' ]);
    Route::post('/Create'           , ['as' => '.create'      , 'uses' => 'ProductController@pb_store' ]);
    Route::get('/Edit/{id}'         , ['as' => '.edit'        , 'uses' => 'ProductController@pb_edit' ]);
    Route::post('/Update/{id}'      , ['as' => '.update'      , 'uses' => 'ProductController@pb_update' ]);
    Route::post('/Delete/{id}'      , ['as' => '.delete'      , 'uses' => 'ProductController@pb_destroy' ]);
    Route::post('/DeleteALL/{id}'   , ['as' => '.delete-all'  , 'uses' => 'ProductController@pb_destroyAll' ]);
});
