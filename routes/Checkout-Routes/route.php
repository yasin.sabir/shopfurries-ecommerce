<?php

Route::group(['prefix' => 'Checkout', 'as' => 'checkout'], function () {

    Route::post('/Payment-Select'          , ['as' => '.paymentselect'      , 'uses' => 'CheckoutController@paymentselect']);
    Route::get('/Payment-return-url'       , ['as' => '.return_paypal_url'  , 'uses' => 'CheckoutController@return_paypal_url']);
    Route::get('/Payment-cancel-url'       , ['as' => '.cancel_paypal_url'  , 'uses' => 'CheckoutController@cancel_paypal_url']);
    Route::get('/pay-with-paypal'          , ['as' => '.paywithpaypal'      , 'uses' => 'CheckoutController@paywithpaypal']);
    Route::get('/create-order/'            , ['as' => '.createorder'        , 'uses' => 'CheckoutController@createorder']);

});

