<?php

//Route::group(['prefix' => LaravelLocalization::setLocale() , 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
//], function(){
//    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
//});


Route::group(['prefix' => 'Admin/FAQs' , 'as' => 'admin.faqs' , 'middleware' =>['auth'] ], function () {

    Route::get('/Add'           ,['as' => '.add'          , 'uses' =>  'FrontViews\FAQsController@create'  ]);
    Route::post('/Create'       ,['as' => '.create'       , 'uses' =>  'FrontViews\FAQsController@store'   ]);
    Route::get('/ViewAll'       ,['as' => '.viewAll'      , 'uses' =>  'FrontViews\FAQsController@viewAll' ]);
    Route::get('/Show/{id}'     ,['as' => '.show'         , 'uses' =>  'FrontViews\FAQsController@show'    ]);
    Route::get('/Edit/{id}'     ,['as' => '.edit'         , 'uses' =>  'FrontViews\FAQsController@edit'    ]);
    Route::post('/Update/{id}'  ,['as' => '.update'       , 'uses' =>  'FrontViews\FAQsController@update'  ]);
    Route::post('/Delete/{id}'  ,['as' => '.delete'       , 'uses' =>  'FrontViews\FAQsController@destroy' ]);
    Route::post('/DeleteAll'    ,['as' => '.delete-all'   , 'uses' =>  'FrontViews\FAQsController@destroyAll' ]);

});
