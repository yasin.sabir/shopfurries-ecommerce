<?php

Route::group(['prefix' => 'Coupon' , 'as' => 'coupon' , 'middleware' =>['auth']  ], function () {
    
    Route::get('/List' , ['as' => '.list' , 'uses' => 'CouponController@index']);
    Route::get('/Add' ,['as' => '.add' , 'uses' =>  'CouponController@create' ]);
    Route::post('/Create' ,['as' => '.create' , 'uses' =>  'CouponController@store' ]);
    Route::get('/Show/{id}' ,['as' => '.show' , 'uses' =>  'CouponController@show' ]);
    Route::get('/Edit/{id}' ,['as' => '.edit' , 'uses' =>  'CouponController@edit' ]);
    Route::post('/Update/{id}' ,['as' => '.update' , 'uses' =>  'CouponController@update' ]);
    Route::post('/Delete/{id}' ,['as' => '.delete' , 'uses' =>  'CouponController@destroy' ]);

});
