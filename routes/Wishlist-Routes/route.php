<?php
//'middleware' => ['auth','auth_maintenance']
Route::group(['prefix' => 'Wishlist' , 'as' => 'wishlist' , 'middleware' => ['auth','auth_maintenance']] , function() {

    Route::get('/List' , ['as' => '.list' , 'uses' => 'WishlistController@index']);
    Route::get('/Add'  , ['as' => '.add' , 'uses'  => 'WishlistController@create']);
    Route::get('/Show/{id}'  , ['as' => '.show' , 'uses'  => 'WishlistController@show']);
    Route::post('/Create/{id}' , ['as' => '.create' , 'uses' => 'WishlistController@store']);
    Route::get('/Edit/{id}' , ['as' => '.edit' , 'uses' => 'WishlistController@edit']);
    Route::post('/Update/{id}' , ['as' => '.update' , 'uses' => 'WishlistController@update']);
    Route::post('/Status/Update/{id}' , ['as' => '.status-update' , 'uses' => 'WishlistController@updateStatus']);
    Route::post('/Delete/{id}' , ['as' => '.delete' , 'uses' => 'WishlistController@destroy']);
    Route::post('/DeleteAll'  ,['as' => '.delete-all'   , 'uses' =>  'WishlistController@destroyAll' ]);


});

