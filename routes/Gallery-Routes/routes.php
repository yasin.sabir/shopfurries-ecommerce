<?php


Route::group(['prefix' => 'Gallery' , 'as' => 'gallery' , 'middleware' => ['auth' , 'checkSocialEmailAuth']] , function() {

    Route::get('/List'                  , ['as' => '.list'          , 'uses' => 'FrontViews\GalleryController@listView']);
    Route::post('/Bulk'                 , ['as' => '.bulk-list'     , 'uses' => 'FrontViews\GalleryController@bulk']);
    Route::post('Show/Insert'           , ['as' => '.insert'        , 'uses' => 'FrontViews\GalleryController@insert' ]);
    Route::get('/Add'                   , ['as' => '.add'           , 'uses' => 'FrontViews\GalleryController@create']);
    Route::get('/Show'                  , ['as' => '.show'          , 'uses' => 'FrontViews\GalleryController@show']);
    Route::post('/SearchResult'          , ['as' => '.search-result' , 'uses' => 'FrontViews\GalleryController@searchResult']);
    Route::post('/Create'               , ['as' => '.create'        , 'uses' => 'FrontViews\GalleryController@store']);
    Route::get('/Edit/{id}'             , ['as' => '.edit'          , 'uses' => 'FrontViews\GalleryController@edit']);
    Route::post('/Update/{id}'          , ['as' => '.update'        , 'uses' => 'FrontViews\GalleryController@update']);
    Route::post('/Status/Update/{id}'   , ['as' => '.status-update' , 'uses' => 'FrontViews\GalleryController@updateStatus']);
    Route::post('/Delete/{id}'          , ['as' => '.delete'        , 'uses' => 'FrontViews\GalleryController@destroy']);
    Route::post('/DeleteAll'            , ['as' => '.delete-all'    , 'uses' => 'FrontViews\GalleryController@destroyAll' ]);

});

