<?php


Route::group(['prefix' => 'ArtWorkGallery' , 'as' => 'art-work-Gallery' , 'middleware' => ['auth']] , function() {

    Route::get('/List'                  , ['as' => '.list'           , 'uses' => 'FrontViews\AdminGalleryController@index']);
    Route::post('/Bulk'                 , ['as' => '.bulk-list'      , 'uses' => 'FrontViews\AdminGalleryController@bulk']);
    Route::post('Show/Insert'           , ['as' => '.insert'         , 'uses' => 'FrontViews\AdminGalleryController@insert' ]);
    Route::get('/Add'                   , ['as' => '.add'            , 'uses' => 'FrontViews\AdminGalleryController@create']);
    Route::get('/Show'                  , ['as' => '.show'           , 'uses' => 'FrontViews\AdminGalleryController@show']);
    Route::get('/Approved'              , ['as' => '.approved'       , 'uses' => 'FrontViews\AdminGalleryController@approvedList']);
    Route::get('/Pending'               , ['as' => '.pending'        , 'uses' => 'FrontViews\AdminGalleryController@pendingList']);
    Route::get('/Search/ArtWork'        , ['as' => '.search-artwork' , 'uses' => 'FrontViews\AdminGalleryController@gallerySearch']);
    Route::post('/Create'               , ['as' => '.create'         , 'uses' => 'FrontViews\AdminGalleryController@store']);
    Route::get('/Edit/{id}'             , ['as' => '.edit'           , 'uses' => 'FrontViews\AdminGalleryController@edit']);
    Route::post('/Update/{id}'          , ['as' => '.update'         , 'uses' => 'FrontViews\AdminGalleryController@update']);
    Route::post('/Status/Update/{id}'   , ['as' => '.status-update'  , 'uses' => 'FrontViews\AdminGalleryController@updateStatus']);
    Route::post('/Delete/{id}'          , ['as' => '.delete'         , 'uses' => 'FrontViews\AdminGalleryController@destroy']);
    Route::post('/DeleteAll'            , ['as' => '.delete-all'     , 'uses' =>  'FrontViews\AdminGalleryController@destroyAll' ]);

});

