<?php

Route::group(['prefix' => 'Contact' , 'as' => 'contact' , 'middleware' => ['auth']] , function() {

    Route::get('/List' , ['as' => '.list' , 'uses' => 'FrontViews\ContactController@list']);
//    Route::get('/Add'  , ['as' => '.add' , 'uses'  => 'FrontViews\ContactController@create']);
//    Route::get('/Show/{id}'  , ['as' => '.show' , 'uses'  => 'FrontViews\ContactController@show']);
//    Route::post('/Create' , ['as' => '.create' , 'uses' => 'FrontViews\ContactController@store']);
//    Route::get('/Edit/{id}' , ['as' => '.edit' , 'uses' => 'FrontViews\ContactController@edit']);
//    Route::post('/Update/{id}' , ['as' => '.update' , 'uses' => 'FrontViews\ContactController@update']);
//    Route::post('/Status/Update/{id}' , ['as' => '.status-update' , 'uses' => 'FrontViews\ContactController@updateStatus']);
    Route::post('/Delete/{id}' , ['as' => '.delete' , 'uses' => 'FrontViews\ContactController@destroy']);
    Route::post('/DeleteAll'  ,['as' => '.delete-all'   , 'uses' =>  'FrontViews\ContactController@destroyAll' ]);

});

