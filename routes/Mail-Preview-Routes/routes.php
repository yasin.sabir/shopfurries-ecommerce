<?php


use Illuminate\Mail\Markdown;

Route::get('mail/contactus', function () {
    $markdown = new Markdown(view(), config('mail.markdown'));

    return $markdown->render('emails.contact');
});


Route::get('mail/verify', function () {
    $markdown = new Markdown(view(), config('mail.markdown'));

    return $markdown->render('emails.verifyUser');
});


Route::get('mail/reset', function () {
    $markdown = new Markdown(view(), config('mail.markdown'));

    return $markdown->render('emails.resetPassword');
});


Route::get('mail/prodsubmit', function () {
    $markdown = new Markdown(view(), config('mail.markdown'));

    return $markdown->render('emails.Product.ProductSubmitted');
});

Route::get('mail/prodsubmitadmin', function () {
    $markdown = new Markdown(view(), config('mail.markdown'));

    return $markdown->render('emails.Product.ProductSubmitted_Admin');
});


Route::get('mail/testpre', function () {
    $markdown = new Markdown(view(), config('mail.markdown'));

    //return $markdown->render('emails.ArtGallery.submitted');
    return $markdown->render('emails.ArtGallery.approved');
    //return $markdown->render('emails.ArtGallery.submitted');
});


Route::get('mail/neworder', function () {
    $markdown = new Markdown(view(), config('mail.markdown'));
    return $markdown->render('emails.Testing.test_new_order');
});


Route::get('mail/test', function () {
    $markdown = new Markdown(view(), config('mail.markdown'));
    return $markdown->render('emails.Testing.testing');
});



Route::get('mail/abandoned' , function (){
    $markdown = new Markdown(view(), config('mail.markdown'));
    return $markdown->render('emails.Abandoned.abandoned_email');
});

