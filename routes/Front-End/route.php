<?php


//Route::get('/', function () {
//    //return view('home');
//    return redirect(app()->getLocale());
//});

//Route::group(['prefix' => '{locale}', 'where' => ['locale' => '[a-zA-Z]{2}'],'middleware' => 'setlocale'], function() {
//    Route::get('/Dashboard','FrontViews\HomeController@dashboard')->middleware(['auth','active_user'])->name('dashboard');
//});
//Route::get('/home'                    , ' HomeController@index')->name('home');



Route::group([
    'prefix' => LaravelLocalization::setLocale() ,
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function(){
Route::get('/Dashboard'                         ,'FrontViews\HomeController@dashboard')->middleware(['auth','active_user'])->name('dashboard');
});

Route::get('/'                                  , 'FrontViews\HomeController@index')->middleware(['auth_maintenance'])->name('home');


//Other Pages
Route::get( '/About'                            , 'FrontViews\AboutController@index')->middleware(['auth_maintenance' ])->name('about');
Route::get('/Contact'                           , 'FrontViews\ContactController@index')->middleware(['auth_maintenance'])->name('contact-us');
Route::post('/Send-Query/'                      , 'FrontViews\ContactController@contact')->middleware(['auth_maintenance'])->name('contact-send');
Route::get('/Gallery'                           , 'FrontViews\GalleryController@index')->middleware(['auth_maintenance'])->name('gallery');
Route::get('/Categories'                        , 'FrontViews\CategoryFV_Controller@index')->middleware(['auth_maintenance'])->name('category');
Route::get('/FAQs'                              , 'FrontViews\FAQsController@index')->middleware(['auth_maintenance'])->name('faqs');
Route::get('/Search'                           , 'FrontViews\SearchController@index')->name('searchResult');
Route::get('/Event/{name}/{event_id}'           , 'EventsController@get_single_event')->middleware(['auth_maintenance'])->name('single_event');



//Pagination for product in Shop Page & Bundle Shop Page
Route::get('/Shop'                              , 'FrontViews\ShopController@index')->middleware(['auth_maintenance'])->name('shop');
//, 'page-not-found'
Route::post('/Shop'                             , 'FrontViews\ShopController@index')->middleware(['auth_maintenance'])->name('filter_data');
Route::get('/Shop/Bundle'                       , 'FrontViews\ShopController@bundleProduct_index')->middleware(['auth_maintenance'])->name('bundle_shop');
Route::post('/Shop/Bundle'                      , 'FrontViews\ShopController@bundleProduct_index')->middleware(['auth_maintenance'])->name('bundle_filter_data');
Route::get('/Shop/fetch_data'                   , 'FrontViews\ShopController@fetch_data')->middleware(['auth_maintenance'])->name('shop_fetch_data');


//Single Product Page
//Route::get('Shop/Product/{name}/{id}'         , 'FrontViews\ShopController@get_SingleProduct')->middleware(['auth_maintenance'])->name('single_product');
Route::get('Shop/Product/{name}'                , 'FrontViews\ShopController@get_SingleProduct')->middleware(['auth_maintenance'])->name('single_product');
Route::get('Shop/Bundle/Product/{name}/{id}'    , 'FrontViews\ShopController@get_SingleBundleProduct')->middleware(['auth_maintenance'])->name('single_bundleProduct');


//Pagination for product in Category Page
Route::get('/Categories'                        , 'FrontViews\CategoryFV_Controller@index')->middleware(['auth_maintenance' ])->name('categories');
Route::post('/Categories'                       , 'FrontViews\CategoryFV_Controller@index')->middleware(['auth_maintenance' ])->name('categories_filter_data');
Route::get('/Category/{name}/{cat_id}'          , 'CategoryController@get_category_products')->middleware(['auth_maintenance'])->name('category_products');
Route::post('/Category/{name}/{cat_id}'         , 'CategoryController@get_category_products_filter')->middleware(['auth_maintenance'])->name('category_products_filter');




//Checkout , Coupon , Billing Routes
Route::get('/Checkout/{id}'                     , 'FrontViews\ShopController@checkout')->middleware(['auth_maintenance'])->name('checkout');
Route::post('/Delete_Cart/{id}'                 , 'FrontViews\ShopController@delete_temp_item')->name('delete_temp_item');
Route::post('/Update_Cart'                      , 'FrontViews\ShopController@update_temp_item')->name('update_temp_item');
Route::post('/couponapply'                      , 'FrontViews\ShopController@couponapply')->name('couponapply');
Route::get('/ThankYou'                          , 'FrontViews\ShopController@thankyouView')->name('thankyouView');

Route::get('/Billing' , function(){
    return view('front-views.billing-summary');
});

Route::get('/image', function() {
    //$img = Image::make('https://via.placeholder.com/1500')->resize(500,500);
    $img = Image::make('https://via.placeholder.com/1500')
                ->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
    return $img->response('jpg');
});
