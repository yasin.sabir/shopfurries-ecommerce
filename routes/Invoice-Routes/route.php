<?php

Route::group(['prefix' => 'Invoice' , 'as' => 'invoice' , 'middleware' => ['auth']], function () {

    Route::get('/List'                 ,['as' => '.list'                ,'uses' => 'InvoiceController@index']);
    Route::post('/generateInvoice'      ,['as' => '.generate-invoice'    ,'uses' => 'InvoiceController@generateInvoice' ]);
    Route::post('/Delete/{id}'          ,['as' => '.delete'              ,'uses' => 'InvoiceController@destroy' ]);
    Route::post('/DeleteAll'            ,['as' => '.delete-all'          ,'uses' => 'InvoiceController@destroyAllOrders' ]);

});

