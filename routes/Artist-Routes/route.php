<?php


Route::group(['prefix' => 'Artist' , 'as' => 'artist' , 'middleware' => ['auth']] , function() {

    Route::get('/List' , ['as' => '.list' , 'uses' => 'ArtistController@index']);
    Route::get('/Add'  , ['as' => '.add' , 'uses'  => 'ArtistController@create']);
    Route::get('/Show/{id}'  , ['as' => '.show' , 'uses'  => 'ArtistController@show']);
    Route::post('/Create' , ['as' => '.create' , 'uses' => 'ArtistController@store']);
    Route::get('/Edit/{id}' , ['as' => '.edit' , 'uses' => 'ArtistController@edit']);
    Route::post('/Update/{id}' , ['as' => '.update' , 'uses' => 'ArtistController@update']);
    Route::post('/Delete/{id}' , ['as' => '.delete' , 'uses' => 'ArtistController@destroy']);
    Route::post('/DeleteAll'  ,['as' => '.delete-all'   , 'uses' =>  'ArtistController@destroyAll' ]);

});
