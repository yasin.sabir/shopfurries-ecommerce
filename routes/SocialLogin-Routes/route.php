<?php

Route::get('login/github', 'Auth\LoginController@redirectToProvider_github');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback_github');

Route::get('login/google', 'Auth\LoginController@redirectToProvider_google');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback_google');

Route::get('login/twitter', 'Auth\LoginController@redirectToProvider_twitter');
Route::get('login/twitter/callback', 'Auth\LoginController@handleProviderCallback_twitter');

Route::get('login/deviantart', 'Auth\LoginController@redirectToProvider_deviantart');
Route::get('login/deviantart/callback', 'Auth\LoginController@handleProviderCallback_deviantart');




Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
